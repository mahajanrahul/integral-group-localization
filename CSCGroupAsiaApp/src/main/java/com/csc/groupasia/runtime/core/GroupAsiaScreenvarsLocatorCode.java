package com.csc.groupasia.runtime.core;

import java.util.List;

import com.csc.fsuframework.core.FsuAsiaScreenvarsLocatorCode;
import com.quipoz.framework.util.QPUtilities;

/**
 * @author Quipoz - Chris Hulley
 * @version 2.0 SMART specific Nov 2007
 *
 * Purpose: To locate the classpath where screen variables are located. Inherits its data from
 * AppLocator, and just replaces "procedures" with "variables" in the path.
 */
public class GroupAsiaScreenvarsLocatorCode extends FsuAsiaScreenvarsLocatorCode {
	
	static {
		final List<String> locations = getLocations();
		locations.clear();
		for (String s : GroupAsiaAppLocatorCode.getLocations()) {
			if (s.endsWith(".procedures")) {
				s = QPUtilities.replaceSubstring(s, ".procedures", ".screens");
			} else if (s.endsWith(".programs")) {
				s = QPUtilities.replaceSubstring(s, ".programs", ".screens");
			} else if (s.endsWith(".tests")) {
				s = QPUtilities.replaceSubstring(s, ".tests", ".variables");
			}
			if (!locations.contains(s)) {
				locations.add(s);
			}
		}
	};
	/**
	 * Getter of locations
	 * 
	 * @return
	 */
	public static List<String> getLocations() {
		return FsuAsiaScreenvarsLocatorCode.getLocations();
	}

}
