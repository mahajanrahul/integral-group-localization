package com.csc.groupasia.runtime.variables;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.common.DD;
import com.csc.smart.dataaccess.FlddTableDAM;
import com.csc.smart400framework.SmartVarModel;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datadictionarydatatype.DDScreenWindowing;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.BaseModel;
public class GroupAsiaAppVarsSon extends GroupAsiaAppVars {
	
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GroupAsiaAppVarsSon.class);
	
	private HttpServletRequest request;
	
	public HttpServletRequest getRequest() {
		return request;
	}
	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}
	public GroupAsiaAppVarsSon(String pAppName) {
		super(pAppName);
	}
	public Map<String, Map<String, String>> getLongDesc(String[][] fields,
			String language, String company, BaseModel bm, SmartVarModel vm,String longorshort) {
		/*
		
		Map<String, Map<String, String>> descMap = new HashMap<String, Map<String, String>>();
		
		String[] itemFields = fields[0]; // get item desc according to DD
		String[] tableFields = fields[1]; // get item desc according to passed
		String[] tableDesc = fields[2]; // get table desc
		
		Map<String, String> tempKey = new HashMap<String, String>();
		tempKey.put(" ", " ");
		
		for(String ss:itemFields){
			descMap.put(ss, tempKey);
		}
		
		for(String ss:tableFields){
			descMap.put(ss, tempKey);
		}
		
		for(String ss:tableDesc){
			descMap.put(ss, tempKey);
		}
		return descMap;
	*/

		Map<String, Map<String,String>> descMap	=	new HashMap<String,Map<String,String>>();
		Map<String, String> fieldTable			=	new HashMap<String,String>();//Map of field name and its window table
		Map<String, String> tempMap				=	new HashMap<String,String>();//Map of item code and its discription
		String[] itemFields 	=	fields[0];	//get item desc according to DD
		String[] tableFields	=	fields[1];	//get item desc according to passed tables' name
		String[] tableDesc		=	fields[2];	//get table desc
		String sql = "";
		/**
		 * If  TCOY = "F"-use WSSP-FSUCO from appvar
		 * TCOY = "S"-use WSSP-COMPANY from appvar
		 * TCOY =  numeric-use directly from TCOY
		 * */
		if(company.trim().equalsIgnoreCase("F")){
			company=bm.getFsuco().toString().trim();
		}else if(company.trim().equalsIgnoreCase("S")){
			company=bm.getCompany().toString().trim();
		}
		Connection conn 		=	null;		
		StringBuffer sqlsb		=	new StringBuffer();
		PreparedStatement ps	=	null;
		ResultSet rs			=	null;
		try 
		{
		
			sqlsb.append("SELECT D.DESCITEM, D."+longorshort+", D.DESCTABL, D.DESCCOY, D.DESCPFX FROM VM1DTA.DESCPF D, VM1DTA.ITEMPF I WHERE" +
					" D.DESCITEM=I.ITEMITEM AND I.VALIDFLAG='1'" +
					" AND D.DESCTABL = I.ITEMTABL" +
					" AND D.DESCCOY = I.ITEMCOY" );
			sqlsb.append(" AND D.DESCCOY ='");
			sqlsb.append(company);
			sqlsb.append("' AND D.\"LANGUAGE\" =  '");
			String lan = (String)request.getSession().getAttribute("language");
			if("zh".equalsIgnoreCase(lan)){
				sqlsb.append("S");
			}else if("en".equalsIgnoreCase(lan)){
				sqlsb.append("E");
			}
			//sqlsb.append(language);
			sqlsb.append("' AND (");
			if(itemFields.length > 0 || tableFields.length > 0) 
			{
					sqlsb.append("D.DESCPFX='IT'");
					if(tableDesc.length > 0) sqlsb.append(" or D.DESCPFX='HE'");
			}
			else if(itemFields.length == 0 && tableFields.length == 0)
			{
				if(tableDesc.length > 0) sqlsb.append(" D.DESCPFX='HE'");
			}
			sqlsb.append(")");
			sqlsb.append(" AND D.DESCTABL IN(");
	
			for(String field:itemFields)
			{
				field=field.trim();
				String realFName=getRealFieldName(field);
				String[] fieldInf=findF4FieldTable(realFName,vm, bm);
				fieldTable.put(fieldInf[1], field+","+fieldInf[2]);
				sqlsb.append("'"+fieldInf[1]+"',");
			}
			
			for(String field:tableFields)
			{
				sqlsb.append("'"+field+"',");
				//use table name as the map key
				fieldTable.put(field, field+"," + bm.getCompany().toString().trim());
			}
			
			for(String field:tableDesc)
			{
				sqlsb.append("'"+field+"',");
				//use TXXXdesc as the map key
				fieldTable.put(field+"desc", field+"desc," + bm.getCompany().toString().trim());
			}
			
			sql = sqlsb.toString();
			sql = sql.substring(0,sql.length()-1)+") ORDER BY DESCTABL ASC";
			
	
			conn = this.getAnotherDBConnection("DESCPF");

			ps = this.prepareStatement(conn, sql);
			rs = this.executeQuery(ps);
			boolean found=false;
			int index=0;
			String itemtable="";//table code
			
			while (rs.next()) 
			{
				index++;
				found = true;
						
				if(rs.getString(5).equals("HE"))
				{
					itemtable = rs.getString(3);
					itemtable = itemtable.trim()+"desc";
										
					if(fieldTable.get(itemtable) != null)
					{						
							tempMap.put(itemtable, rs.getString(2));
							//not count else when this is the first record, it will not 
							//run into next "else if(index==1)"
							index--;
					}
					else
					{
						//if the description of table wasn't apply by user,then pass it.
						continue;
					}
				}
				else if(index==1)
				{
					itemtable = rs.getString(3);	
					tempMap.put(rs.getString(1).trim(), rs.getString(2));
				}
				else
				{
					//get item descriptions that are in the same table 
					if(itemtable.equals(rs.getString(3)))
					{
						tempMap.put(rs.getString(1).trim(), rs.getString(2));
					}
					else
					{						
						//when get the map of next table, run into here
						String tempf=fieldTable.get(itemtable).split(",")[0];
						Map temp=new HashMap();
						temp.putAll(tempMap);
						descMap.put(tempf,temp);
						tempMap.clear();
						itemtable=rs.getString(3);
						tempMap.put(rs.getString(1).trim(), rs.getString(2));							
					  }
				}
			}	
			
			//for the last table data map
			String tt = fieldTable.get(itemtable);
			if(fieldTable!=null && tt!=null){
				String tempf=fieldTable.get(itemtable).split(",")[0];
				Map temp=new HashMap();
				temp.putAll(tempMap);
				descMap.put(tempf,temp);	
			}	
			
			if (found) 
			{
				return descMap;
			}
		} 
		catch (Exception e) 
		{
		    LOGGER.error("", e);
		}
		finally 
		{
			freeDBConnectionIgnoreErr(conn, ps, rs);
		}
		
		return descMap;
	
	
	}
	
public Object loadF4(BaseData bd, BaseModel bm,String language, Boolean isLong){
	Object returnObject = null;
		// First get Screen Window
		ScreenModel sm = bm.getScreenModel();
		SmartVarModel vm = (SmartVarModel)sm.getVariables();
		DDScreenWindowing ddSw = DD.getScreenWindowing(bd, sm.getScreenName());
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("CO", bm.getCompany());
		map.put("FSUCO", bm.getFsuco());
		map.put("LANG", language);
		if (ddSw != null) {
			if (ddSw.program1 != null && !ddSw.program1.trim().equals("")
					&& !ddSw.program1.equals("P0127")) {
				BaseData[] arr = new BaseData[5];
				FlddTableDAM flddIO = new FlddTableDAM();
				arr[1] = getField(vm, ddSw.additionalField1, flddIO);
				arr[2] = getField(vm, ddSw.additionalField2, flddIO);
				arr[3] = getField(vm, ddSw.additionalField3, flddIO);
				arr[4] = getField(vm, ddSw.additionalField4, flddIO);
				returnObject =  callF4InProgram(ddSw, arr, isLong, map);
			}
		}
		if(returnObject!=null){
			return returnObject;
		}
		Map<String, String> tempKey = new HashMap<String, String>();
		tempKey.put(" ", " ");
		return tempKey;
	}
private BaseData getField(SmartVarModel vm, String s,FlddTableDAM flddIO){
	if (s == null){
		return null;
	}
	flddIO.setParams(SPACES);
	flddIO.setFunction("READR");
	flddIO.setFormat("FLDDREC");
	flddIO.setFieldId(s.toUpperCase());
	SmartFileCode.execute(this, flddIO);
	if (isEQ(flddIO.getStatuz(),"****")) {
		String Name = flddIO.getNameForCobol().trim().toLowerCase();
		return vm.getField(Name, BaseData.class);
	}
	return null;
}

private Object callF4InProgram(DDScreenWindowing ddsw, BaseData[] arr,Boolean isLong, Map<String,Object> map) {
	
	
	String whatToCallName = ddsw.program1.toString().trim();
	Class whatToCallClass = null;
	
		try {
			whatToCallClass = (Class) this.getProgram(whatToCallName);
			Method m = whatToCallClass.getMethod("f4", new Class[] {
					COBOLAppVars.class, DDScreenWindowing.class, BaseData[].class, Boolean.class,Map.class});
			return m.invoke(null, this, ddsw, arr, isLong, map );
	} catch (Exception e) {
	    LOGGER.error("", e);
	}

	return null;
}

private String getRealFieldName(String tempFieldName){
	int j = tempFieldName.indexOf("Disp");
	if (j != -1){
		tempFieldName = tempFieldName.substring(0, j);
	}
	int rowNo = 0;
	/* If it is subfile field, then set subfile line no as well */
	int subfilePrefixIndex = tempFieldName.indexOf("_R");
	if (subfilePrefixIndex >= 0) {
		int subfileSuffixIndex = 
			tempFieldName.lastIndexOf("_R");
		if (subfileSuffixIndex > subfilePrefixIndex) {
			rowNo = Integer.parseInt(
					tempFieldName.substring(
							subfileSuffixIndex+"_R".length()).trim());
			tempFieldName = 
				tempFieldName.substring(
						subfilePrefixIndex+1, subfileSuffixIndex);
		} else {
			tempFieldName = tempFieldName.substring(subfilePrefixIndex+1);
		}
	} 
	return tempFieldName;
}
private String[] findF4FieldTable(String fieldN, SmartVarModel vm, BaseModel bm){
	String[] s=new String[3];
	s[0]=fieldN;
	s[1]="";
	try {
		java.lang.reflect.Field f = vm.getClass().getField(fieldN); 	
		BaseData bd = (BaseData) f.get(vm);
		
		DD dd = new DD();
		/* Wayne Yang 2010-04-01
		 * Change to use current screen name to get DD definition
		 * Note: In DD definition, all Screen name is upper case, therefore change the case to uppercase.
		 */
		DDScreenWindowing ddSw=(DDScreenWindowing)((HashMap)this.request.getSession().getAttribute("sessionMap")).get(fieldN);
		if (ddSw != null) {
			if (ddSw.windowTable != null
			&& !ddSw.windowTable.equals("")) {
				s[1]=ddSw.windowTable;
				s[2]=ddSw.tableCompany;
				/**
				 * If  TCOY = "F"-use WSSP-FSUCO from appvar
				 * TCOY = "S"-use WSSP-COMPANY from appvar
				 * TCOY =  numeric-use directly from TCOY
				 * */
				if(s[2].trim().equalsIgnoreCase("F")){
					s[2]=bm.getFsuco().toString().trim();
				}else if(s[2].trim().equalsIgnoreCase("S")){
					s[2]=bm.getCompany().toString().trim();
				}
			}
		}

	}
	catch (Exception e) {
		s[1]="can't find window table of "+fieldN;
		LOGGER.error("", e);
		return new String[]{fieldN,"",""};
	}
	return s;
}

/**
 * This method returns long description and item description from the database
 * @param fields - screen control field array  e.g. {cltsex,cltnum,dob}
 * @param vm -  smartVarModel
 * @param lang - Language to be passed e.g. E for English and CH for chinese
 * @param bm - baseModel
 * @return Map
 * Added by shawn for UIServlet 2012-01-11
 */
public Map<String,Map<String,String>> loadF4FieldsLong(String[] fields, SmartVarModel vm, String lang,BaseModel bm){
	Map<String, Map<String,String>> f4map=new HashMap<String,Map<String,String>>();//Map of F4 Fields and its dropdown list
	Map<String, String> fieldTable=new HashMap<String,String>();//Map of field name and its window table
	Map<String, String> fieldCompany=new HashMap<String,String>();//Map of field name and its company
	Map<String, String> itemDesc=new HashMap<String,String>();//Map of item code and its discription
	
	StringBuilder sqlsb=new StringBuilder();
	sqlsb.append("SELECT DESCITEM, LONGDESC, DESCTABL,DESCCOY FROM vm1dta.DESCPF WHERE DESCPFX='IT'");
	//sqlsb.append(" AND DESCCOY ='1'");
	sqlsb.append(" AND \"LANGUAGE\" =  '");
	sqlsb.append(lang);
	sqlsb.append("' AND DESCTABL IN(");
	
	String generalDropdownSearchSQL="";
	String tblName ="";
	Connection conn=null;
	
	for(String field:fields){
		field=field.trim();
		String realFName=getRealFieldName(field);
		String[] fieldInf=findF4FieldTable(realFName,vm, bm);			
		tblName = "'"+fieldInf[1]+"',";
		generalDropdownSearchSQL=sqlsb.toString()+tblName.toString();
		generalDropdownSearchSQL=generalDropdownSearchSQL.substring(0,generalDropdownSearchSQL.length()-1)+") ORDER BY LONGDESC ASC";
		
		try {
			conn = this.getAnotherDBConnection("DESCPF");
		} catch (SQLException e1) {
			LOGGER.error("SQLException:\n", e1);
		}
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = this.prepareStatement(conn, generalDropdownSearchSQL);				
			rs = this.executeQuery(ps);
			
			while (rs.next()) {
				itemDesc.put(rs.getString("DESCITEM").trim(), rs.getString("LONGDESC").trim());						
			}
			f4map.put(fieldInf[0],itemDesc );
			itemDesc = new HashMap();
		}  catch (Exception e) {
		    LOGGER.error("", e);
		}finally {
			freeDBConnectionIgnoreErr(conn, ps, rs);
		}
	

	
}
	return f4map;
}

/**
 * This method returns short description and item description from the database
 * @param fields - screen control field array  e.g. {cltsex,cltnum,dob}
 * @param vm -  smartVarModel
 * @param lang - Language to be passed e.g. E for English and CH for chinese
 * @param bm - baseModel
 * @return Map
 * Added by shawn for UIServlet 2012-01-11
 */
public Map<String,Map<String,String>> loadF4FieldsShort(String[] fields, SmartVarModel vm, String lang,BaseModel bm){
	Map<String, Map<String,String>> f4map=new HashMap<String,Map<String,String>>();//Map of F4 Fields and its dropdown list
	Map<String, String> fieldTable=new HashMap<String,String>();//Map of field name and its window table
	Map<String, String> fieldCompany=new HashMap<String,String>();//Map of field name and its company
	Map<String, String> itemDesc=new HashMap<String,String>();//Map of item code and its discription
	
	StringBuilder sqlsb=new StringBuilder();
	sqlsb.append("SELECT DESCITEM, SHORTDESC, DESCTABL,DESCCOY FROM vm1dta.DESCPF WHERE DESCPFX='IT'");
	//sqlsb.append(" AND DESCCOY ='1'");
	sqlsb.append(" AND \"LANGUAGE\" =  '");
	sqlsb.append(lang);
	sqlsb.append("' AND DESCTABL IN(");
	
	String generalDropdownSearchSQL="";
	String tblName ="";
	Connection conn=null;
	
	for(String field:fields){
		field=field.trim();
		String realFName=getRealFieldName(field);
		String[] fieldInf=findF4FieldTable(realFName,vm, bm);			
		tblName = "'"+fieldInf[1]+"',";
		generalDropdownSearchSQL=sqlsb.toString()+tblName.toString();
		generalDropdownSearchSQL=generalDropdownSearchSQL.substring(0,generalDropdownSearchSQL.length()-1)+") ORDER BY SHORTDESC ASC";
		
		try {
			conn = this.getAnotherDBConnection("DESCPF");
		} catch (SQLException e1) {
			LOGGER.error("SQLException:\n", e1);
		}
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = this.prepareStatement(conn, generalDropdownSearchSQL);				
			rs = this.executeQuery(ps);
			
			while (rs.next()) {
				itemDesc.put(rs.getString("DESCITEM").trim(), rs.getString("SHORTDESC").trim());						
			}
			f4map.put(fieldInf[0],itemDesc );
			itemDesc = new HashMap();
		}  catch (Exception e) {
		    LOGGER.error("", e);
		}finally {
			freeDBConnectionIgnoreErr(conn, ps, rs);
		}
	


}
	return f4map;
}

}
