package com.csc.groupasia.runtime.variables;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.common.DD;
import com.csc.fsu.general.dataaccess.AnumTableDAM;
import com.csc.fsuframework.variables.FsuAsiaAppVars;
import com.csc.groupasia.runtime.core.GroupAsiaAppLocatorCode;
import com.csc.smart.dataaccess.FlddTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SmartVarModel;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.quipoz.COBOLFramework.common.exception.ExtMsgException;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datadictionarydatatype.DDScreenWindowing;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.BaseModel;

/**
 * Extends SMARTAppVars. Inncludes global variables applicable to Polisy.
 *
 * @author Quipoz - Max Wang
 * @version 1.0 April 2009<br>
 */
public class GroupAsiaAppVars extends FsuAsiaAppVars implements Cloneable, Serializable {

	private static final Logger LOGGER = LoggerFactory.getLogger(GroupAsiaAppVars.class);

	/** Generated UID */
	private static final long serialVersionUID = 1260792893645326723L;

	private static final String generalOptswchSQL="SELECT DESCITEM, LONGDESC FROM vm1dta.DESCPF D, VM1DTA.ITEMPF I WHERE D.DESCITEM=I.ITEMITEM AND I.VALIDFLAG='1' AND D.DESCPFX='IT'" +
	" AND D.DESCTABL='T1660' and D.DESCCOY=? AND D.LANGUAGE=?";
	
	/**
	 * Constructor with application name. Simply invokes super constructor
	 *
	 * @param pAppName -
	 */
	public GroupAsiaAppVars(String pAppName) {
		super(pAppName);
	}

	/**
	 * Class to resolve a program according to the application's class path.
	 * This overrides the COBOLAppVars version and actually does something with an
	 * AppLocator.
	 *
	 * @param progname
	 * @return
	 */
	public Object getProgram(Object progname) {
		addDiagnostic("Transfer-to '" + progname + "'");
		return GroupAsiaAppLocatorCode.find(progname.toString());
	}

	/**
	 * Routine available to free any resources eg caches. Frees cached Svcmtyp.
	 */
	public void freeResources(String... exceptFullClassNames) {
		super.freeResources(exceptFullClassNames);
	}

	private String getRealFieldName(String tempFieldName){
		int j = tempFieldName.indexOf("Disp");
		if (j != -1){
			tempFieldName = tempFieldName.substring(0, j);
		}
		int rowNo = 0;
		/* If it is subfile field, then set subfile line no as well */
		int subfilePrefixIndex = tempFieldName.indexOf("_R");
		if (subfilePrefixIndex >= 0) {
			int subfileSuffixIndex =
				tempFieldName.lastIndexOf("_R");
			if (subfileSuffixIndex > subfilePrefixIndex) {
				rowNo = Integer.parseInt(
						tempFieldName.substring(
								subfileSuffixIndex+"_R".length()).trim());
				tempFieldName =
					tempFieldName.substring(
							subfilePrefixIndex+1, subfileSuffixIndex);
			} else {
				tempFieldName = tempFieldName.substring(subfilePrefixIndex+1);
			}
		}
		return tempFieldName;
	}

	/* UIG Changes Starts*/
	private String getF4Items(SmartVarModel vm,String lang,String[] fi){
		StringBuffer result = new StringBuffer();

		Connection conn=null;
		try {
			conn = this.getAnotherDBConnection("DESCPF");
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			LOGGER.error("SQLException:\n", e1);
		}
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String generalSearchSQL = null;
			ps = this.prepareStatement(conn, generalSearchSQL);
			setDBString(ps, 1, lang);
			setDBString(ps, 2, fi[1]);
			rs = this.executeQuery(ps);
			boolean found=false;
			while (rs.next()) {
				found = true;
				//<option  value="A-Agent Net Commission">A-Agent Net Commission </option>
				String temps=rs.getString(1)+"-"+rs.getString(2);
				result.append("<option value=\""+temps+"\">"+temps+"</option>");
			}

			if (found) {
				result.insert(0, "<option selected=\"selected\" value=\"\">    </option>");
				return result.toString();
			}

			return "No ITEM found on the system.<p>";

		} catch (Exception e) {
			result.append("<p>SQL Error! " + e.toString());
		}
		finally {
			freeDBConnectionIgnoreErr(conn, ps, rs);
		}

		return result.toString();
	}
	/**
	 * return an array, first is field name, second is its table
	 * */
	private String[] findF4FieldTable(String fieldN, SmartVarModel vm, BaseModel bm){
		String[] s=new String[3];
		s[0]=fieldN;
		s[1]="";
		try {
			java.lang.reflect.Field f = vm.getClass().getField(fieldN);

			BaseData bd = (BaseData) f.get(vm);

			DD dd = new DD();

			LOGGER.debug(bm.getScreenModel().getScreenName());
			DDScreenWindowing ddSw;
			/*Modified by atiwari :Bug#5245			 */
			if(bm.getScreenModel().getScreenName().equals("S9126"))
				ddSw=dd.getScreenWindowing(bd, "S9126");
			else
				 ddSw=dd.getScreenWindowing(bd, "S4033");
			if (ddSw != null) {
				if (ddSw.windowTable != null
				&& !ddSw.windowTable.equals("")) {
					s[1]=ddSw.windowTable;
					s[2]=ddSw.tableCompany;
					/**
					 * If  TCOY = "F"-use WSSP-FSUCO from appvar
					 * TCOY = "S"-use WSSP-COMPANY from appvar
					 * TCOY =  numeric-use directly from TCOY
					 * */
					if(s[2].trim().equalsIgnoreCase("F")){
						s[2]=this.getFsuco().toString().trim();
					}else if(s[2].trim().equalsIgnoreCase("S")){
						s[2]=this.getCompany().toString().trim();
					}
				}
				}else{
					if(s[0].trim().equals("agntype")){
						s[1]="T3692";
						s[2]="1";
					}else if(s[0].trim().equals("payfreq")){
						s[1]="T3590";
						s[2]="2";
					}else if(s[0].trim().endsWith("pstate")){
						s[1]="T3588";
						s[2]="2";
					}else if(s[0].trim().endsWith("instcchnl")){
						s[1]="T3692";
						s[2]="1";
					}else if(s[0].trim().endsWith("premout")){
						s[1]="TR9W3";
						s[2]="1";
					}else if(s[0].trim().endsWith("membreq")){
						s[1]="TR9W4";
						s[2]="1";
					}else if(s[0].trim().endsWith("selcomp")){
						s[1]="T1693";
						s[2]="1";
					}else if(s[0].trim().endsWith("acctyp")){
						s[1]="T3692";
						s[2]="1";
					}else if(s[0].trim().endsWith("afund")){
						s[1]="T3595";
						s[2]="1";
					}else if(s[0].trim().endsWith("status")){
						s[1]="T3623";
						s[2]="1";
					}

				}
		}
		catch (Exception e) {
			s[1]="can't find window table of "+fieldN;
			LOGGER.error("", e);
			return new String[]{fieldN,"",""};
		}
		return s;
	}

	/**
	 * This function is used to get option switch long description
	 * and this is taken from Polisy Code
	 * */
	public Map getOptswchDesc(BaseModel bm){

		Map<String, String> optswchMap=new HashMap<String,String>();

		// get language
		FixedLengthStringData language = this.getUserLanguage();
		String lang = this.getLanguageCode();

		Connection conn = null;
		try {

			conn = this.getAnotherDBConnection("DESCPF");
		} catch (SQLException e1) {
			LOGGER.error("SQLException:\n", e1);
		}
		PreparedStatement ps = null;
   		ResultSet rs = null;
		try {

			ps = this.prepareStatement(conn, generalOptswchSQL);
			setDBString(ps, 1, "3");
			setDBString(ps, 2, lang);
			rs = this.executeQuery(ps);
			while (rs.next()) {
				optswchMap.put(rs.getString(1).trim(), rs.getString(2).trim());
			}


		} catch (Exception e) {
			optswchMap.clear();
		} finally {
			freeDBConnectionIgnoreErr(conn, ps, rs);
		}


		return optswchMap;
	}

	// Used to get long description
	public Map<String, Map<String, String>> getLongDesc(String[][] fields, String language, String company, BaseModel bm, SmartVarModel vm) {
		language = this.getLanguageCode();
		return getLongDesc(fields, language, company, bm, vm, "LONGDESC");
	}

	// Used to get short description
	public Map<String, Map<String, String>> getShortDesc(String[][] fields, String language, String company, BaseModel bm, SmartVarModel vm) {
		language = this.getLanguageCode();
		return getLongDesc(fields, language, company, bm, vm, "SHORTDESC");
	}

	/**
	 * This method is used to get long or short description(From table DESCPF) for the following three situations: 1. The item whoselong description was defined
	 * in DD. 2. The item whose long description was <B>not</B> defined in DD. 3. The long description about a table(Its DESCPFX is "HE").
	 *
	 * @author Ai Hao
	 * @param fields
	 *            String[][]
	 * @param language
	 *            String
	 * @param company
	 *            String
	 * @param bm
	 *            BaseModel
	 * @param vm
	 *            SmartVarModel
	 * @return Map<String,Map<String,String>>
	 */
	public Map<String, Map<String, String>> getLongDesc(String[][] fields, String language, String company, BaseModel bm, SmartVarModel vm, String longorshort) {
		Map<String, Map<String, String>> descMap = new HashMap<String, Map<String, String>>();
		Map<String, String> fieldTable = new HashMap<String, String>();// Map of field name and its window table
		Map<String, String> tempMap = new HashMap<String, String>();// Map of item code and its discription
		String[] itemFields = fields[0]; // get item desc according to DD
		String[] tableFields = fields[1]; // get item desc according to passed tables' name
		String[] tableDesc = fields[2]; // get table desc
		String sql = "";
		/**
		 * If TCOY = "F"-use WSSP-FSUCO from appvar TCOY = "S"-use WSSP-COMPANY from appvar TCOY = numeric-use directly from TCOY
		 * */
		if (company.trim().equalsIgnoreCase("F")) {
			company = bm.getFsuco().toString().trim();
		} else if (company.trim().equalsIgnoreCase("S")) {
			company = bm.getCompany().toString().trim();
		}
		Connection conn = null;
		StringBuffer sqlsb = new StringBuffer();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {

			sqlsb.append("SELECT D.DESCITEM, D." + longorshort + ", D.DESCTABL, D.DESCCOY, D.DESCPFX FROM VM1DTA.DESCPF D, VM1DTA.ITEMPF I WHERE"
					+ " D.DESCITEM=I.ITEMITEM AND I.VALIDFLAG='1'" + " AND D.DESCTABL = I.ITEMTABL" + " AND D.DESCCOY = I.ITEMCOY");
			sqlsb.append(" AND D.DESCCOY ='");
			sqlsb.append(company);
			sqlsb.append("' AND D.\"LANGUAGE\" =  '");
			sqlsb.append(language);
			sqlsb.append("' AND (");
			if (itemFields.length > 0 || tableFields.length > 0) {
				sqlsb.append("D.DESCPFX='IT'");
				if (tableDesc.length > 0)
					sqlsb.append(" or D.DESCPFX='HE'");
			} else if (itemFields.length == 0 && tableFields.length == 0) {
				if (tableDesc.length > 0)
					sqlsb.append(" D.DESCPFX='HE'");
			}
			sqlsb.append(")");
			sqlsb.append(" AND D.DESCTABL IN(");

			for (String field : itemFields) {
				field = field.trim();
				String realFName = getRealFieldName(field);
				String[] fieldInf = findF4FieldTable(realFName, vm, bm);
				fieldTable.put(fieldInf[1], field + "," + fieldInf[2]);
				sqlsb.append("'" + fieldInf[1] + "',");
			}

			for (String field : tableFields) {
				sqlsb.append("'" + field + "',");
				// use table name as the map key
				fieldTable.put(field, field + "," + bm.getCompany().toString().trim());
			}

			for (String field : tableDesc) {
				sqlsb.append("'" + field + "',");
				// use TXXXdesc as the map key
				fieldTable.put(field + "desc", field + "desc," + bm.getCompany().toString().trim());
			}

			sql = sqlsb.toString();
			sql = sql.substring(0, sql.length() - 1) + ") ORDER BY DESCTABL ASC";

			conn = this.getAnotherDBConnection("DESCPF");

			ps = this.prepareStatement(conn, sql);
			rs = this.executeQuery(ps);
			boolean found = false;
			int index = 0;
			String itemtable = "";// table code

			while (rs.next()) {
				index++;
				found = true;

				if (rs.getString(5).equals("HE")) {
					itemtable = rs.getString(3);
					itemtable = itemtable.trim() + "desc";

					if (fieldTable.get(itemtable) != null) {
						tempMap.put(itemtable, rs.getString(2));
						// not count else when this is the first record, it will not
						// run into next "else if(index==1)"
						index--;
					} else {
						// if the description of table wasn't apply by user,then pass it.
						continue;
					}
				} else if (index == 1) {
					itemtable = rs.getString(3);
					tempMap.put(rs.getString(1).trim(), rs.getString(2));
				} else {
					// get item descriptions that are in the same table
					if (itemtable.equals(rs.getString(3))) {
						tempMap.put(rs.getString(1).trim(), rs.getString(2));
					} else {
						// when get the map of next table, run into here
						String tempf = fieldTable.get(itemtable).split(",")[0];
						Map temp = new HashMap();
						temp.putAll(tempMap);
						descMap.put(tempf, temp);
						tempMap.clear();
						itemtable = rs.getString(3);
						tempMap.put(rs.getString(1).trim(), rs.getString(2));
					}
				}
			}

			Map temp=new HashMap();
			//Modified to check if the fieldTable is null for that itemtable.
			// if null then iterate the given fields and check whether the map contains
			//the field or not if it not then add that field with a blank map.
			// it will save the null pointer exception on the screen.
			if(fieldTable.get(itemtable) != null)
			{
			//for the last table data map
			String tempf=fieldTable.get(itemtable).split(",")[0];
			temp.putAll(tempMap);
			descMap.put(tempf,temp);
			}
			else
			{
				for(String field:itemFields)
				{
					if(descMap.get(field) == null)
					{
						descMap.put(field,temp);
					}
				}
			}
			if (found)
			{
				return descMap;
			}
		}
		catch (Exception e)
		{
            LOGGER.error("getLongDesc(String[][], String, String, BaseModel, SmartVarModel, String)", e);
		}
		finally
		{
			freeDBConnectionIgnoreErr(conn, ps, rs);
		}

		return descMap;
	}

	/**
	 * Load all the f4 fields in the screen
	 *
	 * return Map<String, Map<String,String>>
	 *
	 * */
	public Map<String,Map<String,String>> loadF4Fields(String[] fields, SmartVarModel vm, String lang,BaseModel bm){
		Map<String, Map<String,String>> f4map=new HashMap<String,Map<String,String>>();//Map of F4 Fields and its dropdown list
		Map<String, String> fieldTable=new HashMap<String,String>();//Map of field name and its window table
		Map<String, String> fieldCompany=new HashMap<String,String>();//Map of field name and its company
		Map<String, String> itemDesc=new HashMap<String,String>();//Map of item code and its discription

		StringBuilder sqlsb=new StringBuilder();
		sqlsb.append("SELECT D.DESCITEM, D.LONGDESC, D.DESCTABL, D.DESCCOY, D.DESCPFX FROM VM1DTA.DESCPF D, VM1DTA.ITEMPF I WHERE D.DESCITEM=I.ITEMITEM AND I.VALIDFLAG='1' AND D.DESCTABL = I.ITEMTABL AND D.DESCCOY = I.ITEMCOY");
		//sqlsb.append(" AND DESCCOY ='1'");
		sqlsb.append(" AND \"LANGUAGE\" =  '");
		sqlsb.append(lang);
		sqlsb.append("' AND \"VALIDFLAG\" =  '1'");
		sqlsb.append(" AND DESCTABL IN(");

		String generalDropdownSearchSQL="";

		for(String field:fields){
			field=field.trim();
			String realFName=getRealFieldName(field);
			String[] fieldInf=findF4FieldTable(realFName,vm, bm);
			fieldTable.put(fieldInf[1], field+","+fieldInf[2]);
			sqlsb.append("'"+fieldInf[1]+"',");
		}

		generalDropdownSearchSQL=sqlsb.toString();
		generalDropdownSearchSQL=generalDropdownSearchSQL.substring(0,generalDropdownSearchSQL.length()-1)+") ORDER BY DESCTABL ASC";

		Connection conn=null;

		try {
			conn = this.getAnotherDBConnection("DESCPF");
		} catch (SQLException e1) {
			LOGGER.error("SQLException:\n", e1);
		}

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = this.prepareStatement(conn, generalDropdownSearchSQL);
			LOGGER.debug("Testing"+ generalDropdownSearchSQL);
			rs = this.executeQuery(ps);
			boolean found=false;
			int index=0;
			String itemtable="";//table code
			String itemcoy="";//company code
			boolean single=true;//only one record.
			//DESCITEM, LONGDESC, DESCTABL,DESCCOY
			while (rs.next()) {
					index++;
					found = true;
				if(index==1){
					itemcoy=rs.getString(4);
					itemtable=rs.getString(3);
					String coyt=fieldTable.get(itemtable).split(",")[1];
					//select fields which interact with company
					if(itemcoy.equalsIgnoreCase(coyt)){
					itemDesc.put(rs.getString(1).trim(), rs.getString(2));
					}


				}else{
					if(itemtable.equals(rs.getString(3))){
						String coyt=fieldTable.get(itemtable).split(",")[1];
						//select fields which interact with company
						itemcoy=rs.getString(4);
						if(itemcoy.equalsIgnoreCase(coyt)){
						itemDesc.put(rs.getString(1).trim(), rs.getString(2));
						}
					}else{
						single=false;
						String tempf=fieldTable.get(itemtable).split(",")[0];
						Map temp=new HashMap();
						temp.putAll(itemDesc);
						f4map.put(tempf,temp);
						itemDesc.clear();
						itemtable=rs.getString(3);
						String coyt=fieldTable.get(itemtable).split(",")[1];
						//select fields which interact with company
						itemcoy=rs.getString(4);
						if(itemcoy.equalsIgnoreCase(coyt)){
						itemDesc.put(rs.getString(1).trim(), rs.getString(2));
						}
					}

				}

			}

				String tempf=fieldTable.get(itemtable).split(",")[0];
				Map temp=new HashMap();
				temp.putAll(itemDesc);
				f4map.put(tempf,temp);

			if (found) {
				return f4map;
			}

		} catch (Exception e) {
		    LOGGER.error("", e);
		}
		finally {
			freeDBConnectionIgnoreErr(conn, ps, rs);
		}


		return f4map;
	}
    /**
     * This method returns long description and item description from the database
     * @param fields - screen control field array  e.g. {cltsex,cltnum,dob}
     * @param vm -  smartVarModel
     * @param lang - Language to be passed e.g. E for English and CH for chinese
     * @param bm - baseModel
     * @return Map
     * @author CSCINDIA Sep 29th 2009
     */
    public Map<String,Map<String,String>> loadF4FieldsLong(String[] fields, SmartVarModel vm, String lang,BaseModel bm){
        Map<String, Map<String,String>> f4map=new HashMap<String,Map<String,String>>();//Map of F4 Fields and its dropdown list
        Map<String, String> itemDesc=new HashMap<String,String>();//Map of item code and its discription
        /*
         * I18N change
         */
        lang=this.getLanguageCode();
        /*
         * I18N change
         */
        StringBuilder sqlsb=new StringBuilder();

        sqlsb.append("SELECT D.DESCITEM, D.LONGDESC, D.DESCTABL,D.DESCCOY FROM vm1dta.DESCPF D");
        sqlsb.append(" INNER JOIN vm1dta.ITEMPF I on D.DESCPFX=I.ITEMPFX ");
        sqlsb.append("AND D.DESCITEM=I.ITEMITEM AND D.DESCCOY = I.ITEMCOY AND D.DESCTABL = I.ITEMTABL");
        sqlsb.append(" WHERE I.VALIDFLAG='1' AND D.DESCPFX='IT'");
        sqlsb.append(" AND D.\"LANGUAGE\" =  '");
        sqlsb.append(lang);
        sqlsb.append("' AND D.DESCTABL IN(");

        String generalDropdownSearchSQL="";
        String tblName ="";
        String company="";
        Connection conn=null;

        for(String field:fields){
            field=field.trim();
            String realFName=getRealFieldName(field);
            String[] fieldInf=findF4FieldTable(realFName,vm, bm);
            tblName = "'"+fieldInf[1]+"',";
            company="'"+fieldInf[2]+"'";
            generalDropdownSearchSQL=sqlsb.toString()+tblName.toString();
            generalDropdownSearchSQL=generalDropdownSearchSQL.substring(0,generalDropdownSearchSQL.length()-1);
            generalDropdownSearchSQL=generalDropdownSearchSQL+") AND D.DESCCOY ="+company.toString()+" ORDER BY LONGDESC ASC";


            try {
                conn = this.getAnotherDBConnection("DESCPF");
            } catch (SQLException e1) {
                LOGGER.error("loadF4FieldsLong(String[], SmartVarModel, String, BaseModel)", e1);
            }

            PreparedStatement ps = null;
            ResultSet rs = null;

            try {
                ps = this.prepareStatement(conn, generalDropdownSearchSQL);
                rs = this.executeQuery(ps);

                while (rs.next()) {
                    itemDesc.put(rs.getString("DESCITEM").trim(), rs.getString("LONGDESC").trim());
                }
                f4map.put(fieldInf[0],itemDesc );
                itemDesc = new HashMap();
            }  catch (Exception e) {
                e.printStackTrace();
            }finally {
                freeDBConnectionIgnoreErr(conn, ps, rs);
            }



    }
        return f4map;
}

    /**
     * This method returns long description and item description from the database
     * @param fields - screen control field array  e.g. {cltsex,cltnum,dob}
     * @param vm -  smartVarModel
     * @param lang - Language to be passed e.g. E for English and CH for chinese
     * @param bm - baseModel
     * @param startPosition - start character to Sub String
     * @param strCompare - String to compare
     * @return Map
     * @author CSCINDIA Sep 29th 2009
     */
    public Map<String,Map<String,String>> loadF4FieldsLong(String[] fields, SmartVarModel vm, String lang,BaseModel bm, int startPosition,  String strFilter)
    {
        Map<String, Map<String,String>> f4map=new HashMap<String,Map<String,String>>(); // Map of F4 Fields and its dropdown list
        Map<String, String> itemDesc=new HashMap<String,String>();//Map of item code and its discription

        lang=this.getLanguageCode();
      	StringBuilder sqlsb=new StringBuilder();

        sqlsb.append("SELECT SUBSTR(D.DESCITEM, '");
        sqlsb.append(strFilter.length() + 1);
        sqlsb.append("') as KEYVALUE, D.LONGDESC, D.DESCTABL,D.DESCCOY FROM vm1dta.DESCPF D");
        sqlsb.append(" INNER JOIN vm1dta.ITEMPF I on D.DESCPFX=I.ITEMPFX ");
        sqlsb.append("AND D.DESCITEM=I.ITEMITEM AND D.DESCCOY = I.ITEMCOY AND D.DESCTABL = I.ITEMTABL");
        sqlsb.append(" WHERE I.VALIDFLAG='1' AND D.DESCPFX='IT'");
        sqlsb.append(" AND D.\"LANGUAGE\" =  '");
        sqlsb.append(lang);
        sqlsb.append("' AND SUBSTR(D.DESCITEM,'");
        sqlsb.append(startPosition);
        sqlsb.append("','");
        sqlsb.append(strFilter.length());
        sqlsb.append("') = '");
        sqlsb.append(strFilter.toString().trim());
        sqlsb.append("' AND D.DESCTABL IN(");

        String generalDropdownSearchSQL="";
        String tblName ="";
        String company="";
        Connection conn=null;

        for(String field:fields){
            field=field.trim();
            String realFName=getRealFieldName(field);
            String[] fieldInf=findF4FieldTable(realFName,vm, bm);
            tblName = "'"+fieldInf[1]+"',";
            company="'"+fieldInf[2]+"'";
            generalDropdownSearchSQL=sqlsb.toString()+tblName.toString();
            generalDropdownSearchSQL=generalDropdownSearchSQL.substring(0,generalDropdownSearchSQL.length()-1);
            generalDropdownSearchSQL=generalDropdownSearchSQL+") AND D.DESCCOY ="+company.toString()+" ORDER BY LONGDESC ASC";

            try {
                conn = this.getAnotherDBConnection("DESCPF");
            } catch (SQLException e1) {
                LOGGER.error("loadF4FieldsLong(String[], SmartVarModel, String, BaseModel)", e1);
            }

            PreparedStatement ps = null;
            ResultSet rs = null;

            try {
                ps = this.prepareStatement(conn, generalDropdownSearchSQL);
                rs = this.executeQuery(ps);

                while (rs.next()) {
                    itemDesc.put(rs.getString("KEYVALUE").trim(), rs.getString("LONGDESC").trim());
                }
                f4map.put(fieldInf[0], itemDesc );
                itemDesc = new HashMap();
            }  catch (Exception e) {
                e.printStackTrace();
            }finally {
                freeDBConnectionIgnoreErr(conn, ps, rs);
            }



    }
        return f4map;
}
	/**
	 * This method returns long description and item description from the database by company code
	 * @param fields - screen control field array  e.g. {cltsex,cltnum,dob}
	 * @param vm -  smartVarModel
	 * @param lang - Language to be passed e.g. E for English and CH for chinese
	 * @param bm - baseModel
	 * @param company
	 * @return Map
	 * @author CSC Vietnam Jan 28th 2013
	 */
	public Map<String, Map<String, String>> loadF4FieldsLong(String[] fields,
			SmartVarModel vm, String lang, BaseModel bm, FixedLengthStringData company) {

		// Map of F4 Fields and its dropdown list
		Map<String, Map<String, String>> f4map = new HashMap<String, Map<String, String>>();

		// Map of item code and its discription
		Map<String, String> itemDesc = new HashMap<String, String>();

		StringBuilder sqlsb = new StringBuilder();
		sqlsb.append(	" SELECT D.DESCITEM, D.LONGDESC, D.DESCTABL, D.DESCCOY, D.DESCPFX "	);
		sqlsb.append(	" FROM VM1DTA.DESCPF D, VM1DTA.ITEMPF I "							);
		sqlsb.append(	" WHERE D.DESCPFX='IT' " 											);
		sqlsb.append(	" 	AND D.DESCITEM = I.ITEMITEM " 									);
		sqlsb.append(	" 	AND I.VALIDFLAG='1' " 											);
		sqlsb.append(	" 	AND D.DESCTABL = I.ITEMTABL " 									);
		sqlsb.append(	" 	AND D.DESCCOY = I.ITEMCOY "										);
		sqlsb.append(	" 	AND D.DESCCOY ='" + company.toString().trim() + "'"				);
		sqlsb.append(	"	AND \"LANGUAGE\" =  '"	+ lang	+ "'"							);
		sqlsb.append(	" 	AND \"VALIDFLAG\" =  '1'");
		sqlsb.append(	" 	AND DESCTABL IN(");

		String generalDropdownSearchSQL = "";
		String tblName = "";
		Connection conn = null;

		for (String field : fields) {
			field = field.trim();
			String realFName = getRealFieldName(field);
			String[] fieldInf = findF4FieldTable(realFName, vm, bm);
			tblName = "'" + fieldInf[1] + "',";
			generalDropdownSearchSQL = sqlsb.toString() + tblName.toString();
			generalDropdownSearchSQL = generalDropdownSearchSQL.substring(0,
					generalDropdownSearchSQL.length() - 1)
					+ ") ORDER BY LONGDESC ASC";

			try {
				conn = this.getAnotherDBConnection("DESCPF");
			} catch (SQLException e1) {
				LOGGER.error("SQLException:\n", e1);
			}

			PreparedStatement ps = null;
			ResultSet rs = null;

			try {
				ps = this.prepareStatement(conn, generalDropdownSearchSQL);
				rs = this.executeQuery(ps);

				while (rs.next()) {
					itemDesc.put(rs.getString("DESCITEM").trim(),
							rs.getString("LONGDESC").trim());
				}
				f4map.put(fieldInf[0], itemDesc);
				itemDesc = new HashMap();
			} catch (Exception e) {
				LOGGER.error("", e);
			} finally {
				freeDBConnectionIgnoreErr(conn, ps, rs);
			}

		}
		return f4map;
}

	/**
	 * This method returns item description [DESCPFX, DESCCOY, DESCTABL, DESCITEM, SHORTDESC, LONGDESC] from the database by company code
	 * @param fields - screen control field array  e.g. {cltsex, cltnum, dob}
	 * @param vm -  smartVarModel
	 * @param lang - Language to be passed e.g. E for English and CH for chinese
	 * @param bm - baseModel
	 * @param company
	 * @return Map
	 * @author CSC Vietnam July 15th 2013
	 */
	public Map<String, Map<String, String[]>> loadFieldsDescription(String[] fields,
			SmartVarModel vm, String lang, BaseModel bm, FixedLengthStringData company) {

		Map<String, Map<String, String[]>> result = new HashMap<String, Map<String, String[]>>();

		// Map of item code and its discription
		Map<String, String[]> itemDesc = null;

		StringBuilder sqlsb = new StringBuilder();
		sqlsb.append(	" SELECT D.DESCPFX, D.DESCCOY, D.DESCTABL, D.DESCITEM, D.SHORTDESC, D.LONGDESC "	);
		sqlsb.append(	" FROM VM1DTA.DESCPF D, VM1DTA.ITEMPF I "							);
		sqlsb.append(	" WHERE D.DESCPFX='IT' " 											);
		sqlsb.append(	" 	AND D.DESCITEM = I.ITEMITEM " 									);
		sqlsb.append(	" 	AND I.VALIDFLAG='1' " 											);
		sqlsb.append(	" 	AND D.DESCTABL = I.ITEMTABL " 									);
		sqlsb.append(	" 	AND D.DESCCOY = I.ITEMCOY "										);
		sqlsb.append(	" 	AND D.DESCCOY ='" + company.toString().trim() + "'"				);
		sqlsb.append(	"	AND \"LANGUAGE\" =  '"	+ lang	+ "'"							);
		sqlsb.append(	" 	AND \"VALIDFLAG\" =  '1'");
		sqlsb.append(	" 	AND DESCTABL IN ( #### ) ORDER BY LONGDESC ASC");

		Connection conn = null;
		String sql = null;

		for (String field : fields) {
			sql = null;
			itemDesc = new HashMap<String, String[]>();

			field = field.trim();
			String realFName = getRealFieldName(field);
			String[] fieldInf = findF4FieldTable(realFName, vm, bm);

			if (fieldInf[1] != null && fieldInf[1].trim().length() > 0){
				sql = sqlsb.toString().replace("####", "'" + fieldInf[1].trim() + "'");

			} else {
				continue;
			}

			try {
				conn = this.getAnotherDBConnection("DESCPF");
			} catch (SQLException e1) {
				LOGGER.error("SQLException:\n", e1);
			}

			PreparedStatement ps = null;
			ResultSet rs = null;

			try {
				ps = this.prepareStatement(conn, sql);
				rs = this.executeQuery(ps);

				while (rs.next()) {
					itemDesc.put(rs.getString("DESCITEM").trim(),
							new String[]{	rs.getString("DESCPFX").trim(),
											rs.getString("DESCCOY").trim(),
											rs.getString("DESCTABL").trim(),
											rs.getString("SHORTDESC").trim(),
											rs.getString("LONGDESC").trim()
										});
				}
				result.put(fieldInf[0], itemDesc);

			} catch (Exception e) {
				LOGGER.error("", e);
			} finally {
				freeDBConnectionIgnoreErr(conn, ps, rs);
			}

		}
		return result;
	}

	/**
	 * This method returns short description and item description from the database
	 * @param fields - screen control field array  e.g. {cltsex,cltnum,dob}
	 * @param vm -  smartVarModel
	 * @param lang - Language to be passed e.g. E for English and CH for chinese
	 * @param bm - baseModel
	 * @return Map
	 * @author CSCINDIA Sep 29th 2009
	 */
	public Map<String,Map<String,String>> loadF4FieldsShort(String[] fields, SmartVarModel vm, String lang,BaseModel bm){
		Map<String, Map<String,String>> f4map=new HashMap<String,Map<String,String>>();//Map of F4 Fields and its dropdown list
		Map<String, String> fieldTable=new HashMap<String,String>();//Map of field name and its window table
		Map<String, String> fieldCompany=new HashMap<String,String>();//Map of field name and its company
		Map<String, String> itemDesc=new HashMap<String,String>();//Map of item code and its discription

		StringBuilder sqlsb=new StringBuilder();
		sqlsb.append("SELECT DESCITEM, SHORTDESC, DESCTABL,DESCCOY FROM vm1dta.DESCPF WHERE DESCPFX='IT'");
		//sqlsb.append(" AND DESCCOY ='1'");
		sqlsb.append(" AND \"LANGUAGE\" =  '");
		sqlsb.append(lang);
		sqlsb.append("' AND DESCTABL IN(");

		String generalDropdownSearchSQL="";
		String tblName ="";
		Connection conn=null;

		for(String field:fields){
			field=field.trim();
			String realFName=getRealFieldName(field);
			String[] fieldInf=findF4FieldTable(realFName,vm, bm);
			tblName = "'"+fieldInf[1]+"',";
			generalDropdownSearchSQL=sqlsb.toString()+tblName.toString();
			generalDropdownSearchSQL=generalDropdownSearchSQL.substring(0,generalDropdownSearchSQL.length()-1)+") ORDER BY SHORTDESC ASC";

			try {
				conn = this.getAnotherDBConnection("DESCPF");
			} catch (SQLException e1) {
				LOGGER.error("SQLException:\n", e1);
			}

			PreparedStatement ps = null;
			ResultSet rs = null;

			try {
				ps = this.prepareStatement(conn, generalDropdownSearchSQL);
				rs = this.executeQuery(ps);

				while (rs.next()) {
					itemDesc.put(rs.getString("DESCITEM").trim(), rs.getString("SHORTDESC").trim());
				}
				f4map.put(fieldInf[0],itemDesc );
				itemDesc = new HashMap();
			}  catch (Exception e) {
			    LOGGER.error("", e);
			}finally {
				freeDBConnectionIgnoreErr(conn, ps, rs);
			}



	}
		return f4map;
}
	/**
	 * Load bank key dropdown list by search table BABRBBM
	 *
	 * @return Map<String,Map<String,String>>
	 * @author Lin for Ticket #4353 - SR289 & S2610 Receipt UIG issues
	 */
	public Map<String,Map<String,String>> loadBankKey(){
		Map<String, Map<String,String>> f4map=new HashMap<String,Map<String,String>>();
		Map<String, String> itemDesc=new HashMap<String,String>();//Map of item code and its discription
		String generalDropdownSearchSQL="select * from VM1DTA.BABRBBM order by BABRDC ASC, UNIQUE_NUMBER ASC";

		Connection conn=null;
		try {
			conn = this.getAnotherDBConnection("DESCPF");
		} catch (SQLException e1) {
            LOGGER.error("loadBankKey()", e1);
		}

		PreparedStatement ps = null;
		ResultSet rs = null;

		boolean found = false;
		try {
			ps = this.prepareStatement(conn, generalDropdownSearchSQL);
			rs = this.executeQuery(ps);

			while (rs.next()) {
				    found = true;
				    itemDesc.put(rs.getString("bankkey"), rs.getString("babrdc").substring(0, 30)+"@split@"+ rs.getString("babrdc").substring(30)+"@split@"+rs.getString("bankkey"));
			}
			Map<String,String> temp=new HashMap<String,String>();
			temp.putAll(itemDesc);
			f4map.put("bankkey",temp);

			if (found) {
				return f4map;
			}

		} catch (Exception e) {
            LOGGER.error("loadBankKey()", e);
		}
		return f4map;
	}

	/**
	 * Load F4 dropdown list by using separate Pxxxx program
	 *
	 * @param bd
	 * @param bm
	 * @param language
	 * @param isLong
	 * @return
	 *
	 * @author Wayne Yang 2010-03-24
	 */
	public Object loadF4(BaseData bd, BaseModel bm,String language, Boolean isLong){

		// First get Screen Window
		ScreenModel sm = bm.getScreenModel();
		SmartVarModel vm = (SmartVarModel)sm.getVariables();
		DDScreenWindowing ddSw = DD.getScreenWindowing(bd, sm.getScreenName());

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("CO", bm.getCompany());
		map.put("FSUCO", bm.getFsuco());
		map.put("LANG", language);
		map.put("screenName", sm.getScreenName());
		if (ddSw != null) {
			if (ddSw.program1 != null && !ddSw.program1.trim().equals("")
					&& !ddSw.program1.equals("P0127")) {
				BaseData[] arr = new BaseData[5];
				FlddTableDAM flddIO = new FlddTableDAM();
				arr[1] = getField(vm, ddSw.additionalField1, flddIO);
				arr[2] = getField(vm, ddSw.additionalField2, flddIO);
				arr[3] = getField(vm, ddSw.additionalField3, flddIO);
				arr[4] = getField(vm, ddSw.additionalField4, flddIO);
				return callF4InProgram(ddSw, arr, isLong, map);
			}
		}

		return null;
	}

	private Object callF4InProgram(DDScreenWindowing ddsw, BaseData[] arr,Boolean isLong, Map<String,Object> map) {


		String whatToCallName = ddsw.program1.toString().trim();
		Class whatToCallClass = null;

			try {
				whatToCallClass = (Class) this.getProgram(whatToCallName);
				Method m = whatToCallClass.getMethod("f4", new Class[] {
						COBOLAppVars.class, DDScreenWindowing.class, BaseData[].class, Boolean.class,Map.class});
				return m.invoke(null, this, ddsw, arr, isLong, map );
		} catch (Exception e) {
		    LOGGER.error("", e);
		}

		return null;
	}

	private BaseData getField(SmartVarModel vm, String s,FlddTableDAM flddIO){
		if (s == null){
			return null;
		}
		flddIO.setParams(SPACES);
		flddIO.setFunction("READR");
		flddIO.setFormat("FLDDREC");
		flddIO.setFieldId(s.toUpperCase());
		SmartFileCode.execute(this, flddIO);
		if (isEQ(flddIO.getStatuz(),"****")) {
			String Name = flddIO.getNameForCobol().trim().toLowerCase();
			return vm.getField(Name, BaseData.class);
		}
		return null;
	}

	/* UIG Changes Ends*/


	@Override
	public void rollback() throws ExtMsgException {
		// txConnection is null, means the transaction contorl has not been
		// started yet
		if (txConnection == null) {
			addExtMessage("CPF8350", "Commitment definition not found.");
		}
		// commit changes
		try {
			// txConnection.rollback();
			if (this.getHibernateSession() != null) {
				this.getHibernateSession().getTransaction().rollback();
				this.getHibernateSession().getTransaction().begin();
			}

			// added by David.Dong @ 2008-11-04 16:38
			COBOLAppVars appVars = (COBOLAppVars) AppVars.getInstance();
			List<FixedLengthStringData> list = appVars.alocnoMap.get(appVars.key);
			if (list != null) {
				Varcom varcom = new Varcom();
				FixedLengthStringData anumrec = new FixedLengthStringData(10).init("ANUMREC");
				txConnection.setAutoCommit(false);
				Iterator<FixedLengthStringData> it = list.iterator();
				while (it.hasNext()) {
					FixedLengthStringData s = it.next();
					AnumTableDAM anumIO = new AnumTableDAM();
					anumIO.setParams(s);
					anumIO.setFunction(varcom.writr);
					anumIO.setFormat(anumrec);
					SmartFileCode.execute(appVars, anumIO);
					it.remove();
				}
				txConnection.commit();
				txConnection.setAutoCommit(false);
			}
		} catch (SQLException e) {
			addExtMessage("CPF8359", "Rollback operation failed.");
			LOGGER.error("SQLException:\n", e);
		} catch (Exception e) {
			addDiagnostic(e.getLocalizedMessage());
			addExtMessage("CPF8359", "Rollback operation failed.");
		}
	}

    /**
     * This method returns long description and item description from the database by company code
     * @param fields - screen control field array  e.g. {cltsex,cltnum,dob}
     * @param vm -  smartVarModel
     * @param lang - Language to be passed e.g. E for English and CH for chinese
     * @param bm - baseModel
     * @param company
     * @return Map
     * @author CSC Vietnam Jan 28th 2013
     */
    public Map<String, Map<String, String>> loadF4FieldsLong(String[] fields,
            SmartVarModel vm, String lang, BaseModel bm, String company) {

        // Map of F4 Fields and its dropdown list
        Map<String, Map<String, String>> f4map = new HashMap<String, Map<String, String>>();

        // Map of item code and its discription
        Map<String, String> itemDesc = new HashMap<String, String>();

        StringBuilder sqlsb = new StringBuilder();
        sqlsb.append(   " SELECT D.DESCITEM, D.LONGDESC, D.DESCTABL, D.DESCCOY, D.DESCPFX " );
        sqlsb.append(   " FROM VM1DTA.DESCPF D, VM1DTA.ITEMPF I "                           );
        sqlsb.append(   " WHERE D.DESCPFX='IT' "                                            );
        sqlsb.append(   "   AND D.DESCITEM = I.ITEMITEM "                                   );
        sqlsb.append(   "   AND I.VALIDFLAG='1' "                                           );
        sqlsb.append(   "   AND D.DESCTABL = I.ITEMTABL "                                   );
        sqlsb.append(   "   AND D.DESCCOY = I.ITEMCOY "                                     );
        sqlsb.append(   "   AND D.DESCCOY ='" + company.toString().trim() + "'"             );
        sqlsb.append(   "   AND \"LANGUAGE\" =  '"  + lang  + "'"                           );
        sqlsb.append(   "   AND \"VALIDFLAG\" =  '1'");
        sqlsb.append(   "   AND DESCTABL IN(");

        String generalDropdownSearchSQL = "";
        String tblName = "";
        Connection conn = null;

        for (String field : fields) {
            field = field.trim();
            String realFName = getRealFieldName(field);
            String[] fieldInf = findF4FieldTable(realFName, vm, bm);
            tblName = "'" + fieldInf[1] + "',";
            generalDropdownSearchSQL = sqlsb.toString() + tblName.toString();
            generalDropdownSearchSQL = generalDropdownSearchSQL.substring(0,
                    generalDropdownSearchSQL.length() - 1)
                    + ") ORDER BY LONGDESC ASC";

            try {
                conn = this.getAnotherDBConnection("DESCPF");
            } catch (SQLException e1) {
                LOGGER.error("SQLException:\n", e1);
            }

            PreparedStatement ps = null;
            ResultSet rs = null;

            try {
                ps = this.prepareStatement(conn, generalDropdownSearchSQL);
                rs = this.executeQuery(ps);

                while (rs.next()) {
                    itemDesc.put(rs.getString("DESCITEM").trim(),
                            rs.getString("LONGDESC").trim());
                }
                f4map.put(fieldInf[0], itemDesc);
                itemDesc = new HashMap();
            } catch (Exception e) {
                LOGGER.error("", e);
            } finally {
                freeDBConnectionIgnoreErr(conn, ps, rs);
            }

        }
        return f4map;
    }
}
