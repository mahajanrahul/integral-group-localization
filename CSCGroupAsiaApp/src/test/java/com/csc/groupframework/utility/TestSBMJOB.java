package com.csc.groupframework.utility;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;

import java.util.Date;

import com.csc.groupasia.runtime.variables.GroupAsiaAppVars;
import com.csc.smart400framework.batch.cls.BatchCLFunctions;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.COBOLFramework.command.CommandExecutor;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class TestSBMJOB extends SMARTCodeModel {
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(300);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(300);

	public static void main(String[] args) {
		QPUtilities.startTest();
		System
				.setProperty("Quipoz.test.XMLPath",
						"C://Users//shendong//workspaces//groupAsia//CSCGroupAsiaApp//src//main//AppIO//QuipozCfg.xml");
		new TestSBMJOB().standaloneTest();
		QPUtilities.endTest();
	}

	public void standaloneTest() {

		AppVars.standaloneEnvironment = true;
		appVars = new GroupAsiaAppVars("test");
		appVars.getAppConfig().sqlDiagLevel = 95;
		appVars.setInstance(appVars);
		appVars.setJobinfo(new JobInfo("test","test",new Date(), "test"));
		test();
	}

	public void test() {
		
//		//call program directly
//		String program = "CRTENRLCMD";
//		if (program.endsWith("CMD")) {
//			program = program.replaceAll("CMD", "CLP");
//		}
//		callProgram(program, new FixedLengthStringData("GC"), new FixedLengthStringData("3"), new FixedLengthStringData("UENTITY123"), new FixedLengthStringData("*CURRENT"));
		
//		String CALL_STRING = "CALL PGM(CRTENRLCLP) PARM('CH' '3' 'uentity1' '')";
//		CommandExecutor.execute(CALL_STRING, CALL_STRING.length());

		wsaaQcmdexc.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
//		stringVariable1.addExpression("SBMJOB CMD(");
//		stringVariable1.addExpression("CRTENRLCLP", SPACES);
//		stringVariable1.addExpression(" PREFIX");
//		stringVariable1.addExpression("(");
//		stringVariable1.addExpression("GC");
//		stringVariable1.addExpression(")");
//		stringVariable1.addExpression(" COMPANY");
//		stringVariable1.addExpression("(");
//		stringVariable1.addExpression("3");
//		stringVariable1.addExpression(")");
//		stringVariable1.addExpression(" UENTITY");
//		stringVariable1.addExpression("(");
//		stringVariable1.addExpression("uentity1", SPACES);
//		stringVariable1.addExpression(")");
//		stringVariable1.addExpression(" USERID");
//		stringVariable1.addExpression("(");
//		stringVariable1.addExpression("userid1", SPACES);
//		stringVariable1.addExpression("))");
//		stringVariable1.addExpression(" JOB(");
//		stringVariable1.addExpression("CRTENRLCLP", SPACES);
//		stringVariable1.addExpression(")", SPACES);
//		stringVariable1.addExpression(" JOBQ(");
//		stringVariable1.addExpression("BACKGROUND", SPACES);
//		stringVariable1.addExpression(") MSGQ(*NONE)");
//		stringVariable1.setStringInto(wsaaQcmdexc);
//		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);

//		wsaaQcmdexc.set(SPACES);
//		stringVariable1 = new StringUtil();
//		stringVariable1.addExpression("SBMJOB CMD(CALL PGM(");
//		stringVariable1.addExpression("CRTENRLCLP", SPACES);
//		stringVariable1.addExpression(") PARM");
//		stringVariable1.addExpression("(");
//		stringVariable1.addExpression("'GC'");
//		stringVariable1.addExpression(" '3'");
//		stringVariable1.addExpression(" 'uentity1'");
//		stringVariable1.addExpression(" 'userid1'");
//		stringVariable1.addExpression("))");
//		stringVariable1.addExpression(" JOB(");
//		stringVariable1.addExpression("CRTENRLCLP", SPACES);
//		stringVariable1.addExpression(")", SPACES);
//		stringVariable1.addExpression(" JOBQ(");
//		stringVariable1.addExpression("BACKGROUND", SPACES);
//		stringVariable1.addExpression(") MSGQ(*NONE)");
//		stringVariable1.setStringInto(wsaaQcmdexc);
//		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
//
//
//		
		wsaaQcmdexc.set(SPACES);
		stringVariable1 = new StringUtil();
		stringVariable1.addExpression("SBMJOB CMD(GENENRLCMD");
		stringVariable1.addExpression(" USERID");
		stringVariable1.addExpression("(");
		stringVariable1.addExpression("userid1", SPACES);
		stringVariable1.addExpression("))");
		stringVariable1.addExpression(" JOB(GENENRLCMD)");
		stringVariable1.addExpression(" JOBQ(");
		stringVariable1.addExpression("BACKGROUND", SPACES);
		stringVariable1.addExpression(") MSGQ(*NONE)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);

		
		/*{CMD=CRTENRLCLP PREFIX(GC) COMPANY(3) UENTITY(UENTITY1) USERID(USERID1), 
				JOB=CRTENRLCLP, 
				JOBQ=BACKGROUND, 
				MSGQ=*NONE, 
				LOG=*JOBD, 
				CURLIB=*CURRENT, 
				HOLD=*JOBD, 
				OUTQ=*CURRENT, 
				INLLIBL=*CURRENT, 
				RTGDTA=QCMDB, 
				SYSLIBL=*CURRENT, 
				JOBD=*USRPRF, 
				LOGCLPGM=*JOBD, 
				USER=*CURRENT, 
				INQMSGRPY=*JOBD, 
				JOBPTY=*JOBD}
		*/
//		BatchCLFunctions.submitJob("CRTENRLCLP", //jobCommand
//									new Object[] {new FixedLengthStringData("GC"), new FixedLengthStringData("3"), new FixedLengthStringData("UENTITY123"), new FixedLengthStringData("*CURRENT")},  //jobParameters
//									"CRTENRLCLP", //strJobName
//									"*USRPRF",  //strJobDescription
//									"BACKGROUND", //strJobQ
//									"*JOBD",  //strJobPty
//									"QCMDB",  //rtgDta
//									"*JOBD",  //log
//									null, 	//logCLPgm
//									"*JOBD", //hold
//									"*CURRENT", //user
//									"*JOBD", 	//inqMsgRpy
//									"*NONE", 	//msgQ
//									"*CURRENT", //syslibl
//									"*CURRENT", //curlib
//									"*CURRENT", //inllibl
//									null);		//rqsDta
		
		
		
		
//		BatchCLFunctions.submitJob(tr24mrec.progname02.toString(), //jobCommand
//				new Object[] {bldenrlrec.prefix.toString(), 
//							bldenrlrec.company.toString(), 
//							bldenrlrec.uentity.toString(), 
//							bldenrlrec.userid.toString()},  //jobParameters
//				tr24mrec.progname02.toString(), //strJobName
//				"*USRPRF",  //strJobDescription
//				tr24mrec.jobq.toString(), //strJobQ
//				"*JOBD",  //strJobPty
//				"QCMDB",  //rtgDta
//				"*JOBD",  //log
//				null, 	//logCLPgm
//				"*JOBD", //hold
//				"*CURRENT", //user
//				"*JOBD", 	//inqMsgRpy
//				"*NONE", 	//msgQ
//				"*CURRENT", //syslibl
//				"*CURRENT", //curlib
//				"*CURRENT", //inllibl
//				null);		//rqsDta

	}
	

}
