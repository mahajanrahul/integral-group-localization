<?xml version="1.0" encoding="UTF-8"?>
<!--
************************************************************************************************************************* 
              Amendment History                                                                                                                      
************************************************************************************************************************* 
 Date....     VSN/MOD    Work Unit       By...                                                               	                        
************************************************************************************************************************* 
05/28/07                         XMLP06           Lewis Liu   
                    Update to avoid  blank rows in PDF tables                                                 *************************************************************************************************************************
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:template name="GSCHDPLN">
		<xsl:apply-templates select="GSCHDPLN"/>
	</xsl:template>
	<xsl:template match="GSCHDPLN">
                   <fo:block font-weight="bold" font-size="16pt" line-height="18pt" text-align="center" space-after.optimum="24pt" space-before.optimum="10pt" text-decoration="underline">
						Plan Applicable
					</fo:block>
					<fo:table width="70%" space-after.optimum="15pt">
    					<fo:table-column column-width="proportional-column-width(40)"/>
						<fo:table-column column-width="proportional-column-width(30)"/>
						<fo:table-column column-width="proportional-column-width(30)"/>
                                         <fo:table-body>
                                            <xsl:for-each select="PlanApplicable">
                                            	<xsl:if test="PlanDesc != ''  or Planno != ''  or DefPlanDesc != '' ">
                                                <fo:table-row line-height="15pt">
                                                    <fo:table-cell>
                                                        <fo:block>
                                                            <xsl:value-of select="PlanDesc"/>
                                                        </fo:block>
                                                    </fo:table-cell>
                                                    <fo:table-cell>
														<fo:block>
															<xsl:text>Plan </xsl:text>
															<xsl:value-of select="Planno"/>
														</fo:block>
                                                    </fo:table-cell>
                                                    <fo:table-cell>
														<fo:block>
																<xsl:text> - </xsl:text>
															<xsl:value-of select="DefPlanDesc"/>														</fo:block>
                                                    </fo:table-cell>
												</fo:table-row>
											</xsl:if>	
											</xsl:for-each>
										</fo:table-body>
					</fo:table>
	</xsl:template>
</xsl:stylesheet>
