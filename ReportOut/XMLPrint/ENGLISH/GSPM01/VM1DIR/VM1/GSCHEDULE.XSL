<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<!--- =============        Header       =============-->	
	<xsl:include href="GSCHD.XSL"/>
	<!-- ============= All Risks Included ============-->
	<xsl:include href="LETTER_HEADER.XSL"/>
	<xsl:include href="LETTER_FOOTER.XSL"/>
	<xsl:include href="Include.XSL"/> 
	<!-- ============  Trailer =====================-->
	<xsl:include href="GSCHDTRL.XSL"/>

	<xsl:template match="/">
	 	<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="GSCHD">
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="Times Roman" font-size="12pt">
			<!--==========   Defining the Page Layout ==================-->
			<fo:layout-master-set>
				<fo:simple-page-master master-name="main" margin-top="1cm" margin-bottom="1cm" margin-left="2cm" margin-right="2cm" page-height="8.5in" page-width="14in">
					<fo:region-body margin-bottom="3cm" margin-top="2cm"/>
					<fo:region-after extent="2.5cm"/>
					<fo:region-before extent="2cm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<!--================= Writing the Page now ======================-->
			<fo:page-sequence master-reference="main">
				<!--=================  Writing Header-details  =======================-->
				<fo:static-content flow-name="xsl-region-before">
					<xsl:call-template name="HEADER"/>
				</fo:static-content>
				<!--=================  Writing Footer-details  =======================-->
				<fo:static-content flow-name="xsl-region-after" display-align="after">
					<fo:block border-bottom-style="dashed" border-bottom-width="thin" border-top-style="dashed" border-top-width="thin">
                        <fo:table width="100%" space-before.optimum="1pt" space-after.optimum="2pt">
                            <fo:table-column column-width="proportional-column-width(40)" />
                            <fo:table-column column-width="proportional-column-width(40)" />
                            <fo:table-column column-width="proportional-column-width(20)" />
                            <fo:table-body>
                               <fo:table-row border-color="white" height="23pt" width="40%" padding-start="3pt" padding-end="3pt" padding-before="3pt" padding-after="3pt" display-align="center" text-align="start">
                                    <fo:table-cell>
                                        <fo:block >Intermediary Number  :<xsl:text>     </xsl:text><xsl:value-of select=" Agntnum"/>(<xsl:value-of select=" MRKSRV"/>)</fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell>
                                        <fo:block  >Policy Number :<xsl:text>     </xsl:text><xsl:value-of select=" CNTTYP"/>/<xsl:value-of select=" CHDRNUM"/>/<xsl:value-of select=" BRN"/></fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell>
                                       <fo:block  >Page:<fo:page-number/></fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                            </fo:table-body>
                        </fo:table>
                    </fo:block>
				</fo:static-content>		
				<!--============ Writing the body section ===================-->
				<fo:flow flow-name="xsl-region-body">
					<xsl:call-template name="GSCHD"/>
					<xsl:if test="GSCHDPLN">
						<xsl:call-template name="GSCHDPLN"/>
					</xsl:if>
					<fo:block font-weight="bold" font-size="16pt" line-height="18pt" text-align="center" space-after.optimum="24pt" space-before.optimum="24pt" text-decoration="underline">
						Schedule of Annual Premium
				</fo:block>	
					<xsl:if test="GSCHDPMA">
						<xsl:call-template name="GSCHDPMA"/>
					</xsl:if>
					<xsl:if test="GSCHDPMB">
						<xsl:call-template name="GSCHDPMB"/>
					</xsl:if>
					<xsl:if test="GSCHDPMC">
						<xsl:call-template name="GSCHDPMC"/>
					</xsl:if>
					<xsl:if test="GSCHDBNF">
						<xsl:call-template name="GSCHDBNF"/>
					</xsl:if>
					<xsl:call-template name="GSCHDTRL"/>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
</xsl:stylesheet>
