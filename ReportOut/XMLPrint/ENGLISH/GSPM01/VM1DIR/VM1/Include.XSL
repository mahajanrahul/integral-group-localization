<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:include href="GSCHDPLN.xsl"/>
	<xsl:include href="GSCHDPMA.xsl"/>
	<xsl:include href="GSCHDPMB.xsl"/>
	<xsl:include href="GSCHDPMC.xsl"/>
	<xsl:include href="GSCHDBNF.xsl"/>
</xsl:stylesheet>
