<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
<xsl:template name="GSCHDTRL">
		<xsl:apply-templates select="GSCHDTRL"/>
</xsl:template>
<xsl:template match="GSCHDTRL">
					<fo:block space-before.optimum="25pt">For and on behalf of the Company</fo:block>
					<fo:block space-before="50pt">
						---------------------------------------------------
					</fo:block>
					<fo:block>Authorised Signature</fo:block>
					<fo:block>
						Date of issue :
						<xsl:value-of select=" Dteiss"/>
						<xsl:text>      </xsl:text>
						<xsl:value-of select=" Usrprf"/>
					</fo:block>
			
</xsl:template>
</xsl:stylesheet>
