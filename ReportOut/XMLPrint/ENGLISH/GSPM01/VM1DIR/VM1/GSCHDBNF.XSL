<?xml version="1.0" encoding="UTF-8"?>
<!--
************************************************************************************************************************* 
              Amendment History                                                                                                                      
************************************************************************************************************************* 
 Date....     VSN/MOD    Work Unit       By...                                                               	                        
************************************************************************************************************************* 
05/28/07                         XMLP06           Lewis Liu   
                    Update to avoid  blank rows in PDF tables                                                 
02/09/08    01/02         V73F03         Gang Liu/ASIA/CSC (China)     
                   Change table defination so that  table-layout="fixed" ."Auto" is no longer supported by FOP 
*************************************************************************************************************************
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:template name="GSCHDBNF">
		<fo:block font-weight="bold" font-size="16pt" line-height="18pt" text-align="center" space-after.optimum="24pt" space-before.optimum="24pt" text-decoration="underline">
						Benefit Schedule
					</fo:block>
		<fo:block>
			<xsl:for-each select="GSCHDBNF">
				<xsl:if test="Planno != ''  or PlanDesc != ''   or Prodtyp != ''   ">
					<fo:table table-layout="fixed" space-after.optimum="15pt" width="70%">
						<fo:table-column column-width="proportional-column-width(20)"/>
						<fo:table-column column-width="proportional-column-width(80)"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>Plan No		: </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select=" Planno"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>Plan Desc	: </fo:block>
								</fo:table-cell>
								<fo:table-cell padding="1pt">
									<fo:block>
										<xsl:value-of select=" PlanDesc"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
							<fo:table-row>
								<fo:table-cell padding="1pt">
									<fo:block>Product Type	: </fo:block>
								</fo:table-cell>
								<fo:table-cell padding-top="1pt">
									<fo:block>
										<xsl:value-of select=" Prodtyp"/>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</xsl:if>
				<xsl:if test="BenefitInfo/Coverage_Basic or BenefitInfo/Coverage_Descr or BenefitInfo/Min_SI or BenefitInfo/Max_SI">
					<xsl:call-template name="GroupTerm"/>
				</xsl:if>
				<xsl:if test="BenefitInfo/AmtLife or BenefitInfo/AmtYear or BenefitInfo/AmtMth or BenefitInfo/AmtDay or BenefitInfo/AmtVis or BenefitInfo/AmtProc">
					<xsl:call-template name="GroupHealth"/>
				</xsl:if>
			</xsl:for-each>
		</fo:block>
	</xsl:template>
	<xsl:template name="GroupTerm">
		<fo:table table-layout="fixed" width="100%" space-after.optimum="15pt">
			<fo:table-column column-width="proportional-column-width(10)"/>
			<fo:table-column column-width="proportional-column-width(30)"/>
			<fo:table-column column-width="proportional-column-width(10)"/>
			<fo:table-column column-width="proportional-column-width(30)"/>
			<fo:table-column column-width="proportional-column-width(10)"/>
			<fo:table-column column-width="proportional-column-width(10)"/>
			<fo:table-header space-after="15pt">
				<fo:table-row>
					<fo:table-cell>
						<fo:block>
							<fo:inline text-decoration="underline">Cover</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<fo:inline text-decoration="underline">
								<xsl:value-of select=" ProdLit"/>
							</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<fo:inline text-decoration="underline">Cvrg Basis</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<fo:inline text-decoration="underline">Cvrg Basis Desc</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<fo:inline text-decoration="underline">Min Sum Insrd</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<fo:inline text-decoration="underline">Max Sum Insrd</fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<xsl:for-each select="BenefitInfo">
					<fo:table-row line-height="15pt">
						<fo:table-cell>
							<xsl:for-each select="Bencde">
								<fo:block>
									<xsl:value-of select="."/>
								</fo:block>
							</xsl:for-each>
						</fo:table-cell>
						<fo:table-cell>
							<xsl:for-each select="Benf_Desc">
								<fo:block>
									<!--<xsl:if test="if (  starts-with(  Benf_Desc  , &quot;?&quot;) ) then Plan Level else  Benf_Desc" />-->
									<xsl:if test=".= '??????????????????????????????' ">
										<xsl:text>Plan Level</xsl:text>
									</xsl:if>
									<xsl:if test=". != '??????????????????????????????' ">
										<xsl:value-of select="."/>
									</xsl:if>
								</fo:block>
							</xsl:for-each>
						</fo:table-cell>
						<fo:table-cell>
							<xsl:for-each select="Coverage_Basic">
								<fo:block>
									<xsl:value-of select="."/>
								</fo:block>
							</xsl:for-each>
						</fo:table-cell>
						<fo:table-cell>
							<xsl:for-each select="Coverage_Desc">
								<fo:block>
									<xsl:value-of select="."/>
								</fo:block>
							</xsl:for-each>
						</fo:table-cell>
						<fo:table-cell>
							<xsl:for-each select="Min_SI">
								<fo:block>
									<xsl:value-of select="."/>
								</fo:block>
							</xsl:for-each>
						</fo:table-cell>
						<fo:table-cell>
							<xsl:for-each select="Max_SI">
								<fo:block>
									<xsl:value-of select="."/>
								</fo:block>
							</xsl:for-each>
						</fo:table-cell>
					</fo:table-row>
				</xsl:for-each>
			</fo:table-body>
		</fo:table>
	</xsl:template>
	<xsl:template name="GroupHealth">
		<fo:table table-layout="fixed" width="100%" space-after.optimum="15pt">
			<fo:table-column column-width="proportional-column-width(10)"/>
			<fo:table-column column-width="proportional-column-width(30)"/>
			<fo:table-column column-width="proportional-column-width(10)"/>
			<fo:table-column column-width="proportional-column-width(10)"/>
			<fo:table-column column-width="proportional-column-width(10)"/>
			<fo:table-column column-width="proportional-column-width(10)"/>
			<fo:table-column column-width="proportional-column-width(10)"/>
			<fo:table-column column-width="proportional-column-width(10)"/>
			<fo:table-header space-after="15pt">
				<fo:table-row>
					<fo:table-cell>
						<fo:block>
							<fo:inline text-decoration="underline">Cover</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<fo:inline text-decoration="underline">
								<xsl:value-of select=" ProdLit"/>
							</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<fo:inline text-decoration="underline">Amt/Life</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<fo:inline text-decoration="underline">Amt/Year</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<fo:inline text-decoration="underline">Amt/Mnth</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<fo:inline text-decoration="underline">Amt/Day</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<fo:inline text-decoration="underline">Amt/Visit</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<fo:inline text-decoration="underline">Amt/Procedure</fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<xsl:for-each select="BenefitInfo">
					<fo:table-row line-height="15pt">
						<fo:table-cell>
							<xsl:for-each select="Bencde">
								<fo:block>
									<xsl:value-of select="."/>
								</fo:block>
							</xsl:for-each>
						</fo:table-cell>
						<fo:table-cell>
							<xsl:for-each select="Benf_Desc">
								<fo:block>
									<!--<xsl:if test="if (  starts-with(  Benf_Desc  , &quot;?&quot;) ) then Plan Level else  Benf_Desc" />-->
									<xsl:if test=". = '??????????????????????????????' ">
										<xsl:text>Plan Level</xsl:text>
									</xsl:if>
									<xsl:if test=". != '??????????????????????????????' ">
										<xsl:value-of select=". "/>
									</xsl:if>
								</fo:block>
							</xsl:for-each>
						</fo:table-cell>
						<fo:table-cell>
							<xsl:for-each select="AmtLife">
								<fo:block>
									<xsl:value-of select="."/>
								</fo:block>
							</xsl:for-each>
						</fo:table-cell>
						<fo:table-cell>
							<xsl:for-each select="AmtLife">
								<fo:block>
									<xsl:value-of select="."/>
								</fo:block>
							</xsl:for-each>
							<fo:block>
								<xsl:value-of select="AmtYear"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<xsl:for-each select="AmtYear">
								<fo:block>
									<xsl:value-of select="."/>
								</fo:block>
							</xsl:for-each>
						</fo:table-cell>
						<fo:table-cell>
							<xsl:for-each select="AmtDay">
								<fo:block>
									<xsl:value-of select="."/>
								</fo:block>
							</xsl:for-each>
						</fo:table-cell>
						<fo:table-cell>
							<xsl:for-each select="AmtVis">
								<fo:block>
									<xsl:value-of select="."/>
								</fo:block>
							</xsl:for-each>
						</fo:table-cell>
						<fo:table-cell>
							<xsl:for-each select="AmtProc">
								<fo:block>
									<xsl:value-of select="."/>
								</fo:block>
							</xsl:for-each>
						</fo:table-cell>
					</fo:table-row>
				</xsl:for-each>
			</fo:table-body>
		</fo:table>
	</xsl:template>
</xsl:stylesheet>
