<?xml version="1.0" encoding="GB2312"?>
<!--
************************************************************************************************************************* 
              Amendment History                                                                                                                      
************************************************************************************************************************* 
 Date....     VSN/MOD    Work Unit       By...                                                               	                        
*************************************************************************************************************************                                                 
02/09/08    01/02         V73F03         Gang Liu/ASIA/CSC (China)     
                   Change table defination so that  table-layout="fixed" ."Auto" is no longer supported by FOP 
*************************************************************************************************************************
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:template name="GSCHDPMA">
		<fo:table table-layout="fixed" width="100%" space-after.optimum="15pt">
			<fo:table-column column-width="proportional-column-width(20)"/>
			<fo:table-column column-width="proportional-column-width(20)"/>
			<fo:table-column column-width="proportional-column-width(15)"/>
			<fo:table-column column-width="proportional-column-width(15)"/>
			<fo:table-column column-width="proportional-column-width(15)"/>
			<fo:table-column column-width="proportional-column-width(15)"/>
			<fo:table-header space-after="15pt">
				<fo:table-row>
					<fo:table-cell>
						<fo:block>
							<fo:inline text-decoration="underline">Cover Level</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell>
						<fo:block>
							<fo:inline text-decoration="underline">Product</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center">
						<fo:block>
							<fo:inline text-decoration="underline">Employee</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center">
						<fo:block>
							<fo:inline text-decoration="underline">Employee and Spouse</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center">
						<fo:block>
							<fo:inline text-decoration="underline">Employee and Child</fo:inline>
						</fo:block>
					</fo:table-cell>
					<fo:table-cell text-align="center">
						<fo:block>
							<fo:inline text-decoration="underline">Employee and Family</fo:inline>
						</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-header>
			<fo:table-body>
				<xsl:for-each select="GSCHDPMA">
					<fo:table-row line-height="15pt">
						<fo:table-cell>
							<fo:block>
								<xsl:value-of select="PlanNumber"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block>
								<xsl:value-of select="ProdType"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell text-align="center">
							<fo:block>
								<xsl:value-of select="PremRate1"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell text-align="center">
							<fo:block>
								<xsl:value-of select="PremRate2"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell text-align="center">
							<fo:block>
								<xsl:value-of select="PremRate3"/>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell text-align="center">
							<fo:block>
								<xsl:value-of select="PremRate4"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</xsl:for-each>
			</fo:table-body>
		</fo:table>
	</xsl:template>
</xsl:stylesheet>
