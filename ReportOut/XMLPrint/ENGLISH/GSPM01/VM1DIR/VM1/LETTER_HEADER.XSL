<?xml version="1.0" encoding="UTF-8"?>
<!--
************************************************************************************************************************* 
              Amendment History                                                                                                                      
************************************************************************************************************************* 
 Date....     VSN/MOD    Work Unit       By...                                                               	                        
************************************************************************************************************************* 
2005/10/11    01/01          XMLP02         Lily  Zhu                                                                    
                   Original Version                               
2009/02/09    01/02          V73F03         Gang Liu/ASIA/CSC (China)     
                    Change the path of file LOGO.JPG
                    Remove font Garamond as it is not supported by FOP for now.
                    Change table defination so that  table-layout="fixed" ."Auto" is no longer supported by FOP 
2009/07/23    01/02          FA5161         Gang Liu/ASIA/CSC (China)     
                    Change the path of  logo.jpg to be  level independant.
                    File logo.jpg has been moved to the corresponding folder. 
*************************************************************************************************************************
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:template name="HEADER">
		<!--<fo:table  width="80%">-->
		<fo:table width="80%" table-layout="fixed">
			<!--<fo:table-column column-width="proportional-column-width(20)"/>
			<fo:table-column column-width="proportional-column-width(80)"/>-->
			<fo:table-column column-width="proportional-column-width(30)"/>
			<fo:table-column column-width="proportional-column-width(70)"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell>
						<fo:block>
							<!--<fo:external-graphic src="../../../gspm01/casdir/BDEV/xsl/logo.jpg"/>-->
							<!--<fo:external-graphic src="logo.jpg"/>-->
							<fo:external-graphic src="D:\Reports\XMLPrint\ENGLISH\GSPM01\VM1DIR\VM1\XSL\GROUP\img\logo.jpg"/>
						</fo:block>
					</fo:table-cell>
					<!--<fo:table-cell  padding-left="2pt" font-family="Garamond">-->
					<fo:table-cell padding-left="2pt">
						<fo:block font-size="18pt" color="rgb(0,0,0)" font-weight="bold">Computer Sciences Corporation </fo:block>
						<fo:block font-size="18pt" color="rgb(0,0,0)" font-weight="200">Financial Services Group</fo:block>
					</fo:table-cell>
				</fo:table-row>
			</fo:table-body>
		</fo:table>
		<fo:block>
			<fo:leader leader-pattern="rule" rule-thickness="2pt" leader-length="100%"/>
		</fo:block>
	</xsl:template>
</xsl:stylesheet>
