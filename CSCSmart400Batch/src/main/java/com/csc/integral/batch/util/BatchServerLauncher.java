package com.csc.integral.batch.util;

import java.sql.Connection;
import java.sql.ResultSet;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.csc.groupasia.runtime.variables.GroupAsiaBatchAppVars;
import com.quipoz.framework.util.AppConfig;
import com.quipoz.framework.util.AppVars;

public class BatchServerLauncher {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(BatchServerLauncher.class);

	public static GroupAsiaBatchAppVars appVars = null;

	private static final String[] APPLICATION_CONTEXT = new String[] {
			"classpath*:mandatory/common.xml",
			"classpath*:scenarios/scheduling/quartz/quartz-common.xml",
			"classpath*:scenarios/scheduling/quartz/quartz-jobs.xml",
			"classpath*:scenarios/scheduling/quartz/quartz-listeners.xml",
			"classpath*:scenarios/scheduling/quartz/quartz-schedulers.xml",
			"classpath*:scenarios/scheduling/quartz/quartz-triggers.xml",
            "classpath*:com/csc/groupasia/context/GroupContext.xml",
			null};

	private static String getFOPPrintLng(AppVars appVars) {
		Connection con = null;
		ResultSet rs = null;
		try {
			con = appVars.getTempDBConnection("DB");
			rs = con.createStatement()
					.executeQuery(
							"select dataarea_data from printlng where dataarea_id='PRINTLNG'");
			rs.next();
			return rs.getString(1);
		} catch (Exception e) {
			throw new RuntimeException(
					"Error when querying printlng from database.", e);
		} finally {
			try {
				appVars.freeDBConnection(con);
			} catch (Exception e) {
				throw new RuntimeException(
						"Error when closing temp connection.", e);
			}
		}
	}

	public static void main(String[] args) {
		// Verifies prerequisites.
		if (ArrayUtils.getLength(args) < 1) {
			throw new IllegalArgumentException(
					"QuipozCfg.xml is expected.");
		}

		System.setProperty("Quipoz.CSCGroupAsiaWeb.XMLPath", args[0]);

		appVars = new GroupAsiaBatchAppVars("CSCGroupAsiaWeb");

		// Set Quipoz datasoure config to system propertiey for Quartz
		// configuration
		// Spring will get these setting first, if not, will get from
		// quartz.properties.
		AppConfig appConfig = appVars.getAppConfig();
		//IGROUP-2135 START
		System.setProperty("org.quartz.dataSource.myDS.driver", 
				appConfig.getDataDriverJDBC());
		//IGROUP-2135 END
		System.setProperty("org.quartz.dataSource.myDS.URL",
				appConfig.getDataSourceJDBC());
		System.setProperty("org.quartz.dataSource.myDS.user",
				appConfig.getUseridJDBC());
		System.setProperty("org.quartz.dataSource.myDS.password",
				appConfig.getPasswordJDBC());

		AppConfig.setPrintlng(getFOPPrintLng(appVars));
		if (StringUtils.isEmpty(AppConfig.getPrintlng())) {
			throw new RuntimeException("AppConfig.printlng must NOT be empty.");
		}
		APPLICATION_CONTEXT[APPLICATION_CONTEXT.length-1] = "classpath:com/csc/groupasia/context/dao/Group-DAO-"+appConfig.getDatabaseType()+".xml";
		// Initializes Spring application context.
		ApplicationContext appCtx = new ClassPathXmlApplicationContext(
				APPLICATION_CONTEXT);

		LOGGER.info("ApplicationUse11=" + appVars.getApplicationUse11()
				+ ",Printlng=" + AppConfig.getPrintlng());
	}
}
