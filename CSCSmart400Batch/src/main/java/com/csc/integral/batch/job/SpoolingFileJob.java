package com.csc.integral.batch.job;

import java.io.File;

import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.groupasia.runtime.variables.GroupAsiaBatchAppVars;
import com.csc.integral.batch.util.BatchServerLauncher;
import com.csc.smart400framework.utility.DMSUploadUtils;
import com.csc.xmltopdf.config.XMLToPDFConfig;
import com.csc.xmltopdf.pdfoutput.PDFGenerator;
import com.csc.xmltopdf.xmlconvert.XMLConverter;
import com.quipoz.framework.util.AppConfig;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.FileUtil;
import com.quipoz.COBOLFramework.common.Utils;

public class SpoolingFileJob extends IntegralJob {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpoolingFileJob.class);

	private PDFGenerator pdfGenerator;

	private XMLConverter xmlConverter;

	private String inputPath;

	private String convertedPath;

	private String templatePath;

	private String outputFile;

	private String tempfile;

	private AppConfig appConfig;

	@Override
	protected void executeImpl(JobExecutionContext ctx) throws Exception {

		GroupAsiaBatchAppVars appVars = BatchServerLauncher.appVars;
		AppVars.setInstance(appVars);

		pdfGenerator = (PDFGenerator) getApplicationContext().getBean("pdfGenerator");
		xmlConverter = (XMLConverter) getApplicationContext().getBean("xmlConverter");

		XMLToPDFConfig xmlToPDFConfig = new XMLToPDFConfig();
		inputPath = xmlToPDFConfig.getXmlSource();
		convertedPath = xmlToPDFConfig.getXmlACon();
		templatePath = xmlToPDFConfig.getPdfTemplate();
		outputFile = xmlToPDFConfig.getPdfOutput();
		tempfile = xmlToPDFConfig.getXmlBackup();

		appConfig = AppVars.getInstance().getAppConfig();
		File dataFile = new File(inputPath);
		File[] tempFile = dataFile.listFiles();
		if (tempFile != null && tempFile.length > 0) {
			readFolderByFile(tempFile);
		}
	}

	private void readFolderByFile(File[] t) {

		for (int i = 0; i < t.length; i++) {

			File[] files = null;
			File oldfile = t[i];
			File newfile = new File(convertedPath + File.separator + t[i].getName());
			File temp = new File(tempfile + File.separator + t[i].getName());

			String fileName = t[i].getName();
			String extendsName = null;

			String user = fileName.substring(0, fileName.indexOf("-"));
			String outputUserFile = null;
			outputUserFile = outputFile.trim() + File.separator + user.trim();
			try {
				if (!(new File(outputUserFile).isDirectory())) {
					new File(outputUserFile).mkdir();
				}

			} catch (SecurityException e) {
				LOGGER.warn("can not make directory", e);
			}

			int j = fileName.lastIndexOf('.');

			if ((j > -1) && (j < (fileName.length()))) {
				extendsName = fileName.substring(j, fileName.length());
				fileName = fileName.substring(0, j);
			}
			fileName = fileName.substring(fileName.indexOf("-") + 1);
			String tempName = fileName.substring(0, fileName.lastIndexOf("_"));
			tempName = tempName.substring(tempName.lastIndexOf("_") + 1);
			String templateFilePath = templatePath.trim() + File.separator + tempName.trim() + ".PDF";
			
			// Scan template file
			templateFilePath = FileUtil.getPathOfReport(templateFilePath, appConfig.getSubFolders());

			File template = new File(templateFilePath);
			if (extendsName.equals(".XML")) {
				xmlConverter.convert(oldfile.toString(), newfile.toString());
				if (newfile.exists()) {
					// Split XML here
					String outPutfilePath = null;
					boolean success = false;
					try {
						if (template.exists()) {
							if (Utils.isLargePDF(tempName.trim())) {
								int pagePerFile = Integer.parseInt(appConfig.getPagesPerFile());
								files = xmlConverter.splits(newfile, pagePerFile, "PAGEBREAK", convertedPath);
								for (File file : files) {
									fileName = file.getName();
									fileName = fileName.substring(fileName.indexOf("-") + 1);
									fileName = fileName.split("\\.")[0];
									outPutfilePath = outputUserFile.trim() + File.separator
									+ fileName.trim() + ".PDF";
									pdfGenerator
									.print(templateFilePath, file.toString(), outPutfilePath);
									// Uploads file to DMS.
									DMSUploadUtils.performFileUpload(outPutfilePath,
										"/com/csc/xmltopdf/dam/MetadataMapping.properties", file,
										appConfig, user.trim());
									
								}
							} else {
								outPutfilePath = outputUserFile.trim() + File.separator
										+ fileName.trim() + ".PDF";
							pdfGenerator.print(templateFilePath, newfile.toString(), outPutfilePath);

							// Uploads file to DMS.
							DMSUploadUtils.performFileUpload(outPutfilePath, "/com/csc/xmltopdf/dam/MetadataMapping.properties", newfile, appConfig);
							}
						} else {
							LOGGER.info("The template for  \"" + fileName + "\"  isn't exit");
							return;
						}
					} catch (Exception e) {
						LOGGER.error("Print file" + fileName + "unsuccessful!");
					} finally {
						oldfile.renameTo(temp);
						oldfile.delete();
						if ( files!= null && files.length > 1 ) {
							System.gc();
							for (File file : files) {
								success = file.delete();
								if (success == false) {
									LOGGER.info("Cannot delete file!!!!! " + file.getAbsolutePath());
								}
							}
						}
					}
				}
			}
		}
	}
}
