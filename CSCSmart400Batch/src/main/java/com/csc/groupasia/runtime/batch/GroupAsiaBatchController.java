package com.csc.groupasia.runtime.batch;

import java.util.Date;

import com.csc.batch.BatchController;

public class GroupAsiaBatchController extends BatchController {


	public GroupAsiaBatchController() {
		super();
	}

	public void submitJob(String jobCommand, Object[] aryJobParameters, String jobName, String jobDescription,
	        String jobQ, String jobPty, String rtgDta, String log, String logCLPgm, String hold, String user,
	        Date entryDate, String rqsDta) {
		GroupAsiaBatchJobManagerFactory.getInstance().getQuartzInstance().submitJob(jobCommand, aryJobParameters, jobName,
		    jobDescription, jobQ, jobPty, rtgDta, log, logCLPgm, hold, user, entryDate, rqsDta);
	}
	
	public static GroupAsiaBatchController getInstance() {
		if (instance == null) {
			instance = new GroupAsiaBatchController();
		}
		return (GroupAsiaBatchController) instance;
	}
	
}
