package com.csc.groupasia.runtime.batch;

import com.csc.batch.quartz.BatchJobManagerQuartz;
import com.csc.groupasia.runtime.variables.GroupAsiaBatchAppVars;
import com.quipoz.framework.util.AppVars;

public class GroupAsiaBatchJobManagerQuartz extends BatchJobManagerQuartz {

	protected GroupAsiaBatchAppVars getAppVars() {
		GroupAsiaBatchAppVars av = (GroupAsiaBatchAppVars) GroupAsiaBatchAppVars.getInstance();
		if (av == null) {
//			av = new GroupAsiaBatchAppVars("LifeAsiaWeb");
			av = new GroupAsiaBatchAppVars("CSCGroupAsiaWeb");
			AppVars.setInstance(av);
		}
		return (GroupAsiaBatchAppVars) av;
	}
}
