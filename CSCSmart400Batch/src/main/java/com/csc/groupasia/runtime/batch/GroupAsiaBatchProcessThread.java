package com.csc.groupasia.runtime.batch;

import java.net.Socket;


import com.csc.batch.socket.BatchProcessThread;

public class GroupAsiaBatchProcessThread extends BatchProcessThread {
	
	public GroupAsiaBatchProcessThread(Socket socket) {
		super(socket);
		batchController = GroupAsiaBatchController.getInstance();
	}
	
}
