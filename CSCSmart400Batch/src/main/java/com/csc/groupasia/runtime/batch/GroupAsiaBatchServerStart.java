package com.csc.groupasia.runtime.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.batch.quartz.BatchJobQueueManager;
import com.csc.batch.test.BatchServerStart;
import com.csc.groupasia.runtime.variables.GroupAsiaBatchAppVars;
import com.csc.smart400framework.SMARTAppVars;

public class GroupAsiaBatchServerStart extends BatchServerStart {
    private static final Logger LOGGER = LoggerFactory.getLogger(GroupAsiaBatchServerStart.class);
    
	public static void main(String[] args) {
		new GroupAsiaBatchServerStart().start(args);
	}
	
	protected SMARTAppVars getBatchAppVars() {
		return new GroupAsiaBatchAppVars("CSCGroupAsiaWeb");
	}
	
	/**
	 * This method is responsible for starting SocketServer.
	 * 
	 * @param quipozCfgPath
	 * @param SocketPort
	 */
	public void startBatchServer(int socketPort) {
		try {
			// start socket server
			new Thread(GroupAsiaBatchSocketServer.getInstance(socketPort)).start();
			BatchJobQueueManager.getInstance().start();
			
		} catch (Exception e) {
		    LOGGER.error("", e);
		}
	}
	
}
