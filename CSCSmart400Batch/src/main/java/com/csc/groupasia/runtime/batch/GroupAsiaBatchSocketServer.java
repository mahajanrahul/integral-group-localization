package com.csc.groupasia.runtime.batch;

import java.io.IOException;
import java.net.Socket;

import com.csc.batch.socket.BatchSocketServer;

public class GroupAsiaBatchSocketServer extends BatchSocketServer {

	/**
	 * Constructor.
	 * 
	 * @param iPort
	 * @throws IOException
	 */
	protected GroupAsiaBatchSocketServer(int iPort) throws IOException {
		super(iPort);
	}
	
	/**
	 * This method is responsible for start the Socket Server.
	 * 
	 * @throws IOException
	 */
	protected void start() throws IOException {
		Socket socket = null;
		while (true) {
			socket = accept();
			// create a new thread to deal with the request
			new Thread(new GroupAsiaBatchProcessThread(socket)).start();
		}
	}
	
	/**
	 * This method is responsible for getting Socket Server instance.
	 * 
	 * @return
	 * @throws IOException
	 */
	public static GroupAsiaBatchSocketServer getInstance(int port) throws IOException {
		if (instance == null) {
			instance = new GroupAsiaBatchSocketServer(port);
		}
		return (GroupAsiaBatchSocketServer) instance;
	}

}
