package com.csc.groupasia.runtime.batch;

import com.csc.batch.quartz.BatchJobManagerFactory;

public class GroupAsiaBatchJobManagerFactory extends BatchJobManagerFactory {

	/**
	 * Returns a handle to the JobManagerQuartz instance
	 * 
	 * @param clazz the name of the job class to run in Quartz
	 */
	public GroupAsiaBatchJobManagerQuartz getQuartzInstance() {
		if (jobManager == null) {
			jobManager = new GroupAsiaBatchJobManagerQuartz();
		}
		return (GroupAsiaBatchJobManagerQuartz) jobManager;
	}
	
	/**
	 * Returns a handle to the JobManagerFactory instance
	 * 
	 * @param clazz the job class to run in Quartz
	 */
	public static GroupAsiaBatchJobManagerFactory getInstance() {
		if (instance == null) {
			instance = new GroupAsiaBatchJobManagerFactory();
		}
		return (GroupAsiaBatchJobManagerFactory) instance;
	}
	
}
