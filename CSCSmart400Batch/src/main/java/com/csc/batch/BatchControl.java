package com.csc.batch;

import java.util.Date;

/**
 * @author dianel.peng
 */
public interface BatchControl {

	/**
	 * @param jobCommand
	 * @param jobParameters
	 * @param strJobName
	 * @param strJobDescription
	 * @param strJobQ
	 * @param strJobPty
	 * @param rtgDta
	 * @param log
	 * @param logCLPgm
	 * @param hold
	 * @param user
	 */
	void submitJob(String jobCommand, Object[] jobParameters, String strJobName, String strJobDescription,
	        String strJobQ, String strJobPty, String rtgDta, String log, String logCLPgm, String hold, String user,
	        Date entryDate, String rqsDta);

	/**
	 * The end job method ends the specified job. The job may be on a job queue, currently running or it may have
	 * already completed running.
	 * 
	 * @param strJobName
	 * @param strJobNumber
	 * @param strJobQueue
	 */
	void endJob(String strJobName, String strJobNumber, String strJobQueue);

	/**
	 * The hold job method holds the specified job. The job may be on a job queue, currently running or it may have
	 * already completed running.
	 * 
	 * @param strJobName
	 * @param strJobName
	 * @param strJobQueue
	 */
	void holdJob(String strJobName, String strJobNumber, String strJobQueue);

	/**
	 * The restart job method restarts the specified job. The job must be on a job queue, currently holded.
	 * 
	 * @param strJobName
	 * @param strJobName
	 * @param strJobQueue
	 */
	void restartJob(String strJobName, String strJobNumber, String strJobQueue);

	/**
	 * The delayJob method delays the specified job. The job must be on a job queue, currently running.
	 * 
	 * @param strJobName
	 * @param strJobNumber
	 * @param strJobQueue
	 * @param date
	 */
	void delayJob(String strJobName, String strJobNumber, String strJobQueue, Date date);

	/**
	 * @param jobQueueName the name of the job queue.
	 */
	void holdJobQueue(String jobQueueName);

	/**
	 * @param jobqueueName the name of the job queue.
	 */
	void releaseJobQueue(String jobqueueName);

	/**
	 * @param jobName
	 * @param jobNumber
	 * @param jobQueue
	 * @param valuePairs
	 */
	void changeJobInfo(String jobName, String jobNumber, String jobQueue, String[] valuePairs);

	/**
	 * @param attrName
	 * @param value
	 */
	void changeJobInfo(String jobName, String jobNumber, String jobQueue, String attr, String value);

}
