/**
 * 
 */
package com.csc.batch.variables;

import com.csc.smart.batchprograms.SMARTAppLocatorSmartBatchProg;
import com.csc.smart.cls.SMARTAppLocatorSmartClsCode;
import com.csc.smart.procedures.SMARTAppLocatorSmartProceduresCode;
import com.csc.smart.programs.SMARTAppLocatorSmartProgCode;
import com.csc.smart400framework.batch.cls.SMARTAppLocatorBatchClsCode;
import com.csc.smart400framework.cls.SMARTAppLocatorSmart400ClsCode;
import com.csc.smart400framework.dummyclass.SMARTAppLocatorSmart400dummyCode;
import com.csc.smart400framework.programs.SMARTAppLocatorSmart400progCode;
import com.csc.smart400framework.templates.SMARTAppLocatorCode;

/**
 * @author Quipoz - Max Wang
 * @version 1.0 Dec 2007 Purpose: To locate the classpath where classes are stored without the need to externalise
 *          strings containing the path. This program replicates the Polisy Asia Model, in that there are no duplicates.
 *          Thus, investigation of the library list is not needed.
 */
public class BatchAppLocatorCode extends SMARTAppLocatorCode {

	static {
		add(SMARTAppLocatorBatchClsCode.class, "BatchCls");
//		// add(PolisyAppLocatorFsuAccountingBatchCode.class, "BatchFsuAccounting");
//		add(PolisyAppLocatorFsuAgentBatchCode.class, "BatchFsuAgent");
//		add(PolisyAppLocatorFsuClientsBatchCode.class, "BatchFsuClients");
//		// add(PolisyAppLocatorFsuDebtorsBatchCode.class, "BatchFsuDebtors");
//		add(PolisyAppLocatorFsuFinancialsBatchCode.class, "BatchFsuFinancials");
//		add(PolisyAppLocatorFsuGeneralBatchCode.class, "BatchFsuGeneral");
//		// add(PolisyAppLocatorFsuPrintingBatchCode.class, "BatchFsuPrinting");
//		// add(PolisyAppLocatorFsuReportingBatchCode.class, "BatchFsuReporting");
//		// add(PolisyAppLocatorPolisyAccountingBatchCode.class, "BatchPolisyAccounting");
//		// add(PolisyAppLocatorPolisyAgentBatchCode.class, "BatchPolisyAgent");
//		// add(PolisyAppLocatorPolisyGeneralBatchCode.class, "BatchPolisyGeneral");
//		// add(PolisyAppLocatorPolisyPrintingBatchCode.class, "BatchPolisyPrinting");
//		// add(PolisyAppLocatorPolisyReinsuranceBatchCode.class, "BatchPolisyReinsurance");
//		add(PolisyAppLocatorPolisyReportingBatchCode.class, "BatchPolisyReporting");
//		// add(PolisyAppLocatorPolisyUnderwritingBatchCode.class, "BatchPolisyUnderwriting");

		// add by Benny for looking the class path
		add(SMARTAppLocatorSmartClsCode.class, "SMARTAppLocatorSmartCls");
		add(SMARTAppLocatorSmartProceduresCode.class, "SMARTAppLocatorSmartProceduresCode");
		add(SMARTAppLocatorSmartProgCode.class, "SMARTAppLocatorSmartProgCode");
		add(SMARTAppLocatorSmart400ClsCode.class, "SMARTAppLocatorSmart400ClsCode");
		add(SMARTAppLocatorSmart400dummyCode.class, "SMARTAppLocatorSmart400dummyCode");
		add(SMARTAppLocatorSmart400progCode.class, "SMARTAppLocatorSmart400progCode");

		// add by Max (csc), move B0236.java B0237.java and B0321.java to a new package
		add(SMARTAppLocatorSmartBatchProg.class, "SMARTAppLocatorSmartBatchProg");

//		// add by iSoftStone for FOP print
//		add(PolisyAppLocatorPolisyPrintingFOPProc.class, "FOPPRINTING");
//		/* Moved to PolisyAsiaAppLocator as it is called by screen programs - By Quipoz-MW */
//		// add(PolisyAsiaGeneralCLs.class,"PolisyAsiaGeneralCLS");
//		// add by iSoftStone for P1AUTOALOC/F9AUTOALOC run
//		add(C2283Interface.class, "C2283");
//
//		// add by Benny for supplyment programs.
//		add(PolisyAppLocatorFsuAccountBatchProgCode.class, "PolisyAsiaAppLocatorFsuAccountBatchProgCode");
//		add(PolisyAppLocatorFsuAgentBatchCode.class, "PolisyAppLocatorFsuAgentBatchCode");
//		add(PolisyAppLocatorFsuClsCode.class, "PolisyAppLocatorFsuClsCode");
//		add(PolisyAppLocatorFsuClientsBatchCode.class, "PolisyAppLocatorFsuClientsBatchCode");
//		add(PolisyAppLocatorFSUClsCode.class, "PolisyAppLocatorFSUClsCode");
//		add(PolisyAppLocatorFsuDebtorsBatchProgCode.class, "PolisyAppLocatorFsuDebtorsBatchProgCode");
//		add(PolisyAppLocatorFsuDebtorsClsCode.class, "PolisyAppLocatorFsuDebtorsClsCode");
//		add(PolisyAppLocatorFsuFinancialsBatchCode.class, "PolisyAppLocatorFsuFinancialsBatchCode");
//		add(PolisyAppLocatorFsuFinancialsClsCode.class, "PolisyAppLocatorFsuFinancialsClsCode");
//		add(PolisyAppLocatorFsuGeneralBatchCode.class, "PolisyAppLocatorFsuGeneralBatchCode");
//		add(PolisyAppLocatorFsuGeneralClsCode.class, "PolisyAppLocatorFsuGeneralClsCode");
//		add(PolisyAppLocatorFsuPrintingBatchProgCode.class, "PolisyAppLocatorFsuPrintingBatchProgCode");
//		add(PolisyAppLocatorFsuPrintingClsCode.class, "PolisyAppLocatorFsuPrintingClsCode");
//		add(PolisyAppLocatorFsuReinsuranceCode.class, "PolisyAppLocatorFsuReinsuranceCode");
//		add(PolisyAppLocatorFsuReportBatchProgCode.class, "PolisyAppLocatorFsuReportBatchProgCode.java");
//		add(PolisyAppLocatorFsuReportClsCode.class, "PolisyAppLocatorFsuReportClsCode");
//		add(PolisyAppLocatorFsuUnderwritingCode.class, "PolisyAppLocatorFsuUnderwritingCode");
//		add(PolisyAppLocatorPolisyAccountClsCode.class, "PolisyAppLocatorPolisyAccountClsCode");
//		add(PolisyAppLocatorPolisyAgentsClsCode.class, "PolisyAppLocatorPolisyAgentsClsCode");
//		add(PolisyAppLocatorPolisyClaimsBatchProgCode.class, "PolisyAppLocatorPolisyClaimsBatchProgCode");
//		add(PolisyAppLocatorPolisyClaimsClsCode.class, "PolisyAppLocatorPolisyClaimsClsCode");
//		add(PolisyAppLocatorPolisyClientsCode.class, "PolisyAppLocatorPolisyClientsCode");
//		add(PolisyAppLocatorPolisyClsCode.class, "PolisyAppLocatorPolisyClsCode");
//		add(PolisyAppLocatorPolisyDebtorsClsCode.class, "PolisyAppLocatorPolisyDebtorsClsCode");
//		add(PolisyAppLocatorPolisyFinancialsClsCode.class, "PolisyAppLocatorPolisyFinancialsClsCode");
//		add(PolisyAppLocatorPolisyGeneralBatchProgCode.class, "PolisyAppLocatorPolisyGeneralBatchProgCode");
//		add(PolisyAppLocatorPolisyPrintBatchProgCode.class, "PolisyAppLocatorPolisyPrintBatchProgCode");
//		add(PolisyAppLocatorPolisyPrintClsCode.class, "PolisyAppLocatorPolisyPrintClsCode");
//		add(PolisyAppLocatorPolisyReinsuranceBatchProgCode.class, "PolisyAppLocatorPolisyReinsuranceBatchProgCode");
//		add(PolisyAppLocatorPolisyReinsuranceClsCode.class, "PolisyAppLocatorPolisyReinsuranceClsCode");
//		add(PolisyAppLocatorPolisyReportingBatchCode.class, "PolisyAppLocatorPolisyReportingBatchCode");
//		add(PolisyAppLocatorPolisyReportingClsCode.class, "PolisyAppLocatorPolisyReportingClsCode");
//		add(PolisyAppLocatorPolisyUnderwritingBatchProgCode.class, "PolisyAppLocatorPolisyUnderwritingBatchProgCode");
//		add(PolisyAppLocatorPolisyUnderwritingClsCode.class, "PolisyAppLocatorPolisyUnderwritingClsCode");

		// add boh and session package on 25 July, 2008
		// add(BOAppLocatorCode.class,"BOAppLocatorCode");
	};

	/**
	 * Find an executable program in one of the available application paths based on the Screen Name.
	 * 
	 * @param code Unqualified program name
	 * @return Fully qualified path to program
	 */
	public static String findPath(String code) {
		return findPath(code);
	}

	public static Class find(String code) {
		return find(code);
	}

}
