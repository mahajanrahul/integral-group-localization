package com.csc.batch.socket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is responsible for accept the Socket Client request and start a new thread to deal with the client
 * request. FileName: BatchSocketServer.java Create time: 23/11/2007 Change history: Created by iSoftStone at 23/11/2007
 * Copyright (2007) CSC Asia, all rights reserved
 * 
 * @author iSoftStone Batch Process Group
 * @version 1.0
 */
public class BatchSocketServer extends ServerSocket implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(BatchSocketServer.class);
    
	/** Store SocketServer reference. */
	protected static BatchSocketServer instance = null;

	/**
	 * Constructor.
	 * 
	 * @param iPort
	 * @throws IOException
	 */
	protected BatchSocketServer(int iPort) throws IOException {
		super(iPort);
	}

	/**
	 * This method is responsible for getting Socket Server instance.
	 * 
	 * @return
	 * @throws IOException
	 */
	public static BatchSocketServer getInstance(int port) throws IOException {
		if (instance == null) {
			instance = new BatchSocketServer(port);
		}
		return instance;
	}

	/**
	 * This method is responsible for start the Socket Server.
	 * 
	 * @throws IOException
	 */
	protected void start() throws IOException {
		Socket socket = null;
		while (true) {
			socket = accept();
			// create a new thread to deal with the request
			new Thread(new BatchProcessThread(socket)).start();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		try {
			start();
		} catch (IOException e) {
		    LOGGER.error("", e);
		}
	}

}
