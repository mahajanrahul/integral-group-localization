package com.csc.batch.socket;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.batch.BatchController;
import com.csc.groupasia.runtime.batch.GroupAsiaBatchController;

/**
 * This class is responsible for generate a new thread to deal with client request. FileName: BatchProcessThread.java
 * Create time: 23/11/2007 Change history: Created by iSoftStone at 23/11/2007 Copyright (2007) CSC Asia, all rights
 * reserved
 * 
 * @author iSoftStone Batch Process Group
 * @version 1.0
 */
public class BatchProcessThread implements Runnable {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BatchProcessThread.class);

	/** Store Socket referecne. */
	private Socket socket;
	
	private BatchTransferData batchTransferData = null;

	/** Store BatchController referecne. */
	protected BatchController batchController = GroupAsiaBatchController.getInstance();

	public BatchProcessThread(Socket socket) {
	    LOGGER.debug("Start a new thread to deal with the Client request");
		this.socket = socket;
	}
	
	public BatchProcessThread(BatchTransferData batchData) {
		LOGGER.debug("BatchProcessThread(BatchTransferData) - Start a new thread to deal with the database request");
		this.batchTransferData = batchData;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		try {
			// get Socket input stream
			ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
			// read object from client
			BatchTransferData batchData = (BatchTransferData) ois.readObject();
			if (batchData != null) {
				// call batch control
				processBatchControl(batchData);
			}
		} catch (IOException e) {
		    LOGGER.error("", e);
			throw new RuntimeException(e);
		} catch (ClassNotFoundException e) {
		    LOGGER.error("", e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * This method is responsible for calling Batch API.
	 * 
	 * @param batchData
	 */
	private void processBatchControl(BatchTransferData batchData) {
		int batchAction = batchData.getBatchControlType();
		LOGGER.debug("batchAction=" + batchAction);
		switch (batchAction) {
		case BatchControlType.SUBMIT_JOB:
			submitJob(batchData); // Submit a job
			break;
		case BatchControlType.PAUSE_JOB:
			pauseJob(batchData); // Pause a job
			break;
		case BatchControlType.RESTART_JOB:
			restartJob(batchData); // Restart a job
			break;
		case BatchControlType.DELAY_JOB:
			delayJob(batchData); // delay a job
			break;
		case BatchControlType.STOP_JOB:
			stopJob(batchData); // stop a job
			break;
		case BatchControlType.HOLD_JOBQ:
			holdJobQ(batchData); // hold a job queue
			break;
		case BatchControlType.RELEASE_JOBQ:
			releaseJobQ(batchData); // release a job queue
			break;
		case BatchControlType.CHANGE_JOB:
			changeJob(batchData); // change a job
			break;
		default:
			throw new RuntimeException("unknown action.");
		}
	}

	/**
	 * This method is responsible for chanding a job.
	 * 
	 * @param batchData
	 */
	private void changeJob(BatchTransferData batchData) {
		String jobName = batchData.getJobName();
		String jobNumber = batchData.getJobNumber();
		String jobQ = batchData.getJobQ();
		String[] valuePairs = batchData.getValuePairsToChange();
		// String attr = batchData.getAttrToChange().toString();
		// String value = batchData.getValueToChange();
		if (valuePairs != null && valuePairs.length > 0) {
			batchController.changeJobInfo(jobName, jobNumber, jobQ, valuePairs);
		}

		// else if (attr != null && value != null) {
		// batchController
		// .changeJobInfo(jobName, jobNumber, jobQ, attr, value);
		// }
	}

	/**
	 * This method is responsible for releasing a job queue.
	 * 
	 * @param batchData
	 */
	private void releaseJobQ(BatchTransferData batchData) {
		String jobQName = batchData.getJobQ();
		if (jobQName != null) {
			batchController.releaseJobQueue(jobQName);
		}
	}

	/**
	 * This method is responsible for holding a job queue.
	 * 
	 * @param batchData
	 */
	private void holdJobQ(BatchTransferData batchData) {
		String jobQName = batchData.getJobQ();
		if (jobQName != null) {
			batchController.holdJobQueue(jobQName);
		}
	}

	/**
	 * This method is responsible for stopping a job.
	 * 
	 * @param batchData
	 */
	private void stopJob(BatchTransferData batchData) {
		String jobName = batchData.getJobName();
		String jobNumber = batchData.getJobNumber();
		String jobQ = batchData.getJobQ();
		if (jobName != null && jobNumber != null && jobQ != null) {
			batchController.endJob(jobName, jobNumber, jobQ);
		}
	}

	/**
	 * This method is responsible for delaying a job.
	 * 
	 * @param batchData
	 */
	private void delayJob(BatchTransferData batchData) {
		String jobName = batchData.getJobName();
		String jobNumber = batchData.getJobNumber();
		String jobQ = batchData.getJobQ();
		Date date = batchData.getDateToDelay();
		if (jobName != null && jobNumber != null && jobQ != null) {
			batchController.delayJob(jobName, jobNumber, jobQ, date);
		}
	}

	/**
	 * This method is responsible for restarting a job.
	 * 
	 * @param batchData
	 */
	private void restartJob(BatchTransferData batchData) {
		String jobName = batchData.getJobName();
		String jobNumber = batchData.getJobNumber();
		String jobQ = batchData.getJobQ();
		if (jobName != null && jobNumber != null && jobQ != null) {
			batchController.restartJob(jobName, jobNumber, jobQ);
		}
	}

	/**
	 * This method is responsible for pause a job.
	 * 
	 * @param batchData
	 */
	private void pauseJob(BatchTransferData batchData) {
		String jobName = batchData.getJobName();
		String jobNumber = batchData.getJobNumber();
		String jobQ = batchData.getJobQ();
		if (jobName != null && jobNumber != null && jobQ != null) {
			batchController.holdJob(jobName, jobNumber, jobQ);
		}
	}

	/**
	 * This method is responsible for submiting a job.
	 * 
	 * @param batchData
	 */
	private void submitJob(BatchTransferData batchData) {
	    LOGGER.debug("Socket Server(Submit a job)");
		String jobCmd = batchData.getJobCommand();
		Object[] jobParms = batchData.getJobParameters();
		String jobName = batchData.getJobName();
		String jobDesc = batchData.getJobDescription();
		String jobQ = batchData.getJobQ();
		String jobPty = batchData.getJobPty();
		String rtgDta = batchData.getRtgDta();
		String log = batchData.getLog();
		String logCLPgm = batchData.getLogCLPgm();
		String hold = batchData.getHold();
		String user = batchData.getUser();
		Date date = new Date();
		String rqsDta = batchData.getRqsDta();
		batchController.submitJob(jobCmd, jobParms, jobName, jobDesc, jobQ, jobPty, rtgDta, log, logCLPgm, hold, user,
		    date, rqsDta);
	}

}
