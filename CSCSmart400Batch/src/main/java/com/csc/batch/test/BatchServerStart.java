package com.csc.batch.test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.batch.api.BatchDataAccess;
import com.csc.batch.quartz.BatchJobQueueManager;
import com.csc.batch.socket.BatchProcessThread;
import com.csc.batch.socket.BatchSocketServer;
import com.csc.batch.socket.BatchTransferData;
import com.csc.groupasia.runtime.variables.GroupAsiaBatchAppVars;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.xmltopdf.dam.FileSearch;
import com.quipoz.framework.util.AppConfig;
import com.quipoz.framework.util.AppVars;

/**
 * This class is responsible for accept the Socket Client request and start a
 * new thread to deal with the client request. FileName: BatchServerStart.java
 * Create time: 23/1/2008 Change history: Created by iSoftStone at 23/1/2008
 * Copyright (2007) CSC Asia, all rights reserved
 * 
 * @author iSoftStone Batch Process Group
 * @version 1.0
 */
public class BatchServerStart {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(BatchServerStart.class);

	/**
	 * This method is responsible for starting SocketServer.
	 * 
	 * @param quipozCfgPath
	 * @param SocketPort
	 */
	public void startBatchServer(int socketPort) {
		try {
			// start socket server
			new Thread(BatchSocketServer.getInstance(socketPort)).start();
			BatchJobQueueManager.getInstance().start();

			ArrayList<BatchTransferData> toRunList = BatchDataAccess.fetchBatchData(AppVars.getInstance(), null);
			for (int i = 0; i < toRunList.size(); i++) {
				BatchTransferData batchData = toRunList.get(i);
				// create a new thread to deal with the request
				new Thread(new BatchProcessThread(batchData)).start();
			}

		} catch (Exception e) {
		    LOGGER.error("", e);
		}
	}

	/**
	 * add on 27th Mar,2008 This method is responsible for get fop print
	 * language from printlng table and set the print language to AppConfig.
	 * 
	 * @param appVars
	 * @author benny.yang
	 * @return
	 */
	protected String getFOPPrintLng(AppVars appVars) {
		Connection con = null;
		ResultSet rs = null;
		Statement st = null;
		try {
			con = appVars.getTempDBConnection("DB");
			st = con.createStatement();
			rs = st.executeQuery("select dataarea_data from printlng where dataarea_id='PRINTLNG'");
			if (rs.next()) {
				return rs.getString(1);
			}

		} catch (Exception e) {
		    LOGGER.error("", e);
		} finally {
			try {
				appVars.freeDBConnectionIgnoreErr(con, st, rs);
			} catch (Exception e) {
			    LOGGER.error("", e);
			}
		}
		return "";
	}

	/**
	 * This method is responsible for start the Batch/AT/Print Server.
	 * 
	 * @param args
	 *            QuipozCfg path and log4j path args[0] QuipozCfg.xml path
	 *            args[1] Log4jConfig.xml absolute path
	 */
	public static void main(String[] args) {
		new BatchServerStart().start(args);
	}

	protected void start(String[] args) {
		// validate the params of command line
		validateCommandParams(args);

		// set the QuipozCfg.xml
		System.setProperty("Quipoz." + args[2] + ".XMLPath", args[0]);

		System.setProperty("log4j.configuration", args[1]);

		// create BatchAppVars and set instance
		SMARTAppVars appVars = new GroupAsiaBatchAppVars("CSCGroupAsiaWeb");
		try {
			// get the print language of XML Print
			AppConfig.setPrintlng(getFOPPrintLng(appVars));
			if (AppConfig.getPrintlng().equals("")) {
				throw new RuntimeException(
						"PrintLng is empty for current server.");
			}

			// start Batch server
			startBatchServer(Integer.parseInt(AppConfig.getSocketPort()));
			// QPLogger.getQPLogger(BatchServerStart.class).warn("Batch Server has started!");
			LOGGER.info("Batch Server has started!");

			// start Adobe print
			FileSearch r = new FileSearch();
			Thread t = new Thread(r);
			t.start();
			LOGGER.info("FileSearchTh has started!");
			LOGGER.info("ApplicationUse11=" + appVars.ApplicationUse11
					+ ",Printlng=" + AppConfig.getPrintlng());
		} finally {
			// Safely close txConnection & tempConnection if they haven't closed
			// yet
			appVars.finallyFreeAllAppVarsConnections();
		}
	}

	/**
	 * Override this in the application's subclas to retrieve the specific
	 * appvars.
	 * 
	 * @return
	 */
	protected SMARTAppVars getBatchAppVars() {
		throw new RuntimeException("Need to override getBatchAppVars");
	}

	/**
	 * This method is responsible for validate the param of command line.
	 * 
	 * @param args
	 *            args[0] QuipozCfg.xml path args[1] Log4jConfig.xml absolute
	 *            path
	 */
	protected void validateCommandParams(String[] args) {
		if (args == null || args.length != 3) {
			throw new RuntimeException(
					"Not enough input arguments, expect QuipozCfg path and app name (e.g. LifeAsiaBatch / PolisyAsiaBatch)");
		}
	}

}
