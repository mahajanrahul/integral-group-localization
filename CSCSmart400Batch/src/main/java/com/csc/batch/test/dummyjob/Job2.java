/**
 * 
 */
package com.csc.batch.test.dummyjob;

import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;

/**
 * @author daniel.peng
 */
public class Job2 extends COBOLConvCodeModel {

	public void mainline(Object[] args) {
		System.out.println("job2 done.");
	}

}
