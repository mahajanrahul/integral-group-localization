package com.csc.batch.quartz;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The manager of all the job queues. The BatchJobQueueManager will monitor all the job queues.
 * 
 * @author daniel.peng
 * @version 1.0
 */
public class BatchJobQueueManager {
    
	/**
	 * The singleton instance of BatchJobQueueManager.
	 */
	private static BatchJobQueueManager instance;

	/*
	 * The private method of BatchJobQueueManager.
	 */
	private BatchJobQueueManager() {

	}

	public static BatchJobQueueManager getInstance() {
		if (instance == null) {
			instance = new BatchJobQueueManager();
		}
		return instance;
	}

	/**
	 * This method should be called when the batch application starts.
	 */
	public void start() {
		BatchJobQueueFactory.getInstance().initializeJobQueues();
		new BatchJobQueueManagerThread().start();
	}

}

class BatchJobQueueManagerThread extends Thread {
    private static final Logger LOGGER = LoggerFactory.getLogger(BatchJobQueueManager.class);
    
	/**
	 * The interval mini seconds between The Manager process.
	 */
	private static final long MINI_SECONDS_TO_WAIT = 10000; // 10seconds.

	/*
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		while (true) {
			try {
				processJobQueue();
				sleep(MINI_SECONDS_TO_WAIT);
			} catch (InterruptedException e) {
			} catch (SchedulerException e) {
				throw new RuntimeException(e);
			}
		}
	}

	/**
	 * Process the job queues to work.
	 * 
	 * @throws SchedulerException
	 */
	private void processJobQueue() throws SchedulerException {
		Collection<BatchJobQueue> queues = BatchJobQueueFactory.getInstance().getJobQueueMap().values();
		BatchJobQueue batchJobQueue = null;
		for (Iterator it = queues.iterator(); it.hasNext();) {
			batchJobQueue = (BatchJobQueue) it.next();
			runNextJob(batchJobQueue);
			removePausedJob(batchJobQueue);// remove job which has been paused
			// for too long.
		}
	}

	private void removePausedJob(BatchJobQueue batchJobQueue) {
		Map map = batchJobQueue.getPausedJobs();
		JobDetail jobDetail;
		Date date;
		Date now = new Date();
		for (Iterator it = map.keySet().iterator(); it.hasNext();) {
			jobDetail = (JobDetail) it.next();
			date = (Date) map.get(jobDetail);
			if (now.getTime() - date.getTime() >= BatchJobQueue.PAUSE_LONGEST_TIME) {
				map.remove(jobDetail);
			}
		}
	}

	private void runNextJob(BatchJobQueue batchJobQueue) throws SchedulerException {
		Scheduler scheduler = null;
		JobDetail jobDetail = null;

		scheduler = batchJobQueue.getScheduler();
		if (batchJobQueue.isActive() && scheduler.getCurrentlyExecutingJobs().size() < batchJobQueue.getJobLmt()) {
			jobDetail = batchJobQueue.getNextJob();
			if (jobDetail != null) {
				recordSubmitLog(jobDetail);
				Date entryDate = (Date) jobDetail.getJobDataMap().get(BatchJobUtils.JOB_ENTRYTIME);
				if (entryDate.before(new Date())) {                 
                    Trigger trigger = TriggerBuilder.newTrigger()
                            .withIdentity(jobDetail.getKey().getName(), batchJobQueue.getBatchJobQueueName())                           
                            .build();
                    scheduler.scheduleJob(jobDetail, trigger);                  
                }
			}
		}
	}

	/**
	 * This method is responsible for recording the submit log.
	 * 
	 * @param jobDetail
	 */
	private void recordSubmitLog(JobDetail jobDetail) {
	    
	    if (LOGGER.isInfoEnabled()) {
    		// String submitTime = new SimpleDateFormat("yyyy-MM-dd hh:MM:ss").format(new Date());
    		String jobNo = ((String) jobDetail.getJobDataMap().get(BatchJobUtils.JOB_ID_SEQ)).trim();
    		String jobName = ((String) jobDetail.getJobDataMap().get(BatchJobUtils.JOB_NAME)).trim();
    		String jobQueue = ((String) jobDetail.getJobDataMap().get(BatchJobUtils.JOB_Q)).trim();
    		String jobPty = ((String) jobDetail.getJobDataMap().get(BatchJobUtils.JOB_PTY)).trim();
    		String userName = ((String) jobDetail.getJobDataMap().get(BatchJobUtils.JOB_USER)).trim();
    		StringBuilder sb = new StringBuilder("Batch Job:\"");
    		sb.append(jobName + "\" submited,User:\"");
    		sb.append(userName + "\",Job Number:\"");
    		sb.append(jobNo + "\",jobQueue:\"");
    		sb.append(jobQueue + "\",jobPty:\"");
    		sb.append(jobPty + "\".\r\n");
    		LOGGER.info(sb.toString());
	    }
	}
}
