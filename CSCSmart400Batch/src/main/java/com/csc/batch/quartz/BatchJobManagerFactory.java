package com.csc.batch.quartz;

/**
 * JobManagerFactory is used to get a handle on a JobManagerQuartz Instance Pass the job to run in Quartz when getting
 * an instance of the JobManagerQuartz usage: JobManagerFactory f = JobManagerFactory.getInstance(); JobManagerQuartz q
 * = f.getQuartzInstance(BcaGenericJob.class);
 * 
 * @author Sarah Kim
 * @version 1.0 Feb 2007
 */
public class BatchJobManagerFactory {

	protected static BatchJobManagerFactory instance;

	protected static BatchJobManagerQuartz jobManager;


	/**
	 * Returns a handle to the JobManagerQuartz instance
	 * 
	 * @param clazz the name of the job class to run in Quartz
	 */
	public BatchJobManagerQuartz getQuartzInstance() {
		if (jobManager == null) {
			jobManager = new BatchJobManagerQuartz();
		}
		return jobManager;
	}

	/**
	 * Returns a handle to the JobManagerFactory instance
	 * 
	 * @param clazz the job class to run in Quartz
	 */
	public static BatchJobManagerFactory getInstance() {
		if (instance == null) {
			instance = new BatchJobManagerFactory();
		}
		return instance;
	}
}
