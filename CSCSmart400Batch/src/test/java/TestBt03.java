import java.util.Date;

import com.csc.smart400framework.SMARTAppVars;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;

/**
 * This class is responsible for testing the Bt03 (Embedden SQL).
 * 
 * @author benny.yang
 */
public class TestBt03 extends COBOLConvCodeModel {

	public TestBt03() {
		System.setProperty("Quipoz.test.XMLPath",
		    "D:\\datadeployment\\CSCPolisyAsiaApplication\\app\\AppIO\\QuipozCfg.xml");
		SMARTAppVars appVars = new SMARTAppVars("test");

		appVars.standaloneEnvironment = true;
		appVars.getAppConfig().sqlDiagLevel = 95;

		JobInfo jobInfo = new JobInfo("12345", "MLS010", new Date(), "OKEWAH", "AT", "ATTest", null);
		appVars.setJobinfo(jobInfo);
		// AppVars.setInstance(appVars);
		appVars.set(appVars);
		this.setGlobalVars(appVars);

	}

	public static void main(String[] args) throws Exception {
		TestBt03 t = new TestBt03();
		t.testEmeddedSQL();

	}

	private void testEmeddedSQL() {
		// callProgram(Bt003.class, null);

	}
}
