import java.lang.reflect.Constructor;
import java.util.Date;

import com.csc.batch.variables.BatchAppLocatorCode;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.batch.cls.Atdriver;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * This class is responsible for testing the AT Function with Quartz.
 * 
 * @author benny.yang
 */
public class TestAT extends COBOLConvCodeModel {

	SMARTAppVars appVars;
	// SMARTAppVars appVars;
	private FixedLengthStringData wsaaFunc = new FixedLengthStringData(4);
	private FixedLengthStringData reqnum = new FixedLengthStringData(8);
	private FixedLengthStringData printq = new FixedLengthStringData(10);
	private FixedLengthStringData atjobpty = new FixedLengthStringData(1);
	private FixedLengthStringData usrprfpty = new FixedLengthStringData(1);

	public TestAT() {
		System.setProperty("Quipoz.test.XMLPath",
		    "D:\\datadeployment\\CSCPolisyAsiaApplication\\app\\AppIO\\QuipozCfg.xml");
		try {
			Class avClass = Class.forName("com.csc.lifeasia.runtime.variables.LifeAsiaBatchAppVars");
			Constructor avCon = avClass.getConstructor(String.class);
			appVars = (SMARTAppVars) avCon.newInstance("test");
		} catch (Exception e) {
			throw new RuntimeException("Unable to instantiate appvars");
		}
		// System.out.println(appVars.getAppConfig().fopPrintRoot);
		JobInfo jobInfo = new JobInfo("12345", "MLS010", new Date(), "OKEWAH", "AT", "ATTest", null);
		appVars.setUserProfile("OKEWAH", "GNG");

		// AppVars.standaloneEnvironment = true;
		// appVars.getAppConfig().sqlDiagLevel = 95;
		appVars.setJobinfo(jobInfo);
		// appVars.setInstance(appVars);
		// scrnparams = new Scrnparams();
		// sv = new S2610ScreenVars();
		// wsspcomn = new Wsspcomn();
		// wsspcomn.company.set("9");
		// wsspcomn.language.set("E");
		setGlobalVars(appVars);
		// 60,61,62,63
		wsaaFunc.set("PROC");
		reqnum.set("00000422");
		printq.set("AT");
		atjobpty.set("7");
		usrprfpty.set("GNG");
	}

	public void testAT() {
		new BatchAppLocatorCode();
		callProgram(Atdriver.class, new Object[] { wsaaFunc, reqnum, printq, atjobpty, usrprfpty });

	}

	public static void main(String[] args) throws Exception {
		new TestAT().testAT();
	}
}
