import java.lang.reflect.Constructor;

import com.csc.batch.api.BatchSocketClient;
import com.csc.batch.socket.BatchControlType;
import com.csc.batch.socket.BatchTransferData;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.batch.cls.BatchCLFunctions;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.AppVars;

/**
 * This class is responsible for testing the AT Function with Quartz.
 * 
 * @author benny.yang
 */
public class TestAT1 extends COBOLConvCodeModel {

	public FixedLengthStringData dataArea = new FixedLengthStringData(992);
	public FixedLengthStringData nonKey = new FixedLengthStringData(736).isAPartOf(dataArea, 256);
	public FixedLengthStringData ucshrecNonKeyData = new FixedLengthStringData(736).isAPartOf(nonKey, 0);
	public FixedLengthStringData ucshLceCcy = new FixedLengthStringData(3).isAPartOf(ucshrecNonKeyData, 343);
	public PackedDecimalData ucshCrate = new PackedDecimalData(18, 9).isAPartOf(ucshrecNonKeyData, 353).init(
	    1.000000000);
	public PackedDecimalData ucshCrateTarget = new PackedDecimalData(18, 9).isAPartOf(ucshrecNonKeyData, 353)
	    .init(0E-9);

	/**
	 * After completed the AT, the at statuz is '****' and statflag is 'C'. If you want to retest ,modify the two
	 * columns. SELECT * FROM DEVTEST.ATRQ ORDER BY UNIQUE_NUMBER DESC UPDATE DEVTEST.ATRQ SET atstatuz=' ',
	 * statflag='A' where unique_number=2325
	 * 
	 * @param jobNumber match atreqno of ATRQ table
	 */
	public static void beginTest(String jobNumber) {

		// Create BatchTransferData and set value
		BatchTransferData batchData = new BatchTransferData();

		batchData.setBatchControlType(BatchControlType.SUBMIT_JOB);
		batchData.setJobCommand("Atdriver");

		batchData.setRqsDta("CALL PGM(ATDRIVER) PARM('PROC' '" + jobNumber + "')");

		// Set parameters of Atdriver
		String[] jobParams = { "PROC", jobNumber };
		batchData.setJobParameters(jobParams);

		batchData.setJobName("AT" + jobNumber);
		batchData.setJobDescription("PAXUS");
		batchData.setJobQ("AT");
		batchData.setJobPty("7");
		batchData.setRtgDta("AT");
		batchData.setLog(null);
		batchData.setLogCLPgm(null);
		batchData.setHold(null);
		batchData.setUser("GNG");
		batchData.setInqMsgRpy(null);
		batchData.setMsgQ(null);
		batchData.setSyslibl(null);
		batchData.setCurlib(null);
		FixedLengthStringData flsd = new FixedLengthStringData(300);
		flsd.set(SPACE);
		batchData.setInllibl(flsd.toString());

		// Call Socket Server
		new BatchSocketClient().callSocket(batchData);

	}

	public void test() {
		ucshCrateTarget.set(ucshCrate);
	}

	public static void main(String[] args) throws Exception {
		// Set Property and instance appVars, need manually modify
		System.setProperty("Quipoz.test.XMLPath", "D:\\batchservice\\QuipozCfg_en.xml");
		SMARTAppVars appVars = null;
		try {
			Class avClass = Class.forName("com.csc.lifeasia.runtime.variables.LifeAsiaBatchAppVars");
			Constructor avCon = avClass.getConstructor(String.class);
			appVars = (SMARTAppVars) avCon.newInstance("test");
		} catch (Exception e) {
			throw new RuntimeException("Unable to instantiate appvars");
		}
		FixedLengthStringData m_DATABASE = new FixedLengthStringData(10);

		// BatchCLFunctions.retrieve("VM1EXC/DBFLIB", m_DATABASE);
		// System.out.println("DBFLIB="+m_DATABASE.toString());

		String dbLibary = AppVars.getInstance().getAppConfig().getUseridJDBC();

		BatchCLFunctions.retrieve(dbLibary + "/DBFLIB", m_DATABASE);
		System.out.println("dbLibary=" + dbLibary + "/DBFLIB,value=" + m_DATABASE);

		// new TestAT1().test();
		// atreq number
		beginTest("00000711");
		// beginTest("00000475");
	}
}
