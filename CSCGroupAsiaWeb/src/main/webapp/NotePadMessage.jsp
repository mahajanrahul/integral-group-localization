<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page import="com.csc.groupasia.runtime.variables.GroupAsiaAppVars"%>
<%@ page session="false"%>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.quipoz.framework.util.*"%>
<%@ page import="com.csc.groupframework.variables.*"%>
<%@ page import="com.properties.PropertyLoader" %> 
<html>
<head>
<title>Message Box</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script language='javaScript' src='js/xmlReader.js'></script>
</head>
<%

	HttpSession session1 = request.getSession(false);
	String user = request.getParameter("user");	
	BaseModel baseModel = (BaseModel) session1.getAttribute(BaseModel.SESSION_VARIABLE);
	if (baseModel == null) {
		return;
	}
	String lang1  = baseModel.getApplicationVariables().getUserLanguage().toString().toUpperCase(); 
	GroupAsiaAppVars av = (GroupAsiaAppVars) baseModel.getApplicationVariables();
	String imageFolder = PropertyLoader.getFolderName(av.getLocale().toString());
	
	String codeString=av.getNotepadMessage(user);	
	
%>
<body>
<form id="npform" method="post">
<table width="560" border="0" style="font:Arial, Helvetica, sans-serif; font-size:13px; font-weight:400;">
  <caption>
    Message List
  </caption>
  <tr bgcolor="#CCCCCC">
  <td width="40">Select</td>
    <td width="100">Msg Ref ID</td>
    <td width="50">Sender</td>
    <td width="50">Receiver</td>
    <td width="200">Date & Time Msg Send</td>
    <td width="100">Reminder Date</td>    
  </tr>
<%=codeString %>

</table>
</form>
<table width="560">
<tr>
<td  align="center"><a href="#" onClick="deleteNPMessage();"><img src="screenFiles/<%=imageFolder%>/delete.gif" border="0"></a></td>
<td  align="center"><a href="#" onClick="deleteANPMessage();"><img src="screenFiles/<%=imageFolder%>/deleteall.gif" border="0"></a></td>
<td  align="center"><a href="#" onClick="window.close();"><img src="screenFiles/<%=imageFolder%>/close.gif" border="0"></a></td>
</tr>
<tr>
<td colspan="3">
<%
											if (request.getAttribute("isWrong") != null) {
											String isWrong = (String) request.getAttribute("isWrong");
											String message = (String) request.getAttribute("message");
											if(isWrong.equals("0") ){
												out.println("<span style=\"color:green\">");
												out.println(message);
												out.println("</span>");
												}
											else {
												out.println("<span style=\"color:red\">");
												out.println(message);
												out.println("</span>");
											}
										}
%>
</td>
</tr>
</table>
<script type="text/javascript">
function deleteNPMessage(){
//if(confirm("You are sure to delete one notepad message?")){
confirm(callCommonConfirm('<%=lang1%>',"No00032"));
document.getElementById("npform").action="NPServlet?type=1&user=<%=user%>";
document.getElementById("npform").submit();
}
return false;
}

function deleteANPMessage(){
//if(confirm("You are sure to delete all the notepad message?")){
confirm(callCommonConfirm('<%=lang1%>',"No00033"));
document.getElementById("npform").action="NPServlet?type=2&user=<%=user%>";
document.getElementById("npform").submit();
}
return false;
}
</script>
</body>
</html>
