<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page import="com.quipoz.COBOLFramework.messaging.SysOperatorMessageFormatter"%>
<%@ page session="false" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@page import="java.util.Set"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Comparator"%>
<%@page import="com.resource.ResourceBundleHandler"%>

<%@page import="com.quipoz.framework.screendef.QPScreenField"%>
<%@page import="com.quipoz.COBOLFramework.common.Utils"%>
<%@page import="com.properties.PropertyLoader"%>
<html>
<head>
<%! 
public static final String IE8 = "IE8";
public static final String IE10 = "IE10";
public static final String IE11 = "IE11";
public static final String Chrome = "Chrome";
public static final String Firefox = "Firefox";
//public static final boolean ieEmulationOn = true;	//IGROUP-1211 make emulation to be configurable
public static String emulationVer="off";			//IGROUP-1211
%>
<%
response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<title>Polisy Asia application</title>
<meta http-equiv="pragram" content="no-cache">
<meta http-equiv="cache-control" content="no-cache, must-revalidate">
<meta http-equiv="expires" content="0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%String ctx = request.getContextPath() + "/";
	//Move these variable here to judge system language.
	BaseModel baseModel = (BaseModel) request.getSession().getAttribute(BaseModel.SESSION_VARIABLE );
    if ( baseModel == null) {
    	return;
    }
    ScreenModel fw = (ScreenModel) baseModel.getOnScreenModel();
    GroupAsiaAppVars av = (GroupAsiaAppVars)baseModel.getApplicationVariables();
	String lan = av.getUserLanguage().toString().trim().toLowerCase();
	
if ("".equals(lan)){lan = "eng";}
	String localeimageFolder = lan;
	
	// IGroup-1086 add the brower version in common jsp
	String browerVersion = IE8;
	String userAgent = request.getHeader("User-Agent");
	
	emulationVer = "off";	//IGROUP-1211
	if (userAgent.contains("Firefox")) {
		browerVersion = Firefox;
	} else if (userAgent.contains("Chrome")) {
		browerVersion = Chrome;
	} else if (userAgent.contains("MSIE")) {
		if (userAgent.contains("MSIE 8.0") || (userAgent.contains("MSIE 7.0") && userAgent.contains("Trident/4.0"))) {
			browerVersion = IE8;
		} else if (userAgent.contains("MSIE 10.0")) {
			browerVersion = IE10;
			if(AppConfig.ieEmulationEnable) emulationVer =  IE10; //IGROUP-1211
		}
	} else if (userAgent.contains("Trident/7.0")) {
			browerVersion = IE11;
		    if(AppConfig.ieEmulationEnable) emulationVer =  IE11;	//IGROUP-1211
	}	
	
	//IGROUP-1211 begin
	if(emulationVer.equals(IE11) || emulationVer.equals(IE10))
	{
		%>
		<style type="text/css">
			.bold_cell{margin-top:-1px !important;height:18px !important;}
			.input_cell{margin-top:-1px !important;}
			.iconPos{margin-top:0px !important;top:0px !important;}
		</style>
		<%			
	}
	//IGROUP-1211 end		
%>

<LINK REL="StyleSheet" HREF="<%=ctx%>theme/<%=lan%>/QAStyle.jsp" TYPE="text/css">
<LINK REL="StyleSheet" HREF="<%=ctx%>theme/<%=lan%>/QAStyle.css" TYPE="text/css">
<LINK REL="StyleSheet" HREF="<%=ctx%>theme/<%=lan%>/tabpane.css" TYPE="text/css">
<LINK REL="StyleSheet" HREF="<%=ctx%>theme/<%=lan%>/superTables.css" TYPE="text/css">
<LINK REL="StyleSheet" HREF="<%=ctx%>theme/<%=lan%>/style.css" TYPE="text/css">

<script language="javascript" src="<%=ctx%>js/menuG4f.js"></script>

<%@ page import="com.quipoz.framework.util.*" %>
<%@ page import="com.quipoz.framework.tablemodel.*" %>
<%@ page import="com.quipoz.framework.screenmodel.*" %>
<%@ page import="com.quipoz.COBOLFramework.*" %>
<%@ page import="com.quipoz.COBOLFramework.util.*" %>
<%@ page import="com.quipoz.framework.datatype.*" %>
<%@ page import="com.quipoz.COBOLFramework.TableModel.*" %>
<%@ page import="com.csc.groupasia.runtime.variables.*" %>
<%@ page import="com.csc.smart400framework.SmartVarModel" %>
<%@ page import="com.csc.smart400framework.AppVersionInfo" %>
<%@ page import="com.csc.smart400framework.SMARTHTMLFormatter" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.LinkedHashMap" %>
<%@ page import="java.lang.reflect.Field" %>
</head>
<%!
     public String formatValue(String aValue) {

     	char[] valueCharArray = new char[aValue.length()];
     	boolean nonZeroFlag = false;

     	if(!aValue.trim().equalsIgnoreCase("")) {
     			valueCharArray = aValue.toCharArray();
     			for (int i = 0; i < valueCharArray.length; i++) {
						if(valueCharArray[i] != '0')  {
							nonZeroFlag =  true;
							break;
						}
				}
     	} else {

     	return " ";

     	}

     	if(nonZeroFlag) {
     		return aValue;
     	}
     	else {
     		return " ";
     	}

     }

/**
*To handle the length of component in Polisy at runtime
*/
public int getLength(FixedLengthStringData str)
{

	if((str.length() == 0)||(str.getFormData().trim().length()==0))
	{
		return 60;
	}/*else if(((str.length()*9)>120)||(str.getFormData().trim().length()>120))
	{
		return 120;
	}*/else
	{
		if((str.length())<(str.getFormData().trim().length()))
		{
			return (str.length()*5+25); //((str.length()*9)<12?12:str.length()*9);
		}else
		{
			return (str.getFormData().trim().length()*5+25); //((str.getFormData().trim().length()*9)<12?12:str.getFormData().trim().length()*9);
		}
	}

}

public int getLength(ZonedDecimalData str)
{

	if((str.getLength() == 0)||(str.getFormData().trim().length()==0))
	{
		return 60;
	}/*else if(((str.getLength()*9)>120)||(str.getFormData().trim().length()>120))
	{
		return 120;
	}*/else
	{
		if((str.getLength())<(str.getFormData().trim().length()))
		{
			return (str.getLength()*5+25); //((str.getLength()*9)<12?12:str.getLength()*9);
		}else
		{
			return  (str.getFormData().trim().length()*5+25); //((str.getFormData().trim().length()*9)<12?12:str.getFormData().trim().length()*9);
		}
	}

}




     public  String getKeysFromValue(Map hm, String value){
     Set set= hm.keySet();
     Iterator it = set.iterator();
	    while(it.hasNext()){
	       Object o=it.next();
	        if(((String)hm.get(o)).equals(value)) {
	            return (String)o;
	        }
	    }
	    return null;
	  }

     //Amit for sorting
     class KeyValueBean implements Comparable{

     private String key;

     private String value;


	public KeyValueBean(String key, String value) {
		this.key = key;
		this.value = value;
	}


	public String getKey() {
		return key;
	}


	public void setKey(String key) {
		this.key = key;
	}


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public int compareTo(Object o) {
		return this.value.compareTo(((KeyValueBean)o).getValue());
	}


	public String toString() {

		return "Key is "+key+" value is "+value;
	}

    }

     //secoond class

     public class KeyComarator implements Comparator{

		public int compare(Object o1, Object o2) {

			return ((KeyValueBean)o1).getKey().compareTo(((KeyValueBean)o1).getKey());
		}

	}
     public String makeDropDownList(Map mp , Object val , int i) {

     		String opValue = "";
     		Map tmp= new HashMap();
			tmp = mp;
			String aValue = "";
			if(val != null) {
			if(val instanceof String){
			 aValue = ((String) val).trim();
			 }else if (val instanceof FixedLengthStringData){
			 aValue = ((FixedLengthStringData) val).getFormData().trim();
			 }
			}

			Iterator mapIterator=tmp.entrySet().iterator();
			ArrayList keyValueList= new ArrayList();
		
			while(mapIterator.hasNext()){
			    Map.Entry entry= (Map.Entry)mapIterator.next();
				KeyValueBean bean = new KeyValueBean((String)entry.getKey(),(String)entry.getValue());
				keyValueList.add(bean);
			}



			int size = keyValueList.size();

			opValue = opValue + "<option value='' title='---------Select---------' SELECTED>---------Select---------" +  "</option>";
			String mainValue ="";
			//Option 1 fr displaying code
			if(i==1) {
			//Sorting on the basis of key
			Collections.sort(keyValueList,new KeyComarator());
			for (int ii=0; ii<size; ii++) {
			KeyValueBean keyValueBean=(KeyValueBean)keyValueList.get(ii);
						if(keyValueBean.getKey().equalsIgnoreCase(aValue)) {
									opValue = opValue + "<option value=\"" + keyValueBean.getKey()+ "\" title=\""
									+ keyValueBean.getKey() + "\" SELECTED>" +keyValueBean.getKey() + "</option>";
						} else {
								opValue = opValue + "<option value=\"" +keyValueBean.getKey() + "\" title=\""
									+ keyValueBean.getKey() + "\">" +keyValueBean.getKey()+ "</option>";
						}
			}
			}
			//Option 2 for long description
			if(i==2) {
			Collections.sort(keyValueList);

			for (int ii=0; ii<size; ii++) {
			KeyValueBean keyValueBean=(KeyValueBean)keyValueList.get(ii);
						if(keyValueBean.getKey().equalsIgnoreCase(aValue)) {
									opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
									+ keyValueBean.getValue()+ "\" SELECTED>" + keyValueBean.getValue() + "</option>";
						} else {
								opValue = opValue + "<option value=\"" +  keyValueBean.getKey()  + "\" title=\""
									+ keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
						}
			 }
			}
			//Option 3 for Short description
		if(i==3) {
		Collections.sort(keyValueList);
			for (int ii=0; ii<size; ii++) {
			KeyValueBean keyValueBean=(KeyValueBean)keyValueList.get(ii);
						if(keyValueBean.getKey().equalsIgnoreCase(aValue)) {
									opValue = opValue + "<option value=\"" + keyValueBean.getKey()+ "\" title=\""
									+ keyValueBean.getValue()+ "\" SELECTED>" +keyValueBean.getValue() + "</option>";
						} else {
								opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
									+keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
						}
			}
		}
		//Option 4 for format Code--Description
		if(i==4) {
		Collections.sort(keyValueList);

			for (int ii=0; ii<size; ii++) {
			KeyValueBean keyValueBean=(KeyValueBean)keyValueList.get(ii);
						if(keyValueBean.getKey().equalsIgnoreCase(aValue)) {
									opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
									+ keyValueBean.getValue()+ "\" SELECTED>" + keyValueBean.getKey()+"--"+keyValueBean.getValue() + "</option>";
						} else {
								opValue = opValue + "<option value=\"" +  keyValueBean.getKey()  + "\" title=\""
									+ keyValueBean.getValue() + "\">" + keyValueBean.getKey()+"--"+ keyValueBean.getValue() + "</option>";
						}
			 }
		}
			return opValue;
	}
	public String makeDropDownList(String val,String longValue,String strKeyValue){
		ArrayList keyValueList=makeList(val) ;
		String opValue = "";

		int size = keyValueList.size();
		opValue = opValue + "<option value='' title='---------Select---------' SELECTED>---------Select---------" +  "</option>";
		String mainValue ="";
		//Collections.sort(keyValueList);
		for (int ii=0; ii<size; ii++) {
			KeyValueBean keyValueBean=(KeyValueBean)keyValueList.get(ii);
			if(keyValueBean.getKey().equalsIgnoreCase(strKeyValue.trim())) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey()+ "\" title=\""
					+ keyValueBean.getValue()+ "\" SELECTED>" +keyValueBean.getValue() + "</option>";

			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
					+keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
			}
		}
		return opValue;
	}
 public String makeDropDownList(Map mp , Object val , int i, ResourceBundleHandler resourceBundleHandler) {

     		String opValue = "";
     		Map tmp= new HashMap();
			tmp = mp;
			String aValue = "";
			if(val != null) {
			if(val instanceof String){
			 aValue = ((String) val).trim();
			 }else if (val instanceof FixedLengthStringData){
			 aValue = ((FixedLengthStringData) val).getFormData().trim();
			 }
			}

			Iterator mapIterator=tmp.entrySet().iterator();
			ArrayList keyValueList= new ArrayList();

			while(mapIterator.hasNext()){
			    Map.Entry entry= (Map.Entry)mapIterator.next();
				KeyValueBean bean = new KeyValueBean((String)entry.getKey(),(String)entry.getValue());
				keyValueList.add(bean);
			}



			int size = keyValueList.size();
			String strSelect=resourceBundleHandler.gettingValueFromBundle("Select");

			opValue = opValue + "<option value='' title='---------"+strSelect+"---------' SELECTED>---------"+strSelect+"---------" +  "</option>";
			String mainValue ="";
			//Option 1 fr displaying code
			if(i==1) {
			//Sorting on the basis of key
			Collections.sort(keyValueList,new KeyComarator());
			for (int ii=0; ii<size; ii++) {
			KeyValueBean keyValueBean=(KeyValueBean)keyValueList.get(ii);
						if(keyValueBean.getKey().equalsIgnoreCase(aValue)) {
									opValue = opValue + "<option value=\"" + keyValueBean.getKey()+ "\" title=\""
									+ keyValueBean.getKey() + "\" SELECTED>" +keyValueBean.getKey() + "</option>";
						} else {
								opValue = opValue + "<option value=\"" +keyValueBean.getKey() + "\" title=\""
									+ keyValueBean.getKey() + "\">" +keyValueBean.getKey()+ "</option>";
						}
			}
			}
			//Option 2 for long description
			if(i==2) {
			Collections.sort(keyValueList);

			for (int ii=0; ii<size; ii++) {
			KeyValueBean keyValueBean=(KeyValueBean)keyValueList.get(ii);
						if(keyValueBean.getKey().equalsIgnoreCase(aValue)) {
									opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
									+ keyValueBean.getValue()+ "\" SELECTED>" + keyValueBean.getValue() + "</option>";
						} else {
								opValue = opValue + "<option value=\"" +  keyValueBean.getKey()  + "\" title=\""
									+ keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
						}
			 }
			}
			//Option 3 for Short description
		if(i==3) {
		Collections.sort(keyValueList);
			for (int ii=0; ii<size; ii++) {
			KeyValueBean keyValueBean=(KeyValueBean)keyValueList.get(ii);
						if(keyValueBean.getKey().equalsIgnoreCase(aValue)) {
									opValue = opValue + "<option value=\"" + keyValueBean.getKey()+ "\" title=\""
									+ keyValueBean.getValue()+ "\" SELECTED>" +keyValueBean.getValue() + "</option>";
						} else {
								opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
									+keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
						}
			}
		}
		//Option 4 for format Code--Description
		if(i==4) {
		Collections.sort(keyValueList);

			for (int ii=0; ii<size; ii++) {
			KeyValueBean keyValueBean=(KeyValueBean)keyValueList.get(ii);
						if(keyValueBean.getKey().equalsIgnoreCase(aValue)) {
									opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
									+ keyValueBean.getValue()+ "\" SELECTED>" + keyValueBean.getKey()+"--"+keyValueBean.getValue() + "</option>";
						} else {
								opValue = opValue + "<option value=\"" +  keyValueBean.getKey()  + "\" title=\""
									+ keyValueBean.getValue() + "\">" + keyValueBean.getKey()+"--"+ keyValueBean.getValue() + "</option>";
						}
			 }
		}
			return opValue;
	}
	public String makeDropDownList(String val,String longValue,String strKeyValue,ResourceBundleHandler resourceBundleHandler){
		ArrayList keyValueList=makeList(val) ;
		String opValue = "";

		int size = keyValueList.size();
		String strSelect=resourceBundleHandler.gettingValueFromBundle("Select");
		opValue = opValue + "<option value='' title='---------"+strSelect+"---------' SELECTED>---------"+strSelect+"---------" +  "</option>";
		String mainValue ="";
		//Collections.sort(keyValueList);
		for (int ii=0; ii<size; ii++) {
			KeyValueBean keyValueBean=(KeyValueBean)keyValueList.get(ii);
			if(keyValueBean.getKey().equalsIgnoreCase(strKeyValue.trim())) {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey()+ "\" title=\""
					+ keyValueBean.getValue()+ "\" SELECTED>" +keyValueBean.getValue() + "</option>";

			} else {
				opValue = opValue + "<option value=\"" + keyValueBean.getKey() + "\" title=\""
					+keyValueBean.getValue() + "\">" + keyValueBean.getValue() + "</option>";
			}
		}
		return opValue;
	}
	public ArrayList makeList(String val){
		String strValues[]=val.split(",");
		ArrayList keyValueList= new ArrayList();
		String opValue = "";
		if(strValues!=null){
			for(int i=0;i<strValues.length;i++){
				String strValueString[]=strValues[i].split("-");
				if(strValueString!=null){
					KeyValueBean bean = new KeyValueBean(strValueString[1],strValueString[0]);
					keyValueList.add(bean);
				}
			}
		}
		return keyValueList;
	}
	public String createDiscription(String val,String strKeyValue){
		ArrayList keyValueList=makeList(val) ;
		int size = keyValueList.size();
		String longValue="";
		for (int ii=0; ii<size; ii++) {
			KeyValueBean keyValueBean=(KeyValueBean)keyValueList.get(ii);
			if(keyValueBean.getKey().equalsIgnoreCase(strKeyValue.trim())) {
				longValue=keyValueBean.getValue();
			}
		}
		if(longValue.equals("")){
			longValue=strKeyValue;
		}
		return longValue;
	}
	 public String makeLongDescription(Map mp , Object aValue ) {


     		Map tmp= new HashMap();
			tmp = mp;
			String longDesc ="";



				longDesc = (String)tmp.get("BB");
			return longDesc;
	}

    public String createDiscription(ArrayList keyValueList, String strKeyValue) {
        int size = keyValueList.size();
        String longValue = "";
        for (int ii = 0; ii < size; ii++) {
            KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
            if (keyValueBean.getKey().equalsIgnoreCase(strKeyValue.trim())) {
                longValue = keyValueBean.getValue();
            }
        }
        if (longValue.equals("")) {
            longValue = strKeyValue;
        }
        return longValue;
    }

    public String makeDropDownList(ArrayList keyValueList, String strKeyValue) {
        String opValue = "";
        int size = keyValueList.size();

        String mainValue = "";
        //Collections.sort(keyValueList);
        for (int ii = 0; ii < size; ii++) {
            KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
            if (keyValueBean.getKey().equalsIgnoreCase(strKeyValue.trim())) {
                opValue = opValue + "<option value=\"" + keyValueBean.getKey()
                        + "^" + keyValueBean.getValue() + "\" title=\""
                        + keyValueBean.getValue() + "\" SELECTED>"
                        + keyValueBean.getKey() + "--"
                        + keyValueBean.getValue() + "</option>";

            } else {
                opValue = opValue + "<option value=\"" + keyValueBean.getKey()
                        + "^" + keyValueBean.getValue() + "\" title=\""
                        + keyValueBean.getValue() + "\">"
                        + keyValueBean.getKey() + "--"
                        + keyValueBean.getValue() + "</option>";
            }
        }
        return opValue;
    }
    
  //Start IFSU-168
  	public String makeDropDownList(Map mp, Object val, int i, ResourceBundleHandler resourceBundleHandler, String lang) {

  		String opValue = "";
  		Map tmp = new HashMap();
  		tmp = mp;
  		String aValue = "";
  		if (val != null) {
  			if (val instanceof String) {
  				aValue = ((String) val).trim();
  			} else if (val instanceof FixedLengthStringData) {
  				aValue = ((FixedLengthStringData) val).getFormData().trim();
  			}
  		}

  		Iterator mapIterator = tmp.entrySet().iterator();
  		ArrayList keyValueList = new ArrayList();

  		while (mapIterator.hasNext()) {
  			Map.Entry entry = (Map.Entry) mapIterator.next();
  			KeyValueBean bean = new KeyValueBean((String) entry.getKey(), (String) entry.getValue());
  			keyValueList.add(bean);
  		}

  		int size = keyValueList.size();

  		String strSelect = resourceBundleHandler.gettingValueFromBundle("Select");
  		opValue = opValue + "<option value='' title='---------" + strSelect + "---------' SELECTED>---------"
  				+ strSelect + "---------" + "</option>";
  		String mainValue = "";

  		//format Description but with 3 Char Codes
  		
  		if (i == 1) {			
  			Collections.sort(keyValueList);
  			for (int ii = 0; ii < size; ii++) {
  				KeyValueBean keyValueBean = (KeyValueBean) keyValueList.get(ii);
  				String itemLang;
  				itemLang = keyValueBean.getKey().substring(0,1).toString().trim().toUpperCase();
  				
  					if (keyValueBean.getKey().equalsIgnoreCase(aValue)) {
  						if (lang.equals(itemLang))
  						opValue = opValue + "<option value=\"" + keyValueBean.getKey().substring(1) + "^" + keyValueBean.getValue()
  								+ "\" title=\"" + keyValueBean.getValue() + "\" SELECTED>" + 
  								 keyValueBean.getValue() + "</option>";
  							
  					} else {
  						if (lang.equals(itemLang))
  						opValue = opValue + "<option value=\"" + keyValueBean.getKey().substring(1) + "^" + keyValueBean.getValue()
  								+ "\" title=\"" + keyValueBean.getValue() + "\">" + 
  								 keyValueBean.getValue() + "</option>";
  					}
  				
  			}
  		}				
  		return opValue;
  	} 	
  	//End IFSU-168
%>
<%--<%!SMARTHTMLFormatter smartHF = new SMARTHTMLFormatter();//Define the smartHF to gloable %>--%>
<%SMARTHTMLFormatter smartHF = new SMARTHTMLFormatter();//Define the smartHF to local
//smartHF.setLocale(request.getLocale());// used to store locale in smartHF
smartHF.setLocale(av.getLocale());
String imageFolder= PropertyLoader.getFolderName(smartHF.getLocale().toString());//used to fetch image folder name.
smartHF.setFolderName(imageFolder);

%>
<%
	//Defined it for lay out by  Michelle
	double ajustValueForText = 0.4;
	//SMARTHTMLFormatter smartHF = (SMARTHTMLFormatter)AppVars.hf;
	GroupAsiaAppVars appVars = av;
	boolean[] savedInds = null;
	if (savedInds == null) {}
	appVars.isEOF(); /* Meaningless code to avoid a Java IDE warning message */
    av.reinitVariables();
    Map fieldItem=new HashMap();//used to store page Dropdown List
	String[][] dropdownItemsUIG = null;   //used to store page Dropdown List
	Map mappedItems = null;
	String optionValue = null;

%>
<%
 String lang = av.getInstance().getUserLanguage().toString().trim();
 QPScreenField qpsf = null;
DataModel sm = null;

String formatValue = null;
String valueThis = null;
String longValue = null;

//String tit = (av.formatTitle(fw.getPageTitle(), (SmartVarModel)fw.getVariables())).replaceAll("&nbsp;","");
//changes related to Internationalization of labels and extra info hyperLinks.

//IGROUP-2072 starts
String tit = "";
ResourceBundleHandler resourceBundleHandler = new ResourceBundleHandler(fw.getScreenName(),smartHF.getLocale());

//tit = (av.formatTitleNew(fw.getPageTitle(), (SmartVarModel)fw.getVariables())).replaceAll("&nbsp;","");
//tit=resourceBundleHandler.gettingValueFromBundleforTitle(tit);

List<String> title = fw.getPageTitleNew();
for(int i =0; i < title.size() ; i++)
{
	tit = tit + resourceBundleHandler.gettingValueFromBundleforTitle(title.get(i))+" ";
} 
//IGROUP-2072 ends

smartHF.setContinueButtonValue(resourceBundleHandler.gettingValueFromBundle("Continue"));
smartHF.setRefreshButtonValue(resourceBundleHandler.gettingValueFromBundle("Refresh"));
smartHF.setPreviousButtonValue(resourceBundleHandler.gettingValueFromBundle("Previous"));
smartHF.setExitButtonValue(resourceBundleHandler.gettingValueFromBundle("Exit"));
/**
Map<String,String> optswchItem=new HashMap<String,String> ();
optswchItem=av.getOptswchDesc(baseModel);
for(String key : optswchItem.keySet())
{
	optswchItem.put(key,resourceBundleHandler.gettingValueFromBundleForLabel(optswchItem.get(key)));
}*/
%>

<body  onload="initSub('Demo');doLoad();" onKeyDown="return checkAllKeys();" <%="onResize='doResize();'"%> onHelp="return screenHelp();" onClick='doClick(this);' onBeforeUnload='return doCheck()' unUnload='doCancel()'>

<!-- <table height="5px" width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td></td></tr></table>Add it for new style by LiuFang(2010-7-20) -->
<!--IGroup-937 yzhong4 start-->
<div style="position:absolute; top:4px;float:none;" id="mainareaDiv" class="mainarea">
<div style="position:absolute; top:2px;height:27px;left:1px;right:1px;z-index:5;" class="header">

<h1 id="screenTitleP" style="margin-left:15px;margin-top:5px;"><%=tit%></h1>
<!-- changes related to Internationalization of hame page link -->
<div class="navigationlink" style="margin-top: 5px;">
<!--IGroup-937 yzhong4 end-->
	<%-- //modified to remove chinese language code for the I18N of integral. --%>
<%--				<%if(av.getUserLanguage().toString().equalsIgnoreCase("ENG")){ %>--%>
				<a href="javascript:;" onClick="doAction('PFKEY02')"><%=resourceBundleHandler.gettingValueFromBundleForLabel("Session_Info")%>&nbsp;&nbsp;</a>|
		        <a href="javascript:;" onClick="doAction('PFKEY01')">&nbsp;&nbsp;<%=resourceBundleHandler.gettingValueFromBundleForLabel("Help")%>&nbsp;&nbsp;</a>|
		        <a href="javascript:;" onClick="doAction('PFKEY16')">&nbsp;&nbsp;<%=resourceBundleHandler.gettingValueFromBundleForLabel("Home")%>&nbsp;&nbsp;</a>|
		        <a href="javascript:;" onClick="kill('<%=imageFolder.toUpperCase()%>');return false;">&nbsp;&nbsp;<%=resourceBundleHandler.gettingValueFromBundleForLabel("Logout")%>&nbsp;&nbsp;</a>
 <%--

				<%}else if(av.getUserLanguage().toString().equalsIgnoreCase("CHI")){ %>
				<a href="javascript:;" onClick="doAction('PFKEY02')">ä¼šè¯�ä¿¡æ�¯&nbsp;&nbsp;</a>|
                <a href="javascript:;" onClick="doAction('PFKEY01')">å¸®åŠ©&nbsp;&nbsp;</a>|
                <a href="javascript:;" onClick="doAction('PFKEY16')">é¦–é¡µ&nbsp;&nbsp;</a>|
                <a href="javascript:;" onClick="kill('<%=imageFolder%>');return false;">æ³¨é”€&nbsp;&nbsp;&nbsp;</a>
				<%} %>

--%>
</div>
</div>
<!-- changes related to Internationalization of images -->
<script language='javaScript'>
var imageFolder = '<%=imageFolder%>';
var moreBtnText_ENG = '<%=resourceBundleHandler.gettingValueFromBundle("moreBtnText")%>';
var addBtnText_ENG = '<%=resourceBundleHandler.gettingValueFromBundle("addBtnText")%>';
var removeBtnText_ENG = '<%=resourceBundleHandler.gettingValueFromBundle("removeBtnText")%>';
var rowNumText_ENG = '<%=resourceBundleHandler.gettingValueFromBundle("rowNumText")%>';
</script>
<!--IGroup-937 yzhong4 start-->
<!-- Ticket IFSU-285 xmae remove the mainDiv style: width:100%; -->
<div id="mainDiv" name="mainDiv" class="mainscreen" style="float:none; position:fixed; ">
<!--IGroup-937 yzhong4 end-->
<%@ include file="commonScript1.jsp" %>
<%String screenN2 = this.getClass().getSimpleName();
  screenN2 = screenN2.substring(1);
  screenN2 = QPUtilities.removeTrailing(screenN2, "Form");
 %>

<script language='javaScript'>
$(document).ready(function() {
	//IGROUP-1211 begin
	<%if(emulationVer.equals(IE11) || emulationVer.equals(IE10)){%>
		$(".iconPos").parent().css("display","inline-block");
	<%}%>
	//IGROUP-1211 end		
	
	//set the policy header information to the newly added fields
	$("#screenTitleP").text($("#invisibletitle").val());
	//set error tab
	var i=1;
	var j=0;
	while (document.getElementById("content"+i) != null) {
		var isError = false;
		$("#content"+i).find("input,div").each(function(j) {
		    if ($(this).hasClass("red")||$(this).css("background-color")=="#ff0000" ||$(this).css("border-color")=="#b55050")
				isError = true;
		});
		if (isError){
			error_display(true,i);
			if(j==0){
				j=i;
			}
		}else{
			error_display(false,i);
		}
		i++;
	}
	if(i>0&&j>0){//When the page has tab and error fields,set the error tab as current tab.
		if($("input[name='currenttab']").length!=0){
			$("input[name='currenttab']").val((i-1)+","+j);
		}
	}
	changeScrollBar("subfo");
	//set default tab
	if($("input[name='currenttab']").length!=0){
		var curtab=$("input[name='currenttab']").val();
		var comp=curtab.indexOf(",");
		if(comp>0){
			var number=parseInt($.trim(curtab.substring(0,comp)));
			var index=parseInt($.trim(curtab.substring(comp+1)));
			if(j>0){
				change_option(number,j);
			}else{
				change_option(number,index);
			}
		}
	}
});

	function doCheck() {
		keyClosing = (event.altKey == 1 || event.ctrlKey == 1) && thisKey == 115;
		if (!submitted && (event.clientY <= 0 || keyClosing))  {
			event.returnValue = "<%=resourceBundleHandler.gettingValueFromBundle("Transaction Rollback")%>";
			doAction('PFKEY03'); //IGROUP-893  
			/*bug 630 modify in S0017Form.jsp and POLACommon1.jsp doCheck()*/
			<%-- var killsession='<%=ctx%>logout?t=' + new Date();
	        parent.location.replace(killsession);  --%>
	        
		 }
	}
	function doCancel() {
		window.showModelessDialog ("<%=cs1ctx%>/AutoKill.jsp", null, "dialogWidth:640px; dialogHeight:420px; resizable:yes;");
	}

	
function hyperLinkTo(nextField){
		nextField.value="X";
		doAction('PFKEY0');
}
function removeXfield(xfield){
	xfield.value="";
		doAction('PFKEY05');
}

//Used to handle tabs
function change_option(number,index)
{
	 var error_flag = 0;
	 var error_index = new Array(number + 1);
	 //set current tab
	 if($("input[name='currenttab']").length!=0){
		$("input[name='currenttab']").val(number+","+index);
	}

	 // Fix the different between Polisy, Life, Group (FSU Web consolidation)
	 // In Group tabcontent id is: tabcontent[i]
	 // In Polisy, Life, tabcontent id is: content[i]
	 var tabContentId = '';
	 if (document.getElementById('content1') != null) {
	     tabContentId = 'content';
	 } else {
	     tabContentId = 'tabContent';
	 }

	 for (var i = 1; i <= number; i++) {
	 	if((document.getElementById('current' + i).className != 'tabcurrenterror') && (document.getElementById('current' + i).className != 'taberror') )
	    {
	        document.getElementById('current' + i).className = 'tab';//set class for tabs by Francis
	     	document.getElementById(tabContentId + i).style.display = 'none';
	    } else {
	    	error_flag ++;
			error_index[i] = i;
	    }
	 }
	 if(error_flag == 0){
	 	 document.getElementById('current' + index).className = 'tabcurrent';//set class for tabs by Francis
	 	 document.getElementById(tabContentId + index).style.display = 'block';
	 } else{//Modified the class name by maxia (2010-7-27)
		  for (var i = 1; i <= number; i++) {
		     if(error_index[i] != undefined) {
				 if(error_index[i] == index) {
				      if(document.getElementById('current' + error_index[i]).className == "taberror") {
				      	  document.getElementById('current' + error_index[i]).className = 'tabcurrenterror';
	    			      document.getElementById(tabContentId + error_index[i]).style.display = 'block';
	    			      continue;
				      }
				      if(document.getElementById('current' + error_index[i]).className == "tabcurrenterror") {
				      	  document.getElementById(tabContentId + error_index[i]).style.display = 'block';
				      }
				 } else {
	 				  if(document.getElementById('current' + error_index[i]).className == "tabcurrenterror") {
	    			      document.getElementById('current' + error_index[i]).className = 'taberror';
	    	              document.getElementById(tabContentId + error_index[i]).style.display = 'none';
		              } else if(document.getElementById('current' + error_index[i]).className == "taberror"){
		              	  document.getElementById(tabContentId + error_index[i]).style.display = 'none';
		              } else  {
		              	  document.getElementById('current' + index).className = 'tab';
	 				      document.getElementById(tabContentId + index).style.display = 'block';
		              }

	             }

			 } else {
			     if(i == index) {
			 	     document.getElementById('current' + i).className = 'tabcurrent';
	 	 			 document.getElementById(tabContentId + i).style.display = 'block';
			 	 }
			 }

		  }
	 }

}

	function error_display(ind,index)
	{
		if(ind == true){
			if(document.getElementById('current' + index).className == 'tabcurrent') {
				document.getElementById('current' + index).className = 'tabcurrenterror';
			} else {
				document.getElementById('current' + index).className = 'taberror';
			}
		}

	}


//Ticket #IFSU-285 for F4 button in the FF&Chrome browser by xma3 	
function changeF4Image(thi){
 // var sorce=thi.childNodes[0].src;  
	var sorce;		
	for(var i = 0; i < thi.childNodes.length; i++){
		sorce=thi.childNodes[i].src; 
		 if(sorce == null) continue;
  if(sorce.indexOf("_after")==-1){
    var img1 = new Image();
             img1.src=sorce.replace(".gif","_after.gif");
             thi.childNodes[0].src=img1.src;
   }else{
   	thi.onclick=function(){return  false;};
   }
}
}

function changeMoreImage(thi){
	   // change by yy for IPNC-1915
	   //var sorce=thi.childNodes[0];
	   var sorce=$(thi).children()[0];
	   var img1 = new Image();
	   var flagForSorce = typeof(sorce);

	  if(flagForSorce=="undefined")
	  {
	   sorce=thi.src;
	  }else{
	   sorce=$(thi).children()[0].src;
	  }

	   var endObj = sorce.indexOf("_hover");


	   if(endObj!=-1){
	   sorce=sorce.substring(0,endObj)+".gif";
	   }
	   if(sorce.indexOf("_after")==-1){

	   img1.src=sorce.replace(".gif","_after.gif");
	       if(flagForSorce=="undefined"){
	        thi.src=img1.src;
	       }else{
	        thi.childNodes[0].src=img1.src;
	       }



	   }else{

	   }
	}

function changeMoreImageOut(thi)
{
	   var sorce=thi.childNodes[0];
	   var img1 = new Image();
	   var flagForSorce = typeof(sorce);

	  if(flagForSorce=="undefined")
	  {
	   sorce=thi.src;
	  }else{
	   sorce=thi.childNodes[0].src;
	  }

	   var endObj = sorce.indexOf("_hover");


	   if(endObj!=-1){
	   sorce=sorce.substring(0,endObj)+".gif";
	   }
	   if(sorce.indexOf("_after")==-1){

	   }else{
		   img1.src=sorce.replace("_after.gif",".gif");
	       if(flagForSorce=="undefined"){
	        thi.src=img1.src;
	       }else{
	        thi.childNodes[0].src=img1.src;
	       }
	   }
	}
function obj(aData,aValue,aText){
	this.Data=aData;
	this.Value=aValue;
	this.Text=aText;
  }
  function chg(parent,child){
	chgComitem(parent.options[parent.selectedIndex].value,child,Set_data);
	}
  function chgComitem(parentValue,child,objs){
	  DelAllComitem(child);
	  parentValue = parentValue.substring(0,3);
	  for(i=0;i<objs.length;i++)
	  {
		  if (objs[i].Data==parentValue)
		AddComitem(child,objs[i].Value+"^"+objs[i].Text,objs[i].Text);
	   }
   }
  function DelAllComitem(aList){
	for(i=aList.options.length-1;i>=0;i--)
	{
		aList.options[i]=null;
	}
	}
  function AddComitem(aList,aValue,aText)
  {
	var aOption=new Option(aText,aValue);
	aList.options[aList.options.length]=aOption;
	}

	//This function deal with "locate to the record at the begining of the next page".
	function changeScrollBar(thi){
		if(document.getElementById(thi) != null) {
			var obj = document.getElementById(thi);
			obj.scrollTop = obj.scrollHeight;
		}
	}
  //This function moved form POLATitle.jsp by Michelle (2010-7-20)
  function kill(language) {
		//x = confirm("Are you sure you want to interrupt and kill your session?");
		x = confirm(callCommonConfirm(language,"No00031"));
		if (x == true) {
			// fix bug147
			top.frames["heartbeat"].frames["heart"].clearTimeout(
				top.frames["heartbeat"].frames["heart"].document.timerHeartBeat);
			/*window.showModalDialog ('<%=ctx%>KillSession.jsp?t=' + new Date(), ' ', 'dialogWidth:100px; dialogHeight:100px; resizable:yes; status:yes;');*/
			var killsession='<%=ctx%>logout?t=' + new Date();
			parent.location.replace(killsession);
		}
	}


	//Added by Amit for checkbox
function handleCheckBox(name)
{
	var cbs = document.forms[0].elements[name];

	if(!cbs[0].checked){
		cbs[1].checked=true;
	}else{
		cbs[1].checked=false;
	}


}




function perFormOperation(act){
    if(selectedRow1!=null && selectedRow1!=""){
        document.getElementById(selectedRow1).value=act;
        doAction('PFKEY0');
    }else{
        var elementSelected=false;
        for (var index = 0; index < idRowArray.length; ++index) {
            var item = idRowArray[index];
            if(item!=null && item!=""){
                document.getElementById(item).value=act;
                elementSelected=true;
            }
        }
        if(elementSelected){
            doAction('PFKEY0');
        }
    }
}


var selectedRow1;
var firstTimeOver=false;
function selectedRow(idt){

	document.getElementById(idt).value='1';
	document.getElementById(idt).selected=true;

	if(firstTimeOver && document.getElementById(idt).type!='checkbox'){
		document.getElementById(selectedRow1).value=' ';
		document.getElementById(selectedRow1).selected=false;
		document.getElementById(selectedRow1).checked=false;

	}
	firstTimeOver=true;
	selectedRow1=idt;
}

function selectedRowNew(idt){
	
	if (document.getElementById(idt).checked) {
		document.getElementById(idt).value='Y';
		document.getElementById(idt).selected=true;
	} else {
		document.getElementById(idt).value='';
		document.getElementById(idt).selected=false;
	}
	selectedRow1=idt;
	alert(document.getElementById(idt).value);
	/*
	document.getElementById(idt).value='1';
	document.getElementById(idt).selected=true;

	if(firstTimeOver && document.getElementById(idt).type!='checkbox'){
		document.getElementById(selectedRow1).value=' ';
		document.getElementById(selectedRow1).selected=false;
		document.getElementById(selectedRow1).checked=false;

	}
	firstTimeOver=true;
	selectedRow1=idt;*/
}



var rowID;
function selectedForDelete(idt)
{
   rowID=idt;
   //alert(rowID);
}

//**************************** Add Row Start *********************************

function addRow(strtindx , endindx)
{
	  var table = document.getElementById("table");
	  var rowCount = table.rows.length;
	  var row=table.rows;
	  var tempvisibility = null, tempvisibility1 = 'hidden'.toString();
	  var count=0;

		for(var z=0;z<rowCount;z++)
		{
			tempvisibility = row[z].style.visibility;

			if (tempvisibility.toLowerCase() == tempvisibility1)
			{
					count=parseInt(count)+1;

			}
		}

count=parseInt(rowCount)-parseInt(count)-1;

	if(parseInt(count)<parseInt(rowCount-1))
	{


	  var temp="",str="",len="",nameindx="",str1="",nameOfElement="",typeOfElement="";

		var check = "",flagname = true,flagtype = true, flg = true;
		var elementIndex=0;
		var visibleElement = new Array();
		for(var r=0; r<2; r++)
		{
			check[r] = 'nxtrow';
		}

	 //alert("strtindx : "+strtindx+" endindx : "+endindx);
	 for(var i=1; i<=rowCount-1; i++){
	 	 var visibility = row[i].style.visibility;
	    var disp = row[i].style.display;
	   	if (visibility.toLowerCase() != tempvisibility1)
			{
					visibleElement[parseInt(i)-1]=true;

			}
			else{
				visibleElement[parseInt(i)-1]=false;
			}
	 }
	 for(i=1; i<=rowCount-1; i++)
	 {
	    visibility = row[i].style.visibility;
	    disp = row[i].style.display;
	   	if(visibleElement[parseInt(i)-1])
		{
		for (var j=1; j<=parseInt(endindx); j++)
		{
			str = row[i].cells[j].innerHTML;
			str1 = str.split(" ");
			len = str1.length;

			for(var k=0; k<str1.length; k++)
			{
				if((parseInt(str1[k].indexOf('id='))!= parseInt(-1))&& flagname)
				{
					nameOfElement = str1[k].substring(3);
					if(parseInt(nameOfElement.indexOf('>'))!= parseInt(-1))
						nameOfElement = nameOfElement.substring(0,parseInt(nameOfElement.length-1))
						flagname = false;
				}

				if((parseInt(str1[k].indexOf('type='))!= parseInt(-1))&& flagtype)
				{
					typeOfElement = str1[k].substring(5);
					if(parseInt(typeOfElement.indexOf('>'))!= parseInt(-1))
						typeOfElement = typeOfElement.substring(0,parseInt(typeOfElement.length-1))
						flagtype = false;
				}


				if((parseInt(str1[k].toLowerCase().indexOf('<select'))!= parseInt(-1)) && flagtype)
				{
					typeOfElement = 'Select';
					flagtype = false;
				}else if((parseInt(str1[k].indexOf('type='))!= parseInt(-1))&& ((typeOfElement.toLowerCase() != 'radio' && typeOfElement.toLowerCase() != 'radio') && flagtype))
				{
					typeOfElement = 'text';
					flagtype = false;
				}
				//alert("nameOfElement : "+nameOfElement+"typeOfElement : "+typeOfElement);
			}

			if(typeOfElement.toLowerCase() ==  'select')
				{
				//alert("In Select : "+document.getElementById(nameOfElement.toString()).selectedIndex);
					if(document.getElementById(nameOfElement.toString()).selectedIndex == 0)
					{
						check[k] = 'nxtrow1';
						flg = false;
					}
						typeOfElement = "";
						nameOfElement = "";
				}

				if(parseInt(typeOfElement.toLowerCase().indexOf('text'))!= parseInt(-1))
				{
					//alert("In text value is : "+document.getElementById(nameOfElement.toString()).value.trim());
					//alert("In text Condition is: "+(document.getElementById(nameOfElement.toString()).value.trim() == null || document.getElementById(nameOfElement.toString()).value.trim() == ""));

					if(document.getElementById(nameOfElement.toString()).value.trim() == null || document.getElementById(nameOfElement.toString()).value.trim() == "")
					{
						flg = false;
					}
					typeOfElement = "";
					nameOfElement = "";
				}

			flagtype = true;
			flagname = true;
		}

		}else{
			elementIndex=parseInt(i);
			break;
		}
	}
		//alert("<<<<<<<<<<<<<< Flag >>>>>>>>>>>> "+flg);

		if(!flg)
		{
			alert("Field can not be empty!");
		}else
		{
			  row[parseInt(elementIndex)].style.visibility='visible';
			  row[parseInt(elementIndex)].style.display = '';
		}
	}else{
					// No more left

					doAction('PFKEY90');
		}
}


//**************************** Add Row End *********************************

// **************************** Reset Field for Delete row operation Start *********************************
function resetfields(subStr)
{
		//alert("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<In Reset Field Method >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		var temp="",str="",len="",nameindx="",str1="",nameOfElement="",typeOfElement="";
		var check = "",flagname = true,flagtype = true;isNumber = false;

		var noofChild = document.getElementById(subStr).cells.length;

		var row=document.getElementById(subStr);

		for (var j=0; j<parseInt(noofChild); j++)
		{
			str = row.cells[j].innerHTML;
			str1 = str.split(" ");
			len = str1.length;

			for(var k=0; k<str1.length; k++)
			{
				if((parseInt(str1[k].indexOf('id='))!= parseInt(-1))&& flagname)
				{
					nameOfElement = str1[k].substring(3);
					if(parseInt(nameOfElement.indexOf('>'))!= parseInt(-1))
						nameOfElement = nameOfElement.substring(0,parseInt(nameOfElement.length-1))
						flagname = false;
				}

				if((parseInt(str1[k].indexOf('type='))!= parseInt(-1))&& flagtype)
				{
					typeOfElement = str1[k].substring(5);
					if(parseInt(typeOfElement.indexOf('>'))!= parseInt(-1))
						typeOfElement = typeOfElement.substring(0,parseInt(typeOfElement.length-1))
						flagtype = false;
				}

				if((parseInt(str1[k].toLowerCase().indexOf('<select'))!= parseInt(-1)) && flagtype)
				{
					typeOfElement = 'Select';
					flagtype = false;
				}else if((typeOfElement.toLowerCase() != 'radio' && typeOfElement.toLowerCase() != 'radio') && flagtype)
				{
					typeOfElement = 'text'
					flagtype = false;
				}

			}

				if ((parseInt(str.indexOf('digitsOnly'))!= parseInt(-1))){
					isNumber = true;
				}

				if(typeOfElement.toLowerCase() ==  'select')
				{
					document.getElementById(nameOfElement.toString()).selectedIndex = 0;
				}

				if(parseInt(typeOfElement.toLowerCase().indexOf('text'))!= parseInt(-1))
				{
					if (isNumber){
						document.getElementById(nameOfElement.toString()).value = "0";
					} else {
						//document.getElementById(nameOfElement.toString()).value = "";
					}
				}
			isNumber = false;
			flagtype = true;
			flagname = true;
		}
}
// **************************** Reset Field for Delete row operation End *********************************

// **************************** Delete Row Start *********************************
var indx = 0;
 function getSelected(opt)
 {
	var selected = new Array();

	for (var intLoop = 0; intLoop < opt.length; intLoop++)
	{
		//alert("***************** intLoop *************** "+intLoop);

		//alert("Opt in Get Selected : "+opt[intLoop].checked);
	   if (opt[intLoop].checked)
		{
		  indx = selected.length;
		  selected[indx] = new Object;
		  selected[indx].value = opt[intLoop].value;
		//alert("selected[indx].value : "+selected[indx].value );
		  selected[indx].index = intLoop;
		//alert("selected[indx].index : "+selected[indx].index );
	   }
	}
	//alert("return selected");

	for(var i=0; i<selected.length;i++)
	{
		//alert("================ selected[i].value : "+ selected[i].value);
		//alert("================ selected[i].index : "+ selected[i].index);
	}
	return selected;
 }

 function removeSelected(opt)
 {
	indx = 0;
	//alert("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<In Uncheck Function >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	for (var intLoop = 0; intLoop < opt.length; intLoop++)
	{
		//alert("Opt : "+opt[intLoop].toString());
	   if (opt[intLoop].checked)
		{
			opt[intLoop].checked = false;
			//alert("opt[intLoop].checked : "+opt[intLoop].checked );
	   }
	}
 }

 function getCheckBoxName(rowID)
 {

 	//alert("HELLOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
 	//alert(document.getElementById(rowID).innerHTML);
 	var innerHtml = document.getElementById(rowID).innerHTML;
 	var nameOfElement, subStr1;
 	var str1 = innerHtml.split("<td>");
 	var len = str1.length;

 	if((parseInt(str1[0].indexOf('id='))!= parseInt(-1)))
 	{

 		 subStr1 = str1[0].substring(parseInt(str1[0].indexOf('id='))+3);

 		 nameOfElement = subStr1.substring(0,parseInt(subStr1.indexOf(" ")));


 		if(parseInt(nameOfElement.indexOf('>'))!= parseInt(-1))
 			nameOfElement = nameOfElement.substring(0,parseInt(nameOfElement.length-1))

 			//alert("1111111111111111111111111111111111111111111"+nameOfElement)
 	}
 	return nameOfElement;
 }



function deleteRow(opt)
{

var indexstr = 8;
var table = document.getElementById("table");
var row=table.rows.length;
var totalRow;
var substr1,substr,str;

//alert("Opt is : "+opt);
var sel = new Array();
sel = getSelected(opt);
//alert("get return selected : " +sel.length);

	  var tableRow=table.rows;
	  var tempvisibility = null, tempvisibility1 = 'hidden'.toString();
	  var rowCount=0;
for(var z=0;z<row;z++)
		{
			tempvisibility = tableRow[z].style.visibility;

			if (tempvisibility.toLowerCase() == tempvisibility1)
			{
					rowCount=parseInt(rowCount)+1;

			}
		}

rowCount=parseInt(row)-parseInt(rowCount)-1;
//alert("rowCount" +rowCount);
for(var i=sel.length-1; i>=0;i--)
{
	if(rowCount!=sel.length||i!=0)
	{
	//alert("str is : "+ sel[i].index);
	substr = parseInt(sel[i].index);//str.substring(indexstr);
	//alert("substr is : "+ substr);
	totalRow = parseInt(row)-1;


	if(parseInt(substr)<totalRow)
	{
		substr1 = parseInt(substr)+1;
			//alert("alert in if "+substr1);
	}else
	{
		substr1 = parseInt(substr);
			//alert("alert in else "+substr1);
	}
	var substr2 = "tablerow"+substr1;
	//alert("substr2 is : "+ substr2);


	var chkname = getCheckBoxName(substr2);

	//alert("chkname is : "+ chkname);

	if(substr1<totalRow)
	{
		document.getElementById(chkname).checked = 'false';
		document.getElementById(substr2).style.visibility='hidden';
		document.getElementById(substr2).style.display='none';
	}
	if(substr1==totalRow)
	{
		document.getElementById(chkname).checked = 'false';
		document.getElementById(substr2).style.visibility='hidden';
		document.getElementById(substr2).style.display='none';
	}
	resetfields(substr2);
}
}
//alert(" Going to call Reset & Remove Selected : "+ substr1);
for(var i=0; i<opt.length;i++)
{
	//alert("opt[i].checked : "+opt[i].checked)
}

removeSelected(opt);

}


// **************************** Delete Row End *********************************


function moveScroll()
{
   var div1 = document.getElementById("subfo");
   var scrollAmt = div1.scrollTop;

  div1.scrollTop = parseInt(div1.scrollHeight);
}


function clearFocusField() {
	lastSelectedF = null;
	aForm = document.commonForm;
    if (aForm == null) {
    	aForm = document.form1;
    }
	if (aForm.focusInField) {
      	aForm.focusInField.value = "";
    }
}

var idRowArray=new Array();
var radioOptionElementId;
var radioElementCounter;
function selectedRowChecked(idt1,idt2,count)
{
    count=count-1;
        if(document.getElementById(idt2).type!='radio'){

            if(document.getElementById(idt2).checked){
                document.getElementById(idt1).value='1';
                idRowArray[count]=idt1;
            }else{
                document.getElementById(idt1).value=' ';
                idRowArray[count]="";
            }
        }else{

            if(radioOptionElementId!=null && radioOptionElementId!=""){
                var item = idRowArray[radioElementCounter];
                if(item!=null && item!=""){
                    if(document.getElementById(radioOptionElementId).checked){
                        document.getElementById(item).value=' ';
                        document.getElementById(radioOptionElementId).selected=false;
                        document.getElementById(radioOptionElementId).checked=false;
                        idRowArray[radioElementCounter]="";
                        radioOptionElementId="";
                    }
                }
            }
            if(document.getElementById(idt2).checked){
                document.getElementById(idt1).value='1';
                idRowArray[count]=idt1;
                radioElementCounter=count;
                radioOptionElementId=idt2;
            }else{
                document.getElementById(idt1).value=' ';
                idRowArray[count]="";
                radioElementCounter="";
                radioOptionElementId="";
            }
        }
}
</script>