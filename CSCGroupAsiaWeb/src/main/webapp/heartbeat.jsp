<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page session="false" %>
<%@ page import="com.quipoz.framework.util.*" %>
<%@ page import="com.csc.groupasia.runtime.variables.*" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head>
<title>POLISY/Unix</title>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%String ctx = request.getContextPath() + "/";%>
<%  HttpSession sess = request.getSession();
	BaseModel baseModel = (BaseModel) sess.getAttribute(BaseModel.SESSION_VARIABLE );
	GroupAsiaAppVars av = (GroupAsiaAppVars)baseModel.getApplicationVariables();
	long hb = av.getAppConfig().getUserHeartbeat();
	String lang1  = av.getUserLanguage().toString().toUpperCase();
%>







<LINK REL="StyleSheet" HREF="<%=ctx%>theme/eng/QAStyle.jsp" TYPE="text/css">
<LINK REL="StyleSheet" HREF="<%=ctx%>theme/eng/QAStyle.css" TYPE="text/css">
<script language='javaScript' src='<%=ctx%>/js/xmlReader.js' charset=UTF-8></script>
</head>
<body  onload='beatHeart()'>
<iframe  name='heart' src="heartbeat1.jsp"></iframe>
</body>
<script>
var lang1 = "<%=lang1%>";
	var timerHeartBeat;
	function beatHeart() {
		// fix bug147
		timerHeartBeat = setTimeout("delayedBeatHeart()", <%=hb%>);
	}

	function delayedBeatHeart() {
		try {
			// fix bug147
			//if (document.frames.heart.document.forms[0].heartcore.value == 0) {
			if (document.frames["heart"].document.forms[0].heartcore.value == 0) {
				//alert("The Server has gone down, is inaccessible, or has been restarted. You must log in again. Attempting to redirect you to the main menu ... V2'" + document.frames[0].document.forms[0].heartcore.value + "'");
				callCommonAlert(lang1,"No00024",document.frames[0].document.forms[0].heartcore.value);
				parent.location.replace('<%=ctx%>');
			}
			// fix bug147
			//else if (document.frames.heart.document.forms[0].heartcore.value == -1) {
			else if (document.frames["heart"].document.forms[0].heartcore.value == -1) {
				parent.location.replace('<%=ctx%>timeout.jsp');
			}
			else {
				//document.frames.heart.location = document.frames.heart.location;
				// fix bug147
				//document.frames.heart.location.reload(false);
				//setTimeout("delayedBeatHeart()", <%=hb%>);
				document.frames["heart"].location.reload(false);
				timerHeartBeat = setTimeout("delayedBeatHeart()", <%=hb%>);
			}
		}
		catch (e) {
			//alert("Error - The Server has gone down, is inaccessible, or has been restarted. You must log in again. Attempting to redirect you to the main menu ..." + e.toString());
			callCommonAlert(lang1,"No00025",e.toString());
			parent.location.replace('<%=ctx%>');
		}
	}
</script>
</html>