
	<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%String ctx = request.getContextPath() + "/";
	//Move these variable here to judge system language.

	String lan = "eng";

%>
		
		<title>POLISY JE</title>	
		<LINK REL="StyleSheet" HREF="<%=ctx%>theme/<%=lan%>/style.css" TYPE="text/css">  
		<script language='javaScript' src='js/xmlReader.js'></script>	
		<Script language="javascript" type="text/javascript">
			String.prototype.trim= function()  
			{    
   				 return this.replace(/(^\s*)|(\s*$)/g, "");  
			}	
			
			function submitform(form){ 
				userid = document.getElementById("userid");
				password = document.getElementById("password");
				if(userid.value.trim().length == 0){
					//alert("Please input your user name!");
					userid.className="input_cell red";
					callCommonAlert("CHI","No00028");
					return false;
				}
				else
				{
					userid.className="input_cell";
				}
				if(password.value.trim().length == 0 ){
					//alert("Please input your password !");
					password.className="input_cell red";
					callCommonAlert("CHI","No00029");
					return false;
				}
				else
				{
					password.className="input_cell";
				}	
				
				var thi = this.document.getElementById("loginId");
			    var sorce = thi.src;
				var img1 = new Image();
                var endObj = sorce.indexOf("_hover");
                if(endObj!=-1){
                  sorce=sorce.substring(0,endObj)+".png";   
                }
                if(sorce.indexOf("_after")==-1){
   
                  img1.src=sorce.replace(".png","_after.png");
                  thi.src=img1.src;
   
                  form.action="LoginServlet";
                  
                  return true;
                 }
				
			}
				

function changeMouseover(thi){
   var sorce=thi.src;
   if(sorce.indexOf("_after")==-1){
   var img1 = new Image();
   img1.src=sorce.replace(".png","_hover.png");
   thi.src=img1.src; 
   }
}

function changeMouseout(thi){
   var sorce=thi.src;
   if(sorce.indexOf("_after")==-1){
   var img1 = new Image();
   img1.src=sorce.replace("_hover.png",".png");
   thi.src=img1.src; 
   }
}
		</Script>
	</head>
<body style="overflow:hidden;">

<table align="center"  width="690">
	<tr>
				<td height="100"></td>
			
			</tr>
		<tr>
			<td height="400" background="screenFiles/logon.jpg">
			<img style="height:60px; width:300px; position:relative; top:-7px; left:20px;" src="screenFiles/csclogo.gif">
			
			<form id="signin" name="signin" method="post"
			onsubmit="return submitform(this);">
			<table align="left"  width="41%">
	
		<tr>
				
				<td height="190" width="15"></td>
			</tr>
			<tr>
				<td width="15"></td>
				<td align="left" width="65" >&nbsp;</td>
				<td width="114" style="font-family: Arial, Helvetica, sans-serif;	"><b>用户名</b></td>
				</tr>
				<tr>
				<td width="15"></td>
				<td align="left" width="65" style="font-family: Arial, Helvetica, sans-serif;	font-size: 12px;">&nbsp;</td>
				<td width="114"><input class="input_cell" style="width:109px; height:18px; " id="userid"  name="userid" type="text" value="" ></td>
				</tr>
				<tr>
				<td width="15"></td>
				<td height="10" width="65"></td>
				<td width="114"><span id="usernameErr" style="color:red; font-size:11px;"></span></td>
				</tr>
				<tr>
				<td width="15"></td>
				<td align="left" width="65" >&nbsp;</td>
				<td width="114" style="font-family: Arial, Helvetica, sans-serif;	"><b>密码</b>
				</td>
				</tr>
				<tr>
				<td width="15"></td>
				<td align="left" width="65" style="font-family: Arial, Helvetica, sans-serif;	font-size: 12px;">&nbsp;</td>
				<td width="114"><input class="input_cell" style="width:109px; height:18px;"  id="password" name="password" type="password" value="">
				</td>
				</tr>
				<tr>
				<td width="15"></td>
				<td height="10" width="65"></td>
				<td width="114"><span id="passwordErr" style="color:red; font-size:11px;"></span></td>
				
			</tr>
			
			<tr>
				<td width="15"></td>
				<td height="10" width="65">
				
				</td>
				<td width="114"><input id="loginId" type="image" src="screenFiles/btn_login_cn.png" border="0"  onMouseOver="changeMouseover(this)" onMouseOut="changeMouseout(this)"></td>
			</tr>
		</table>
		</form>
		</td>
	</tr>
	<tr>
		<td align="center" width="690" height="4">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<%
			if (request.getAttribute("isWrong") != null) {
			String isWrong = (String) request.getAttribute("isWrong");
			String message = (String) request.getAttribute("message");
			if (isWrong.equals("1")) {%>
				<span style="color:red">
					对不起, 您用户名或密码错误! 
				</span>
			<%}
		}
		%>
		</td>
	</tr>
</table>


</body>
</html>

