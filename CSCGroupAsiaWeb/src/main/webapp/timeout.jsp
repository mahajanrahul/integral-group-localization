<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Frameset//EN">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %> 
<%@ page import="com.quipoz.framework.util.*" %>
<HTML LANG="EN">
  <HEAD>
    <LINK REL="StyleSheet" HREF="theme/eng/QAStyle.jsp" TYPE="text/css">
    <LINK REL="StyleSheet" HREF="theme/eng/QAStyle.css" TYPE="text/css">
    <TITLE> TIME OUT Page </TITLE>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
  <HEAD>
<%String ctx = request.getContextPath() + "/";
String lang="eng";
	try{
		if (session != null) {
			BaseModel baseModel = (BaseModel) session.getAttribute(BaseModel.SESSION_VARIABLE );
	    	if ( baseModel != null) {
				AppVars av = baseModel.getApplicationVariables();
				lang=av.getUserLanguage().trim();
			}
		}
	}catch(Throwable e){
	}
%>
	<FRAMESET  ROWS="31,*"   frameBorder="0" framespacing="0">
		<FRAME style="border: 0" noresize="noresize" SRC="<%=ctx%>timeout_1.jsp" NAME="frameTitle" scrolling="no">
		<FRAMESET COLS="225,*"  frameBorder="0" framespacing=0>
			<FRAME style="border: 0"  noresize="noresize" SRC='<%=ctx%>timeout_2.jsp?lang=<%=lang%>' NAME="frameMenu" scrolling="no">
				<FRAMESET name="activeframe" ROWS="*, 5%, 0%"  frameBorder=0 framespacing="0">
				<FRAME style="border: 0" SRC='<%=ctx%>timeout_3.jsp' noresize="noresize" frameborder='0' NAME='mainForm'>
				<FRAME style="border: 0" SRC="<%=ctx%>POLAMessages.jsp?lang=<%=lang%>" frameborder='0' NAME='messages'>
			</FRAMESET>
		</FRAMESET>
	</FRAMESET>
</HTML>