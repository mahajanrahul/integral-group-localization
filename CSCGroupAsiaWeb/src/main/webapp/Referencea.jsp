<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page import="java.lang.ref.SoftReference"%>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<HTML>
<HEAD>
<title>Whwere is an object referenced</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="expires" content="-1">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-store">
<LINK REL="StyleSheet" HREF="theme/QAStyle.jsp" TYPE="text/css">
<LINK REL="StyleSheet" HREF="theme/QAStyle.css" TYPE="text/css">

<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="com.quipoz.framework.util.*" %>
<%@ page import="com.quipoz.framework.datatype.*" %>
<%@ page import="java.lang.reflect.*" %>
</head>
<BODY class="main">
<h2>Program references</h2>
<table class=none style='width:800px'>
<%Hashtable ht = AppVars.performance;
  HttpSession session1 = request.getSession(false);
  String fieldString = request.getParameter("ArrElt");
  String[] s = new String[ht.size()];%>
<%="Count of known Objects: " + s.length %>
<%s = (String[]) ht.keySet().toArray(s);
  Object o = null;
  Object v = null;
  Field[] fields = null;
  Field f = null;
  SoftReference r = null;
  Object searchFor = null;
  Class c = null;
  
  Object[] thing = (Object[]) ht.get(fieldString);
  r = (SoftReference)thing[0];
  if (r == null) {%>
  	<%="Object at position '" + fieldString + "' is null!."%>
<% 	return;
  }
  
  searchFor = r.get();
  if (searchFor == null) {%>
  	<%="Object at position '" + fieldString + "' has been cleaned up."%>
<% 	return;
  }
  
  String du = "Unable to view contents";
  if (searchFor instanceof BaseData) {
  	BaseData bd = (BaseData)searchFor;
  	du = "Field is a " + searchFor.getClass().getSimpleName();
  	du += "\nHashmap: " + bd.hashCode();
  	du += "\nLength: " + bd.getLength();
  	du += "\nField name: " + bd.getFieldName();
  	du += "\nValue: " + bd;
  	FixedLengthStringData fsd = bd.getParent();
  	if (fsd != null) {
  		du += "\nParent length: " + fsd.getLength();
  		du += "\nParent offset: " + bd.getParentOffset();
  	}
  	else {
  		du += "\nParent is null";
  	}
  	du += "\nCreation stack: " + thing[1];
  }
  else {
  	du = QPUtilities.dumpClass(searchFor, null);
  }
  du = QPUtilities.replaceSubstring(du, "\n", "<br>");
%>

<p><Class dump:><%=du%>
  
  
<table>  
  		
<%for (int i=0; i<s.length; i++) {
	if (fieldString.equals(s[i])) continue;
    Object[] inter = (Object[])ht.get(s[i]);
    r = (SoftReference)inter[0];
    if (r == null) continue;
   	o = r.get();
   	if (o == null) continue;
   	c = o.getClass();
   	while (c != null) {
		try {
			fields = o.getClass().getDeclaredFields();
	   		for (int j=0; j<fields.length; j++) {
	   			f = fields[j];
	   			f.setAccessible(true);
	   			v = f.get(o);
	   			if (v == searchFor) {%>
				    <tr>
				    <td><%=s[i]%>'></td>
				    <td><%=o.getClass().getSimpleName()%></td>
				    <td align=right><%=f.getName()%></td>
				    </tr>
<%	   			}
    		}
    	}
    	catch (Exception e) {}
    	c = c.getSuperclass();
    }%>
<%}%>
</table>
</BODY>
</HTML>