<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import='com.quipoz.framework.util.BaseModel' %>
<%String ctx = request.getContextPath() + "/";%>
 <%BaseModel bm3 = (BaseModel) request.getSession().getAttribute(BaseModel.SESSION_VARIABLE );
  String lang1  = bm3.getApplicationVariables().getUserLanguage().toString().toUpperCase(); 
%>
<html>
<%@ page session="false" %>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /></head>
<script language='javaScript' src='<%=ctx%>/js/xmlReader.js' charset=UTF-8></script>
<script language="javascript" type="text/javascript">
var lang1 = "<%=lang1%>";
	/*if monitor resolution is 1024*768, then display the application at left and top
	**else center the application screen
	**if the monitor resolution is smaller than 1024*768, display a warning message
	*/
	function OpenLogin() {
		var screenWidth = screen.width;
		var screenHeight = screen.height;
		if ((screenWidth < 1024) || (screenHeight < 768)) {
			//alert("Please change the resolution to 1024*768 in order to display the application normally");
			callCommonAlert(lang1,"No00026");
		}
		
		var displayFeatures = 'toolbar=1, location=1, menubar=1, status=1, scrollbars=1, width=1024, height=600, resizable=0';
		if ((screenWidth == 1024) &&(screenHeight == 768)) {
			displayFeatures += ', left=0, top=0';
		}else {
			var left = (screenWidth - 1024)/2;
			var top = (screenHeight - 768)/2;
			displayFeatures += ', left='+ left + ', top=' + top;
		}
		var newWindow = window.open('logon.jsp', '', displayFeatures);
		if (!newWindow) {
			//alert("Popup Window is not allowed, please make sure to make it allowed for this application running.");
			callCommonAlert(lang1,"No00027");
		}
	}
</script>
<body>
<h2>Basic menu</h2>
<h3>Currently available options</h3>
<ol>
	<li><a href='process?action_key=screenInit&screen=default&previousMenu=index.jsp' TARGET='_top'>Main Menu</a></li>
	<li><a href='process?action_key=screenInit&screen=defaultb&previousMenu=index.jsp' TARGET='_top'>Test DBCS</a></li>
	<li><a href='process?action_key=screenInit&screen=defaultc&previousMenu=index.jsp' TARGET='_top'>JSP Tester</a></li>
	<li><a TARGET='_top' onclick="OpenLogin();" href="javascript:">Logon</a></li>
</ol>
</body>
</html>