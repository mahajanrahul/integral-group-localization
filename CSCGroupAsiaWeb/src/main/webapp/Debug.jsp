<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<HTML>
<HEAD>
<title>Diagnostics</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="expires" content="-1">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-store">
<LINK REL="StyleSheet" HREF="theme/eng/QAStyle.jsp" TYPE="text/css">
<LINK REL="StyleSheet" HREF="theme/eng/QAStyle.css" TYPE="text/css">

<%@ page import="java.util.*" %>
<%@ page import="com.quipoz.framework.error.*" %>
<%@ page import="com.quipoz.framework.util.*" %>
<%@ page import="com.quipoz.framework.tablemodel.*" %>
<%@ page import="com.quipoz.framework.screenmodel.*" %>

</head>
<%
    BaseModel baseModel = (BaseModel) session.getAttribute(BaseModel.SESSION_VARIABLE );
    if ( baseModel != null) {
        ScreenModel fw = (ScreenModel) baseModel.getScreenModel();
        AppVars av = fw.getAppVars();
        AppConfig ac = av.getAppConfig();
%>
<BODY class="main">
<script>
    <%if (request.getParameter("reqDLevel") != null) {
        ac.setDebugLevel(QPUtilities.cInt(request.getParameter("reqDLevel") + "", 5));
    }%>
    <%if (request.getParameter("reqSLevel") != null) {
        ac.setSqlDiagLevel(QPUtilities.cInt(request.getParameter("reqSLevel") + "", 20));
    }%>
    <%if (request.getParameter("reqELevel") != null) {
        ac.setSqlEDiagLevel(QPUtilities.cInt(request.getParameter("reqELevel") + "", 500));
    }%>
    <%if (request.getParameter("reqMon") != null) {
        ac.setMonitorOn(request.getParameter("reqMon"));
    }%>
    <%if (request.getParameter("reqTrace") != null) {
        ac.setTraceSource(request.getParameter("reqTrace"));
    }%>
</script>
<Table>
<tr><th>Variable</th><th>Value</th></tr>
<tr><td>Debug level</td>
<td>

<SELECT name='DLevel'>
<OPTION VALUE="0"
<%if (ac.getDebugLevel() == 0) {%>
SELECTED
<%}%>
>None, not recommended</OPTION>
<OPTION VALUE="1" 
<%if (ac.getDebugLevel() == 1) {%>
SELECTED
<%}%>
>Errors only</OPTION>
<OPTION VALUE="2" 
<%if (ac.getDebugLevel() >= 2 && ac.getDebugLevel() < 5) {%>
SELECTED
<%}%>
>Warnings</OPTION>
<OPTION VALUE="5" 
<%if (ac.getDebugLevel() >= 5 && ac.getDebugLevel() < 55) {%>
SELECTED
<%}%>
>Low Debug</OPTION>
<OPTION VALUE="55" 
<%if (ac.getDebugLevel() >= 55 && ac.getDebugLevel() < 125) {%>
SELECTED
<%}%>
>Medium debug</OPTION>
<OPTION VALUE="125" 
<%if (ac.getDebugLevel() >= 125 && ac.getDebugLevel() < 250) {%>
SELECTED
<%}%>
>High debug</OPTION>
<OPTION VALUE="250"  
<%if (ac.getDebugLevel() >= 250) {%>
SELECTED
<%}%>
>Maximum debug</OPTION>
</SELECT>
</td>
</tr>
<tr>
<td>SQL Diagnostics</td>
<td>

<SELECT name='SLevel'>
<OPTION VALUE="0"
<%if (ac.getSqlDiagLevel() == 0) {%>
SELECTED
<%}%>
>None, not recommended</OPTION>
<OPTION VALUE="20" 
<%if (ac.getSqlDiagLevel() >= 20 && ac.getSqlDiagLevel() < 30) {%>
SELECTED
<%}%>
>Trace problems</OPTION>
<OPTION VALUE="30" 
<%if (ac.getSqlDiagLevel() >= 30 && ac.getSqlDiagLevel() < 50) {%>
SELECTED
<%}%>
>Trace Ins/Upd/Del</OPTION>
<OPTION VALUE="50" 
<%if (ac.getSqlDiagLevel() >= 50 && ac.getSqlDiagLevel() < 60) {%>
SELECTED
<%}%>
>Trace Selects</OPTION>
<OPTION VALUE="60" 
<%if (ac.getSqlDiagLevel() >= 60 && ac.getSqlDiagLevel() < 70) {%>
SELECTED
<%}%>
>Trace fetch times</OPTION>
<OPTION VALUE="70" 
<%if (ac.getSqlDiagLevel() >= 70 && ac.getSqlDiagLevel() < 100) {%>
SELECTED
<%}%>
>Show fetched data</OPTION>
<OPTION VALUE="100" 
<%if (ac.getSqlDiagLevel() >= 100 && ac.getSqlDiagLevel() < 150) {%>
SELECTED
<%}%>
>Trace connects</OPTION>
<OPTION VALUE="150"  
<%if (ac.getSqlDiagLevel() >= 150) {%>
SELECTED
<%}%>
>Maximum SQL debug</OPTION>
</SELECT>
</td>
</tr>

<tr>
<td>Diag SQL > mSecs</td>
<td><input name='ELevel' value=<%=ac.getSqlEDiagLevel()%>>
</tr>

<tr>
<td>Monitor variables</td>
<td>

<SELECT name='Mon'>
<OPTION VALUE="Y"
<%if (ac.getMonitorOn().equals("Y")) {%>
SELECTED
<%}%>
>Yes</OPTION>
<OPTION VALUE="Y"
<%if (ac.getMonitorOn().equals("N")) {%>
SELECTED
<%}%>
>No</OPTION>
</SELECT>
</td>
</tr>

<tr>
<td>Trace source location</td>
<td>

<SELECT name='Trace'>
<OPTION VALUE="on"
<%if (ac.getTraceSource().equals("on")) {%>
SELECTED
<%}%>
>Trace debugs to Class, Method and line number</OPTION>
<OPTION VALUE="low"
<%if (ac.getTraceSource().equals("low")) {%>
SELECTED
<%}%>
>Trace debugs to Class</OPTION>
<OPTION VALUE="off"
<%if (ac.getTraceSource().equals("low")) {%>
SELECTED
<%}%>
>Don't trace debugs</OPTION>
</SELECT>
</td>
</tr>

</table>
<Button onClick="doRefresh()">Update Diagnostics</Button>
<br><Button onClick="window.close()">Close this window</Button>
<p>
<%}%>
<Script>
  function doRefresh() {
     avar = 'Debug.jsp?reqDLevel=' + document.all("DLevel").value;
     avar = avar + '&&reqSLevel=' + document.all("SLevel").value;
     avar = avar + '&&reqELevel=' + document.all("ELevel").value;
     avar = avar + '&&reqMon=' + document.all("Mon").value;
     avar = avar + '&&reqTrace=' + document.all("Trace").value;
     window.showModalDialog(avar, '', 'dialogWidth:640px; dialogHeight:480px; resizable:yes; status:yes;');
     //window.open(avar);
     window.close();
  }
</Script>
</BODY>
</HTML>