<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<HTML>
<HEAD>
<title>Clear Diagnostics</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="expires" content="-1">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-store">
<LINK REL="StyleSheet" HREF="theme/QAStyle.jsp" TYPE="text/css">
<LINK REL="StyleSheet" HREF="theme/QAStyle.css" TYPE="text/css">

<%@ page import="java.util.*" %>
<%@ page import="com.quipoz.framework.error.*" %>
<%@ page import="com.quipoz.framework.util.*" %>
<%@ page import="com.quipoz.framework.tablemodel.*" %>
<%@ page import="com.quipoz.framework.screenmodel.*" %>

</head>
<%
    BaseModel baseModel = (BaseModel) session.getAttribute(BaseModel.SESSION_VARIABLE );
    if ( baseModel != null) {
        ScreenModel fw = (ScreenModel) baseModel.getScreenModel();
%>
<BODY class="main">
<script src="js/Sidebar.js"></script>
<h2>Clear Diagnostics results</h2>
<p>
<%=fw.clearDiagnostics()%>
<%}%>
<p>
<!--Button onClick="window.showModalDialog ('Diagnostics.jsp', ' ', 'dialogWidth:640px; dialogHeight:480px; resizable:yes; status:yes;');window.close()"'>Back to Diagnostics</Button-->
<Button onClick='window.open("Diagnostics.jsp", "Diagnostics", "height=480,width=640,status=yes,toolbar=no,menubar=yes,location=no,resizable=yes,scrollbars=yes")'>Show Diagnostics</Button>
<br><Button onClick="window.close()">Close this window</Button>

</BODY>
</HTML>