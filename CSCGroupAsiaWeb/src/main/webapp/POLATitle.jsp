<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">             
<%@ page session="true" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.quipoz.framework.util.*" %>
<%@ page import="com.csc.groupasia.runtime.variables.*"%>
<%@ page import="com.properties.PropertyLoader" %> 
<%@page import="com.resource.ResourceBundleHandler"%>


<HTML>                                                            
<HEAD>                                                            
<title>Generic Title page</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<%
String ctx = request.getContextPath() + "/";
 BaseModel baseModel = (BaseModel) session.getAttribute(BaseModel.SESSION_VARIABLE );
 GroupAsiaAppVars av = (GroupAsiaAppVars)baseModel.getApplicationVariables();
ResourceBundleHandler resourceBundle = new ResourceBundleHandler(av.getLocale());
String imageFolder = PropertyLoader.getFolderName(av.getLocale().toString()); 
String lan = av.getUserLanguage().toString().trim().toLowerCase();
if ("".equals(lan)){lan = "eng";}
%>  
<LINK REL="StyleSheet" HREF="theme/<%=lan%>/QAStyle.jsp" TYPE="text/css"> 
<LINK REL="StyleSheet" HREF="theme/<%=lan%>/QAStyle.css" TYPE="text/css">  
<%if(lan.equalsIgnoreCase("eng")){%>
<LINK REL="StyleSheet" HREF="<%=ctx%>theme/layout_1.css" TYPE="text/css">
<%}else{ %>
<LINK REL="StyleSheet" HREF="<%=ctx%>theme/<%=lan%>/layout.css" TYPE="text/css">
<%} %>
<style type="text/css">

#apDiv1 {
	position:absolute;
	left:750px;
	top:7px;
	width:250px;
	height:40px;
	z-index:1;
	overflow: hidden;
	
}
a {
    color:#FFFFFF;
	font-size:13px;
	font-family:Arial;
	text-decoration:none;
	height:12px;
	display:block;
	padding:1px;
}
a:hover
{
	background:#849EC6;
	color:#FFFFFF;
}


</style>
<script language='javaScript' src='js/xmlReader.js'></script>
<%@ page import="java.util.*" %>
<%@ page import="com.quipoz.framework.error.*" %>
<%@ page import="com.quipoz.framework.util.*" %>
<%@ page import="com.quipoz.framework.tablemodel.*" %>
<%@ page import="com.quipoz.framework.screenmodel.*" %>
<%@ page import="com.csc.smart400framework.SmartVarModel" %>
</HEAD>                                                            
  <BODY class="main">
  <script type="text/javascript">
  var contextPathName = "<%= request.getContextPath() %>";
  </script>
	<script src=js/Sidebar.js></script>
      <div id="apDiv1">
        <% 
    ScreenModel fw = (ScreenModel) baseModel.getOnScreenModel();
	GroupAsiaAppVars av1 = (GroupAsiaAppVars)baseModel.getApplicationVariables();
	GroupAsiaAppVars appVars = av1;
	boolean[] savedInds = null;
	if (savedInds == null) {}
	appVars.isEOF(); /* Meaningless code to avoid a Java IDE warning message */
    av1.reinitVariables();
%>
  <%String screenN2 = this.getClass().getSimpleName();
  screenN2 = screenN2.substring(1);
  screenN2 = QPUtilities.removeTrailing(screenN2, "Form");
 %>

      
      
</div>
 
<div class="container">
			<div class="browsing">
				<div class="logo" align="right">
					<img  src="<%=ctx%>screenFiles/<%=imageFolder%>/csclogo_new.gif">
				</div>
				<div style="position:absolute; left:220px;"><img src="<%=ctx%>screenFiles/<%=imageFolder%>/polisyJE_new.gif"></div>
				<div style="position:relative; left:570px; top:10px;" class="navigation">
				 
				<%--<%if(av.getUserLanguage().toString().equalsIgnoreCase("ENG")){ %>--%>
				<a href="javascript:;" style="float:left;" onClick="doAction('PFKEY02')"><%=resourceBundle.gettingValueFromBundle("Session Info")%>&nbsp;&nbsp;|</a>
                <a href="javascript:;" style="float:left;"  onClick="doAction('PFKEY01')">&nbsp;&nbsp;<%=resourceBundle.gettingValueFromBundle("Help")%>&nbsp;&nbsp;|</a>
                <a href="javascript:;" style="float:left;"  onClick="doAction('PFKEY16')">&nbsp;&nbsp;<%=resourceBundle.gettingValueFromBundle("Home")%>&nbsp;&nbsp;|</a>
                <a href="javascript:;" style="float:left;"  onClick="kill('ENG');return false;">&nbsp;&nbsp;<%=resourceBundle.gettingValueFromBundle("Logout")%>&nbsp;&nbsp;</a>

				
				<%--<%}else if(av.getUserLanguage().toString().equalsIgnoreCase("CHI")){ %>
				<a href="javascript:;" style="float:left;" onClick="doAction('PFKEY02')">会话信息&nbsp;&nbsp;|</a>
                <a href="javascript:;" style="float:left;"  onClick="doAction('PFKEY01')">帮助&nbsp;&nbsp;|</a>
                <a href="javascript:;" style="float:left;"  onClick="doAction('PFKEY16')">首页&nbsp;&nbsp;|</a>
                <a href="javascript:;" style="float:left;"  onClick="kill('CHI');return false;">注销&nbsp;&nbsp;&nbsp;</a>
				<%} %>--%>
				
			</div>
			</div>
	</div>
		<script type="text/javascript">		
	function kill(language) {
		//x = confirm("Are you sure you want to interrupt and kill your session?");
		x = confirm(callCommonConfirm(language,"No00031"));
		if (x == true) {
			// fix bug147
			top.frames["heartbeat"].frames["heart"].clearTimeout(
				top.frames["heartbeat"].frames["heart"].document.timerHeartBeat);
			/*window.showModalDialog ('<%=ctx%>KillSession.jsp?t=' + new Date(), ' ', 'dialogWidth:100px; dialogHeight:100px; resizable:yes; status:yes;');*/
			var killsession='<%=ctx%>logout?t=' + new Date();
			parent.location.replace(killsession);
		}
	}
</script>
			
  </BODY>
</HTML>
