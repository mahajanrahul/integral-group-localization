﻿var classCHI = "#535353";
var classENG = "";
var classSPA = "";
var classPOR = "";
var classJAP = "";
var classLSP = "";

var fontHightCHI = 10;
var fontHightENG = 8;
var fontHightSPA = 8;
var fontHightPOR = 8;
var fontHightJAP = 10;
var fontHightLSP = 8;

var fontCHI = "选择";
var fontENG = "Select";
var fontSPA = "Seleccione";
var fontPOR = "selecionar";
var fontJAP = "選択してください";
var fontLSP = "Seleccionar";

var font0CHI = "--------------选择-------------";
var font0ENG = "-------------------Select-------------------";
var font0SPA = "-------------------Seleccione-------------------";
var font0POR = "-------------------selecionar-------------------";
var font0JAP = "-------------------選択してください-------------------";
var font0LSP = "-------------------Seleccionar-------------------";