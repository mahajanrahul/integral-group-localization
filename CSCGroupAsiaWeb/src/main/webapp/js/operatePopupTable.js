/*
* Author: Ai Hao 2010-7-20
*/

var popupTableId;
var popupFormId;
var popupPageSize;
var popupSfl;
var popupElements;

function operatePopupTable(tableId, formId, pageSize, sfl, elements){
	popupTableId	=	tableId;
	popupFormId		=	formId;
	popupPageSize	=	pageSize;
	popupSfl		=	sfl;
	popupElements	=	elements;
	//remove it for new style by maxia(2010-8-2)
	//$("#" + popupFormId).css("border","4px solid #35759b");
}
//-----------------------------------------------------------------------
function adddata(){
	var rownum = 0;
	$(".sSky-Main").find("tr").filter('.popupRow').each(function(){
		rownum++;
	});
	
	rownum++;
	
	if($("#" + popupFormId).css("visibility") != "visible") {
		$("#" + popupFormId).css("visibility", "visible");
		
		//$("#rownumber").val(rownum);
		$("#rownumberPopup")[0].innerText = rownum;
		
		for(var i=0; i < popupElements.length; i++)
		{
			$("#" + popupElements[i]).val("");
		}
	}
}
//-----------------------------------------------------------------------
function selectRow(itemrow){	
	var total=1;
	var hasMore = true;
	while(hasMore)
	{
		hasMore = false;
		
		$(".row" + total).each(function(){
			if((Number(total)%2)==0)
				$(this).css("background","#EEEEEE");
			else
				$(this).css("background","#FFFFFF");
				
			hasMore = true;
		});	
		
		total++;
	}

	$("#rownumber").focus();
	var rowId = itemrow.id;
	var rowNum = rowId.substr(3);
	
	$("." + rowId).each(function(){
		if($(this).css("background") == "#FFF799")
		{
			if((Number(rowNum)%2)==0)
			{
				$(this).css("background","#EEEEEE");
			}
			else
			{
				$(this).css("background","#FFFFFF");
			}
			$("#rownumber").val("");
			$("#rownumber").focus();
			$("#removeDivPopup").removeClass("sectionbutton");
			$("#removeDivPopup").addClass("sectionbtndisable");
		}
		else
		{
			$(this).css("background","#FFF799");
			$("#rownumber").val(rowNum);
			$("#removeDivPopup").removeClass("sectionbtndisable");
			$("#removeDivPopup").addClass("sectionbutton");
		}	
	});
	
	//$("#subfile_remove").attr("src","/"+ contextPathName +"/screenFiles/remove.gif");
	$("#subfile_remove").attr("src","/"+ contextPathName +"/screenFiles/"+ imageFolder +"/remove.gif");
	$("#subfile_remove").click(function() {removedata();});
}
// add by Cary_zhong (03-08-2010)for disable remove button when double click
$(document).ready(function() {
	$("tr").each(function(i){
		if($(this).attr("ondblclick")!=null && 
		$(this).attr("ondblclick").toString().indexOf("checkRow")!=-1){
			$(this).bind('dblclick', function() {
				$("#removeDivPopup").removeClass("sectionbutton");
				$("#removeDivPopup").addClass("sectionbtndisable");
			});
		}
	});
});
//-----------------------------------------------------------------------
function closediv(){
	$("#" + popupFormId).css("visibility", "hidden");
}
//-----------------------------------------------------------------------
function removedata(){
	var total=1;		
	$(".sSky-Main").find("tr").filter('.popupRow').each(function(){
		total++;
	});

	//don't remove the last line
	if(total == 2) return false;
	
	var deletedRow = $("#row" + ($('#rownumber').val()));
	
	if($("#" + popupFormId).css("visibility") != "visible") {	
		if(deletedRow){
			var rownum = $('#rownumber').val(); 
			$("#rownumber").val("");
			$("#rownumberPopup")[0].innerText="";
			
			var remove = rownum;
			for(var i = Number(remove) + 1; i<= total ; i++) {
				for(var n=0; n < popupElements.length; n++)
				{
					//document.getElementById(popupSfl + ".seqno_R" + remove + "_div").value = document.getElementById(popupSfl + ".seqno_R" + i + "_div").value;
					document.getElementById(popupSfl + "." + popupElements[n] + "_R" + remove).value = 
							document.getElementById(popupSfl + "." + popupElements[n] + "_R" + i).value;
				}
				remove++;
			}			

			for(var i=0; i < popupElements.length; i++)
			{
				document.getElementById(popupSfl + "." + popupElements[i] + "_R" + remove).value = ""; 
			}
			
			doAction('PFKEY05');
		}else{
			alert('now rows to be deleted');
		}
	}
}