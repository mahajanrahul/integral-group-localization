/*
* Author: Ai Hao 2010-7-20
*/
//global variable
var pageLang;

var superTable = function (tableId, options) {
/////* Initialize */
	options = options || {};
	this.cssSkin = options.cssSkin || "sSky";
	this.headerRows = parseInt(options.headerRows || "1");
	this.fixedCols = parseInt(options.fixedCols || "0");
	this.colWidths = options.colWidths || [];
	this.initFunc = options.onStart || null;
	this.callbackFunc = options.onFinish || null;
	this.moreBtn = options.moreBtn || "N";
	this.addRemoveBtn = options.addRemoveBtn || "N";
	this.hasHorizonScroll = options.hasHorizonScroll || "N";
	this.popupAddRemoveBtn = options.popupAddRemoveBtn || "N";
	this.pageLanguage = options.pageLanguage || "ENG";
	this.moreBtnPath = options.moreBtnPath || contextPathName+"/screenFiles/eng/moreButton.gif";	//IPNC-1825 modify  More button image path
	this.isReadOnlyFlag = options.isReadOnlyFlag; // to jduge if the page is readonly or not
	this.singleColumn = options.singleColumn || "N";	//IGroup-1086 for table with single column
	this.initFunc && this.initFunc();
	
	pageLang = this.pageLanguage;

/////* Create the framework dom */
	this.sBase = document.createElement("DIV");
	this.sFHeader = this.sBase.cloneNode(false);
	this.sHeader = this.sBase.cloneNode(false);
	this.sHeaderInner = this.sBase.cloneNode(false);
	this.sFData = this.sBase.cloneNode(false);
	this.sFDataInner = this.sBase.cloneNode(false);
	this.sData = this.sBase.cloneNode(false);
	this.sColGroup = document.createElement("COLGROUP");
	
	this.sDataTable = document.getElementById(tableId);
	//remove "relative"
	var tempRows = this.sDataTable.tBodies[0].rows;
	var tempStr;
	for(i=0; i<tempRows.length; i++)
	{
		for (j=0; j<tempRows[i].cells.length; j++)
		{
			tempStr = this.sDataTable.tBodies[0].rows[i].cells[j].innerHTML;
			tempStr = tempStr.replace(/relative/g, "");
			this.sDataTable.tBodies[0].rows[i].cells[j].innerHTML = tempStr;
		}	
	}		

	this.sDataTable.style.margin = "0px"; /* Otherwise looks bad */
	if (this.cssSkin !== "") {
		this.sDataTable.className += " " + this.cssSkin;
	}
	if (this.sDataTable.getElementsByTagName("COLGROUP").length > 0) {
		this.sDataTable.removeChild(this.sDataTable.getElementsByTagName("COLGROUP")[0]); /* Making our own */
	}
	this.sParent = this.sDataTable.parentNode;
	this.sParentHeight = this.sParent.offsetHeight;
	this.sParentWidth = this.sParent.offsetWidth;
	
/////* Attach the required classNames */
	this.sBase.className = "sBase";
	this.sFHeader.className = "sFHeader";
	this.sHeader.className = "sHeader";
	this.sHeaderInner.className = "sHeaderInner";
	this.sFData.className = "sFData";
	this.sFDataInner.className = "sFDataInner";
	this.sData.className = "sData";
	
/////* Clone parts of the data table for the new header table */
	var alpha, beta, touched, clean, cleanRow, i, j, k, m, n, p;
	this.sHeaderTable = this.sDataTable.cloneNode(false);
	if (this.sDataTable.tHead) {
		alpha = this.sDataTable.tHead;
		this.sHeaderTable.appendChild(alpha.cloneNode(false));
		beta = this.sHeaderTable.tHead;
	} else {
		alpha = this.sDataTable.tBodies[0];
		this.sHeaderTable.appendChild(alpha.cloneNode(false));
		beta = this.sHeaderTable.tBodies[0];
	}
	alpha = alpha.rows;
	for (i=0; i<this.headerRows; i++) {
		beta.appendChild(alpha[i].cloneNode(true));
	}
	this.sHeaderInner.appendChild(this.sHeaderTable);
	
	if (this.fixedCols > 0) {
		this.sFHeaderTable = this.sHeaderTable.cloneNode(true);
		this.sFHeader.appendChild(this.sFHeaderTable);
		this.sFDataTable = this.sDataTable.cloneNode(true);
		/* modified  IGroup-937 yzhong4*/
		$(this.sFDataTable).html($(this.sDataTable).html());
		//added by Ai Hao
		//remvoe duplicate column, make fields unique between two tables
		var srows = this.sFDataTable.tBodies[0].rows;
		for(i=this.headerRows; i<srows.length; i++)
		{
			for (j=this.fixedCols; j<srows[i].cells.length; j++)
			{
				srows[i].cells[j].innerHTML = "";
			}	
		}	
		var sDataRows = this.sDataTable.tBodies[0].rows;
		for(i=this.headerRows; i<sDataRows.length; i++)
		{
			for (j=0; j<this.fixedCols; j++)
			{
				sDataRows[i].cells[j].innerHTML = "";
			}	
		}			
		
		
		this.sFDataInner.appendChild(this.sFDataTable);
	}
	
/////* Set up the colGroup */
	alpha = this.sDataTable.tBodies[0].rows;
	for (i=0, j=alpha.length; i<j; i++) {
		clean = true;
		for (k=0, m=alpha[i].cells.length; k<m; k++) {
			if (alpha[i].cells[k].colSpan !== 1 || alpha[i].cells[k].rowSpan !== 1) {
				i += alpha[i].cells[k].rowSpan - 1;
				clean = false;
				break;
			}
		}
		if (clean === true) break; /* A row with no cells of colSpan > 1 || rowSpan > 1 has been found */
	}
	
	cleanRow = (clean === true) ? i : 0; /* Use this row index to calculate the column widths */
	for (i=0, j=alpha[cleanRow].cells.length; i<j; i++) {
		if (i === this.colWidths.length || this.colWidths[i] === -1) {
			this.colWidths[i] = alpha[cleanRow].cells[i].offsetWidth;
		}
	}
	
	for (i=0, j=this.colWidths.length; i<j; i++) {
		this.sColGroup.appendChild(document.createElement("COL"));
		this.sColGroup.lastChild.setAttribute("width", this.colWidths[i]);
	}		

	this.sDataTable.insertBefore(this.sColGroup.cloneNode(true), this.sDataTable.firstChild);
	this.sHeaderTable.insertBefore(this.sColGroup.cloneNode(true), this.sHeaderTable.firstChild);
	if (this.fixedCols > 0) {
		this.sFDataTable.insertBefore(this.sColGroup.cloneNode(true), this.sFDataTable.firstChild);
		this.sFHeaderTable.insertBefore(this.sColGroup.cloneNode(true), this.sFHeaderTable.firstChild);
	}
	
/////* Style the tables individually if applicable */
	if (this.cssSkin !== "") {
		this.sDataTable.className += " " + this.cssSkin + "-Main";
		this.sHeaderTable.className += " " + this.cssSkin + "-Headers";
		if (this.fixedCols > 0) {
			this.sFDataTable.className += " " + this.cssSkin + "-Fixed";
			this.sFHeaderTable.className += " " + this.cssSkin + "-FixedHeaders";
		}
	}

/////* Throw everything into sBase */
	if (this.fixedCols > 0) {
		this.sBase.appendChild(this.sFHeader);
	}
	this.sHeader.appendChild(this.sHeaderInner);
	this.sBase.appendChild(this.sHeader);
	if (this.fixedCols > 0) {
		this.sFData.appendChild(this.sFDataInner);
		this.sBase.appendChild(this.sFData);
	}
	this.sBase.appendChild(this.sData);
	this.sParent.insertBefore(this.sBase, this.sDataTable);
	this.sData.appendChild(this.sDataTable);
		
/////* Align the tables */
	var sDataStyles, sDataTableStyles;
	
	//calculate total height of header
	if(this.sDataTable.tBodies[0].rows.length == this.headerRows) 
	{
		//For none-data table
		this.sHeaderHeight = this.sDataTable.tBodies[0].rows[this.headerRows - 1].offsetTop + 		//the offsetTop of last head-row
							 this.sDataTable.tBodies[0].rows[this.headerRows - 1].offsetHeight;		//the height of last head-row
	}
	else
	{
		this.sHeaderHeight = this.sDataTable.tBodies[0].rows[(this.sDataTable.tHead) ? 0 : this.headerRows].offsetTop;
	}	
	
	sDataTableStyles = "margin-top: " + (this.sHeaderHeight * -1) + "px;";
	sDataStyles = "margin-top: " + this.sHeaderHeight + "px;";
	sDataStyles += "height: " + (this.sParentHeight - this.sHeaderHeight) + "px;";

	$(".fakeContainer").css("height", (parseInt($(".fakeContainer").css("height")) + 4));
	$(".fakeContainer").css("width", (parseInt($(".fakeContainer").css("width")) + 4));
			
	if (this.fixedCols > 0) {		
		/* A collapsed table's cell's offsetLeft is calculated differently (w/ or w/out border included) across broswers - adjust: */
		this.sFHeaderWidth = this.sDataTable.tBodies[0].rows[cleanRow].cells[this.fixedCols].offsetLeft;
		if (window.getComputedStyle) {
			alpha = document.defaultView;
			beta = this.sDataTable.tBodies[0].rows[0].cells[0];
			if (navigator.taintEnabled) { /* If not Safari */
				this.sFHeaderWidth += Math.ceil(parseInt(alpha.getComputedStyle(beta, null).getPropertyValue("border-right-width")) / 2);
			} else {
				this.sFHeaderWidth += parseInt(alpha.getComputedStyle(beta, null).getPropertyValue("border-right-width"));
			}
		} else if (/*@cc_on!@*/0) { /* Internet Explorer */
			alpha = this.sDataTable.tBodies[0].rows[0].cells[0];
			beta = [alpha.currentStyle["borderRightWidth"], alpha.currentStyle["borderLeftWidth"]];
			if(/px/i.test(beta[0]) && /px/i.test(beta[1])) {
				beta = [parseInt(beta[0]), parseInt(beta[1])].sort();
				this.sFHeaderWidth += Math.ceil(parseInt(beta[1]) / 2);
			}
		}
		
		/* Opera 9.5 issue - a sizeable data table may cause the document scrollbars to appear without this: */
		this.sFData.style.width = this.sParentWidth + "px";
		
		this.sFHeader.style.width = this.sFHeaderWidth + "px";
		sDataTableStyles += "margin-left: " + (this.sFHeaderWidth * -1) + "px;";
		sDataStyles += "margin-left: " + this.sFHeaderWidth + "px;";
		//modified by hai2 for IGroup-937, Example S2473 begin
		//modified by hai2 for IPNC-1245, Example S2473 begin
		/*
		if(navigator.userAgent.indexOf("Firefox") > 0)	sDataStyles += "width: " + (this.sParentWidth - this.sFHeaderWidth + 15) + "px;";
		else sDataStyles += "width: " + (this.sParentWidth - this.sFHeaderWidth) + "px;";
		*/
		//Since UIG make 730px as the key point in lots of JSP 
		if($(".fakeContainer").parent().is("div") && 
		   $(".fakeContainer").parent().parent().attr("class") != "tabcontent" && 
		   this.sParentWidth >= 730 &&	//for superTable generated by UIG
		   $(".fakeContainer").parent().attr("class") != "outerDiv"	//IGroup-1086
		)	
		{
			if(navigator.userAgent.indexOf("Firefox") > 0 || 
			   navigator.userAgent.indexOf("Chrome") > 0 || 
			   navigator.userAgent.indexOf("Trident/7.0") > 0 ||
			   navigator.userAgent.indexOf("Trident/6.0") > 0)			//IGroup-1086 Add Trident/6.0 for IE10
			{
				sDataStyles += "width: " + (parseInt($(".fakeContainer").css("width")) - this.sFHeaderWidth + 2)  + "px;";	
			}
			else
			{
				sDataStyles += "width: " + (this.sParentWidth - this.sFHeaderWidth)  + "px;";	
			}
		}
		else //for original manual superTable or UIG generated table with less than 730px width
		{
			if(this.hasHorizonScroll == "N" && 
			  (navigator.userAgent.indexOf("Firefox") > 0 || 
			   navigator.userAgent.indexOf("Chrome") > 0 || 
			   navigator.userAgent.indexOf("Trident/7.0") > 0 ||
			   navigator.userAgent.indexOf("Trident/6.0") > 0))	//IGroup-1086 Add Trident/6.0 for IE10
			{
				sDataStyles += "width: " + (this.sParentWidth - this.sFHeaderWidth + 17)  + "px;";
			}
			else
			{
				sDataStyles += "width: " + (this.sParentWidth - this.sFHeaderWidth) + "px;";
			}
		}
	} 
	//IPNC-1915 for table with single column
	else if((this.singleColumn == "Y" || this.fixedCols == 0) && (
			navigator.userAgent.indexOf("Firefox") > 0 || 
			navigator.userAgent.indexOf("Chrome") > 0 || 
			navigator.userAgent.indexOf("Trident/7.0") > 0 ||
			navigator.userAgent.indexOf("Trident/6.0") > 0		
			))
	{
		sDataStyles += "width: " + (this.sParentWidth - this.sFHeaderWidth + 17)  + "px;";
	}
	else {
		sDataStyles += "width: " + this.sParentWidth + "px;";
	}
	////modified by hai2 for IGroup-937, Example S2473 end

	if(this.hasHorizonScroll == "Y") sDataStyles += "overflow-x:scroll;"
	this.sData.style.cssText = sDataStyles;
	this.sDataTable.style.cssText = sDataTableStyles;
	
    //set the different color of interlace row
	var line = 1;
	//IGROUP-1211 IE11 emulation, remove bg color for sData, begin
	if(navigator.userAgent.indexOf("Trident/7.0") > 0 && this.hasHorizonScroll == "N")
	{	
		var tdIndex = 1;
		var fixed = this.fixedCols;
	$(".sSky-Main").find("tr:gt(" + (this.headerRows - 1) + ")").map(function(){
			tdIndex = 1;
		if(line % 2 == 0)
			{
				$(this).find("td").map(function(){
					if(tdIndex > fixed)
					{
			$(this).addClass("evenLineBg");
					}
					tdIndex++;
				});				
			}
		else
			{
				$(this).find("td").map(function(){
					if(tdIndex > fixed)
					{
		    $(this).addClass("oddLineBg");
					}
					tdIndex++;
				});					
			}
			line++;	
		});
	}
	else
	{
		$(".sSky-Main").find("tr:gt(" + (this.headerRows - 1) + ")").map(function(){
			if(line % 2 == 0)
				$(this).addClass("evenLineBg");
			else
			    $(this).addClass("oddLineBg");
						
		line++;
	});
	}
	//IGROUP-1211 IE11 emulation, remove bg color for sData, end
	
	line = 1;
	$(".sSky-Fixed").find("tr:gt(" + (this.headerRows - 1) + ")").map(function(){
		if(line % 2 == 0)
			$(this).addClass("evenLineBg");
		else
		    $(this).addClass("oddLineBg");
						
		line++;
	});		

	//set cover for right-top
	$(".topCover").appendTo($(".fakeContainer"));
	$(".topCover").css("height", $(".sSky-Headers").height() - 1);
	if(this.headerRows == 0) $(".topCover").css("display","none");
	
	
	$(".bottomCover").css("display","none");

	//if has horizon scroll, uses div cover the left-bottom and set "left" of right-top cover
	if(this.hasHorizonScroll == "Y" && 
	   navigator.userAgent.indexOf("Firefox") == -1 &&
	   navigator.userAgent.indexOf("Chrome") == -1 &&
	   navigator.userAgent.indexOf("Trident/7.0") == -1 &&
	   navigator.userAgent.indexOf("Trident/6.0") == -1) //IGroup-1086 Add Trident/6.0 for IE10
	{	
		//For IE8
		$(".topCover").css("left", $(".fakeContainer").innerWidth() - 20);
		
		$(".bottomCover").css("display","");
		$(".bottomCover").appendTo($(".fakeContainer"));
		$(".bottomCover").css("top", $(".fakeContainer").innerHeight() - 17);
		
		var i = this.fixedCols;
		var coverWidth = 0;
		for(var j=0; j<i; j++)
			coverWidth = coverWidth + this.colWidths[j];
		$(".bottomCover").css("width", coverWidth).css("height", 17);
		$(".fakeContainer").css("width", parseInt($(".fakeContainer").css("width")));	
	}
	//modified by hai2 for IGroup-937
	//else if(this.hasHorizonScroll == "Y" && navigator.userAgent.indexOf("Firefox") > 0)
	else if(this.hasHorizonScroll == "Y" && 
			(navigator.userAgent.indexOf("Firefox") > 0 ||
			 navigator.userAgent.indexOf("Chrome") > 0 || 
			 navigator.userAgent.indexOf("Trident/7.0") > 0 ||
			 navigator.userAgent.indexOf("Trident/6.0") > 0))	//IGroup-1086 Add Trident/6.0 for IE10
	{
		$(".topCover").css("left", $(".fakeContainer").innerWidth() - 20);
		
		$(".bottomCover").css("display","");
		$(".bottomCover").appendTo($(".fakeContainer"));
		$(".bottomCover").css("top", $(".fakeContainer").innerHeight() - 17);
		
		var i = this.fixedCols;
		var coverWidth = 0;
		for(var j=0; j<i; j++)
			coverWidth = coverWidth + this.colWidths[j];
		$(".bottomCover").css("width", coverWidth).css("height", 17);		
	}
	//IGroup-1086 for table with single column
	else if(this.singleColumn == "Y" && (
			navigator.userAgent.indexOf("Firefox") > 0 || 
			navigator.userAgent.indexOf("Chrome") > 0 || 
			navigator.userAgent.indexOf("Trident/7.0") > 0 ||
			navigator.userAgent.indexOf("Trident/6.0") > 0		
			))
	{
		$(".topCover").css("left", $(".fakeContainer").innerWidth() - 4);
		$(".fakeContainer").css("width", this.sParentWidth + 17);
	}	
	//modified by hai2 for IPNC-1245
	//else if(navigator.userAgent.indexOf("Firefox") > 0)
	else if(navigator.userAgent.indexOf("Firefox") > 0 || 
			navigator.userAgent.indexOf("Chrome") > 0 || 
			navigator.userAgent.indexOf("Trident/7.0") > 0 ||
			navigator.userAgent.indexOf("Trident/6.0") > 0)	//IGroup-1086 Add Trident/6.0 for IE10
	{
		if($(".fakeContainer").innerWidth() == 0)
		{
			$(".topCover").css("left", parseInt($(".fakeContainer").css("width")) - 2);
		}
		else
		{
			$(".topCover").css("left", $(".fakeContainer").innerWidth() - 4);
		}
		
		$(".fakeContainer").css("width", parseInt($(".fakeContainer").css("width")) + 17);
	}
	else
	{
		//For IE
		$(".topCover").css("left", $(".fakeContainer").innerWidth() - 3);
		//17 is the width of vertical scroll.
		//Extend width of container because the vertical scroll always appears.	
		$(".fakeContainer").css("width", parseInt($(".fakeContainer").css("width")) + 17);
	}

	//More button and add-remove buttons
	//define more button and add-remove button of popup
	//normal add-remove button defines in the operateSuperTale.js
	var moreBtnText		=	moreBtnText_ENG;
	var addBtnText		=	addBtnText_ENG;
	var removeBtnText	=	removeBtnText_ENG;
	var rowNumText		=	rowNumText_ENG;
	if(this.pageLanguage == "CHN")
	{
		moreBtnText		=	moreBtnText_CHN;
		addBtnText		=	addBtnText_CHN;
		removeBtnText	=	removeBtnText_CHN;
		rowNumText		=	rowNumText_CHN;
	}	
	var moreBtnDiv =  "<DIV id='moreDiv' style='POSITION: absolute; OVERFLOW-Y: hidden;'>" +
						"<INPUT style='display:none;Z-INDEX: 2; POSITION: relative; TEXT-ALIGN: left; " +
						"WIDTH: 30px; TOP: 0px; LEFT: 0px' readOnly class='output_cell  bold' value='' id='rownumber'>" +
						"<div class='hasMore'><div>" +
						"<a id='more' href='javascript:;'  onClick=pressMoreButton('PFKey90')>" + 
						"<img id='moreButton' name='moreButton' src=" + this.moreBtnPath + " border='0' style='height:25px'>"
						+ "</a></div></div>" +
						"</DIV>";		 
	var popupTableDiv = "<DIV id='popupTableDiv' style='POSITION: absolute; OVERFLOW-Y: hidden;" +
						"border:#35759b 1px solid; border-top:0px;' >" +
            			"<table style='height:30px;border:none;POSITION:relative;left:10px;' cellspacing='0'>" +
            			"<tr>" +
      				  	"<td><div class='sectionbutton mr10'><p>" +
      				  	"<a  id='subfile_add' href='javascript:;' onclick='adddata()'>" +
      				  	addBtnText + "</a></p></div></td>" +
      				  	"<td><div id ='removeDivPopup' class='sectionbtndisable mr10'><p>" +
      				  	"<a id='subfile_remove' href='javascript:;'>" +
      				  	removeBtnText + "</a></p></div></td>" +
            			"<td><div style='POSITION: relative;left:20px; font-weight:bold;'>" + rowNumText + "&nbsp;&nbsp;&nbsp;" +
						"<INPUT style='Z-INDEX: 2; POSITION: relative; TEXT-ALIGN: left; " +
						"WIDTH: 30px; TOP: 0px; LEFT: 0px' readOnly class='output_cell  bold' value='' id='rownumber'></div></td>" +
            			"</tr></table></DIV>";			

	if(this.isReadOnlyFlag && this.moreBtn == "Y")
	{
			$(".fakeContainer").after(moreBtnDiv);
	
			//modified by hai2 for IGroup-937, for Firefox, Chrome and IE11 
			/*if(navigator.userAgent.indexOf("Firefox") > 0)
				$("#moreDiv").css("top", parseInt($(".fakeContainer").css("top")));
			else
			*/

			$("#moreDiv").css("top", parseInt($(".fakeContainer").css("top")) - 25);
			//added by hai2 for IGroup-937, for Firefox, Chrome and IE11 
			if(navigator.userAgent.indexOf("Firefox") > 0 || 
			   navigator.userAgent.indexOf("Chrome") > 0 || 
			   navigator.userAgent.indexOf("Trident/7.0") > 0 ||
			   navigator.userAgent.indexOf("Trident/6.0") > 0)		//IGroup-1086 changed for IE10	
			{
				$("#moreDiv").css("left", parseInt($(".fakeContainer").css("left")) + parseInt($(".fakeContainer").css("width")) - 88)
				 .css("width", parseInt($(".fakeContainer").css("width")));					
			}
			else
			{
				//IE8
				$("#moreDiv").css("left", parseInt($(".fakeContainer").css("left")) + parseInt($(".fakeContainer").css("width")) - 92)
				 .css("width", parseInt($(".fakeContainer").css("width")));	
			}
	

			//if(this.popupAddRemoveBtn != 'Y') $(".rowNum").css("display","none")			
	}
	else if(!this.isReadOnlyFlag)
	{
			if(this.addRemoveBtn == 'Y')
			{	
				$("#addRemoveDiv").css("display","block");
				 //modified by hai2 for IGroup-937, for Firefox, Chrome and IE11 
				 //if(navigator.userAgent.indexOf("Firefox") > 0)
				 if(navigator.userAgent.indexOf("Firefox") > 0 || 
				    navigator.userAgent.indexOf("Chrome") > 0 || 
				    navigator.userAgent.indexOf("Trident/7.0") > 0 ||
				    navigator.userAgent.indexOf("Trident/6.0") > 0)		//IGroup-1086 changes for IE10
					 $("#addRemoveDiv").css("top", parseInt($(".fakeContainer").css("top")) + parseInt($(".fakeContainer").css("height")));
			     else
			    	 $("#addRemoveDiv").css("top", parseInt($(".fakeContainer").css("top")) + parseInt($(".fakeContainer").css("height")) - 2);
				 
				 $("#addRemoveDiv").css("left", parseInt($(".fakeContainer").css("left")))
								   .css("width", parseInt($(".fakeContainer").css("width")));
				 
				 //For different language
				 $("#subfile_add").html(addBtnText);
				 $("#subfile_remove").html(removeBtnText);
				 
				 $(".fakeContainer").css("border-bottom","#316494 1px solid");
				 
				 //IGroup-937, in the firefox etc, this "this" refer to a JQuery object
				 var tempHeaderRows = this.headerRows;
				 //bind the click action for selection checkbox to ensure which remove button(enable or disable) to show
				 $(".sSky-Fixed").find("tr:not(:hidden):gt("+(this.headerRows-1)+")").each(function(){				
					$(this).find("td:first input[type='checkbox']").each(function(){	
							$(this).bind("click", function(){
								//decide if the remove action could be triggered
								/*IGroup-937, move actions to a new function
								var delAction = false;
								var fixedTable = $(".sSky-Fixed").find("tr:not(:hidden):gt("+(this.headerRows-1)+")");
								var rowNum = fixedTable.length;
									
								for(var i=rowNum-1; i>=0; i--){
									$(fixedTable.get(i)).find("td:first input[type='checkbox']").each(function(){			 	
										   if ($(this).is(":checked")){			    	
										    delAction = true;   
										   }
									});
								}
							
								if(delAction){
									$("#removeDiv").removeClass("sectionbtndisable");
									$("#removeDiv").addClass("sectionbutton");
								}else{
									$("#removeDiv").removeClass("sectionbutton");
									$("#removeDiv").addClass("sectionbtndisable");
								}
								*/
								
								changeRemoveBtnStatus(tempHeaderRows);	
							});   
						});

						$(this).find("td:first select").each(function(){	
							$(this).bind("click", function(){
								changeRemoveBtnStatus(tempHeaderRows);	
							});   
						}); 					    
						
						//IGroup-937
						changeRemoveBtnStatus(tempHeaderRows);	
				});				 
			}
			else if(this.popupAddRemoveBtn == 'Y')
			{			
				$(".fakeContainer").after(popupTableDiv);
				
				if(navigator.userAgent.indexOf("Firefox") > 0)
					$("#popupTableDiv").css("top", parseInt($(".fakeContainer").css("top")) + parseInt($(".fakeContainer").css("height")));
				else
					$("#popupTableDiv").css("top", parseInt($(".fakeContainer").css("top")) + parseInt($(".fakeContainer").css("height")) - 2);
				
				$("#popupTableDiv").css("left", parseInt($(".fakeContainer").css("left")))
								   .css("width", parseInt($(".fakeContainer").css("width")));	
				
				$(".fakeContainer").css("border-bottom","none");
			}			
	}
	
/////* Set up table scrolling and IE's onunload event for garbage collection */
	(function (st) {
		if (st.fixedCols > 0) {
			st.sData.onscroll = function () {
				st.sHeaderInner.style.right = st.sData.scrollLeft + "px";
				st.sFDataInner.style.top = (st.sData.scrollTop * -1) + "px";
			};
		} else {
			st.sData.onscroll = function () {
				st.sHeaderInner.style.right = st.sData.scrollLeft + "px";
			};
		}	
		if (/*@cc_on!@*/0) { /* Internet Explorer */
			window.attachEvent("onunload", function () {
				st.sData.onscroll = null;
				st = null;
			});
		}
		
	})(this);

	this.callbackFunc && this.callbackFunc();
};

var setDisabledMoreBtn = function(){	
	var moreBtnText;
	///modified to remove chinese language code for the I18N of integral.
	//if(pageLang == "ENG")
	//{
		moreBtnText	=	moreBtnText_ENG;
	//}
	/*else if(pageLang == "CHN")
	{	
		moreBtnText	=	moreBtnText_CHN;
	}*/

	$(".hasMore").html("<div class='more_disable'>" + moreBtnText + "</div>");
	
	//IGroup-1086 changes top position of disabled more button
	 if(navigator.userAgent.indexOf("Firefox") == -1 &&  
		navigator.userAgent.indexOf("Chrome") == -1)
	{
		$("#moreDiv").css("top", parseInt($(".fakeContainer").css("top")) - 24);
		$("#moreDiv").css("left", parseInt($(".fakeContainer").css("left")) + parseInt($(".fakeContainer").css("width")) - 88);
	}
}


/*
IGroup-937 Remove button can't be changed to available status in the following status
1. select one row
2. click search button and jump to another searching page
3. select one item and back, then although the checkbox was selected, but remove button is unavailable		
*/
function changeRemoveBtnStatus(headerRows)
{
	var delAction = false;
	var fixedTable = $(".sSky-Fixed").find("tr:not(:hidden):gt("+(headerRows-1)+")");
	var rowNum = fixedTable.length;
	
	for(var i=rowNum-1; i>=0; i--){
		$(fixedTable.get(i)).find("td:first input[type='checkbox']").each(function(){			 	
		    if ($(this).is(":checked")){			    	
		    	delAction = true;   
			}
		});
		
		$(fixedTable.get(i)).find("td:first select").each(function(){	
		    if ($(this).val() == '1'){			    	
		    	delAction = true;   
			}
		});			
	}
	
	if(delAction){
		$("#removeDiv").removeClass("sectionbtndisable");
		$("#removeDiv").addClass("sectionbutton");
	}else{
		$("#removeDiv").removeClass("sectionbutton");
		$("#removeDiv").addClass("sectionbtndisable");
	}	
}
