/* This function involved all the subfile table operations(add and remove row)
 * 
 * rows: the number of rows
 * isPageDown: variable to indicate whether current page is shown by clicking enter or page down
 * pageSize: number of rows in one subfile
 * fields: all the necessary fields that user must enter
 * subfile_table: subfile table id
 * subfile_add: add button id
 * subfile_remove: remove button id
 * divDelfields: clear the content of div when more control in one td
 * 
 * author: Wayne Yang 2010-02-01
 */
 

function operateTable(rows, isPageDown, pageSize, fields,subfile_table,subfile_add,subfile_remove){
	operateTableBase(rows, isPageDown, pageSize, fields,subfile_table,subfile_add,subfile_remove,null);

}
function operateTableWithDiv(rows, isPageDown, pageSize, fields,subfile_table,subfile_add,subfile_remove,divDelfields){
	operateTableBase(rows, isPageDown, pageSize, fields,subfile_table,subfile_add,subfile_remove,divDelfields);

}
function operateTableBase(rows, isPageDown, pageSize, fields,subfile_table,subfile_add,subfile_remove,divDelfields){
	
	var a = new Array(rows);
	var spaceLine = 0;
	$("#"+subfile_table).find("tr").each(function(i) {
		var isEmpty = true;
		
		// By default, search both input and div 
		var isInputFound = true;
		var isDivFound = true;
		for (var j=0;j<fields.length;j++){
			if (isInputFound){
				$(this).find("input[name*='"+fields[j]+"']").each(function (i){
					// later on, will only search input
					isInputFound = true;
					isDivFound = false;
					var value = $(this).val();
					value = $.trim(value);
					/* Francis 2010-4-7
					 * Fixed UI enhamecent bug 1084
					 * if backgroundColor is red, display the whole record.
					 */
					//update for new error style by Ai Hao 2010-08-01
					if(value != "" || $(this).hasClass("red")){
						isEmpty = false;
					}
				});
			}
			if (isDivFound){
				$(this).find("div[id*='"+fields[j]+"']").each(function (i){
					// later on, will only search div
					isInputFound = false;
					isDivFound = true;
					
					var value = $(this).html();
					value = $.trim(value);
					if (value == ""){
						//does nothing
					}else{
						isEmpty = false;
					}
				});
			}
			// if there is one field not empty, then the whole tr should be displayed
			if (isEmpty == false)
				break;
		}
		
		//modified by Ai Hao for bug#1259
		var index = i+1;
		
		if (isEmpty){
			a[index] = 0;
			//added for bug#1259
			spaceLine++;
		}else{
			a[index] = 1;
		}
		
		//modified by Ai Hao for bug#1259
		if (index == 1){
			a[1] = 1;
			return true;// continue the loop. By default, first "tr" is shown
		}		
		
		if (isPageDown == 1){
			if (index%pageSize == 1){
				a[index] = 1;
			}else if(pageSize == 1){
			    //added for bug#1178
				a[index] = 1;
				//added for bug#1259
				if (spaceLine > 1 && isEmpty) a[index] = 0;
			}
		}
		if (a[index] == 0){
			$(this).hide();
		}
	});
	
	$("#"+subfile_add).click(
		function() {
			var temp = 0;
			   
			for (var i=rows-1;i>=1;i--){ 
			    if (a[i] == 1){
			    	temp = i;  
			    	break;
			    }
			}
			
			var mustInput = false;
			$("#"+subfile_table).find("tr").each(function(k) {
				
				if (k+1 != temp)
					return true;
				var field;
				var discrField,discrName;
				for (var j=0;j<fields.length;j++){
					$(this).find("input[name*='"+fields[j]+"']").each(function (i){
						var value = $(this).val();
					       discrName = this.name+"discr";
						value = $.trim(value);
						if (value == ""){
							mustInput = true;							
							field=this;
						}
					});
					//check the field whether is the select control or not
					$(this).find("input[id='"+discrName+"']").each(function(i){
						discrName = "true";
						discrField = this;
						})
					
					//check the fields that user must input
					if (mustInput == true){//Francis 2010-2-9 for UI Enhancement.
						/*if (isChinese())
							alert("请输入必填字段");
						else
							alert("Field must be entered!");*/
						callCommonAlert(document.getElementById("lang1").value,"No00019");
						
						//$(field).css("background","red");
						//Set the font color by Michelle 2010-2-10 for UI Enhancement.
						//$(field).css("color","#FFFFFF");
						//update for new error style by Ai Hao 2010-08-01
						$(field).addClass("red");						
						$(field).focus();
						
						//check the select control that user must be chosed
						if(discrName=="true"){
				       	         //$(discrField).css("background","red");
				       	         //$(field).css("color","#FFFFFF");
								 //update for new error style by Ai Hao 2010-08-01
								 $(field).addClass("red");
				       	         $(field).focus();
				         	}
						break;
					}
				       
				}
				if (mustInput == true)
					return false;
				
				if (k+1 < rows-1){
					$(this).next().show();
 
				  a[k+1+1] = 1;	
				}else{
					// No more left
					doAction('PFKEY90');
				}
				return false;
			});
		}
	)

	
	//$("#"+subfile_remove).find("img").attr("src","/"+ contextPathName +"/screenFiles/remove" + langSuffix() + "_after.gif");
	$("#"+subfile_remove).find("img").attr("src","/"+ contextPathName +"/screenFiles/"+ imageFolder +"/remove_after.gif");

	$("#"+subfile_remove).click(
		function() {
			// first decide which to hide, by default is the last tr to hide.
			var t = rows - 1;
			for (var i=1;i<rows;i++){
				if (a[i] == 0){
					t = i-1;
					break;
				}
			}   
			var isSelected = false;	
			$("#"+subfile_table).find("tr").each(function(j) {
				// special case: if t== 1, means a[2]==0.Then there is only one row.
				// Can't delete the only one row.
				if (t == 1){
					// t == 1, then only one rwo is shown, when click delete, 
					// reset all values but don't hide the tr
					/*$(this).find("input[type='text']").each(function(i) {
						$(this).val("");
			    	});*/
		
				    
			    	return false;
				} 
				if (isSelected){
				
					var temp = new Array();
					var hidden_temp=new Array();
					
					/* Francis 2010-2-9 for UI Enhancement. 
					 * support checkbox checked status
					 * Finding input fields that all types instead of type="text" fields.
					 * This solutions can cover values of all input fileds of pre line.
					 */
					
		    		
					$(this).prev().find("input").each(function(i) {
						$(this).val(temp[i]);
		    		});
		    		
		    		if (j+1 == t){
		    			//recover background style with text control in a "tr".
		    			//Francis 2010-2-9 for UI Enhancement.
		    			$(this).find("input[type='text']").each(function(i) {
		    				$(this).css("background","white");
			    		});
						$(this).hide();
						a[t] = 0;
						return false;
					}
				}else{
	 	 	 	 	$(this).find("input[type='radio']").each(function(i) {
						if ($(this).is(":checked")){
							isSelected = true;
							return false;
						}
					});
					if (isSelected){
					// If deleting last row, then no need to switch data with others,just delete them all and hide
						if (j+1 == t){
						/* support checkbox checked status*/
							$(this).find("input").each(function(i) {
								$(this).val("");
								if($(this).attr("checked")){//Francis 2010-2-9 for UI Enhancement.
									$(this).removeAttr("checked");
								}
				    		});
							/*$(this).find("input[type='text']").each(function(i) {
								alert("id==="+this.id+" name==="+this.name+"  value="+this.value);								
								$(this).val("");
				    		});*/
				    	//recover background style with text control in a "tr".
		    			//Francis 2010-2-9 for UI Enhancement.
			    			$(this).find("input[type='text']").each(function(i) {
			    				$(this).css("background","white");
				    		});
							$(this).hide();
							a[t] = 0;
							return false;
						}
					}
				}
					 
			});
			
			/*
			 * Set remove button disabled 
			 * Francis 2010-3-19 for UI Enhancement.
			 */
			//$(this).find("img").attr("src","/"+ contextPathName +"/screenFiles/remove" + langSuffix() + "_after.gif");
			$(this).find("img").attr("src","/"+ contextPathName +"/screenFiles/"+ imageFolder +"/remove_after.gif");
			
			//deleted for checkbox by Michelle
			var trdel = false;
			var isText = false;
			var trDisplay = $("#"+subfile_table).find("tr:not(:hidden)");
			var len = trDisplay.length;
			var tdArray = new Array();
			var tdindex = 0;
			for(var i=len-1;i>=0;i--){
			 trdel = false;
			
			 $(trDisplay.get(i)).find("td:first input[type='checkbox']").each(function(n){
			 	
			    if ($(this).is(":checked")){			    	
			       trdel = true;
			      
			     }
			 })
			 
			 if(trdel){
			    //clear each content of td.
			     $(trDisplay.get(i)).find("td").each(function(m){
			      isText = false;
			      $(this).find("input").each(function(o){
			 	   	 $(this).val("");			 	   				 	   	
			 	   	if($(this).attr("type")=="checkbox"||$(this).attr("type")=="radio") {		 	   		
			 	   	        $(this).attr("checked","");			 	   
			 	   	}
			 	        
			 	       else if($(this).attr("type")=="text"){
			 	   	        $(this).css("backgroundColor","#FFFFFF");
			 	   	        $(this).css("color","#000000");
			 	   	}
			 	   	
			 	   	if(this.id.indexOf("discr")!=-1){this.focus();$(this).blur();}
			 	   	 isText = true;
			 	   	})			
			 	  if(!isText) $(this).html(""); 
			 	  if(divDelfields!=null){
			 	    for(var divDelI = 0; divDelI<divDelfields.length;divDelI++){
			 	    	  $(this).find("div[id*='"+divDelfields[divDelI]+"']").text("");
			 	    	}
			 	  }
			    })
			    
			    if(i==len-1) {
			    $(trDisplay.get(i)).css("display","none");
                            a[i+1]=0;
                            len--;			    
			    continue;
			   }
		         
               //move the data and the css of each control         
			  for(var j=i;j<len-1;j++){
			    tdArray = new Array();
			    tdindex = 0;
		            
			    $(trDisplay.get(j+1)).find("td").each(function(m){
			      isText = false;
			      $(this).find("input[type='text']").each(function(o){
			 	   	tdArray.push($(this).val());			  
			 	        tdArray.push($(this).css("backgroundColor"));
			 	   	$(this).val("");		
			 	   	$(this).css("backgroundColor","#FFFFFF");
			 	        $(this).css("color","#000000");	 
			 	        if(this.id.indexOf("discr")!=-1){this.focus();$(this).blur();}	   				 	   	
			 	   	 isText = true;
			 	   	})
			      $(this).find("input[type='hidden']").each(function(o){
			 	   	tdArray.push($(this).val()); 
			 	   	$(this).val("");		
			 	   	isText = true;			 	   				 	   				 	   	
			 	   	})  
			      $(this).find("input[type='checkbox']").each(function(o){
			 	   	tdArray.push($(this).attr("checked"));
			 	   	
			 	   	 $(this).attr("checked","");
			 	   	
			 	   	isText = true;			 	   				 	   				 	   	
			 	   	})   
			     $(this).find("input[type='radio']").each(function(o){
			 	   	tdArray.push($(this).attr("checked")); 
			 	   	
			 	   	 $(this).attr("checked","");
			 	   	 $(this).css("backgroundColor","#FFFFFF");
			 	   	 $(this).css("color","#000000");
			 	   	isText = true;			 	   				 	   				 	   	
			 	   	}) 
			 	   	  						
			     if(!isText) {tdArray.push($(this).html()); $(this).html("");}
			     
			     if(divDelfields!=null){
			 	    for(var divDelI = 0; divDelI<divDelfields.length;divDelI++){
			 	    	tdArray.push($(this).find("div[id*='"+divDelfields[divDelI]+"']").text());
			 	    	$(this).find("div[id*='"+divDelfields[divDelI]+"']").text("");
			 	    	  
			 	    	}
			 	  }
			     			     
			    })
			   	    
			     $(trDisplay.get(j)).find("td").each(function(m){
			       isText = false;			    
			      $(this).find("input[type='text']").each(function(o){			      	      	
			 	   	$(this).val(tdArray[tdindex]); 
			 	   	tdindex++;
			 	   	
			 	   	if(tdArray[tdindex]=="red" || tdArray[tdindex]=="#FF0000" || tdArray[tdindex]=="#ff0000"){
			 	   	 $(this).css("backgroundColor",tdArray[tdindex]);
			 	   	 $(this).css("color","#FFFFFF");
			 	   	}
			 	   	 tdindex++;		 	   				 	   	
			 	   	 isText = true;
			 	   	})
			      $(this).find("input[type='hidden']").each(function(o){
			 	   	$(this).val(tdArray[tdindex]); 
			 	   	 tdindex++;
			 	   	isText = true;			 	   				 	   				 	   	
			 	   	})  
			      $(this).find("input[type='checkbox']").each(function(o){
			 	   	$(this).attr("checked",tdArray[tdindex]); 
			 	   	
			 	   	 tdindex++;
			 	    	
			 	   	isText = true;			 	   				 	   				 	   	
			 	   	})   
			     $(this).find("input[type='radio']").each(function(o){
			 	   	$(this).attr("checked",tdArray[tdindex]); 
			 	   	 tdindex++;
			 	 
			 	   	isText = true;			 	   				 	   				 	   	
			 	   	})   	
					
			     if(!isText) {$(this).html(tdArray[tdindex]);  tdindex++;}
			     
			     
			     if(divDelfields!=null){
			 	    for(var divDelI = 0; divDelI<divDelfields.length;divDelI++){
			 	    	$(this).find("div[id*='"+divDelfields[divDelI]+"']").text(tdArray[tdindex]);
			 	    	tdindex++; 
			 	    	}
			 	  }
			    
			    })
			     
			    
			   }
			   $(trDisplay.get(len-1)).css("display","none");
			    a[len] = 0;	
			    len--;
			  }
			
			}
                    if($("#"+subfile_table).find("tr:not(:hidden)").length==0){
                        $("#"+subfile_table).find("tr:first").css("display","");
                        a[1]=1;
                     }
		
	
			
		}
	)
//Michelle 2010-4-2

     $("#"+subfile_table).find("tr").each(function(o){
     	  $(this).find("td:first").each(function(i){
     	  	$(this).find("input[type='checkbox']").each(function(n){
     	        $(this).bind("click",function(){setRemoveStyle(subfile_table,subfile_remove);});
     	    })
              
           })
     	});
	
	}

/* This function support subfile table remove button disable/enable.
 * 
 * subfile_table: subfile table id
 * subfile_remove: remove button id
 * 
 * author: Francis Wang 2010-03-24
 */
function setRemoveStyle(subfile_table,subfile_remove){
	 var delCheck=false;
	 $("#"+subfile_table).find("tr").each(function(m){
		 $(this).find("td:first input[type='checkbox']").each(function(i){
		    if ($(this).is(":checked")){
		       delCheck = true;
		    }	
		 })
	 })
	 if(delCheck){
	 	//$("#"+subfile_remove).find("img").attr("src","/"+ contextPathName +"/screenFiles/remove" + langSuffix() + ".gif");	 
		 $("#"+subfile_remove).find("img").attr("src","/"+ contextPathName +"/screenFiles/"+ imageFolder +"/remove.gif");
	 }else{
	 	$("#"+subfile_remove).find("img").attr("src","/"+ contextPathName +"/screenFiles/"+ imageFolder +"/remove_after.gif");
	 	$("#"+subfile_remove).removeAttr("onclick");
	 }
}
/* To decide the picutre suffix according to the language
 * Wayne Yang 2010-04-22
 */
function langSuffix(){
	var lang = $("#lang1");
	if (lang && ($.trim(lang.val()) == "CHI/"))
		return "_cn";
	return "";
}

/* To decide the language
 * Wayne Yang 2010-04-22
 */
function isChinese(){
	var lang = $("#lang1");
	if (lang && ($.trim(lang.val()) == "CHI/"))
		return true;
	return false;
}