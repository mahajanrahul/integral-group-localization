/* 
 * This function involved all the text area operations.
 * 
 * author: Francis Wang 2010-8-3
 */
function getLines(txtArea) 
{
  var lineHeight = parseInt(txtArea.style.lineHeight.replace(/px/i,''));  
  var tr = txtArea.createTextRange();
  return Math.ceil(tr.boundingHeight/lineHeight);
}
function checkLimits(txtArea,totalLength)
{
  var maxLines = txtArea.rows;
  var maxChars = totalLength+2*(maxLines-1);//txtArea.rows * txtArea.cols;
  
 if((getLines(txtArea) > maxLines) ||((getLines(txtArea) == maxLines)&&(window.event.keyCode == 10 || window.event.keyCode == 13) )   )
  {
      txtArea.value = txtArea.value.substr(0,txtArea.value.length);
      callCommonAlert(document.getElementById("lang1").value,"No00034");
  }
 
  else if(txtArea.value.length > maxChars )
  {
      txtArea.value = txtArea.valuesubstr;
      callCommonAlert(document.getElementById("lang1").value,"No00035");
  }
}
/*function isChinese(str)
{
    var lst = /[u00-uFF]/;       
    return !lst.test(str);      
}
function strLen(str1)
{
    var i,str1,str2,str3,nLen;
    //str1 = s.value;
    nLen = 0;
    for(i=1;i<=str1.length;i++)
    {
          str2=str1.substring(i-1,i)
          str3=escape(str2);
          if(str3.length>3)
          {
               nLen = nLen + 2;
          }
          else
          {
               nLen = nLen + 1;
          }
     }
     return nLen;
}*/

function dospace(txtArea,maxLength){
	if ( window.event.keyCode == '9' //tab
		   || window.event.keyCode == '8'//BackSpace
			   || window.event.keyCode == '16'//Shift 16 
		   || window.event.keyCode == '46'//Delete   
		   || window.event.keyCode == '20' //caps lock
		   || window.event.keyCode == '35' // home
		   || window.event.keyCode == '36' // end
		   || window.event.keyCode == '37' //left arrow
		   || window.event.keyCode == '38' //up arrow
		   || window.event.keyCode == '39' //right arrow
		   || window.event.keyCode == '40' //down arrow
			   //|| window.event.keyCode == '13' //Enter
				 //  || window.event.keyCode == '108' //Enter digital keyword
		    ) {
		return true;
	}

	var txt=txtArea.value;
	var data = txt.replace(/[^\x00-\xff]/g,"aa");//add by Francis for multi-language issue.
	var fields=data.split("\r\n");
	var currentRow=getCursorPosition();
	
	if((currentRow==fields.length) && (currentRow<txtArea.rows)){
		/*if(strLen(fields[currentRow-1])>=maxLength){
			$(txtArea).val(txt+"\r\n");
			return true;
		}*/
		if(fields[currentRow-1].length==maxLength){
			$(txtArea).val(txt+"\r\n");
			return true;
		}else if(fields[currentRow-1].length>maxLength){
			$(txtArea).val(txt.substr(0,txt.length-1)+"\r\n"+txt.substr(txt.length-1));
			return true;
		}
	}else if(((currentRow<fields.length) && (currentRow<txtArea.rows))||(currentRow==txtArea.rows)){
		if(fields[currentRow]!=null && fields[currentRow].length==0){
			return true;
		}
		if(fields[currentRow-1].length>=maxLength){
			event.returnValue = false;
			return true;
		}
	}
		
}
function splitText(txtArea,screenFields){

	var screenFields=screenFields.split('-');
	var fields=txtArea.value.split('\r\n');
	for(i=0;i<screenFields.length;i++){
		$("#"+screenFields[i]).val("");
	}
	
	for(i=0;i<fields.length;i++){
		$("#"+screenFields[i]).val(fields[i]);
	}
	
}

function getCursorPosition(){
	var src = event.srcElement;
	var oTR = src.createTextRange();
	var oSel = document.selection.createRange();
	var textLength = src.innerText.length;
	var line, chars, total, cl
	oTR.moveToPoint(oSel.offsetLeft, oSel.offsetTop)
	oTR.moveStart("character", -1*textLength)
	cl = oTR.getClientRects()
	line = cl.length-1
	total = oTR.text.length
	oTR.moveToPoint(cl[cl.length-1].left, cl[cl.length-1].top)
	oTR.moveStart("character", -1*textLength)
	chars = total - oTR.text.length
	if (oSel.offsetTop != cl[cl.length-1].top) {line++; chars = 0}
	else if (src.createTextRange().text.substr(oTR.text.length, 2) == "\r\n") chars -= 2
	return line;
}
