var GLB_FIRST_FLAG=false;
/**
 * This file contains most functions of dropdown list.
 * Dropdown list is a standard widget used by system,
 * It support dynamic search and edit.
 * @author shawn.wang
 * create date:2010-04-22 
 */
//This function will display the select box
function displaySearchField(data){
	//Hidden all select div @Michelle 091011
	f4fieldn=data+"dropdown";
	var discrName=data+'discr';//Get input box position to deal with widget in the table by shawn
	var dropdownobj=document.getElementById(f4fieldn);
	var language=$("#lang1").val();//language information to deal with drop down list height. 
	if(dropdownobj.style.visibility == "hidden"){
// fix bug IPNC-825	
//		$("select").each(function(){
//			var idt=$(this).attr("id");
//			if(idt.indexOf("back")<1){
//				$(this).context.parentNode.parentNode.style.visibility='hidden';
//			}
//		});		   
		dropdownobj.style.visibility="visible";
		var dropdownsize=dropdownobj.lastChild.lastChild.size;
		
		var dropdownheight=parseInt(dropdownsize)*15;
		if(language.indexOf("CHI")!=-1){
			dropdownheight=parseInt(dropdownsize)*12;
		}
		//Dealwith table dropdown. 	
		var newtop=getElementPos(discrName).y;
		var newleft=getElementPos(discrName).x;
		if(parseInt(newtop)+parseInt(dropdownheight)>510){
			newtop=parseInt(newtop)-parseInt(dropdownheight)-30;			  
		}
		dropdownobj.style.top=parseInt(newtop)+20;
		dropdownobj.style.left=parseInt(newleft);
		dropdownobj.style.zIndex=99;
		dropdownobj.style.position="absolute";
		var newdropdownobj=$("div[id='"+f4fieldn+"']").clone(true);
		$("div[id='"+f4fieldn+"']").remove();
		$("#mainDiv").append(newdropdownobj);
		document.getElementsByTagName("body")[0].onclick=function(){closeDiv(f4fieldn);}
		$("select[id='"+f4fieldn+"s']").bind("keydown",function(){
			if(event.keyCode=='13') {
				var selectobj=document.getElementById(data+'dropdowns');

				if(selectobj.parentElement.nodeName =="A"){
					if(selectobj.parentElement.outerHTML.indexOf("hideItemDiv")>0){
						hideItemDiv(document.getElementById(data+'dropdowns'));	
					}
					else if(selectobj.parentElement.outerHTML.indexOf("hidecodeItemDiv")>0){
						var discfildname=selectobj.parentElement.getAttributeNode("onclick").nodeValue;
						var comaposition=discfildname.lastIndexOf(',');
						var kposition=discfildname.lastIndexOf(')');
						discfildname=discfildname.substring(comaposition+3,kposition-1);
						hidecodeItemDiv(document.getElementById(data+'dropdowns'),data+'discr',discfildname);//code description type
					}
				}
				event.keyCode=='9'
					return false;
			}	
		});
	} else {
		closeDiv(f4fieldn);
	}

}
//This function get the element position, return horizental and vertical position. 
function getElementPos(elementId) { 
	var ua = navigator.userAgent.toLowerCase(); 
	var isOpera = (ua.indexOf('opera') != -1); 
	var isIE = (ua.indexOf('msie') != -1 && !isOpera); // not opera spoof 
	var el = document.getElementById(elementId); 
	if(el.parentNode === null || el.style.display == 'none') { 
		return false; 
	}       
	var parent = null; 
	var pos = [];      
	var box;      
	if(el.getBoundingClientRect)    //IE 
	{          
		box = el.getBoundingClientRect(); 
		var scrollTop = Math.max(document.documentElement.scrollTop, document.body.scrollTop); 
		var scrollLeft = Math.max(document.documentElement.scrollLeft, document.body.scrollLeft); 
		return {x:box.left + scrollLeft, y:box.top + scrollTop}; 
	}else if(document.getBoxObjectFor)    // gecko     
	{ 
		box = document.getBoxObjectFor(el);  
		var borderLeft = (el.style.borderLeftWidth)?parseInt(el.style.borderLeftWidth):0;  
		var borderTop = (el.style.borderTopWidth)?parseInt(el.style.borderTopWidth):0;  
		pos = [box.x - borderLeft, box.y - borderTop]; 
	} else    // safari & opera     
	{ 
		pos = [el.offsetLeft, el.offsetTop];  
		parent = el.offsetParent;      
		if (parent != el) {  
			while (parent) {  
				pos[0] += parent.offsetLeft;  
				pos[1] += parent.offsetTop;  
				parent = parent.offsetParent; 
			}  
		}    
		if (ua.indexOf('opera') != -1 || ( ua.indexOf('safari') != -1 && el.style.position == 'absolute' )) {  
			pos[0] -= document.body.offsetLeft; 
			pos[1] -= document.body.offsetTop;          
		}     
	}               
	if (el.parentNode) {  
		parent = el.parentNode; 
	} else { 
		parent = null; 
	} 
	while (parent && parent.tagName != 'BODY' && parent.tagName != 'HTML') { // account for any scrolled ancestors 
		pos[0] -= parent.scrollLeft; 
		pos[1] -= parent.scrollTop; 
		if (parent.parentNode) { 
			parent = parent.parentNode; 
		} else { 
			parent = null; 
		} 
	} 
	return {x:pos[0], y:pos[1]}; 
} 


//This function will hidden the <div>
function closeDiv(data){
	if(document.getElementById(data).style.visibility=="visible")
		document.getElementsByTagName("body")[0].onclick=function(){this.document.getElementById(data).style.visibility="hidden";}
}
//When user input some thing in the input box, the options of select box will adjust and 
//display what user want.		
function adjustitem(itemName){
	 if ( window.event.keyCode == '9' //tab
  || window.event.keyCode == '13' //enter
  || window.event.keyCode == '16' //shift
  || window.event.keyCode == '17' //ctrl
  || window.event.keyCode == '18' //alt
  || window.event.keyCode == '20' //caps lock
  || window.event.keyCode == '27' //esc
  || window.event.keyCode == '33' //page up
  || window.event.keyCode == '34' //page down
  || window.event.keyCode == '35' //end
  || window.event.keyCode == '36' //home
  || window.event.keyCode == '37' //left arrow
  || window.event.keyCode == '39' //right arrow
  || window.event.keyCode == '45' //print screen
  || window.event.keyCode == '93' //windows right-click
  || window.event.keyCode == '96' //ins key
  || (window.event.keyCode >= '112' && window.event.keyCode <= '123')   //down arrow
  || window.event.keyCode == '144' //num lock
  || window.event.keyCode == '145' //pause
  || lastKey              == '18' //alt keys
   )
  return true;
	 var found=false;
	 var selectName=itemName+'dropdowns';
	 var discrName=itemName+'discr';
	 var nowValue=document.getElementById(discrName).value;

	 nowValue=nowValue+"";
	 nowValue=nowValue.trim();
	 //Added by shawn to deal with delete item code issue
	 //@091201
	 var selectTag=document.getElementById(selectName);   

	 var colls=selectTag.options;
	 var slength=colls.length;
	 //alert(slength);
	 //alert(colls(0).text.indexOf('O'));

	 if(nowValue==""){
		 found=true;
		 if(colls(0)){
			 colls(0).selected=true;//If no item had been entered, the first item will be selected as default.
		 }
		 displaySearchField(itemName);
		 document.getElementById(itemName).value="";
		 //When press up or down arrow to let the select box get focus by shawn
		 if(window.event.keyCode == '38' //up arrow
			 || window.event.keyCode == '40' //down arrow
		 ){
			 document.getElementById(selectName).focus ();
		 }
		 return;
	 }

	 //When press up or down arrow to let the select box get focus by shawn
	 if(window.event.keyCode == '38' //up arrow
		 || window.event.keyCode == '40' //down arrow
	 ){
		 document.getElementById(selectName).focus ();
	 }
	 var valuePos=0;
	 for(var i=0;i<slength;i++){
		 if(Number(colls(i).text.indexOf(nowValue))==0){
			 colls(i).selected = true;
			 displaySearchField(itemName);
			 found=true;
			 document.getElementById(discrName).style.color='#434343';
			 return false;
		 }
	 }
	 if(!found){
		 document.getElementById(discrName).style.color='red';
	 }
}
//This function will fill the input box with the value which user 
//select and close the select box.
function hideItemDiv(selectfield){
	if(f4fieldn){
		var fieldBoxName=f4fieldn.substring(0,f4fieldn.length-8);
		var discrBoxName=fieldBoxName+"discr";
		var selectValue=selectfield.value;
		if(selectValue.indexOf('^')>0){
			var code=selectValue.substring(0,selectValue.indexOf('^'));
			var discription=selectValue.substring(code.length+1);
			document.getElementById(fieldBoxName).value=$.trim(code);
			document.getElementById(discrBoxName).value=discription;
			document.getElementById(discrBoxName).style.color='#434343';
		} else {
		    var description = selectfield.options[selectfield.selectedIndex].text;
            document.getElementById(fieldBoxName).value= selectValue;
            document.getElementById(discrBoxName).value= description;
            document.getElementById(discrBoxName).style.color='#434343';
		}
		document.getElementById(f4fieldn).style.visibility='hidden';
	}else{
		return false;
	}
}
// Add by yy IGroup-937
function hideItemDivHtml(selectfield){
	var fieldBoxName=selectfield.id.replace("dropdowns","");
	var discrBoxName=fieldBoxName+"discr";
	var selectValue=selectfield.value;
	if(selectValue.indexOf('^')>=0){
		var code=selectValue.substring(0,selectValue.indexOf('^'));
		var discription=selectValue.substring(code.length+1);
		document.getElementById(fieldBoxName).value=$.trim(code);
		document.getElementById(discrBoxName).value=discription;
		//document.getElementById(discrBoxName).style.color='#434343';
	} else {
		if(selectfield.selectedIndex!=-1){
			var description = selectfield.options[selectfield.selectedIndex].text;
			document.getElementById(fieldBoxName).value= selectValue;
			document.getElementById(discrBoxName).value= description;
		}
        //document.getElementById(discrBoxName).style.color='#434343';
	}
}
//find the description according to user input.
//This function is not use in the 3.1 system.
function findDiscr(field){

	var fieldDropdown=field+"dropdown";
	var fieldDisc=field+"discr";
	var selectField=field+"dropdowns";
	var found=false;
	if(document.getElementById(fieldDropdown)==null){
		return false;
	}
	if(document.getElementById(selectField)==null){
		return false;
	}
	if(document.getElementById(selectField).options==null){
		return false;
	}
	var itemLength=document.getElementById(selectField).options.length;
	var itemco=document.getElementById(field).value;
	for(i=0;i<itemLength;i++){
		var opv=document.getElementById(selectField).options[i].value;
		var op=opv.substring(0,opv.indexOf("^"));
		if(op==itemco){
			found=true;
			document.getElementById(fieldDisc).value=opv.substring(itemco.length+1);
		}
	}
	if(!found){
		if(itemco.Trim()==""){
			document.getElementById(fieldDisc).value="";
			return false;
		}
		document.getElementById(fieldDisc).value="Invalid value!";
	}		
}

//Set ---select-- to input box when there is no value.
function getDefault(thi,width,longdesc){
	var language=$("#lang1").val();
	var currentValue = thi.value;
	var defaultValue = "";
	var endCount;

	if (width > 0){
		if(width<90){
			defaultValue = "-" + document.getElementById("dropSelect").value + "-";

		}else{
			var slnumber=0;
			var sl ="";
			var numberCode = getValue("fontHight");
			slnumber=parseInt((width-50)/numberCode);
			for(var i=0;i<slnumber;i++){
				sl += "-";
			}
			defaultValue = sl + "-" + document.getElementById("dropSelect").value + "-" + sl;
		}
	}else{
		if(longdesc){
			defaultValue = "-------------------" + document.getElementById("dropSelect").value + "-------------------";
		}else{
			defaultValue = "--" + document.getElementById("dropSelect").value + "--";
		}
	}
	if(currentValue.trim() == "") {
		thi.value = defaultValue;
	}

	// When the input be set up nothing or default value, the real control will also be set nothing (Michelle Ma)
	if(thi.value == ""||thi.value == defaultValue){

		endCount = thi.id.indexOf("discr");

		if(endCount!=-1){
			try{
				document.getElementById(thi.id.substring(0,endCount)).value="";
			}catch(e){}
		}
	}
}
//add by Cary zhong 07/11/2010 start
function getValue(code){
	return eval(code + document.getElementById("lang1").value.substr(0,3));
}
function getDefaultOne(thi){
	var currentValue = thi.value;
	defaultValue = "-------------------" + document.getElementById("dropSelect").value + "-------------------";;
	thi.value = defaultValue;
}
function getDefaultTwo(thi,width){
	var defaultValue = "";
	if(width<90){
		defaultValue = "-" + document.getElementById("dropSelect").value + "-";
	}else{
		var slnumber=0;
		var sl ="";
		var numberCode = getValue("fontHight");
		slnumber=parseInt((width-50)/numberCode);
		for(var i=0;i<slnumber;i++){
			sl += "-";
		}
		defaultValue = sl + "-" + document.getElementById("dropSelect").value + "-"+ sl;
	}
	thi.value = defaultValue;
}
function getDefaultThree(thi,width,longdesc){
	var defaultValue = "";

	if (width > 0){
		if(width<90){
			defaultValue = "-" + document.getElementById("dropSelect").value + "-";
		}else{
			var slnumber=0;
			var sl ="";
			var numberCode = getValue("fontHight");
			slnumber=parseInt((width-50)/numberCode);
			for(var i=0;i<slnumber;i++){
				sl += "-";
			}
			defaultValue = sl + "-" + document.getElementById("dropSelect").value + "-"+ sl;
		}
	}else{
		if(longdesc){
			defaultValue = "-------------------" + document.getElementById("dropSelect").value + "-------------------";;
		}else{
			defaultValue = "--" + document.getElementById("dropSelect").value + "--";
		}
	}
	thi.value = defaultValue;
}
//add by Cary zhong 07/11/2010 end
function setDefault(thi,width,longdesc){
	var language=$("#lang1").val();
	var currentValue = thi.value;
	var defaultValue = "";
	var discname=thi.id;
	var fildname=discname.substring(0,discname.length-5);

	if(window.event.keyCode=='13'){
		//hidecodeItemDiv(document.getElementById('occpcddropdowns'), 'occpcddiscr', 'occupn');
		var selectobj=document.getElementById(fildname+'dropdowns');

		if(selectobj.parentElement.nodeName =="A"){
			if(selectobj.parentElement.outerHTML.indexOf("hideItemDiv")>0){
				hideItemDiv(document.getElementById(fildname+'dropdowns'));	
			}
			else if(selectobj.parentElement.outerHTML.indexOf("hidecodeItemDiv")>0){
				var discfildname=selectobj.parentElement.getAttributeNode("onclick").nodeValue;
				var comaposition=discfildname.lastIndexOf(',');
				var kposition=discfildname.lastIndexOf(')');
				discfildname=discfildname.substring(comaposition+3,kposition-1);
				hidecodeItemDiv(document.getElementById(fildname+'dropdowns'),fildname+'discr',discfildname);//code description type
			}
		}

		window.event.keyCode='9';
		return false;
	}

	if (width > 0){
		if(width<90){
			defaultValue = "-" + document.getElementById("dropSelect").value + "-";
		}else{
			var slnumber=0;
			var sl ="";
			var numberCode = getValue("fontHight");
			slnumber=parseInt((width-50)/numberCode);
			for(var i=0;i<slnumber;i++){
				sl += "-";
			}
			defaultValue = sl + "-" + document.getElementById("dropSelect").value + "-"+ sl;
		}
	}else{
		if(longdesc){
			defaultValue = "-------------------" + document.getElementById("dropSelect").value + "-------------------";;
		}else{
			defaultValue = "--" + document.getElementById("dropSelect").value + "--";
		}
	}
	if(currentValue == defaultValue || currentValue.indexOf(document.getElementById("dropSelect").value) != -1) {
		thi.value = "";   
	}
	//When the input box is clicked also display the dropdown list. Modified by shawn.
	if(discname){
		if(window.event.keyCode=='0'){
			adjustitem(fildname);
		}
	}	
}

//This function deal with code description dropdown.
function hidecodeItemDiv(selectfield,codeboxname,descboxname){

	var dropdownlistid=selectfield.id;

	var fieldBoxName=dropdownlistid.substring(0,dropdownlistid.length-9);
	var discrBoxName=descboxname;
	var selectValue=selectfield.value;
	if(selectValue.indexOf('--')>0){
		var code=selectValue.substring(0,selectValue.indexOf('^'));
		var discription=selectValue.substring(code.length+code.length+3);
		document.getElementById(fieldBoxName).value=$.trim(code);
		document.getElementById(codeboxname).value=$.trim(code);
		if(discrBoxName!=null){//no description box had been offered.
			//document.getElementById(discrBoxName).style.color="#434343";
			document.getElementById(discrBoxName).value=discription;
		}
	} else {
	    // Fix for FSU screen. The description doesn't have ^ character.
        var description = selectfield.options[selectfield.selectedIndex].text;
        document.getElementById(fieldBoxName).value = selectValue;
        if (codeboxname != null) {
            document.getElementById(codeboxname).value = selectValue;
        }
        if (discrBoxName != null) {
            document.getElementById(discrBoxName).value = description;
        }
	}
	document.getElementById(fieldBoxName+"dropdown").style.visibility='hidden';
}

// This function deal with code description dropdown.
// create by yy for standardize dropdown, IGroup-937
function reWriteDescDropdown(selectfield,inputBoxId){
	var dropdownlistid=selectfield.id;
    var description;
    if(selectfield.value == ""){
    	description = "";
    }else{
    	description = selectfield.options[selectfield.selectedIndex].text;
    	description = description.replace(selectfield.value+"--","");
    }
	inputBoxId.value=description;
}

//JavaScript Document
jQuery.dynamicfilter=function(e){
	//The parameters should be 2D array,the outer array is all the dropdown fields which need dynamic filter.
	// the inner array is used to wrap one dropdown information
	$.each(e,function(i,n){
		//parse outer array
		var dropdownfieldname;
		var filtercodes="";//To store the filters.
		var dropdownfieldvalue="";
		var filterfields=new Array();
		$.each(n,function(i,n){
			//parse inner array, retrieve all the information we need.
			if(i==0){
				//this is dropdown fields name
				dropdownfieldname=n;
			}else{
				//these are filter names
				//when output, the input box will not exist.
				if($("input[name='"+n+"']").length!=0){

					filtercodes+=$("input[name='"+n+"']").val();
				}else{
					filtercodes+=$("div[id='"+n+"_div']").text();
				}
				filterfields[i-1]=n;				
			}
		});
		if($("input[name='"+dropdownfieldname+"']").length==0){//which means the page is in output state
			if($.trim($("div[id='"+dropdownfieldname+"_div']").text())!=""){
				var outputcode=$("div[id='"+dropdownfieldname+"_div']").text();
				$("select[id='"+dropdownfieldname+"dropdowns'] option").each(function(){ 
					var valu=$(this).val();
					var codep=valu.indexOf('^');
					var codev=valu.substring(0,codep);
					if(codev==$.trim(filtercodes+outputcode)){
						//geolocndiscr
						var disc=valu.substring(codep+1);
						$("div[id='"+dropdownfieldname+"discr']").text(disc);
						return true;

					}

				});

			}
			return true;
		}

		//backup original dropdownlist.
		var dropdownobjbackup=$("select[id='"+dropdownfieldname+"dropdowns']").clone(true);
		dropdownobjbackup.attr("id",dropdownfieldname+"dropdownsback");
		$("#mainDiv").append(dropdownobjbackup);
		dropdownobjbackup.hide();
		//clean all the options for this dropdown.
		$("select[id='"+dropdownfieldname+"dropdowns'] option").each(function(){
			$(this).remove();
		});
		// Add by yy
		$("select[id='"+dropdownfieldname+"dropdowns']")[0].options[0] = new Option("---------Select---------", "");
		dropdownfieldvalue=$("input[name='"+dropdownfieldname+"']").val();

		if($.trim(dropdownfieldvalue)!=""){
			//First if dropdown code is not space, fill the discription box
			$("select[id='"+dropdownfieldname+"dropdownsback'] option").each(function(){ 
				var valu=$(this).val();
				var codep=valu.indexOf('^');
				var codev=valu.substring(0,codep);
				if(codev==($.trim(filtercodes+dropdownfieldvalue))){
					//geolocndiscr
					var disc=valu.substring(codep+1);
					$("input[id='"+dropdownfieldname+"discr']").val(disc);
				}

			});
		}

		//remove original action;
		$("a[id='"+dropdownfieldname+"f4']").removeAttr("onclick");

		$("input[id='"+dropdownfieldname+"discr']").change(function(){
			filtercodes="";							   
			$.each(filterfields, function(i,n){
				filtercodes+=$("input[name='"+n+"']").val();

			});
			if(!GLB_FIRST_FLAG){
				$("input[id='"+dropdownfieldname+"']").val("");
			}
			if($.trim(filtercodes)==""){
				//First clear the original dropdown list
				$("select[id='"+dropdownfieldname+"dropdowns'] option").each(function(){
					$(this).remove();
				});
				// Add by yy
				$("select[id='"+dropdownfieldname+"dropdowns']")[0].options[0] = new Option("---------Select---------", "");
				$("input[id='"+dropdownfieldname+"']").val("");
				return false;
			}else{
				//filter the dropdown options
				var size=0;
				//filter the dropdown options
				//First clear the original dropdown list
				$("select[id='"+dropdownfieldname+"dropdowns'] option").each(function(){
					$(this).remove();
				});
				// Add by yy
				$("select[id='"+dropdownfieldname+"dropdowns']")[0].options[0] = new Option("---------Select---------", "");
				$("select[id='"+dropdownfieldname+"dropdownsback'] option").each(function(){ 
					var valu=$(this).val();
					var codep=valu.indexOf('^');
					var codev=valu.substring(0,codep);

					if(codev.indexOf(filtercodes)==0){			
						var newc=codev.substring(filtercodes.length);
						var newtitle=valu.substring(codep+1);
						var newval=newc+valu.substring(codep);
						$("<option title='"+newtitle+"' value='"+newval+"'>"+newtitle+"</option>").appendTo($("select[id='"+dropdownfieldname+"dropdowns']"));
						size+=1;
					}

				});
				//if(size<2){size=2;}
				//$("select[id='"+dropdownfieldname+"dropdowns']").attr("size",size);
				displaySearchField(dropdownfieldname);
			}		
		});
		$("input[id='"+dropdownfieldname+"discr']").focus(function(){
			$("input[id='"+dropdownfieldname+"discr']").change();
		});
		//redefine click function
		$("a[id='"+dropdownfieldname+"f4']").click(function(){
			filtercodes="";							   
			$.each(filterfields, function(i,n){
				filtercodes+=$("input[name='"+n+"']").val();

			});

			if($.trim(filtercodes)==""){
				return false;
			}else{
				//filter the dropdown options
				var size=0;
				//filter the dropdown options
				//First clear the original dropdown list
				$("select[id='"+dropdownfieldname+"dropdowns'] option").each(function(){
					$(this).remove();
				});

				$("select[id='"+dropdownfieldname+"dropdownsback'] option").each(function(){ 
					var valu=$(this).val();
					var codep=valu.indexOf('^');
					var codev=valu.substring(0,codep);

					if(codev.indexOf(filtercodes)==0){			
						var newc=codev.substring(filtercodes.length);
						var newtitle=valu.substring(codep+1);
						var newval=newc+valu.substring(codep);
						//<option title="Automatic" value="A-Automatic">
						$("<option title='"+newtitle+"' value='"+newval+"'>"+newtitle+"</option>").appendTo($("select[id='"+dropdownfieldname+"dropdowns']"));
						size+=1;
					}

				});
				//if(size<2){size=2;}
				//$("select[id='"+dropdownfieldname+"dropdowns']").attr("size",size);
				displaySearchField(dropdownfieldname);
			}
		});			
	});
};

//////////////////////////////////////////Enhance dropdown//////////////

// IPNC-1915
/**
 * for the item in subfile
 */
function createDropdown(obj,size,flag,posx,posy){
	var newDrop = $(obj).clone(false);
	newDrop[0].size=size;
	newDrop.css("position","absolute");
	if(posx){
		newDrop.css("top",posy);
		newDrop.css("left",posx);
	}else{
		newDrop.css("top",$(obj).offset().top);
		newDrop.css("left",$(obj).offset().left);
	}
	newDrop.css("z-index","999");
	newDrop.css("height",(16*size)+"px")
	newDrop.css("display","none");
	$("body").append(newDrop);
	var oid = $(obj).attr("id");
	var nid = "temp_"+oid;
	newDrop.attr("id",nid);
	newDrop.attr("name",nid);
	newDrop.click(function(){
		$(obj).val(this.value);
		$(obj).css("visibility","");
		if(flag){
			loadSelect(obj);
		}
		$(this).css("display","none");
	});
	// Add for IGROUP-1105
	newDrop.keyup(function(){
		$(obj).val(this.value);
		if(flag){
			loadSelect(obj);
		}
	});
	newDrop.blur(function(){
		$(obj).css("visibility","");
		$(this).css("display","none");
	});
	obj.subselect = newDrop;
}
function createDropdownInTable(tableId,dropdownId,dropdownSize,linkedFlag){
	var selectorId = "select[id^='" + tableId+"."+dropdownId+"_R"+"']";
	
	$(selectorId).each(function(){
		createDropdown(this,dropdownSize,linkedFlag);
		if(checkNonIEBrowser()){
			$(this).bind("mousedown",function(){
				this.subselect.css("display","");
				//must re-set top-left when in tab.
				this.subselect.css("top",$(this).offset().top);
				this.subselect.css("left",$(this).offset().left);
				this.subselect.css("background-color","white");
				$(this).css("visibility","hidden");
				// Add for IGROUP-1105
				this.subselect.focus();
				event.preventDefault();
			});
		}else{
			$(this).bind("beforeactivate",function(){
				this.subselect.css("display","");
				//must re-set top-left when in tab.
				this.subselect.css("top",$(this).offset().top);
				this.subselect.css("left",$(this).offset().left);
				this.subselect.css("background-color","white");
				// Add for IGROUP-1105
				this.subselect.focus();
				$(this).css("visibility","hidden");
			});
		}
	})
	$("body").click(function(){
		$(selectorId).each(function(){
			$(this).css("display","");
			$(this).css("visibility","");
			this.subselect.css("display","none");
		})
	})
}
/**
 * for the item in table but not in subfile
 * @param dropdownId
 * @param dropdownSize
 */
function createDropdownInTableNoId(dropdownId,dropdownSize,posx,posy){
	var dorpItem  = document.getElementById(dropdownId);
	if(dorpItem){
		createDropdown(dorpItem,dropdownSize,false,posx,posy);
		if(checkNonIEBrowser()){
			$(dorpItem).bind("mousedown",function(event){
				this.subselect.css("display","");

				if(posx){
					this.subselect.css("top",posy);
					this.subselect.css("left",posx);
				}else{
					//must re-set top-left when in tab.
					this.subselect.css("top",$(this).offset().top);
					this.subselect.css("left",$(this).offset().left);
				}
				this.subselect.css("background-color","white");
				$(this).css("visibility","hidden");
				$(this).css("z-index","-1");
				// Add for IGROUP-1105
				this.subselect.focus();
				event.preventDefault();
				//event.stopPropagation();
				//return false;
			});
		}else{
			$(dorpItem).bind("beforeactivate",function(){
				this.subselect.css("display","");
				if(posx){
					this.subselect.css("top",posy);
					this.subselect.css("left",posx);
				}else{
					//must re-set top-left when in tab.
					this.subselect.css("top",$(this).offset().top);
					this.subselect.css("left",$(this).offset().left);
				}
				this.subselect.css("background-color","white");
				$(this).css("visibility","hidden");
				// Add for IGROUP-1105
				this.subselect.focus();
			});
		}
		$("body").click(function(){
			$(dorpItem).css("visibility","");
			dorpItem.subselect.css("display","none");
		})
	}
}

/**
 * for the item not in table
 * @param obj
 * @param size
 * @returns {Boolean}
 */
function reSizeDropDownOnMouseDown(selectTag,size){
	$(selectTag).css("background-color","white");
	selectTag.style.zIndex = 99;
	selectTag.size = size;
	selectTag.style.height = size*18;
	return false;
}
function recoverDropDown(selectTag){
	selectTag.options.size = 1;
	selectTag.size=1;
	selectTag.style.height = 18;
	//$(selectTag).css("background-color","white");
}
function createDropdownNotInTable(dorpId,size){
	createDropdownInTableNoId(dorpId,size);
	/* var dorpItem  = document.getElementById(dorpId);
	if(dorpItem){
		dorpItem.onbeforeactivate = function() { reSizeDropDownOnMouseDown(this,size); }
		dorpItem.onchange = function() { recoverDropDown(this); }
		dorpItem.onblur = function() { recoverDropDown(this); }
	}*/
}
// IPNC-2681 Cross Browser 5th Sprint
$(document).ready(function(){
	var sizeDD = $("select").attr("size");
	if(sizeDD==1||sizeDD==0){
		$("select").removeClass("bold_cell");
		$("select").removeClass("input_cell");
	} 
	//$("select[class='bold_cell']").each(function(){
		//$(this).css("background-color","white");
	//})
	//$(".redSelect").css("background-color","white");
});
function checkNonIEBrowser(){
	if(navigator.userAgent.indexOf("Firefox") > 0 || 
			   navigator.userAgent.indexOf("Chrome") > 0 || 
			   navigator.userAgent.indexOf("Trident/7.0") > 0)
			return true;
}


/**
 * for the item in table but not in subfile
 * @param dropdownId
 * @param dropdownSize
 */
function createDropdownObjInTableNoId(dorpItem,dropdownSize,posx,posy){
	if(dorpItem){
		createDropdown(dorpItem,dropdownSize,false,posx,posy);
		if(checkNonIEBrowser()){
			$(dorpItem).bind("mousedown",function(event){
				this.subselect.css("display","");

				if(posx){
					this.subselect.css("top",posy);
					this.subselect.css("left",posx);
				}else{
					//must re-set top-left when in tab.
					this.subselect.css("top",$(this).offset().top);
					this.subselect.css("left",$(this).offset().left);
				}
				this.subselect.css("background-color","white");
				$(this).css("visibility","hidden");
				$(this).css("z-index","-1");
				// Add for IGROUP-1105
				this.subselect.focus();
				event.preventDefault();
				//event.stopPropagation();
				//return false;
			});
		}else{
			$(dorpItem).bind("beforeactivate",function(){
				this.subselect.css("display","");
				if(posx){
					this.subselect.css("top",posy);
					this.subselect.css("left",posx);
				}else{
					//must re-set top-left when in tab.
					this.subselect.css("top",$(this).offset().top);
					this.subselect.css("left",$(this).offset().left);
				}
				this.subselect.css("background-color","white");
				$(this).css("visibility","hidden");
				// Add for IGROUP-1105
				this.subselect.focus();
			});
		}
		$("body").click(function(){
			$(dorpItem).css("visibility","");
			dorpItem.subselect.css("display","none");
		})
	}
}
/////////IPNC-1915 END