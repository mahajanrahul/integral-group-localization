$.extend({
	disableLinks: function(linkObjs) {
	for (var i = 0; i < linkObjs.length; i++) {
		var linkObj = "#" + linkObjs[i];
		$(linkObj).removeAttr("href");
		$(linkObj).css({color: "#999999","text-decoration":"underline","cursor":"default"});
	}
},
enableLinks: function(linkObjs) {
	for (var i = 0; i < linkObjs.length; i++) {
		var linkObj = "#" + linkObjs[i];
		$(linkObj).attr("href" , "javascript:");
		$(linkObj).css({color: "#aa6e01","text-decoration":"underline","cursor":"pointer"});
		$(linkObj).hover(
				function(){ $(this).css({color: "#834e00","text-decoration":"none"});},
				function(){$(this).css({color: "#aa6e01","text-decoration":"underline"});}
		);
	}
},
linkClickHandler: function(e) {
	var ItemSelected = false;
	var id = this.id;
	if (e.data.singleSelect == true) {

		$("input[type='checkbox']").each(function(i) {
			if (this.id != id) {									 
				this.checked = false;
				if ((e.data.screenName !=null) && (e.data.actionField != null)) {
					var actionFld ="#" + e.data.screenName + "screensfl." + e.data.actionField + "_R" + this.id;
					$(actionFld).val(" ");
				}
			}
		})
	}
	$("input[type='checkbox']").each(function(i) {
		if (this.checked == false) {
			if ((e.data.screenName !=null) && (e.data.actionField != null)) {
				var actionFld ="#" + e.data.screenName + "screensfl." + e.data.actionField + "_R" + this.id;
				$(actionFld).val(" ");
			}
		}

	})
	$("input[type='checkbox']").each(function(i) {
		if (this.checked == true) {
			ItemSelected = true;
			return;
		}

	})
	if (ItemSelected == true) {
		$.enableLinks(e.data.linkObjs);
	} else {
		$.disableLinks(e.data.linkObjs);
	}
	ItemSelected = false;
},
replaceContinueButtonClick :function(e) {
	var linkHtml = '' + $("#continuebutton").parent().clone(true).html();
	var searchText = /onClick\=\"\w+\(this.'PFKEY0'\)\"/gi;
	linkHtml = linkHtml.replace(searchText, '');
	$("#continuebutton").remove();
	$("#continuebuttondiv").append(linkHtml);
}
});
