/* This function involved all the subfile table operations(add and remove row)
 * 
 * rows: the number of rows
 * isPageDown: variable to indicate whether current page is shown by clicking enter or page down
 * pageSize: number of rows in one subfile
 * fields: all the necessary fields that user must enter
 * subfile_table: subfile table id
 * subfile_add: add button id
 * subfile_remove: remove button id
 * divDelfields: clear the content of div when more control in one td
 * 
 * Author: Ai Hao 2010-7-20
 */
 
//set subfile_remove as global variable because the function setRemoveStyle uses it.
var subfile_remove	=	"subfile_remove";

function operateTableForSuperTable(rows, isPageDown, pageSize, fields,subfile_table,divDelfields,headerRows){
	var subfile_add 	=	"subfile_add";
	//var subfile_remove	=	"subfile_remove";
	
	operateSuperTableBase(rows, isPageDown, pageSize, fields,subfile_table,subfile_add,subfile_remove,divDelfields,headerRows);
}

function operateSuperTableBase(rows, isPageDown, pageSize, fields,subfile_table,subfile_add,subfile_remove,divDelfields,headerRows){
	
	var a = new Array(rows);
	var spaceLine = 0;

	//add buttons to page before call superTable, otherwise the function couldn't be binded.
	var addRemoveBtnDiv = "<DIV id='addRemoveDiv' style='display:normal; POSITION: absolute; OVERFLOW-Y: hidden;" +
	  					  " border-top:0px;'>" +
	                      "<table style='height:30px;border:none;POSITION:relative;top:3px;' cellspacing='0'>" +
	                      "<tr>" +
	                      "<td><div class='sectionbutton mr10'><p>" +
	                      "<a  id='subfile_add' href='javascript:;'>" +
	                      "Add</a></p></div></td>" +
	                      "<td><div id='removeDiv' class='sectionbtndisable mr10' ><p>" +
	                      "<a id='subfile_remove' href='javascript:;'>" +
	                      "Remove</a></p></div></td>" +
	                      "</tr></table></DIV>";	
        							  
    $(".fakeContainer").after(addRemoveBtnDiv);	
	$("#addRemoveDiv").css("display","none");	

	$("#"+subfile_table).find("tr:gt("+(headerRows - 1)+")").each(function(i) {
		var isEmpty = true;
		var hasRed = false;
		// By default, search both input and div 
		var isInputFound = true;
		var isDivFound = true;
		var isSelectFound = true;
		for (var j=0;j<fields.length;j++){
			if (isInputFound){
				$(this).find("input[name*='"+fields[j]+"']").each(function (){
					// later on, will only search input
					isInputFound = true;
					isDivFound = false;
					var value = $(this).val();
					value = $.trim(value);
					/* Francis 2010-4-7
					 * Fixed UI enhamecent bug 1084
					 * if backgroundColor is red, display the whole record.
					 */
					//update for new error style
					//if(value != "" || ($(this).css("backgroundColor")=="red"||$(this).css("backgroundColor")=="#FF0000"||$(this).css("backgroundColor")=="#ff0000")){
					if(value != "" || $(this).hasClass("red")){
						isEmpty = false;
						if($(this).hasClass("red")) hasRed = true;
					}
				});
			}
			if (isDivFound){
				$(this).find("div[id*='"+fields[j]+"']").each(function (i){
					// later on, will only search div
					isInputFound = false;
					isDivFound = true;
					
					var value = $(this).html();
					value = $.trim(value);
					if (value == ""){
						//does nothing
					}else{
						isEmpty = false;
					}
				});
			}
			
			if (isSelectFound){
				$(this).find("select[name*='"+fields[j]+"']").each(function (i){
					// later on, will only search div
					isInputFound = false;
					isDivFound = false;
					var index=document.getElementById($(this).attr('id')).selectedIndex;
					var options = document.getElementById($(this).attr('id')).options;
					var value=options[index].value;
					if (value == ""){
							if($(this).hasClass("red")){
							isEmpty = false;
							hasRed = true;
						}
					}else{
						isEmpty = false;
					}
				});
			}
			
			// if there is one field not empty, then the whole tr should be displayed
			if (isEmpty == false)
				break;
		}
		
		
		//modified by Ai Hao for bug#1259
		var index = i + 1;
		
		if (isEmpty){
			a[index] = 0;
		}else{
			a[index] = 1;
		}
		
		//hasRed -- for the following situation:
		//1. the row has required inputs but without value
		//2. the row has been marked by red
		if(hasRed || isEmpty) 
		{
			//added for bug#1259
			spaceLine++;
		}
		
		//modified by Ai Hao for bug#1259
		if (index == 1){
			a[1] = 1;
			return true;// continue the loop. By default, first "tr" is shown
		}		
		
		if (isPageDown == 1){
			if (index%pageSize == 1){
				a[index] = 1;
			}else if(pageSize == 1){
			    //added for bug#1178
				a[index] = 1;
				//added for bug#1259
				if (spaceLine >= 1 && isEmpty) a[index] = 0;
			}
		}

		if (a[index] == 0){
			$(this).hide();
		}
	});
	
	function checkRequiredField(temp)
	{
			var mustInput = false;
			var requiredFieds = new Array();
			//IGroup-1086 Add it for select control start by xma3
			var IE8 = false;
			if (navigator.userAgent.search("MSIE") >= 0){
		    	var position = navigator.userAgent.search("MSIE") + 5;
		    	var end = navigator.userAgent.search("; Windows");
		    	var version = navigator.userAgent.substring(position,end);
		    	if(version == "8.0" ){
		    		IE8 = true;
		    	}
			}
			//IGroup-1086 Add it for select control end by xma3
			$(".sSky-Fixed").find("tr:gt("+(headerRows-1)+")").each(function(k) {
				if (k+1 != temp)
					return true;
				var field;
				var discrField;
				for (var j=0;j<fields.length;j++){
					//remove previous red class first
					$(".sSky-Fixed").find("tr:gt("+(headerRows-1)+")").find("input[id*='"+fields[j]+"']").each(function (i){
						$(this).removeClass("red");
					});
					//remove previous red class first
					$(".sSky-Main").find("tr:gt("+(headerRows-1)+")").find("input[id*='"+fields[j]+"']").each(function (i){
						$(this).removeClass("red");
					});
					//remove select red class for select by xma3
					if(!IE8){
					  $(".sSky-Fixed").find("tr:gt("+(headerRows-1)+")").find("select[id*='"+fields[j]+"']").each(function (i){
						$(this).removeClass("red");
					  });
					  $(".sSky-Main").find("tr:gt("+(headerRows-1)+")").find("select[id*='"+fields[j]+"']").each(function (i){
						$(this).removeClass("red");
					  });
					}
					
					//add red class for field in the new line if necessary
					$(".sSky-Fixed").find("tr:eq("+(k+headerRows)+")").find("input[id*='"+fields[j]+"']").each(function (i){
						var value = $(this).val();
						value = $.trim(value);
						if (value == "" || 
							value.indexOf(fontENG) != -1 ||
							value.indexOf(fontCHI) != -1){
							mustInput = true;							
							requiredFieds[i] = this;
						}
					});

					$(".sSky-Main").find("tr:eq("+(k+headerRows)+")").find("input[id*='"+fields[j]+"']").each(function (i){			
						var value = $(this).val(); 
						value = $.trim(value);
						if (value == "" || 
							value.indexOf(fontENG) != -1 ||
							value.indexOf(fontCHI) != -1){
							mustInput = true;							
							requiredFieds[i] = this; 
						}
					});										
					
					//added for IGroup-937 by hai2 begin
					//check required field for <select>
					$(".sSky-Fixed").find("tr:eq("+(k+headerRows)+")").find("select[id*='"+fields[j]+"'] option:selected").each(function (i){
						var value = $(this).val();
						value = $.trim(value);
						if (value == "" || 
							value.indexOf(fontENG) != -1 ||
							value.indexOf(fontCHI) != -1){
							mustInput = true;							
							requiredFieds[i] = this;
						}
					});					
					 
					$(".sSky-Main").find("tr:eq("+(k+headerRows)+")").find("select[id*='"+fields[j]+"'] option:selected").each(function (i){			
						var value = $(this).val(); 
						value = $.trim(value);
						if (value == "" || 
							value.indexOf(fontENG) != -1 ||
							value.indexOf(fontCHI) != -1){
							mustInput = true;							
							requiredFieds[i] = this; 
						}
					});	
					//added for IGroup-937 by hai2 end
					//IGroup-1086 added for red select control by xma3
					if(!IE8){
						//IGROUP-1211					
						$(".sSky-Fixed").find("tr:eq("+(k+headerRows)+")").find("select[id*='"+fields[j]+"'] option:selected").each(function (i){
							var value = $(this).val();
							value = $.trim(value);
							if (value == "" || 
								value.indexOf(fontENG) != -1 ||
								value.indexOf(fontCHI) != -1){
								mustInput = true;							
								requiredFieds[i] = this;
							}
						});			
						//IGROUP-1211
				    	$(".sSky-Main").find("tr:eq("+(k+headerRows)+")").find("select[id*='"+fields[j]+"'] option:selected").each(function (i){			
						var value = $(this).val(); 
						value = $.trim(value);
						if (value == "" || 
							value.indexOf(fontENG) != -1 ||
							value.indexOf(fontCHI) != -1){
							mustInput = true;							
							requiredFieds[i] = this; 
						 }
				    	});	
					}
					
					//check the fields that user must input
					if (mustInput == true){//Francis 2010-2-9 for UI Enhancement.
						if (isChinese())
							alert("请输入必填字段");
						else
							alert("Field must be entered!");
						
						//update for new error style
						for(var n=0; n < requiredFieds.length; n++)
						{
							$(requiredFieds[n]).addClass("red");
							$(requiredFieds[n]).focus();
						}
						break;
					}
				       
				}
				if (mustInput == true)
					return false;
				
				if (k+1 < rows-1){
					$(this).next().show();
					$(".sSky-Main").find("tr:eq("+(k + headerRows)+")").next().show();
				    a[k+1+1] = 1;
					
					$(".sData").scrollTop($(".sData")[0].scrollHeight);					
				}else{
					// No more left
					doAction('PFKEY90');
				}
				return false;
			});	
	}
	
	
	$("#"+subfile_add).click(
		function() {
			var temp = 0;
			for (var i=rows-1;i>=1;i--){
			    if (a[i] == 1){
			    	temp = i;  
			    	break;
			    }
			}
			
			checkRequiredField(temp);
			//IGroup-937 need "headerRows" since "this.headerRows" is undefined in the Firefox etc
			//setRemoveStyle();
			//IGroup-1211
			setRemoveBtnStyle(headerRows);
		}
	)

	

	$("#"+subfile_remove).click(
		function() {
			//set remove button style
			$("#removeDiv").removeClass("sectionbutton");
			$("#removeDiv").addClass("sectionbtndisable");
			//IGroup-937 need "headerRows" since "this.headerRows" is undefined in the Firefox etc
			//setRemoveStyle();
			
			
			// first decide which to hide, by default is the last tr to hide.
			var t = rows - 1;
			for (var i=1;i<rows;i++){
				if (a[i] == 0){
					t = i-1;
					break;
				}
			}   
			var isSelected = false;	
			$(".sSky-Fixed").find("tr:gt("+(headerRows-1)+")").each(function(j) {
				// special case: if t== 1, means a[2]==0.Then there is only one row.
				// Can't delete the only one row.
				if (t == 1){
					// t == 1, then only one rwo is shown, when click delete, 
					// reset all values but don't hide the tr
					/*
					$(this).find("input[type='text']").each(function(i) {
						$(this).val("");
			    	});
					*/
			    	return false;
				} 
				if (isSelected){
					var temp = new Array();
					var hidden_temp=new Array();
					
					/* Francis 2010-2-9 for UI Enhancement. 
					 * support checkbox checked status
					 * Finding input fields that all types instead of type="text" fields.
					 * This solutions can cover values of all input fileds of pre line.
					 */
					
		    		
					$(this).prev().find("input").each(function(i) {
						$(this).val(temp[i]);
		    		});
		    		
		    		if (j+1 == t){
		    			//recover background style with text control in a "tr".
		    			//Francis 2010-2-9 for UI Enhancement.
		    			$(this).find("input[type='text']").each(function(i) {
		    				$(this).css("background","white");
			    		});
						$(this).hide();
						a[t] = 0;
						return false;
					}
				}else{
	 	 	 	 	$(this).find("input[type='radio']").each(function(i) {
						if ($(this).is(":checked")){
							isSelected = true;
							return false;
						}
					});
					if (isSelected){
					// If deleting last row, then no need to switch data with others,just delete them all and hide
						if (j+1 == t){
						/* support checkbox checked status*/
							$(this).find("input").each(function(i) {
								$(this).val("");
								if($(this).attr("checked")){//Francis 2010-2-9 for UI Enhancement.
									$(this).removeAttr("checked");
								}
				    		});
							/*$(this).find("input[type='text']").each(function(i) {
								alert("id==="+this.id+" name==="+this.name+"  value="+this.value);								
								$(this).val("");
				    		});*/
				    	//recover background style with text control in a "tr".
		    			//Francis 2010-2-9 for UI Enhancement.
			    			$(this).find("input[type='text']").each(function(i) {
			    				$(this).css("background","white");
				    		});
							$(this).hide();
							a[t] = 0;
							return false;
						}
					}
				}
					 
			});
			//deleted for checkbox by Michelle
			var trdel = false;
			var isText = false;
			var trDisplayMain = $(".sSky-Main").find("tr:not(:hidden):gt("+(headerRows-1)+")");
			var trDisplayFixed= $(".sSky-Fixed").find("tr:not(:hidden):gt("+(headerRows-1)+")");
			var len = trDisplayMain.length; 
			//var lenMain = trDisplayFixed.length;alert(lenMain);
			var tdArray = new Array();
			var tdindex = 0;
			for(var i=len-1;i>=0;i--){
			 trdel = false;
			 $(trDisplayFixed.get(i)).find("td:first input[type='checkbox']").each(function(n){			 	
			    if ($(this).is(":checked")){			    	
			       trdel = true;   
			     }
			 })
			 if(trdel){
			    //clear each content of td.
				var clearTd = new Array();
				clearTd[0] = trDisplayMain.get(i);
				clearTd[1] = trDisplayFixed.get(i)
			    for(var iTd=clearTd.length-1; iTd>=0; iTd--)
				{
					$(clearTd[iTd]).find("td").each(function(m){			 
					  isText = false;
					  $(this).find("input").each(function(o){
						 $(this).val("");			 	   				 	   	
						if($(this).attr("type")=="checkbox"||$(this).attr("type")=="radio") {		 	   		
								$(this).attr("checked","");			 	   
						}
							
						   else if($(this).attr("type")=="text"){
								//IGroup-1086 begin
								if($(this).attr("class").indexOf("output_cell") == -1)
								{
								$(this).css("backgroundColor","#FFFFFF");
								}
								$(this).css("color","#000000");
						}
						
						if(this.id.indexOf("discr")!=-1){this.focus();$(this).blur();}
						 isText = true;
						})			
					  //IGroup-1086 begin	
					  $(this).find("select").each(function(o){
						  isText = true;
						  $(this).val("");
					  })
					  // end	
					  if(!isText) $(this).html(""); 
					  if(divDelfields!=null){
						for(var divDelI = 0; divDelI<divDelfields.length;divDelI++){
							  $(this).find("div[id*='"+divDelfields[divDelI]+"']").text("");
							}
					  }
					})			
				}
			    
				 if(len == 1) return false;
				
			    if(i==len-1) {
					$(trDisplayFixed.get(i)).css("display","none");	
					$(trDisplayMain.get(i)).css("display","none");	
					//make two tables aligning
					$(".sFDataInner").css("top", $(".sData")[0].scrollTop * -1);
				
					a[i+1]=0;
					len--;			    
					continue;
			   }

              //move the data and the css of each control
			  var moveArray = new Array();
			  moveArray[0] = trDisplayMain;
			  moveArray[1] = trDisplayFixed;
			  for(var j=i;j<len-1;j++){
			    tdArray = new Array();
			    tdindex = 0;
		            
				for(var n=0; n<2; n++)
				{
					//Copy the values of rows which are behind deleted row
					$(moveArray[n].get(j+1)).find("td").each(function(m){
					  isText = false;
					  $(this).find("input[type='text']").each(function(o){
						  tdArray.push($(this).val());			  
						  tdArray.push($(this).css("backgroundColor"));
						  //IGroup-1086 begin
						  if($(this).attr("class").indexOf("output_cell") == -1)
						  {
						  $(this).val("");		
						  $(this).css("backgroundColor","#FFFFFF");
						  }
						  //end
						  $(this).css("color","#000000");	 
						  if(this.id.indexOf("discr")!=-1){this.focus();$(this).blur();}	   				 	   	
						  isText = true;
						})
					  $(this).find("input[type='hidden']").each(function(o){
						  tdArray.push($(this).val()); 
						  $(this).val("");		
						  isText = true;			 	   				 	   				 	   	
						})  
					  $(this).find("input[type='checkbox']").each(function(o){
						  tdArray.push($(this).attr("checked"));					
						  $(this).val("");		
						  isText = true;			 	   				 	   				 	   	
						})   
					  $(this).find("textarea").each(function(o){
						  tdArray.push($(this).val());		
						  $(this).val("");	
						  isText = true;			 	   				 	   				 	   	
						})  
					 $(this).find("input[type='radio']").each(function(o){
						 tdArray.push($(this).attr("checked")); 					
						 $(this).attr("checked","");
						 $(this).css("backgroundColor","#FFFFFF");
						 $(this).css("color","#000000");
						isText = true;			 	   				 	   				 	   	
						}) 
					  //IGroup-1086 begin
					  $(this).find("select").each(function(o){
						  tdArray.push($(this).get(0).selectedIndex);		
						  $(this).val("");
						  isText = true;						  
						}) 						
					 // end
												
					 if(!isText) {tdArray.push($(this).html()); $(this).html("");}
					 
					 if(divDelfields!=null){
						for(var divDelI = 0; divDelI<divDelfields.length;divDelI++){
							tdArray.push($(this).find("div[id*='"+divDelfields[divDelI]+"']").text());
							$(this).find("div[id*='"+divDelfields[divDelI]+"']").text("");
							  
							}
					  }
									 
					})
					 
					 //move data
					 $(moveArray[n].get(j)).find("td").each(function(m){
					   isText = false;			    
					  $(this).find("input[type='text']").each(function(o){			      	      	
						  $(this).val(tdArray[tdindex]); 
						  tdindex++;			
						  if(tdArray[tdindex]=="red" || tdArray[tdindex]=="#FF0000" || tdArray[tdindex]=="#ff0000"){
						  $(this).css("backgroundColor",tdArray[tdindex]);
						  $(this).css("color","#FFFFFF");
						}
						  tdindex++;		 	   				 	   	
						  isText = true;
						})
					  $(this).find("input[type='hidden']").each(function(o){
						  $(this).val(tdArray[tdindex]); 
						  tdindex++;
						  isText = true;			 	   				 	   				 	   	
						})  
					  $(this).find("input[type='checkbox']").each(function(o){
						  $(this).attr("checked",tdArray[tdindex]); 					
						  tdindex++;						
						  isText = true;			 	   				 	   				 	   	
						})   
					  $(this).find("textarea").each(function(o){
						  $(this).val(tdArray[tdindex]); 				
						  tdindex++;						
						  isText = true;			 	   				 	   				 	   	
						})	
					 $(this).find("input[type='radio']").each(function(o){
						 $(this).attr("checked",tdArray[tdindex]); 
						 tdindex++;
						isText = true;			 	   				 	   				 	   	
						})   	
						
					//IGroup-1086 begin
					 $(this).find("select").each(function(o){ 
						 $(this).get(0).selectedIndex = tdArray[tdindex];
						 tdindex++;
						isText = true;			 	   				 	   				 	   	
						})  						
					// end
					 if(!isText) {$(this).html(tdArray[tdindex]);  tdindex++;}
					 
					 
					 if(divDelfields!=null){
						for(var divDelI = 0; divDelI<divDelfields.length;divDelI++){
							$(this).find("div[id*='"+divDelfields[divDelI]+"']").text(tdArray[tdindex]);
							tdindex++; 
							}
					  }
					
					})
				}
			   }
			 
			   $(trDisplayFixed.get(len-1)).css("display","none");
			   $(trDisplayMain.get(len-1)).css("display","none");		
			   $(".sFDataInner").css("top", $(".sData")[0].scrollTop * -1);

			    a[len] = 0;	
			    len--;
			  }
			}	
			
			if($(".sSky-Fixed").find("tr:not(:hidden)").length==0){
               $(".sSky-Fixed").find("tr:first").css("display","");
               a[1]=1;
            }
                    
			if($(".sSky-Main").find("tr:not(:hidden)").length==0){
              $(".sSky-Main").find("tr:first").css("display","");
              a[1]=1;
            }
			
			//IGroup-937 Check and set the remove button status after deleting action
			//IGroup-1211
			setRemoveBtnStyle(headerRows);			
		}	
	) 

}

// This function support subfile table remove button disable/enable.
// IGroup-937
// function setRemoveStyle(subfile_table){
//IGroup-1211
function setRemoveBtnStyle(headerRows){
	//IGroup-937, change "this.headerRows" to "headerRows", 
	//in the Firefox etc. "this" refert to the object who calls this function, this.headerRows will return undefined
	//$(".sSky-Fixed").find("tr:not(:hidden):gt("+(this.headerRows-1)+")").each(function(){
	$(".sSky-Fixed").find("tr:not(:hidden):gt("+(headerRows-1)+")").each(function(){
		$(this).find("td:first input[type='checkbox']").each(function(){
				$(this).bind("click", function(){
					//decide if the remove action could be triggered
					/* IGroup-937, move actions to a new function
					var delAction = false;
					var fixedTable = $(".sSky-Fixed").find("tr:not(:hidden):gt("+(this.headerRows-1)+")");
					var fixedTable = $(".sSky-Fixed").find("tr:not(:hidden):gt("+(headerRows-1)+")");
					var rowNum = fixedTable.length;
					
					for(var i=rowNum-1; i>=0; i--){
						$(fixedTable.get(i)).find("td:first input[type='checkbox']").each(function(){			 	
						    if ($(this).is(":checked")){			    	
						    	delAction = true;   
							}
						});
					}
					*/
					changeRemoveBtnStatus(headerRows);	
				});   
		});
		
		//IGroup-937
		changeRemoveBtnStatus(headerRows);	
	});		
}
/* To decide the picutre suffix according to the language
 * Wayne Yang 2010-04-22
 */
function langSuffix(){
	var lang = $("#lang1");
	if (lang && ($.trim(lang.val()) == "CHI/"))
		return "_cn";
	return "";
}

/* To decide the language
 * Wayne Yang 2010-04-22
 */
function isChinese(){
	var lang = $("#lang1");
	if (lang && ($.trim(lang.val()) == "CHI/"))
		return true;
	return false;
}

/*
IGroup-937 Remove button can't be changed to available status in the following status
1. select one row
2. click search button and jump to another searching page
3. select one item and back, then although the checkbox was selected, but remove button is unavailable		
*/
function changeRemoveBtnStatus(headerRows)
{
	var delAction = false;
	var fixedTable = $(".sSky-Fixed").find("tr:not(:hidden):gt("+(headerRows-1)+")");
	var rowNum = fixedTable.length;
	
	for(var i=rowNum-1; i>=0; i--){
		$(fixedTable.get(i)).find("td:first input[type='checkbox']").each(function(){			 	
		    if ($(this).is(":checked")){			    	
		    	delAction = true;   
			}
		});
		
		$(fixedTable.get(i)).find("td:first select").each(function(){	
		    if ($(this).val() == '1'){			    	
		    	delAction = true;   
			}
		});		
	}
	
	if(delAction){
		$("#removeDiv").removeClass("sectionbtndisable");
		$("#removeDiv").addClass("sectionbutton");
	}else{
		$("#removeDiv").removeClass("sectionbutton");
		$("#removeDiv").addClass("sectionbtndisable");
	}	
}