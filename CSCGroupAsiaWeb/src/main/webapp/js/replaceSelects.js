function replaceSelects(){
	if(!document.getElementById && !document.createTextNode) {
		return;
	}
	
	/* Dropdown stuff */
	var ts_selectclass='makeDOMDrop1'; 	// SELECTs of this Class will be replaced
	var ts_triggeroff='dropSelected';	// Class for the combo box when the SELECT is selected.
	var ts_triggerns='dropNotSelected';	// Class for the combo box when not selected.

	var count=0;
	var toreplace=new Array();
	var sels=document.getElementsByTagName('select');

	/* Turn all selects of the appropriate Class into input cells */
	for (var i=0;i<sels.length;i++) {
		//Is this the appropriate Class ? 
		if (ts_check(sels[i],ts_selectclass)) {
			//Create the input field
			var trigger=document.createElement('input');
			trigger.name=sels[i].name;
			//window.status = window.status + " " + sels[i].name;
			trigger.type='text';
			trigger.id=sels[i].id;
			trigger.value="";
			trigger.title=sels[i].title;
			trigger.className=sels[i].className;
			ts_addclass(trigger,ts_triggerns);
			ts_replaceclass(trigger,ts_selectclass,"");

			/* Clone the array of current values */
			optArray = sels[i].getElementsByTagName('option');
			opArr = new Array();
			for (var j=0; j<optArray.length; j++) {
				opArr[j] = new Object();
				opArr[j].value = optArray[j].value;
				opArr[j].label = optArray[j].label;
				opArr[j].selected = optArray[j].selected;
			}
			trigger.optarray = opArr;
			sels[i].parentNode.insertBefore(trigger,sels[i]);
			found = 0;
			for(var j=0;j<opArr.length;j++) {
				if (opArr[j].selected) {
					found = j;
					break;
				}
			}
			trigger.value = opArr[found].value;
			trigger.style.width="100%";
			toreplace[count]=sels[i];
			count++;
			sels[i].parentNode.insertBefore(trigger,sels[i])

			/* When the link gets focus, change its appearance */
			trigger.onfocus=function(){
				doFocus(this);
				ts_swapclass(this ,ts_triggerns, ts_triggeroff);
				return false;
			}

			/* Restore appearance on loss of focus */
			trigger.onblur=function(){
				ts_swapclass(this ,ts_triggerns, ts_triggeroff);
				return false;
			}

			/* When clicked, popup a modal dialog. */
			trigger.onclick=function(){
				thisElt = this.optarray;
				thisElt.title=this.title;
				thisElt.value = this.value;
				if (thisElt.length >= 2) {
					aval = window.showModalDialog (getCommonScreens("popupt.jsp"), new Array(thisElt), "dialogWidth:25; dialogHeight:25");
					///* Update the value on return. */
					this.value = aval;
				}
				else {
					//alert("No PROMPT data available.");
					callCommonAlert(document.getElementById("lang1").value,"No00021");
				}
				return false;
			}

			/* When a key is pressed, emulate scrolling through the values */
			trigger.onkeypress=function(){
				if (event.keyCode==13) {
					return true;
				}
				elt = this;
				arr = this.optarray;
				cnt = arr.length;
				c = String.fromCharCode(event.keyCode).toUpperCase();
				if (c == " ") {
					this.value = arr[0].value;
					return false;
				}
				ii = 0;
				for (ii=0; ii<arr.length; ii++) {
					if (elt.value == arr[ii].value) {
						break;
					}
				}
				for (ii=ii+1; ii<arr.length; ii++) {
					if (arr[ii].value.substring(0,1).toUpperCase() == c) {
						this.value = arr[ii].value;
						return false;
					}
				}
				for (ii=0; ii<arr.length; ii++) {
					if (arr[ii].value.substring(0,1).toUpperCase() == c) {
						this.value = arr[ii].value;
						return false;
					}
				}
				return false;
			}

			/* When a key is pressed, emulate scrolling through the values */
			trigger.onkeydown=function() {
				k = event.keyCode;
				if (k==8 || k==46 || k==37 || k==39 || k==36 || k==35) {
					event.returnValue = false;
					return false;
				}
				if (k==115) {
					window.status = 'pf4 found';
					event.returnValue = false;
					event.keyCode = 9;
					this.onclick();
					return false;
				}
			}

			/* When a key is pressed, emulate scrolling through the values */
			trigger.onkeyup=function(){
				k = event.keyCode;
				if (k==8 || k==46 || k==37 || k==39 || k==36 || k==35) {
					event.returnValue = false;
					return false;
				}
				if (event.keyCode != 38 && event.keyCode != 40) {
					return true;
				}
				if (event.keyCode == 40 && event.altKey) {
					this.onclick();
					return false;
				}
				if (event.altKey || event.ctrlKey) {
					return true;
				}
				if (event.keyCode == 40) {
					elt = this;
					arr = this.optarray;
					cnt = arr.length;
					ii = 0;
					for (ii=0; ii<arr.length; ii++) {
						if (elt.value == arr[ii].value) {
							break;
						}
					}
					if (ii < arr.length - 1) {
						ii++;
					}
				}
				else  {
					elt = this;
					arr = this.optarray;
					cnt = arr.length;
					ii = 0;
					for (ii=0; ii<arr.length; ii++) {
						if (elt.value == arr[ii].value) {
							break;
						}
					}
					if (ii > 0) {
						ii--;
					}
				}
					
				this.value = arr[ii].value;
				this.select();
				return false;
			}
		}
	}
	
	/* Remove the selects  */
	for(i=0;i<count;i++){
		toreplace[i].parentNode.removeChild(toreplace[i]);
	}
	
	function ts_check(o,c){
	 	return new RegExp('\\b'+c+'\\b').test(o.className);
	}
	function ts_swapclass(o,c1,c2){
		var cn=o.className;
		o.className=!ts_check(o,c1)?cn.replace(c2,c1):cn.replace(c1,c2);
	}
	function ts_replaceclass(o,c1,c2){
		var cn=o.className;
		if (ts_check(o,c1)) {
			o.className = cn.replace(c1,c2);
		}
	}

	function ts_addclass(o,c) {
		if(!ts_check(o,c)){o.className+=o.className==''?c:' '+c;}
	}
}