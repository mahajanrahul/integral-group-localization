addMenu("Demo", "top-menu");

addSubMenu("top-menu", "sub-menu 1", "", "", "sub-1", "x");
addSubMenu("top-menu", "sub-menu 2", "", "", "sub-2", "x");
addSubMenu("top-menu", "sub-menu 3", "", "", "sub-3", "x");
addLink("top-menu", "link 4", "", "", "");
addLink("top-menu", "link 5", "", "", "");

addLink("sub-1", "link s1-1", "", "", "");
addLink("sub-1", "link s1-2", "", "", "");
addLink("sub-1", "link s1-3", "", "", "");
addLink("sub-1", "link s1-4", "", "", "");

addLink("sub-2", "link s2-1", "", "", "");
addLink("sub-2", "link s2-2", "", "", "");
addLink("sub-2", "link s2-3", "", "", "");
addLink("sub-2", "link s2-4", "", "", "");
addLink("sub-2", "link s2-5", "", "", "");
addLink("sub-2", "link s2-6", "", "", "");

addLink("sub-3", "link s3-1", "", "", "");
addLink("sub-3", "link s3-2", "", "", "");
addLink("sub-3", "link s3-3", "", "", "");
addSubMenu("sub-3", "sub-menu s3-4", "", "", "sub-31", "x");
addLink("sub-3", "link s3-5", "", "", "");

addLink("sub-31", "link s3-4-1", "", "", "");
addLink("sub-31", "link s3-4-2", "", "", "");
addLink("sub-31", "link s3-4-3", "", "", "");
addLink("sub-31", "link s3-4-4", "", "", "");
addLink("sub-31", "link s3-4-5", "", "", "");
addLink("sub-31", "link s3-4-6", "", "", "");

endMenu();
