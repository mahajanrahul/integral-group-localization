submitted    = false;

loopcnt      = 0;
delay        = 10;
lastGoodDate = "  /  /    ";
before       = "";
cp           = 0;
b4cp         = 0;
lk           = 0;
lkc          = 0;
debugkeys    = false;
lastKey      = 0;
thisKey      = 0;
keySeq       = "";
blinkArray   = new Array();
var bookmark = "~";
force        = 0;
var focusInLast;
var lastSelectedF = ''; // fix bug46

var dateformatChar; // add by Cary Zhong june 07 2010

/* Cursor positioning mode
	0 = Positioning on the active field before submit;
	1 = Positioning on the first input enabled field, or the first element with customized attribute - hasCursor=YES is specified
*/ 
positioningMode = 1;
/* These fields are used to track latest focus element*/
lastFocus = null;
activity  = false;

/* This method trims white space off both ends of a string and returns the result. */
String.prototype.trim = function() {
    return( this.replace(/^\s*([\s\S]*\S+)\s*$|^\s*$/,'$1') );
}

String.prototype.trimRight = function() {
    return( this.replace(/^([\s\S]*\S+)\s*$|^\s*$/,'$1') );
}

function addValidKey(key) {
	if (validKeys == null) {
		validKeys = new Array();
		for (var i=1; i<=24; i++) {
			validKeys[i] = false;
		}
	}
	validKeys[key] = true;
}


function checkAllKeys() {
    lastKey = thisKey;
    thisKey = event.keyCode;
 
    if (thisKey == 17) {
    	keySeq = "Ctrl";
    	window.status = " ";
    }
    else if (lastKey != thisKey) {
    	keySeq = keySeq + String.fromCharCode(thisKey);
    }
     //Backspace key
    if (thisKey == 8) {
    	if (event.srcElement.tagName.toUpperCase() == "TEXTAREA" && event.srcElement.readOnly) {
     		return false;
     	}
     	//Check if it is currently focusing on a text field. If yes, then return true, otherwise false
    	if (event.srcElement.tagName.toUpperCase() != "INPUT" && event.srcElement.tagName.toUpperCase() != "TEXTAREA") {
			//alert("Backspace is disabled!");
			callCommonAlert(document.getElementById("langCode").value,"No0001");
     		return false;
     	}
    }
    /* window.status = "Key '" + thisKey + "' " + window.status; */
    if ((thisKey >= 112) && (thisKey <= 123)) {
      pfKey = thisKey - 111 + (lastKey == 16 ? 12 : 0);
      if (validKeys != null) {
		if (validKeys[pfKey] == false) {
			/*if(document.getElementById("lang1").value == "CHI/"){
				alert("Ã¥ÂŠÂŸÃ¨ÂƒÂ½Ã©Â”Â®" + pfKey + "Ã¤Â¸Â�Ã¨ÂƒÂ½Ã¥ÂœÂ¨Ã¥Â½Â“Ã¥Â‰Â�Ã¥Â±Â�Ã¥Â¹Â•Ã¤Â½Â¿Ã§Â”Â¨");
			} else {
				alert("Function key " + pfKey + " is not active on this screen at this time.");
			}*/
			callCommonAlert(document.getElementById("langCode").value,"No0002",pfKey);
			event.keyCode = 9;
			return false;
		}
		/* This check for Polisy Asia: F4 not valid if the current field is disabled */
		if (pfKey == 4) {
			if (!selectableInputField(thisElt)) {
				event.keyCode = 9;
				return false;
			}
		}
      }
      aForm.action_key.value = "PFKey" + fm2(pfKey);
      clearFocusField(); // fix bug46
      doSub("1");
      event.keyCode = 9;
      return false;
    }
    
    /* Key of Ctrl-DIAG = show diagnostics */
    if (keySeq == "CtrlDIAG") {
		x = window.showModalDialog (ctx + "Diagnostics.jsp", "q", "dialogWidth:640px; dialogHeight:480px; resizable:yes");
	}   
    
    /* Key of Ctrl-DEBUG = show debugging */
    if (keySeq == "CtrlDEBUG") {
		x = window.showModalDialog (ctx + "Debug.jsp", "q", "dialogWidth:640px; dialogHeight:480px");
	}   
    
    /* Key of Ctrl-TIME = show timings */
    if (keySeq == "CtrlTIME") {
		x = window.showModalDialog (ctx + "Timings.jsp", "q", "dialogWidth:640px; dialogHeight:480px; resizable:yes");
	}   
    
    /* Key of Ctrl-PING = show network response */
    if (keySeq == "CtrlPING") {
		x = window.showModalDialog (ctx + "Ping.jsp", new Date(), "dialogWidth:640px; dialogHeight:480px; resizable:yes");
	}   
    
    /* Key of Alt-PDB = show network & DB response */
    if (keySeq == "CtrlPDB") {
		x = window.showModalDialog (ctx + "PingDB.jsp", new Date(), "dialogWidth:640px; dialogHeight:480px; resizable:yes");
	}   
    
    /* Key of Alt-PEJB = show network & EJB response */
    if (keySeq == "CtrlPEJB") {
    	x = "redo";
    	ctr = 0;
    	while (x == "redo") {
    		ctr++;
    		x = window.showModalDialog (ctx + "PingEJB.jsp?ctr=" + ctr, new Date(), "dialogWidth:640px; dialogHeight:480px; resizable:yes");
    	}
	}   

    /* RPG translation of PageUp and PageDown */
    if ((thisKey == 33) || (thisKey == 34)) {
		if (thisKey == 33) {
			if (rollDownEnabled) {
/*			aForm.action_key.value = "PFKey29"*/
				aForm.action_key.value = "PFKey91";
			} else {
				//alert("Function key PAGEUP is not active on this screen at this time.");
				callCommonAlert(document.getElementById("langCode").value,"No00017");
				event.keyCode = 9;
				return false;
			}
		}
		else {
			if (rollUpEnabled) {
/*			aForm.action_key.value = "PFKey28";*/
				aForm.action_key.value = "PFKey90";
			} else {
				//alert("Function key PAGEDOWN is not active on this screen at this time.");
				callCommonAlert(document.getElementById("langCode").value,"No00018");
				event.keyCode = 9;
				return false;
			}
		}
      	doSub("2");
      	event.keyCode = 9;
      	return false;
	}

    /** This code makes down/up go down/up
       a whole row, and select the first field in the next line,
       i.e. like the mainframe "newline" key.
    */
    if ((thisKey == 38 || thisKey == 40) && event.ctrlKey == true) {
	    diffy = 9999;
	    minx = 9999;
	    dirn = 1;
	    if (thisKey == 38) dirn = -1;
	    candidate = thisElt;
	    left = candidate.parentElement.offsetLeft;
	    topp = candidate.parentElement.offsetTop;
	    //window.status = thisField + " Candidate " + candidate.name + " '" + candidate.type + "'";
	    for (var z=0; z<elts.length; z=z+1) {
	   	    anElt = elts[z];
    	    if (anElt == candidate) continue;
 		    if (thisKey == 38 && anElt.parentElement.offsetTop >= topp) continue;
    	    if (thisKey == 40 && anElt.parentElement.offsetTop <= topp) continue;
    	    if (topp < 0) return false;
   			if (selectableInputField(anElt) && !anElt.readOnly) {
   				diff = (anElt.parentElement.offsetTop - topp) * dirn;
   				if (diff > 0 && diff < diffy) {
   					candidate = elts[z];
   					diffy = diff;
   				}
   			}
   		}
	    /*for (var z=0; z<elts.length; z=z+1) {
    	    anElt = elts[z];
    	    if (anElt == candidate) continue;
    	    if (anElt.parentElement.offsetTop != candidate.parentElement.offsetTop) continue;
   			if (((anElt.type == "text") || (anElt.type == "select-one") || (anElt.type == "password") || (anElt.type == "textarea")) && !anElt.disabled && !anElt.readOnly) {
   				diff = (anElt.parentElement.offsetLeft - candidate.left);
   				if (diff > 0 && diff < minx) {
   					candidate = elts[z];
   					minx = diff;
   				}
   			}
   		}*/
		selectaField(candidate);
      	return false;
    }
    
    if (thisKey == 13) {	
    	if(event.srcElement.tagName.toUpperCase() != "TEXTAREA"){
    	clearFocusField(); // fix bug46
     	doSub("K");
      	return false;
      	}
    }
    
    if (thisKey == 9) {
    	selectNextLogicalField();
    }
        
    return true;
}

function checkKeyDBChoice() {
    if (event.keyCode == 13) {
    	clearFocusField(); // fix bug46
      doSub("3");
    }
}

String.prototype.gbtrim = function(len) {   
    var str;   
    if (len <= 0) {
    	str = '';
    } else if (this.length < len) {   
        str = this;   
    } else {
    	// will truncate but notice supplementary characters which takes two code units for one character
    	var code = this.charCodeAt(len-1);
    	if (code >= 0xD800 && code <= 0xDBFF) { // High surrogate of supplementary character
    		str = this.substr(0, len - 1);
    	} else {
    		str = this.substr(0, len);
    	}
    }
    return str;   
}  

function checkMaxLength (e, l) {
 if ( window.event.keyCode == '9' //tab
   || window.event.keyCode == '13' //enter
   || window.event.keyCode == '16' //shift
   || window.event.keyCode == '17' //ctrl
   || window.event.keyCode == '18' //alt
   || window.event.keyCode == '20' //caps lock
   || window.event.keyCode == '27' //esc
   || window.event.keyCode == '33' //page up
   || window.event.keyCode == '34' //page down
   || window.event.keyCode == '35' //end
   || window.event.keyCode == '36' //home
   || window.event.keyCode == '37' //left arrow
   || window.event.keyCode == '38' //up arrow
   || window.event.keyCode == '39' //right arrow
   || window.event.keyCode == '40' //down arrow
   || window.event.keyCode == '45' //print screen
   || window.event.keyCode == '93' //windows right-click
   || window.event.keyCode == '96' //ins key
   || (window.event.keyCode >= '112' && window.event.keyCode <= '123')   //down arrow
   || window.event.keyCode == '144' //num lock
   || window.event.keyCode == '145' //pause
   || window.event.keyCode == '8' //backspace
   || lastKey              == '18' //alt keys
    )
   return true;

	if (!l) {
		l = e.maxLength;
	}
    e.caretPos    = document.selection.createRange();
    var trimmedValue = e.value.gbtrim(e.maxLength);
    var save          = e.value;
    var olen      = e.maxLength;
    /* If text is selected, then needs to restore the selection range */
    if (e.caretPos.text.length > 0) { 
	    
	    e.maxLength   = e.maxLength + 1;
	    var bookmark  = "~";
	    var caretPos  = e.caretPos;
	    caretPos.text = bookmark;
	    var cp        = e.value.search( bookmark );
	    var newv      = e.value; 
	    e.value       = save;
	    e.maxLength   = olen;
	    /* Restore the selection range */
	    var upto      = cp + save.length - newv.length + 1;
	    if (cp==0 && upto==0) {
	    	cp+=1;
	    	upto+=1;
	    };
	    setSelectionRange(e, cp, upto);
	}
	/* If the length of trimmed value is less than original value, 
	 * then it indicates that the max length is reached. 
	 * Trim value as required, and then focuses the next field
	 */
	//Modified by shawn.wang, the e.maxLength may be changed in line 285 of this file.
	 var trimmedValue = save.gbtrim(olen);
	  

	if ( e.value.length >= e.maxLength) {
		nextOPrev = "NEXT";
		selectNextField(e);
		/* For unknown reason, selectNextField(e) might change value of current field which is weride.
		 * To ensure the value is correct, set trimmed value after it
		 */
		e.value = trimmedValue;
		return true;
	}
	else if (e.value.length == 0) {
		selectaField(thisElt);
		return false;
	}
	else {
		return true;
	}

}
/**
 *  @author	Francis Wang
 	@date 	2009-11-30 
 *	This function check defined text area's max length.
 */
function checkTextAreaMaxLength(e, maxLength) {
if ( window.event.keyCode == '9' //tab
   || window.event.keyCode == '13' //enter
   || window.event.keyCode == '16' //shift
   || window.event.keyCode == '17' //ctrl
   || window.event.keyCode == '18' //alt
   || window.event.keyCode == '20' //caps lock
   || window.event.keyCode == '27' //esc
   || window.event.keyCode == '33' //page up
   || window.event.keyCode == '34' //page down
   || window.event.keyCode == '35' //end
   || window.event.keyCode == '36' //home
   || window.event.keyCode == '37' //left arrow
   || window.event.keyCode == '38' //up arrow
   || window.event.keyCode == '39' //right arrow
   || window.event.keyCode == '40' //down arrow
   || window.event.keyCode == '45' //print screen
   || window.event.keyCode == '93' //windows right-click
   || window.event.keyCode == '96' //ins key
   || (window.event.keyCode >= '112' && window.event.keyCode <= '123')   //down arrow
   || window.event.keyCode == '144' //num lock
   || window.event.keyCode == '145' //pause
   || window.event.keyCode == '8' //backspace
   || lastKey              == '18' //alt keys
    )
   return true;
   
	if ( e.value.length >= maxLength) { 
		nextOPrev = "NEXT";
		selectNextField(e);
		/* For unknown reason, selectNextField(e) might change value of current field which is weride.
		 * To ensure the value is correct, set trimmed value after it
		 */
		e.value = e.value.gbtrim(maxLength);
		return true;
	}
	else if (e.value.length == 0) {
		selectaField(thisElt);
		return false;
	}
	else {
		return true;
	}
}

function checkTextAreaMaxLengthField(e, maxLength) {
	if ( window.event.keyCode == '9' //tab
	   || window.event.keyCode == '13' //enter
	   || window.event.keyCode == '16' //shift
	   || window.event.keyCode == '17' //ctrl
	   || window.event.keyCode == '18' //alt
	   || window.event.keyCode == '20' //caps lock
	   || window.event.keyCode == '27' //esc
	   || window.event.keyCode == '33' //page up
	   || window.event.keyCode == '34' //page down
	   || window.event.keyCode == '35' //end
	   || window.event.keyCode == '36' //home
	   || window.event.keyCode == '37' //left arrow
	   || window.event.keyCode == '38' //up arrow
	   || window.event.keyCode == '39' //right arrow
	   || window.event.keyCode == '40' //down arrow
	   || window.event.keyCode == '45' //print screen
	   || window.event.keyCode == '93' //windows right-click
	   || window.event.keyCode == '96' //ins key
	   || (window.event.keyCode >= '112' && window.event.keyCode <= '123')   //down arrow
	   || window.event.keyCode == '144' //num lock
	   || window.event.keyCode == '145' //pause
	   || window.event.keyCode == '8' //backspace
	   || lastKey              == '18' //alt keys
	    )
	   return true;
	   
		if ( e.value.length >= maxLength) { 
			nextOPrev = "NEXT";
			selectNextField(e);
			/* For unknown reason, selectNextField(e) might change value of current field which is weride.
			 * To ensure the value is correct, set trimmed value after it
			 */
			var count = e.value.split("\n").length-1
			e.value = e.value.gbtrim(maxLength+count);
			return true;
		}
		else if (e.value.length == 0) {
			//selectaField(thisElt); //IGJL-586
			return false;
		}
		else {
			return true;
		}
	}
/**
 *  @author	Francis Wang
 	@date 	2009-11-30 
 *	This function do sub string.
 */
function doSubstr(screenVar,index,maxlength,textareaName){

	var hiddenValue=document.getElementsByName(screenVar)[0];
    var textarea_innerText=document.getElementsByName(textareaName)[0].value;
	//var str=textarea_innerText.replace(/\r\n/ig,''); 
	hiddenValue.value=textarea_innerText.substr(index,maxlength);
	
	//alert(hiddenValue.value);
}
/**
 *  @author kle32
 *  @date   2014-07-31
 *  This function do Sub String and Trim String
 */
function doSubstrAndTrim(maxlength, textlength , value){
	var textarea_div=document.getElementById("textarea_div_"+value);
	var listHiddenField = textarea_div.getElementsByTagName("input");
    var textarea_innerText=document.getElementsByName("textarea_"+value)[0].value;   
    var hiddenValue = "";
    var leftText = 0;
    var rightText = 0;
    var index = 0;
    var newIndex = 0;    
    var subStr = "";
    var newStr = "";
//    var str = textarea_innerText.substr(index, textlength);
    for (var i = 0; i < listHiddenField.length; i ++){
    	hiddenValue = listHiddenField[i];
    	subStr = textarea_innerText.substr(index, textlength);    	
    		for (var j = 0; j <= textlength; j++){
    			if(subStr.substr(j,1).trim().length != 0){
    				leftText = j;
    				break;
    			}
    		}
    		rightText = textlength - subStr.trim().length - leftText;
    		if(leftText > 5 && rightText > 5){
    			if(textarea_innerText.substr(index+textlength,1).trim().length == 0){
    				newStr = subStr.replace(/\r\n/, "");    				
    			}else{
    				if(textarea_innerText.substr(index+textlength+1,1).trim().length != 0){
    					for (var x = textlength; x >= 0 ; x--){
        					if(textarea_innerText.substr(x+index,1).trim().length == 0){         						
        						newStr = textarea_innerText.substr(index, x).replace(/\r\n/, "");
        						index = index - textlength +x;
        						break;
        					}
        				}
    				}
    				
    			}
    			
    		}else if (leftText > 0){
    			if(textarea_innerText.substr(index+leftText+textlength,1).trim().length == 0){
    				newStr = textarea_innerText.substr(index + leftText, textlength).replace(/\r\n/, "");
    				index = index + leftText;
    			}else{
    				if(textarea_innerText.substr(index+textlength+leftText+1,1).trim().length != 0){
    					for (var x = textlength; x >= 0 ; x--){
        					if(textarea_innerText.substr(x+index+leftText,1).trim().length == 0){ 
        						newStr = textarea_innerText.substr(index + leftText, x).replace(/\r\n/, "");
        						index = index - textlength +x+leftText;
        						break;
        					}
        				}
    				}
    			}
    		
    		}else if(leftText <= 0){
    			if(textarea_innerText.substr(index+leftText+textlength,1).trim().length == 0){
    				newStr = textarea_innerText.substr(index + leftText, textlength).replace(/\r\n/, "");
    				index = index + leftText;
    			}else{
    				if(textarea_innerText.substr(index+textlength+leftText+1,1).trim().length != 0){
    					for (var x = textlength; x >= 0 ; x--){
        					if(textarea_innerText.substr(x+index+leftText,1).trim().length == 0){         						
        						newStr = textarea_innerText.substr(index + leftText, x).replace(/\r\n/, "");
        						index = index - textlength +x+leftText;
        						break;
        					}
        				}
    				}
    			}
    			
    		}
    		hiddenValue.value = newStr;
    		index += textlength;
    }

}

function doSubstrAndTrimField(maxlength, textlength , value){
	var textarea_div=document.getElementById("textarea_div_"+value);
	var listHiddenField = textarea_div.getElementsByTagName("input");
    var textarea_innerText=document.getElementsByName("textarea_"+value)[0].value;   
    var hiddenValue = "";
    var leftText = 0;
    var rightText = 0;
    var index = 0;
    var newIndex = 0;    
    var subStr = "";
    var newStr = "";
//    var str = textarea_innerText.substr(index, textlength);
    for (var i = 0; i < listHiddenField.length; i ++){
    	hiddenValue = listHiddenField[i];
    	subStr = textarea_innerText.substr(index, textlength);    	
    		for (var j = 0; j <= textlength; j++){
    			if(subStr.substr(j,1).trim().length != 0){
    				leftText = j;
    				break;
    			}
    		}
    		rightText = textlength - subStr.trim().length - leftText;
    		if(leftText > 5 && rightText > 5){
    			if(textarea_innerText.substr(index+textlength,1).trim().length == 0){
    				newStr = subStr.replace(/\r\n/, "");    				
    			}else{
    				if(textarea_innerText.substr(index+textlength+1,1).trim().length != 0){
    					for (var x = textlength; x >= 0 ; x--){
        					if(textarea_innerText.substr(x+index,1).trim().length == 0){         						
        						newStr = textarea_innerText.substr(index, x).replace(/\r\n/, "");
        						index = index - textlength +x;
        						break;
        					}
        				}
    				}
    				
    			}
    			
    		}else if (leftText > 0){
    			newStr = (textarea_innerText.trim().replace(/[\n\r]/g,'')).substr(index , maxlength);
    				index = index + leftText;
    		
    		
    		}else if(leftText <= 0){
    				newStr = (textarea_innerText.trim().replace(/[\n\r]/g,'')).substr(index + leftText, textlength);
    				index = index + leftText;
    		
    			
    		}
    		hiddenValue.value = newStr;
    		index += textlength;
    }

}
		
		
function clickSelect(selField) {
	selField.value = window.showModalDialog (getCommonScreens("popupt.jsp"), new Array(selField), "dialogWidth:25; dialogHeight:25");
}

function delayedBlink() {
	for (var i=0; i<blinkArray.length; i++) {
		blinkElt = blinkArray[i];
		fcolor = blinkElt.currentStyle.color;
		blinkElt.style.color = blinkElt.currentStyle.backgroundColor;
		blinkElt.style.backgroundColor = fcolor;
	}
	setTimeout("delayedBlink()", 1000);
}
/* Only , . + - and 0 to 9 is allowed. 
 * Meanwhile, decimal point will be validated.
 */
function digitsOnly(field, decimalPlace) {

	if (window.event.altKey || window.event.ctrlKey) {
		return true;
	}
	
	if ( window.event.keyCode == '9' //tab
	   || window.event.keyCode == '13' //enter
	   || window.event.keyCode == '16' //shift
	   || window.event.keyCode == '17' //ctrl
	   || window.event.keyCode == '18' //alt
	   || window.event.keyCode == '20' //caps lock
	   || window.event.keyCode == '27' //esc
	   || window.event.keyCode == '33' //page up
	   || window.event.keyCode == '34' //page down
	   || window.event.keyCode == '35' //end
	   || window.event.keyCode == '36' //home
	   || window.event.keyCode == '37' //left arrow
	   || window.event.keyCode == '38' //up arrow
	   || window.event.keyCode == '39' //right arrow
	   || window.event.keyCode == '40' //down arrow
	   || window.event.keyCode == '45' //print screen
	   || window.event.keyCode == '93' //windows right-click
	   || window.event.keyCode == '96' //ins key
	   || window.event.keyCode == '144' //num lock
	   || window.event.keyCode == '145' //pause
	   || lastKey              == '18' //alt keys
	    ) {
	   return true;
	}

	var keycode0 = window.event.keyCode;

	//non-numbers is not allowed
	if (keycode0 < 43 || keycode0 == 47 || keycode0 > 57 || keycode0 == '37') {
		return false;
	}
	//+ or - is always allowed
	if ((keycode0 == 43)  || (keycode0 == 45)) {
		return true;
	}
	//If content is selected, then it is supposed to be replaced.`
   	var selected = document.selection.createRange();
   	selected.text = "";

	fValue = field.value;
	
	var hasDecimal = fValue.indexOf(".");
	
	//comma is only allowed when decimal point is not entered yet
	if (keycode0 == 44) {
		if (hasDecimal == -1) {
			return true;
		} else {
			return false;
		}
	}
	
	if (!decimalPlace) {
		decimalPlace = 0;
	}

	//Decimal point
	if (keycode0 == 46) {
		//If decimal is not allowed, or already entered
		if ((decimalPlace == 0) || (hasDecimal > -1)) {
			//alert("Incorrect decimal place!");
			callCommonAlert(document.getElementById("lang1").value,"No0003");
			return false;
		} else {
			var savedValue = field.value;
			//selected.text = ".";
			var insertPosition = field.value.indexOf(".") + 1;
			var pNoOfDecimal = field.value.length - insertPosition;

			if (pNoOfDecimal < 0) {
				//As dot has been added, then don't add it again although it means alright.
				return false;
			}
			//As there could be sign after decimal point, then needs to take them out of count
			var decimalPart = field.value.substr(insertPosition, decValue.length-insertPosition);
			decimalPart = decimalPart.replace(/[^0-9\.]/, "");

			pNoOfDecimal = decimalPart.length;
			//alert("pNoOfDecimal: " + pNoOfDecimal + ", decimalPlace: " + decimalPlace);
			if (pNoOfDecimal > decimalPlace) {
				//alert("Incorrect decimal place!");
				callCommonAlert(document.getElementById("lang1").value,"No0003");
				//Restore the value without dot added.
				field.value = savedValue;
				return false;
			} else {
				//As dot has been added, then don't add it again although it means alright.
				return false;
			}
		}
	}

	//No decimal allowed? then it's okey.
	if (decimalPlace ==0) {
		return true;
	}
	//Since there will be spaces after the number, The noDecimalPlaces will some times larger than decimalPlace.
	// leave this check to isValidDigits();
	
	//Get existing no of decimal places
	var noDecimalPlaces = 0;
	if (hasDecimal >= 0 && hasDecimal < fValue.length -1) {
		noDecimalPlaces = fValue.length - hasDecimal - 1;
	}
	
	//If already reach the maximum, then not allowed
	
	//alert('noDecimalPlaces==  '+noDecimalPlaces+'  decimalPlace=='+decimalPlace);
	if (noDecimalPlaces > decimalPlace) {
		return false; 
	}
	return true;
}


// This function is added for more button functionality for UI enhanced screens.
function pressMoreButton(act) {
	
	if (rollUpEnabled) {
		aForm.action_key.value = act;
		doSub("4");
	
	} else {
		alert("No more Records to display..");
		event.keyCode = 9;
		return false;
	}
}
function pressMoreButton(act,language) {
	
	if (rollUpEnabled) {
		aForm.action_key.value = act;
		doSub("4");
	
	} else {
		if(language == 'chi/' || language == 'CHI/'){
			alert("没有更多的记录了..");
		}else{
			alert("No more Records to display..");
		}
		event.keyCode = 9;
		return false;
	}
}
function doAction(act) {
	//fo = document.forms;
	alert("SSSSSSSS");
	aForm.action_key.value = act;
	alert("AAAAA");
	doSub("4");
}
  
function doActionInIframe(act) {
	aForm.isInIframe.value = '1';
	doAction(act);
}

function doAdd() {
    aForm.action_key.value = "QPValidNewRow";
    doSub("5");
}

function doBlink() {
    foundany = false;
    blinki = 0;
	for (var i=0; i<elts.length; i++) {
		if (elts[i].className.indexOf(" blink") > 0) {
			foundany = true;
			blinkArray[blinki] = elts[i];
			blinki++;
		}
	}
	if (foundany) {
		setTimeout("delayedBlink()", 1000);
	}
}

function doBlur(pBlurObj) {
    blurParm = pBlurObj
    x = setTimeout('doBlur1()',10);
}
  
function doBlur1() {
    doPost(blurParm);
}

function doBlur2(pBlurObj, pMethod) {
    blur2Parm = pBlurObj
    blur2Meth = pMethod
    x = setTimeout('doBlur2a()',10);
}

function doBlur2a() {
    doPost2(blur2Parm, blur2Meth);
}

function doBlurHelp(pBlurObj) {
    blurParm = pBlurObj
    x = setTimeout('doBlur1Help()',10);
    return false;
}

function doBlur1Help() {
    doPostHelp(blurParm);
}

function doBlur2Help(pBlurObj, pMethod) {
    blur2Parm = pBlurObj
    blur2Meth = pMethod
    x = setTimeout('doBlur2aHelp()',10);
    return false;
}

function doBlur2aHelp() {
    doPost2Help(blur2Parm, blur2Meth);
}

/*
	To handle the blur event for number field.
	if content of the field is not valid, then focus and alert
*/
function doBlurNumber(objEvent) {

	var objInput = objEvent.srcElement;
	var decimal;
	//bug #IGROUP-997 start
	if ($(objInput).attr("decimal")) {
		//decimal = objInput.decimal;
		decimal = $(objInput).attr("decimal");
	} else {
		decimal = 0;
	}
	//bug #IGROUP-997 end
	
	// bug65 fix the minus was inputed
	if(objInput.value.indexOf("-")==-1){
		if(objInput.value.indexOf(".")==-1){//if integer was inputed
			if(decimal > 0 
					&& objInput.value.length > (objInput.maxLength-decimal-1)){
			 	//alert("Use of decimals not correct or too many numbers entered.");
				callCommonAlert(document.getElementById("lang1").value,"No0004");
			 	objInput.focus();
				return false;	
			}
		}else{
			var index=objInput.value.indexOf(".");
			if(index > (objInput.maxLength-decimal-1) ){
				//alert("Use of decimals not correct or too many numbers entered.");
				callCommonAlert(document.getElementById("lang1").value,"No0004");
				objInput.focus();
				return false;
			}
		}
	}else{//positive number was inputed
		if(objInput.value.indexOf(".")==-1){//if integer was inputed	
			if(objInput.value.length > (objInput.maxLength-decimal)){
			 	//alert("Use of decimals not correct or too many numbers entered.");
				callCommonAlert(document.getElementById("lang1").value,"No0004");
			 	objInput.focus();
				return false;
			}  
		}else{//decimal number was inputed 
			var index=objInput.value.indexOf(".");
			if(index > (objInput.maxLength-decimal) ){
				//alert("Use of decimals not correct or too many numbers entered.");
				callCommonAlert(document.getElementById("lang1").value,"No0004");
				objInput.focus();
				return false;
			}
		}
	}
	
	if (objInput.precentFlag == '1') {
		if (!isValidDigitsContainPre(objInput.value, objInput.maxLength, decimal, true)) {
			if (focusInLast == null) {
				objInput.focus();
				focusInLast = objInput;
			} else {
				focusInLast.focus();
			}
			return false;
		}
	} else {
		if (!isValidDigits(objInput.value, objInput.maxLength, decimal, true)) {
			if (focusInLast == null) {
				objInput.focus();
				focusInLast = objInput;
			} else {
				focusInLast.focus();
			}
			return false;
		}
	}
	focusInLast = null;

	return true;
}

/*
 * Improvement the doBlurNumber method
 * */
function doBlurNumber1(objEvent) {

	var objInput = objEvent.srcElement;
	var decimal;
	if (objInput.decimal) {
		decimal = objInput.decimal;
	} else {
		decimal = 0;
	}
	
	if(objInput.value.indexOf("-")==-1){//positive number was inputed, 777.77 is correct
		if(objInput.value.indexOf(".")==-1) {
			//if integer was inputed
			if(decimal > 0 
					&& objInput.value.trim().length > (objInput.maxLength-decimal-1)) {
				//if two many numbers entered: 77777
				callCommonAlert(document.getElementById("lang1").value,"No0004");
			 	objInput.focus();
				return false;	
			} else if (decimal == 0 && objInput.value.length == objInput.maxLength) {
				//if two many numbers entered: 77777
				callCommonAlert(document.getElementById("lang1").value,"No0004");
			 	objInput.focus();
				return false;	
			}
		} else {
			//decimal number was inputed
			var index=objInput.value.indexOf(".");
			if(index >= (objInput.maxLength-decimal) ){
				//if too many number entered before "." 7777.77
				callCommonAlert(document.getElementById("lang1").value,"No0004");
				objInput.focus();
				return false;
			} else if (decimal < (objInput.value.length-index-1)) {
				//if too many number entered after "." 777.777
				callCommonAlert(document.getElementById("lang1").value,"No0004");
				objInput.focus();
				return false;
			}
		}
	}else{//negative number was inputed, 777.77- is correct
		if(objInput.value.indexOf(".")==-1){
			//if integer was entered
			if(decimal > 0 && objInput.value.length > (objInput.maxLength-decimal-1)){
				//if two many numbers entered 777777-
				callCommonAlert(document.getElementById("lang1").value,"No0004");
			 	objInput.focus();
				return false;
				
			}
		}else{
			//decimal number was inputed
			var index=objInput.value.indexOf(".");
			if(index > (objInput.maxLength-decimal-1) ){
				//if too many number entered before "." 7777.7-
				callCommonAlert(document.getElementById("lang1").value,"No0004");
				objInput.focus();
				return false;
			} else if(decimal < (objInput.value.length-index-1) ){
				//if too many number entered after "."  77.777-
				callCommonAlert(document.getElementById("lang1").value,"No0004");
				objInput.focus();
				return false;
			}
		}
	}
	
	if (objInput.precentFlag == '1') {
		if (!isValidDigitsContainPre(objInput.value, objInput.maxLength, decimal, true)) {
			if (focusInLast == null) {
				objInput.focus();
				focusInLast = objInput;
			} else {
				focusInLast.focus();
			}
			return false;
		}
	} else {
		if (!isValidDigits(objInput.value, objInput.maxLength, decimal, true)) {
			if (focusInLast == null) {
				objInput.focus();
				focusInLast = objInput;
			} else {
				focusInLast.focus();
			}
			return false;
		}
	}
	focusInLast = null;

	return true;
}

function doDateDown(obj) {

    if (activity == true) {
      event.returnValue=false;
      return false;
    }

    kd     = event.keyCode;
    activity = true;

    /* PF keys - not our concern. */
    if (kd >= 113 && kd <= 123) {
      doDateBlur(obj); 
      activity = false;
      return true;
    }

    /* Save the current selection range, for later to work out where the cursor is. */
    obj.caretPos = document.selection.createRange();
    before = obj.value;
    if (cp > 0) {
      b4cp   = cp;
    }

    /* The following keys are OK: 0-9, 0-9 numeric keypad, delete, slash, "c=calendar", "d=day", "m=month", "y=year"       +          +         -          -*/
    if ((kd>=48 & kd<=57) || (kd>=96 & kd<=105) || kd==46 || kd==191 || kd==67 || kd==68 || kd==77 || kd==89|| kd==107 || kd==187 || kd==109 || kd==189) {
      return true;
    }

    activity = false;

    /* These keys can safely auto-repeat: esc (clears), =(current date), cursor keys */
    /*  esc       equals     home       end       tab                left      right     tab*/
    if (kd==27 || kd==187 || kd==36  || kd==35 || kd==9 || kd==13 || kd==37 || kd==39 || kd==8 ) {
      if (kd == lk) {
        lkc++;
      }
      else {
        lk  = kd;
        lkc = 1;
     }
     if (kd==8) {
       event.keyCode = 37;
     }
     return true;
    }

    event.returnValue=false;
}

function doDateUp(obj) {

    after  = obj.value;
    ku      = event.keyCode;
    cp     = getCaretPos(obj);
    pic    = getDateFormat("9999");

    /* Case 0: "c" key pressed == popup calendar */
    if (ku == 67) {
      after = window.showModalDialog (ctx + "popupc.jsp", before, "dialogWidth:22; dialogHeight:22");
      obj.value = after;
      setSelectionRange(obj, cp, cp+1);
      lastGoodDate = after;
      if (debugkeys) window.status = "Case 0 Up '" + before + "' '" + after + "' " + ku;
      activity = false;
      return false;
    }

    /* Case 1: Today's date */
    if (ku == 187) {
      after = todayDate;
      obj.value = after;
      setSelectionRange(obj, cp, cp+1);
      lastGoodDate = after;
      if (debugkeys) window.status = "Case 1 Up '" + before + "' '" + after + "' " + ku;
      activity = false;
      return false;
    }

    /* Case 2: replaced the whole string with a number */
    if (((ku >= 48 & ku <= 57) | (ku >= 96 & ku <= 105)) & after.length == 1) {
      after = after + getDateFormat("224");
      obj.value = after;
      if (after.substring(0,1) != " ") {
        setSelectionRange(obj, 1, 2);
      }
      else {
        setSelectionRange(obj, 0, 1);
      }
      lastGoodDate = after;
      if (debugkeys) window.status = "Case 2 Up '" + before + "' '" + after + "' " + ku;
      activity = false;
      return;
    }

    /* Case 3: Escape clears the date */
    if (ku == 27) {
      after = "";
      obj.value = after;
      setSelectionRange(obj, 1, 2);
      lastGoodDate = after;
      if (debugkeys) window.status = "Case 3 Up '" + before + "' '" + after + "' " + ku;
      activity = false;
      return;
    }

    if (after.length != 10) {
      after = fixDels(10, getDateFormat("9999"));
    }

    /* Case 4: delete */
    if (ku == 46) {
      while (cp < pic.length && pic.substring(cp, cp+1) == getDateFormatChar()) {
        cp++;
      }
      if (cp >= 0) {
        var j = cp;
        while (j < pic.length && pic.substring(j, j+1) == "9") {
          j++;
        }
        after = after.substring(0,cp) + after.substring(cp+1,j) + " " + after.substring(j);
      }
      obj.value = after;
      setSelectionRange(obj, cp, cp+1);
      lastGoodDate = after;
      if (debugkeys) window.status = "Case 4 Up '" + before + "' '" + after + "' " + ku + " " + ti + " " + j;
      activity = false;
      return;
    }

    /* Case 5: Cursor movement */
    if (before == after && (ku==39 || ku==37 || ku==8 || ku==36 || ku==35)) {
      /* Sometimes cp gets corrupted by fast typing. Restore it, if possible. */
      if (cp <= 0 && b4cp != 0) {
        cp = b4cp;
      }
      if (ku == 39) {cp = cp + lkc; if (cp==2 || cp==5) cp++;}
      else if (ku == 37 || ku == 8) {cp = cp - lkc; if (cp==2 || cp==5) cp--;}
      else if (ku == 36) cp = 0;
      else if (ku == 35) cp = 9;
      if (cp < 0)  cp = 0;
      if (cp >  9) cp = 9;
      setSelectionRange(obj, cp, cp+1);
      if (debugkeys) window.status = "Case 5b Up '" + before + "' '" + after + "' " + ku + " " + b4cp + " " + cp + " lkc=" + lkc;
      activity = false;
      b4cp = cp;
      lkc  = 0;
      return;
    }

    /* Case 6: Slash */
    if (ku == 191) {
      if (cp < 2) {
        if (before.substring(0,2) == "  ") before = "01" + before.substring(2);
        else if (before.substring(0,1) == " ") before = "0" + before.substring(1);
        else if (before.substring(1,2) == " ") before = "0" + before.substring(0,1) + before.substring(2);
        cp = 3;
      }
      else if (cp < 5) {
        if (before.substring(3,5) == "  ") before = before.substring(0,3) + "01" + before.substring(5);
        else if (before.substring(3,4) == " ")  before = before.substring(0,3) + "0" + before.substring(4);
        else if (before.substring(4,5) == " ") before = before.substring(0,3) + "0" + before.substring(3,4) + before.substring(5);
        cp = 6;
      }
      after = before;
      obj.value = after;
      setSelectionRange(obj, cp, cp+1);
      lastGoodDate = after;
      if (debugkeys) window.status = "Case 6 Up '" + before + "' '" + after + "' " + ku;
      activity = false;
      return;
    }


    /* Case 7: Number */
    if ((ku >= 48 & ku <= 57) | (ku >= 96 & ku <= 105)) {
      /* Sometimes cp gets corrupted by fast typing. Restore it, if possible. */
      if (cp == 0 && b4cp != 0) {
        cp = b4cp;
      }
      cp++;
      if (cp==2 || cp==5) cp++;
      if (cp >  9) cp = 9;
      /* Test for corrupted date - overtyping. */
      if (after.substring(2,3) != getDateFormatChar() || after.substring(5,6) != getDateFormatChar() || after.length != 10) {
        errs = 0;
        corr = "";
        for (var ti=0; ti<after.length; ti++) {
          c = after.substring(ti, ti+1);
          if ((ti == 2 || ti == 5) && c != " ") {errs++; c = getDateFormatChar();}
          else if (" 0123456789".indexOf(c) < 0) {errs++; c = "0";}
          corr = corr + c;
        }
        if (errs > 0) {
          //alert("Buffer overrun. Please don't type so fast in a date field!");
		  callCommonAlert(document.getElementById("langCode").value,"No0005");
          after = corr;
        }
      }
      obj.value = after;
      setSelectionRange(obj, cp, cp+1);
      lastGoodDate = after;
      if (debugkeys) window.status = "Case 7 Up '" + before + "' '" + after + "' " + ku + " " + cp;
      activity = false;
      return;
    }

    /* Case 8 - Tabbed into the field */
    if (ku == 9) {
      if (debugkeys) window.status = "Case 8 Up '" + before + "' '" + after + "' " + ku + " " + cp;
      activity = false;
      obj.select();
      obj.focus();
    }

    /* Case 9 - inc d,m,y */
    if (ku==107 || ku==187 || ku==109 || ku==189) {
      after = before;
      anum = 0;
      if (cp == 0 || cp == 1) {
        astr = after.substring(0,2);
        if (isNaN(astr)) astr = "00";
        if (ku==107 || ku==187) anum = parseInt(astr,10) + 1;
        else                    anum = parseInt(astr,10) - 1;
        dnum = anum;
        if (anum > 31) anum = 31;
        if (anum <  1) anum = 1;
        astr = anum.toString();
        dstr = astr;
        if (astr.length == 1) astr = "0" + astr;
        after = astr + after.substring(2);
      }
      if (cp == 3 || cp == 4) {
        astr = after.substring(3,5);
        if (isNaN(astr)) astr = "00";
        if (ku==107 || ku==187) anum = parseInt(astr,10) + 1;
        else                    anum = parseInt(astr,10) - 1;
        if (anum > 12) anum = 12;
        if (anum <  1) anum = 1;
        astr = anum.toString();
        if (astr.length == 1) astr = "0" + astr;
        after = after.substring(0,3) + astr + after.substring(5);
      }
      if (cp >= 6 && cp <= 9) {
        astr = after.substring(6);
        if (isNaN(astr)) astr = "0000";
        if (ku==107 || ku==187) anum = parseInt(astr,10) + 1;
        else                    anum = parseInt(astr,10) - 1;
        if (anum > 3000) anum = 3000;
        if (anum < 1900) anum = 1900;
        astr = anum.toString();
        after = after.substring(0,6) + astr;
      }
      obj.value = after;
      setSelectionRange(obj, cp, cp+1);
      lastGoodDate = after;
      if (debugkeys) window.status = "Case 9 Up '" + before + "' '" + after + "' " + ku + " " + cp + " '" + dstr + "' " + dnum + " " + lkc ;
      activity = false;
      return;
    }

    if (ku==68 || ku==77 || ku==89) {
      obj.value = before;
      if (ku==68) {
        cp = 0;
        setSelectionRange(obj, 0, 2);
      }
      else if (ku==77) {
        cp = 3;
        setSelectionRange(obj, 3, 5);
      }
      else {
        cp = 6;
        setSelectionRange(obj, 6, 10);
      }
      if (debugkeys) window.status = "Case 10 Up '" + before + "' '" + after + "' " + ku + " " + cp;
      activity = false;
      return;
    }

    if (debugkeys) window.status = "Case 11 Up '" + before + "' '" + after + "' " + ku + " " + cp;
    activity = false;
    setSelectionRange(obj, 0, 1);
    b4cp = 0;
    cp = 0;
    return;

}

function doDate8Up(obj) {

    after  = obj.value;
    ku      = event.keyCode;
    cp     = getCaretPos(obj);
    pic    = getDateFormat("99");

    /* Case 0: "c" key pressed == popup calendar */
    if (ku == 67) {
      after = window.showModalDialog (ctx + "popupc.jsp", before, "dialogWidth:22; dialogHeight:22");
      after = after.substring(0,6) + after.substring(8);	      
      obj.value = after;
      setSelectionRange(obj, cp, cp+1);
      lastGoodDate = after;
      if (debugkeys) window.status = "Case 0 Up '" + before + "' '" + after + "' " + ku;
      activity = false;
      return false;
    }

    /* Case 1: Today's date */
    if (ku == 187) {
      after = todayDate;
      after = after.substring(0,6) + after.substring(8);	      
      obj.value = after;
      setSelectionRange(obj, cp, cp+1);
      lastGoodDate = after;
      if (debugkeys) window.status = "Case 1 Up '" + before + "' '" + after + "' " + ku;
      activity = false;
      return false;
    }

    /* Case 2: replaced the whole string with a number */
    if (((ku >= 48 & ku <= 57) | (ku >= 96 & ku <= 105)) & after.length == 1) {
      after = after + getDateFormat("222");
      obj.value = after;
      if (after.substring(0,1) != " ") {
        setSelectionRange(obj, 1, 2);
      }
      else {
        setSelectionRange(obj, 0, 1);
      }
      lastGoodDate = after;
      if (debugkeys) window.status = "Case 2 Up '" + before + "' '" + after + "' " + ku;
      activity = false;
      return;
    }

    /* Case 3: Escape clears the date */
    if (ku == 27) {
      after = "";
      obj.value = after;
      setSelectionRange(obj, 1, 2);
      lastGoodDate = after;
      if (debugkeys) window.status = "Case 3 Up '" + before + "' '" + after + "' " + ku;
      activity = false;
      return;
    }

    if (after.length != 8) {
      after = fixDels(8, getDateFormat("99"));
    }

    /* Case 4: delete */
    if (ku == 46) {
      fixd = after;
      while (cp < pic.length && pic.substring(cp, cp+1) == getDateFormatChar()) {
        cp++;
      }
      if (cp >= 0) {
        var j = cp;
        while (j < pic.length && pic.substring(j, j+1) == "9") {
          j++;
        }
        after = after.substring(0,cp) + after.substring(cp+1,j) + " " + after.substring(j);
      }
      obj.value = after;
      setSelectionRange(obj, cp, cp+1);
      lastGoodDate = after;
      if (debugkeys) window.status = "Case 4 Up '" + before + "' '" + fixd + "' '" + after + "' " + ku + " " + ti + " " + j;
      activity = false;
      return;
    }

    /* Case 5: Cursor movement */
    if (before == after && (ku==39 || ku==37 || ku==8 || ku==36 || ku==35)) {
      /* Sometimes cp gets corrupted by fast typing. Restore it, if possible. */
      entry = cp + " " + b4cp;
      if (cp <= 0 && b4cp > 0) {
        cp = b4cp;
      }
      if (ku == 39) {cp = cp + lkc; if (cp==2 || cp==5) cp++;}
      else if (ku == 37 || ku == 8) {cp = cp - lkc; if (cp==2 || cp==5) cp--;}
      else if (ku == 36) cp = 0;
      else if (ku == 35) cp = 9;
      if (cp < 0)  cp = 0;
      if (cp >  9) cp = 9;
      setSelectionRange(obj, cp, cp+1);
      entry = entry + " cp now " + cp + " ";
      if (debugkeys) window.status = "Case 5b Up ' entry=" + entry + " " + before + "' '" + after + "' " + ku + " " + b4cp + " " + cp + " lkc=" + lkc;
      activity = false;
      b4cp = cp;
      lkc  = 0;
      return;
    }

    /* Case 6: Slash */
    if (ku == 191) {
      if (cp < 2) {
        if (before.substring(0,2) == "  ") before = "01" + before.substring(2);
        else if (before.substring(0,1) == " ") before = "0" + before.substring(1);
        else if (before.substring(1,2) == " ") before = "0" + before.substring(0,1) + before.substring(2);
        cp = 3;
      }
      else if (cp < 5) {
        if (before.substring(3,5) == "  ") before = before.substring(0,3) + "01" + before.substring(5);
        else if (before.substring(3,4) == " ")  before = before.substring(0,3) + "0" + before.substring(4);
        else if (before.substring(4,5) == " ") before = before.substring(0,3) + "0" + before.substring(3,4) + before.substring(5);
        cp = 6;
      }
      after = before;
      obj.value = after;
      setSelectionRange(obj, cp, cp+1);
      lastGoodDate = after;
      if (debugkeys) window.status = "Case 6 Up '" + before + "' '" + after + "' " + ku;
      activity = false;
      return;
    }


    /* Case 7: Number */
    if ((ku >= 48 & ku <= 57) || (ku >= 96 & ku <= 105)) {
      /* Sometimes cp gets corrupted by fast typing. Restore it, if possible. */
      if (cp == 0 && b4cp != 0) {
        cp = b4cp;
      }
      cp++;
      if (cp==2 || cp==5) cp++;
      if (cp >  7) cp = 7;
      /* Test for corrupted date - overtyping. */
      if (after.substring(2,3) != getDateFormatChar() || after.substring(5,6) != getDateFormatChar() || after.length != 8) {
        errs = 0;
        corr = "";
        for (var ti=0; ti<after.length; ti++) {
          c = after.substring(ti, ti+1);
          if ((ti == 2 || ti == 5) && c != " ") {errs++; c = getDateFormatChar();}
          else if (" 0123456789".indexOf(c) < 0) {errs++; c = "0";}
          corr = corr + c;
        }
        if (errs > 0) {
          //alert("Buffer overrun. Please don't type so fast in a date field!");
		  callCommonAlert(document.getElementById("langCode").value,"No0005");
          after = corr;
        }
      }
      obj.value = after;
      setSelectionRange(obj, cp, cp+1);
      lastGoodDate = after;
      if (debugkeys) window.status = "Case 7 Up '" + before + "' '" + after + "' " + ku + " " + cp;
      activity = false;
      return;
    }

    /* Case 8 - Tabbed into the field */
    if (ku == 9) {
      if (debugkeys) window.status = "Case 8 Up '" + before + "' '" + after + "' " + ku + " " + cp;
      activity = false;
      obj.select();
      obj.focus();
    }

    /* Case 9 - inc d,m,y */
    if (ku==107 || ku==187 || ku==109 || ku==189) {
      after = before;
      anum = 0;
      if (cp == 0 || cp == 1) {
        astr = after.substring(0,2);
        if (isNaN(astr)) astr = "00";
        if (ku==107 || ku==187) anum = parseInt(astr,10) + 1;
        else                    anum = parseInt(astr,10) - 1;
        dnum = anum;
        if (anum > 31) anum = 31;
        if (anum <  1) anum = 1;
        astr = anum.toString();
        dstr = astr;
        if (astr.length == 1) astr = "0" + astr;
        after = astr + after.substring(2);
      }
      if (cp == 3 || cp == 4) {
        astr = after.substring(3,5);
        if (isNaN(astr)) astr = "00";
        if (ku==107 || ku==187) anum = parseInt(astr,10) + 1;
        else                    anum = parseInt(astr,10) - 1;
        if (anum > 12) anum = 12;
        if (anum <  1) anum = 1;
        astr = anum.toString();
        if (astr.length == 1) astr = "0" + astr;
        after = after.substring(0,3) + astr + after.substring(5);
      }
      if (cp >= 6 && cp <= 7) {
        astr = after.substring(6);
        if (isNaN(astr)) astr = "00";
        if (ku==107 || ku==187) anum = parseInt(astr,10) + 1;
        else                    anum = parseInt(astr,10) - 1;
        if (anum > 99) anum = 99;
        if (anum < 0)  anum = 00;
        astr = anum.toString();
        after = after.substring(0,6) + astr;
      }
      obj.value = after;
      setSelectionRange(obj, cp, cp+1);
      lastGoodDate = after;
      if (debugkeys) window.status = "Case 9 Up '" + before + "' '" + after + "' " + ku + " " + cp + " '" + dstr + "' " + dnum + " " + lkc ;
      activity = false;
      return;
    }

    if (ku==68 || ku==77 || ku==89) {
      obj.value = before;
      if (ku==68) {
        cp = 0;
        setSelectionRange(obj, 0, 2);
      }
      else if (ku==77) {
        cp = 3;
        setSelectionRange(obj, 3, 5);
      }
      else {
        cp = 6;
        setSelectionRange(obj, 6, 8);
      }
      if (debugkeys) window.status = "Case 10 Up '" + before + "' '" + after + "' " + ku + " " + cp;
      activity = false;
      return;
    }

    if (debugkeys) window.status = "Case 11 Up '" + before + "' '" + after + "' " + ku + " " + cp;
    activity = false;
    setSelectionRange(obj, 0, 1);
    b4cp = 0;
    cp = 0;
    return;

}

function doDate8Down(obj) {

    if (activity == true) {
      event.returnValue=false;
      return false;
    }

    kd     = event.keyCode;
    activity = true;

    /* PF keys - not our concern. */
    if (kd >= 113 && kd <= 123) {
      doDate8Blur(obj); 
      activity = false;
      return true;
    }

    /* Save the current selection range, for later to work out where the cursor is. */
    obj.caretPos = document.selection.createRange();
    before = obj.value;
    if (cp > 0) {
      b4cp   = cp;
    }

    /* The following keys are OK: 0-9, 0-9 numeric keypad, delete, slash, "c=calendar", "d=day", "m=month", "y=year"       +          +         -          -*/
    if ((kd>=48 & kd<=57) || (kd>=96 & kd<=105) || kd==46 || kd==191 || kd==67 || kd==68 || kd==77 || kd==89|| kd==107 || kd==187 || kd==109 || kd==189) {
      return true;
    }

    activity = false;

    /* These keys can safely auto-repeat: esc (clears), =(current date), cursor keys */
    /*  esc       equals     home       end       tab                left      right     tab*/
    if (kd==27 || kd==187 || kd==36  || kd==35 || kd==9 || kd==13 || kd==37 || kd==39 || kd==8 ) {
      if (kd == lk) {
        lkc++;
      }
      else {
        lk  = kd;
        lkc = 1;
     }
     if (kd==8) {
       event.keyCode = 37;
     }
     return true;
    }

    event.returnValue=false;
}

function doDateBlur(obj) {

    v = obj.value;
    if (v.length == 0) {
      return;
    }

    if (v == getDateFormat("224")) {
      return;
    }

    while (v.length < 10) {
      v = v + " ";
    }

         if (v.substring(0,2) == "  ")     v = "01" + v.substring(2);
    else if (v.substring(0,1) == " ")      v = "0" + v.substring(1);
    else if (v.substring(1,2) == " ")      v = "0" + v.substring(0,1)  + v.substring(2);
         if (v.substring(3,5) == "  ")     v = v.substring(0,3) + "01" + v.substring(5);
    else if (v.substring(3,4) == " ")      v = v.substring(0,3) + "0"  + v.substring(4);
    else if (v.substring(4,5) == " ")      v = v.substring(0,3) + "0"  + v.substring(3,4) + v.substring(5);

    y = v.substring(6).trim();

    if (y.length == 0) {
      v = v.substring(0,6) + todayDate.substring(6);
    }
    if (y.length == 1) {
      v = v.substring(0,6) + "200" + y;
    }
    else if (y.length == 2) {
      if (y <= "09") {
        v = v.substring(0,6) + "20" + y;
      }
      else {
        v = v.substring(0,6) + "19" + y;
      }
    }
    else if (y.length == 3) {
      if (y <= "009") {
        v = v.substring(0,6) + "2" + y;
      }
      else {
        v = v.substring(0,6) + "1" + y;
      }
    }
    v = v.replace(/ /, "0");

    obj.value = v;
}

/* Blur function for short dates */
function doDate8Blur(obj) {

    v = obj.value;
    if (v.length == 0) {
      return;
    }

    if (v == getDateFormat("222")) {
      return;
    }

    while (v.length < 8) {
      v = v + " ";
    }

         if (v.substring(0,2) == "  ")     v = "01" + v.substring(2);
    else if (v.substring(0,1) == " ")      v = "0" + v.substring(1);
    else if (v.substring(1,2) == " ")      v = "0" + v.substring(0,1)  + v.substring(2);
         if (v.substring(3,5) == "  ")     v = v.substring(0,3) + "01" + v.substring(5);
    else if (v.substring(3,4) == " ")      v = v.substring(0,3) + "0"  + v.substring(4);
    else if (v.substring(4,5) == " ")      v = v.substring(0,3) + "0"  + v.substring(3,4) + v.substring(5);

    y = v.substring(6).trim();

    if (y.length == 0) {
      v = v.substring(0,6) + todayDate.substring(8);
    }
    if (y.length == 1) {
      v = v.substring(0,6) + "0" + y;
    }
    v = v.replace(/ /, "0");

    obj.value = v;

}

function doPopup(thisElt) {
      if (popupMenu == 'true') {
        aForm.action_key.value = window.showModalDialog (ctx + "popupm.jsp", " ", "dialogWidth:30; dialogHeight:30;");
        doSub("6");
      }
      else {
        val = window.showModalDialog (ctx + "popup.jsp", new Array(thisElt), "dialogWidth:30; dialogHeight:30; status:yes;");
        ti = 0;
        while (val == "$retry$" && ti < 10) {
          val = window.showModalDialog (ctx + "popup.jsp", new Array(thisElt), "dialogWidth:30; dialogHeight:30; status:yes;");
          ti++;
        }
        if (val == "$retry$") {
          //alert("Popup failure. Please retry.");
		  callCommonAlert(document.getElementById("langCode").value,"No0006");
        }
        else {
			thisElt.value = val;
        }

        popup = 'false';
      }
      return;
}
function doClick(pFocusObj){
	//added by Ai Hao
	aForm = document.commonForm;
    if (aForm == null) {
    	aForm = document.form1;
    }
    
	var t = event.target;
	if (!t) t=event.srcElement;
	event.cancelBubble = true;
	if (t.type == "text"  || t.type == "password") {
      
    }else{
    	aForm.activeField.value = "";
    	aForm.focusField.value = "";
    	aForm.focusInField.value = ""; // fix bug46
    }
}

function doFocus(pFocusObj) { 
    /* window.status = "Dofocus v1 */
	//added by Francis Wang 2010-11-3
	aForm = document.commonForm;
    if (aForm == null) {
    	aForm = document.form1;
    }
    aForm.focusPrevF.value = aForm.focusField.value;
    aForm.focusField.value = pFocusObj.name;
	aForm.activeField.value = pFocusObj.name;
    lastField = aForm.focusPrevF.value;
    thisField = aForm.focusField.value;
    thisElt = pFocusObj;
    /* window.status = "Dofocus: LastField " +lastField + ", This Field=" + thisField; */
    if (pFocusObj.type == "text"  || pFocusObj.type == "password" || pFocusObj.tagName.toUpperCase()=="TEXTAREA" ) {
    	// fix bug786
    	pFocusObj.value = pFocusObj.value.replace(/\s+$/, '');
    	//end
        pFocusObj.select();
    }
    ii = getIndexByName(thisField);
    jj = getIndexByName(lastField);
    if (jj == ii + 1) {
      nextOPrev = "PREV";
    }
    else if (jj == ii - 1) {
      nextOPrev = "NEXT";
    }
    else {
      nextOPrev = "";
    }
    lastSelectedF = pFocusObj; // fix bug46
    
    /* Temporary solution for bug 6040 */
    var checkScreen = eval('aForm.screen');
    if (checkScreen != null) {
    	if (checkScreen.value == "S2200") {
    		lastSelectedF = document.getElementById('s2200screensfl.trandesc_R1');
    	}
    }
    /* End bug 6040 */
}


function doI90Prompt(thisElt) {
    force++;
	path = ctx + "commonJSP/" + lang + "/I90Prompt.jsp?Force=" + force + "&I90PromptField=" + thisElt.name;
	path1 = path + "&I90PromptCode=" + thisElt.value + "&I90PromptCVal=" + thisElt.value;
	promptedValue = window.showModalDialog (path1, thisElt, "dialogWidth:640px; dialogHeight:420px; resizable:yes;");
	while (promptedValue != null) {
		if (promptedValue[0] != undefined) {
			url = "&I90PromptCode=" + promptedValue[1];
			url = url + "&I90PromptGeneric=" + promptedValue[2];
			url = url + "&I90PromptDesc=" + promptedValue[3];
			url = url + "&I90PromptLang=" + promptedValue[4];
			url = url + "&I90PromptCVal=" + promptedValue[5];
			url = url + "&I90PromptField=" + promptedValue[6];
			url = url + "&I90PromptDate=" + promptedValue[7];
			url = url + "&I90PromptAction=" + promptedValue[8];
			promptedValue = window.showModalDialog (path + url, thisElt, "dialogWidth:640px; dialogHeight:420px; resizable:yes;");
		}
		else {
			thisElt.value = promptedValue.trimRight();
			break;
		} 
		/*thisElt.value = promptedValue;*/
	}
	return;
}

/*
 * Added by Quipoz-MW
 * Handle ctrl+V or paste event. 
 * Validate value post-pasting.
 */
function doPasteNumber(objEvent) {

	var objInput = objEvent.srcElement;
	var decimal;
	if (objInput.decimal) {
		decimal = objInput.decimal;
	} else {
		decimal = 0;
	}
	
	//Save original value
	var savedValue = objInput.value;
	
	var selected = document.selection.createRange();
	selected.text=window.clipboardData.getData('text');
	
	var afterValue = objInput.value;
	objInput.value = savedValue;

	//if (objInput.precentFlag) {
	//	if (!isValidDigitsContainPre(afterValue, objInput.maxLength, decimal, true)) {
	//		objInput.focus();
	//		return false;
	//	}
	//} else {
	//	if (!isValidDigits(afterValue, objInput.maxLength, decimal, true)) {
	//		objInput.focus();
	//		return false;
	//	}
	//}
	if (!isValidDigits(afterValue, objInput.maxLength, decimal, true)) {
		objInput.focus();
		return false;
	}

	return true;
}

function doPost(pThis) {
    if (pThis.name == aForm.focusField.value) {
        return;
    }
    aForm.action_key.value = pThis.title;
    doSub("7");
}

function doPostHelp(pThis) {
    aForm.action_key.value = pThis.title + ",help";
    doSub("8");
}

function doPost2(pThis, pMethod) {
    if (pThis.name == aForm.focusField.value) {
        return;
    }
    aForm.action_key.value = pMethod;
    doSub("9");
}

function doPost2Help(pThis, pMethod) {
    aForm.action_key.value = pMethod + ",help";
    doSub("10");
}

function doBlurValidateRange(pBlurObj, pfrom, pto) {
    blurRParm = pBlurObj;
    blurRFrom = pfrom;
    blurRTo   = pto;
    x = setTimeout('doBlurValidateRangea()',10);
}

function doBlurValidateRangea() {
    if (blurRParm.name == aForm.focusField.value) {
        return;
    }
    thisValue = parseInt(blurRParm.value);
    if (!isNaN(thisValue)) blurRParm.value = thisValue;
    if ((isNaN(thisValue)) || (thisValue < blurRFrom) || (thisValue > blurRTo)) {
      blurRParm.value = window.showModalDialog ("popupr.htm", new Array(blurRFrom, blurRTo, blurRParm), "dialogWidth:30; dialogHeight:30");
    }
}

function doSub(action) {
	/* alert("Submit came from point " + from);*/
	//added by Ai Hao
	aForm = document.commonForm;
    if (aForm == null) {
    	aForm = document.form1;
    }
    x = subProcess(aForm);
    if (x == true) {
      aD = new Date();
      aoD = new Date(aD.getYear(), aD.getMonth(), aD.getDate(), aD.getHours(), aD.getMinutes(), aD.getSeconds());
      aForm.processTime.value = fm2(aD.getHours()) + ":" + fm2(aD.getMinutes()) + ":" + fm2(aD.getSeconds()) + "." + fm3(aD.getTime() - aoD.getTime()); 
      cancelled = false;
      // fix bug46
      if (aForm.focusInField && lastSelectedF) {
      	aForm.focusInField.value = lastSelectedF.name;
      }
      /*IGJL-711 CHANGES START*/
      if(action!='PFKEY03' && restrictCharsFlag){
    	  if(isInputValidAsPerAcceptedCharList()) {
    		  aForm.submit();
    		  return true;
    	  }
    	  else{
    		  console.log(restrictCharsErrorMessage);
    		  submitted=false;
    		  return false;
    	  }
      }else{
    	  aForm.submit();
    	  return true;
      }
      /*IGJL-711 CHANGES END*/
    }
}

/*IGJL-711 CHANGES START*/
function isInputValidAsPerAcceptedCharList(){
	var retVal=true;
	var acceptedCharsObject=null;
	var jsonString=null;
	if(restrictCharsJson!=null){
		jsonString = restrictCharsJson.toString();
		if(jsonString.length>0 && jsonString!=restrictCharsKey){
			acceptedCharsObject=restrictCharsJson;
		} else{
			console.log("Cannot parse the accepted character list!!");
			return false;
		}
	} else{
		console.log("Accepted character json is null or empty!!");
		return false;
	}
	var frameDocument = $('frame[name="mainForm"]', top.document)[0].contentDocument;
	
	$('input:text, textarea', frameDocument).each(
	    function(index){
	    	var isNotVisibile = ($( this ).css("visibility")=='hidden');
	    	var isTypeHidden = ($( this ).attr('type') == 'hidden');
	    	//var isHidden = $( this ).is(":hidden");
	    	//if(!(isNotVisibile || isTypeHidden || isHidden))
	    	if(!(isNotVisibile || isTypeHidden))
	    	{
	    		retVal= validateForAcceptedCharList($(this), jsonString);
	    		if(!retVal){
	    			return retVal;
	    		}
	    	}
	    }
	);
	return retVal;
}

function validateForAcceptedCharList(field, jsonString){
	var frameDocument = $('frame[name="frameMenu"]', top.document)[0].contentDocument;
	var tstr = field.attr("value");
	var bstr = null;
	var errorFlag=0;
	var fieldName = field.attr("name");
	console.log("FIELD NAME = ["+fieldName+"]");
	if(tstr===undefined || tstr==null || tstr.trim()==""){
		clearErrorMessage(frameDocument);
		removeHighlightsOfTheField(field, frameDocument);
		hideMessageBox(frameDocument);
		errorFlag=0;
		return true;
	}else{
		tstr=tstr.trim();
	}
	for(i=0; i<tstr.length; i++){
		//if(tstr.charCodeAt(i)>127){
			bstr = tstr.charCodeAt(i);
			var hexString=Number(bstr).toString(16).toUpperCase();
			hexString=hexString.toString();
			for(j=hexString.length;j<4;j++){
				hexString="0"+hexString;						
			}
			if(!(("0x"+hexString) in JSON.parse(jsonString))){
				displayErrorOnTheScreen(field, frameDocument);
				//doFocus(field);
				errorFlag=1;
				break;
			}
		//}
	}
	if(errorFlag==0){
		clearErrorMessage(frameDocument);
		removeHighlightsOfTheField(field,frameDocument);
		hideMessageBox(frameDocument);
		return true;
	} else {
		return false;	
	}
}
function displayErrorOnTheScreen(element, frameDocument){
	removeHighlightsFromOtherFields(frameDocument);
	highlightTheField(element,frameDocument);
	removeOtherErrorMessages(frameDocument);
	populateErrorMessage(frameDocument);
}
function removeHighlightsFromOtherFields(frameDocument){
	$(".red.reverse").removeClass("red reverse");
	$(".red.reverse").removeAttr("hascursor");
	$("select.red").removeClass("red");
	
}
function highlightTheField(element,frameDocument){
	$(element).attr("hascursor","yes");
	$(element).addClass("red reverse");		
}
function removeHighlightsOfTheField(element, frameDocument){
	if(!$(frameDocument).find('div.sidebarbg.sidemessage').children('div.message').children('div').children('a').length >0){
		$(element).removeAttr("hascursor");
		$(element).removeClass("red reverse");	
	}
}
function removeOtherErrorMessages(frameDocument){
	$(frameDocument).find('div.sidebarbg.sidemessage').children('div.message').children('div').children('a').remove();
	$(frameDocument).find('div.sidebarbg.sidemessage').children('div.message').children('div').children('br').remove();
	$(frameDocument).find('div.sidebarbg.sidemessage').children('div.message').children('div').empty();
}
function populateErrorMessage(frameDocument){
	$(frameDocument).find('div.sidebararea.sideMsg').css("display", "block");
	$(frameDocument).find('div.sidebarbg.sidemessage').children('div.message').children('div').children('a#shiftjis').remove();
	$(frameDocument).find('div.sidebarbg.sidemessage').children('div.message').children('div').children('br#shiftBr').remove();
	$(frameDocument).find('div.sidebarbg.sidemessage').children('div.message').children('div').append('<a id="shiftjis" href="#">' + restrictCharsErrorMessage + '</a><br id="shiftBr">');
}
function clearErrorMessage(frameDocument){
	$(frameDocument).find('div.sidebarbg.sidemessage').children('div.message').children('div').children('a#shiftjis').remove();
	$(frameDocument).find('div.sidebarbg.sidemessage').children('div.message').children('div').children('br#shiftBr').remove();
}
function hideMessageBox(frameDocument){
	if(!$(frameDocument).find('div.sidebarbg.sidemessage').children('div.message').children('div').children('a').length >0 && 
			!$(frameDocument).find('div.sidebarbg.sidemessage').children('div.message').children('div').text().trim().length){
		$(frameDocument).find('div.sidebararea.sideMsg').css("display", "none");
	}
}
/*IGJL-711 CHANGES END*/
 
function doLoad() {
    /* window.status = "Active field '" + currField + "'"; */
    /* window.status = "Last field=" + lastField + ", This Field=" + thisField; */

    // the main form in the screen
    aForm = document.commonForm;
    if (aForm == null) {
    	aForm = document.form1;
    }
    // each of the elements in the form
    elts = aForm.elements;
	if(document.getElementById("continuebutton")){
	    document.getElementById("continuebutton").focus();
	  }else if(document.getElementById("hiddeninput")){
	    document.getElementById("hiddeninput").focus();
	 }
	//positioningMode is defined in commonScript1.jsp. So fat there are two modes
	//If 1, position cursor to the first input enabled field or field with 
	if (positioningMode == 1) {
	
		thisField = findFocusingField(elts);
		//Not sure what these two fields are used for, but keep them as-is to avoid unexpected resutl
	    if (currField != "null") {
	    	currField = "null";
	    	nextOPrev = "SET";
	    }
	} else { //Otherwise, keeps it as-is 
	    if (error == "true") {
	      thisField = lastField;
	    }
	
	    if (currField != "null") {
	    	thisField = currField;
	    	currField = "null";
	    	nextOPrev = "SET";
	    }	    
	}
    aForm.focusPrevF.value = lastField;
    aForm.focusField.value = thisField;

    if (popup == "true") {
      thisElt = getFieldByName(thisField);/*CCH CHange 23 April */
      doPopup(thisElt);
    }
    else if (error == "true") {
      selectFieldByName(thisField);
    }
    else {
      selectNextLogicalField();
    }
    
    // allow form submission on all selects
	/*
	var sels=document.getElementsByTagName('select');
	for (var i=0; i<sels.length; i++) {
	    if (document.all) 
	    	sels[i].attachEvent('onkeyup', submitOnSelect);
	    else 
	    	if (document.getElementById) 
	    		sels[i].addEventListener('keyup', submitOnSelect, true);
	}
	*/

    /* Go through and assign tabindex for all fields */
 	for (var i=0; i<elts.length; i++) {
 		x = elts[i].offsetLeft;
 		y = elts[i].offsetTop;
 		p = elts[i].offsetParent;
 		while (p != undefined) {
 			x += p.offsetLeft;
 			y += p.offsetTop;
			p = p.offsetParent;
 		}
 		if (x > 0 && y > 0) {
 			y = y/screen.height;
 			x = x/screen.width;
 			elts[i].tabIndex = (y * 10000) + x * 100;
 		}
	}
	
    nextOPrev = "";
    now = new Date();
    doResize();
    // fix bug46
    if (aForm.focusInField) {
    	goBackSel(aForm.focusInField.value);
    }
    //replaceSelects();
    //doBlink();
    
}

function doResize() {
	return; /* Functionality removed for Polisy */
	if (window.dialogArguments != null) {
		return;
	}
    xsize = screen.width;
    ysize = screen.height;
    asize = document.body.offsetWidth;
    bsize = document.body.offsetHeight;
    pct   = 100;
    if (xsize >= 1024) {
    	pct = 130;
    }
    document.body.style.fontSize = (asize * pct / xsize) + "%";
    /* alert("Result " + document.body.style.fontSize); */
}

function fieldHelp(theField) {
    //thisElt = theField;
    //window.showModalDialog(ctx + "I90FieldHelp.jsp?field='" + thisElt + "'");
    //window.open(ctx + "I90FieldHelp.jsp?field='" + thisElt + "'");
    //event.cancelBubble = true;
    return false;
}

/* 	Added by Quipoz-MW
	To return a field name which is 
		1) not being disabled, and
		2) the first input enabled field with hasCursor defined as true
		3) the first input enabled field (INPUT-text/OPTION/SELECT/TEXTAREA), or
	1 && (2 || 3)
*/
function findFocusingField(elements) {
	var fieldName = null;
	for (var i=0; i<elements.length; i++) {
		if (isEnterableElement(elements[i])) {
			if (elements[i].hasCursor != null && elements[i].hasCursor.toUpperCase() == "YES") {
				fieldName = elements[i].name;
				break;
			}
			if(elements[i].className.indexOf("red")>0){
			fieldName = elements[i].name;
				break;
			}
			if (fieldName == null) {
				fieldName = elements[i].name;
			} 
		}
	}
	return fieldName;
}

function screenHelp() {
	
    //window.showModalDialog(ctx + "I90FieldHelp.jsp?field='" + thisElt + "'");
    //window.open(ctx + "I90ScreenHelp.jsp");
    //event.cancelBubble = true;
    return false;
}

function fixDels(n, pic) {
    if (debugkeys) window.status = "Fixdels entered '" + before + "' '" + after + "' " + ku;
    var ti = 0;
    var j = 0;
    var k = 0;
    pre = "";
    
    /* Case 1 - inserted characters. */
    if (after.length > before.length) {
      ti = 0;
      while (ti < before.length && before.substring(ti, ti+1) == after.substring(ti, ti+1)) {;
        ti++;
      }
      if (ti > n) {
        return after.substring(0, n);
      }
      j = before.length - 1;
      k = after.length  - 1;
      while (j < before.length && before.substring(j, j+1) == after.substring(k, k+1)) {
        j--;
        k--;
      }
      after = after.substring(0,ti) + after.substring(k + 1);
      return after;
    }

    /* Case 2 - deleted characters. */
    /* Step 1. Skip over leading characters that are the same.*/
    i = 0;
    while (ti < before.length && before.substring(ti,ti+1) == after.substring(ti,ti+1)) {
      ti++;
    }

    /* Step 2. Insert pic characters as required. */
    p = pic.substring(ti, ti+1);
    while (ti < pic.length && "9AX".indexOf(p) < 0) {
      after = after.substring(0,ti) + pic.substring(ti, ti+1) + after.substring(ti);
      ti++;
      p = pic.substring(ti, ti+1);
    }

    /* Step 3. Skip the next character if this wasn't a delete. */
    if (ku != 46) {
      ti++;
    }

    /* Step 4. Insert blanks to make after length the same as the pic length. */
    while (after.length < n) {
      after = after.substring(0,ti) + " " + after.substring(ti);
    }

    /* Step 5. Fix pic non-numeric chars. */
    while (ti < n) {
      p = pic.substring(ti, ti+1);
      c = after.substring(ti, ti+1);
      if ("9AX".indexOf(p) < 0) {
        after = after.substring(0,ti) + p + after.substring(ti+1);
      }
      ti++;
    }

    /* Step 6. Fix chars which may now be invalid. */
    fixInvalidChars("A", pic);
    /* window.status = after; */
    return after;
}
  
function fixInvalidChars(val, pic) {
    /* Fix chars in "after" which may now be invalid.                 */
    /* E.g., pic part is "999 9A9 999" value "123 6T7 456"            */
    /* After deletion of "6", value is       "123 T7  456"            */
    /* Neither the T nor the 7 matches the pic field.                 */
    n = after.length;
    for (ti=0; ti<n; ti++) {
      c = after.substring(ti, ti+1);
    }
    for (ti=0; ti<n; ti++) {
      p = pic.substring(ti, ti+1);
      c = after.substring(ti, ti+1);
      if (p == "9" && " 0123456789".indexOf(c) < 0) {
        after = after.substring(0,ti) + " " + after.substring(ti+1);
      }
      if (p == "A" && " ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(c) < 0) {
        after = after.substring(0,ti) + " " + after.substring(ti+1);
      }
      if (p == "X" && " 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(c) < 0) {
        after = after.substring(0,ti) + " " + after.substring(ti+1);
      }
    }
}
  
function fm2(num) {
    if (num < 10) return "0" + num;
    return num;
}
  
function fm3(num) {
    if (num < 10) return "00" + num;
    if (num < 100) return "0" + num;
    return num;
}

function getCaretPos(elem) {
    if ( elem.isTextEdit && elem.caretPos != undefined) {
            var orig = elem.value;
            var caretPos = elem.caretPos;
            var olen = elem.maxLength;
            elem.maxLength = elem.maxLength + 1;
            caretPos.text = bookmark;
            var ci = elem.value.search( bookmark );
            elem.value = orig;
            elem.maxLength = olen;
            return ci;
    }
    else {
    	return 0;
    }
}

function getFieldByName(thisF) {
	if (thisF != "")  {
		found = false;
		var i = 0;
		for (i=0; i < elts.length; i++) {
			anElt = elts[i];
			if (selectableInputField(anElt)) {
				if (anElt.name == thisF) {
					return anElt;
				}
			}
		}
	}
	return " ";
}

function gFN(thisF) {
  	return getFieldByName(thisF);
}

function getIndexByName(thisF) {
    if (thisF == "")  {
      return -1;
    }
    found = false;
    var i = 0;
    //added by Francis Wang 2010-11-3
    elts = aForm.elements;
    for (i=0; i < elts.length; i++) {
      if (elts[i].name == thisF) {
        found = true;
        break;
      }
    }

    if (found) {
      anElt = elts[i];
      if (selectableInputField(anElt)) {
        return i;
      }
    }
    return -1;
}

/* Is an element an entable field, name = INPUT/OPTION/SELECT/TEXTAREA & not disabled*/
function isEnterableElement(element) {
	if (element.disabled) {
		return false;
	}
	var tagName = element.tagName.toUpperCase();
	return (tagName == "INPUT" && element.type.toUpperCase() == "TEXT"
		|| "OPTION|SELECT|TEXTAREA".indexOf(tagName) >=0);
}
/* Check if a decimal field is valid
	1) contains only comma, dot, sign(s), 0-9
	2) No longer than maxLength
	3) decimal places is no greater than expected
*/
function isValidDigits(fValue, maxLength, maxDecimal, alertMsg) {

	var strValue = fValue.trim();
	
	if (strValue.length == 0) {
		return true;
	}

	//Check if string contains on [, . + - 0-9];
	if (/[^\,\.\+\-0-9\%]/.test(strValue)) {//Only % is also permitted, modified by shawn.wang @20100913
		if(alertMsg) {
			//alert('[' + fValue + "] does not contains comma, dot, positive sign, negative sign and 0-9 only !");
			callCommonAlert(document.getElementById("lang1").value,"No0007",fValue);
		}
		return false;
	}

	//Remove commas.
	strValue = strValue.replace(/[^0-9\.]/, "");

	//Check total length
	if (strValue.length > maxLength) {
		if (alertMsg) {
			//alert('[' + fValue + "] exceeds max length [" + maxLength + "]!");
			callCommonAlert(document.getElementById("lang1").value,"No0008",fValue,maxLength);
		}
		return false;
	}

	//Check decimal places.
	var indexDot = strValue.indexOf(".");
	if (indexDot > -1) {
		//Check if there is second dot
		if (strValue.lastIndexOf(".") > indexDot) {
			if (alertMsg) {
				//alert('[' + fValue + "] has more than one decimal points!");
				callCommonAlert(document.getElementById("lang1").value,"No0009",fValue);
			}
			return false;
		}
		//Check if decimal place is less than expected.
		var decimalPlace;
		if (indexDot+1 == strValue.length) {
			decimalPlace = 0;
		} else {
			decimalPlace = strValue.substring(indexDot+1).length;
		}
		if (decimalPlace > maxDecimal) {
			if (alertMsg) {
				//alert('[' + fValue + "] has decimal place more than expected [" + maxDecimal + "]!");
				callCommonAlert(document.getElementById("lang1").value,"No0010",fValue,maxDecimal);
			}
			return false;
		}
	}
	return true;
}

/* Like the function isValidDigits but can contain % symbol

*/
function isValidDigitsContainPre(fValue, maxLength, maxDecimal, alertMsg) {
	var strValue = fValue.trim();

	if (strValue.length == 0) {
		return true;
	}

	//Check rule
	var precIntNum = /^(0|[1-9][0-9]*)%$/;
	var precfloatNum = /^-?([1-9][0-9]*.[0-9]*|0.[0-9]*[1-9][0-9]*|0?.0+|0)%$/;
	var intnum = /^(0|[1-9][0-9]*)$/;
	var floatnum = /^-?([1-9][0-9]*.[0-9]*|0.[0-9]*)[0-9]$/
	var percentSign=/^%$/ //only % IS ALSO PERMIT, added by shawn.
	if (percentSign.test(strValue)||floatnum.test(strValue) || intnum.test(strValue) || precfloatNum.test(strValue) || precIntNum.test(strValue)) {
		// TODO
	} else {
		if(alertMsg) {
			//alert('[' + fValue + "] does not contains comma, dot, positive sign, negative sign and 0-9 only !");
			callCommonAlert(document.getElementById("lang1").value,"No0007",fValue);
		}
		return false;
	}

	// Remove %
	strValue = strValue.replace(/%$/, "");
	//Remove commas.
	strValue = strValue.replace(/[^0-9\.]/, "");

	//Check total length
	if (strValue.length > maxLength) {
		if (alertMsg) {
			alert('[' + fValue + "] exceeds max length [" + maxLength + "]!");
			callCommonAlert(document.getElementById("lang1").value,"No0008",fValue,maxLength);
		}
		return false;
	}

	//Check decimal places.
	var indexDot = strValue.indexOf(".");
	if (indexDot > -1) {
		//Check if there is second dot
		if (strValue.lastIndexOf(".") > indexDot) {
			if (alertMsg) {
				//alert('[' + fValue + "] has more than one decimal points!");
				callCommonAlert(document.getElementById("lang1").value,"No0009",fValue);
			}
			return false;
		}
		//Check if decimal place is less than expected.
		var decimalPlace;
		if (indexDot+1 == strValue.length) {
			decimalPlace = 0;
		} else {
			decimalPlace = strValue.substring(indexDot+1).length;
		}
		if (decimalPlace > maxDecimal) {
			if (alertMsg) {
				//alert('[' + fValue + "] has decimal place more than expected [" + maxDecimal + "]!");
				callCommonAlert(document.getElementById("lang1").value,"No0010",fValue,maxDecimal);
			}
			return false;
		}
	}
	return true;
}

function noHelp() {
    //alert("No Help");
	callCommonAlert(document.getElementById("langCode").value,"No0011");
    return false;
}

function noDateHelp() {
    //alert("Press 'C' for calendar");
	callCommonAlert(document.getElementById("langCode").value,"No0012");
    return false;
}

/* The following pair of edit functions are to handle the "hard" cases where a field contains */
/* an embedded picture editing clause, e.g. 999-999.                                          */
function numericOnlyDown(obj, pic) {

    if (activity == true) {
      event.returnValue=false;
      return false;
    }
    
    kd     = event.keyCode;
    activity = true;

    if (kd >= 113 && kd <= 123) {
      activity = false;
      return true;
    }

    obj.caretPos = document.selection.createRange();

    before = obj.value;
    if (cp > 0) {
      b4cp   = cp;
    }

    if (kd==46) {
      return true;
    }

    if ((kd >= 48 & kd <= 57) || (kd >= 96 & kd <= 105)) {
      return true;
    }

    if ((kd >= 65 & kd <= 90) || kd==32) {
      return true;
    }

    activity = false;
    /* These keys can safely auto-repeat */

    if (kd== 187 || kd == 36 || kd== 35 || kd==9 || kd==13 || kd==37 || kd==39 || kd==8 ) {
      if (kd == lk) {
        lkc++;
      }
      else {
        lk  = kd;
        lkc = 1;
      }
      if (kd==8) {
        event.keyCode = 37;
      }
      return true;
    }

    event.returnValue = false;
    return false;
}

function numericOnlyUp(obj, pic) {
    
    after = obj.value.toUpperCase();
    ku      = event.keyCode;
    cp = getCaretPos(obj);
    /* window.status = " "; */

    if (debugkeys) window.status = "Up '" + before + "' '" + after + "' " + ku;

    /* Case 1: tabbed into the field */
    if (ku == 9 || ku == 16) {
        if (debugkeys) window.status = "Case 1 Up '" + before + "' '" + after + "' " + ku;
        activity = false;
        cp = 0;
        setSelectionRange(obj, cp, cp+1);
        return;
    }
    
    /* Case 2: replaced the whole string with a number/letter */
    if (((ku >= 48 & ku <= 57) | (ku >= 96 & ku <= 105) | (ku >=65 & ku < 90) | ku==32) & after.length == 1) {
      p = pic.substring(0, 1);
      c = after.substring(0, 1);
      if (p == "9" && "0123456789".indexOf(c) < 0) {
        window.status = "Only numbers allowed for this keystroke!";
        after = " ";
      }
      else if (p == "A" && " ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(c) < 0) {
        window.status = "Only letters allowed for this keystroke!";
        after = " ";
      }
      for (ti=after.length; ti<pic.length; ti++) {
        p = pic.substring(ti, ti+1);
        if ("9AX".indexOf(p) >= 0) {
          after = after + " ";
        }
        else {
          after = after + p;
        }
      }
      if (after.substr(0,1) == " ") cp = 0;
      else                          cp = 1;
      b4cp = cp;
      obj.value = after;
      setSelectionRange(obj, cp, cp+1);
      lastGoodDate = after;
      if (debugkeys) window.status = "Case 2 Up '" + before + "' '" + after + "' " + ku;
      activity = false;
      return;
    }

    /* Case 3: Escape clears the field */
    if (ku == 27) {
      after = "";
      obj.value = after;
      setSelectionRange(obj, 1, 2);
      lastGoodDate = after;
      if (debugkeys) window.status = "Case 3 Up '" + before + "' '" + after + "' " + ku;
      activity = false;
      return;
    }

    if (after.length != pic.length) {
      after = fixDels(pic.length, pic);
    }

    /* Case 4: delete */
    if (ku == 46) {
      if (cp >= 0) {
        j = cp;
        p = pic.substring(j, j+1);
        while (j < pic.length && "9AX".indexOf(p) >= 0) {
          j++;
          p = pic.substring(j, j+1);
        }
        after = after.substring(0,cp) + after.substring(cp+1,j) + " " + after.substring(j);
      }
      b4cp = cp; 
      fixInvalidChars("B", pic);
      obj.value = after;
      setSelectionRange(obj, cp, cp+1);
      lastGoodDate = after;
      if (debugkeys) window.status = "Case 4 Up '" + before + "' '" + after + "' " + cp;
      activity = false;
      return;
    }

    /* Case 5: Cursor movement */
    if (before == after && !((ku >= 48 & ku <= 57) | (ku >= 96 & ku <= 105) | (ku >= 65 & ku <= 90) | ku==32)) {
      /* Sometimes cp gets corrupted by fast typing. Restore it, if possible. */
      if (cp <= 0 && b4cp != 0) {
        cp = b4cp;
      }
      if (ku == 39)                 {cp = cp + lkc; p = pic.substring(cp, cp+1); if ("9AX".indexOf(p) < 0) cp++;}
      else if (ku == 37 || ku == 8) {cp = cp - lkc; p = pic.substring(cp, cp+1); if ("9AX".indexOf(p) < 0) cp--;}
      else if (ku == 36)            cp = 0;
      else if (ku == 35)            cp = pic.length - 1;
      if (cp < 0)                   cp = 0;
      if (cp >  pic.length - 1)     cp = pic.length - 1;
      setSelectionRange(obj, cp, cp+1);
      b4cp = cp;
      lkc  = 0;
      if (debugkeys) window.status = "Case 5 Up '" + before + "' '" + after + "' " + ku;
      activity = false;
      return;
    }

    /* Case 6: Number */
    if ((ku >= 48 & ku <= 57) | (ku >= 96 & ku <= 105) | (ku >= 65 & ku < 90) | ku==32) {
      /* Sometimes cp gets corrupted by fast typing. Restore it, if possible. */
      if (cp == 0 && b4cp != 0) {
        cp = b4cp;
      }

      /* Check for invalid keystroke */
      p = pic.substring(cp, cp+1);
      c = after.substring(cp, cp+1);
      if (cp >= 0 && p == "9" && "0123456789".indexOf(c) < 0) {
        window.status = "Only numbers allowed for this keystroke!";
        after = before;
      }
      else if (cp >= 0 && p == "A" && " ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(c) < 0) {
        window.status = "Only letters allowed for this keystroke!";
        after = before;
      }
      else {
        cp++;
        p = pic.substring(cp, cp+1);
        if ("9AX".indexOf(p) < 0) cp++;
      }

      if (cp > pic.length - 1) cp = pic.length - 1;
      /* Test for corruption - overtyping. */
      errs = 0;
      corr = "";
      deb = "";
      for (ti=0; ti < pic.length; ti++) {
        p = pic.substring(ti, ti+1);
        c = after.substring(ti, ti+1);
        if ("9AX".indexOf(p) < 0) {
          if (after.substring(ti, ti+1) != p) {
            errs++;
            deb = deb + " " + ti + " '" + c + "' '" + p + "'";
            c = p;
          }
        }
        else if (p == "9" & " 0123456789".indexOf(c) < 0) {
          errs++;
          deb = deb + " " + ti + " '" + c + "' '" + p + "'";
          c = " ";
        }
        else if (p == "A" & " ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(c) < 0) {
          errs++;
          deb = deb + " " + ti + " '" + c + "' '" + p + "'";
          c = " ";
        }
        else if (p == "X" & " 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(c) < 0) {
          errs++;
          deb = deb + " " + ti + " '" + c + "' '" + p + "'";
          c = " ";
        }
        corr = corr + c;
      }
      if (errs > 0) {
        //alert("Buffer overrun. Please don't type so fast in a numeric-edited field, or select a separator! " + deb);
        callCommonAlert(document.getElementById("langCode").value,"No00013",deb);
		after = corr;
      }
      obj.value = after;
      setSelectionRange(obj, cp, cp+1);
      lastGoodDate = after;
      if (debugkeys) window.status = "Case 6 Up '" + before + "' '" + after + "' " + cp + " " + b4cp;
      activity = false;
      return;
    }

    if (debugkeys) window.status = "Case 7 Up '" + before + "' '" + after + "' " + ku + " " + cp;
    activity = false;
    setSelectionRange(obj, 0, 1);
    b4cp = 0;
    cp = 0;
    return;

}


function numbersOnly(obj, pic) {

    kd     = event.keyCode;
    /* window.status = "Key " + kd; */

    /* control keys */
    if (kd==null || kd==0 || kd==8 || kd==9 || kd==13 || kd==27) {
      return true;
    }

    picdot = pic.indexOf(".");
    b4dec  = picdot;
    if (b4dec < 0) {
    	b4dec = pic.length;
    }

    if (pic.substring(0,3) == "@Z ") b4dec = b4dec - 3;

    /* Allow a decimal point if there's one in the pic and one is not yet present */
    if (kd==46 && picdot >= 0) {
      afterAction = whatsThereAfterReplace(obj);
      if (afterAction.indexOf(".") < 0) {
        pos = picdot;
        afterDot = pic.substring(pos + 1);
        decs = afterDot.length;

        pos = afterAction.indexOf("~");
        afterDot = afterAction.substring(pos + 1);
        if (afterDot.length <= decs) {
          return true;
        }
      }
    }

    if (kd >= 48 & kd <= 57) {
      /* If there is no decimal point, a number is valid as long as it doesn't */
      /* exceed the number of digits before the dec point                      */
      if (picdot < 0) {
      	window.status = 'Debug ';
      	return obj.value.trim().length < b4dec;
      }

      afterAction = whatsThereAfterReplace(obj);
      pos = afterAction.trim().indexOf("~");
      /* If there is no decimal point in the current number, a number is valid */
      /* provided it doesn't make too many digits                              */
      if (afterAction.indexOf(".") < 0) {
        if (obj.value.length == b4dec && pos == b4dec) {
          obj.value = obj.value + ".";
        } 
        if (obj.value.length >= b4dec) {
          return false;
        } 
        return true;
      }

      /* Else, a number is only valid if it doesn't make more digits after the decimal point than are allowed. */
      /* or, put too many digits before the decimal point.                                                     */
      pos = picdot;
      afterDot = pic.substring(pos + 1);
      decs = afterDot.length;

      pos = afterAction.indexOf(".");
      afterDot = afterAction.substring(pos + 1);
      cpos = afterAction.indexOf("~");
      
      if (cpos > pos && afterDot.length <= decs) {
        return true;
      }
      else if (cpos < pos) {
        digits = obj.value.trim().indexOf(".");
        if (digits < b4dec) {
          return true;
        }
      }
    }

    return false;

}

function onlyCertainChars(obj, pic) {

    kd     = event.keyCode;
    if (debugkeys) window.status = kd + " ";


    /* control keys */
    if (kd==null || kd==0 || kd==8 || kd==9 || kd==13 || kd==27) {
      return true;
    }

    ks     = "?";

    /* Lower case letters - assume translated to upper case */
    if (kd >= 97 && kd <= 122) {
      ti = kd - 97;
      ks = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".substring(ti, ti+1);
    }

    /* Upper case letters */
    if (kd >= 65 && kd <= 90) {
      ti = kd - 65;
      ks = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".substring(ti, ti+1);
    }

    /* Digits */
    if (kd >= 48 && kd <= 57) {
      ti = kd - 48;
      ks = "01234567890".substring(ti, ti+1);
    }
 
    if (kd == 32) {
      ks = " ";
    }

    if (pic.indexOf(ks) >= 0) {
      return true;
    }

    return false;

}	

/* Fold any characters entered to uppercase                                                       */
/* This allows defaults or current data to be lower case but all input to be forced to upper case */
/* Note this function allows ESC to reset to the prior value                                      */  
function onlyUppercase(obj) {

	if (obj == null) {
		return false;
	}
	
	if (obj.previous == null) {
		obj.provious = obj.value;
	}
	
    kd     = event.keyCode;
    window.status = kd + " ";

    /* Lower case letters - assume translated to upper case */
    if (kd >= 97 && kd <= 122) {
      event.keyCode = kd - 32;
    }
    
    return true;

}	  

function poplist(thelist) {
    thisElt = thelist;
    if (thelist.length >= 2) {
      //thelist.value = window.showModalDialog (ctx + "popupt.jsp", new Array(thisElt), "dialogWidth:25; dialogHeight:25");
      thelist.value = window.open(ctx + "popupt.jsp");
    }
    else {
      //alert("No HELP data available.");
	    callCommonAlert(document.getElementById("langCode").value,"No00014");
    }
    return false;
}

function popupLeftNum(pLeft) {
	lcalc = pLeft * 100.0/80.0;
	return lcalc + '%';
}

function popupTopNum(pTop) {
	tcalc = pTop * 100.0/24.0;
	return tcalc + '%';
}

function popupLeftPct(pLeft) {
	lcalc = document.body.offsetWidth * pLeft/100;
	return lcalc + '%';
}

function popupTopPct(pTop) {
	tcalc = document.body.offsetHeight * pTop/100;
	return tcalc + '%';
}

function popupWidth(pWidth) {
	return pWidth * screen.width/100.0;
}

function popupHeight(pHeight) {
	return pHeight * screen.height/100.0;
}

function selectaField(thisF) {
    if (thisF.type == "text"  || thisF.type == "password") {
      thisF.select();
    }
   
	thisField = thisF.name;
	lastField = thisF.name;
	aForm.focusPrevF.value = thisField;
	aForm.focusField.value = thisField;
    thisElt = thisF;
    //Modified by shawn, to deal with can't move focus on some field.
    //@100108
     if (thisF.type != "hidden" && thisF.style.visibility != "hidden") {
    try{
      thisF.focus();
      }catch(e){
		return false;
      }
    }
}

function selectCurrentField() {
    selectaField(thisElt);
}

function selectFieldByName(thisF) {
    found = false;
    var i = 0;
    for (i=0; i < elts.length; i++) {
      if (elts[i].name == thisF) {
        found = true;
        break;
      }
    }
    if (!found) {
      selectFirstField();
      return;
    }
    anElt = elts[i];
    if (selectableInputField(anElt)) {
      selectaField(anElt);
      return;
    }
    selectFirstField()
}

function setSelectionRange(input, selectionStart, selectionEnd) {
    if (input.setSelectionRange) {
      input.focus();
      input.setSelectionRange(selectionStart, selectionEnd);
    }
    else if (input.createTextRange) {
      var range = input.createTextRange();
      range.collapse(true);
      range.moveEnd('character', selectionEnd);
      range.moveStart('character', selectionStart);
      range.select();
    }
}

function selectSpecial() {
	return;
	window.status = "Select special triggered";
	
    var var1 =  document.activeElement.name;
    if (var1 == undefined) {
    	var1 = "none!";
    }
    /* window.status = "Active element: '" + var1.toString() + "'";*/
    if (var1 == "none!") {
          selectaField(thisElt);
    }
    return false;
}

function selectNextLogicalField() {

    if (nextOPrev != "NEXT" && nextOPrev != "PREV")  {
      /*window.status = "No decision needed because '" + nextOPrev + "'";*/
      if (thisElt == null) {
      	selectFieldByName(thisField);
      }
      else {
      	selectaField(thisElt);
      }
      return;
    }

    i = getIndexByName(lastField);

    if (i == -1) {
      selectFirstField();
      return;
    }

    if (nextOPrev == "NEXT") {
      found = false;
      for (i=i+1; i < elts.length; i++) {
        anElt = elts[i];
        if (selectableInputField(anElt)) {
            /*window.status = "Next field found where expected.";*/
            selectaField(anElt);
            return;
        }
      }
      if (!found) {
        /*window.status = "No field found after the last one.";*/
        selectaField(thisElt);
        return;
      }
    }

    if (nextOPrev == "PREV") {
      found = false;
      for (i=i-1; i < elts.length; i++) {
        anElt = elts[i];
        if (selectableInputField(anElt)) {
            /*window.status = "Prev field found where expected.";*/
            selectaField(anElt);
            return;
        }
      }
      if (!found) {
        /*window.status = "No field found before the last one.";*/
        selectaField(thisElt);
        return;
      }
    }

}
  
function selectableInputField(anElt) {
	if (anElt == null) return false;
	if (anElt == undefined) return false;
	dis = anElt.disabled;
	pobj = anElt.parentElement;
	while (pobj != null && dis == false) {
		dis = dis || pobj.disabled;
		pobj = pobj.parentElement;
	}

	return (((	anElt.type == "text") 
		|| (anElt.type == "select-one") 
		|| (anElt.type == "password") 
		|| (anElt.type == "textarea")) && !dis);
}

function selectNextField(aField) {
	found = false;
	for (i=0; i < elts.length; i++) {
		anElt = elts[i];
		if (found) {
	        if (selectableInputField(anElt)) {
				selectaField(anElt);
				return;
			}
		}
		else {
			if (anElt == aField) {
				found = true;
			}
		}
	}
}

/* this method to set last registered focus element */
function setFocus() {
  	if (lastFocus != null && lastFocus != "") {
  		lastFocus.focus();
  	}
}

function selectFirstField() {
	for (i=0; i < elts.length; i++) {
		if (selectableInputField(elts[i])) {
			selectaField(elts[i]);
			return;
		}
	}
    aForm.lastResort.focus();
}

function selectLastField() {
	selectaField(elts[elts.length - 8]);
}

function subProcess(form) {
    if (submitted) {
      //alert("The server is still processing your last request. Please wait for the response before proceeding.");
      callCommonAlert(document.getElementById("langCode").value,"No00015");
	  return false;
    }
    aForm.focusNxtPr.value = nextOPrev;
    submitted = true;
    

    return true;
}

function whatsThereAfterReplace(obj) {
    save          = obj.value;
    obj.caretPos  = document.selection.createRange();
    var bookmark  = "~";
    var caretPos  = obj.caretPos;
    caretPos.text = bookmark;
    var ci        = obj.value.search( bookmark );
    var newv      = obj.value; 
    obj.value     = save;
    /* Restore the selection range */
    var upto      = ci + save.length - newv.length + 1;
    setSelectionRange(obj, ci, upto);
    return newv;
}

function checkValue(obj, validValues) {

	var valuesArray = validValues.split('/');

	var valid = false;
	for (i = 1; i < valuesArray.length - 1; i++)
	{
		if (obj.value == valuesArray[i])
		{
			valid = true;
		}
	}

	//alert(valid);
}

function checkBooleanValue(obj) {
	/* commentted out by Wayne Yang 2010-03-05
	if (obj.value != 'Y')
	{
		obj.value = 'N';
	}
	*/
	var value_Y = "Y";
	var value_N = "N";
	var id = null;
	var numargs = arguments.length;
	if (numargs == 4){
		id = arguments[1];
		value_Y = arguments[2];
		value_N = arguments[3];
	}
	if (id != null){	
		if (obj.checked){
			document.getElementById(id).value = value_Y;
		}else{
			document.getElementById(id).value = value_N;
		}
	}
}
function checkRange(start, end, e) {
  var st=parseInt(start);
  var ed= parseInt(end);
  var vl=parseInt(e.value);
  
  if(vl<st||vl>ed){
	//alert('The value you entered is out of range ' + start + ' - ' + end);
	callCommonAlert(document.getElementById("langCode").value,"No00016",start,end);
	e.focus();
	}
}
//document.oncontextmenu = context(field, menuStr);
//document.oncontextmenu = context;
//document.onmouseout = popup_mouseout;

function context() {

	//createMenu(field, menuStr);	

	var ie	= document.all
	var ns6	= document.getElementById&&!document.all

	var isMenu 	= false ;

	var menuSelObj = null ;
	var overpopupmenu = false;
	
	var	obj = ns6 ? e.target.parentNode : event.srcElement.parentElement;	

    menuSelObj = obj ;
	var	field = ns6 ? e.target.name : event.srcElement.name;	
	
	if (ns6)
	{
		document.getElementById(field).style.left = e.clientX+document.body.scrollLeft;
		document.getElementById(field).style.top = e.clientY+document.body.scrollTop;
	} else {
		document.getElementById(field).style.pixelLeft = event.clientX+document.body.scrollLeft;
		document.getElementById(field).style.pixelTop = event.clientY+document.body.scrollTop;
	}
	document.getElementById(field).style.display = "";

	isMenu = true;
	return false ;
}
function createMenu(field, menuStr) {

	document.write('<div id="menudiv" style="position:absolute;display:none;top:20px;left:20px;z-index:1;">\n');
	document.write('<table width=82 cellspacing=1 cellpadding=0 bgcolor=lightgray>\n');
  	document.write('<tr><td>\n');
    document.write('<table width=80 cellspacing=0 cellpadding=0>\n');
    
	var valuesArray = menuStr.split(';');
	var valid = false;
	for (i = 1; i < valuesArray.length - 1; i++)
	{
		var menuItem = valuesArray[i];
		var itemArray = menuItem.split(':');
    	document.write('<tr>\n');
    	document.write('<td bgcolor="#FFFFFF" width="80" height="16" ');
    	document.write('onMouseOver="this.style.backgroundColor=\'#EFEFEF\'" ');
    	document.write('onMouseOut="this.style.backgroundColor=\'#FFFFFF\'" ');
    	document.write('onClick="javascript:setValueFromMenuItem(' + field + ',' + itemArray[0] + ')">');
    	document.write(itemArray[1] + '</td>\n');
    	document.write('</tr>\n');
	}
    
    document.write('</table>\n');
  	document.write('</td></tr>\n');
	document.write('</table>\n');
	document.write('</div>\n');

}
function setValueFromMenuItem(fieldName, value) {
	document.getElementById(fieldName).style.display = "none";
	document.form1.elements[fieldName].value = value;
}
function setValueOnClick(fieldName, value) {
	//document.getElementById(fieldName).style.display = "none";
	document.form1.elements[fieldName].value = value;
	doSub("12");
}
function popup_mouseout(fieldName) {
	document.getElementById(fieldName).style.display = "none";
}
function popup_mouseover(fieldName) {
	document.getElementById(fieldName).style.display = "";
}

/**
* fix bug46
*
* When go back from subfile, make the last field focused in.
*/
function goBackSel(fieldItem) {
	found = false;
    var i = 0;
    for (i=0; i < elts.length; i++) {
      if (elts[i].name == fieldItem) {
        found = true;
        break;
      }
    }
    if (!found) {
      selectFirstField();
      //alert("first field");
      return;
    }
    anElt = elts[i];
    //alert(fieldItem);
    if (selectableInputField(anElt)) {
      selectaField(anElt);
      //alert(anElt.name);
      return;
    } else {
		j = getIndexByName(fieldItem);

		if (j == -1) {
		  selectFirstField();
		  //alert("first field"); removed by shawn
		  return;
		}

		if (nextOPrev == "PREV") {
		  found = false;
		  for (j=j-1; j < elts.length; j++) {
			anElts = elts[j];
			if (selectableInputField(anElts)) {
				/*window.status = "Prev field found where expected.";*/
				selectaField(anElts);
				//alert(anElts.name);
				return;
			}
		  }
		  if (!found) {
			/*window.status = "No field found before the last one.";*/
			selectaField(fieldItem);
			//alert(anElt.name);
			return;
		  }
		}
	}
}

/**
* fix bug46
* 
* Clear the last focus in field.
*/
function clearFocusField() {
	lastSelectedF = null;
	aForm = document.commonForm;
    if (aForm == null) {
    	aForm = document.form1;
    }
	if (aForm.focusInField) {
      	aForm.focusInField.value = "";
    }
}

/*function validatePercentage(inputValue)
{
   var exp = new RegExp("^\\d{1,2}$|^\\d{1,2}\\.\\d{1,2}$");
   return exp.test(inputValue);
}*/

/**
*Get the single select multi-action value;

By shawn.wang @091119
*/
function checkSelAndAction(fN,rN)
{
var rownumber;
var action='';

for (var i=0; i <document.form1.action.length; i++)
   {
   if (document.form1.action[i].checked)
      {
		action = document.form1.action[i].value;
      }
   }
   
   for (var i=0; i <document.form1.slt.length; i++)
   {
   if (document.form1.slt[i].checked)
      {
		rownumber = document.form1.slt[i].value;
      }
   }
   if(document.getElementById(fN+rownumber+'')!=null)
document.getElementById(fN+rownumber+'').value=action;
}

/***
*Used to create a dom node,
*e.g. create DIV, UL, OL, SPAN
*added by shawn.wang
**/

createEl = function(nodetype, nodeattribute, nodestyle, nodeinnertext)
{ 
    var e = document.createElement(nodetype); 
    if (nodeattribute) { 
        for (var k in nodeattribute) { 
            if (k == 'class') e.className = nodeattribute[k]; 
            else if (k == 'id') e.id = nodeattribute[k]; 
    else e.setAttribute(k, nodeattribute[k]); 
    } 
    } 
    if (nodestyle) { for (var k in nodestyle) e.style[k] = nodestyle[k]; } 
    if (nodeinnertext) { e.appendChild(document.createTextNode(nodeinnertext)); } 
    return e; 
}



function doGeneralLines(textareaName1,textareaName2,count,maxlen,tablename,lineFieldName){
	var textarea1=document.getElementById(textareaName1);
	var textarea2=document.getElementById(textareaName2);
	var str="", str2="";
	if(textarea2.value != ""){
		str2="\r\n.np"+textarea2.value+"\r\n.p";
	}	
	str=textarea1.value+str2;
	//added by Ai Hao for replace html codes
	str = str = str.replace(/&/g, '&amp;');
	str = str.replace(/</g, '&lt;');
	str = str.replace(/>/g, '&gt;');	
	str = str.replace(/  /g, '&nbsp;');
	str = str.replace(/[']/g, '&#39;'); //single quote
	
	var index=0;
	for(i=1;i<=count;i++){
		//modified by Ai Hao 2010-08-05
		var hiddenValue=document.getElementsByName(tablename+"."+ lineFieldName + "_R"+i)[0];
		//var hiddenValue=document.getElementsByName(tablename+".genplen_R"+i)[0];
		hiddenValue.value="";	
		hiddenValue.value=str.substr(index,maxlen);
		index+=maxlen;
	}
}

function setTextareaValue(textareaName1,textareaName2){
var textarea1=document.getElementById("textarea1");
var textarea2=document.getElementById("textarea2");

while(textareaName1.indexOf("{rn}") > -1 ){
	textareaName1=textareaName1.replace("{rn}","\r\n"); 
}
while(textareaName2.indexOf("{rn}") > -1 ){
	textareaName2=textareaName2.replace("{rn}","\r\n");
}

//added by Ai Hao
textareaName1 = textareaName1.replace(/&amp;/g, "&");
textareaName1 = textareaName1.replace(/&lt;/g, "<");
textareaName1 = textareaName1.replace(/&gt;/g, ">");	
textareaName1 = textareaName1.replace(/&nbsp;/g, " ");
textareaName1 = textareaName1.replace(/&#39;/g, "'"); 

textareaName2 = textareaName2.replace(/&amp;/g, "&");
textareaName2 = textareaName2.replace(/&lt;/g, "<");
textareaName2 = textareaName2.replace(/&gt;/g, ">");	
textareaName2 = textareaName2.replace(/&nbsp;/g, " ");
textareaName2 = textareaName2.replace(/&#39;/g, "'");  

textarea1.value=textareaName1;
textarea2.value=textareaName2;
}

function getDateFormat(flag){
	var formatChar = getDateFormatChar();
	if(flag == "222"){
		return "  " + formatChar + "  " + formatChar +  "  ";
	} else if(flag == "224"){
		return "  " + formatChar + "  " + formatChar +  "    ";
	} else if(flag == "99"){
		return "99" + formatChar + "99" + formatChar +  "99";
	} else if(flag == "9999"){
		return "99" + formatChar + "99" + formatChar +  "9999";
	}
}
String.prototype.replaceAll  = function(s1,s2){    
    return this.replace(new RegExp(s1,"g"),s2);    
  } 
function getDateFormatChar(){
	try{
		if (dateformatChar != ''){
			return  dateformatChar;
		}
		var dformat=document.getElementById('dateformat').value.trimSpp().toUpperCase();;
		dformat = dformat.replaceAll("D","").replaceAll("M","").replaceAll("Y","");
		dateformatChar = dformat.substr(0,1);
		return  dateformatChar;
	}catch(e){
		return  '/';
	}
}

//added by Ai Hao for new tab sytle 2010-07-20
function changeTab(number,index)
{
 for (var i = 1; i <= number; i++) {
      document.getElementById('current' + i).className = 'tab';
      document.getElementById('content' + i).style.display = 'none';
 }
  document.getElementById('current' + index).className = 'tabcurrent';
  document.getElementById('content' + index).style.display = 'block';
}

function handleValueCheckBox(checkboxId, hiddenId) {
	var chks = document.getElementById(checkboxId);
	var values = document.getElementById(hiddenId);
	if (chks.checked) {
		values.value = "Y";		
	} else {
		values.value = "N";
	}
}