var row;
var pageSize;
var allPage;
var currentPage=1;
var table;
var currentDisplayPage = 1;
/*
 * @param {Object} divId ---- div for holding the navigating buttongs, 
 * 							  such as previous, next and maybe numbering buttongs
 * @param {Object} pagesize ---- rows which will be displayed on the screens
 * @param {Object} tableId ---- table ID for displaying records
 * @param {Object} displaypage ---- current page displayed on the area
 */
function page(divId, pagesize, tableId, displaypage){
	var div = divId;
	var x = "#" + tableId + " tr:has(td)";
	var z = "#" + tableId;
	table = $(x);
	row = table.length;
	pageSize = pagesize;
	allPage = parseInt("" + ((row + pageSize - 1) / pageSize), 10);
	$(table).hide();
	var contentTable = $(z);
	var width = contentTable.attr("width");
	var max_number = parseInt((width - 130) / 20, 10);
	currentPage = $("#currentpage").text();
	currentDisplayPage = displaypage;
	if (currentDisplayPage == 'null') {
		currentDisplayPage = 1;
	}
	if (currentPage == "") {
		currentPage = 1;
	}
	//create navigating bar
	createNavigtorBar(divId);

	if ((parseInt(currentDisplayPage)+1) < currentPage) {
		showRow(parseInt(currentDisplayPage)+1);
	} else {
		showRow(currentPage);
	}
}

function createNavigtorBar(divId) {
	var divObj = "#" + divId;
	$(divObj).empty();
	$(divObj).append("<span><a id='pre' style='position: relative; margin-top:10px;' href='javascript:'>Previous</a></span>");
	$(divObj).append("<span style='width:20px;';></span>");
	$(divObj).append("<span><a id='next' style='position: relative; margin-top:10px;' href='javascript:'>Next</a></span>");
	$("#next").css("width","30px");
	$("#pre").css("width","30px");
	$("#pre").bind("click", showPre);
	$("#next").bind("click", nextClick);
}

function showRow(page){
	currentPage = page - 0;
	$(table).hide();
	var first = (currentPage - 1) * pageSize;
	var last = pageSize * currentPage;
	if (last > row) 
		last = row;
	for (var i = first; i < last; i++) {
		table.eq(i).show();
	}
	document.getElementById("currentdisplaypage").value = currentPage;
	//$("#currentpage").text(currentPage);
	var endOfFile = $("#endoffile").text();
	if (currentPage == 1) {
		$("#pre").css("visibility", "hidden");
	}else {
		$("#pre").css("visibility", "visible");
	}
	if (endOfFile == 0) {
		$("#next").css("visibility", "visible");
	} else {
		if (currentPage != allPage) {
			$("#next").css("visibility", "visible");
		}else {
			$("#next").css("visibility", "hidden");
		}
	}
}
function showPre(){
	var p;
	if (currentPage - 0 == 1) {
		p = 1;
	}
	else {
		$("#pre").bind("mouseover", {}, function(){
			$(this).css("cursor", "hand");
		});
		p = currentPage - 1;
	}

	showRow(p);

}

function showNext(){
	var p;
	if (currentPage == allPage) {
		p = allPage;
	}
	else {
		p = currentPage + 1;
		$("#next").bind("mouseover", {}, function(){
			$(this).css("cursor", "hand");
		});
	}
	showRow(p);
}

function nextClick() {
	var endoffile = $("#endoffile").text();
	var currentDisP = $("#currentdisplaypage").val();
	var currentP = $("#currentpage").text();
	if (endoffile == "0") {
		if (currentDisP >= currentP) {
			doAction('PFKey90');
		} else {
			showNext();
		}
	}else {
		showNext();
	}
}
