//add by Cary zhong 06/02/2010 start
var   xmlDoc; 
function parseXML(language){
	try{//IE
		xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
	}catch(e){
		try{//Mozilla, Firefox, Opera, etc.
			xmlDoc=document.implementation.createDocument("","",null);
		}catch(e){
			return;
		}
	}
	xmlDoc.async=false;
	var locHref = location.href;
	var locArray = locHref.split("/");
	var xmlnam = "alertMessages_"+language;
	xmlDoc.load(contextPathName+"/js/"+xmlnam+".xml");
	xmlDoc.async=false;

}

function getmessageAlert(alertCode,language){
	parseXML(language);
	if (language == null || language == "" || language == undefined){language = "ENG";}
	var  cNodes=xmlDoc.getElementsByTagName("alertMessages")[0];	
	cNodes=cNodes.getElementsByTagName(alertCode)[0];
	//cNodes=cNodes.getElementsByTagName(language)[0];
	return cNodes.firstChild.nodeValue;
}
String.prototype.trimSpp = function() { var t = this.replace(/(^\s*)|(\s*$)/g, "");    
return t.replace(/(^ *)|( *$)/g, "");}
function callCommonAlert(){
	try{
		var language = arguments[0].trimSpp().substr(0,3).toUpperCase();
		var alertCode = arguments[1].trimSpp();
		var msg = getmessageAlert(alertCode,language);
		for(var i=2;i<arguments.length;i++){
			msg = msg.replace("$"+(i-1),arguments[i]);
		}
		alert(msg);
	}catch(e){//alert(e.message);
		return;
	}
}
function callCommonConfirm(){
	try{
		var language = arguments[0].trimSpp().substr(0,3).toUpperCase();
		var alertCode = arguments[1].trimSpp();
		var msg = getmessageAlert(alertCode,language);
		for(var i=2;i<arguments.length;i++){
			msg = msg.replace("$"+(i-1),arguments[i]);
		}
		return msg;
	}catch(e){//alert(e.message);
		return;
	}
}
//add by Cary zhong 06/02/2010 end