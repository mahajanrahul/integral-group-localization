<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.quipoz.framework.util.*" %>
<%@ page import="java.util.Locale" %>
<%@page import="com.resource.ResourceBundleHandler"%>
<html>
<head>
	<title>logout</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
	<LINK REL="StyleSheet" HREF="theme/eng/QAStyle.jsp" TYPE="text/css">
	<LINK REL="StyleSheet" HREF="theme/eng/QAStyle.css" TYPE="text/css">
</head>

<body class="main" style='overflow-x:hidden; overflow-y:hidden; width=716px;' >
<DIV class="main" style='position: absolute; left: 4%; top:5px; width: 100%; height: 100%'>
<div class="roundleft"></div>
<div class="roundborder"></div>
<div class="roundright"></div>
<div class="roundborderv"></div>
<div class="roundleft2"></div>
<div class="roundborderv" style="left:752px;   height:500px;"></div>
<div class="roundborder" style="left:-15px; width:758px; top:519px;"></div>
<div class="roundright2"></div>
<div class="titleNew">
<%long time = 0;
	ResourceBundleHandler resourceBundle = null;
	String units = "";
	String lang="eng";
	//String title="Timeout Page";
	String title=null;
	String fonttype="font-size:12px; font-weight:bold;  font-family:Arial;";
	try{
	if (session != null) {
		BaseModel baseModel = (BaseModel) session.getAttribute(BaseModel.SESSION_VARIABLE );
    	if ( baseModel != null) {
			AppVars av = baseModel.getApplicationVariables();
			lang=av.getUserLanguage().trim();
			resourceBundle = new ResourceBundleHandler(av.getLocale());
			if (av != null) {
				time = av.getAppConfig().getUserTimeout()/1000;
				/**if("CHI".equalsIgnoreCase(lang)){
				 fonttype="font-size:12px; font-weight:normal;  font-family:宋体;";
					title="超时页";
					units = "秒";
					if (time > 60) {
						time = time/60;
						units = "分";
						if (time > 60) {
							time = time/60;
							units = "小时";
						}
					}
				}else{*/
				//units = "seconds";
					units = resourceBundle.gettingValueFromBundle("seconds");
				if (time > 60) {
					time = time/60;
					//units = "minutes";
					units = resourceBundle.gettingValueFromBundle("minutes");
					if (time > 60) {
						time = time/60;
						//units = "hours";
						units = resourceBundle.gettingValueFromBundle("hours");
					}
				}
			//}
			}
		}
		session.invalidate();
	}
	}catch(Throwable e){
	}
	if(resourceBundle == null)
	{
		resourceBundle = new ResourceBundleHandler(new Locale("en","US"));
	}
	title = resourceBundle.gettingValueFromBundle("Timeout Page");
	%>

<p id="screenTitleP" style="text-align:left;color:#346495;position:absolute;top:9px;margin-left:10px; font-weight:bold; width:90%; font-size:16px;  "><%=title%></p>

</div>
<div id="mainDiv" style="height: 479px; position: absolute; width: 778px; background-color: DDDDDD; z-index: 2; top: 40px; left: -22px; text-align: center; <%=fonttype %>">
<br>
<br>

<%if("CHI".equalsIgnoreCase(lang)){
if (time > 0) {%>
		<p>由于<%=time%> <%=units%>内未执行任何操作导致系统超时。
		 </p>
		<p>您需要重新登录系统。 </p>
		<p>此超时处理是出于系统安全性考虑，由此给您造成的不便我们深表歉意。	</p>
		
<%}else {%>
		<p>由于1小时内未执行任何操作导致系统超时。	</p>
		<p>您需要重新登录系统。 </p>
		<p>此超时处理是出于系统安全性考虑，由此给您造成的不便我们深表歉意。	</p>
		
<%}
}else{%>
<%if (time > 0) {%>
		<p><%=resourceBundle.gettingValueFromBundle("You have been timed out after more than")%><%=time%> <%=units%> 
		<%=resourceBundle.gettingValueFromBundle("of	inactivity")%>. </p>
		<p><%=resourceBundle.gettingValueFromBundle("You will have to log on again in order to use the system")%>. </p>
		<p><%=resourceBundle.gettingValueFromBundle("We apologise for the inconvenience")%>,
		<%=resourceBundle.gettingValueFromBundle("however such timeouts are required for security reasons")%>.</p>
<%}else {%>
		<p><%=resourceBundle.gettingValueFromBundle("You have been timed out after an extended period of inactivity")%>.</p>
		<p><%=resourceBundle.gettingValueFromBundle("You will have to log on again in order to use the system")%>. </p>
		<p><%=resourceBundle.gettingValueFromBundle("We apologise for the inconvenience")%>,
		<%=resourceBundle.gettingValueFromBundle(" however such timeouts are required for security reasons")%>.</p>
<%}
} %>
</div>
</div>
</body>
</html>
