<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<HTML>
<HEAD>
<title>Timings</title>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="expires" content="-1">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-store">
<LINK REL="StyleSheet" HREF="theme/eng/QAStyle.jsp" TYPE="text/css">
<LINK REL="StyleSheet" HREF="theme/eng/QAStyle.css" TYPE="text/css">

<%@ page import="com.quipoz.framework.util.*" %>
<%@ page import="com.quipoz.framework.screenmodel.*" %>

</head>
<%
    BaseModel baseModel = (BaseModel) session.getAttribute(BaseModel.SESSION_VARIABLE );
    if ( baseModel != null) {
        ScreenModel fw = (ScreenModel) baseModel.getScreenModel();
%>
<BODY class="main">
<script type="text/javascript">
var contextPathName = "<%= request.getContextPath() %>";
</script>
<script src="js/Sidebar.js"></script>
<h2>Timings</h2>
<p>
<Button onClick="window.showModalDialog ('TimingsClear.jsp', ' ', 'dialogWidth:640px; dialogHeight:480px; resizable:yes; status:yes;');window.close()"'>Clear Timings</Button>
<br><Button onClick="window.close()">Close this window</Button>
<p>
<%String timings = fw.getAppVars().getTimings().toString();
  timings = QPUtilities.replaceSubstring(timings, "\n", "$n<br>");
  timings = QPUtilities.replaceSubstring(timings, "$n<br>", "\n<br>");
%>
<%=timings%>
<%}%>
</BODY>
</HTML>
