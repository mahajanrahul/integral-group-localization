<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<HTML>
<HEAD>
<title>Program call statistics</title>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="expires" content="-1">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-store">
<LINK REL="StyleSheet" HREF="theme/QAStyle.jsp" TYPE="text/css">
<LINK REL="StyleSheet" HREF="theme/QAStyle.css" TYPE="text/css">

<%@ page import="java.util.*" %>
<%@ page import="com.quipoz.COBOLFramework.util.COBOLConvCodeModel" %>
</head>
<BODY class="main">
<h2>Program references</h2>
<table class=none style='width:400px'>
<%Hashtable ht = COBOLConvCodeModel.statistics;
  String[] s = new String[ht.size()];
  s = (String[]) ht.keySet().toArray(s);
  Arrays.sort(s);
  for (int i=0; i<s.length; i++) {
    Integer v = (Integer)ht.get(s[i]);%>
    <tr><td><%=s[i]%></td><td align=right><%=v.toString()%></td></tr>
<%}%>
</table>
</BODY>
</HTML>