<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@page import="java.lang.ref.SoftReference"%>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<HTML>
<HEAD>
<title>Program structures in memory</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="expires" content="-1">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-store">
<LINK REL="StyleSheet" HREF="theme/QAStyle.jsp" TYPE="text/css">
<LINK REL="StyleSheet" HREF="theme/QAStyle.css" TYPE="text/css">

<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="com.quipoz.framework.util.AppVars" %>
</head>
<BODY class="main">
<h2>Memory</h2>
<%=(Runtime.getRuntime().totalMemory()/1000000) + " in JVM, " + (Runtime.getRuntime().freeMemory()/1000000) + " free."%>
<h2>Program references</h2>
<table class=none style='width:800px'>
<%Hashtable ht = AppVars.performance;
  String[] s = new String[ht.size()];%>
<%="Count of known Objects: " + s.length %>
<%s = (String[]) ht.keySet().toArray(s);
  Arrays.sort(s);
  for (int i=0; i<s.length; i++) {
    String v = "Cleaned up";
    Object[] thing = (Object[])ht.get(s[i]);
    SoftReference r = (SoftReference)thing[0];
    if (r != null) {
    	Object o = r.get();
    	if (o != null) {
    		v = o.getClass().getSimpleName();
    	}
    	else {
    		ht.remove(s[i]);
    	}
    }%>
    <tr>
    <td><button onClick="window.open('Referencea.jsp?ArrElt=<%=s[i]%>', 'Diagnostics', 'height=480,width=640,status=yes,toolbar=no,menubar=yes,location=no,resizable=yes,scrollbars=yes')"><%=s[i]%></button></td>
    <td align=right><%=v%></td>
    </tr>
<%}
 System.gc();%>
</table>
</BODY>
</HTML>