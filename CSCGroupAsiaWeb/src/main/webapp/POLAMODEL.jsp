<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Frameset//EN">
<%@page import="com.ia.framework.dialect.FrameworkDialectFactory"%>
<%@ page session="true" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.quipoz.framework.util.*" %>
<%@ page import="com.csc.groupasia.runtime.variables.*" %>
<%@ page import="com.quipoz.framework.screenmodel.*" %>
<%@ page import="java.util.*" %>
<%@ page import="com.csc.groupasia.runtime.core.GroupAsiaAppLocatorCode" %>
<%@ page import="com.resource.ResourceBundleHandler"%>
<HTML>
<%
    char fileSeparator = '/'; //System.getProperty("file.separator");
    BaseModel baseModel = (BaseModel) session.getAttribute(BaseModel.SESSION_VARIABLE );
    String screenForm = "UnavailableSingle.jsp";
    String sideofScreen="POLASidebar.jsp";
    if ( baseModel != null) {
    	GroupAsiaAppVars av = (GroupAsiaAppVars)baseModel.getApplicationVariables();
		String lang = av.getUserLanguage().toString().trim().toLowerCase();
		String scr = av.getNextSingleModelJSPScreen();
		String path;

		/* JSP path should be relevant to SxxxxScreenVars other than program
		 * e.g.
		 *  P4135 is in com.csc.polisy.underwriting.programs
		 *  S4135ScreenVars is in com.csc.polisy.reporting.screens
		 *  S4135Form.jsp is in web_root/polisy/reporting/...
		 * Further, to use java package of screen vars will improve performance as well
		 *  as it does not need to use Class.forName() a lot.
		 */

		VarModel screenVars = baseModel.getScreenModel().getVariables();
		if (screenVars == null) {
			path = GroupAsiaAppLocatorCode.findPath(scr);
		} else {
			path = screenVars.getClass().getPackage().getName();
		}
		String[] sa = path.split("\\.");
		if (sa.length < 4 || scr.startsWith("JSPTester") || scr.startsWith("TestOnline")) {
			screenForm = scr;
		} else {
			String prefix = sa[2] + fileSeparator;
			if (sa.length >= 5) {
				prefix += sa[3] + fileSeparator;
			}
			screenForm = prefix + "eng" + fileSeparator + scr;
			/* ~cpatodi #IGROUP-1761 Sidebar values are not displaying for "jap" locale as path contain "jap" folder which does not exist*/
			//sideofScreen=prefix + lang + fileSeparator+"sdof"+scr;
			sideofScreen=prefix + "eng" + fileSeparator+"sdof"+scr;
			/* ~cpatodi #IGROUP-1761 Sidebar values are not displaying for "jap" locale as path contain "jap" folder which does not exist*/
            // If there is no Chinese version jsp, then switch to English version
            if (!lang.equals("eng") && request.getSession().getServletContext().getResource(fileSeparator + screenForm) == null) {
                screenForm = prefix + "eng" + fileSeparator + scr;
            }
            if(sideofScreen.equals("smart/eng/sdofS0028Form.jsp")) {
            	if(!FrameworkDialectFactory.getInstance().isLife()) {
            		sideofScreen="POLASidebar.jsp";	
            	}
            }
            else {
            	if(request.getSession().getServletContext().getResource(fileSeparator + sideofScreen) == null) {
                    sideofScreen="POLASidebar.jsp";
                }	
            }
		}
	}
    String ctx = request.getContextPath() + fileSeparator;
    GroupAsiaAppVars av = (GroupAsiaAppVars)baseModel.getApplicationVariables();
    ResourceBundleHandler rb = new ResourceBundleHandler(av.getLocale());
    // added by wayne 2008-04-07

   	// Fix bug 3858 (wrong menu displayed when logout then login again)
    // Get system menu and master menu from instances saved in the session.
    // Getting menus from ApVars.getInstance() would not work as menu data
    // is generated and saved in different AppVars instance in different thread.
    Object[] systemMenuArray = AppVars.getInstance().getSystemMenu();
    HashMap map = AppVars.getInstance().getMasterMenu();
  	if (request.getSession().getAttribute("SYSTEM_MENU") != null && request.getSession().getAttribute("MASTER_MENU") != null) {
   	    systemMenuArray = (Object[]) request.getSession().getAttribute("SYSTEM_MENU");
   	    map = (HashMap) request.getSession().getAttribute("MASTER_MENU");

   	    request.getSession().removeAttribute("SYSTEM_MENU");
   	    request.getSession().removeAttribute("MASTER_MENU");
   	}


   	// added by wayne 2008-04-07

    ScreenModel fw = (ScreenModel) baseModel.getScreenModel();
	String masterMenu = "[]";
	String systemMenu = "[]";
	//for multi-user login, systemMenuArray is null. so need to bypass null pointer error. revised by Bear, 2010-3-10
	if (systemMenuArray == null){
	}else{
		//build systemMenu array in javascript
		StringBuffer sb = new StringBuffer();
		sb.append("[");
		for (int i = 1;i<systemMenuArray.length;i++){
		//for (int i = 1;i<2;i++){
			String masterMenuName = systemMenuArray[i].toString().substring(11).trim();
			sb.append("[");
			sb.append("\""+masterMenuName+"\"");
			sb.append(",");
			sb.append("\""+i+"\"");
			sb.append("]");
			sb.append(",");
		}
		sb.deleteCharAt(sb.length()-1);
		sb.append("]");
		systemMenu = sb.toString();

		sb.setLength(0);
		Iterator iter = map.keySet().iterator();
		sb.append("[");
		while (iter.hasNext()){
		//{
			String key = (String)iter.next();
			String index = key.substring(0, key.indexOf("map"));
			String subMenuName = map.get(key).toString().substring(11).trim();
			sb.append("[");
			sb.append("\""+index+"\"");
			sb.append(",");
			sb.append("\""+subMenuName+"\"");
			sb.append(",");
			sb.append("\""+key+"\"");
			sb.append("]");
			sb.append(",");
		}
		sb.deleteCharAt(sb.length()-1);
		sb.append("]");
		masterMenu = sb.toString();
	}
%>


  <HEAD>
  	<%
  		//IGROUP-1211
  		if(AppConfig.ieEmulationEnable){
  	%>
  	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
  	<%}%>
  	<meta http-equiv="pragram" content="no-cache">
  	<meta http-equiv="cache-control" content="no-cache, must-revalidate">
  	<meta http-equiv="expires" content="0">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <LINK REL="StyleSheet" HREF="theme/eng/QAStyle.jsp" TYPE="text/css">
    <LINK REL="StyleSheet" HREF="theme/eng/QAStyle.css" TYPE="text/css">

    <script type="text/javascript">
	    var scriptPath = "../../../js/";
	    var imagePath = "<%= request.getContextPath() %>/screenFiles/";
	    var contentScript = "../../../js/content2.js";
    </script>
    	<!--IGroup-937-->
        </SCRIPT>
        <SCRIPT language=javascript src="js/jquery-1.3.2.min.js">
        </SCRIPT>
        <SCRIPT language=javascript src="js/menuG4Loaderfs.js">
        </SCRIPT>
        <SCRIPT language=javascript type=text/javascript src="js/menuG4IE5fs.js">
        </SCRIPT>
        <%	// To decide whether menus are displayed or not
	boolean isShow = av.isMenuDisplayed();
	if  (isShow){%>

      <script language=javascript type="text/javascript">
        var systemMenu = <%=systemMenu%>;
        var masterMenu = <%=masterMenu%>;
      	addMenu("Demo", "top-menu");

        for (var i = 0;i<systemMenu.length;i++){
        	addSubMenu("top-menu", systemMenu[i][0], "", "", systemMenu[i][1], "x");
		}
		//Modify by shawn @20100721 for master menu style
		addSubMenu("top-menu", " &nbsp;", "", "", "", "x1");
		var index = masterMenu[0][0];
		var count = 0;
		var others = 0;
		for (var j = 0;j<masterMenu.length;j++){
			if (index == masterMenu[j][0]){
				count++;
				if (count < 21){
					if(count==1) {
						addLink(masterMenu[j][0], " ", "", "", "y0");//add top
						addLink(masterMenu[j][0], masterMenu[j][1], "", masterMenu[j][2], "y");
					}else{
					addLink(masterMenu[j][0], masterMenu[j][1], "", masterMenu[j][2], "y");
					}
				}else if (count == 21){
					others++;
					<%-- <%if(av.getUserLanguage().trim().equalsIgnoreCase("CHI")){%>
					addSubMenu(masterMenu[j][0], "更多......", "", "", "others"+others,"y");
					<%}else{%>--%>
					addSubMenu(masterMenu[j][0], "<%=rb.gettingValueFromBundle("More......")%>", "", "", "others"+others,"y");
					<%--<%}%> --%>
					if(others==1){
						addLink("others"+others, "", "", "", "y0");
						addLink("others"+others, masterMenu[j][1], "", masterMenu[j][2], "y");
					}else{
						addLink("others"+others, masterMenu[j][1], "", masterMenu[j][2], "y");
					}
				}else{
					addLink("others"+others, masterMenu[j][1], "", masterMenu[j][2], "y");
				}
       		}else{
       			if(count>21){
       			addLink("others"+others, "", "", "", "y1");
       			}
       			addLink(index, " ", "", "", "y1");//add bottom
       			index = masterMenu[j][0];
       			count = 1;
       			addLink(masterMenu[j][0], "", "", "", "y0");
       			addLink(masterMenu[j][0], masterMenu[j][1], "", masterMenu[j][2], "y");
       		}
		}
		addLink(index, " ", "", "", "y1");//add bottom
		endMenu();

       	addStylePad("pad", "width:225; border-size:0;  border-style:solid; border-color:#eeeeee; background-color:#eeeeee; offset-left:0; offset-top:0;");
        addStylePad("subpad", "visibility:hidden;");
        <%-- <%if(av.getUserLanguage().toString().trim().toLowerCase().equals("eng")){%>--%>
        addStyleItem("item", "width:225; padding:2 6; border-size:0; border-style:solid solid; border-color:#eeeeee; background-color:#eeeeee #4b80b2;");
         addStyleFont("bluefont", "font-family:Arial; font-size:12; font-weight:bold; color:#4d81b1 #eeeeee; text-align:left;");
       addStyleFont("blackfont", "font-family:Arial; font-size:12; font-weight:bold; color:#e5e5e5 #ffff00; text-align:left;");

       <%-- <%}else{%>
        addStyleItem("item", "width:225; padding:2 6; border-size:0; border-style:solid solid; border-color:#eeeeee; background-color:#eeeeee #4b80b2;");
        addStyleFont("bluefont", "font-family:宋体; font-size:12; font-weight:normal; color:#4d81b1 #eeeeee; text-align:left;");
       addStyleFont("blackfont", "font-family:Arial; font-size:12; font-weight:bold; color:#e5e5e5 #ffff00; text-align:left;");

        <%}%> --%>

        addStyleItem("sub-item", "width:207; padding:2 16;");
        addStyleItem("top-item","width:207; height:18; padding:2 16; ");//IGroup-937
        addStyleItem("menu-bottom","height:20");//IGroup-937

        addStyleMenu("menu", "pad", "item", "bluefont", "", "");
        addStyleMenu("submenu", "subpad", "sub-item", "blackfont", "", "");
        addStyleMenu("submenu2", "subpad", "sub-item", "blackfont", "", "");
        addStyleMenu("topbottom1", "subpad", "top-item", "", "", "");//IGroup-937
        addStyleMenu("bottom1", "pad", "menu-bottom", "", "", "");//IGroup-937

        addStyleGroup("group", "menu", "top-menu");
        for (var i = 0;i<systemMenu.length;i++){
        	addStyleGroup("group", "submenu", systemMenu[i][1]);
		}
		for (var i = 1;i<=others;i++){
        	addStyleGroup("group", "submenu2", "others"+i);
		}
        addStyleGroup("group", "submenu", "sub-1", "sub-2", "sub-3");
        addStyleGroup("group", "submenu2", "sub-31");
        addStyleGroup("group","topbottom1","y0","y1");//IGroup-937
        addStyleGroup("group","bottom1","x1");//IGroup-937
        setBGImage("y0", "", "bg_off.png", "bg_off.png");
     	setBGImage("y", "", "bg_off.png", "bg_off.png");
     	setBGImage("y1", "", "bg_bottom_sub.png", "bg_bottom_sub.png");
     	//Modify by shawn @20100721 for master menu style
     	setBGImage("x1", "", "bg_mastermenu_bt_on.png", "bg_mastermenu_bt_on.png");
        setBGImage("x", "", "bg_mastermenu_on.png", "bg_bottom_sub.png");
		addInstance("Demo", "Demo", "position:0 0; align:left; valign:top; target:main; style:group");
 </SCRIPT>
        <%	}%>
    <TITLE>INTEGRAL Admin</TITLE>
  </HEAD>

	<Script>ctx = "<%=ctx%>";</Script>

  <FRAMESET  cols="229,*"   frameBorder="0" framespacing="0">
			<FRAME style="border: 0"  noresize="noresize" NAME="frameMenu" src='<%=ctx%><%=sideofScreen %>' scrolling="no">
				<FRAMESET name="activeframe" ROWS="*, 0%"  frameBorder=0 framespacing="0">
				<%if  (request.getParameter("currentdisplaypage") != null) { %>
				<FRAME style="border: 0" SRC='<%=ctx%><%=screenForm%>?currentdisplaypage=<%=request.getParameter("currentdisplaypage")%>' noresize="noresize" frameborder='0' NAME='mainForm'>
				<% }else{ %>
				<FRAME style="border: 0" SRC='<%=ctx%><%=screenForm%>' frameborder='0' NAME='mainForm'>
				<%} %>
				<FRAME style="border: 0" SRC='<%=ctx%>heartbeat.jsp' NAME='heartbeat'></FRAME>
			</FRAMESET>
	</FRAMESET>
</HTML>
