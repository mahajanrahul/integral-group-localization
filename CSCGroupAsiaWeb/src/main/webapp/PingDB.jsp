<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<HTML>
<HEAD>
<title>Timings</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="expires" content="-1">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-store">
<LINK REL="StyleSheet" HREF="theme/eng/QAStyle.jsp" TYPE="text/css">
<LINK REL="StyleSheet" HREF="theme/eng/QAStyle.css" TYPE="text/css">

<%@ page import="com.quipoz.framework.util.*" %>
<%@ page import="com.quipoz.framework.screenmodel.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="com.csc.groupasia.runtime.variables.GroupAsiaAppVars" %>

<%
    long t0 = System.currentTimeMillis();
    BaseModel baseModel = (BaseModel) session.getAttribute(BaseModel.SESSION_VARIABLE );
    GroupAsiaAppVars av = (GroupAsiaAppVars)baseModel.getApplicationVariables();
	Connection conn = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	String val = request.getParameter("tab");
	if (val == null || val.trim().length() == 0) {
		val = "SYS.DUAL";
	}
	long t1 = System.currentTimeMillis();
	long t2 = 0;
	long t3 = 0;
	long t4 = 0;
	long t5 = 0;
	long t6 = 0;
	String sql = "select 1 from " + val;
	String err = "";
	try {
		conn = av.getDBConnection("PingDB");
		t2 = System.currentTimeMillis(); t3 = t2; t4 = t2; t5 = t2; t6 = t2;
		ps = conn.prepareStatement(sql);
		t3 = System.currentTimeMillis(); t4 = t3; t5 = t3; t6 = t3;
		rs = ps.executeQuery();
		t4 = System.currentTimeMillis(); t5 = t4; t6 = t4;
		rs.next();
		rs.getInt(1);
		t5 = System.currentTimeMillis(); t6 = t5;
		av.freeDBConnectionIgnoreErr(conn, ps, rs);
		t6 = System.currentTimeMillis();
		err = "None";
	}
	catch (Exception e) {
		err = e.toString();
		err = err.replace('"', '\'');
		err = err.replaceAll("\n", "<br>");
		err = err.replaceAll("\r", "<br>");
	}
	finally {
		av.freeDBConnectionIgnoreErr(conn, ps, rs);
	}
%>

</head>
<BODY class="main">
<script type="text/javascript">
var contextPathName = "<%= request.getContextPath() %>";
</script>
<script src="js/Sidebar.js"></script>
<h2>Network response + database estimate</h2>
<p>This screen submits a trivial request to the Application Web Server,
and executes a simple DB2 query.
<p>No application code is used.
<p>This test can be used to see if response time issues are due to the
Network/Web Server and database connection.
<p>To just see the network response, use Ping.
<p>Table to SELECT from <input name=tab value=<%=val%>></input>
<Button onClick='doAgain()'>Repeat</Button>
<br><Button onClick="window.close()">Close this window</Button>
<Script>
    sT = window.dialogArguments;
    sT1 = new Date(sT.getYear(), sT.getMonth(), sT.getDate(), sT.getHours(), sT.getMinutes(), sT.getSeconds());
    sTime = fm2(sT.getHours()) + ":" + fm2(sT.getMinutes()) + ":" + fm2(sT.getSeconds()) + "." + fm3(sT.getTime() - sT1.getTime()); 

    eT = new Date();
    eT1 = new Date(eT.getYear(), eT.getMonth(), eT.getDate(), eT.getHours(), eT.getMinutes(), eT.getSeconds());
    eTime = fm2(eT.getHours()) + ":" + fm2(eT.getMinutes()) + ":" + fm2(eT.getSeconds()) + "." + fm3(eT.getTime() - eT1.getTime()); 

	document.write("<Table border=1>");
	document.write("<tr><td>Request  time:</td><td>" + sTime + "</td></tr>");
	document.write("<tr><td>SQL</td><td><%=sql%></td></tr>");
	document.write("<tr><td>Response received</td><td>" + eTime + "</td></tr>");
	document.write("<tr><td>Web/Response time</td><td>" + ((eT.getTime() - sT.getTime())/1000) + "</td></tr>");
	document.write("<tr><td colspan=2>Database/JSP components:</td></tr>");
	document.write("<tr><td>SQL Executed</td><td><%=sql%></td></tr>");
	document.write("<tr><td>Java Declarations</td><td><%=t1-t0%></td></tr>");
	document.write("<tr><td>Get DB connection</td><td><%=t2-t1%></td></tr>");
	document.write("<tr><td>Prepare SQL</td><td><%=t3-t2%></td></tr>");
	document.write("<tr><td>Execute SQL</td><td><%=t4-t3%></td></tr>");
	document.write("<tr><td>Fetch results</td><td><%=t5-t4%></td></tr>");
	document.write("<tr><td>Free resources</td><td><%=t6-t5%></td></tr>");
	document.write("<tr><td>SQL errors encountered</td><td><%=err%></td></tr>");
	document.write("</Table>");
	
  function fm2(num) {
    if (num < 10) return "0" + num;
    return num;
  }
  
  function fm3(num) {
    if (num < 10) return "00" + num;
    if (num < 100) return "0" + num;
    return num;
  }
  
  function doAgain() {
  	var v = "PingDB.jsp?tab=" + document.all.tab.value;
  	window.showModalDialog (v, new Date(), 'dialogWidth:640px; dialogHeight:480px; resizable:yes; status:yes;');
  	window.close();
  }
</Script>
</BODY>
</HTML>