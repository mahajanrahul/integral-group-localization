\<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<HTML>
<HEAD>
<title>Diagnostics</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="expires" content="-1">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-store">
<LINK REL="StyleSheet" HREF="theme/eng/QAStyle.jsp" TYPE="text/css">
<LINK REL="StyleSheet" HREF="theme/eng/QAStyle.css" TYPE="text/css">

<%@ page import="java.util.*" %>
<%@ page import="com.quipoz.framework.error.*" %>
<%@ page import="com.quipoz.framework.util.*" %>
<%@ page import="com.quipoz.framework.tablemodel.*" %>
<%@ page import="com.quipoz.framework.screenmodel.*" %>

</head>
<%
    BaseModel baseModel = (BaseModel) session.getAttribute(BaseModel.SESSION_VARIABLE );
    if ( baseModel != null) {
        ScreenModel fw = (ScreenModel) baseModel.getScreenModel();
%>
<BODY class="main">
<script type="text/javascript">
var contextPathName = "<%= request.getContextPath() %>";
</script>
<script src="js/Sidebar.js"></script>
<h2>Memory</h2>
<%=(Runtime.getRuntime().totalMemory()/1000000) + " in JVM, " + (Runtime.getRuntime().freeMemory()/1000000) + " free."%>
<p>
<!--Button onClick="window.showModalDialog ('DiagnosticsClear.jsp', ' ', 'dialogWidth:640px; dialogHeight:480px; resizable:yes; status:yes;');window.close()"'>Clear Diagnostics</Button-->
<Button onClick='window.open("DiagnosticsClear.jsp", "Diagnostics", "height=480,width=640,status=yes,toolbar=no,menubar=yes,location=no,resizable=yes,scrollbars=yes")'>Clear Diagnostics</Button>
<br><Button onClick="window.close()">Close this window</Button>
<p>
<%String[] sa = QPUtilities.parseXML(fw.getDiagnostics().toString(), "line");%>
  <pre>
  <%=sa[0]%>
<%while (!sa[1].equals("")) {
  	String aLine = sa[1];
	aLine = QPUtilities.replaceSubstring(aLine, "'", "&#039;");
	aLine = QPUtilities.replaceSubstring(aLine, "\"", "&#039;&#039;");

  	String sb[] = QPUtilities.parseXML(aLine, "longtext");
  	if (!sb[1].equals("")) {
  		aLine = "\n<br>" + AppVars.hf.formatDiagnosticPreamble(sb[0], sb[1]) + sb[2];
  	}
  	else {
  		aLine = "\n<br>" + aLine;
  	}
  	
  	sb = QPUtilities.parseXML(aLine, "SQLmsg");
  	boolean neitherfound = true;
  	if (!sb[1].equals("")) {
	  	neitherfound = false;
  		String sql = QPUtilities.extractXMLTag(sb[1], "msg");
		sql = QPUtilities.replaceSubstring(sql, "\n", " ");
  		String access = QPUtilities.extractXMLTag(sb[1], "access");
  		String where = QPUtilities.extractXMLTag(sb[1], "where");
  		String code = QPUtilities.extractXMLTag(sb[1], "code");
  		String time = QPUtilities.extractXMLTag(sb[1], "time").trim();
  		aLine = sb[0] + AppVars.hf.formatDiagnosticSQL(sql, access, where, code, time) + sb[2];
  	}
  	
  	sb = QPUtilities.parseXML(aLine, "SQLLongmsg");
  	if (!sb[1].equals("")) {
	  	neitherfound = false;
  		String sql = QPUtilities.extractXMLTag(sb[1], "msg");
		sql = QPUtilities.replaceSubstring(sql, "\n", " ");
  		String access = QPUtilities.extractXMLTag(sb[1], "access");
  		String where = QPUtilities.extractXMLTag(sb[1], "where");
  		String code = QPUtilities.extractXMLTag(sb[1], "code");
  		String time = QPUtilities.extractXMLTag(sb[1], "time");
  		aLine = sb[0] + AppVars.hf.formatDiagnosticLongSQL(sql, access, where, code, time) + sb[2];
  	}
	if (neitherfound) {
		aLine = QPUtilities.replaceSubstring(aLine, "\n", "<br>");
		aLine = QPUtilities.replaceSubstring(aLine, "<br><br>", "<br>");
		aLine = QPUtilities.removeTrailing(aLine, "<br>");
	}
  	%><%=aLine%>
  	<%
  	sa = QPUtilities.parseXML(sa[2], "line");
  }
%>
</pre>
<%}%>
</BODY>
</HTML>