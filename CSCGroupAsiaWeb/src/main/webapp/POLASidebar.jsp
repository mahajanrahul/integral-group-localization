﻿<%@page import="org.apache.tools.ant.filters.EscapeUnicode"%>
<%@page import="org.apache.commons.lang3.StringEscapeUtils" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import='com.quipoz.framework.util.*'%>
<%@ page import='com.quipoz.framework.screenmodel.*'%>
<%@ page import='com.quipoz.COBOLFramework.screenModel.COBOLVarModel'%>
<%@ page import="com.csc.groupasia.runtime.variables.*"%>
<%@ page import="com.quipoz.COBOLFramework.util.COBOLAppVars"%>
<%@page import="com.resource.ResourceBundleHandler"%>
<%@ page import="com.csc.smart400framework.SMARTHTMLFormatter" %>
<%
	//added by Ai Hao for new error message style
%>
<%@page import="com.quipoz.framework.datatype.StringBase"%>
<%@ page import="com.quipoz.framework.util.*"%>
<%@ page import="com.quipoz.framework.error.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.properties.PropertyLoader" %>
<%
	BaseModel bm3 = (BaseModel) request.getSession().getAttribute(BaseModel.SESSION_VARIABLE);
	ScreenModel fw3 = (ScreenModel) bm3.getScreenModel();
	COBOLAppVars cobolAv3 = (COBOLAppVars) bm3.getApplicationVariables();
	ResourceBundleHandler resourceBundleHandler = new ResourceBundleHandler(cobolAv3.getLocale());//<!-- Japanese Internationalize change -->
	ResourceBundleHandler resourceBundleHandlerFun = new ResourceBundleHandler(fw3.getScreenName(),cobolAv3.getLocale() );//<!-- Japanese Internationalize change -->
	String imageFolder = PropertyLoader.getFolderName(cobolAv3.getLocale().toString());



	String lang1 = bm3.getApplicationVariables().getUserLanguage().toString().toUpperCase();

	AppConfig appCfg = AppConfig.getInstance();//<!-- Japanese Internationalize change -->
	Object[] systemMenuArray = AppVars.getInstance().getSystemMenu();
  	if (request.getSession().getAttribute("SYSTEM_MENU") != null && request.getSession().getAttribute("MASTER_MENU") != null) {
   	    systemMenuArray = (Object[]) request.getSession().getAttribute("SYSTEM_MENU");
   	}
%>

<HTML>

<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script language="javascript" src="js/menuG4f.js"></script>
<script language='javaScript' src='js/jquery-1.3.2.min.js'></script>
<script language='javaScript' src='js/xmlReader.js'></script>
<script language='javaScript'>
	<%char[] c = cobolAv3.additionalValidKeys.toCharArray();
			String validKeys = "[";
			for (int i = 0; i < c.length; i++) {
				if (c[i] == '1') {
					validKeys += i + ",";
				}
			}
			if (validKeys.endsWith("]")) {
				validKeys += "";//empty array
			} else {
			validKeys = validKeys.substring(0,validKeys.length()-1)+"]";
		}
	%>
	var validKeyArr = <%=validKeys%>;
	var isSupported = false;
	var lang1 = "<%=lang1%>";
	function isSupportKey(validKeyArr,action){
		if (validKeyArr.length > 0){
			for (var i=0;i<validKeyArr.length;i++){
				if (validKeyArr[i] == action){
					isSupported = true;
					break;
				}
			}
			if (isSupported){
				clearFField(); // fix bug46
				doAction('PFKEY0'+action);
			}else{
			<%-- /*<% if ("CHI".equals(lang1)) { %>
			alert("功能键" + action + "不能在当前屏幕使用");
			<%} else {%> --%>
			    alert('<%=resourceBundleHandler.gettingValueFromBundle("Function key")%>' + action + '<%=resourceBundleHandler.gettingValueFromBundle("is not active on this screen at this time")%>'+'.');
				//alert("Function key " + action + " is not active on this screen at this time.");
			<%--  <%}%>*/ --%>
			callCommonAlert(lang1,"No0002",action);
			}
		} else{
			//alert("no keys are supported");
			callCommonAlert(lang1,"No00020");
		}
	}

   function errorHelp(ctx,code,message) {
	enCodeMessage = encodeURI(message);
    thisElt = code;
    window.open(ctx + "I90ErrorHelp.jsp?code='" + thisElt + "'"+"&message='"+enCodeMessage+"'");
    return false;
}

</script>

<%@ page session="false"%>
<title>Generic SideBar</title>
<!-- remove QAStyle.css by Michelle (2010-7-20) -->
<LINK REL="StyleSheet" HREF="theme/<%=lang1.toLowerCase()%>/style.css"
	TYPE="text/css">

<%@ page import="com.quipoz.framework.util.*"%>
<%@ page import="com.quipoz.framework.screenmodel.*"%>
<%
	HttpSession sess = request.getSession();
	BaseModel baseModel = (BaseModel) sess.getAttribute(BaseModel.SESSION_VARIABLE );
	if (baseModel != null) {
		baseModel.getApplicationVariables();
		ScreenModel fw = (ScreenModel) baseModel.getScreenModel();
		//add by gulizhi
		Class svClass = fw.getVariables().getClass();


%>
<script type="text/javascript">
function OPSTo(nextField){
	var fieldName=nextField;
	var doc=parent.frames["mainForm"].document;
	if(doc.getElementById(fieldName)!=null)
	{

		doc.getElementById(fieldName).value="X";
		doAction('PFKEY0');
		}
}

function hyperLinkTo(nextField){
	if (nextField != null){
		nextField.value="X";
		doAction('PFKEY0');
	}

}
function changeImg(thi,path){
	thi.src=path;
}

function disableIcon(thi){
  thi.src = thi.src.replace(".png","_after.png");
  thi.disabled = "true";
}
function removeXfield(xfield){

var doc=parent.frames["mainForm"].document;
		doc.getElementById(xfield.id).value="";
		doAction('PFKEY05');


}

</script>
</HEAD>
<BODY onload="initMenu2('Demo', 'top');setSubFrame('Demo', parent.main)" onResize="doResize(); ">
<!--sidemessage Added it for new lay out by Ai Hao(2010-7-23) Begin-->
<%
	String msgs = "No Message";
		String fontStyle = " font-family:Arial; font-weight:bold; font-size:12px;";
		String lang = request.getParameter("lang");
	if("CHI".equalsIgnoreCase(lang)){
			msgs = "没有信息";
			fontStyle = " font-type:宋体; font-weight:normal; font-size:12px;";
	}
		try {
			sess = request.getSession();
		BaseModel bm = (BaseModel) sess.getAttribute( BaseModel.SESSION_VARIABLE );
			AppVars avs = bm.getApplicationVariables();
			lang = avs.getUserLanguage().toString().trim();
			if (!avs.mainFrameLoaded) {
  			for (int i=0; i<40; i++) {
					avs.waitabit(250);
					if (avs.mainFrameLoaded) {
						break;
					}
				}
			}
			if (!avs.mainFrameLoaded) {
		  	msgs = "Messages failed to load after 10 seconds."
						+ "<br>This is usually caused by slow response on the main form; errors may be incorrect."
						+ "<br>Press the refresh button here when the main form loads."
						+ "<button onClick='document.location.reload(false)'>Refresh errors</Button>";
		}
		else {
				String ctx = request.getContextPath() + "/";
				MessageList list = avs.getMessages();
				Iterator i = list.iterator();
				StringBuffer sb = new StringBuffer();
				while (i.hasNext()) {
					String str = i.next().toString();
                 HTMLFormatter formatter = new HTMLFormatter();

					/* Remove any trailing ? which is historic and means "stop underlining" */
					str = QPUtilities.removeTrailing(str.trim(), "?");
					//added by wayne to parse errorno
					String[] arr = str.split("ErrorMessage");
					if (arr.length < 2)
						sb.append(str + "<br>");
					else {
						sb.append(arr[0]);
						for (int j = 1; j < arr.length - 1; j++) {
						sb.append("<a href='#' onclick=\"return errorHelp('")
						.append(ctx).append("','")
						.append(arr[j].substring(0,4)).append("','")
						.append(formatter.HTMLIfy(QPUtilities.removeTrailing(arr[j].substring(4), "?")))
									.append("')\">")
						.append(/* StringEscapeUtils.escapeHtml4 */(formatter.HTMLIfy(QPUtilities.removeTrailing(arr[j].substring(4), "?"))))	//IGROUP-1763
						.append("</a><br/>")
						;
					}
					sb.append("<a href='#' onclick=\"return errorHelp('")
						.append(ctx).append("','")
						.append(arr[arr.length-1].substring(0,4)).append("','")
						.append(formatter.HTMLIfy(QPUtilities.removeTrailing(arr[arr.length-1].substring(4), "?")))
								.append("')\">")
						.append(/* StringEscapeUtils.escapeHtml4 */(formatter.HTMLIfy(QPUtilities.removeTrailing(arr[arr.length-1].substring(4), "?"))))	//IGROUP-1763
								.append("</a>");
						sb.append("<br>");
					}
				}
				msgs = sb.toString();
				msgs = msgs.replaceFirst("Message:", "");
			}
	}
	catch (Exception e) {
		}
%>


<script language="javascript">
   	// Ai Hao 2010-05-07
   	// if there is no error message, hide the error message panel
   	$(document).ready(function(){
		if(<%=msgs.length()%> == 0)	$(".sideMsg").css("display","none");
    });
</script>
<!--sidemessage Added it for new lay out by Ai Hao(2010-7-23) End-->

<script src=js/Sidebar.js></script>
<div class="sidearea">
	<div class="logoarea"><img src="screenFiles/<%=imageFolder%>/img_logo.gif" width="219" height="50" alt="logo"></div>
	<!-- Japanese Internationalize change start-->
<% if (Str.YES_CC.equalsIgnoreCase(appCfg.getShowScreenID())) { %>
		<div class="logoarea" style="position:absolute; height:50px; width:219px; top:39px; left:5px; border:none;font-size:11px"><%= fw3.getScreenName() %></div>
	<%} %>
	<!-- Japanese Internationalize change end-->	
		<!-- <div class="logoarea" style="position:absolute; height:50px; width:219px; top:41px; left:65px; border:none;font-size:10px;">4.3.10</div> -->
		<div class="logoarea" style="position:absolute; height:50px; width:219px; top:11px; left:72px; border:none;font-size:10px;color:white;"><%=appCfg.getVersion() %></div>
		<div class="logoarea" style="position:absolute; height:50px; width:219px; top:22px; left:90px; border:none;font-size:10px;color:white;"><%=appCfg.getFsuVersion() %></div>
<%
	// To decide whether menus are displayed or not
		GroupAsiaAppVars av = (GroupAsiaAppVars) baseModel.getApplicationVariables();
		boolean isShow = av.isMenuDisplayed();
		if (!isShow) {
	%>
<!--sideextrainfo  Added it for new lay out by Michelle(2010-7-20)-->
<div class="sidebararea ">
<div class="sidebarbg sideextrainfo">
<div class="title">
      <!-- modified to remove chinese language code for the I18N of integral life. -->
	<%--
		<%if(av.getUserLanguage().toString().equalsIgnoreCase("ENG")){ %>  --%>
		<img src="screenFiles/<%=imageFolder%>/title_extrainfo.gif" width="85" height="25" alt="extra info">
		 <%-- <%}else{ %>
		<img src="screenFiles/<%=imageFolder%>/title_extrainfo_cn.gif" width="85" height="25" alt="extra info">
		<%} %>
--%>
</div>

<div class="sidebar ">
	<div id="sidebar_OPTS">

	</div>
</div>
</div><!-- Ticket IGROUP-937 Modified for other browser xma3-->
<img src="screenFiles/<%=imageFolder%>/bg_sidemenu_02.gif" width="219" height="14" style="margin-top:-5px;" alt="">
</div>


<!--sidefunction Added it for new lay out by Michelle(2010-7-20)-->
<div class="sidebararea ">
<div class="sidebarbg sidefunction" style="height:115px;height:75px\9;height:95px\9\0;"> <!-- IGroup-1086 --> <!-- BSIBG-323 --> 
<div class="title">
<!-- modified to remove chinese language code for the I18N of integral life. -->
        <%--
<%if(av.getUserLanguage().toString().equalsIgnoreCase("ENG")){ %> --%>
<img src="screenFiles/<%=imageFolder%>/title_functions.gif" width="85" height="25" alt="functions">
	<%--
<%}else{ %>
<img src="screenFiles/<%=imageFolder%>/title_functions_cn.gif" width="85" height="25" alt="functions">
<%} %>
--%>
</div>

<%
String sa[]=fw.getFormActions();
int k=sa.length;
String link[]=new String[k];
String fSize[] = null;
String interValue=null;
int order=0;
for (int i = 0; i < sa.length; i++) {
	fSize = sa[i].split("/");
	if(fSize!=null)
	{
		for(int j =0; j<fSize.length;j++)
		{
			if(order==0)
			{
				link[i]= resourceBundleHandlerFun.gettingValueFromBundle(fSize[j]);//<!-- Japanese Internationalize change -->
			}
			else
			{
				link[i]= link[i]+"/"+resourceBundleHandler.gettingValueFromBundle(fSize[j]);//<!-- Japanese Internationalize change -->
			}
				order++;
		}
		order=0;
	}
}
%>
<div class="sidebar">
    <%=AppVars.hf.getHTMLFormActionButtons(link)%>
	<%-- <%=AppVars.hf.getHTMLFormActionButtons(fw.getFormActions())%> --%>
<div id="sidebar_functions">
<!-- Wayne Yang 2010-05-07. This Div will contains all the handle-writting function. e.g. S4050 cancel function -->
</div>
</div>
</div>
<img src="screenFiles/<%=imageFolder%>/bg_sidemenu_02.gif" width="219" height="28" alt="">
</div>

<!--sidemessage Added it for new lay out by Michelle(2010-7-20)-->
<div class="sidebararea sideMsg">
<div class="sidebarbg sidemessage">
<div class="title">
<!-- modified to remove chinese language code for the I18N of integral life. -->
<%-- <%if(av.getUserLanguage().toString().equalsIgnoreCase("ENG")){ %> --%>
<img src="screenFiles/<%=imageFolder%>/title_messages.gif" width="87" height="25" alt="messages">
<%-- <%}else{ %>
<img src="screenFiles/<%=imageFolder%>/title_messages_cn.gif" width="87" height="25" alt="messages">
	<%} %> --%>
</div>
<div class="message" style="height: 80px; overflow-y: auto;">
<div style="left: 1px;"><%=msgs%></div>
</div>
</div>
<img src="screenFiles/<%=imageFolder%>/bg_sidemenu_02.gif" width="219" height="14" alt="">
</div>
<!--sidemessage-->

	<%}else{ %>
	<!-- Added it for new lay out by Michelle(2010-7-20)  -->
	<!-- Modify by shawn @20100721 for master menu style -->
<div class="sidebararea">
	<div class="sidebarbg sidemainmenu" style="height:auto;" >
	<div class="title" style="border-left:#4d81b1 1px solid;border-top:#4d81b1 1px solid;border-right:#4d81b1 1px solid;">
	<!-- modified to remove chinese language code for the I18N of integral life. -->
    <%--
	<%if(av.getUserLanguage().toString().equalsIgnoreCase("ENG")){ %> --%>
	<img src="screenFiles/<%=imageFolder%>/title_mainmenu.gif" width="85" height="25" alt="mainmenu">
    <%--  <%}else{ %>
	<img src="screenFiles/<%=imageFolder%>/title_mainmenu_cn.gif" width="85" height="25" alt="mainmenu">
	<%} %>
--%>
</div>
</div>
	<img src="screenFiles/<%=imageFolder%>/bg_sidemenu_02.gif" width="219" height="14" alt="">
	</div>

<%int sideMsgTop = (systemMenuArray.length-1) * 19 - 23; %>
<!--sidemessage Added it for new lay out by Michelle(2010-7-20)-->
	<div class="sidebararea sideMsg" style="margin-top:31px; padding-top:<%=sideMsgTop%>px">
<div class="sidebarbg sidemessage">
<div class="title">
	<!-- modified to remove chinese language code for the I18N of integral life. -->
    <%--
	<%if(av.getUserLanguage().toString().equalsIgnoreCase("ENG")){ %> --%>
	<img src="screenFiles/<%=imageFolder%>/title_messages.gif" width="87" height="25" alt="messages">
	<%--  <%}else{ %>
	<img src="screenFiles/<%=imageFolder%>/title_messages_cn.gif" width="87" height="25" alt="messages">
	<%} %> --%>
	</div>
<div class="message" style="height: 80px; overflow-y: auto;">
<div style="left: 1px;"><%=msgs%></div>
</div>
</div>
<img src="screenFiles/<%=imageFolder%>/bg_sidemenu_02.gif" width="219" height="14" alt="">
</div>
	<!--sidemessage-->
	<%} %>
</div>
</div>
<div id="scriptDiv"></div>
<Script language="javascript">
//timer=setInterval(checkload,100) ;
waitMainFormReady();
function loaddata(){

    /* Wayne Yang 2010-03-29
     For Option Switch:
     To copy code block from mainForm to the side bar page.
     In this way, no need to add extra info in *.xml.
     Just need to setup the Hyper link in SxxxxForm.jsp . Then copy them to side bar page. */
	var doc1=parent.frames["mainForm"].document;
	var obj = doc1.getElementById("mainForm_OPTS");
	if(obj !=null)
   	{
   		document.getElementById("sidebar_OPTS").innerHTML =  obj.innerHTML;
   	}

   	/* Wayne Yang 2010-05-07
   	   For handle-writting functions. e.g. S4050 cancel function
   	*/
   	var obj1 = doc1.getElementById("mainForm_functions");
	if(obj1 !=null)
   	{
   		document.getElementById("sidebar_functions").innerHTML =  obj1.innerHTML;
   	}
}
function waitMainFormReady()
{
	if(parent.frames["mainForm"].document.readyState == "complete")
	{
		loaddata();
	}
	else
	{
		//alert("wait");
		setTimeout("waitMainFormReady()",100);

	}
}


</script>

<%}%>
</BODY>
</HTML>

