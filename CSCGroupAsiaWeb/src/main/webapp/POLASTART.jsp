<%@ page import='com.quipoz.framework.util.BaseModel' %>
 <%BaseModel bm3 = (BaseModel) request.getSession().getAttribute(BaseModel.SESSION_VARIABLE );
  String lang1  = bm3.getApplicationVariables().getUserLanguage().toString().toUpperCase();
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<%
char fileSeparator = '/';
String ctx = request.getContextPath() + fileSeparator;
 %>
<head>
<link rel="stylesheet" href="theme/Master.css" type="text/css">
<title>POLASTART</title>
<script language='javaScript' src='<%=ctx%>/js/xmlReader.js' charset=UTF-8></script>
<script language="javascript" type="text/javascript">
function closeWindow() {
	/*if monitor resolution is 1024*768, then display the application at left and top
	**else center the application screen
	**if the monitor resolution is smaller than 1024*768, display a warning message
	*/
	var lang1 = "<%=lang1%>";
	var screenWidth = screen.width;
	var screenHeight = screen.height;
	if ((screenWidth < 1024) || (screenHeight < 768)) {
		//alert("Please change the resolution to 1024*768 in order to display the application normally");
		callCommonAlert(lang1,"No00026");
	}
	var newWindownName = window.name;
	window.name = window.name + "_temp";
	var displayFeatures = 'toolbar=1, location=1, menubar=1, status=1, scrollbars=1, width=1024, height=600, resizable=0';
	if ((screenWidth == 1024) &&(screenHeight == 768)) {
		displayFeatures += ', left=0, top=0';
	}else {
		var left = (screenWidth - 1024)/2;
		var top = (screenHeight - 768)/2;
		displayFeatures += ', left='+ left + ', top=' + top;
	}
	var newWindow = window.open('<%=ctx%>' + 'POLAMODEL.jsp?currentdisplaypage=' + '<%=request.getParameter("currentdisplaypage")%>', newWindownName, displayFeatures);
	if (!newWindow) {
		//alert("Popup Window is not allowed, please make sure to make it allowed for this application running.");
		callCommonAlert(lang1,"No00027");
	}
	if(navigator.appName=="Microsoft Internet Explorer") {
		this.focus();
		window.open('','_self');
		window.close();
	}else {
		window.open('','_parent','');
		window.close();
	}
	return newWindow;
}


</script>

</head>
<body onload="closeWindow();">

</body>
</html>