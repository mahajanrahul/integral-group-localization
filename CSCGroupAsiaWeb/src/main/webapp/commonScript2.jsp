<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import='com.quipoz.framework.util.*' %>
<%@ page import='com.quipoz.framework.screenmodel.*' %>
<%@ page import='com.quipoz.COBOLFramework.screenModel.COBOLVarModel' %>
<%@ page import="com.csc.groupframework.variables.*" %>
<%@ page import="com.quipoz.COBOLFramework.util.COBOLAppVars" %>
<%@ page import="java.util.HashMap" %>
<%! String focusName = "";%>
<%BaseModel bm3 = (BaseModel) request.getSession().getAttribute(BaseModel.SESSION_VARIABLE );
  ScreenModel fw3 = (ScreenModel) bm3.getScreenModel();
  COBOLAppVars cobolAv3 = (COBOLAppVars)bm3.getApplicationVariables();
  
  if (request.getSession().getAttribute("FocusObjects") != null) {
  	HashMap fObject = (HashMap) request.getSession().getAttribute("FocusObjects");
  	if (fObject.containsKey(fw3.getScreenName())) {
  		focusName = (String) fObject.get(fw3.getScreenName());
  	}
  }
%>
	<input type="hidden" name="<%=RequestParms.ACTION%>" value="PFKey0">
    <input type="hidden" name="<%=RequestParms.SCREEN_NAME%>" value="<%=fw3.getScreenName()%>"/>
    <input type="hidden" name="<%=RequestParms.FOCUSFIELD%>" value="">
    <input type="hidden" name="<%=RequestParms.FOCUSINFIELD%>" value="<%=focusName%>">
    <input type="hidden" name="<%=RequestParms.FOCUSPREVF%>" value="">
    <input type="hidden" name="<%=RequestParms.FOCUSNXTPR%>" value="">
    <input type="hidden" name="<%=RequestParms.SEMAPHORE%>"  value="">
  <!-- input class="invisibleSubmit" type="SUBMIT" name="lastResort" value="Enter" onClick="if (document.activeElement.name != 'lastResort') {return false;}" onFocus="selectFirstField()" onMouseDown="selectCurrentField();return false"  onSelectStart="selectFirstField()" TABINDEX=32767 -->


<%=AppVars.hf.getLit("</form>")%>


<script language="javascript">
//the code is used the repostion of the options div.  (Michelle Ma 10-1-26)
var navbarObj = document.getElementById("navbar");
if(navbarObj!=null&&navbarObj!="undefined"){
   var contentCount = navbarObj.getElementsByTagName("li").length;
   var top2 = parseFloat(navbarObj.style.top.replace("px",""));
   var top1,tempObj,topOver;
   for(var i=1;i<=contentCount;i++){
     var contentObj = document.getElementById("content"+i);
     var selectObj = contentObj.getElementsByTagName("select");
     if(selectObj!=null&&selectObj!="undefined"){
         for(var j=0;j<selectObj.length;j++){              
            top1 = parseFloat(selectObj[j].parentNode.parentNode.style.top.replace("px",""));         
            topOver = 387 - (selectObj[j].size-1)*15     
            if(top1+top2>topOver){
              selectObj[j].parentNode.parentNode.style.top = (top1 - (43+15*(selectObj[j].size-1)))+"px";
            }
         }
     }
   }
 
}
</script>

<script language='javaScript'>
	rollUpEnabled = <%=cobolAv3.isPagedownEnabled()%>
   	rollDownEnabled = <%=cobolAv3.isPageupEnabled()%>;

   	<%=AppVars.hf.getValidKeys(fw3.getFormActions())%>
    alerts = "<%=AppVars.hf.formatMessageBox(fw3.getAlerts())%>";
    if (alerts != "") {
      alert(alerts);
      alerts = "";
    }
    promptstr = "<%=fw3.getPrompt()%>";
    if (promptstr != "") {
      if (confirm(promptstr)) {
            document.forms[0].action_key.value = "Yes";
      }
      else {
            document.forms[0].action_key.value = "No";
      }
      doLoad();
      doSub();
    }
    
//Added it for the style after clicking the button (@Michelle 2010-7-19)    
function changeDivClass(thi,actionKey){

if($(thi).parent().parent().hasClass("sectionbtndisable")){
  thi.onclick=function(){return  false;};
 }
else{
  $(thi).parent().parent().removeClass();
  $(thi).parent().parent().addClass("sectionbtndisable");
  thi.onclick=function(){return  false;};
  doAction(actionKey);
}
   
}
/*IGroup-937 yzhong4 start*/
function changeContinueImage(thi,actionKey){
   //var sorce=thi.childNodes[0];
   var sorce;
   var sorceSrc;
   $(thi).find("img").each(function(i) {
		sorce = $(this);
		sorceSrc = $(this).attr("src");
		return false;
	});  
   var img1 = new Image();
   var flagForSorce = typeof(sorce);
  
  if(flagForSorce=="undefined")
  {
   sorce=thi.src;
  }else{
   //sorce=thi.childNodes[0].src;
	  sorce=sorceSrc;
  }
    
   var endObj = sorce.indexOf("_hover");
   
   
   if(endObj!=-1){
   sorce=sorce.substring(0,endObj)+".gif";   
   }
   if(sorce.indexOf("_after")==-1){
   
   img1.src=sorce.replace(".gif","_after.gif");
       if(flagForSorce=="undefined"){
        thi.src=img1.src;
       }else{
       // thi.childNodes[0].src=img1.src;
    	   $(thi).find("img").each(function(i) {
   			$(this).attr("src",img1.src);
   			return false;
   		});
       }
       
       thi.onclick=function(){return  false;};
       doAction(actionKey);
  
   }else{
   	thi.onclick=function(){return  false;};
   }
}

//copy a duplicated method to handle png image
//gu lizhi 2010-7-21
function changeContinueImagePNG(thi,actionKey){
   //var sorce=thi.childNodes[0];
   var sorce;
   var sorceSrc;
   $(thi).find("img").each(function(i) {
		sorce = $(this);
		sorceSrc = $(this).attr("src");
		return false;
	});  
   var img1 = new Image();
   var flagForSorce = typeof(sorce);
  if(flagForSorce=="undefined")
  {
   sorce=thi.src;
  }else{
   //sorce=thi.childNodes[0].src;
	  sorce=sorceSrc;
  }
   var endObj = sorce.indexOf("_hover");
   
   if(endObj!=-1){
   sorce=sorce.substring(0,endObj)+".png";   
   }
   if(sorce.indexOf("_after")==-1){
   
   img1.src=sorce.replace(".png","_after.png");
       if(flagForSorce=="undefined"){
        thi.src=img1.src;
       }else{
        //thi.childNodes[0].src=img1.src;
    	   $(thi).find("img").each(function(i) {
    			$(this).attr("src",img1.src);
    			return false;
    		});  
       }
       /*IGJL-711 CHANGES START*/
       //thi.onclick=function(){return  false;};
       if(!doAction(actionKey)){
       		//var clickEvent="changeContinueImagePNG("+ thi+",'"+actionKey+"')"; 
			//thi.onclick=clickEvent;
			img1.src=sorce.replace("_after.png",".png");
    	   	$(thi).find("img").each(function(i) {
   			$(this).attr("src",img1.src);
   			return false;
			}); 
       }
  
   }else{
   	thi.onclick=function(){return  false;};
   }
   /*IGJL-711 CHANGES END*/
} 
function getSorce(thi){
	var sorce;
	var sorceSrc;
	   $(thi).find("img").each(function(i) {
			sorce = $(this);
			sorceSrc = $(this).attr("src");
			return false;
		});  
}
function changeMouseover(thi){
  // var sorce=thi.childNodes[0];
  var sorce;
   var sorceSrc;
   $(thi).find("img").each(function(i) {
		sorce = $(this);
		sorceSrc = $(this).attr("src");
		return false;
	});  
   if(typeof(sorce)=="undefined"){
    sorce = thi.src;
   if(sorce.indexOf("_after")==-1){
   var img1 = new Image();
   img1.src=sorce.replace(".gif","_hover.gif");
   thi.src=img1.src; 
   }
   }
   else{
   //sorce = sorce.src;
   sorce=sorceSrc;
   if(sorce.indexOf("_after")==-1){
   var img1 = new Image();
   img1.src=sorce.replace(".gif","_hover.gif");
   //thi.childNodes[0].src=img1.src;
   $(thi).find("img").each(function(i) {
		$(this).attr("src",img1.src);
		return false;
	});
   }
   }
}
//copy a duplicated method to handle png image
//gu lizhi 2010-7-21
function changeMouseoverPNG(thi){
   //var sorce=thi.childNodes[0];
   var sorce;
   var sorceSrc;
   $(thi).find("img").each(function(i) {
		sorce = $(this);
		sorceSrc = $(this).attr("src");
		return false;
	});
   if(typeof(sorce)=="undefined"){
    sorce = thi.src;
   if(sorce.indexOf("_after")==-1){
   var img1 = new Image();
   img1.src=sorce.replace(".png","_hover.png");
   thi.src=img1.src; 
   }
   }
   else{
   //sorce = sorce.src;
   sorce=sorceSrc;
   if(sorce.indexOf("_after")==-1){
   var img1 = new Image();
   img1.src=sorce.replace(".png","_hover.png");
   //thi.childNodes[0].src=img1.src; 
   $(thi).find("img").each(function(i) {
		$(this).attr("src",img1.src);
		return false;
	});
   }
   }
}
function changeMouseout(thi){
   //var sorce=thi.childNodes[0];
    var sorce;
   var sorceSrc;
   $(thi).find("img").each(function(i) {
		sorce = $(this);
		sorceSrc = $(this).attr("src");
		return false;
	}); 
   if(typeof(sorce)=="undefined"){
    sorce = thi.src;
   if(sorce.indexOf("_after")==-1){
   var img1 = new Image();
   img1.src=sorce.replace("_hover.gif",".gif");
   thi.src=img1.src; 
   }
   }else{
   //sorce = sorce.src;
   sorce=sorceSrc;
   if(sorce.indexOf("_after")==-1){
   var img1 = new Image();
   img1.src=sorce.replace("_hover.gif",".gif");
   //thi.childNodes[0].src=img1.src; 
   $(thi).find("img").each(function(i) {
		$(this).attr("src",img1.src);
		return false;
	});
   }
   }
}
//copy a duplicated method to handle png image
//gu lizhi 2010-7-21
function changeMouseoutPNG(thi){
   //var sorce=thi.childNodes[0];
   var sorce;
   var sorceSrc;
   $(thi).find("img").each(function(i) {
		sorce = $(this);
		sorceSrc = $(this).attr("src");
		return false;
	});
   if(typeof(sorce)=="undefined"){
    sorce = thi.src;
   if(sorce.indexOf("_after")==-1){
   var img1 = new Image();
   img1.src=sorce.replace("_hover.png",".png");
   thi.src=img1.src; 
   }
   }else{
   //sorce = sorce.src;
   sorce=sorceSrc;
   if(sorce.indexOf("_after")==-1){
   var img1 = new Image();
   img1.src=sorce.replace("_hover.png",".png");
   //thi.childNodes[0].src=img1.src; 
   $(thi).find("img").each(function(i) {
		$(this).attr("src",img1.src);
		return false;
	});
   }
   }
}
/*IGroup-937 yzhong4 end*/
function setDefaultRadio(nameObj,defaultValue){
  
  var radioCheck = false;
  $(document).find("input[name='"+nameObj+"']").each(function(i){
   if(this.checked) radioCheck = true;
  });
  if(!radioCheck){
  $(document).find("input[name='"+nameObj+"']").each(function(i){
   if(this.value==defaultValue) this.checked = 'checked'; 
  });
  }
}

function setCheckboxVal(obj,nameObj,checkValue,uncheckValue){
  if(obj.checked){
    $("input[name='"+nameObj+"']").val(checkValue);
  }else{
    $("input[name='"+nameObj+"']").val(uncheckValue);
  }
}
</script>

<%    fw3.setFrameLoaded(2);%>

