<%@ page import="java.util.*" %> 
<%@ page import="com.quipoz.framework.error.*" %>   
<%@ page import="com.quipoz.framework.exception.*" %> 
<%@ page import="com.quipoz.framework.util.*" %>  
<%@ page import="com.quipoz.framework.screenmodel.*" %> 
<%@ page import="com.properties.PropertyLoader" %> 
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%	
Locale locale = (Locale)request.getSession().getAttribute("integralLocale");
String loc;
if(locale == null){
	loc = "en_US";
}
else
{
	loc = locale.toString();
}
String imageFolder = PropertyLoader.getFolderName(loc);
%>

<HTML>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /></head>
<LINK REL="StyleSheet" HREF="theme/eng/QAStyle.jsp" TYPE="text/css">
<LINK REL="StyleSheet" HREF="theme/eng/QAStyle.css" TYPE="text/css">
  <BODY class="menu">
	<table>
		<tr><td><img src="screenFiles/<%=imageFolder%>/csclogo.gif" class="logo"></td></tr>
	</table>

    <BR/>
	  <P TITLE="Return to Main Menu"><A class="dmd_text_label" href="." TARGET="_top">Main Menu</A></P>            
  </BODY>
</HTML>