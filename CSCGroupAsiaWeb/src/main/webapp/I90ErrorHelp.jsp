<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page session="false"%>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="org.apache.commons.lang3.StringEscapeUtils" %>
<html>
<head>
<title>POLISY/Unix</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%
String ctx = request.getContextPath() + "/";
%>
<LINK REL="StyleSheet" HREF="<%=ctx%>theme/eng/QAStyle.jsp" TYPE="text/css">
<LINK REL="StyleSheet" HREF="<%=ctx%>theme/eng/QAStyle.css" TYPE="text/css">
<%@ page import="com.quipoz.framework.util.*"%>
<%@ page import="com.quipoz.framework.tablemodel.*"%>
<%@ page import="com.quipoz.framework.screenmodel.*"%>
<%@ page import="com.quipoz.framework.datatype.*"%>
<%@ page import="javax.servlet.http.*"%>
<%@ page import="com.csc.groupasia.runtime.variables.*"%>
<%@ page import="java.sql.*"%>
</head>
<%
	HttpSession session1 = request.getSession(false);
	String codeString = request.getParameter("code");
	byte[] b = new byte[100];
	b = request.getParameter("message").getBytes("iso-8859-1");
	String messageString = new String(b,"utf-8");
	String code = codeString.substring(1,codeString.length()-1).toUpperCase();
	String message = StringEscapeUtils.escapeHtml4(messageString.substring(1,messageString.length()-1).toUpperCase());
	BaseModel baseModel = (BaseModel) session1
			.getAttribute(BaseModel.SESSION_VARIABLE);
	if (baseModel == null) {
		return;
	}
	GroupAsiaAppVars av = (GroupAsiaAppVars) baseModel
			.getApplicationVariables();
%>
<body>
<!-- add by max wang(csc) to add message -->
<%=message%>
<p>
<%
	String help = av.getErrorHelp(code);
	if (help == null){
		// get language
		FixedLengthStringData language = av.getUserLanguage();
		if ((language.toString().startsWith("e")) ||(language.toString().startsWith("E")))
			help = "No details found";
		else if ((language.toString().startsWith("c")) ||(language.toString().startsWith("C")))
			help = "查无细节";
		else
			help = "No details found";
	}
	out.write(help);
%>
</p>
</body>
</html>
