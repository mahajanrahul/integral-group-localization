<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page session="false"%>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head>
<title>POLISY/Unix</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%
String ctx = request.getContextPath() + "/";
%>
<LINK REL="StyleSheet" HREF="<%=ctx%>theme/eng/QAStyle.jsp" TYPE="text/css">
<LINK REL="StyleSheet" HREF="<%=ctx%>theme/eng/QAStyle.css" TYPE="text/css">
<%@ page import="com.quipoz.framework.util.*"%>
<%@ page import="com.quipoz.framework.tablemodel.*"%>
<%@ page import="com.quipoz.framework.screenmodel.*"%>
<%@ page import="com.quipoz.framework.datatype.*"%>
<%@ page import="javax.servlet.http.*"%>
<%@ page import="com.csc.groupasia.runtime.variables.*"%>
<%@ page import="java.sql.*"%>
</head>
<%
	HttpSession session1 = request.getSession(false);
	String fieldString = request.getParameter("field");
	String field = fieldString.substring(1,fieldString.length()-1);
	BaseModel baseModel = (BaseModel) session1.getAttribute(BaseModel.SESSION_VARIABLE);
	if (baseModel == null) {
		return;
	}

	GroupAsiaAppVars av = (GroupAsiaAppVars) baseModel.getApplicationVariables();
	VarModel vm = baseModel.getOnScreenModel().getVariables();
	
	//Connection con = av.getAnotherDBConnection("I90FieldHelp");
	//String sql = "SELECT helpline from helppf where helptype='F' and helpitem='MAND'";
	//PreparedStatement ps = con.prepareStatement(sql);
	//ResultSet rs = ps.executeQuery();
%>
<body class="main" onload="doLoad();" <%="onResize='doResize();'"%>>
<%
	//while (rs.next()) {
	//	out.write(rs.getString(1)+"<br>");
	//}
	out.write(av.getFieldHelp(field, vm));
%>
