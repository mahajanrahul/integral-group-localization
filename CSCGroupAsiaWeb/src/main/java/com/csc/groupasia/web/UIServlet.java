package com.csc.groupasia.web;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;//Japanese Internationalize change

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.groupasia.runtime.core.GroupAsiaAppLocatorCode;
import com.csc.groupasia.runtime.variables.GroupAsiaAppVars;
import com.csc.groupasia.runtime.variables.GroupAsiaAppVarsSon;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.SmartVarModel;
import com.properties.PropertyLoader;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datadictionarydatatype.DDFieldWindowing;
import com.quipoz.framework.datadictionarydatatype.DDScreenWindowing;
import com.quipoz.framework.datadictionarydatatype.DataDictionaryObject;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.BaseModel;
import com.quipoz.framework.util.VarModel;
import com.quipoz.framework.util.WebUtil;

/**
 * This class is the responsible for generating JSP according to Screen(S4014) and language.
 *
 * FileName: UIServlet.java
 * Create time: 3rd July,2008
 *
 * Change history:
 * Created by Benny on 3rd July, 2008
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 * @author Benny
 * @version 1.0
 */
public class UIServlet extends HttpServlet {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UIServlet.class);
	
	private static final long serialVersionUID = 1L;
	private GroupAsiaAppVarsSon appVars;
	private BaseModel bm;
	private SmartVarModel smartVarModel;
	private String programPath;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/* 
	 * This method is responsible for receiving the user request and dispatcher request to corresponding servlet.
	 * 
	 * (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter pw = response.getWriter();
		if (request.getParameter("type")==null) {
			appVars = getPolisyAppVarsInstance(request,"E", "");
			generateCompanyBranchArray(request);
			dispalyQueryPage(pw,request);
		} else if (request.getParameter("type").equals("2")) {
			String jspNo = request.getParameter("jspno");
			String language = request.getParameter("language");
			if(language.equalsIgnoreCase("E")){
				language = "en";
			}else if(language.equalsIgnoreCase("S")){
				language = "zh";
			}
			String company = request.getParameter("company");
			request.getSession().setAttribute("language", language);
			if(jspNo!=null && jspNo.length()>2){
				jspNo = jspNo.substring(0, 1).toUpperCase()+jspNo.substring(1).toLowerCase();
			}
			
			dispalyJSPPage(request, response, jspNo,language,company);
		} 
	}

	/**
	 * This method is responsible for displaying the JSP by screenForm and language by user.
	 * @param request
	 * @param response
	 * @param screenForm
	 * @param language
	 * @throws ServletException
	 * @throws IOException
	 */
	private void dispalyJSPPage(HttpServletRequest request,
			HttpServletResponse response, String screenForm, String language,String company)
			throws ServletException, IOException {
        GroupAsiaAppVars appVars = new GroupAsiaAppVars(getServletContext().getInitParameter(
            "Appname"));
       
		bm = new BaseModel();
		if(company==null||"".equals(company.trim())){
			company = "1";
		}
		bm.setCompany(new FixedLengthStringData(company));
		bm.setFsuco(new FixedLengthStringData(company));
		// get PolisyAisa AppVars
		appVars = getPolisyAppVarsInstance(request,language, screenForm);
		appVars.setContextPath(request.getContextPath().replaceFirst("/", ""));	//Japanese Internationalize change
		appVars.setLocale(request.getLocale());//Japanese Internationalize change
		if (appVars != null) {
			String browserLocale = appVars.getAppConfig().getBrowserLocale();
			Locale bLocale = null;
			if (!"Y".equals(browserLocale) && browserLocale !=null) {
				bLocale = new Locale(appVars.getAppConfig().getBrowserLocale().split("_")[0], appVars.getAppConfig().getBrowserLocale().split("_")[1]);
				appVars.setLocale(bLocale);

			} else {
				appVars.setLocale(request.getLocale());
			}
			String languageCode = getLanguageCode(appVars.getLocale().toString());
			String userLanguage = getUserLanguage(appVars.getLocale().toString());
			if(null == languageCode){
				languageCode =  getLanguageCode(request);
			}
			if(null == userLanguage){
				userLanguage = getUserLanguage(request);
			}
			appVars.setLanguageCode(languageCode);
			appVars.setUserLanguage(userLanguage);
			request.getSession().setAttribute("integralLocale", appVars.getLocale());
		}
                smartVarModel = initSmartModel(request,screenForm);
		getScreenModel(screenForm);
		forwardScreen(request, response, screenForm);
	}
 public String getLanguageCode(HttpServletRequest req)
	{
		return PropertyLoader.getLanguageCode(req.getLocale().toString());
	}
	/**
	 * Added to fetch the language from the properties file for Integral i18n
	 */
	public String getUserLanguage(HttpServletRequest req)
	{
		 return PropertyLoader.getFolderName(req.getLocale().toString());
	}
    public String getLanguageCode(String loc)
    {
        return PropertyLoader.getLanguageCode(loc);
    }
    
    /**
     * Added to fetch the language from the properties file for Integral i18n
     */
    public String getUserLanguage(String loc)
    {
         return PropertyLoader.getFolderName(loc);
        
    }

	/**
	 * THis method is responsible for forward to corresponding page.
	 * @param request
	 * @param response
	 * @param screenForm
	 * @throws ServletException
	 * @throws IOException
	 */
	private void forwardScreen(HttpServletRequest request,
			HttpServletResponse response, String screenForm)
			throws ServletException, IOException {
		// call write method to display content in the JSP by reflection
		Object o = null;
		try {
			try{
				o = Class.forName(programPath + "." + screenForm + "screen")
					.newInstance();
				//Class.forName(programPath.replace("screens", "programs") + "." + screenForm.replace("S", "P")).newInstance();
			}catch(ClassNotFoundException e){
				o = Class.forName("com.csc.fsu.general.screens." + screenForm + "screen")
				.newInstance();
			}
			Method m = o.getClass().getDeclaredMethod("write",new Class[] { COBOLAppVars.class, VarModel.class,
							Indicator.class, Indicator.class });
			Object[] agrs = new Object[] { appVars, smartVarModel,
					Indicator.getInstance(true), Indicator.getInstance(true) };
			m.invoke(o, agrs);
		} catch (Exception e) {
		    LOGGER.error("", e);
		}
		//init SXXXscreenctl
		try {
			try{
				o = Class.forName(programPath + "." + screenForm + "screenctl").newInstance();
			}catch(ClassNotFoundException e){
				o = Class.forName("com.csc.fsu.general.screens." + screenForm + "screenctl")
				.newInstance();
			}
			Method m = o.getClass().getDeclaredMethod("write",new Class[] { COBOLAppVars.class, VarModel.class,
							Indicator.class, Indicator.class });
			Object[] agrs = new Object[] { appVars, smartVarModel,
					Indicator.getInstance(true), Indicator.getInstance(true) };
			m.invoke(o, agrs);
		} catch (Exception e) {
			//e.printStackTrace();
		}
		//init SXXXscreensfl
		try {
			try{
			o = Class.forName(programPath + "." + screenForm + "screensfl").newInstance();
			}catch(ClassNotFoundException e){
				o = Class.forName("com.csc.fsu.general.screens." + screenForm + "screensfl")
				.newInstance();
			}
			Method m = o.getClass().getDeclaredMethod("write",new Class[] { COBOLAppVars.class, VarModel.class,
							Indicator.class, Indicator.class });
			Object[] agrs = new Object[] { appVars, smartVarModel,
					Indicator.getInstance(true), Indicator.getInstance(true) };
			m.invoke(o, agrs);
		} catch (Exception e) {
			//e.printStackTrace();
		}
		request.getSession().setAttribute(BaseModel.SESSION_VARIABLE, bm);
		WebUtil.openPage(getServletConfig().getServletContext()
				.getRequestDispatcher("/POLAMODEL.jsp"), request, response);
	}

	/**
	 * This method is responsible for getting screen model.
	 * @param screenForm
	 */
	private void getScreenModel(String screenForm) {
		ScreenModel screenModel = null;
		if (screenModel == null) {
			screenModel = new ScreenModel(screenForm, appVars, smartVarModel);
		}
		bm.setScreenModel(screenModel);
	}

	/**
	 * This method is responsible for initializing smart model.
	 * @param screenForm
	 * @return smartVarModel
	 */
	private SmartVarModel initSmartModel(HttpServletRequest request,String screenForm) {
		HashMap<String,DDScreenWindowing> sessionMap =  new HashMap<String,DDScreenWindowing>();
//		bm.setApplicationVariables(appVars);
		Object screenVars = null;
		programPath = GroupAsiaAppLocatorCode.findPath(screenForm);
		if (programPath == null) {
			throw new RuntimeException("Can't find program '"+screenForm+"' in any path.");
		}
		try {
			String classVarName = screenForm + "ScreenVars";
			programPath = programPath.replace("programs", "screens");
			LOGGER.debug("classVarNamePath=" + programPath);
			try{
				screenVars =  Class.forName(programPath + "." + classVarName).newInstance();
			}catch(ClassNotFoundException e){
				screenVars =  Class.forName("com.csc.fsu.general.screens." + classVarName).newInstance();
			}
			smartVarModel = (SmartVarModel) screenVars; 
			// set value for every field by reflection
			Field []field = screenVars.getClass().getDeclaredFields();
			for(int i=0;i<field.length;i++){
				String simpleTypeName = field[i].getType().getSimpleName();
				//System.out.println(field[i].getName()+"\t"+ field[i].getType().getSimpleName());
		
				String fieldName = field[i].getName();

					//Object cntbranch = ((DataDictionaryObject)((BaseData)field[i].get(screenVars)).getRefobject());
					
					
				if(fieldName.equalsIgnoreCase("isPageDown")){
					int length = field[i].get(screenVars).toString().length();
					String data = "2";
					field[i].set(screenVars, new FixedLengthStringData(length,data));
				} /*else if(fieldName.endsWith("Disp")){
					int length = field[i].get(screenVars).toString().length();
					String data = "11/11/2011";
					field[i].set(screenVars, new FixedLengthStringData(length,data));
				}*/
				else if(simpleTypeName.equals("FixedLengthStringData")){
					
					DDScreenWindowing dds = getScreenWindowing((BaseData)field[i].get(screenVars),fieldName);
					
					if(dds!=null){
						sessionMap.put(fieldName, dds);
						}
					
					int length = field[i].get(screenVars).toString().length();
					String data = "XXXXXXXXXXXXXXXXXXXX";
					field[i].set(screenVars, new FixedLengthStringData(length,data));
				}else if(simpleTypeName.equals("ZonedDecimalData")){
					int length = field[i].get(screenVars).toString().length();
					String data = "";
					for(int j=0;j<length-2;j++){
						data+="9";
					}
					double d = Double.parseDouble(data+".99");
					field[i].set(screenVars, new ZonedDecimalData(length,2,d));
				}else if(simpleTypeName.equalsIgnoreCase("Map")||simpleTypeName.equalsIgnoreCase("HashMap")){
					field[i].set(screenVars, new HashMap());
				}else if(simpleTypeName.equalsIgnoreCase("List")||simpleTypeName.equalsIgnoreCase("ArrayList")){
					field[i].set(screenVars, new ArrayList());
				}
			}
			request.getSession().setAttribute("sessionMap", sessionMap);
			return smartVarModel;
		} catch (Exception e) {
		    LOGGER.error("", e);
			return smartVarModel;
		} 
	}
	public  DDScreenWindowing getScreenWindowing(BaseData field, String screenName) {
		Object refObj = field.getRefobject();

		if (refObj != null && refObj instanceof DataDictionaryObject) {
			DataDictionaryObject ddObj = (DataDictionaryObject)refObj;
			DDFieldWindowing fW = ddObj.ddFieldWind;
			if (fW == null)
				return null;

			if (fW.mapScrWindow == null) {
				return null;
			}

			/* To fix bug 1163 by Wayne Yang 2010-04-12
			 * In current DD, screen name in all DDScreenWindowing is uppercase.
			 * But in ScreenModel, some of the screen name is lowercase(e.g. Sp035).
			 * Therefore it will cause the code can't get the right DDScreenWindowing,
			 * thus causing the bug 1163(validation error)
			 */
			DDScreenWindowing sW = fW.mapScrWindow.get(screenName.toUpperCase());
			if (sW == null)
				sW = fW.mapScrWindow.get(null);
   	                    
			return sW;
			}
		return null;
	}
	/**
	 * This method is responsible for generating random letters.
	 * @return
	 */
	private String getRandomLetter() {
		StringBuffer sb = new StringBuffer("");
		for(int i=0;i<300;i++){
			int in =( int)(Math.random()*123);
			if(in>=97 && in<=122)
			sb.append((char)in);
		}
		return sb.toString();
	}

	/**
	 * This method is responsible for getting PolisyAppVars.
	 * 1. initilize the QuipozCfg.xml
	 * @param language
	 * @param screenForm
	 * @return appVars
	 */
	private GroupAsiaAppVarsSon getPolisyAppVarsInstance(HttpServletRequest request,String language, String screenForm) {
//		change POLAWeb to PolisyAsiaWeb by shawn for PJE3.0
		//@091124
		String prop = "Quipoz.CSCGroupAsiaWeb.XMLPath";
		String path = System.getProperty(prop);
		if(language.equals("en")){
			language = "eng";
		}else if(language.equals("zh")){
			language = "chi";
		}
		LOGGER.debug("quipozConfigFile="+path);
		System.setProperty(prop,path);
		// define master menu and system menu
		HashMap<String, String> map = new LinkedHashMap<String,String>();
		ArrayList<String> a = new ArrayList<String>();
		// a.add("UI Test System menu");
		// map.put("UITest","UITest");
//		change POLAWeb to PolisyAsiaWeb by shawn for PJE3.0
		//@091124
		// TODO: always create new instance of GroupAsiaAppVarsSon ?????????
		appVars = new GroupAsiaAppVarsSon("CSCGroupAsiaWeb");
		GroupAsiaAppVarsSon.setInstance(appVars);
		appVars.setRequest(request);
		// set next JSP screen.
		appVars.setNextSingleModelJSPScreen(screenForm);
		// set user language
		appVars.setUserLanguage(language);
		appVars.SetMenuDisplayed(false);
		appVars.setMasterMenu(map);
		appVars.setSystemMenu(a.toArray());
		return appVars;
	}

	/**
	 * This method is responsible for generating input screen. User input screen name and language and 
	 * submit to servlet.
	 * @param pw
	 */
	private void dispalyQueryPage(PrintWriter pw,HttpServletRequest request) {
		StringBuffer sb = new StringBuffer("");
		sb.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\r\n");
		sb.append("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\r\n");
		sb.append("<LINK REL=\"StyleSheet\" HREF=\"theme/eng/QAStyle.jsp\" TYPE=\"text/css\">");
		sb.append("<LINK REL=\"StyleSheet\" HREF=\"theme/eng/QAStyle.css\" TYPE=\"text/css\">");
		sb.append("<LINK REL=\"StyleSheet\" HREF=\""+ request.getContextPath() +"/theme/layout_1.css\" TYPE=\"text/css\">");
		sb.append("<TITLE>Group Asia 1.0</TITLE>");
		sb.append("</head><body><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
				"<font size=4><B>Screen Display</B></font></br>");
		sb.append("<form name=\"form\" method=\"POST\"><br>");
		sb.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
				"<b>Screen Name:</b><input id=\"jspno\" name=\"jspno\" type=\"text\" size=\"5\">");
		
		List languageList = (List)request.getSession().getAttribute("language");
		List companyList = (List)request.getSession().getAttribute("company");
		List branchList = (List)request.getSession().getAttribute("branch");
		
		sb.append("<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
				"<b>Language:&nbsp;&nbsp;&nbsp;<b><select name=\"language\">") ;
				int size = languageList.size();
				for(int i=0;i<size;i++){
					String[] lan = languageList.get(i).toString().split("-");
					sb.append("<option value=\""+lan[0]+"\">"+lan[1]+"</option>" );
				}
				sb.append("</select>" );
		
		sb.append("<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
				"<b>Company:&nbsp;&nbsp;<b><select name=\"company\">" );
		int size1 = companyList.size();
		for(int i=0;i<size1;i++){
			String[] lan = companyList.get(i).toString().split("-");
			sb.append("<option value=\""+lan[0]+"\">"+lan[1]+"</option>" );
		}
		sb.append("</select>" );
		
		
		sb.append("<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
		"<b>Branch:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><select name=\"branch\">" +
		"<option value=\"**\">ALL BRANCHES</option>" +
		"<option value=\"10\">Shenton Way Main Branch</option>" +
		"<option value=\"20\">Jurong Branch</option>" +
		"<option value=\"SG\">Singapore Branch</option>" +
		"</select>");
		
		
		
		
		sb.append("<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
				"<input type=\"button\" name=\"Submit\" value=\"Submit\" width=\"30\" onClick=\"f()\">");
		sb.append("</body></html>");
		sb.append("<script language='javascript'>");
		sb.append("function f(){form.action =\"UIServlet?type=2\"; form.submit(); }");
		sb.append("</script>");
		pw.write(sb.toString());
	}
	
	/**
	 * This method is responsible for redirect the SXXXXForm.jsp directly.
	 * @param screenForm
	 * @param language
	 * @return
	 */
	@SuppressWarnings("unused")
	private String generateJSPPage(String screenForm, String language) {
		String path = GroupAsiaAppLocatorCode.findPath(screenForm);
		String array[] = path.split("[.]");
		String pathTemp = "";
		for (int i = 2; i <= array.length - 2; i++) {
			pathTemp += File.separator + path.split("[.]")[i];
		}
		LOGGER.debug("language=" + language);
		if (language.equals("en")) {
			path = pathTemp + File.separator + "eng" + File.separator;
		} else if (language.equals("zh")) {
			path = pathTemp + File.separator + "chi" + File.separator;
		}
		return path + screenForm + "Form.jsp";
	}
	
	
	
	private void generateCompanyBranchArray(HttpServletRequest request) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		PreparedStatement stmt2 = null;
		ResultSet rs2 = null;
		List companyList = new ArrayList();
		List branchList = new ArrayList();
		List languageList = new ArrayList();
		Connection conn = null;
		try {
			conn = AppVars.getInstance().getTempDBConnection("in P0017");
			
			StringBuilder languageSQL=new StringBuilder();
			//Add item validation by shawn to fix bug 1188
			languageSQL.append("SELECT D.DESCITEM, D.LONGDESC FROM VM1DTA.DESCPF D, VM1DTA.ITEMPF I WHERE" +
					" D.DESCITEM=I.ITEMITEM AND I.VALIDFLAG='1'" +
					" AND D.DESCTABL = I.ITEMTABL" +
					" AND D.DESCCOY = I.ITEMCOY" );
			languageSQL.append(" AND D.DESCCOY ='0' AND D.\"LANGUAGE\" =  'E");
			//languageSQL.append(wsspcomn.language);
			languageSQL.append("' AND D.DESCPFX='IT' AND D.DESCTABL='T1680'");
			
//			
//			languageSQL.append("select DESCITEM, LONGDESC from DESCPF where DESCTABL='T1680' and DESCpfx='IT' and DESCcoy='0' AND LANGUAGE='");
//			languageSQL.append(wsspcomn.language+"'");
//			
			// generate language list
			stmt = conn.prepareStatement(languageSQL.toString());
			rs = stmt.executeQuery();
			while (rs.next()){
				languageList.add(rs.getString(1).trim()+"-"+rs.getString(2));
			}
			rs.close();
			stmt.close();
			//Modified by Shawn to retrieve Company description
			StringBuilder companySQL=new StringBuilder();
			//Add item validation
			companySQL.append("SELECT D.DESCITEM, D.LONGDESC FROM VM1DTA.DESCPF D, VM1DTA.ITEMPF I WHERE" +
					" D.DESCITEM=I.ITEMITEM AND I.VALIDFLAG='1'" +
					" AND D.DESCTABL = I.ITEMTABL" +
					" AND D.DESCCOY = I.ITEMCOY" );
			companySQL.append(" AND D.DESCCOY ='0' AND D.\"LANGUAGE\" =  'E");
			//companySQL.append(wsspcomn.language);
			companySQL.append("' AND D.DESCPFX='IT' AND D.DESCTABL='T1693'");
//			companySQL.append("select DESCITEM, LONGDESC from DESCPF where DESCTABL='T1693' and DESCpfx='IT' and DESCcoy='0' and language='");
//			companySQL.append(wsspcomn.language+"'");
			// generate company list
			stmt = conn.prepareStatement(companySQL.toString());
			rs = stmt.executeQuery();

			//Modified by Shawn to retrieve branch description
			StringBuilder branchSQL=new StringBuilder();
			//Add item validation.
			branchSQL.append("SELECT D.DESCITEM, D.LONGDESC FROM VM1DTA.DESCPF D, VM1DTA.ITEMPF I WHERE" +
					" D.DESCITEM=I.ITEMITEM AND I.VALIDFLAG='1'" +
					" AND D.DESCTABL = I.ITEMTABL" +
					" AND D.DESCCOY = I.ITEMCOY" );
			branchSQL.append(" AND D.DESCCOY =? AND D.\"LANGUAGE\" =  'E");
			//branchSQL.append(wsspcomn.language);
			branchSQL.append("' AND D.DESCPFX='IT' AND D.DESCTABL='T1692'");
//			
//			branchSQL.append("select DESCITEM, LONGDESC from DESCPF where DESCtabl='T1692' and DESCpfx='IT' and DESCCOY=?  AND LANGUAGE='");
//			branchSQL.append(wsspcomn.language+"'");
			stmt2 = conn
					.prepareStatement(branchSQL.toString());
			
			while (rs.next()) {
				String tcompany = rs.getString(1).trim();
				String tCompanyDescr=rs.getString(2).trim();
				companyList.add(tcompany+"-"+tCompanyDescr);
				stmt2.setString(1, tcompany);
				rs2 = stmt2.executeQuery();
				
				/* to generate branch list*/
				String branch = "";
				boolean hasBranch = false;
				while (rs2.next()) {
					hasBranch = true;
					String tbranch = rs2.getString(1).trim();
					String tbranchDesc=rs2.getString(2).trim();
					
					branch=branch+tbranch.trim()+"-"+tbranchDesc+",";
				}
				if (hasBranch){
					branchList.add(branch.substring(0,branch.length()-1));
				}else{
					branchList.add("");
				}
				rs2.close();
			}
			SMARTAppVars smartAppVars = (SMARTAppVars) appVars;
			if (companyList.size() < 1){
				throw new RuntimeException("no company exists.");
			}else{
				String[][] arr= new String[companyList.size()][2];
				for (int i=0;i<arr.length;i++){
					arr[i][0] = (String) companyList.get(i);
					arr[i][1] = (String) branchList.get(i);
				}
				smartAppVars.setCompanyBranchArray(arr);
			}
			request.getSession().setAttribute("language", languageList);
			request.getSession().setAttribute("company", companyList);
			request.getSession().setAttribute("branch", branchList);
			
		} catch (Exception e) {
			// e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					
				}
			if (rs2 != null)
				try {
					rs2.close();
				} catch (SQLException e) {
					
				}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					
				}
			}
			if (stmt2 != null) {
				try {
					stmt2.close();
				} catch (SQLException e) {
					
				}
			}
			try {
				AppVars.getInstance().freeDBConnection(conn);
			} catch (Exception e) {
				
			}
		}
	}
}
