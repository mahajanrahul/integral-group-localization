package com.csc.groupasia.web;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import com.quipoz.framework.util.AppVars;

public class SessionAppVarsStore {
	 private static final ConcurrentMap<String, AppVars> sessionAppVars = new ConcurrentHashMap<String, AppVars>();

	    public static void set(String sessionId, AppVars av) {
	        sessionAppVars.put(sessionId, av);
	    }

	    public static AppVars get(String sessionId) {
	        return sessionAppVars.get(sessionId);
	    }

	    public static void remove(String sessionId) {
	        sessionAppVars.remove(sessionId);
	    }
}
