package com.csc.groupasia.web;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The servlet is responsible for testing on-line print.
 */
public class TestServlet extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestServlet.class);
    
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getParameter("type").equals("1")) {
			directoryExistOrNot(response);
		} else if (request.getParameter("type").equals("2")) {
			readXSLT(response);
		} else if (request.getParameter("type").equals("3")) {
			writeXML(response);
		}
	}

	private void writeXML(HttpServletResponse response) throws IOException {
		String file = "D:\\SharedData\\FT\\XMLPrint\\ENGLISH\\PRINTXML\\VM1\\MAY2007\\test.XML";
		StringBuffer sb = new StringBuffer("3. Final Write Test.XML to \"" + file
				+ "\".\r\n");
		response.getWriter().println(sb.toString());
		File f = new File(file);
		if(f.isFile()){
			response.getWriter().println("'"+file+"' is a File and Exist.\r\n");
		}else{
			response.getWriter().println("File '"+file+"' is not Exist.Begin to ceate it.\r\n");
			if (!f.createNewFile()) {
				response.getWriter().println("Create New File \"" + file + " \" Fail.\r\n");
				throw new IOException("Create New File \"" + file + " \" Fail.");
			} else {
				response.getWriter().println("Create New File \"" + file + " \" Success.\r\n");
				BufferedWriter writer = new BufferedWriter(new FileWriter(f));
				StringBuffer sb1 = new StringBuffer("");
				sb1.append("<?xml version=\"1.0\" encoding=\"GB2312\"?>\r\n");
				sb1.append("<PRIPAY>\r\n");
				sb1.append("<LETTERCTL>RIPAY</LETTERCTL>\r\n");
				sb1.append("<LOCPATH>/PRINTOUTPUT/VM1/MAY2007/</LOCPATH>\r\n");
				sb1.append("<DOCIDNUM>00000532</DOCIDNUM>\r\n");
				sb1.append("<CLNTNUM>50000005</CLNTNUM>\r\n");
				sb1.append("</PRIPAY>\r\n");
				writer.write(sb1.toString());
				writer.flush();
				writer.close();
			}
		}
	}

	private void readXSLT(HttpServletResponse response) {
		String file = "D:\\SharedData\\FT\\XMLPrint\\ENGLISH\\GSPM01\\VM1DIR\\VM1\\RNLNOT.xsl";
		StringBuffer sb = new StringBuffer("2. Final Read XSL from \"" + file
				+ "\" and output in the page.\r\n");
		try {
			response.getWriter().println(sb.toString());
		} catch (Exception e1) {
		    LOGGER.error("", e1);
		}
		sb = new StringBuffer("");
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(new File(file)));
			String content = br.readLine();
			while (content != null) {
				sb.append(content + "\r\n");
				content = br.readLine();
			}
			response.getWriter().println(sb.toString());
		} catch (FileNotFoundException e) {
		    LOGGER.error("", e);
		} catch (IOException e) {
		    LOGGER.error("", e);
		}catch (Exception e) {
		    LOGGER.error("", e);
		}
		
	}

	private void directoryExistOrNot(HttpServletResponse response)
			throws IOException {
		String xmlDirectory1 = "D:\\SharedData";
		StringBuffer sb1 = new StringBuffer("1-A. Final Root Mapping Directory(" + xmlDirectory1
				+ ") Exist or Not.\r\n");
		response.getWriter().println(sb1.toString());
		File fileDirectory1 = new File(xmlDirectory1);
		if (fileDirectory1.isDirectory()) {
			response.getWriter().println(
					"XML Directory:\"" + xmlDirectory1 + "\" is Directory and exists.\r\n");
		} else {
			response.getWriter().println(
					"XML Directory:\"" + xmlDirectory1
							+ "\" is not directory.\r\n");
		}
		
		String xmlDirectory = "D:\\SharedData\\FT\\XMLPrint\\ENGLISH\\PRINTXML\\VM1\\MAY2007";
		StringBuffer sb = new StringBuffer("1-B. Final Directory(" + xmlDirectory
				+ ") Exist or Not.\r\n");
		response.getWriter().println(sb.toString());
		File fileDirectory = new File(xmlDirectory);
		if (fileDirectory.isDirectory()) {
			response.getWriter().println(
					"XML Directory:\"" + xmlDirectory + "\" is Directory and exists.\r\n");
		} else {
			response.getWriter().println(
					"XML Directory:\"" + xmlDirectory
							+ "\" is not directory.\r\n");
		}

		xmlDirectory = "D:\\SharedData\\FT\\XMLPrint\\ENGLISH\\PRINTXML\\VM1\\MAY2008";
		sb = new StringBuffer("1-C. Final Directory(" + xmlDirectory
				+ ") Exist or Not, If it doesn't exits, create directory.\r\n");
		response.getWriter().println(sb.toString());
		fileDirectory = new File(xmlDirectory);
		if (fileDirectory.isDirectory()) {
			response.getWriter().println(
					"\"" + xmlDirectory + "\" is Directory and exists.\r\n");
		} else {
			if (fileDirectory.isFile()) {
				response.getWriter().println(
						"XML Directory:\"" + xmlDirectory + "\" is File.\r\n");
			} else {
				if (!fileDirectory.mkdirs()) {
					response.getWriter().println(
							"XML Directory:" + xmlDirectory
									+ " is not exists, create fail.\r\n");
				} else {
					response.getWriter().println(
							"XML Directory:" + xmlDirectory
									+ " is not exists, create success.\r\n");
				}
			}
		}
	}

}
