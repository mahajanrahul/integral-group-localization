package com.csc.groupasia.web;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quipoz.framework.sessionfacade.GeneralSession;
import com.quipoz.framework.sessionfacade.GeneralSessionDelegate;
import com.quipoz.framework.sessionfacade.GeneralSessionFacade;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.BaseModel;
import com.quipoz.framework.util.ThreadLocalStore;

/**
 * The servlet get the inputed userID and password. Authenticate user
 * validity using LDAP. Go to master menu if validited.<br>
 * @author csc
 * 2007-9-26
 */
public class LogoutServlet extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(LogoutServlet.class);
    
	private final long	serialVersionUID	= 1L;

	/**
	 * URL for redirect after logout.
	 */
	private String redirectToUrl;

	/**
	 * CAS server logout URL.
	 */
	private String casServerLogoutUrl;

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occure
	 */
	public void destroy() {
		super.destroy();
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occure
	 */
	public void init(ServletConfig config) throws ServletException {
		// must be set
	    casServerLogoutUrl = config.getInitParameter("casServerLogoutUrl");
	    if (casServerLogoutUrl == null)
	      throw new ServletException("Init parameter casServerLogoutUrl is not set!");

	    // optinal, can be null. If not set CAS will not redirect user after logout.
	    redirectToUrl = config.getInitParameter("redirectToUrl");
	    if (redirectToUrl != null) {
	      try {
	        redirectToUrl = URLEncoder.encode(redirectToUrl, "UTF-8");
	      } catch (UnsupportedEncodingException e) {
	        LOGGER.error("", e);
	        throw new ServletException(e.getMessage());
	      }
	    }
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
		    LOGGER.debug("KillSession is having a go V2 ...");
			GeneralSessionDelegate gd = (GeneralSessionDelegate) session.getAttribute(GeneralSessionDelegate.SESSION_VARIABLE);
			LOGGER.info("KillSession got the GD ...");
			if (gd != null) {
				BaseModel bm = (BaseModel) session.getAttribute(BaseModel.SESSION_VARIABLE);
				if (bm != null) {
				    LOGGER.info("KillSession is cleaning up ...");
					GeneralSession impl = gd.getImpl();
					if (AppVars.getInstance().getAppConfig().getServer().equalsIgnoreCase("EJB")) {
					    LOGGER.info("KillSession is removing a Session Bean ...");
						((GeneralSessionFacade)impl).remove();
					}
					else {
					    LOGGER.info("KillSession is removing a Local Bean ...");
						gd.cleanup(bm);
					}
					LOGGER.info("KillSession has cleaned up !");
				}
				else {
				    LOGGER.info("KillSession got the GD, but the BaseModel is null!");
				}
			}
			else {
			    LOGGER.info("Error cleaning up the EJB session, gd is null");
			}
		}
		catch (Exception e) {
			LOGGER.error("Error while cleaning up the EJB session", e);
		}
		ThreadLocalStore.clear();
		session.invalidate();
		try {
//			getServletContext().getRequestDispatcher("/logon.jsp").forward(request, response);
//			response.sendRedirect(request.getContextPath());
			String url = casServerLogoutUrl;
		    if (redirectToUrl != null && redirectToUrl.length() > 0)
		      url += "?service=" + redirectToUrl;

		    response.sendRedirect(url);
		}
		catch (IOException e) {
		    LOGGER.error("", e);
		}
//		catch (ServletException e) {
//			e.printStackTrace();
//		}
	}
}
