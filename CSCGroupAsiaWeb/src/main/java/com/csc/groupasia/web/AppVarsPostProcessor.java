package com.csc.groupasia.web;

import com.csc.groupasia.runtime.variables.GroupAsiaAppVars;

public interface AppVarsPostProcessor {
	
	public void process(GroupAsiaAppVars av);

}
