package com.csc.groupasia.web;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jasig.cas.client.authentication.AttributePrincipal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.ldap.Config;
import com.csc.groupasia.runtime.variables.GroupAsiaAppVars;
import com.csc.ldap.Ldap;
import com.quipoz.framework.dbdialect.DbDialectSupportObjFactory;
import com.quipoz.framework.dbdialect.FirstServletSupport;
import com.quipoz.framework.util.AppConfig;
import com.quipoz.framework.util.AppVars;

/**
 * The first servlet users access. To show the different logon pages according
 * to the language configuration in config file.
 *
 * @author Wayne Yang 2010-04-28
 */
public class FirstServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * Logger for this class
     */
	private static final Logger LOGGER = LoggerFactory.getLogger(FirstServlet.class);

    private static final FirstServletSupport firstServletSupport = DbDialectSupportObjFactory
            .getInstance().getFirstServletSupport();

	public void destroy() {
		super.destroy();
	}

	public void init() throws ServletException {
		// ApplicationName = getInitParameter("Appname");
		// if (ApplicationName == null) {
		// String s = "Parameter 'Appname' is not set. \n";
		// s = s + "This needs to be set in web.xml. \n";
		// s = s
		// +
		// "This must then point to a System Property 'Quipoz.Appname.XMLPATH' \n"
		// ;
		// s = s
		// +
		// "This must contain the full path and file name of the System Configuration File. \n"
		// ;
		// s = s + "The file name must end in '.XML' (not case sensitive) \n";
		// s = s
		// +
		// "The file name can be omitted from the path, in which case 'QuipozCfg.XML' will be assumed.\n"
		// ;
		// throw new RuntimeException(s);
		// }
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// Find the config file
		String prop = "Quipoz.CSCGroupAsiaWeb.XMLPath";
		String path = System.getProperty(prop);
		if (path == null) {
			String s = "Quipoz.CSCGroupAsiaWeb.XMLPath is not set. \n";
			throw new RuntimeException(s);
		}

		// By pass log on if the user has already been authenticated by CAS server.
		if (request.getUserPrincipal() instanceof AttributePrincipal) {

			// This code is a copy of the {@link LoginServlet#doPost(HttpServletRequest, HttpServletResponse)}
			// without the LDAP authentication.
			AttributePrincipal principal = (AttributePrincipal) request
					.getUserPrincipal();
            LOGGER.info("======================================================");
            LOGGER.info("The incomming request has been already authenticated in CAS ...");
            LOGGER.info("Principal's name is: " + principal.getName());
            LOGGER.info("Attributes of the principal are: " + principal.getAttributes());

			String userId = principal.getName();
			userId = userId.trim().toUpperCase();
			String lastLoginTime = "never";
			String userDescription = "";

			boolean isAdmin = false;

			GroupAsiaAppVars appVars = new GroupAsiaAppVars(getInitParameter("Appname"));
			appVars.setContextPath(request.getContextPath().replaceFirst("/", ""));
			AppVars.setInstance(appVars);


			Ldap ldap = new Ldap();
			lastLoginTime = updateUserInfo(userId);
			//userDescription = ldap.getUserDescription(userId + " ext").toString();
			userDescription = ldap.getUserDescription(userId).toString();

			// Cache User description for next use
			appVars.setUserDescription(userDescription);

			// create user http session
			HttpSession session = request.getSession();
			session.setAttribute("USERID", userId);
			session.setAttribute("userD", userDescription);
			session.setAttribute("LastLoginTime", lastLoginTime);

			// Judge whether this is System admin
			if (isAdmin) {
				session.setAttribute("ISSYSTEMADMIN", true);
			} else {
				session.setAttribute("ISSYSTEMADMIN", false);

			}
			// initialize AppVar
			try {
				getServletContext()
						.getRequestDispatcher(
								"/process?action_key=screenInit&screen=default&previousMenu=index.jsp")
						.forward(request, response);
				return;
			} catch (IOException e) {
                LOGGER.error("doGet(HttpServletRequest, HttpServletResponse)", e);
			} catch (ServletException e) {
                LOGGER.error("doGet(HttpServletRequest, HttpServletResponse)", e);
			}
		}

		String url = "logon.jsp";
		try {
			String language = AppConfig.getInstance().getCurrentLanguage();
			if ("S".equalsIgnoreCase(language)) {
				url = "logon_cn.jsp";
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		// decide which page to show
		try {
			getServletContext().getRequestDispatcher(url).forward(request,
					response);
		} catch (IOException e) {
			throw new RuntimeException(e);
		} catch (ServletException e) {
			throw new RuntimeException(e);
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doGet(request, response);
	}


private String updateUserInfo(String userId) {

		Config config=new Config();
		Timestamp dt = new Timestamp(System.currentTimeMillis());
		String result = "Can't Retrieve Date!";

		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.applyPattern(config.getDateFormat() + " " + config.getTimeFormat());
		PreparedStatement stmt = null;
		ResultSet rs = null;
		PreparedStatement stmt2 = null;
		Connection conn = null;
		String strDriver = "";
		try {
			strDriver = AppVars.getInstance().getAppConfig().getDriverClassNameJDBC();
			Class.forName(strDriver).newInstance();
			conn = DriverManager.getConnection(config.getDataSourceJDBC(),
					config.getUseridJDBC(), config.getPasswordJDBC());

            String sqlString = firstServletSupport.getLastLogonTimeSql();

			// generate language list
            stmt = conn.prepareStatement(sqlString);
			stmt.setString(1, userId.trim());
			rs = stmt.executeQuery();
			if (rs.next())
				result = sdf.format(rs.getTimestamp(1));
			else
				result = "never";
			stmt.close();
            stmt = conn.prepareStatement(" INSERT INTO USRINF(USERID,LASTLOGON_TIME) VALUES(?,?) ");
			stmt.setString(1, userId);
			stmt.setTimestamp(2, dt);
			stmt.executeUpdate();
		} catch (SQLException e) {
            LOGGER.error("updateUserInfo(String)", e);
		} catch (Exception e) {
            LOGGER.error("updateUserInfo(String)", e);
		} finally {
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
                    LOGGER.error("updateUserInfo(String)", e);
				}
			}
			if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    LOGGER.error("updateUserInfo(String)", e);
                }
            }
			if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    LOGGER.error("updateUserInfo(String)", e);
				}
			}
		}

		return result;

	}
}
