package com.csc.groupasia.web;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.xml.parsers.FactoryConfigurationError;

import com.quipoz.framework.util.AppConfig;
import com.quipoz.framework.util.log.QPLogger;

/**
 * Class to initialize AppConfig on start up
 * 
 * @author htran50
 * 
 */
public class AppConfigListener implements ServletContextListener {

    @Override
    public void contextDestroyed(ServletContextEvent contextEvent) {

    }

    @Override
    public void contextInitialized(ServletContextEvent contextEvent) {
        configLogging();

        ServletContext context = contextEvent.getServletContext();
        String appName = context.getInitParameter("Appname");

        // Load AppCongig
        AppConfig.getInstance(appName);
    }

    private void configLogging() throws FactoryConfigurationError {
        try {
            // Call QPLogger which in turn checks and initializes log4j
            QPLogger.getQPLogger(this.getClass());
        } catch (Exception e) {
            throw new RuntimeException("Unable to load log4jConfig.xml");
        }
    }

}
