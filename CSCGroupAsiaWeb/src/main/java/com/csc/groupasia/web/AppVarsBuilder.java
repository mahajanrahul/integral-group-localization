package com.csc.groupasia.web;

import com.csc.groupasia.runtime.variables.GroupAsiaAppVars;

public final class AppVarsBuilder {
	
	private AppVarsBuilder() {
		super();
	}

	/**
	 * A convenient method to create new instance of GroupAsiaAppVars, with possibility of post processing
	 * 
	 * @param appName
	 * @param postProcessor
	 * @return
	 */
	public static GroupAsiaAppVars getGroupAsiaAppVars(String appName, AppVarsPostProcessor postProcessor) {
		GroupAsiaAppVars av = new GroupAsiaAppVars(appName);
		
		if (postProcessor != null) {
			postProcessor.process(av);
		}
		
		return av;
	}
}
