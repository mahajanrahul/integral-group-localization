package com.csc.groupasia.web;
import java.util.ArrayList;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quipoz.framework.sessionfacade.GeneralSessionDelegate;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.BaseModel;
import com.quipoz.framework.util.QPUtilities;

/**
 * Class to clean up resources on a session timeout. This is important since
 * we're using stateful session beans that don't timeout for an hour. In
 * here if we lose a session we'll get the session bean out and call remove
 * on it.
 */
public class SessionCleaner1 implements HttpSessionListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(SessionCleaner1.class);
    
	public static final String ROUTINE = QPUtilities.getThisClass();

	// Class variables to keep track of the number of active sessions
	//
	private static int no_sessions_created = 0;
	private static int no_sessions_destroyed = 0;
	private static int no_sessions_active = 0;
	private static ArrayList knownSessions = new ArrayList(100);

	public void sessionCreated(HttpSessionEvent sessionEvent) {
		no_sessions_created++;
		no_sessions_active++;
		sessionEvent.getSession().setAttribute("LoginFilterAction", "home");
		LOGGER.debug("HTTP Session Listener has detected the creation of a new session.");
		LOGGER.debug("There are now " + no_sessions_active + " active HTTP sessions, knownactive=" + knownSessions.size());
		/* Add this session to the list in a synchronised way */
		addSession(sessionEvent.getSession());
	}
	
	/** 
	 * Walk through all known sessions, to find any that should be
	 * done away with because they have timed out.
	 */
	public static void lookForExpiredSessions() {
		HttpSession sess = null;
		for (int i=0; i<knownSessions.size(); i++) {
			try {
				sess = (HttpSession) knownSessions.get(i);
//				BaseModel bm = (BaseModel) sess.getAttribute(BaseModel.SESSION_VARIABLE);
//				AppVars av = bm.getApplicationVariables();
				AppVars av = SessionAppVarsStore.get(sess.getId());
				long then = av.getHeartBeat();
				long now = System.currentTimeMillis();
				long msecs = now - then;
				LOGGER.debug("AppVars " + av.hashCode() + " Comparing time=" + msecs + " to max=" + (2 * av.getAppConfig().userHeartbeat));
				if (msecs >= 2 * av.getAppConfig().userHeartbeat) {
				    LOGGER.debug("Sessioncleaner found a session to kill");
					sess.invalidate();
					LOGGER.debug("And killed it.");
				}
			}
			catch (Exception e) {
				Integer checked = (Integer) sess.getAttribute("CheckCount");
				if (checked == null) {
					checked = new Integer(1);
				}
				else {
					checked = checked + 1;
				}
				sess.setAttribute("CheckCount", checked);
				if (checked > 3) {
				    LOGGER.error("Error accession session, removing it", e);
					deleteSession(sess);
				}
			}
		}

	}

	/**
	 * @param sessionEvent
	 */
	private synchronized void addSession(HttpSession session) {
		knownSessions.add(session);
	}

	/**
	 * @param sessionEvent
	 */
	private static synchronized void deleteSession(HttpSession session) {
		try {
			for (int i=0; i<knownSessions.size(); i++) {
				if (session == knownSessions.get(i)) {
					knownSessions.remove(i);
					return;
				}
			}
		}
		catch (Exception e) {
		    LOGGER.error("Error accession session", e);
		}
	}
	
	public void finalize() {
		try {
		    LOGGER.debug("Finalize on " + ROUTINE + " called.");
		}
		catch (Exception e) {
			/* Why this fails I dunno, but it causes constant "breaks" in
			 * debugging which is annoying. No problem in normal runs. */
			String s = e.toString();
		}
	}

	/**
	 * clean up any resources....
	 */
	public void sessionDestroyed(HttpSessionEvent sessionEvent) {

		no_sessions_destroyed++;
		no_sessions_active--;
		LOGGER.debug("HTTP Session Listener has detected the destruction of a session.");
		LOGGER.debug("There are now " + no_sessions_active + " active HTTP sessions.");

		AppVars.addStaticDiagnostic("HSessionCleaner cleaning session....." + sessionEvent.getSession().getMaxInactiveInterval());

		HttpSession session = sessionEvent.getSession();
		
		SessionAppVarsStore.remove(session.getId());
		
		/* Tell the bean on the other side of the EJB layer that its services
		 * are no longer required.
		 */
		try {
			GeneralSessionDelegate gd = (GeneralSessionDelegate) session.getAttribute(GeneralSessionDelegate.SESSION_VARIABLE);
			if (gd != null) {
				BaseModel bm = (BaseModel) session.getAttribute(BaseModel.SESSION_VARIABLE);
				if (bm != null) {
					gd.cleanup(bm);
				}
			}
			else {
			    LOGGER.debug("Error cleaning up the EJB session, gd is null");
			}
		}
		catch (Exception e) {
			LOGGER.error("Error while cleaning up the EJB session", e);
		}
		
		deleteSession(session);
	}

}
