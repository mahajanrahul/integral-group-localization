package com.csc.groupasia.web;

import java.util.HashMap;
import java.util.Map;

import com.csc.smart400framework.SmartHttpScreenController;
import com.quipoz.framework.screencontrol.HTTPScreenController;

public class ScreenToControllerMap {

	public static Map screenToObjectMap = new HashMap();

	/**
	 * Obtains a new screenController instance.
	 * 
	 * @param screenName String
	 * @return controller instance
	 * @exception java.lang.ClassNotFoundException
	 * @exception java.lang.InstantiationException
	 * @exception java.lang.Exception
	 * @exception java.lang.IllegalAccessException
	 */
	public static HTTPScreenController getScreenController(String screenName) throws ClassNotFoundException,
	        InstantiationException, Exception, IllegalAccessException {
		HTTPScreenController o = com.quipoz.framework.webcontrol.ScreenToControllerMap.getScreenController(screenName);

		if (o == null) {
			o = new SmartHttpScreenController(screenName);
		}

		return o;
	}
}
