package com.csc.groupasia.web;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.ldap.Config;
import com.csc.groupasia.runtime.variables.GroupAsiaAppVars;
import com.csc.ldap.Ldap;
import com.quipoz.framework.util.AppConfig;
import com.quipoz.framework.util.AppVars;

/**
 * The servlet get the inputed userID and password. Authenticate user validity
 * using LDAP. Go to master menu if validited.<br>
 * 
 * @author csc 2007-9-26
 */
public class LoginServlet extends HttpServlet {

	private static final Logger LOGGER = LoggerFactory.getLogger(LoginServlet.class);
	
	/**
	 * Initialization of the servlet. <br>
	 * 
	 * @throws ServletException
	 *             if an error occure
	 */
	public void destroy() {
		super.destroy();
	}

	/**
	 * Initialization of the servlet. <br>
	 * 
	 * @throws ServletException
	 *             if an error occure
	 */
	public void init() throws ServletException {
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * Judge Whether this user is System admin
	 * 
	 * @param Appvar
	 * @param UserId
	 * @param UserPassword
	 * @return Boolean
	 */
	private boolean isSystemAdmin(String userId, String password,
			AppVars appVars) {
		boolean isAdmin = false;
		String userid = null;
		String pwd = null;
		AppConfig appConfig = appVars.getAppConfig();
		userid = appConfig.getSystemAdminName();
		pwd = appConfig.getSystemAdminPwd();
		if (userid.equals(userId) && pwd.equals(password)) {
			isAdmin = true;
		}
		return isAdmin;
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String userId = request.getParameter("userid");
		userId = userId.trim().toUpperCase();
		String lastLoginTime = "never";
		String passWord = request.getParameter("password");
		passWord = passWord.trim();
		String userDescription="";
		boolean isAdmin=false;		
		AppVars appVars = new GroupAsiaAppVars("CSCGroupAsiaWeb");
		Ldap ldap = new Ldap();
		
		//ldap verifacation
		boolean isauthoritied = ldap.isAuthenticatedUser(userId, passWord,appVars);
		if (isauthoritied) {
			lastLoginTime = updateUserInfo(userId);
			//userDescription=ldap.getUserDescription(userId+" ext",appVars).toString();
			userDescription = ldap.getUserDescription(userId).toString();
		}
		
		//judge whether this is system Admin

		if (isauthoritied == false) {
			try {
				String message = "Sorry, your user name or password is wrong!";
				String isWrong = "1";
				request.setAttribute("message", message);
				request.setAttribute("isWrong", isWrong);
				getServletContext().getRequestDispatcher("/logon.jsp").forward(
						request, response);
			} catch (IOException e) {
			    LOGGER.error("", e);
			} catch (ServletException e) {
			    LOGGER.error("", e);
			}
		} else {
			// create user http session

			HttpSession session = request.getSession();

			session.setAttribute("USERID", userId);
			session.setAttribute("userD", userDescription);
			session.setAttribute("LastLoginTime", lastLoginTime);
//			Judge whether this is System admin
			if(isAdmin){
				session.setAttribute("ISSYSTEMADMIN", true);	
			}else{
				session.setAttribute("ISSYSTEMADMIN", false);	

			}
			// initialize AppVar
			try {
				getServletContext()
						.getRequestDispatcher(
								"process?action_key=screenInit&screen=default&previousMenu=index.jsp")
						.forward(request, response);
			} catch (IOException e) {
			    LOGGER.error("", e);
			} catch (ServletException e) {
			    LOGGER.error("", e);
			}
		}

	}
	/***************************************************************************
	 * Update user last login time
	 **************************************************************************/
	private String updateUserInfo(String userId) {
		
		Config config=new Config();
		Timestamp dt = new Timestamp(System.currentTimeMillis());
		String result = "Can't Retrieve Date!";

		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.applyPattern(AppConfig.getDateFormat());
		PreparedStatement stmt = null;
		ResultSet rs = null;
		PreparedStatement stmt2 = null;
		Connection conn = null;
		
		String strDriver = "";
		
		try {
			strDriver = AppVars.getInstance().getAppConfig().getDataDriverJDBC();
			Class.forName(strDriver).newInstance();
			conn = DriverManager.getConnection(
					config.getDataSourceJDBC(), config.getUseridJDBC(),
					config.getPasswordJDBC());

			// generate language list
			stmt = conn
					.prepareStatement("select LASTLOGON_TIME from USRINF where TRIM(USERID)=? order by LASTLOGON_TIME DESC");
			stmt.setString(1, userId.trim());
			rs = stmt.executeQuery();
			if (rs.next())
				result = sdf.format(rs.getTimestamp(1));
			else
				result = "never";
			stmt2 = conn
					.prepareStatement("insert into USRINF(USERID,LASTLOGON_TIME) values(?,?)");
			stmt2.setString(1, userId);
			stmt2.setTimestamp(2, dt);
			stmt2.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("SQLException:\n", e);
		} catch (Exception e) {
		    LOGGER.error("", e);
		} finally {
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					
				}
			}
		}

		return result;

	}
}
