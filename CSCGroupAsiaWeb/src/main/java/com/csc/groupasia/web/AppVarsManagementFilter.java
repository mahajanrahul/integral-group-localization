package com.csc.groupasia.web;
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.ThreadLocalStore;

public class AppVarsManagementFilter implements Filter {
	 @Override
	    public void destroy() {
	    }

	    @Override
	    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
	            ServletException {
	        HttpServletRequest httpReq = (HttpServletRequest) request;

	        try {
	            // Set AppVars from SessionAppVarsStore into thread local
	            HttpSession s = httpReq.getSession(false);
	            if (s != null) {
	                String sessionId = s.getId();
	                AppVars av = SessionAppVarsStore.get(sessionId);

	                if (av != null) {
	                    AppVars.setInstance(av);
	                }
	            }

	            chain.doFilter(request, response);
	        } finally {
	            // Set thread local's AppVars back into SessionAppVarsStore
	            HttpSession s = httpReq.getSession(false);
	            if (s != null) {
	                String sessionId = s.getId();
	                AppVars threadAv = AppVars.getInstance();
	                if (threadAv != null) {
	                    SessionAppVarsStore.set(sessionId, threadAv);
	                }
	            }

	            // Clear ThreadLocalStore
	            ThreadLocalStore.clear();
	        }
	    }

	    @Override
	    public void init(FilterConfig filterConfig) throws ServletException {
	    }
}
