package com.resource;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quipoz.framework.datatype.StringData;

public class ResourceBundleHandler {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ResourceBundleHandler.class);

	public static final String LOCALE_ENG = "eng";
	public static final String LOCALE_CHI = "chi";
	
	private static final String INTEGRAL_TITLE_COMMONS = "com.bundle.common.Integral_Title_Commons";
	private static final String INTEGRAL_TITLE_GROUP = "com.bundle.group.Integral_Title_Group";	
	
	private static final String INTEGRAL_COMMONS = "com.bundle.common.Integral_Commons";
	private static final String INTEGRAL_GROUP = "com.bundle.group.Integral_Group";	
		
	private ResourceBundle resourceBundleCommons = null;
	private ResourceBundle resourceBundleTitleCommons = null;
	
	private ResourceBundle resourceBundle = null;
	private ResourceBundle resourceBundleTitle = null;
	
	private String screenName = null;
	private String language = null;

	/**
	 * 
	 */
	public ResourceBundleHandler() {
	};

	/**
	 * 
	 * @param screenName
	 * @param language
	 */
	public ResourceBundleHandler(String screenName, String language) {
		this.screenName = screenName;
		this.language = language;
		
		resourceBundle = getResourceBundle(INTEGRAL_GROUP, new Locale(language));
		resourceBundleTitle = getResourceBundle(INTEGRAL_TITLE_GROUP, new Locale(language));
		
		resourceBundleCommons = getResourceBundle(INTEGRAL_COMMONS, new Locale(language));
		resourceBundleTitleCommons = getResourceBundle(INTEGRAL_TITLE_COMMONS, new Locale(language));
	}
	
	public ResourceBundleHandler(String screenName, Locale locale) {
		this.screenName = screenName;
		
		resourceBundle = getResourceBundle(INTEGRAL_GROUP, locale);
		resourceBundleTitle = getResourceBundle(INTEGRAL_TITLE_GROUP, locale);
		
		resourceBundleCommons = getResourceBundle(INTEGRAL_COMMONS, locale);
		resourceBundleTitleCommons = getResourceBundle(INTEGRAL_TITLE_COMMONS, locale);
	}

	public ResourceBundleHandler(Locale locale) {
		resourceBundle = getResourceBundle(INTEGRAL_GROUP, locale);
		resourceBundleTitle = getResourceBundle(INTEGRAL_TITLE_GROUP, locale);
		
		resourceBundleCommons = getResourceBundle(INTEGRAL_COMMONS, locale);
		resourceBundleTitleCommons = getResourceBundle(INTEGRAL_TITLE_COMMONS, locale);
	}
	
	private static ResourceBundle getResourceBundle(String bundleFile, Locale locale) {
		return ResourceBundle.getBundle(bundleFile, locale);
	}

	public String gettingValueFromBundle(String key) {
		String value = null;
		
		/*
		 * For Commons Screens (FSU, Smart & Diary)
		 */
		if (value == null){
			value = gettingValueFromBundleHelper(resourceBundleCommons, key);
		}
		
		/*
		 * For Group Screens
		 */
		if (value == null){
			value = gettingValueFromBundleHelper(resourceBundle, key);
		}
				
		/*
		 * If not found, the key will be used
		 */
		if (value == null){
			value = key;
		}
		
		return value;
	}
	
	
	private String gettingValueFromBundleHelper(ResourceBundle rb, String key) {
		try {
			return ((rb.getString(this.screenName + "." + key.trim().replaceAll(" ", "_"))).trim());
			
		} catch (MissingResourceException mre) {
			
			try {
				return ((rb.getString(key.trim().replaceAll(" ", "_"))).trim());
				
			} catch (MissingResourceException mre1) {
				
				return null;
			}
		}
	}
	
	
	/**
	 * @param clazz
	 * @param key
	 * @return the String wrapped in String data object to be used for in18
	 */
	 public StringData gettingValueFromBundle(Class clazz, String key) {
		 return new StringData(gettingValueFromBundle(key));
	}

	/**
	 * Added to get title labels from resource bundle.
	 * @param key
	 * @return
	 */
	public String gettingValueFromBundleforTitle(String key) {
		String value = null;
		
		/*
		 * For Commons Screens (FSU, Smart & Diary)
		 */
		if (value == null){
			value = gettingValueFromBundleforTitleHelper(resourceBundleTitleCommons, key);
		}
		
		/*
		 * For Group Screens
		 */
		if (value == null){
			value = gettingValueFromBundleforTitleHelper(resourceBundleTitle, key);
		}
		
		/*
		 * If not found, the key will be used
		 */
		if (value == null){
			value = key;
		}
		
		return value;
	}
	
	private String gettingValueFromBundleforTitleHelper(ResourceBundle rb, String key) {
		String value = "";
		try {
			return (rb.getString(this.screenName + "." + key.trim().replaceAll(" ", "_"))).trim();
			
		} catch (MissingResourceException mre) {
			try {
				return (rb.getString(key.trim().replaceAll(" ", "_"))).trim();
				
			} catch (MissingResourceException mre1) {
				
				try {
					String[] strkey = key.split(" ");
					
					for (int i = 0; i < strkey.length; i++) {
						value = value + rb.getString(strkey[i].trim()) + " ";
					}
					
					return value.trim();
					
				} catch (Exception e) {
					return null;
				}
			}
		}
	}

	/**
	 * Added to get labels from resource bundle.
	 * @param key
	 * @return
	 */
	public String gettingValueFromBundleForLabel(String key) {
		try {
			String s = key.trim();
			String k = resourceBundle.getString(s.replaceAll(" ", "_"));
			return k.trim();
		} catch (MissingResourceException mre1) {
			return key;
		}
	}

}
