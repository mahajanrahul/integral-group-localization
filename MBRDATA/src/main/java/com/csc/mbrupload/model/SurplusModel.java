package com.csc.mbrupload.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SurplusModel implements Serializable {
	
	private String chdrnum;
	private String nameextd;
	private String incurred;
	private String zamnt;
	private String amtpaid;

	public SurplusModel(String chdrnum, String nameextd, String incurred, String zamnt, String amtpaid){		
		this.chdrnum =  chdrnum;
		this.nameextd = nameextd;
		this.incurred = incurred;
		this.zamnt = zamnt;
		this.amtpaid = amtpaid;
		
	}
	public SurplusModel(){
		
	}
	
	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getNameextd() {
		return nameextd;
	}

	public void setNameextd(String nameextd) {
		this.nameextd = nameextd;
	}

	public String getIncurred() {
		return incurred;
	}

	public void setIncurred(String incurred) {
		this.incurred = incurred;
	}

	public String getZamnt() {
		return zamnt;
	}

	public void setZamnt(String zamnt) {
		this.zamnt = zamnt;
	}

	public String getAmtpaid() {
		return amtpaid;
	}

	public void setAmtpaid(String amtpaid) {
		this.amtpaid = amtpaid;
	}
	
}
