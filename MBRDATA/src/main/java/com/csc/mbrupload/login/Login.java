package com.csc.mbrupload.login;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Login extends HttpServlet {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Login.class);

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
		
		response.setContentType("text/html");

		String username = "";
		String userpass = "";
		String message = "Username\\Password can not be blank...!!!";

		try {
			if (request.getParameter("userName") != null
					&& request.getParameter("userName") != ""
					&& request.getParameter("password") != null
					&& request.getParameter("password") != "") {
				
				message = "Invalid User Name or Password...!!!";
				
				username = request.getParameter("userName").toString();
				userpass = request.getParameter("password").toString();

				int count = 0;
				if (username.trim().equalsIgnoreCase("stang20")
						&& userpass.trim().equalsIgnoreCase("stang20")) {
					request.setAttribute("userName", username);
					count++;
				}
				
				
				
				
				if (count > 0) {
					response.sendRedirect("MBRUpload.jsp");
				} else {
					request.setAttribute("message", message);
					request.getRequestDispatcher("/index.jsp").forward(request, response);
				}
			} else {
				request.setAttribute("message", message);
				request.getRequestDispatcher("/index.jsp").forward(request, response);
			}
			LOGGER.info("Login Servlet Ends");

		} catch (Exception e) {
			LOGGER.error("", e);
		}
	}
}