package com.csc.mbrupload.handler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.mbrupload.DAO.MBRUploadDAO;
import com.csc.mbrupload.model.SurplusModel;

/**
 * Servlet implementation class SurplusUploadHandler
 */
public class SurplusUploadHandler extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SurplusUploadHandler.class);
	private static final int MAX_MEMORY_SIZE = 1024 * 1024 * 2;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SurplusUploadHandler() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String uploadSurplusStatus = "Upload failed, Please check your file data!!!";
		SurplusModel surModel = null;
		List<SurplusModel> listSurModel = null;
		MBRUploadDAO uploadDAO = new MBRUploadDAO();
		HttpSession session = request.getSession();
		boolean isValid = true;
		try {
			String contentType = request.getContentType();
			InputStream is = null;
			HSSFWorkbook wb = null;
			HSSFSheet sheet = null;
			HSSFRow row = null;
			HSSFCell cell = null;
			boolean isMultipart = ServletFileUpload.isMultipartContent(request);
			if (!isMultipart) {
				return;
			}

			if ((contentType != null)
					&& (contentType.indexOf("multipart/form-data") >= 0)) {
				// Create a factory for disk-based file items
				DiskFileItemFactory factory = new DiskFileItemFactory();
				// Sets the size threshold beyond which files are written
				// directly to disk.
				factory.setSizeThreshold(MAX_MEMORY_SIZE);
				// Create a new file upload handler
				ServletFileUpload upload = new ServletFileUpload(factory);
				List items = upload.parseRequest(request);
				Iterator iter = items.iterator();
				FileItem item = null;
				File surFile = null;
				while (iter.hasNext()) {
					item = (FileItem) iter.next();
					if (!item.isFormField()) {
						is = item.getInputStream();
					}
				}				
				wb = new HSSFWorkbook(is);
				sheet = wb.getSheetAt(0);
				int rowNum = sheet.getLastRowNum() + 1;
				int colNum = sheet.getRow(0).getLastCellNum();
				listSurModel = new ArrayList();
				for (int i = 0; i < rowNum; i++) {
					surModel = new SurplusModel();
					row = sheet.getRow(i);
					if (isValid) {
					for (int j = 0; j < colNum; j++) {
						cell = row.getCell(j);
						String value = cellToString(cell);
						if (j == 0) {
							Double dvalue = Double.parseDouble(value);
							DecimalFormat df = new DecimalFormat("########");
							value = df.format(dvalue);
							if (value.length() > 8) {
								uploadSurplusStatus = "Data is not valid at Column["
										+ j+1 + "] and Row[" + i+1 + "]";
								isValid = false;
								break;
							}
							surModel.setChdrnum(value);
						} else if (j == 1) {
							if (value.length() > 60) {
								uploadSurplusStatus = "Data is not valid at Column["
										+ j+1 + "] and Row[" + i+1 + "]";
								isValid = false;
								break;
							}
							surModel.setNameextd(value);
						} else if (j == 2) {
							Double dvalue = Double.parseDouble(value);
							DecimalFormat df = new DecimalFormat(
									"###############");
							value = df.format(dvalue);
							if (value.length() > 15) {
								uploadSurplusStatus = "Data is not valid at Column["
										+ j+1 + "] and Row[" + i+1 + "]";
								isValid = false;
								break;
							}
							surModel.setIncurred(value);
						} else if (j == 3) {
							Double dvalue = Double.parseDouble(value);
							DecimalFormat df = new DecimalFormat(
									"###############");
							value = df.format(dvalue);
							if (value.length() > 15) {
								uploadSurplusStatus = "Data is not valid at Column["
										+ j+1 + "] and Row[" + i+1 + "]";
								isValid = false;
								break;
							}
							surModel.setZamnt(value);
						} else if (j == 4) {
							Double dvalue = Double.parseDouble(value);
							DecimalFormat df = new DecimalFormat(
									"###############");
							value = df.format(dvalue);
							if (value.length() > 15) {
								uploadSurplusStatus = "Data is not valid at Column["
										+ j+1 + "] and Row[" + i+1 + "]";
								isValid = false;
								break;
							}
							surModel.setAmtpaid(value);
						}
					}					
						listSurModel.add(surModel);
					}
				}
				if (isValid) {
					uploadDAO.saveSurplusDataIntoDB(listSurModel);
					is.close();
					uploadSurplusStatus = "File Uploaded Successfully";
				}
			}

		} catch (FileNotFoundException fe) {
			LOGGER.error("File can not found", fe);
		} catch (IOException ie) {
			LOGGER.error("", ie);
		} catch (Exception e) {
			LOGGER.error("", e);
		}
		session.setAttribute("uploadSurplusStatus", uploadSurplusStatus);
		response.sendRedirect("MBRUpload.jsp");

	}

	private String cellToString(HSSFCell cell) {
		int type;
		Object result = null;
		type = cell.getCellType();
		switch (type) {
		case 0:// numeric value in excel
			result = cell.getNumericCellValue();
			break;
		case 1: // string value in excel
			result = cell.getStringCellValue();
			break;
		case 2: // boolean value in excel
			result = cell.getBooleanCellValue();
			break;
		}

		return result.toString();
	}

}
