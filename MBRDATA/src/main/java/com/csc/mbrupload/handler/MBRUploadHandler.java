package com.csc.mbrupload.handler;

import java.io.DataInputStream;
import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.mbrupload.DAO.MBRUploadDAO;

/**
 * Servlet implementation class LabelValueImportServlet
 */
public class MBRUploadHandler extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger LOGGER = LoggerFactory.getLogger(MBRUploadHandler.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MBRUploadHandler() {
		super();
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);

		MBRUploadDAO mbrDao = new MBRUploadDAO();

		String contentType = request.getContentType();
		
		String uploadStatus = "Upload failed, Please check your file data!!!";

		if ((contentType != null)
				&& (contentType.indexOf("multipart/form-data") >= 0)) {
			DataInputStream in = new DataInputStream(request.getInputStream());
			int formDataLength = request.getContentLength();
			byte dataBytes[] = new byte[formDataLength];
			
			int byteRead = 0;
			int totalBytesRead = 0;
			while (totalBytesRead < formDataLength) {				
				byteRead = in.read(dataBytes, totalBytesRead, formDataLength);
				if(byteRead > 0) { //IGROUP-2080
					totalBytesRead += byteRead;
				} //IGROUP-2080
			}

			String file = new String(dataBytes);
			String saveFile = file.substring(file.indexOf("filename=\"") + 10);
			saveFile = saveFile.substring(0, saveFile.indexOf("\n"));
			saveFile = saveFile.substring(saveFile.lastIndexOf("\\") + 1,
					saveFile.indexOf("\""));
			int lastIndex = contentType.lastIndexOf("=");
			String boundary = contentType.substring(lastIndex + 1, contentType
					.length());

			int pos;

			pos = file.indexOf("filename=\"");
			pos = file.indexOf("\n", pos) + 1;
			pos = file.indexOf("\n", pos) + 1;
			pos = file.indexOf("\n", pos) + 1;
			int boundaryLocation = file.indexOf(boundary, pos) - 4;
			int startPos = ((file.substring(0, pos)).getBytes()).length;
			int endPos = ((file.substring(0, boundaryLocation)).getBytes()).length;
			String str = file.substring(startPos, boundaryLocation);
			String lineStr[] = str.split("\\n"); //IGROUP-2080
			LOGGER.info("Value of my File is: {}", str);
			for (int i = 0; i < lineStr.length; i++) {  //IGROUP-2080
				LOGGER.debug("Value of my array at {} th lication is: {} \n", (i + 1), lineStr[i]);		//IGROUP-2080	
			}
			boolean flag = false;
			try {
				flag = mbrDao.loadDataIntoDB(lineStr);  //IGROUP-2080
			} catch (SQLException e) {
				LOGGER.error("", e);
			}

			if (flag) {
				uploadStatus = "File Uploaded Successfully";
			}
			request.setAttribute("uploadStatus", uploadStatus);
			request.getRequestDispatcher("/MBRUpload.jsp").forward(request,
					response);
		}
	}

}
