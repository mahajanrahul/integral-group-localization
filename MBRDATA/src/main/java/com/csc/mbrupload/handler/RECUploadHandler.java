package com.csc.mbrupload.handler;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.mbrupload.DAO.MBRUploadDAO;

/**
 * Servlet implementation class RECUploadHandler
 */
public class RECUploadHandler extends HttpServlet {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(RECUploadHandler.class);
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RECUploadHandler() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String uploadStatus = "Upload failed, Please check your file data!!!";
		try {

			ServletInputStream fs = request.getInputStream();
			byte[] junk = new byte[1024];
			int bytesRead = 0;
			List<String> queryList = new ArrayList<String>();
			bytesRead = fs.readLine(junk, 0, junk.length);
			bytesRead = fs.readLine(junk, 0, junk.length);
			bytesRead = fs.readLine(junk, 0, junk.length);
			bytesRead = fs.readLine(junk, 0, junk.length);
			HSSFWorkbook workbook = new HSSFWorkbook(fs);
			HSSFSheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			rowIterator.next();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				String query = "";

				for (int k = 0; k < 12; k++) {
					Cell cell = row.getCell(k);
					if (cell != null) {
						switch (cell.getCellType()) {
						case Cell.CELL_TYPE_NUMERIC:
							int val = (int) cell.getNumericCellValue();
							if (val != 0) {
								if (k == 5 || k == 9)
									query = query + "" + val + " ,";
								else
									query = query + "'" + val + "' ,";
							} else {
								query = query + "null,";
							}
							break;
						case Cell.CELL_TYPE_STRING:
							String value = cell.getStringCellValue().trim();
							if (value != null && !value.trim().equals("")) {
								if (k == 5 || k == 9)
									query = query + "'" + value + "' ,";
								else
									query = query + "'" + value + "' ,";
							} else {
								query = query + "null,";
							}
							break;
						}
					} else {
						query = query + "null,";
					}
				}
				query = query.substring(0, query.length() - 1);

				queryList.add(query);
			}
			MBRUploadDAO uploadDAO = new MBRUploadDAO();
			uploadDAO.saveRJSFPFDataIntoDB(queryList);
			fs.close();
			uploadStatus = "File Uploaded Successfully";
			
		} catch (FileNotFoundException e) {
			LOGGER.error("", e);
		} catch (IOException e) {
			LOGGER.error("", e);
		} catch (Exception e) {
			LOGGER.error("", e);
		}
		request.setAttribute("uploadStatusExcel", uploadStatus);
		request.getRequestDispatcher("/MBRUpload.jsp").forward(request,
				response);
	}

}
