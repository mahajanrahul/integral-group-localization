package com.csc.mbrupload.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.mbrupload.model.SurplusModel;

/**
 * @author spandey20
 * 
 */
public class MBRUploadDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(MBRUploadDAO.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.csc.gild.dao.BaseDAO#getPrimaryKey()
	 */
	public String getPrimaryKey() {
		return "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.csc.gild.dao.BaseDAO#getTableName()
	 */
	public String getTableName() {
		return "COMMON";
	}

	/**
	 * @param firstSheet
	 * @return
	 * @return
	 * @throws SQLException
	 */
	public boolean loadDataIntoDB(String str[]) throws SQLException {
	String sqlSelectQuery = "select * from micdpf";
	String sqlDeleteQuery = "delete from micdpf";

	Connection conn = DBConnection.getConnection();
	PreparedStatement pstmtSelect = null;
	PreparedStatement pstmtdelete = null;
	PreparedStatement pstmtInsert = null;
	ResultSet rs = null;
	int temp = 0;
	try {
		conn.setAutoCommit(false);	
		pstmtdelete = conn.prepareStatement(sqlDeleteQuery);
		pstmtdelete.execute();
		
		String sqlStmt = "Insert into MICDPF (MIDATA) values (?)";
		String line = "";   //IGROUP-2080
		for (int i = 0; i < str.length; i++) {
			line = str[i].trim();  //IGROUP-2080
			if(line.length()>0) {   //IGROUP-2080
					pstmtInsert = conn.prepareStatement(sqlStmt);
					pstmtInsert.setString(1, str[i]);
					temp = pstmtInsert.executeUpdate();
					pstmtSelect = conn.prepareStatement(sqlSelectQuery);
			}  //IGROUP-2080
		}
		
		rs = pstmtSelect.executeQuery();
		while (rs.next()) {				
			LOGGER.info("Inserted Table Data is: {}", rs.getString(2));
		}
		if (temp != 1) {
			return false;
		}
		return true;		
	} 
	catch (SQLException e) {
		LOGGER.error("Unable to insert data into micdpf Table.", e);		
	} 
	finally {
		conn.commit();
		conn.close();
	}
	return false;}

	public boolean saveRJSFPFDataIntoDB(List<String> queryList)
			throws SQLException {

		String query = "INSERT INTO RJSFPF (BATCH, CHDRNUM, APLNNO, MBRNO, DPNTNO, ORIGAMT, BANKCODE, PAYTYPE, CHQNUM, TCHQDATE, BANKKEY, ZCHQTYP ) VALUES ";
		Connection conn = DBConnection.getConnection();

		try {
			conn.setAutoCommit(false);
			Statement stmt = conn.createStatement();
			ResultSet rs = null;

			int temp = 0;
			int recordInserted = 0;
			for (int i = 0; i < queryList.size(); i++) {
				LOGGER.debug("{} ({})", query, queryList.get(i));
				temp = stmt.executeUpdate(query + "(" + queryList.get(i) + ")");
				recordInserted++;
			}
			LOGGER.info("No of records inserted in table : {}", recordInserted);
			if (temp != 1) {
				return false;
			}
			return true;
		} catch (SQLException e) {
			LOGGER.error("Unable to insert data into micdpf Table.", e);			
		} finally {
			conn.commit();
			conn.close();
		}
		return false;

	}

	public boolean saveSurplusDataIntoDB(List<SurplusModel> listSurModel)
			throws SQLException {

		String sqlinsert = "INSERT INTO SPLUPF (CHDRNUM, NAMEEXTD, INCURRED, ZAMNT, AMTPAID ) VALUES (?,?,?,?,?)";
		String sqldelete = "DELETE FROM SPLUPF";
		Connection conn = DBConnection.getConnection();
		PreparedStatement pstmt = null;
		PreparedStatement pstmtdelete = null;

		try {
			conn.setAutoCommit(false);
			pstmt = conn.prepareStatement(sqlinsert);
			pstmtdelete = conn.prepareStatement(sqldelete);
			pstmtdelete.execute();
			for (SurplusModel surModel : listSurModel) {
				pstmt.setString(1, surModel.getChdrnum());
				pstmt.setString(2, surModel.getNameextd());
				pstmt.setString(3, surModel.getIncurred());
				pstmt.setString(4, surModel.getZamnt());
				pstmt.setString(5, surModel.getAmtpaid());
				pstmt.addBatch();

			}			
			pstmt.executeBatch();
			conn.commit();

		} catch (SQLException e) {
			LOGGER.error("Unable to insert data into splupf Table", e);
			conn.rollback();
		} finally {

			if (pstmt != null) {
				pstmt.close();
			}
			if(pstmtdelete != null){
				pstmtdelete.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return false;
	}
}
