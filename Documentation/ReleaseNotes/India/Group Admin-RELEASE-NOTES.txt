﻿============================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Admin - Version 5.2  Date: 04/30/2013
============================================================================================
 Bug Fixes
-----------

#6595 Ticket for Group I18N file changes

============================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Admin - Hotfix for #6032 Date: 02/08/2013
============================================================================================
 Bug Fixes
-----------

#6032	MRTA Schedule calc error

============================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Admin - Version 3.5 Date: 07/06/2012
============================================================================================
 Bug Fixes
-----------

#5183	Two Master menu options missing in INTEGRAL
#5181 	SR942 - Work with Headcount UI display issue
#5109 	SR95A - Discrepancy in the prompt message displayed
#5208  	SR9TW Cash Marry UIG issues
#5200 	S0027 User Security Issues

============================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Admin - Hotfixes on Date: 07/03/2012
============================================================================================
 Bug Fixes
-----------
#5128   SR9TW - Cash Marry Inquiry, amount is still displayed after receipting
#5145   SR943 Member/Headcount Premium - system proceed to display each mbr's details without selection
#5197   S2473-Client Scroll screen, duplicate records displayed

============================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Admin - Hotfixes on Date: 06/29/2012
============================================================================================
 Bug Fixes
-----------
#5101   Table literal are way out of alignment
#5131   SR9AF UI - fields are not in proper sequence/missing field

============================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Admin - Hotfixes on Date: 06/28/2012
============================================================================================
 Bug Fixes
-----------
#5113   NB Premium computation - discrepancy
#5141   S9379 - Client Scroll_Discrepany in the information details displayed
#4842   S2464 Client needs to be selected twice
#5134   SR9B1 - Product Definition NewBusiness_field issues
#4616   can not change plan for existing members
#4875   SR9WN - Prem Meth 11 Copy Screen errors
#5140 	SR9GH - Policy Posted Premium History - duplicate records displayed


============================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Admin - Hotfixes on Date: 06/26/2012
============================================================================================
 Bug Fixes
-----------
#4922   S9105 Action buttons exceed margin during WW Members


============================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Admin - Hotfixes on Date: 06/25/2012
============================================================================================
 Bug Fixes
-----------

#5101	Table literal are way out of alignment
#5110 	SR289 Receipt Header screen has an extra unnamed field
#5111 	SR95T- Using the tab ' Balance Enquiry' against a selected record in this screen - screen error occurs
#5132	SR9AG_UI fields presentation issues
#5133 	SR9WA checkbox protected in modified mode


============================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Admin - Version 3.4 Date: 06/22/2012
============================================================================================
 Bug Fixes
-----------

#5042	SG004 Policy Header 2 Issues
#5014 	Dissection codes UIG issues
#5015 	SR9PT Policy Type 2 Parm UIG issues
#5095 	Amounts in all reports have leading zeroes
#4996  	SR241 UW & Claims Rules UIG issues
#5101 	Table literal are way out of alignment
#5107 	SR9VT - table items' fields protected on modify mode
#5031 	S9126 - Cloning error on Subsidiary check box



============================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Admin - Version 3.3 Date: 06/11/2012
============================================================================================
 Bug Fixes
-----------

#5013	SR976 Annualise Prem Screen hangs
#5048	S0016 - System error screen not displayed
#4773	SR9CB - Unable to clear hold codes for Health Claim
#5065	SR96H Enq 3 WW All Policies UIG issues
#5041	SR96H Policy Enquiry-3 (UIG)
#5071	SR9GB Renewal Review WW All Policies errors
#4994	S2310 G3GLBALST Param - GL Account code list is empty
#5013 	SR976 Annualise Prem Screen hangs
#5065	SR96H Enq 3 WW All Policies UIG issues
#5041	SR96H Policy Enquiry-3 (UIG)
#5058	PRTF for G3GTRIAL and G3COMMON
#4855	G3MBRDATAI - Batch Job issues
#4688	SR9KI WW Member Data Format UIG issues

============================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Admin - Version 3.2 Date: 5/25/2012
============================================================================================
 Bug Fixes
-----------

#4990	G3GLUPDATE in Aborted Status
#5035	S9106 UW Decision field should use dropdown
#5036	S9106 Sum Insured field issues
#5068	ORA-00955: Name already used by an existing object.
#4535	G3INSTBILL and G3INSTBIL2 not working
#4729	G3COMMON Batch Job - Not completed successfully
#5029	SR9K2 Online Renewal Review screen hangs

============================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Admin - Version 3.1 Date: 5/18/2012
============================================================================================
 Bug Fixes
-----------

#4715	S9121 GP New Par Screen Issue
#4618	S9106 Group Member Entry Addtl Layout Issues
#4741	Batch G3CLM2PAY Failed
#5019	Table create errors
#5047   Group Performance Improvement Project] is Fixed


============================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Admin - Version 3.0 Date: 5/11/2012
============================================================================================
 Bug Fixes
-----------

#4848		Batch Job G3AGNTRPT did not complete successfully
#4845		G3GISSUE Aborted during TVL New Business Issuance
#4908		SR939 Transaction Inquiry Screen Issues
#4985		S9105 Change table Header
#4566		S9105 WW Members Label Issues
#4771		SR9BX - Only showing 1 claim detail
#4858		Group Performance Improvement Project


============================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Admin - Version 2.9 Date: 5/4/2012
============================================================================================
UI Modernization Bug Fixes
---------------------------
#4427		SR9FP - Product/Plan Additional Data not showing additional plan
#4564		SR9XX - Provider org link be Interal and External (UIG)
#4575		S9114 Long Plan Desc should be an XT field
#4602		SR9GB WW All Policies Screen issues
#4624		S9124 Payor Screen Layout issues
#4706		SR941 WW Members UIG issues
#4731		S9804 Fields written outside screen margin
#4770		S2085 Approve Payment - Unable to enter password
#4688		SR9KI WW Member Data Format UIG issues
#4680		S2200 - Payment maintenance UIG issues
#4443		SR9J2 Default GST Details UIG issues
#4552		S2067 Receipt Number not reflected in the message
#4944		S2111 GL Chart Inquiry not UIG
#4869		S2081 Client Bank UIG issues
#4874		SR22U Post dated cheque scroll list error
#4876		SR0XH Prem meth 12 UIG issues
#4877		SR9XJ Prem meth 13 UIG issues
#4914		SR9TI JSP Error upon click on MRTA Schd
#4901		S2465 Client missing functions link
#4414		Welcome Page UIG issues
#4264		S2477 & S2472 - Unable to exit screen with any input
#4867		SR95A Error Message on Auto Renew Checkbox


Transformation Bug Fixes/Changes
-------------------------------------
#4108		SR25C - JSP error when invoke Payment Remarks screen
#4266		S2151 - GL Posted Transactions Inquiry System hangs
#4581		S9216 GST Journal Entry Screen issues
#4616		S9106 Cannot change plan for existing members
#4663		S9117 WW Headcount enquiry mode, data not protected
#4768		S1719 Error Page upon click on any Item from table
#4773		SR9CB - Unable to clear hold codes for Health Claim
#4783		Extra .. in extra info text
#4769		S2321 screen not displayed for policy selection
#4733		G3CLAIMPAY in Aborted status
#4729		G3COMMON Batch Job - Not completed successfully
#4873		SR9UG UW Decision Date should not be protected


============================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Admin - Version 1.10 Date: 2/17/2012
============================================================================================
UI Modernization Bug Fixes
---------------------------
4296		S9103 Policy Header 1 List of Issues
4312		S9104 Cannot add subsidiary
4316		Tables & Codes Generic Layout Issues
4319		Generic Client Layout issues
4331		SR9FQ Addtl Member Data Screen issues
4332		SR9K9 Plan Scroll Screen Issues
4348		S2478 Client Roles UIG issues
4358		S0028 User Details UIG Issues
4409		Unable to inquire on Table items
4402		S9219 Default Text checkbox value not accepted
4413		S0260 Soft Lock Held UIG issues
4346		S2473 Client Scroll UIG issues


Transformation Bug Fixes/Changes
-------------------------------------
4099 		S9106 - Error Message of "Mbr Insdte is invalid" during Major Alteration
4211 		SR241 - UW & Claims Limit Page errors during enquiry


============================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Admin - Version 1.9 Date: 2/16/2012
============================================================================================
Transformation Bug Fixes/Changes
-------------------------------------
4154 		S9369 - System hangs upon click on Continue
4271 		SR9B5 - Benefit Mapping loading too long
4273 		S2464 - Client Scroll screen blank during initial display
4287 		Remove unnecessary printStackTrace
4352 		Error Page upon click on Continue on Member Entry NB Screen
4343 		Error Page upon click on Continue
4421 		Batch Server not running on FT server


============================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Admin - Version 1.8 Date: 2/13/2012
============================================================================================

UI Modernization Bug Fixes
---------------------------
4296		S9103 Policy Header 1 List of Issues
4300		S9114 Product Definition NB List of Issues
4302		S2321 Not all policies under the client are being shown
4310		SR9BO Eligibility table do not need horizontal scrollbar
4313		S9126 Default data template drop down value



Transformation Bug Fixes/Changes
-------------------------------------
3677		UI enhancement
3918 		Shakedown Client sorting
4035		S2473 Trans History Enquiry - Page error in client scroll
4053 		SR25E - Cash Inquiry error
4064 		SR23H - Policy AC Movement page errors
4162 		S2610 Contract not found
4147		G3GTRIAL not generated
4259 		SR9GB Duplicate records found
4284 		S2610 - Confirmation (MSGBOX) error during Receipt
4287		Monmsg changes
4317 		BO Screen validation
4318		Array variables not handled


===================================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Admin - Version 1.7.3 (HOT FIX)Date: 2/10/2012
===================================================================================================

UI Modernization Bug Fixes
---------------------------
4288		S0017 Welcome Page Layout issues
4291		SR9VX Policy Issue Rule - Fields should use dropdown list
4295		S0026 Layout issues
4297		SG004 Policy Header 2 List of Issues
4298		SR95A Policy Header 3 List of Issues
4299		S9112 Work with Proposals NB List of Issues
4301		S0080 WW Submitted Schedules Issues
4305		S0027 User Security List is empty
4307		S0023 Validflag value is not being accepted during Item Modification
4312		S9104 Cannot add subsidiary
4303		S9104 Work with Subsidiaries


===================================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Admin - Version 1.7.2 (HOT Fix) Date: 2/8/2012
===================================================================================================

UI Modernization Bug Fixes
---------------------------
4288		S0017 Welcome Page Layout issues
4289		S0017 Error Messages and cannot change Default values
4292		S9126 NB Submenu Layout Issues
4293		S9126 Error encountered durign WW Policy (Unissued Policy)
4294		S9126 Error encountered during creation of new policy
NA		Common Fix for: Images (including dropdown) were not visible




============================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Admin - Version 1.7 Date: 2/3/2012
============================================================================================
Deploying UI Enriched Screens

Transformation Bug Fixes/Changes
--------------------------------
4022 		S9102 - Policy Enquiry Unable to exit header screen
4126 		S0109 Error Page on Process Definition 
4127 		S0109 Error message at the bottom of the page
4160 		SR9WM - Screen does not proceed to next screen 
4161 		SR9XH - Screen does not proceed after option is entered


============================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Admin - Version 1.6 Date: 1/29/2012
============================================================================================

Bug Fixes/Changes
-----------------
#3921 		Shakedown Table 
#4052 		S2610 Receipt creation failed 
#4054 		S2067 - Unable to do Receipt Enquire using Cheque no 
#4100 		S2473 - Client Scroll errors 
#4195 		Policy Issue not working


============================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Admin - Version 1.5 Date: 1/19/2012
============================================================================================

Bug Fixes/Changes
-----------------
#3917 		Shakedown Clients - Various errors 
#3989 		Coy 0 - Main Menu Error Msg 
#4006 		Help Window scroll - Error on page 
#4061 		Welcome Page - Errors 
#4103 		SR209 - 
Function not available in Client screen 
#4104 		S2479 - Client Merge error 
#4121 		S0261 Error Page upon Click on Softlock Maintenance 
#4123 		S9120 Unable to open Text and General screens 
#4125 		SR96T System hangs upon click on Add 



============================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Admin - Version 1.4 Date: 1/13/2012
============================================================================================

Bug Fixes/Changes
-----------------
#3919		Shakedown Agent
#3920		Shakedown Client Bank AC
#3923		Shakedown New Business
#3995		Coy 1 - Client modify error
#4020		S0023 Coy 9 - Tables and codes
#4037		SG004 Age Calculation Method not accepted
#4102		S9106 JSP Error when terminating a member
#4105		S9103 JSP Error when entering Gen Page in Pol Header 1
#4108		SR25C - JSP error when invoke Payment Remarks screen
#4122		S9105 JSP Error during Delete Member

============================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Admin - Version 1.3  Date : 1/11/2012
============================================================================================

Bug Fixes/Changes
-----------------
#4116		S9103 JSP Error upon entry of Text in Extra Info
#4024		SR267 Policy Inquiry 2 - Missing option
#4033		SR921 Member Enquiry - Missing option for Beneficiaries
#4102		S9106 JSP Error when terminating a member
#4053		SR25E - Cash Inquiry error
#4052		S2610 Receipt creation failed
#4046		SR9FR Convert quotation number to new policy number failed
#4044		S9352 JSP Error on Claim Header Screen
#4040		S2480 - Unable to inquire on client
#4042		S2303 - System error during Link clients
#3993		Coy 1 - Bank Branch Modify Scroll selection

============================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Admin - Version 1.2 Date : 1/06/2012
============================================================================================

Bug Fixes/Changes
-----------------
#3987		Coy 0 - Batch Submission screen
#3988		Coy 0 - User Security
#3990		Coy 0 - Table item modify
#3991		Coy 1 - Agent creation
#3992		Coy 1 - Bank Branch Modify T3716 error
#3996		Coy 1 - Table item modify
#4003		S0027 User-ID Enquiry & Modify
#4004		S0023 Table item modify

============================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Admin - Version 1.1 Date : 12/23/2011
============================================================================================

Base Release
