﻿============================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Batch - Version 2.3
============================================================================================
Bug Fixes/Changes
-----------
#6254: On G3INSTBILL parameter prompt screen S9180 the "Calander" icon and "Continue" button is not proper
#6193: The batch G3GTRIAL aborted (Set up)

============================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Batch - Version 2.2
============================================================================================
Bug Fixes/Changes
-----------

Enhancements
-----------
#6069: Batch server runs in stand-alone mode (Group)

============================================================================================
		  RELEASE NOTES - CSC INTEGRAL Group Batch - Version 1.1
============================================================================================
Bug Fixes/Changes
-----------
#4900: G3GTRIAL is in Aborted/Started Status
