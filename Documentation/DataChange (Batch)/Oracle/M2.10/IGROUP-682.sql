CREATE OR REPLACE FORCE VIEW "VM1DTA"."BDDA" ("UNIQUE_NUMBER", "PAYRCOY", "PAYRNUM", "MANDREF", "GRACE", "DTEFSBM", "DTEFREJ", "FREASON", "DTESSBM", "DTESREJ", "SREASON", "DTEAPROV", "DTEDDUC", "DTEEXTR", "JOBNM", "USRPRF", "DATIME", "VALIDFLAG") AS 
  SELECT UNIQUE_NUMBER,
            PAYRCOY,
            PAYRNUM,
            MANDREF,
            GRACE,
            DTEFSBM,
            DTEFREJ,
            FREASON,
            DTESSBM,
            DTESREJ,
            SREASON,
            DTEAPROV,
            DTEDDUC,
            DTEEXTR,
            JOBNM,
            USRPRF,
            DATIME,
            VALIDFLAG
       FROM BDDAPF
       WHERE VALIDFLAG = '1'
   ORDER BY PAYRCOY, PAYRNUM, MANDREF;
/
   
create or replace TRIGGER "VM1DTA"."TR_BDDAPF" 
before insert on  BDDAPF
for each row
declare
v_pkValue  number;
v_flag     exception;
v_rownum   number;
PRAGMA EXCEPTION_INIT (v_flag, -1);  -- it is assumed
begin
v_rownum := 0;
SELECT COUNT(*) INTO v_rownum  FROM BDDAPF
WHERE (PAYRCOY= :New.PAYRCOY AND PAYRNUM= :New.PAYRNUM AND MANDREF=
:New.MANDREF AND (VALIDFLAG='1')) 
;
if (v_rownum > 0 ) then
RAISE v_flag;
else
select SEQ_BDDAPF.nextval into v_pkValue from dual;
:New.unique_number := v_pkValue;
end if;
end TR_BDDAPF;
/

DROP INDEX "VM1DTA"."BDDA";
CREATE INDEX "VM1DTA"."BDDA" ON "VM1DTA"."BDDAPF" ("PAYRCOY", "PAYRNUM", "MANDREF");
/