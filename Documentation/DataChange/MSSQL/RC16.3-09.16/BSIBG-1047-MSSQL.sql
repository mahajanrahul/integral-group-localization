ALTER TABLE VM1DTA.GDCMPF ALTER COLUMN CLTADDR01 [nchar](50)
ALTER TABLE VM1DTA.GDCMPF ALTER COLUMN CLTADDR02 [nchar](50)
ALTER TABLE VM1DTA.GDCMPF ALTER COLUMN CLTADDR03 [nchar](50)
ALTER TABLE VM1DTA.GDCMPF ALTER COLUMN CLTADDR04 [nchar](50)
ALTER TABLE VM1DTA.GDCMPF ALTER COLUMN CLTADDR05 [nchar](50)

ALTER TABLE VM1DTA.GRIEPF ALTER COLUMN CLTADDR01 [nchar](50)
ALTER TABLE VM1DTA.GRIEPF ALTER COLUMN CLTADDR02 [nchar](50)
ALTER TABLE VM1DTA.GRIEPF ALTER COLUMN CLTADDR03 [nchar](50)
ALTER TABLE VM1DTA.GRIEPF ALTER COLUMN CLTADDR04 [nchar](50)
ALTER TABLE VM1DTA.GRIEPF ALTER COLUMN CLTADDR05 [nchar](50)

ALTER TABLE VM1DTA.GSTIPF ALTER COLUMN CLTADDR1 [nchar](50)
ALTER TABLE VM1DTA.GSTIPF ALTER COLUMN CLTADDR2 [nchar](50)
ALTER TABLE VM1DTA.GSTIPF ALTER COLUMN CLTADDR3 [nchar](50)
ALTER TABLE VM1DTA.GSTIPF ALTER COLUMN CLTADDR4 [nchar](50)
ALTER TABLE VM1DTA.GSTIPF ALTER COLUMN CLTADDR5 [nchar](50)

ALTER TABLE VM1DTA.MIERPF ALTER COLUMN CLTADDR01 [nchar](50)
ALTER TABLE VM1DTA.MIERPF ALTER COLUMN CLTADDR02 [nchar](50)
ALTER TABLE VM1DTA.MIERPF ALTER COLUMN CLTADDR03 [nchar](50)
ALTER TABLE VM1DTA.MIERPF ALTER COLUMN CLTADDR04 [nchar](50)

ALTER TABLE VM1DTA.MIOKPF ALTER COLUMN CLTADDR01 [nchar](50)
ALTER TABLE VM1DTA.MIOKPF ALTER COLUMN CLTADDR02 [nchar](50)
ALTER TABLE VM1DTA.MIOKPF ALTER COLUMN CLTADDR03 [nchar](50)
ALTER TABLE VM1DTA.MIOKPF ALTER COLUMN CLTADDR04 [nchar](50)

ALTER TABLE VM1DTA.MISFPF ALTER COLUMN CLTADDR01 [nchar](50)
ALTER TABLE VM1DTA.MISFPF ALTER COLUMN CLTADDR02 [nchar](50)
ALTER TABLE VM1DTA.MISFPF ALTER COLUMN CLTADDR03 [nchar](50)
ALTER TABLE VM1DTA.MISFPF ALTER COLUMN CLTADDR04 [nchar](50)

ALTER TABLE VM1DTA.TPAMPF ALTER COLUMN CLTADDR01 [nchar](50)
ALTER TABLE VM1DTA.TPAMPF ALTER COLUMN CLTADDR02 [nchar](50)
ALTER TABLE VM1DTA.TPAMPF ALTER COLUMN CLTADDR03 [nchar](50)
ALTER TABLE VM1DTA.TPAMPF ALTER COLUMN CLTADDR04 [nchar](50)
ALTER TABLE VM1DTA.TPAMPF ALTER COLUMN CLTADDR05 [nchar](50)

ALTER TABLE VM1DTA.ZTCHPF ALTER COLUMN CLTADDR01 [nchar](50)
ALTER TABLE VM1DTA.ZTCHPF ALTER COLUMN CLTADDR02 [nchar](50)
ALTER TABLE VM1DTA.ZTCHPF ALTER COLUMN CLTADDR03 [nchar](50)
ALTER TABLE VM1DTA.ZTCHPF ALTER COLUMN CLTADDR04 [nchar](50)