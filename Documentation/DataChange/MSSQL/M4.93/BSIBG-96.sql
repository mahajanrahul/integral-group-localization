
/*TABLE*/
/*ALTER TABLE BNFHPF*/
Alter Table BNFHPF Add
OPTBENEFIT NCHAR(1) 
Go

/*CREATE TABLE RSLLPF*/

CREATE TABLE [VM1DTA].[RSLLPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL    ,
	[CHDRCOY] [nchar](1)    ,
	[CHDRNO] [nchar](8)    ,
	[VFLAG] [nchar](1)    ,
	[CNTTYP] [nchar](3)    ,
	[MBRNO] [nchar](5)    ,
	[DPNTNO] [nchar](2)    ,
	[CLNTNUM] [nchar](8)    ,
	[SUBSNUM] [nchar](8)    ,
	[STDATE] [INT]    ,
	[PRODTYP] [NCHAR](4)    ,
	[REOVANLIM] [int]    ,
	[STLOANLIM01] [INT]    ,
	[STLOANLIM02] [INT]    ,
	[STLOANLIM03] [INT]    ,
	[STLOANLIM04] [INT]    ,
	[STLOANLIM05] [INT]    ,
	[GDIAGCD01] [nchar](5)    ,
	[GDIAGCD02] [nchar](5)    ,
	[GDIAGCD03] [nchar](5)    ,
	[GDIAGCD04] [nchar](5)    ,
	[GDIAGCD05] [nchar](5)    ,
	[USRPRF] [nchar](10)    ,
	[JOBNM] [nchar](10)    ,
	[DATIME] [DATETIME]    ,
CONSTRAINT [PK_RSLLPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/*CREATE TABLE ADLMPF*/
CREATE TABLE [VM1DTA].[ADLMPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL    ,
	[CHDRCOY] [nchar](1)    ,
	[CHDRNO] [nchar](8)    ,
	[MBRNO] [nchar](5)    ,
	[CNTTYP] [nchar](3)    ,
	[CLMNO] [nchar](8)    ,	
	[CLNTNUM] [nchar](8)    ,
	[SUBSNUM] [nchar](8)    ,
	[GCOCCNO] [nchar](2)    ,
	[DPNTNO] [nchar](2)    ,
	[CCDATE] [INT]    ,
	[CRDATE] [INT]    ,
	[REOVANLIM] [INT]    ,
	[STLOANLIM01] [INT]    ,
	[STLOANLIM02] [INT]    ,
	[STLOANLIM03] [INT]    ,
	[STLOANLIM04] [INT]    ,
	[STLOANLIM05] [INT]    ,
	[GDIAGCD01] [nchar](5)    ,
	[GDIAGCD02] [nchar](5)    ,
	[GDIAGCD03] [nchar](5)    ,
	[GDIAGCD04] [nchar](5)    ,
	[GDIAGCD05] [nchar](5)    ,
	[AMTPAY01] [INT]    ,
	[AMTPAY02] [INT]    ,
	[AMTPAY03] [INT]    ,
	[AMTPAY04] [INT]    ,
	[AMTPAY05] [INT]    ,
	[BALANCE] [INT]    ,
	[BALANCE01] [INT]    ,
	[BALANCE02] [INT]    ,
	[BALANCE03] [INT]    ,
	[BALANCE04] [INT]    ,
	[BALANCE05] [INT]    ,
	[USRPRF] [nchar](10)    ,
	[JOBNM] [nchar](10)    ,
	[DATIME] [DATETIME]    ,
CONSTRAINT [PK_ADLMPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
Go
select * from opbn
/*CREATE TABLE OPBNPF*/
CREATE TABLE [VM1DTA].[OPBNPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL    ,
	[CHDRCOY] [nchar](1)    ,
	[CHDRNO] [nchar](8)    ,
	[VFLAG] [nchar](1)    ,
	[CNTTYP] [nchar](3)    ,
	[MBRNO] [nchar](5)    ,
	[DPNTNO] [nchar](2)    ,
	[CLNTNUM] [nchar](8)    ,
	[SUBSNUM] [nchar](8)    ,
	[STDATE] [INT]    ,
	[PRODTYP] [NCHAR](4)    ,
	[PLANNO] [NCHAR](3)    ,
	[BENCDE] [NCHAR](4)    ,
	[SIAMT] [NUMERIC](6,2)    ,
	[WEEKSIAMT] [NUMERIC](6,2)    ,
	[MINSIWEEK] [NUMERIC](6,2)    ,
	[PREMAMT] [NUMERIC](6,2)    ,
	[PREMRATE] [NUMERIC](6,2)    ,
	[PREMCALC] [NUMERIC](6,2)    ,
    [SFSTDATE]  [INT]    ,
	[SFENDDATE] [INT]    ,
	[ACTION] [nchar](1)    ,
	[TYPE]  [nchar](1)    ,
	[USRPRF] [nchar](10)    ,
	[JOBNM] [nchar](10)    ,
	[DATIME] [DATETIME]    ,
CONSTRAINT [PK_OPBNPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
Go

/*CREATE TABLE DIWPPF*/
select * from diwp
CREATE TABLE [VM1DTA].[DIWPPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL    ,
	[CHDRCOY] [nchar](1)    ,
	[CHDRNO] [nchar](8)    ,
	[VFLAG] [nchar](1)    ,
	[MBRNO] [nchar](5)    ,
	[DPNTNO] [nchar](2)    ,
	[CLNTNUM] [nchar](8)    ,
	[SUBSNUM] [nchar](8)    ,
	[STDATE] [INT]    ,
	[GDIAGCD01] [nchar](5)    ,
	[GDIAGCD02] [nchar](5)    ,
	[GDIAGCD03] [nchar](5)    ,
	[GDIAGCD04] [nchar](5)    ,
	[GDIAGCD05] [nchar](5)    ,
	[WAITPERI01] [INT]    ,
	[WAITPERI02] [INT]    ,
	[WAITPERI03] [INT]    ,
	[WAITPERI04] [INT]    ,
	[WAITPERI05] [INT]    ,
	[USRPRF] [nchar](10)    ,
	[JOBNM] [nchar](10)    ,
	[DATIME] [DATETIME]    ,
CONSTRAINT [PK_DIWPPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
Go

/*VIEW*/

/*ALTER VIEW BNFH*/

ALTER VIEW [VM1DTA].[BNFH](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, PRODTYP, PLANNO, BENCDE, BNFTLST, STRTDT, ENDDT, DEDUCTYPE, NOVISITS, TRANNO, USRPRF, JOBNM, OPTBENEFIT, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            PRODTYP,
            PLANNO,
            BENCDE,
            BNFTLST,
            STRTDT,
            ENDDT,
            DEDUCTYPE,
            NOVISITS,
            TRANNO,
            USRPRF,
            JOBNM,
			OPTBENEFIT,
            DATIME
       FROM VM1DTA.BNFHPF

GO

/*CREATE VIEW RSLL*/

CREATE VIEW [VM1DTA].[RSLL](UNIQUE_NUMBER, CHDRCOY, CHDRNO, VFLAG, CNTTYP, MBRNO, 
DPNTNO, CLNTNUM, SUBSNUM, STDATE, PRODTYP, REOVANLIM, STLOANLIM01, STLOANLIM02, 
STLOANLIM03, STLOANLIM04, STLOANLIM05, GDIAGCD01, GDIAGCD02, GDIAGCD03, GDIAGCD04, GDIAGCD05, USRPRF, JOBNM, DATIME) AS
SELECT [UNIQUE_NUMBER]
      ,[CHDRCOY]
      ,[CHDRNO]
      ,[VFLAG]
      ,[CNTTYP]
      ,[MBRNO]
      ,[DPNTNO]
      ,[CLNTNUM]
      ,[SUBSNUM]
      ,[STDATE]
      ,[PRODTYP]
      ,[REOVANLIM]
      ,[STLOANLIM01]
      ,[STLOANLIM02]
      ,[STLOANLIM03]
      ,[STLOANLIM04]
      ,[STLOANLIM05]
      ,[GDIAGCD01]
      ,[GDIAGCD02]
      ,[GDIAGCD03]
      ,[GDIAGCD04]
      ,[GDIAGCD05]
      ,[USRPRF]
      ,[JOBNM]
      ,[DATIME]
  FROM [VM1DTA].[RSLLPF]

GO

/*CREATE VIEW ADLM*/

CREATE VIEW [VM1DTA].[ADLM](UNIQUE_NUMBER,CHDRCOY,CHDRNO,CNTTYP,MBRNO,DPNTNO,CLNTNUM,SUBSNUM,CLMNO,GCOCCNO,CCDATE,CRDATE,REOVANLIM,STLOANLIM01,STLOANLIM02,STLOANLIM03,STLOANLIM04,STLOANLIM05,GDIAGCD01,GDIAGCD02,GDIAGCD03,GDIAGCD04,GDIAGCD05,AMTPAY01,AMTPAY02,AMTPAY03,AMTPAY04,AMTPAY05,BALANCE,BALANCE01,BALANCE02,BALANCE03,BALANCE04,BALANCE05,USRPRF,JOBNM,DATIME) 
AS
SELECT [UNIQUE_NUMBER]
      ,[CHDRCOY]
      ,[CHDRNO]
      ,[CNTTYP]
      ,[MBRNO]
      ,[DPNTNO]
      ,[CLNTNUM]
      ,[SUBSNUM]
      ,[CLMNO]
      ,[GCOCCNO]
      ,[CCDATE]
      ,[CRDATE]
	  ,[REOVANLIM]
      ,[STLOANLIM01]
      ,[STLOANLIM02]
      ,[STLOANLIM03]
      ,[STLOANLIM04]
      ,[STLOANLIM05]
      ,[GDIAGCD01]
      ,[GDIAGCD02]
      ,[GDIAGCD03]
      ,[GDIAGCD04]
      ,[GDIAGCD05]
      ,[AMTPAY01]
      ,[AMTPAY02]
      ,[AMTPAY03]
      ,[AMTPAY04]
      ,[AMTPAY05]
      ,[BALANCE]
      ,[BALANCE01]
      ,[BALANCE02]
      ,[BALANCE03]
      ,[BALANCE04]
      ,[BALANCE05]
      ,[USRPRF]
      ,[JOBNM]
      ,[DATIME]
  FROM [VM1DTA].[ADLMPF]
GO
/*CREATE VIEW OPBN*/

CREATE VIEW [VM1DTA].[OPBN] (UNIQUE_NUMBER,CHDRCOY,CHDRNO,VFLAG,CNTTYP,MBRNO,DPNTNO,CLNTNUM,SUBSNUM,STDATE,PRODTYP,PLANNO,BENCDE,SIAMT,WEEKSIAMT,MINSIWEEK,PREMAMT,PREMRATE,PREMCALC,SFSTDATE,SFENDDATE,ACTION,TYPE,USRPRF,JOBNM,DATIME)
AS
SELECT [UNIQUE_NUMBER]
      ,[CHDRCOY]
      ,[CHDRNO]
      ,[VFLAG]
      ,[CNTTYP]
      ,[MBRNO]
      ,[DPNTNO]
      ,[CLNTNUM]
      ,[SUBSNUM]
      ,[STDATE]
      ,[PRODTYP]
      ,[PLANNO]
      ,[BENCDE]
      ,[SIAMT]
      ,[WEEKSIAMT]
      ,[MINSIWEEK]
      ,[PREMAMT]
      ,[PREMRATE]
      ,[PREMCALC]
	  ,[SFSTDATE] 
	  ,[SFENDDATE]
      ,[ACTION]
	  ,[TYPE]
      ,[USRPRF]
      ,[JOBNM]
      ,[DATIME]
  FROM [VM1DTA].[OPBNPF]
GO
/*CREATE VIEW DIWP*/

CREATE VIEW [VM1DTA].[DIWP] (UNIQUE_NUMBER,CHDRCOY,CHDRNO,DPNTNO,VFLAG,MBRNO,CLNTNUM,SUBSNUM,STDATE,GDIAGCD01,GDIAGCD02,GDIAGCD03,GDIAGCD04,GDIAGCD05,WAITPERI01,WAITPERI02,WAITPERI03,WAITPERI04,WAITPERI05,USRPRF,JOBNM,DATIME)
AS
SELECT [UNIQUE_NUMBER]
      ,[CHDRCOY]
      ,[CHDRNO]
	  ,[DPNTNO]
      ,[VFLAG]
      ,[MBRNO] 
      ,[CLNTNUM]
      ,[SUBSNUM]
      ,[STDATE]
      ,[GDIAGCD01]
      ,[GDIAGCD02]
      ,[GDIAGCD03]
      ,[GDIAGCD04]
      ,[GDIAGCD05]
      ,[WAITPERI01]
      ,[WAITPERI02]
      ,[WAITPERI03]
      ,[WAITPERI04]
      ,[WAITPERI05]
      ,[USRPRF]
      ,[JOBNM]
      ,[DATIME]
  FROM [VM1DTA].[DIWPPF]
GO

/* ERROR */

	INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFQ5'
           ,'Visit Reduce/Stop Loss Limits'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
GO 
INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFRY'
           ,'Adjust Limits'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
GO 

INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFRZ'
           ,'Invalid for Health Benefit'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
GO 
/*HELP TEXT*/
/*SQ931*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ931'
           ,''
           ,'1'
           ,''
           ,'1'
           ,'This screen is used to maintain '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ931'
           ,''
           ,'2'
           ,''
           ,'1'
           ,'the Reduced Overall Annual Limit '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ931'
           ,''
           ,'3'
           ,''
           ,'1'
           ,'and/or the Stop Loss Limit(s) '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ931'
           ,''
           ,'4'
           ,''
           ,'1'
           ,'of the selected member.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 


/*SQ932*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ932'
           ,''
           ,'1'
           ,''
           ,'1'
           ,'This screen will display the original '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ932'
           ,''
           ,'2'
           ,''
           ,'1'
           ,'Reduced Overall Annual Limit and/or  '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ932'
           ,''
           ,'3'
           ,''
           ,'1'
           ,'the Stop Loss Limit(s) and the respective '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ931'
           ,''
           ,'4'
           ,''
           ,'1'
           ,'balance of the claimant.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 


/*FIELD IN SQ931*/
/*Policy */
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'CHDRNO'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field which displays'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'CHDRNO'
           ,'2'
           ,''
           ,'1'
           ,'the Policy Number, Policy Type'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'CHDRNO'
           ,'3'
           ,''
           ,'1'
           ,'and Policy Type description.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
/*Member*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'MBRNO'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field for which displays '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'MBRNO'
           ,'2'
           ,''
           ,'1'
           ,'the Member No. and member�s name of the  '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'MBRNO'
           ,'3'
           ,''
           ,'1'
           ,'selected member.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
/*Subsidiary*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'SUBSNUM'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field which displays the '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'SUBSNUM'
           ,'2'
           ,''
           ,'1'
           ,'Subsidiary which the member is under (if any).'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
/*Start Date*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'CCDATE'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field which displays the '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'CCDATE'
           ,'2'
           ,''
           ,'1'
           ,'Start Date of the member. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
/*Reduced Overall Annual Limit*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'REOVANLIM'
           ,'1'
           ,''
           ,'1'
           ,'This field is for user to enter in the Reduced  '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'REOVANLIM'
           ,'2'
           ,''
           ,'1'
           ,'Overall Annual Limit of the member.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
/*Stop Loss Annual Limit*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'STLOANLIM'
           ,'1'
           ,''
           ,'1'
           ,'This field is for user to enter in the Stop'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'STLOANLIM'
           ,'2'
           ,''
           ,'1'
           ,'Loss Annual Limit of the member.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

/*Diagnosis Code*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'GDIAGCD'
           ,'1'
           ,''
           ,'1'
           ,'This field is for user to enter in the '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'GDIAGCD'
           ,'2'
           ,''
           ,'1'
           ,'Diagnosis Code against the respective Stop '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'GDIAGCD'
           ,'3'
           ,''
           ,'1'
           ,'Loss Annual Limit of the member. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'GDIAGCD'
           ,'4'
           ,''
           ,'1'
           ,'A search can be performed which will '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'GDIAGCD'
           ,'5'
           ,''
           ,'1'
           ,'display the Diagnosis Code T9697 search screen, SR9IY. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 


/*Field for SQ932*/
/*Policy */
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'CHDRNO'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field which displays'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'CHDRNO'
           ,'2'
           ,''
           ,'1'
           ,'the Policy Number, Policy Type'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'CHDRNO'
           ,'3'
           ,''
           ,'1'
           ,'and Policy Type description.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
/*Member*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'MBRNO'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field for which displays'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'MBRNO'
           ,'2'
           ,''
           ,'1'
           ,'the Member No. with the dependant no. and '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'MBRNO'
           ,'3'
           ,''
           ,'1'
           ,'member�s name of the selected member.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
/*Subsidiary*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'SUBSNUM'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field which displays the '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'SUBSNUM'
           ,'2'
           ,''
           ,'1'
           ,'Subsidiary which the member is under (if any).'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
/*Claim Number*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'CLMNO'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field which displays the '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'CLMNO'
           ,'2'
           ,''
           ,'1'
           ,'Claim Number of the current claim. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
/*Reduced Overall Annual Limit*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'REOVANLIM'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field which displays  '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'REOVANLIM'
           ,'2'
           ,''
           ,'1'
           ,'Reduced Overall Annual Limit of the member.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
/*Stop Loss Annual Limit*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'STLOANLIM'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field which displays Stop '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'STLOANLIM'
           ,'2'
           ,''
           ,'1'
           ,'Loss Annual Limit of the member.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

/*Diagnosis Code*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'GDIAGCD'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field which displays the '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'GDIAGCD'
           ,'2'
           ,''
           ,'1'
           ,'Diagnosis Code and the description of the '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'GDIAGCD'
           ,'3'
           ,''
           ,'1'
           ,'respective Stop Loss Annual Limit.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
/*Balance*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'BALANCE'
           ,'1'
           ,''
           ,'1'
           ,'This field will display the balance of the'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'BALANCE'
           ,'2'
           ,''
           ,'1'
           ,'Reduced Overall Annual Limit/ Stop Loss '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'BALANCE'
           ,'3'
           ,''
           ,'1'
           ,'Annual Limit based on the previous Approved Claim.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

/*Field for SQ934*/
/*Policy */
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'CHDRNO'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field which displays'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'CHDRNO'
           ,'2'
           ,''
           ,'1'
           ,'the Policy Number, Policy Type'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'CHDRNO'
           ,'3'
           ,''
           ,'1'
           ,'and Policy Type description.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
/*Member*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'MBRNO'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field for which displays'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'MBRNO'
           ,'2'
           ,''
           ,'1'
           ,'the Member No. with the dependant no. and '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'MBRNO'
           ,'3'
           ,''
           ,'1'
           ,'member�s name of the selected member.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
/*Subsidiary*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'SUBSNUM'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field which displays the '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'SUBSNUM'
           ,'2'
           ,''
           ,'1'
           ,'Subsidiary which the member is under (if any).'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
/*Start Date*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'STDATE'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field which displays the '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'CCDATE'
           ,'2'
           ,''
           ,'1'
           ,'Start Date of the member.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
/*Diagnosis Code*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'GDIAGCD'
           ,'1'
           ,''
           ,'1'
           ,'This field is for user to enter in the  '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'GDIAGCD'
           ,'2'
           ,''
           ,'1'
           ,'Diagnosis Code against the respective Stop'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'GDIAGCD'
           ,'3'
           ,''
           ,'1'
           ,'Loss Annual Limit of the member. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'GDIAGCD'
           ,'4'
           ,''
           ,'1'
           ,'A search can be performed which will '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'GDIAGCD'
           ,'5'
           ,''
           ,'1'
           ,'display the Diagnosis Code T9697 search screen, SR9IY. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO
/*Waiting Period*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'WAITPERI'
           ,'1'
           ,''
           ,'1'
           ,'Waiting Period in number of days to be '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'WAITPERI'
           ,'2'
           ,''
           ,'1'
           ,'entered for the respective Diagnosis Code.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

