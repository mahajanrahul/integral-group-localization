﻿/****** Object:  UserDefinedFunction [VM1DTA].[TranslateKana]    Script Date: 01-Mar-16 11:12:53 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [VM1DTA].[TranslateKana]
(
@vIN VARCHAR(60)
) RETURNS VARCHAR(60)
AS BEGIN 
  DECLARE @vOUT VARCHAR(60)
  Select @vOUT = vm1dta.TRANSLATE(@vIN,
  'ｱｲｳｴｵｶｷｸｹｺｻｼｽｾｿﾀﾁﾂﾃﾄﾅﾆﾇﾈﾉﾊﾋﾌﾍﾎﾏﾐﾑﾒﾓﾔﾕﾖﾗﾘﾙﾚﾛﾜｦﾝｧｨｩｪｫｯｬｭｮABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ﾟﾞ｡､ｰ1234567890!"#$%&()=-~^|\[]@*+;:./?<>,_{}｢｣''`･',
  'ｱｲｳｴｵｶｷｸｹｺｻｼｽｾｿﾀﾁﾂﾃﾄﾅﾆﾇﾈﾉﾊﾋﾌﾍﾎﾏﾐﾑﾒﾓﾔﾕﾖﾗﾘﾙﾚﾛﾜｦﾝｱｲｳｴｵﾂﾔﾕﾖABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ');

  if Len(@vOUT)>60 
  BEGIN
     Select @vOUT=SUBSTRING(@vOUT,1,60)
  end 

  RETURN @vOUT

END
GO


