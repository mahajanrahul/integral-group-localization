/* ERROR */
	INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFS1'
           ,'Total Claim Auth exceeded'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
	

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'Sq945'
           ,''
           ,''
           ,''
           ,'1'
           ,'Additional Benefits'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO


/*GCMHPF*/
/*TABLE*/
/*ALTER TABLE*/
ALTER TABLE [VM1DTA].[GCMHPF] ADD 
DIACODE NCHAR(8) NULL,
ONDUTY NCHAR(1) NULL
GO

ALTER VIEW [VM1DTA].[GCMH](UNIQUE_NUMBER, CHDRCOY, CLAMNUM, GCOCCNO, GCCLMSEQ, CHDRNUM, PRODTYP, GCCLMTNO, GCDPNTNO, CLNTPFX, CLNTCOY, CLNTNUM, GCSTS, DTECLAM, CLAIMCUR, CRATE, PLANNO, FMLYCDE, GCRMK, BNKCHARGE, SPECTRM, GCDBLIND, GCFRPDTE, GCLRPDTE, GCDTHCLM, GCCAUSCD, GCOPPVCD, GCOPBNCD, GCOPIAMT, GCOPBAMT, GCOPBNPY, GCOPDGCD, GCOPPTCD, REQNBCDE, REQNTYPE, GCAUTHBY, DATEAUTH, GCOPRSCD, GCGRSPY, GCNETPY, GCCOINA, DEDUCTPC, GCADVPY, GCTRDRPY, GCLOCCNO, MMFLG, PACCAMT, OVRLMT, DTEPYMTADV, DTEPYMTOTH, DTEATT, DTETRM, REVLINK, REVIND, TERMID, USER_T, TRDT, TRTM, APAIDAMT, INTPAIDAMT, GCSETLMD, GCSECSTS, ZCLTCLMREF, ZCLMRECD, ZCLNTPROC, ZCLMREF, ZFEECURR, ZFEETYP, ZCALCFEE, ZOVRDFEE, GRSKCLS, BENCDE, CLAIMCOND, OUTLOAN, USRPRF, JOBNM, DATIME, DIACODE, ONDUTY) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CLAMNUM,
            GCOCCNO,
            GCCLMSEQ,
            CHDRNUM,
            PRODTYP,
            GCCLMTNO,
            GCDPNTNO,
            CLNTPFX,
            CLNTCOY,
            CLNTNUM,
            GCSTS,
            DTECLAM,
            CLAIMCUR,
            CRATE,
            PLANNO,
            FMLYCDE,
            GCRMK,
            BNKCHARGE,
            SPECTRM,
            GCDBLIND,
            GCFRPDTE,
            GCLRPDTE,
            GCDTHCLM,
            GCCAUSCD,
            GCOPPVCD,
            GCOPBNCD,
            GCOPIAMT,
            GCOPBAMT,
            GCOPBNPY,
            GCOPDGCD,
            GCOPPTCD,
            REQNBCDE,
            REQNTYPE,
            GCAUTHBY,
            DATEAUTH,
            GCOPRSCD,
            GCGRSPY,
            GCNETPY,
            GCCOINA,
            DEDUCTPC,
            GCADVPY,
            GCTRDRPY,
            GCLOCCNO,
            MMFLG,
            PACCAMT,
            OVRLMT,
            DTEPYMTADV,
            DTEPYMTOTH,
            DTEATT,
            DTETRM,
            REVLINK,
            REVIND,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            APAIDAMT,
            INTPAIDAMT,
            GCSETLMD,
            GCSECSTS,
            ZCLTCLMREF,
            ZCLMRECD,
            ZCLNTPROC,
            ZCLMREF,
            ZFEECURR,
            ZFEETYP,
            ZCALCFEE,
            ZOVRDFEE,
            GRSKCLS,
            BENCDE,
            CLAIMCOND,
            OUTLOAN,
            USRPRF,
            JOBNM,
            DATIME,
			DIACODE,
			ONDUTY

       FROM VM1DTA.GCMHPF
GO


/*New Screen */
 INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
('HP','3','E','S','SQ945','','1','','1','Additional Benefits','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP)
GO

/*New Field*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
('HP',' ','E','F','S9352','onduty',1,'',1,'indicate the claim occurred during on or off duty hours','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','S9352','diacode',1,'','1','The diagnosis code for claim','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ945','clamnum',1,'','1','Group claim number.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ945','chdrnum',1,'','1','Policy number.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ945','gcmbname',1,'','1','The name of the claimant','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ945','prodtyp',1,'','1','This is the Product code. ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ945','planno',1,'','1','This is the plan number field.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ945','bencde',1,'','1','This is the benefit code.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ945','itmshdes',1,'','1','The description of the benefit code.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ945','claimcond',1,'','1','This is the claim condition code. ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ945','zuminsu',1,'','1','The sum insured of the in policy. ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ945','prcnt',1,'','1','The percentage % of the benefit.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ945','datestart',1,'','1','start date of temporary disablement peri.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ945','dateend',1,'','1','end date of temporary disablement perid.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ945','zgcbenpy',1,'','1','Amount payable for the benefit.  ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ945','apaidamt',1,'','1','Claim amounthas been aid previously','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ945','totpre',1,'','1','Outstanding To Be Paid.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ945','minutfrom',1,'',1,'This field is used to capture Time From','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ945','minutto',1,'',1,'This field is used to capture Time To','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ945','timetyp1',1,'',1,'This field is used to capture Time From','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ945','timetyp2',1,'',1,'This field is used to capture Time To','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP)
GO

