INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,'          '
           ,'RFRY'
           ,'Issue Trial Bill not yet run                                                                                                                                                                                                                              '
           ,0
           ,0
           ,'000036    '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,getDate()
           ,'      ')


		   INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,'          '
           ,'RFRV'
           ,'GST Set up required                                                                                                                                                                                                                              '
           ,0
           ,0
           ,'000036    '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,getDate()
           ,'      ')

--Add columns to table GBIHPF
ALTER TABLE [VM1DTA].GBIHPF ADD
	ZGSTCOM	[numeric](9, 2) ,
	ZGSTAFEE [numeric](9, 2) 
GO
CREATE TABLE [VM1DTA].[GBITPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[SUBSCOY] [nchar](1) NULL,
	[SUBSNUM] [nchar](8) NULL,
	[BILLTYP] [nchar](1) NULL,
	CCDATE [int] NULL,
	CRDATE [int] NULL,
	BILLCURR [nchar](3) NULL,
	AGTNUM [nchar](8) NULL,
	PPREM  NUMERIC(15, 2) ,
	ZGSTAFEE NUMERIC(9, 2) ,
	DISCAMT1  NUMERIC(11, 2) ,
	DISCAMT2  NUMERIC(11, 2) ,
	ADMINFEE NUMERIC(9, 2) ,
	ZGSTAMT NUMERIC(8, 2) ,
	GRPSDUTY [numeric](9, 2) ,
	ZCOMM1 [numeric](9, 2) ,
	[ZGSTCOM] [numeric](9, 2) ,
	ZCOMM2 [numeric](9, 2) 
) ON [PRIMARY]
GO
CREATE VIEW  [VM1DTA].[GBIT]( [UNIQUE_NUMBER]
      ,[CHDRCOY]
      ,[CHDRNUM]
      ,[SUBSCOY]
      ,[SUBSNUM]
      ,[BILLTYP]
      ,[CCDATE]
      ,[CRDATE]
      ,[BILLCURR]
      ,[AGTNUM]
      ,[PPREM]
      ,[ZGSTAFEE]
      ,[DISCAMT1]
      ,[DISCAMT2]
      ,[ADMINFEE]
      ,[ZGSTAMT]
      ,[GRPSDUTY]
      ,[ZCOMM1]
      ,[ZGSTCOM]
      ,[ZCOMM2])
AS 
SELECT [UNIQUE_NUMBER]
	  ,[CHDRCOY]
      ,[CHDRNUM]
      ,[SUBSCOY]
      ,[SUBSNUM]
      ,[BILLTYP]
      ,[CCDATE]
      ,[CRDATE]
      ,[BILLCURR]
      ,[AGTNUM]
      ,[PPREM]
      ,[ZGSTAFEE]
      ,[DISCAMT1]
      ,[DISCAMT2]
      ,[ADMINFEE]
      ,[ZGSTAMT]
      ,[GRPSDUTY]
      ,[ZCOMM1]
      ,[ZGSTCOM]
      ,[ZCOMM2]
  FROM [VM1DTA].[GBITPF]
GO
GO
ALTER  VIEW [VM1DTA].[GBIH](UNIQUE_NUMBER, BILLNO, CHDRCOY, CHDRNUM, SUBSCOY, SUBSNUM, MBRNO, BILLTYP, PRBILFDT, PRBILTDT, INSTNO, PBILLNO, TERMID, USER_T, TRDT, TRTM, TRANNO, GRPGST, GRPSDUTY, VALIDFLAG, BILFLAG, RDOCPFX, RDOCCOY, RDOCNUM, NRFLG, TGTPCNT, PREMOUT, BILLDUEDT, REVFLAG, USRPRF, JOBNM, DATIME, ZGSTCOM, ZGSTAFEE) AS
SELECT UNIQUE_NUMBER,
            BILLNO,
            CHDRCOY,
            CHDRNUM,
            SUBSCOY,
            SUBSNUM,
            MBRNO,
            BILLTYP,
            PRBILFDT,
            PRBILTDT,
            INSTNO,
            PBILLNO,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            TRANNO,
            GRPGST,
            GRPSDUTY,
            VALIDFLAG,
            BILFLAG,
            RDOCPFX,
            RDOCCOY,
            RDOCNUM,
            NRFLG,
            TGTPCNT,
            PREMOUT,
            BILLDUEDT,
            REVFLAG,
            USRPRF,
            JOBNM,
            DATIME,
			ZGSTCOM,
			ZGSTAFEE
       FROM VM1DTA.GBIHPF
      WHERE VALIDFLAG = '1'

GO
CREATE TABLE [VM1DTA].[GGSTPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[DOCTPFX] [nchar](2) NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[PRODTYP] [nchar](4) NULL,
	[BILLNO] [nchar](9) NULL,
	[TRANCD] [nchar](4) NULL,
	[TRANNO] [int] NULL,
	[TAXINVNO] [nchar](24) NULL,
	[TAXINVDT] [int] NULL,
	[SACSCODE] [nchar](2) NULL,
	[SACSTYP01] [nchar](2) NULL,
	[SACSTYP02] [nchar](2) NULL,
	[SACSTYP03] [nchar](2) NULL,
	[SACSTYP04] [nchar](2) NULL,
	[SACSTYP05] [nchar](2) NULL,
	[SACSTYP06] [nchar](2) NULL,
	[SACSTYP07] [nchar](2) NULL,
	[SACSTYP08] [nchar](2) NULL,
	[SACSTYP09] [nchar](2) NULL,
	[SACSTYP10] [nchar](2) NULL,
	[SACSTYP11] [nchar](2) NULL,
	[SACSTYP12] [nchar](2) NULL,
	[SACSTYP13] [nchar](2) NULL,
	[SACSTYP14] [nchar](2) NULL,
	[SACSTYP15] [nchar](2) NULL,
	[AGNTACCT] [nchar](3) NULL,
	[TRANAMT01] [numeric](17, 2) NULL,
	[TRANAMT02] [numeric](17, 2) NULL,
	[TRANAMT03] [numeric](17, 2) NULL,
	[TRANAMT04] [numeric](17, 2) NULL,
	[TRANAMT05] [numeric](17, 2) NULL,
	[TRANAMT06] [numeric](17, 2) NULL,
	[TRANAMT07] [numeric](17, 2) NULL,
	[TRANAMT08] [numeric](17, 2) NULL,
	[TRANAMT09] [numeric](17, 2) NULL,
	[TRANAMT10] [numeric](17, 2) NULL,
	[TRANAMT11] [numeric](17, 2) NULL,
	[TRANAMT12] [numeric](17, 2) NULL,
	[TRANAMT13] [numeric](17, 2) NULL,
	[TRANAMT14] [numeric](17, 2) NULL,
	[TRANAMT15] [numeric](17, 2) NULL,
	[TRANAMT16] [numeric](17, 2) NULL,
	[TRANAMT17] [numeric](17, 2) NULL,
	[TRANAMT18] [numeric](17, 2) NULL,
	[TRANAMT19] [numeric](17, 2) NULL,
	[TRANAMT20] [numeric](17, 2) NULL,
	[TRANAMT21] [numeric](17, 2) NULL,
	[TRANAMT22] [numeric](17, 2) NULL,
	[TRANAMT23] [numeric](17, 2) NULL,
	[POLTYPE] [nchar](3) NULL,
	[TAXCODE01] [nchar](6) NULL,
	[TAXCODE02] [nchar](6) NULL,
	[TAXCODE03] [nchar](6) NULL,
	[TAXCODE04] [nchar](6) NULL,
	[TAXCODE05] [nchar](6) NULL,
	[TAXCODE06] [nchar](6) NULL,
	[TAXCODE07] [nchar](6) NULL,
	[TAXCODE08] [nchar](6) NULL,
	[TAXCODE09] [nchar](6) NULL,
	[TAXCODE10] [nchar](6) NULL,
	[TAXCODE11] [nchar](6) NULL,
	[TAXCODE12] [nchar](6) NULL,
	[TAXCODE13] [nchar](6) NULL,
	[TAXCODE14] [nchar](6) NULL,
	[TAXCODE15] [nchar](6) NULL,
	[BATCACTYR] [int] NULL,
	[BATCACTMN] [int] NULL,
	[ACCTCURR] [nchar](3) NULL,
	[CRATE] [numeric](18, 9) NULL,
	[SUBSNO] [nchar](8) NULL,
	[SUBSNAME] [nchar](47) NULL,
	[PROCIND] [nchar](1) NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL
) ON [PRIMARY]

GO

/****** Object:  View [VM1DTA].[GGST]    Script Date: 6/6/2016 10:00:20 AM ******/

CREATE VIEW [VM1DTA].[GGST]
(UNIQUE_NUMBER,DOCTPFX,CHDRCOY,CHDRNUM,PRODTYP,BILLNO,TRANCD,TRANNO,TAXINVNO,TAXINVDT,SACSCODE,SACSTYP01,SACSTYP02,SACSTYP03,SACSTYP04,SACSTYP05,SACSTYP06,SACSTYP07,SACSTYP08,SACSTYP09,SACSTYP10,SACSTYP11,SACSTYP12,SACSTYP13,SACSTYP14,SACSTYP15,AGNTACCT,TRANAMT01,TRANAMT02,TRANAMT03,TRANAMT04,TRANAMT05,TRANAMT06,TRANAMT07,TRANAMT08,TRANAMT09,TRANAMT10,TRANAMT11,TRANAMT12,TRANAMT13,TRANAMT14,TRANAMT15,TRANAMT16,TRANAMT17,TRANAMT18,TRANAMT19,TRANAMT20,TRANAMT21,TRANAMT22,TRANAMT23,POLTYPE,TAXCODE01,TAXCODE02,TAXCODE03,TAXCODE04,TAXCODE05,TAXCODE06,TAXCODE07,TAXCODE08,TAXCODE09,TAXCODE10,TAXCODE11,TAXCODE12,TAXCODE13,TAXCODE14,TAXCODE15,BATCACTYR,BATCACTMN,ACCTCURR,CRATE,SUBSNO,SUBSNAME,PROCIND,USRPRF,JOBNM,DATIME) AS
SELECT UNIQUE_NUMBER,
DOCTPFX,
CHDRCOY,
CHDRNUM,
PRODTYP,
BILLNO,
TRANCD,
TRANNO,
TAXINVNO,
TAXINVDT,
SACSCODE,
SACSTYP01,
SACSTYP02,
SACSTYP03,
SACSTYP04,
SACSTYP05,
SACSTYP06,
SACSTYP07,
SACSTYP08,
SACSTYP09,
SACSTYP10,
SACSTYP11,
SACSTYP12,
SACSTYP13,
SACSTYP14,
SACSTYP15,
AGNTACCT,
TRANAMT01,
TRANAMT02,
TRANAMT03,
TRANAMT04,
TRANAMT05,
TRANAMT06,
TRANAMT07,
TRANAMT08,
TRANAMT09,
TRANAMT10,
TRANAMT11,
TRANAMT12,
TRANAMT13,
TRANAMT14,
TRANAMT15,
TRANAMT16,
TRANAMT17,
TRANAMT18,
TRANAMT19,
TRANAMT20,
TRANAMT21,
TRANAMT22,
TRANAMT23,
POLTYPE,
TAXCODE01,
TAXCODE02,
TAXCODE03,
TAXCODE04,
TAXCODE05,
TAXCODE06,
TAXCODE07,
TAXCODE08,
TAXCODE09,
TAXCODE10,
TAXCODE11,
TAXCODE12,
TAXCODE13,
TAXCODE14,
TAXCODE15,
BATCACTYR,
BATCACTMN,
ACCTCURR,
CRATE,
SUBSNO,
SUBSNAME,
PROCIND,
USRPRF,
JOBNM,
DATIME		
FROM VM1DTA.GGSTPF
WHERE PROCIND = '0'


GO


/****** Object:  Table [VM1DTA].[GSTIPF]    Script Date: 6/6/2016 10:07:05 AM ******/

CREATE TABLE [VM1DTA].[GSTIPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[DOCTPFX] [nchar](2) NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[BILLNO] [nchar](9) NULL,
	[TRANCD] [nchar](4) NULL,
	[TRANNO] [int] NULL,
	[TAXINVNO] [nchar](24) NULL,
	[TAXINVDT] [int] NULL,
	[GSTREG] [nchar](15) NULL,
	[CLNTNUM] [nchar](8) NULL,
	[CLNTNAME] [nchar](47) NULL,
	[CLTADDR1] [nchar](30) NULL,
	[CLTADDR2] [nchar](30) NULL,
	[CLTADDR3] [nchar](30) NULL,
	[CLTADDR4] [nchar](30) NULL,
	[CLTADDR5] [nchar](30) NULL,
	[Z6GSTRGN] [nchar](12) NULL,
	[AGNTNUM] [nchar](8) NULL,
	[AGNTNAME] [nchar](47) NULL,
	[POLTYPE] [nchar](3) NULL,
	[POLDESC] [nchar](30) NULL,
	[CCDATE] [int] NULL,
	[CRDATE] [int] NULL,
	[BILCURR] [nchar](3) NULL,
	[BILCURRD] [nchar](30) NULL,
	[GROSPREM] [numeric](17, 2) NULL,
	[DICAMT] [numeric](17, 2) NULL,
	[ZDISPCT] [numeric](4, 2) NULL,
	[TRNSFEE] [numeric](17, 2) NULL,
	[BNKCHARGE] [numeric](17, 2) NULL,
	[SERVCHRG] [numeric](17, 2) NULL,
	[MONENQFEE] [numeric](17, 2) NULL,
	[TPCAFEE] [numeric](17, 2) NULL,
	[TOTEXGST] [numeric](17, 2) NULL,
	[GSTAMT] [numeric](17, 2) NULL,
	[GSTRATE] [numeric](4, 2) NULL,
	[STMDUTY] [numeric](17, 2) NULL,
	[TOAMTPY] [numeric](17, 2) NULL,
	[TAXCODE] [nchar](6) NULL,
	[TIDBCRNO] [nchar](24) NULL,
	[DBCRNO] [nchar](22) NULL,
	[DBCRPOLNO] [nchar](25) NULL,
	[DBCRDT] [int] NULL,
	[ENDNUM] [nchar](25) NULL,
	[EFFDATE] [int] NULL,
	[RSNDESC] [nchar](50) NULL,
	[COMMAMT] [numeric](17, 2) NULL,
	[COMMPER] [numeric](5, 2) NULL,
	[SERNAME] [nchar](15) NULL,
	[PROCIND] [nchar](1) NULL,
	[ZREFNUM] [nchar](20) NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL,
	[AGNTACCT] [nchar](3) NULL,
	[ACCNO] [nchar](8) NULL,
	[ORGTINO] [nchar](24) NULL,
	[ORGTIDT] [int] NULL,
	[CEDCLNT] [nchar](8) NULL,
	[CEDPOLNO] [nchar](8) NULL,
	[CEDCESSNO] [nchar](20) NULL,
	[ZINSNAME] [nchar](80) NULL,
	[ORGTOTSI] [numeric](15, 2) NULL,
	[RISIRATE] [numeric](4, 2) NULL,
	[RISIAMT] [numeric](15, 2) NULL,
	[CRATE] [numeric](18, 9) NULL,
	[RICOMRATE] [numeric](5, 2) NULL,
	[RICOMAMT] [numeric](17, 2) NULL,
	[GSTCOMRT] [numeric](4, 2) NULL,
	[GSTCOMAMT] [numeric](17, 2) NULL
) ON [PRIMARY]

GO

/****** Object:  View [VM1DTA].[GSTI]    Script Date: 6/6/2016 10:07:36 AM ******/

CREATE VIEW [VM1DTA].[GSTI]
(UNIQUE_NUMBER,DOCTPFX,CHDRCOY,CHDRNUM,BILLNO,TRANCD,TRANNO,TAXINVNO,TAXINVDT,GSTREG,CLNTNUM,CLNTNAME,CLTADDR1,CLTADDR2,CLTADDR3,CLTADDR4,CLTADDR5,Z6GSTRGN,AGNTNUM,AGNTNAME,POLTYPE,POLDESC,CCDATE,CRDATE,BILCURR,BILCURRD,GROSPREM,DICAMT,ZDISPCT,TRNSFEE,BNKCHARGE,SERVCHRG,MONENQFEE,TPCAFEE,TOTEXGST,GSTAMT,GSTRATE,STMDUTY,TOAMTPY,TAXCODE,TIDBCRNO,DBCRNO,DBCRPOLNO,DBCRDT,ENDNUM,EFFDATE,RSNDESC,COMMAMT,COMMPER,SERNAME,PROCIND,ZREFNUM,USRPRF,JOBNM,DATIME) AS
SELECT UNIQUE_NUMBER,
DOCTPFX,
CHDRCOY,
CHDRNUM,
BILLNO,
TRANCD,
TRANNO,
TAXINVNO,
TAXINVDT,
GSTREG,
CLNTNUM,
CLNTNAME,
CLTADDR1,
CLTADDR2,
CLTADDR3,
CLTADDR4,
CLTADDR5,
Z6GSTRGN,
AGNTNUM,
AGNTNAME,
POLTYPE,
POLDESC,
CCDATE,
CRDATE,
BILCURR,
BILCURRD,
GROSPREM,
DICAMT,
ZDISPCT,
TRNSFEE,
BNKCHARGE,
SERVCHRG,
MONENQFEE,
TPCAFEE,
TOTEXGST,
GSTAMT,
GSTRATE,
STMDUTY,
TOAMTPY,
TAXCODE,
TIDBCRNO,
DBCRNO,
DBCRPOLNO,
DBCRDT,
ENDNUM,
EFFDATE,
RSNDESC,
COMMAMT,
COMMPER,
SERNAME,
PROCIND,
ZREFNUM,
USRPRF,
JOBNM,
DATIME

FROM VM1DTA.GSTIPF
WHERE PROCIND='0'


GO