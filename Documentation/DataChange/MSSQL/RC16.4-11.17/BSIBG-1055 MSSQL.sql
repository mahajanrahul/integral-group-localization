
DROP VIEW [VM1DTA].[GCHIASC]
GO

/****** Object:  View [VM1DTA].[GCHIASC]    Script Date: 9/6/2016 6:30:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [VM1DTA].[GCHIASC]
(UNIQUE_NUMBER, CHDRCOY, CHDRNUM, EFFDATE, CCDATE, CRDATE, PRVBILFLG, BILLFREQ, GADJFREQ, PAYRPFX, PAYRCOY, PAYRNUM, AGNTPFX, AGNTCOY, AGNTNUM, CNTBRANCH, STCA, STCB, STCC, STCD, STCE, BTDATENR, NRISDATE, CRATE, 
             TERNMPRM, SURGSCHMV, AREACDEMV, MEDPRVDR, SPSMBR, CHILDMBR, SPSMED, CHILDMED, TERMID, USER_T, TRDT, TRTM, TRANNO, BANKCODE, BILLCHNL, MANDREF, RIMTHVCD, PRMRVWDT, APPLTYP, RIIND, POLBREAK, CFTYPE, LMTDRL, CFLIMIT, 
             NOFCLAIM, TPA, WKLADRT, WKLCMRT, NOFMBR, USRPRF, JOBNM, DATIME, TIMECH01, TIMECH02, ECNV, CVNTYPE, COVERNT, DOCRCDTE)
 AS
SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, EFFDATE, CCDATE, CRDATE, PRVBILFLG, BILLFREQ, GADJFREQ, PAYRPFX, PAYRCOY, PAYRNUM, AGNTPFX, AGNTCOY, AGNTNUM, CNTBRANCH, STCA, STCB, STCC, STCD, STCE, BTDATENR, NRISDATE, CRATE, 
             TERNMPRM, SURGSCHMV, AREACDEMV, MEDPRVDR, SPSMBR, CHILDMBR, SPSMED, CHILDMED, TERMID, USER_T, TRDT, TRTM, TRANNO, BANKCODE, BILLCHNL, MANDREF, RIMTHVCD, PRMRVWDT, APPLTYP, RIIND, POLBREAK, CFTYPE, LMTDRL, CFLIMIT, 
             NOFCLAIM, TPA, WKLADRT, WKLCMRT, NOFMBR, USRPRF, JOBNM, DATIME, TIMECH01, TIMECH02, ECNV, CVNTYPE, COVERNT, DOCRCDTE
FROM   VM1DTA.GCHIPF
GO

DROP VIEW [VM1DTA].[GCHIRNL]
GO

/****** Object:  View [VM1DTA].[GCHIRNL]    Script Date: 9/6/2016 6:30:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [VM1DTA].[GCHIRNL]
(UNIQUE_NUMBER, CHDRCOY, CHDRNUM, EFFDATE, CCDATE, CRDATE, PRVBILFLG, BILLFREQ, GADJFREQ, PAYRPFX, PAYRCOY, PAYRNUM, AGNTPFX, AGNTCOY, AGNTNUM, CNTBRANCH, STCA, STCB, STCC, STCD, STCE, BTDATENR, NRISDATE, CRATE, 
             TERNMPRM, SURGSCHMV, AREACDEMV, MEDPRVDR, SPSMBR, CHILDMBR, SPSMED, CHILDMED, TERMID, USER_T, TRDT, TRTM, TRANNO, BANKCODE, BILLCHNL, MANDREF, RIMTHVCD, PRMRVWDT, APPLTYP, RIIND, POLBREAK, CFTYPE, LMTDRL, CFLIMIT, 
             NOFCLAIM, TPA, WKLADRT, WKLCMRT, NOFMBR, USRPRF, JOBNM, DATIME, TIMECH01, TIMECH02, ECNV, CVNTYPE, COVERNT, DOCRCDTE)
 AS
SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, EFFDATE, CCDATE, CRDATE, PRVBILFLG, BILLFREQ, GADJFREQ, PAYRPFX, PAYRCOY, PAYRNUM, AGNTPFX, AGNTCOY, AGNTNUM, CNTBRANCH, STCA, STCB, STCC, STCD, STCE, BTDATENR, NRISDATE, CRATE, 
             TERNMPRM, SURGSCHMV, AREACDEMV, MEDPRVDR, SPSMBR, CHILDMBR, SPSMED, CHILDMED, TERMID, USER_T, TRDT, TRTM, TRANNO, BANKCODE, BILLCHNL, MANDREF, RIMTHVCD, PRMRVWDT, APPLTYP, RIIND, POLBREAK, CFTYPE, LMTDRL, CFLIMIT, 
             NOFCLAIM, TPA, WKLADRT, WKLCMRT, NOFMBR, USRPRF, JOBNM, DATIME, TIMECH01, TIMECH02, ECNV, CVNTYPE, COVERNT, DOCRCDTE
FROM   VM1DTA.GCHIPF
GO

DROP VIEW [VM1DTA].[GCHITPA]
GO

/****** Object:  View [VM1DTA].[GCHITPA]    Script Date: 9/6/2016 6:30:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [VM1DTA].[GCHITPA]
(UNIQUE_NUMBER, CHDRCOY, CHDRNUM, EFFDATE, CCDATE, CRDATE, PRVBILFLG, BILLFREQ, GADJFREQ, PAYRPFX, PAYRCOY, PAYRNUM, AGNTPFX, AGNTCOY, AGNTNUM, CNTBRANCH, STCA, STCB, STCC, STCD, STCE, BTDATENR, NRISDATE, CRATE, 
             TERNMPRM, SURGSCHMV, AREACDEMV, MEDPRVDR, SPSMBR, CHILDMBR, SPSMED, CHILDMED, TERMID, USER_T, TRDT, TRTM, TRANNO, BANKCODE, BILLCHNL, MANDREF, RIMTHVCD, PRMRVWDT, APPLTYP, RIIND, POLBREAK, CFTYPE, LMTDRL, CFLIMIT, 
             NOFCLAIM, TPA, WKLADRT, WKLCMRT, NOFMBR, USRPRF, JOBNM, DATIME, TIMECH01, TIMECH02, ECNV, CVNTYPE, COVERNT, DOCRCDTE)
 AS
SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, EFFDATE, CCDATE, CRDATE, PRVBILFLG, BILLFREQ, GADJFREQ, PAYRPFX, PAYRCOY, PAYRNUM, AGNTPFX, AGNTCOY, AGNTNUM, CNTBRANCH, STCA, STCB, STCC, STCD, STCE, BTDATENR, NRISDATE, CRATE, 
             TERNMPRM, SURGSCHMV, AREACDEMV, MEDPRVDR, SPSMBR, CHILDMBR, SPSMED, CHILDMED, TERMID, USER_T, TRDT, TRTM, TRANNO, BANKCODE, BILLCHNL, MANDREF, RIMTHVCD, PRMRVWDT, APPLTYP, RIIND, POLBREAK, CFTYPE, LMTDRL, CFLIMIT, 
             NOFCLAIM, TPA, WKLADRT, WKLCMRT, NOFMBR, USRPRF, JOBNM, DATIME, TIMECH01, TIMECH02, ECNV, CVNTYPE, COVERNT, DOCRCDTE
FROM   VM1DTA.GCHIPF
WHERE (TPA <> '')
GO

DROP VIEW [VM1DTA].[GCHITRN]
GO

/****** Object:  View [VM1DTA].[GCHITRN]    Script Date: 9/6/2016 6:30:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [VM1DTA].[GCHITRN]
(UNIQUE_NUMBER, CHDRCOY, CHDRNUM, EFFDATE, CCDATE, CRDATE, PRVBILFLG, BILLFREQ, GADJFREQ, PAYRPFX, PAYRCOY, PAYRNUM, AGNTPFX, AGNTCOY, AGNTNUM, CNTBRANCH, STCA, STCB, STCC, STCD, STCE, BTDATENR, NRISDATE, CRATE, 
             TERNMPRM, SURGSCHMV, AREACDEMV, MEDPRVDR, SPSMBR, CHILDMBR, SPSMED, CHILDMED, TERMID, USER_T, TRDT, TRTM, TRANNO, BANKCODE, BILLCHNL, MANDREF, RIMTHVCD, PRMRVWDT, APPLTYP, RIIND, POLBREAK, CFTYPE, LMTDRL, CFLIMIT, 
             NOFCLAIM, TPA, WKLADRT, WKLCMRT, NOFMBR, USRPRF, JOBNM, DATIME, TIMECH01, TIMECH02, ECNV, CVNTYPE, COVERNT, DOCRCDTE)
 AS
SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, EFFDATE, CCDATE, CRDATE, PRVBILFLG, BILLFREQ, GADJFREQ, PAYRPFX, PAYRCOY, PAYRNUM, AGNTPFX, AGNTCOY, AGNTNUM, CNTBRANCH, STCA, STCB, STCC, STCD, STCE, BTDATENR, NRISDATE, CRATE, 
             TERNMPRM, SURGSCHMV, AREACDEMV, MEDPRVDR, SPSMBR, CHILDMBR, SPSMED, CHILDMED, TERMID, USER_T, TRDT, TRTM, TRANNO, BANKCODE, BILLCHNL, MANDREF, RIMTHVCD, PRMRVWDT, APPLTYP, RIIND, POLBREAK, CFTYPE, LMTDRL, CFLIMIT, 
             NOFCLAIM, TPA, WKLADRT, WKLCMRT, NOFMBR, USRPRF, JOBNM, DATIME, TIMECH01, TIMECH02, ECNV, CVNTYPE, COVERNT, DOCRCDTE
FROM   VM1DTA.GCHIPF
GO

DROP VIEW [VM1DTA].[GCAG]
GO

/****** Object:  View [VM1DTA].[GCAG]    Script Date: 9/6/2016 6:30:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [VM1DTA].[GCAG]
(UNIQUE_NUMBER, CHDRCOY, CHDRNUM, EFFDATE, CCDATE, CRDATE, PRVBILFLG, BILLFREQ, GADJFREQ, PAYRPFX, PAYRCOY, PAYRNUM, AGNTPFX, AGNTCOY, AGNTNUM, CNTBRANCH, STCA, STCB, STCC, STCD, STCE, BTDATENR, NRISDATE, CRATE, 
             TERNMPRM, SURGSCHMV, AREACDEMV, MEDPRVDR, SPSMBR, CHILDMBR, SPSMED, CHILDMED, TERMID, USER_T, TRDT, TRTM, TRANNO, BANKCODE, BILLCHNL, MANDREF, RIMTHVCD, PRMRVWDT, APPLTYP, RIIND, POLBREAK, CFTYPE, LMTDRL, CFLIMIT, 
             NOFCLAIM, TPA, WKLADRT, WKLCMRT, NOFMBR, USRPRF, JOBNM, DATIME, TIMECH01, TIMECH02, ECNV, CVNTYPE, COVERNT, DOCRCDTE)
 AS
SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, EFFDATE, CCDATE, CRDATE, PRVBILFLG, BILLFREQ, GADJFREQ, PAYRPFX, PAYRCOY, PAYRNUM, AGNTPFX, AGNTCOY, AGNTNUM, CNTBRANCH, STCA, STCB, STCC, STCD, STCE, BTDATENR, NRISDATE, CRATE, 
             TERNMPRM, SURGSCHMV, AREACDEMV, MEDPRVDR, SPSMBR, CHILDMBR, SPSMED, CHILDMED, TERMID, USER_T, TRDT, TRTM, TRANNO, BANKCODE, BILLCHNL, MANDREF, RIMTHVCD, PRMRVWDT, APPLTYP, RIIND, POLBREAK, CFTYPE, LMTDRL, CFLIMIT, 
             NOFCLAIM, TPA, WKLADRT, WKLCMRT, NOFMBR, USRPRF, JOBNM, DATIME, TIMECH01, TIMECH02, ECNV, CVNTYPE, COVERNT, DOCRCDTE
FROM   VM1DTA.GCHIPF
GO
