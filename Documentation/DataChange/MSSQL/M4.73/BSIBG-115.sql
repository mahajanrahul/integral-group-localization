
/****** Object:  Table [VM1DTA].[MGFSPF]    ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [VM1DTA].[MGFSPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[RECIND] [nchar](1) NULL,
	[CORNAME] [nchar](50) NULL,
	[SECURITYNO] [nchar](15) NULL,
	[TRDATE] [nchar](10) NULL,
	[Z6GSTRGN][int] NULL,
	[Z6LINENO] [int] NULL,
	[PRODDESC] [nchar](30) NULL,
	[TRANAMT1] [numeric](17, 2) NULL,
	[TRANAMT2] [numeric](8, 2) NULL,
	[Z6TAXCDE] [nchar](6) NULL,
	[Z6CNTYCD] [nchar](50) NULL,
	[BILCURRD] [nchar](30) NULL,
	[ORIGAMT1] [numeric](17, 2) NULL,
	[ORIGAMT2] [numeric](8, 2) NULL,
	[DATIME] [datetime2](6) NULL
) ON [PRIMARY]

GO


/****** Object:  Table [VM1DTA].[MGFPPF]    ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [VM1DTA].[MGFPPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[RECIND] [nchar](1) NULL,
	[CORNAME] [nchar](50) NULL,
	[SECURITYNO] [nchar](15) NULL,
	[TRDATE] [nchar](10) NULL,
	[Z6GSTRGN][int] NULL,
	[ZREFNUM][int] NULL,
	[Z6LINENO] [int] NULL,
	[PRODDESC] [nchar](30) NULL,
	[TRANAMT1] [numeric](17, 2) NULL,
	[TRANAMT2] [numeric](8, 2) NULL,
	[Z6TAXCDE] [nchar](6) NULL,
	[Z6CNTYCD] [nchar](50) NULL,
	[BILCURRD] [nchar](30) NULL,
	[ORIGAMT1] [numeric](17, 2) NULL,
	[ORIGAMT2] [numeric](8, 2) NULL,
	[DATIME] [datetime2](6) NULL
) ON [PRIMARY]

GO

/****** Object:  Table [VM1DTA].[GTAXPF]    Script Date: 5/25/2016 5:46:13 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [VM1DTA].[GTAXPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[RECIND] [nchar](1) NULL,
	[CORNAME] [nchar](50) NULL,
	[SECURITYNO] [nchar](15) NULL,
	[TAXINVDT] [int] NULL,
	[TAXINVNO] [nchar](24) NULL,
	[Z6LINENO] [int] NULL,
	[PRODDESC] [nchar](30) NULL,
	[TRANAMT1] [numeric](17, 2) NULL,
	[TRANAMT2] [numeric](8, 2) NULL,
	[Z6TAXCDE] [nchar](6) NULL,
	[Z6CNTYCD] [nchar](50) NULL,
	[BILCURRD] [nchar](30) NULL,
	[ORIGAMT1] [numeric](17, 2) NULL,
	[ORIGAMT2] [numeric](8, 2) NULL,
	[ZREFNUM] [nchar](20) NULL,
	[BATCACTYR] [int] NULL,
	[BATCACTMN] [int] NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL
) ON [PRIMARY]

GO

/****** Object:  View [VM1DTA].[GTAXDTE]    ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [VM1DTA].[GTAXDTE]
(UNIQUE_NUMBER,RECIND,CORNAME,SECURITYNO,TAXINVDT,TAXINVNO,Z6LINENO,PRODDESC,TRANAMT1,TRANAMT2,Z6TAXCDE,Z6CNTYCD,BILCURRD,ORIGAMT1,ORIGAMT2,ZREFNUM,BATCACTYR,BATCACTMN,USRPRF,JOBNM,DATIME) AS
SELECT UNIQUE_NUMBER,
RECIND,
CORNAME,
SECURITYNO,
TAXINVDT,
TAXINVNO,
Z6LINENO,
PRODDESC,
TRANAMT1,
TRANAMT2,
Z6TAXCDE,
Z6CNTYCD,
BILCURRD,
ORIGAMT1,
ORIGAMT2,
ZREFNUM,
BATCACTYR,
BATCACTMN,
USRPRF,
JOBNM,
DATIME

FROM VM1DTA.GTAXPF

GO

