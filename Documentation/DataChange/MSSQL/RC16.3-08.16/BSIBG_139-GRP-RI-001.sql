/*ALTER TABLE*/
Alter table GCMHPF
ADD STLBASIS nchar(1) 
GO

Alter table GCLHPF
Add STLBASIS nchar(1)
Go


Alter table GBIDPF
Add RIBFEE [numeric](17,2) null,
	RIBFGST [numeric](17,2) null
GO

/*CREATE TABLE*/
------ GPMRPF --------
IF NOT EXISTS (select * from sysobjects where name = 'GPMRPF' and xtype = 'U')
CREATE TABLE [VM1DTA].[GPMRPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[SUBSCOY] [nchar](1) NULL,
	[SUBSNUM] [nchar](8) NULL,
	[PRODTYP] [nchar](4) NULL,
	[PLANNO] [nchar](3) NULL,
	[MBRNO] [nchar](5) NULL,
	[DPNTNO] [nchar](2) NULL,
	[HEADCNTIND] [nchar](1) NULL,
	[TRANNO] [int] NULL,
	[DTEEFF] [int] NULL,
	[DTETER] [int] NULL,
	[VFLAG] [nchar](1) NULL,
	[RPFX] [nchar](2) NULL,
	[RCOY] [nchar](1) NULL,
	[RACC] [nchar](8) NULL,
	[RITYPE] [nchar](2) NULL,
	[CEDTYP] [numeric](1, 0) NULL,
	[SEQNO] [int] NULL,
	[PRBILFDT] [int] NULL,
	[PRBILTDT] [int] NULL,
	[SACSCODE] [nchar](2) NULL,
	[SACSTYP01] [nchar](2) NULL,
	[SACSTYP02] [nchar](2) NULL,
	[SACSTYP03] [nchar](2) NULL,
	[SACSTYP04] [nchar](2) NULL,
	[SACSTYP05] [nchar](2) NULL,
	[SACSTYP06] [nchar](2) NULL,
	[SACSTYP07] [nchar](2) NULL,
	[SACSTYP08] [nchar](2) NULL,
	[SACSTYP09] [nchar](2) NULL,
	[SACSTYP10] [nchar](2) NULL,
	[SACSTYP11] [nchar](2) NULL,
	[SACSTYP12] [nchar](2) NULL,
	[SACSTYP13] [nchar](2) NULL,
	[SACSTYP14] [nchar](2) NULL,
	[SACSTYP15] [nchar](2) NULL,
	[CURR] [nchar](3) NULL,
	[CRATE] [numeric](18, 9) NULL,
	[EXTR01] [numeric](17, 2) NULL,
	[EXTR02] [numeric](17, 2) NULL,
	[EXTR03] [numeric](17, 2) NULL,
	[EXTR04] [numeric](17, 2) NULL,
	[EXTR05] [numeric](17, 2) NULL,
	[EXTR06] [numeric](17, 2) NULL,
	[EXTR07] [numeric](17, 2) NULL,
	[EXTR08] [numeric](17, 2) NULL,
	[EXTR09] [numeric](17, 2) NULL,
	[EXTR10] [numeric](17, 2) NULL,
	[EXTR11] [numeric](17, 2) NULL,
	[EXTR12] [numeric](17, 2) NULL,
	[EXTR13] [numeric](17, 2) NULL,
	[EXTR14] [numeric](17, 2) NULL,
	[EXTR15] [numeric](17, 2) NULL,
	[GSTRIAMT01] [numeric](17, 2) NULL,
	[GSTRIAMT02] [numeric](17, 2) NULL,
	[GSTRIAMT03] [numeric](17, 2) NULL,
	[GSTRIAMT04] [numeric](17, 2) NULL,
	[GSTRIAMT05] [numeric](17, 2) NULL,
	[GSTRIAMT06] [numeric](17, 2) NULL,
	[GSTRIAMT07] [numeric](17, 2) NULL,
	[GSTRIAMT08] [numeric](17, 2) NULL,
	[GSTRIAMT09] [numeric](17, 2) NULL,
	[GSTRIAMT10] [numeric](17, 2) NULL,
	[GSTRIAMT11] [numeric](17, 2) NULL,
	[GSTRIAMT12] [numeric](17, 2) NULL,
	[GSTRIAMT13] [numeric](17, 2) NULL,
	[GSTRIAMT14] [numeric](17, 2) NULL,
	[GSTRIAMT15] [numeric](17, 2) NULL,
	[Z6TAXCDE01] [nchar](6) NULL,
	[Z6TAXCDE02] [nchar](6) NULL,
	[Z6TAXCDE03] [nchar](6) NULL,
	[Z6TAXCDE04] [nchar](6) NULL,
	[Z6TAXCDE05] [nchar](6) NULL,
	[Z6TAXCDE06] [nchar](6) NULL,
	[Z6TAXCDE07] [nchar](6) NULL,
	[Z6TAXCDE08] [nchar](6) NULL,
	[Z6TAXCDE09] [nchar](6) NULL,
	[Z6TAXCDE10] [nchar](6) NULL,
	[Z6TAXCDE11] [nchar](6) NULL,
	[Z6TAXCDE12] [nchar](6) NULL,
	[Z6TAXCDE13] [nchar](6) NULL,
	[Z6TAXCDE14] [nchar](6) NULL,
	[Z6TAXCDE15] [nchar](6) NULL,
	[Z6PERCNT01] [numeric](5, 3) NULL,
	[Z6PERCNT02] [numeric](5, 3) NULL,
	[Z6PERCNT03] [numeric](5, 3) NULL,
	[Z6PERCNT04] [numeric](5, 3) NULL,
	[Z6PERCNT05] [numeric](5, 3) NULL,
	[Z6PERCNT06] [numeric](5, 3) NULL,
	[Z6PERCNT07] [numeric](5, 3) NULL,
	[Z6PERCNT08] [numeric](5, 3) NULL,
	[Z6PERCNT09] [numeric](5, 3) NULL,
	[Z6PERCNT10] [numeric](5, 3) NULL,
	[Z6PERCNT11] [numeric](5, 3) NULL,
	[Z6PERCNT12] [numeric](5, 3) NULL,
	[Z6PERCNT13] [numeric](5, 3) NULL,
	[Z6PERCNT14] [numeric](5, 3) NULL,
	[Z6PERCNT15] [numeric](5, 3) NULL,
	[EXTOUT] [nchar](1) NULL,
	[RIPPN] [numeric](11, 8) NULL,
	[RECTYPE] [nchar](1) NULL,
	[BILLTYP] [nchar](1) NULL,
	[BILLNO] [int] NULL,
	[CNOTENUM] [numeric](8, 0) NULL,
	[COTYPE] [nchar](1) NULL,
	[COPPN] [numeric](11, 8) NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL)
	
Go

IF NOT EXISTS (select * from sysobjects where name = 'GLRIPF' and xtype = 'U')
CREATE TABLE [VM1DTA].[GLRIPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CLMCOY] [nchar](1) NULL,
	[CLAMNUM] [nchar](8) NULL,
	[GCOCCNO] [nchar](2) NULL,
	[GCCLMSEQ] [numeric](3, 0) NULL,
	[TRANNO] [int] NULL,
	[PRODTYP] [nchar](4) NULL,
	[PLANNO] [nchar](3) NULL,
	[MBRNO] [nchar](5) NULL,
	[DPNTNO] [nchar](2) NULL,
	[HEADCNTIND] [nchar](1) NULL,
	[RPFX] [nchar](2) NULL,
	[RCOY] [nchar](1) NULL,
	[RACC] [nchar](8) NULL,
	[RITYPE] [nchar](2) NULL,
	[CEDTYP] [numeric](1, 0) NULL,
	[SEQNO] [int] NULL,
	[RIPPN] [numeric](11, 8) NULL,
	[COTYPE] [nchar](1) NULL,
	[COPPN] [numeric](11, 8) NULL,
	[STLBASIS] [nchar](1) NULL,
	[VFLAG] [nchar](1) NULL,
	[CLCURR] [nchar](3) NULL,
	[CRATE] [numeric](18, 9) NULL,
	[JCODE] [nchar](2) NULL,
	[PAYMRI] [numeric](17, 2) NULL,
	[LPAYMRI] [numeric](17, 2) NULL,
	[RDOCNUM] [nchar](9) NULL,
	[RICLMREF] [nchar](20) NULL,
	[LXLLOW] [numeric](17, 0) NULL,
	[LXLUP] [numeric](17, 0) NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL)
	
Go

CREATE VIEW [VM1DTA].[GLRI](
UNIQUE_NUMBER, 
CLMCOY, 
CLAMNUM, 
GCOCCNO, 
GCCLMSEQ, 
TRANNO, 
PRODTYP, 
PLANNO, 
MBRNO, 
DPNTNO, 
HEADCNTIND, 
RPFX, 
RCOY, 
RACC, 
RITYPE, 
CEDTYP, 
SEQNO, 
RIPPN, 
COTYPE, 
COPPN, 
STLBASIS, 
VFLAG, 
CLCURR, 
CRATE, 
JCODE, 
PAYMRI, 
LPAYMRI, 
RDOCNUM, 
RICLMREF, 
LXLLOW, 
LXLUP, 
USRPRF, 
JOBNM, 
DATIME) AS
SELECT UNIQUE_NUMBER, 
CLMCOY, 
CLAMNUM, 
GCOCCNO, 
GCCLMSEQ, 
TRANNO, 
PRODTYP, 
PLANNO, 
MBRNO, 
DPNTNO, 
HEADCNTIND, 
RPFX, 
RCOY, 
RACC, 
RITYPE, 
CEDTYP, 
SEQNO, 
RIPPN, 
COTYPE, 
COPPN, 
STLBASIS, 
VFLAG, 
CLCURR, 
CRATE, 
JCODE, 
PAYMRI, 
LPAYMRI, 
RDOCNUM, 
RICLMREF, 
LXLLOW, 
LXLUP, 
USRPRF, 
JOBNM, 
DATIME
FROM VM1DTA.GLRIPF;

GO

IF NOT EXISTS (select * from sysobjects where name = 'GFXLPF' and xtype = 'U')
CREATE TABLE [VM1DTA].[GFXLPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[SUBSCOY] [nchar](1) NULL,
	[SUBSNUM] [nchar](8) NULL,
	[PRODTYP] [nchar](4) NULL,
	[PLANNO] [nchar](3) NULL,
	[MBRNO] [nchar](5) NULL,
	[DPNTNO] [nchar](2) NULL,
	[HEADCNTIND] [nchar](1) NULL,
	[TRANNO] [int] NULL,
	[DTEEFF] [int] NULL,
	[DTETER] [int] NULL,
	[VFLAG] [nchar](1) NULL,
	[RECNO] [numeric](3, 0) NULL,
	[RPFX] [nchar](2) NULL,
	[RCOY] [nchar](1) NULL,
	[RACC] [nchar](8) NULL,
	[RITYPE] [nchar](2) NULL,
	[SEQNO] [int] NULL,
	[RCVDATE] [int] NULL,
	[DEFVAL] [nchar](30) NULL,
	[PREM] [numeric](17, 2) NULL,
	[LCEPRM] [numeric](17, 2) NULL,
	[ORGPRM] [numeric](17, 2) NULL,
	[LCEDED] [numeric](17, 0) NULL,
	[LCEUPP] [numeric](17, 0) NULL,
	[ORGDED] [numeric](17, 0) NULL,
	[ORGUPP] [numeric](17, 0) NULL,
	[RICOMM] [numeric](5, 3) NULL,
	[COMMAMT] [numeric](17, 2) NULL,
	[GRNR] [nchar](1) NULL,
	[OCP] [nchar](1) NULL,
	[RIREF] [nchar](20) NULL,
	[COTYPE] [nchar](1) NULL,
	[COPPN] [numeric](11, 8) NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL)
Go

CREATE VIEW [VM1DTA].[GFXL](
UNIQUE_NUMBER, 
CHDRCOY, 
CHDRNUM, 
SUBSCOY, 
SUBSNUM, 
PRODTYP, 
PLANNO, 
MBRNO, 
DPNTNO, 
HEADCNTIND, 
TRANNO, 
DTEEFF, 
DTETER, 
VFLAG, 
RECNO, 
RPFX, 
RCOY, 
RACC, 
RITYPE, 
SEQNO, 
RCVDATE, 
DEFVAL, 
PREM, 
LCEPRM, 
ORGPRM, 
LCEDED, 
LCEUPP, 
ORGDED, 
ORGUPP, 
RICOMM, 
COMMAMT, 
GRNR, 
OCP, 
RIREF, 
COTYPE, 
COPPN, 
USRPRF, 
JOBNM, 
DATIME) AS
SELECT UNIQUE_NUMBER, 
CHDRCOY, 
CHDRNUM, 
SUBSCOY, 
SUBSNUM, 
PRODTYP, 
PLANNO, 
MBRNO, 
DPNTNO, 
HEADCNTIND, 
TRANNO, 
DTEEFF, 
DTETER, 
VFLAG, 
RECNO, 
RPFX, 
RCOY, 
RACC, 
RITYPE, 
SEQNO, 
RCVDATE, 
DEFVAL, 
PREM, 
LCEPRM, 
ORGPRM, 
LCEDED, 
LCEUPP, 
ORGDED, 
ORGUPP, 
RICOMM, 
COMMAMT, 
GRNR, 
OCP, 
RIREF, 
COTYPE, 
COPPN, 
USRPRF, 
JOBNM, 
DATIME
FROM VM1DTA.GFXLPF;

GO

IF NOT EXISTS (select * from sysobjects where name = 'GMAPPF' and xtype = 'U')
CREATE TABLE [VM1DTA].[GMAPPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[SUBSCOY] [nchar](1) NULL,
	[SUBSNUM] [nchar](8) NULL,
	[PRODTYP] [nchar](4) NULL,
	[PLANNO] [nchar](3) NULL,
	[MBRNO] [nchar](5) NULL,
	[DPNTNO] [nchar](2) NULL,
	[HEADCNTIND] [nchar](1) NULL,
	[TRANNO] [int] NULL,
	[DTEEFF] [int] NULL,
	[DTETER] [int] NULL,
	[VFLAG] [nchar](1) NULL,
	[TARR] [nchar](4) NULL,
	[ACURR] [nchar](3) NULL,
	[RIRETBAS] [nchar](1) NULL,
	[CEDEBS] [nchar](1) NULL,
	[SIORIG] [numeric](17, 0) NULL,
	[SILCE] [numeric](17, 0) NULL,
	[SIEXP] [numeric](17, 0) NULL,
	[SIEXPLCE] [numeric](17, 0) NULL,
	[SICURR] [nchar](3) NULL,
	[SIRAT] [numeric](18, 9) NULL,
	[PML] [numeric](5, 2) NULL,
	[NRPPN] [numeric](11, 8) NULL,
	[NRLCE] [numeric](17, 0) NULL,
	[NRORIG] [numeric](17, 0) NULL,
	[NRPML] [numeric](5, 2) NULL,
	[SQSPPN] [numeric](10, 7) NULL,
	[ZQSPML] [numeric](5, 2) NULL,
	[SRIPPN01] [numeric](10, 7) NULL,
	[SRIPPN02] [numeric](10, 7) NULL,
	[SRIPPN03] [numeric](10, 7) NULL,
	[SRIPPN04] [numeric](10, 7) NULL,
	[SRIPPN05] [numeric](10, 7) NULL,
	[SRIPPN06] [numeric](10, 7) NULL,
	[ZSPLCE01] [numeric](17, 0) NULL,
	[ZSPLCE02] [numeric](17, 0) NULL,
	[ZSPLCE03] [numeric](17, 0) NULL,
	[ZSPLCE04] [numeric](17, 0) NULL,
	[ZSPLCE05] [numeric](17, 0) NULL,
	[ZSPLCE06] [numeric](17, 0) NULL,
	[ZSPORIG01] [numeric](17, 0) NULL,
	[ZSPORIG02] [numeric](17, 0) NULL,
	[ZSPORIG03] [numeric](17, 0) NULL,
	[ZSPORIG04] [numeric](17, 0) NULL,
	[ZSPORIG05] [numeric](17, 0) NULL,
	[ZSPORIG06] [numeric](17, 0) NULL,
	[ZSPPML01] [numeric](5, 2) NULL,
	[ZSPPML02] [numeric](5, 2) NULL,
	[ZSPPML03] [numeric](5, 2) NULL,
	[ZSPPML04] [numeric](5, 2) NULL,
	[ZSPPML05] [numeric](5, 2) NULL,
	[ZSPPML06] [numeric](5, 2) NULL,
	[GRPPN] [numeric](11, 8) NULL,
	[GRLCE] [numeric](17, 0) NULL,
	[GRORIG] [numeric](17, 0) NULL,
	[ZGOVPPN] [numeric](10, 7) NULL,
	[ZGOVLCE] [numeric](17, 0) NULL,
	[ZGOVORIG] [numeric](17, 0) NULL,
	[ZGOVPML] [numeric](5, 2) NULL,
	[ZFACPPN] [numeric](10, 7) NULL,
	[ZFACLCE] [numeric](17, 0) NULL,
	[ZFACORIG] [numeric](17, 0) NULL,
	[ZGALPPN] [numeric](10, 7) NULL,
	[ZGALLCE] [numeric](17, 0) NULL,
	[ZGALORIG] [numeric](17, 0) NULL,
	[TXOLLCE] [numeric](17, 0) NULL,
	[TXOLORIG] [numeric](17, 0) NULL,
	[TXOLPRM] [numeric](17, 2) NULL,
	[TTLCE] [numeric](17, 0) NULL,
	[TTORIG] [numeric](17, 0) NULL,
	[COTYPE] [nchar](1) NULL,
	[COPPN] [numeric](11, 8) NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL)
	
Go

CREATE VIEW [VM1DTA].[GMAP](
UNIQUE_NUMBER, 
CHDRCOY, 
CHDRNUM, 
SUBSCOY, 
SUBSNUM, 
PRODTYP, 
PLANNO, 
MBRNO, 
DPNTNO, 
HEADCNTIND, 
TRANNO, 
DTEEFF, 
DTETER, 
VFLAG, 
TARR, 
ACURR, 
RIRETBAS, 
CEDEBS, 
SIORIG, 
SILCE, 
SIEXP, 
SIEXPLCE, 
SICURR, 
SIRAT, 
PML, 
NRPPN, 
NRLCE, 
NRORIG, 
NRPML, 
SQSPPN, 
ZQSPML, 
SRIPPN01,
SRIPPN02,
SRIPPN03,
SRIPPN04,
SRIPPN05,
SRIPPN06, 
ZSPLCE01,
ZSPLCE02,
ZSPLCE03,
ZSPLCE04,
ZSPLCE05,
ZSPLCE06, 
ZSPORIG01,
ZSPORIG02,
ZSPORIG03,
ZSPORIG04,
ZSPORIG05,
ZSPORIG06, 
ZSPPML01,
ZSPPML02,
ZSPPML03,
ZSPPML04,
ZSPPML05,
ZSPPML06, 
GRPPN, 
GRLCE, 
GRORIG, 
ZGOVPPN, 
ZGOVLCE, 
ZGOVORIG, 
ZGOVPML, 
ZFACPPN, 
ZFACLCE, 
ZFACORIG, 
ZGALPPN, 
ZGALLCE, 
ZGALORIG, 
TXOLLCE, 
TXOLORIG, 
TXOLPRM, 
TTLCE, 
TTORIG, 
COTYPE, 
COPPN, 
USRPRF, 
JOBNM, 
DATIME) AS
SELECT UNIQUE_NUMBER, 
CHDRCOY, 
CHDRNUM, 
SUBSCOY, 
SUBSNUM, 
PRODTYP, 
PLANNO, 
MBRNO, 
DPNTNO, 
HEADCNTIND, 
TRANNO, 
DTEEFF, 
DTETER, 
VFLAG, 
TARR, 
ACURR, 
RIRETBAS, 
CEDEBS, 
SIORIG, 
SILCE, 
SIEXP, 
SIEXPLCE, 
SICURR, 
SIRAT, 
PML, 
NRPPN, 
NRLCE, 
NRORIG, 
NRPML, 
SQSPPN, 
ZQSPML, 
SRIPPN01,
SRIPPN02,
SRIPPN03,
SRIPPN04,
SRIPPN05,
SRIPPN06, 
ZSPLCE01,
ZSPLCE02,
ZSPLCE03,
ZSPLCE04,
ZSPLCE05,
ZSPLCE06, 
ZSPORIG01,
ZSPORIG02,
ZSPORIG03,
ZSPORIG04,
ZSPORIG05,
ZSPORIG06, 
ZSPPML01,
ZSPPML02,
ZSPPML03,
ZSPPML04,
ZSPPML05,
ZSPPML06, 
GRPPN, 
GRLCE, 
GRORIG, 
ZGOVPPN, 
ZGOVLCE, 
ZGOVORIG, 
ZGOVPML, 
ZFACPPN, 
ZFACLCE, 
ZFACORIG, 
ZGALPPN, 
ZGALLCE, 
ZGALORIG, 
TXOLLCE, 
TXOLORIG, 
TXOLPRM, 
TTLCE, 
TTORIG, 
COTYPE, 
COPPN, 
USRPRF, 
JOBNM, 
DATIME
FROM VM1DTA.GMAPPF;
GO

/*ALTER VIEW*/
------ GCLH --------
ALTER VIEW [VM1DTA].[GCLH] (
	UNIQUE_NUMBER
	,CLMCOY
	,CLAMNUM
	,GCOCCNO
	,CHDRNUM
	,MBRNO
	,DPNTNO
	,CLNTCOY
	,CLNTNUM
	,GCSTS
	,CLAIMCUR
	,CRATE
	,PRODTYP
	,GRSKCLS
	,DTEVISIT
	,DTEDCHRG
	,GCDIAGCD
	,PLANNO
	,PREAUTNO
	,PROVORG
	,AREACDE
	,REFERRER
	,CLAMTYPE
	,TIMEHH
	,TIMEMM
	,ZCLTCLMREF
	,GCDTHCLM
	,APAIDAMT
	,REQNTYPE
	,CRDTCARD
	,WHOPAID
	,DTEKNOWN
	,GCFRPDTE
	,ZCLMRECD
	,MCFROM
	,MCTO
	,GDEDUCT
	,COPAY
	,MBRTYPE
	,PROVNET
	,AAD
	,THIRDRCVY
	,THIRDPARTY
	,TLMBRSHR
	,TLHMOSHR
	,DATEAUTH
	,GCAUTHBY
	,GCOPRSCD
	,REVLINK
	,REVIND
	,TPRCVPND
	,PENDFROM
	,MMPROD
	,HMOSHRMM
	,TAKEUP
	,DATACONV
	,CLRATE
	,REFNO
	,UPDIND
	,PREMRCVY
	,DATEAUTHF
	,DATEAUTHS
	,GCAUTHBYF
	,GCAUTHBYS
	,CASHLESS
	,TPAREFNO
	,INWARDNO
	,ICD101L
	,ICD102L
	,ICD103L
	,REGCLM
	,GCCAUSCD
	,JOBNM
	,USRPRF
	,DATIME
	,TLCFAMT
	,CLMPREVLMT
	,CLRRSVFLAG
	,CPYAPLYFLG
	,CLMPROCID
	,CLMAPRVID
	,CPYAPLYIND
	,STLBASIS
	)
AS
SELECT UNIQUE_NUMBER
	,CLMCOY
	,CLAMNUM
	,GCOCCNO
	,CHDRNUM
	,MBRNO
	,DPNTNO
	,CLNTCOY
	,CLNTNUM
	,GCSTS
	,CLAIMCUR
	,CRATE
	,PRODTYP
	,GRSKCLS
	,DTEVISIT
	,DTEDCHRG
	,GCDIAGCD
	,PLANNO
	,PREAUTNO
	,PROVORG
	,AREACDE
	,REFERRER
	,CLAMTYPE
	,TIMEHH
	,TIMEMM
	,ZCLTCLMREF
	,GCDTHCLM
	,APAIDAMT
	,REQNTYPE
	,CRDTCARD
	,WHOPAID
	,DTEKNOWN
	,GCFRPDTE
	,ZCLMRECD
	,MCFROM
	,MCTO
	,GDEDUCT
	,COPAY
	,MBRTYPE
	,PROVNET
	,AAD
	,THIRDRCVY
	,THIRDPARTY
	,TLMBRSHR
	,TLHMOSHR
	,DATEAUTH
	,GCAUTHBY
	,GCOPRSCD
	,REVLINK
	,REVIND
	,TPRCVPND
	,PENDFROM
	,MMPROD
	,HMOSHRMM
	,TAKEUP
	,DATACONV
	,CLRATE
	,REFNO
	,UPDIND
	,PREMRCVY
	,DATEAUTHF
	,DATEAUTHS
	,GCAUTHBYF
	,GCAUTHBYS
	,CASHLESS
	,TPAREFNO
	,INWARDNO
	,ICD101L
	,ICD102L
	,ICD103L
	,REGCLM
	,GCCAUSCD
	,JOBNM
	,USRPRF
	,DATIME
	,TLCFAMT
	,CLMPREVLMT
	,CLRRSVFLAG
	,CPYAPLYFLG
	,CLMPROCID
	,CLMAPRVID
	,CPYAPLYIND
	,STLBASIS
FROM VM1DTA.GCLHPF;

GO

------ GCLHAUT --------
ALTER VIEW [VM1DTA].[GCLHAUT](UNIQUE_NUMBER, CLMCOY, DTEVISIT, CLAMNUM, GCOCCNO, CHDRNUM, MBRNO, DPNTNO, CLNTCOY, CLNTNUM, GCSTS, CLAIMCUR, CRATE, PRODTYP, GRSKCLS, DTEDCHRG, GCDIAGCD, PLANNO, PREAUTNO, PROVORG, AREACDE, REFERRER, CLAMTYPE, TIMEHH, TIMEMM, ZCLTCLMREF, GCDTHCLM, APAIDAMT, REQNTYPE, CRDTCARD, WHOPAID, DTEKNOWN, GCFRPDTE, ZCLMRECD, MCFROM, MCTO, GDEDUCT, COPAY, MBRTYPE, PROVNET, AAD, THIRDRCVY, THIRDPARTY, TLMBRSHR, TLHMOSHR, DATEAUTH, GCAUTHBY, GCOPRSCD, REVLINK, REVIND, TPRCVPND, PENDFROM, MMPROD, HMOSHRMM, TAKEUP, DATACONV, CLRATE, REFNO, UPDIND, PREMRCVY, JOBNM, USRPRF, DATIME, STLBASIS) AS
SELECT UNIQUE_NUMBER,
            CLMCOY,
            DTEVISIT,
            CLAMNUM,
            GCOCCNO,
            CHDRNUM,
            MBRNO,
            DPNTNO,
            CLNTCOY,
            CLNTNUM,
            GCSTS,
            CLAIMCUR,
            CRATE,
            PRODTYP,
            GRSKCLS,
            DTEDCHRG,
            GCDIAGCD,
            PLANNO,
            PREAUTNO,
            PROVORG,
            AREACDE,
            REFERRER,
            CLAMTYPE,
            TIMEHH,
            TIMEMM,
            ZCLTCLMREF,
            GCDTHCLM,
            APAIDAMT,
            REQNTYPE,
            CRDTCARD,
            WHOPAID,
            DTEKNOWN,
            GCFRPDTE,
            ZCLMRECD,
            MCFROM,
            MCTO,
            GDEDUCT,
            COPAY,
            MBRTYPE,
            PROVNET,
            AAD,
            THIRDRCVY,
            THIRDPARTY,
            TLMBRSHR,
            TLHMOSHR,
            DATEAUTH,
            GCAUTHBY,
            GCOPRSCD,
            REVLINK,
            REVIND,
            TPRCVPND,
            PENDFROM,
            MMPROD,
            HMOSHRMM,
            TAKEUP,
            DATACONV,
            CLRATE,
            REFNO,
            UPDIND,
            PREMRCVY,
            JOBNM,
            USRPRF,
            DATIME,
			STLBASIS
       FROM VM1DTA.GCLHPF
      WHERE NOT (DATEAUTH IN (99999999))

GO



------ GCLHCLN --------
ALTER VIEW [VM1DTA].[GCLHCLN](UNIQUE_NUMBER, CLMCOY, CLAMNUM, GCOCCNO, CHDRNUM, MBRNO, DPNTNO, CLNTCOY, CLNTNUM, GCSTS, CLAIMCUR, CRATE, PRODTYP, GRSKCLS, DTEVISIT, DTEDCHRG, GCDIAGCD, PLANNO, PREAUTNO, PROVORG, AREACDE, REFERRER, CLAMTYPE, TIMEHH, TIMEMM, ZCLTCLMREF, GCDTHCLM, APAIDAMT, REQNTYPE, CRDTCARD, WHOPAID, DTEKNOWN, GCFRPDTE, ZCLMRECD, MCFROM, MCTO, GDEDUCT, COPAY, MBRTYPE, PROVNET, AAD, THIRDRCVY, THIRDPARTY, TLMBRSHR, TLHMOSHR, DATEAUTH, GCAUTHBY, GCOPRSCD, REVLINK, REVIND, TPRCVPND, PENDFROM, MMPROD, HMOSHRMM, TAKEUP, DATACONV, CLRATE, REFNO, UPDIND, PREMRCVY, JOBNM, USRPRF, DATIME, STLBASIS) AS
SELECT UNIQUE_NUMBER,
            CLMCOY,
            CLAMNUM,
            GCOCCNO,
            CHDRNUM,
            MBRNO,
            DPNTNO,
            CLNTCOY,
            CLNTNUM,
            GCSTS,
            CLAIMCUR,
            CRATE,
            PRODTYP,
            GRSKCLS,
            DTEVISIT,
            DTEDCHRG,
            GCDIAGCD,
            PLANNO,
            PREAUTNO,
            PROVORG,
            AREACDE,
            REFERRER,
            CLAMTYPE,
            TIMEHH,
            TIMEMM,
            ZCLTCLMREF,
            GCDTHCLM,
            APAIDAMT,
            REQNTYPE,
            CRDTCARD,
            WHOPAID,
            DTEKNOWN,
            GCFRPDTE,
            ZCLMRECD,
            MCFROM,
            MCTO,
            GDEDUCT,
            COPAY,
            MBRTYPE,
            PROVNET,
            AAD,
            THIRDRCVY,
            THIRDPARTY,
            TLMBRSHR,
            TLHMOSHR,
            DATEAUTH,
            GCAUTHBY,
            GCOPRSCD,
            REVLINK,
            REVIND,
            TPRCVPND,
            PENDFROM,
            MMPROD,
            HMOSHRMM,
            TAKEUP,
            DATACONV,
            CLRATE,
            REFNO,
            UPDIND,
            PREMRCVY,
            JOBNM,
            USRPRF,
            DATIME,
			STLBASIS

       FROM VM1DTA.GCLHPF

GO

------ GCLHCLT --------
ALTER VIEW [VM1DTA].[GCLHCLT](UNIQUE_NUMBER, CLMCOY, CLNTNUM, CLAMNUM, GCOCCNO, CHDRNUM, MBRNO, DPNTNO, CLNTCOY, GCSTS, CLAIMCUR, CRATE, PRODTYP, GRSKCLS, DTEVISIT, DTEDCHRG, GCDIAGCD, PLANNO, PREAUTNO, PROVORG, AREACDE, REFERRER, CLAMTYPE, TIMEHH, TIMEMM, ZCLTCLMREF, GCDTHCLM, APAIDAMT, REQNTYPE, CRDTCARD, WHOPAID, DTEKNOWN, GCFRPDTE, ZCLMRECD, MCFROM, MCTO, GDEDUCT, COPAY, MBRTYPE, PROVNET, AAD, THIRDRCVY, THIRDPARTY, TLMBRSHR, TLHMOSHR, DATEAUTH, GCAUTHBY, GCOPRSCD, REVLINK, REVIND, TPRCVPND, PENDFROM, MMPROD, HMOSHRMM, TAKEUP, DATACONV, CLRATE, REFNO, UPDIND, PREMRCVY, GCCAUSCD, JOBNM, USRPRF, DATIME, STLBASIS) AS
SELECT UNIQUE_NUMBER,
            CLMCOY,
            CLNTNUM,
            CLAMNUM,
            GCOCCNO,
            CHDRNUM,
            MBRNO,
            DPNTNO,
            CLNTCOY,
            GCSTS,
            CLAIMCUR,
            CRATE,
            PRODTYP,
            GRSKCLS,
            DTEVISIT,
            DTEDCHRG,
            GCDIAGCD,
            PLANNO,
            PREAUTNO,
            PROVORG,
            AREACDE,
            REFERRER,
            CLAMTYPE,
            TIMEHH,
            TIMEMM,
            ZCLTCLMREF,
            GCDTHCLM,
            APAIDAMT,
            REQNTYPE,
            CRDTCARD,
            WHOPAID,
            DTEKNOWN,
            GCFRPDTE,
            ZCLMRECD,
            MCFROM,
            MCTO,
            GDEDUCT,
            COPAY,
            MBRTYPE,
            PROVNET,
            AAD,
            THIRDRCVY,
            THIRDPARTY,
            TLMBRSHR,
            TLHMOSHR,
            DATEAUTH,
            GCAUTHBY,
            GCOPRSCD,
            REVLINK,
            REVIND,
            TPRCVPND,
            PENDFROM,
            MMPROD,
            HMOSHRMM,
            TAKEUP,
            DATACONV,
            CLRATE,
            REFNO,
            UPDIND,
            PREMRCVY,
            GCCAUSCD,
            JOBNM,
            USRPRF,
            DATIME,
			STLBASIS

       FROM VM1DTA.GCLHPF

GO

------ GCLHDUP --------
ALTER VIEW [VM1DTA].[GCLHDUP](UNIQUE_NUMBER, CLMCOY, CLAMNUM, MBRNO, DPNTNO, PRODTYP, GCDIAGCD, DTEVISIT, PROVORG, GCOCCNO, CHDRNUM, CLNTCOY, CLNTNUM, GCSTS, DTEDCHRG, CLAIMCUR, CRATE, GRSKCLS, PLANNO, PREAUTNO, AREACDE, REFERRER, CLAMTYPE, TIMEHH, TIMEMM, ZCLTCLMREF, GCDTHCLM, APAIDAMT, REQNTYPE, CRDTCARD, WHOPAID, DTEKNOWN, GCFRPDTE, ZCLMRECD, MCFROM, MCTO, GDEDUCT, COPAY, MBRTYPE, PROVNET, AAD, THIRDRCVY, THIRDPARTY, TLMBRSHR, TLHMOSHR, DATEAUTH, GCAUTHBY, GCOPRSCD, REVLINK, REVIND, TPRCVPND, PENDFROM, MMPROD, HMOSHRMM, TAKEUP, DATACONV, CLRATE, REFNO, UPDIND, PREMRCVY, JOBNM, USRPRF, DATIME, STLBASIS) AS
SELECT UNIQUE_NUMBER,
            CLMCOY,
            CLAMNUM,
            MBRNO,
            DPNTNO,
            PRODTYP,
            GCDIAGCD,
            DTEVISIT,
            PROVORG,
            GCOCCNO,
            CHDRNUM,
            CLNTCOY,
            CLNTNUM,
            GCSTS,
            DTEDCHRG,
            CLAIMCUR,
            CRATE,
            GRSKCLS,
            PLANNO,
            PREAUTNO,
            AREACDE,
            REFERRER,
            CLAMTYPE,
            TIMEHH,
            TIMEMM,
            ZCLTCLMREF,
            GCDTHCLM,
            APAIDAMT,
            REQNTYPE,
            CRDTCARD,
            WHOPAID,
            DTEKNOWN,
            GCFRPDTE,
            ZCLMRECD,
            MCFROM,
            MCTO,
            GDEDUCT,
            COPAY,
            MBRTYPE,
            PROVNET,
            AAD,
            THIRDRCVY,
            THIRDPARTY,
            TLMBRSHR,
            TLHMOSHR,
            DATEAUTH,
            GCAUTHBY,
            GCOPRSCD,
            REVLINK,
            REVIND,
            TPRCVPND,
            PENDFROM,
            MMPROD,
            HMOSHRMM,
            TAKEUP,
            DATACONV,
            CLRATE,
            REFNO,
            UPDIND,
            PREMRCVY,
            JOBNM,
            USRPRF,
            DATIME,
			STLBASIS

       FROM VM1DTA.GCLHPF

GO

------ GCLHINC --------
ALTER VIEW [VM1DTA].[GCLHINC](UNIQUE_NUMBER, CLMCOY, CLAMNUM, GCOCCNO, CHDRNUM, MBRNO, DPNTNO, CLNTCOY, CLNTNUM, GCSTS, CLAIMCUR, CRATE, PRODTYP, GRSKCLS, DTEVISIT, DTEDCHRG, GCDIAGCD, PLANNO, PREAUTNO, PROVORG, AREACDE, REFERRER, CLAMTYPE, TIMEHH, TIMEMM, ZCLTCLMREF, GCDTHCLM, APAIDAMT, REQNTYPE, CRDTCARD, WHOPAID, DTEKNOWN, GCFRPDTE, ZCLMRECD, MCFROM, MCTO, GDEDUCT, COPAY, MBRTYPE, PROVNET, AAD, THIRDRCVY, THIRDPARTY, TLMBRSHR, TLHMOSHR, DATEAUTH, GCAUTHBY, GCOPRSCD, REVLINK, REVIND, TPRCVPND, PENDFROM, MMPROD, HMOSHRMM, TAKEUP, DATACONV, CLRATE, REFNO, UPDIND, PREMRCVY, JOBNM, USRPRF, DATIME, STLBASIS) AS
SELECT UNIQUE_NUMBER,
            CLMCOY,
            CLAMNUM,
            GCOCCNO,
            CHDRNUM,
            MBRNO,
            DPNTNO,
            CLNTCOY,
            CLNTNUM,
            GCSTS,
            CLAIMCUR,
            CRATE,
            PRODTYP,
            GRSKCLS,
            DTEVISIT,
            DTEDCHRG,
            GCDIAGCD,
            PLANNO,
            PREAUTNO,
            PROVORG,
            AREACDE,
            REFERRER,
            CLAMTYPE,
            TIMEHH,
            TIMEMM,
            ZCLTCLMREF,
            GCDTHCLM,
            APAIDAMT,
            REQNTYPE,
            CRDTCARD,
            WHOPAID,
            DTEKNOWN,
            GCFRPDTE,
            ZCLMRECD,
            MCFROM,
            MCTO,
            GDEDUCT,
            COPAY,
            MBRTYPE,
            PROVNET,
            AAD,
            THIRDRCVY,
            THIRDPARTY,
            TLMBRSHR,
            TLHMOSHR,
            DATEAUTH,
            GCAUTHBY,
            GCOPRSCD,
            REVLINK,
            REVIND,
            TPRCVPND,
            PENDFROM,
            MMPROD,
            HMOSHRMM,
            TAKEUP,
            DATACONV,
            CLRATE,
            REFNO,
            UPDIND,
            PREMRCVY,
            JOBNM,
            USRPRF,
            DATIME,
			STLBASIS

       FROM VM1DTA.GCLHPF

GO


------ GCLHMB2 --------
ALTER VIEW [VM1DTA].[GCLHMB2](UNIQUE_NUMBER, CLMCOY, CHDRNUM, MBRNO, DPNTNO, TLHMOSHR, CLAMNUM, GCOCCNO, CLNTCOY, CLNTNUM, GCSTS, CLAIMCUR, CRATE, PRODTYP, GRSKCLS, DTEVISIT, DTEDCHRG, GCDIAGCD, PLANNO, PREAUTNO, PROVORG, AREACDE, REFERRER, CLAMTYPE, TIMEHH, TIMEMM, ZCLTCLMREF, GCDTHCLM, APAIDAMT, REQNTYPE, CRDTCARD, WHOPAID, DTEKNOWN, GCFRPDTE, ZCLMRECD, MCFROM, MCTO, GDEDUCT, COPAY, MBRTYPE, PROVNET, AAD, THIRDRCVY, THIRDPARTY, TLMBRSHR, DATEAUTH, GCAUTHBY, GCOPRSCD, REVLINK, REVIND, TPRCVPND, PENDFROM, MMPROD, HMOSHRMM, TAKEUP, DATACONV, CLRATE, REFNO, UPDIND, PREMRCVY, JOBNM, USRPRF, DATIME, STLBASIS) AS
SELECT UNIQUE_NUMBER,
            CLMCOY,
            CHDRNUM,
            MBRNO,
            DPNTNO,
            TLHMOSHR,
            CLAMNUM,
            GCOCCNO,
            CLNTCOY,
            CLNTNUM,
            GCSTS,
            CLAIMCUR,
            CRATE,
            PRODTYP,
            GRSKCLS,
            DTEVISIT,
            DTEDCHRG,
            GCDIAGCD,
            PLANNO,
            PREAUTNO,
            PROVORG,
            AREACDE,
            REFERRER,
            CLAMTYPE,
            TIMEHH,
            TIMEMM,
            ZCLTCLMREF,
            GCDTHCLM,
            APAIDAMT,
            REQNTYPE,
            CRDTCARD,
            WHOPAID,
            DTEKNOWN,
            GCFRPDTE,
            ZCLMRECD,
            MCFROM,
            MCTO,
            GDEDUCT,
            COPAY,
            MBRTYPE,
            PROVNET,
            AAD,
            THIRDRCVY,
            THIRDPARTY,
            TLMBRSHR,
            DATEAUTH,
            GCAUTHBY,
            GCOPRSCD,
            REVLINK,
            REVIND,
            TPRCVPND,
            PENDFROM,
            MMPROD,
            HMOSHRMM,
            TAKEUP,
            DATACONV,
            CLRATE,
            REFNO,
            UPDIND,
            PREMRCVY,
            JOBNM,
            USRPRF,
            DATIME,
			STLBASIS

       FROM VM1DTA.GCLHPF

GO


------ GCLHNET --------
ALTER VIEW [VM1DTA].[GCLHNET](UNIQUE_NUMBER, CLMCOY, CLAMNUM, GCOCCNO, CHDRNUM, MBRNO, DPNTNO, CLNTCOY, CLNTNUM, GCSTS, CLAIMCUR, CRATE, PRODTYP, GRSKCLS, DTEVISIT, DTEDCHRG, GCDIAGCD, PLANNO, PREAUTNO, PROVORG, AREACDE, REFERRER, CLAMTYPE, TIMEHH, TIMEMM, ZCLTCLMREF, GCDTHCLM, APAIDAMT, REQNTYPE, CRDTCARD, WHOPAID, DTEKNOWN, GCFRPDTE, ZCLMRECD, MCFROM, MCTO, GDEDUCT, COPAY, MBRTYPE, PROVNET, AAD, THIRDRCVY, THIRDPARTY, TLMBRSHR, TLHMOSHR, DATEAUTH, GCAUTHBY, GCOPRSCD, REVLINK, REVIND, TPRCVPND, PENDFROM, MMPROD, HMOSHRMM, TAKEUP, DATACONV, CLRATE, REFNO, UPDIND, PREMRCVY, JOBNM, USRPRF, DATIME, STLBASIS) AS
SELECT UNIQUE_NUMBER,
            CLMCOY,
            CLAMNUM,
            GCOCCNO,
            CHDRNUM,
            MBRNO,
            DPNTNO,
            CLNTCOY,
            CLNTNUM,
            GCSTS,
            CLAIMCUR,
            CRATE,
            PRODTYP,
            GRSKCLS,
            DTEVISIT,
            DTEDCHRG,
            GCDIAGCD,
            PLANNO,
            PREAUTNO,
            PROVORG,
            AREACDE,
            REFERRER,
            CLAMTYPE,
            TIMEHH,
            TIMEMM,
            ZCLTCLMREF,
            GCDTHCLM,
            APAIDAMT,
            REQNTYPE,
            CRDTCARD,
            WHOPAID,
            DTEKNOWN,
            GCFRPDTE,
            ZCLMRECD,
            MCFROM,
            MCTO,
            GDEDUCT,
            COPAY,
            MBRTYPE,
            PROVNET,
            AAD,
            THIRDRCVY,
            THIRDPARTY,
            TLMBRSHR,
            TLHMOSHR,
            DATEAUTH,
            GCAUTHBY,
            GCOPRSCD,
            REVLINK,
            REVIND,
            TPRCVPND,
            PENDFROM,
            MMPROD,
            HMOSHRMM,
            TAKEUP,
            DATACONV,
            CLRATE,
            REFNO,
            UPDIND,
            PREMRCVY,
            JOBNM,
            USRPRF,
            DATIME,
			STLBASIS

       FROM VM1DTA.GCLHPF

GO


------ GCLHOCC --------
ALTER VIEW [VM1DTA].[GCLHOCC](UNIQUE_NUMBER, CLMCOY, CHDRNUM, PRODTYP, MBRNO, DPNTNO, GCDIAGCD, CLAMNUM, GCOCCNO, CLNTCOY, CLNTNUM, GCSTS, CLAIMCUR, CRATE, GRSKCLS, DTEVISIT, DTEDCHRG, PLANNO, PREAUTNO, PROVORG, AREACDE, REFERRER, CLAMTYPE, TIMEHH, TIMEMM, ZCLTCLMREF, GCDTHCLM, APAIDAMT, REQNTYPE, CRDTCARD, WHOPAID, DTEKNOWN, GCFRPDTE, ZCLMRECD, MCFROM, MCTO, GDEDUCT, COPAY, MBRTYPE, PROVNET, AAD, THIRDRCVY, THIRDPARTY, TLMBRSHR, TLHMOSHR, DATEAUTH, GCAUTHBY, GCOPRSCD, REVLINK, REVIND, TPRCVPND, PENDFROM, MMPROD, HMOSHRMM, TAKEUP, DATACONV, CLRATE, REFNO, UPDIND, PREMRCVY, JOBNM, USRPRF, DATIME, STLBASIS) AS
SELECT UNIQUE_NUMBER,
            CLMCOY,
            CHDRNUM,
            PRODTYP,
            MBRNO,
            DPNTNO,
            GCDIAGCD,
            CLAMNUM,
            GCOCCNO,
            CLNTCOY,
            CLNTNUM,
            GCSTS,
            CLAIMCUR,
            CRATE,
            GRSKCLS,
            DTEVISIT,
            DTEDCHRG,
            PLANNO,
            PREAUTNO,
            PROVORG,
            AREACDE,
            REFERRER,
            CLAMTYPE,
            TIMEHH,
            TIMEMM,
            ZCLTCLMREF,
            GCDTHCLM,
            APAIDAMT,
            REQNTYPE,
            CRDTCARD,
            WHOPAID,
            DTEKNOWN,
            GCFRPDTE,
            ZCLMRECD,
            MCFROM,
            MCTO,
            GDEDUCT,
            COPAY,
            MBRTYPE,
            PROVNET,
            AAD,
            THIRDRCVY,
            THIRDPARTY,
            TLMBRSHR,
            TLHMOSHR,
            DATEAUTH,
            GCAUTHBY,
            GCOPRSCD,
            REVLINK,
            REVIND,
            TPRCVPND,
            PENDFROM,
            MMPROD,
            HMOSHRMM,
            TAKEUP,
            DATACONV,
            CLRATE,
            REFNO,
            UPDIND,
            PREMRCVY,
            JOBNM,
            USRPRF,
            DATIME,
			STLBASIS

       FROM VM1DTA.GCLHPF

GO

------ GCLHPL2 --------
ALTER VIEW [VM1DTA].[GCLHPL2](UNIQUE_NUMBER, CLMCOY, CHDRNUM, TLHMOSHR, CLAMNUM, GCOCCNO, MBRNO, DPNTNO, CLNTCOY, CLNTNUM, GCSTS, CLAIMCUR, CRATE, PRODTYP, GRSKCLS, DTEVISIT, DTEDCHRG, GCDIAGCD, PLANNO, PREAUTNO, PROVORG, AREACDE, REFERRER, CLAMTYPE, TIMEHH, TIMEMM, ZCLTCLMREF, GCDTHCLM, APAIDAMT, REQNTYPE, CRDTCARD, WHOPAID, DTEKNOWN, GCFRPDTE, ZCLMRECD, MCFROM, MCTO, GDEDUCT, COPAY, MBRTYPE, PROVNET, AAD, THIRDRCVY, THIRDPARTY, TLMBRSHR, DATEAUTH, GCAUTHBY, GCOPRSCD, REVLINK, REVIND, TPRCVPND, PENDFROM, MMPROD, HMOSHRMM, TAKEUP, DATACONV, CLRATE, REFNO, UPDIND, PREMRCVY, JOBNM, USRPRF, DATIME, STLBASIS) AS
SELECT UNIQUE_NUMBER,
            CLMCOY,
            CHDRNUM,
            TLHMOSHR,
            CLAMNUM,
            GCOCCNO,
            MBRNO,
            DPNTNO,
            CLNTCOY,
            CLNTNUM,
            GCSTS,
            CLAIMCUR,
            CRATE,
            PRODTYP,
            GRSKCLS,
            DTEVISIT,
            DTEDCHRG,
            GCDIAGCD,
            PLANNO,
            PREAUTNO,
            PROVORG,
            AREACDE,
            REFERRER,
            CLAMTYPE,
            TIMEHH,
            TIMEMM,
            ZCLTCLMREF,
            GCDTHCLM,
            APAIDAMT,
            REQNTYPE,
            CRDTCARD,
            WHOPAID,
            DTEKNOWN,
            GCFRPDTE,
            ZCLMRECD,
            MCFROM,
            MCTO,
            GDEDUCT,
            COPAY,
            MBRTYPE,
            PROVNET,
            AAD,
            THIRDRCVY,
            THIRDPARTY,
            TLMBRSHR,
            DATEAUTH,
            GCAUTHBY,
            GCOPRSCD,
            REVLINK,
            REVIND,
            TPRCVPND,
            PENDFROM,
            MMPROD,
            HMOSHRMM,
            TAKEUP,
            DATACONV,
            CLRATE,
            REFNO,
            UPDIND,
            PREMRCVY,
            JOBNM,
            USRPRF,
            DATIME,
			STLBASIS

       FROM VM1DTA.GCLHPF

GO


------ GCLHPLS --------
ALTER VIEW [VM1DTA].[GCLHPLS] (
	UNIQUE_NUMBER
	,CLMCOY
	,CHDRNUM
	,MBRNO
	,DPNTNO
	,PRODTYP
	,DTEVISIT
	,CLAMNUM
	,GCOCCNO
	,CLNTCOY
	,GCSTS
	,CLAIMCUR
	,CRATE
	,GRSKCLS
	,DTEDCHRG
	,GCDIAGCD
	,PLANNO
	,PREAUTNO
	,PROVORG
	,AREACDE
	,REFERRER
	,CLAMTYPE
	,TIMEHH
	,TIMEMM
	,ZCLTCLMREF
	,GCDTHCLM
	,APAIDAMT
	,REQNTYPE
	,CRDTCARD
	,WHOPAID
	,DTEKNOWN
	,GCFRPDTE
	,ZCLMRECD
	,MCFROM
	,MCTO
	,GDEDUCT
	,COPAY
	,MBRTYPE
	,PROVNET
	,AAD
	,THIRDRCVY
	,THIRDPARTY
	,TLMBRSHR
	,TLHMOSHR
	,DATEAUTH
	,GCAUTHBY
	,GCOPRSCD
	,REVLINK
	,REVIND
	,TPRCVPND
	,PENDFROM
	,MMPROD
	,HMOSHRMM
	,TAKEUP
	,DATACONV
	,CLRATE
	,REFNO
	,UPDIND
	,JOBNM
	,USRPRF
	,DATIME
	,STLBASIS
	)
AS
SELECT UNIQUE_NUMBER
	,CLMCOY
	,CHDRNUM
	,MBRNO
	,DPNTNO
	,PRODTYP
	,DTEVISIT
	,CLAMNUM
	,GCOCCNO
	,CLNTCOY
	,GCSTS
	,CLAIMCUR
	,CRATE
	,GRSKCLS
	,DTEDCHRG
	,GCDIAGCD
	,PLANNO
	,PREAUTNO
	,PROVORG
	,AREACDE
	,REFERRER
	,CLAMTYPE
	,TIMEHH
	,TIMEMM
	,ZCLTCLMREF
	,GCDTHCLM
	,APAIDAMT
	,REQNTYPE
	,CRDTCARD
	,WHOPAID
	,DTEKNOWN
	,GCFRPDTE
	,ZCLMRECD
	,MCFROM
	,MCTO
	,GDEDUCT
	,COPAY
	,MBRTYPE
	,PROVNET
	,AAD
	,THIRDRCVY
	,THIRDPARTY
	,TLMBRSHR
	,TLHMOSHR
	,DATEAUTH
	,GCAUTHBY
	,GCOPRSCD
	,REVLINK
	,REVIND
	,TPRCVPND
	,PENDFROM
	,MMPROD
	,HMOSHRMM
	,TAKEUP
	,DATACONV
	,CLRATE
	,REFNO
	,UPDIND
	,JOBNM
	,USRPRF
	,DATIME
	,STLBASIS
FROM VM1DTA.GCLHPF;

GO

------ GCLHPOC --------
ALTER VIEW [VM1DTA].[GCLHPOC](UNIQUE_NUMBER, CLMCOY, CLAMNUM, GCOCCNO, CHDRNUM, MBRNO, DPNTNO, CLNTCOY, CLNTNUM, GCSTS, CLAIMCUR, CRATE, PRODTYP, GRSKCLS, DTEVISIT, DTEDCHRG, GCDIAGCD, PLANNO, PREAUTNO, PROVORG, AREACDE, REFERRER, CLAMTYPE, TIMEHH, TIMEMM, ZCLTCLMREF, GCDTHCLM, APAIDAMT, REQNTYPE, CRDTCARD, WHOPAID, DTEKNOWN, GCFRPDTE, ZCLMRECD, MCFROM, MCTO, GDEDUCT, COPAY, MBRTYPE, PROVNET, AAD, THIRDRCVY, THIRDPARTY, TLMBRSHR, TLHMOSHR, DATEAUTH, GCAUTHBY, GCOPRSCD, REVLINK, REVIND, TPRCVPND, PENDFROM, MMPROD, HMOSHRMM, TAKEUP, DATACONV, CLRATE, REFNO, UPDIND, PREMRCVY, JOBNM, USRPRF, DATIME, STLBASIS) AS
SELECT UNIQUE_NUMBER,
            CLMCOY,
            CLAMNUM,
            GCOCCNO,
            CHDRNUM,
            MBRNO,
            DPNTNO,
            CLNTCOY,
            CLNTNUM,
            GCSTS,
            CLAIMCUR,
            CRATE,
            PRODTYP,
            GRSKCLS,
            DTEVISIT,
            DTEDCHRG,
            GCDIAGCD,
            PLANNO,
            PREAUTNO,
            PROVORG,
            AREACDE,
            REFERRER,
            CLAMTYPE,
            TIMEHH,
            TIMEMM,
            ZCLTCLMREF,
            GCDTHCLM,
            APAIDAMT,
            REQNTYPE,
            CRDTCARD,
            WHOPAID,
            DTEKNOWN,
            GCFRPDTE,
            ZCLMRECD,
            MCFROM,
            MCTO,
            GDEDUCT,
            COPAY,
            MBRTYPE,
            PROVNET,
            AAD,
            THIRDRCVY,
            THIRDPARTY,
            TLMBRSHR,
            TLHMOSHR,
            DATEAUTH,
            GCAUTHBY,
            GCOPRSCD,
            REVLINK,
            REVIND,
            TPRCVPND,
            PENDFROM,
            MMPROD,
            HMOSHRMM,
            TAKEUP,
            DATACONV,
            CLRATE,
            REFNO,
            UPDIND,
            PREMRCVY,
            JOBNM,
            USRPRF,
            DATIME,
			STLBASIS

       FROM VM1DTA.GCLHPF

GO

------ GCLHPOG --------
ALTER VIEW [VM1DTA].[GCLHPOG](UNIQUE_NUMBER, CLMCOY, CLAMNUM, GCOCCNO, CHDRNUM, MBRNO, DPNTNO, CLNTCOY, CLNTNUM, GCSTS, CLAIMCUR, CRATE, PRODTYP, GRSKCLS, DTEVISIT, DTEDCHRG, GCDIAGCD, PLANNO, PREAUTNO, PROVORG, AREACDE, REFERRER, CLAMTYPE, TIMEHH, TIMEMM, ZCLTCLMREF, GCDTHCLM, APAIDAMT, REQNTYPE, CRDTCARD, WHOPAID, DTEKNOWN, GCFRPDTE, ZCLMRECD, MCFROM, MCTO, GDEDUCT, COPAY, MBRTYPE, PROVNET, AAD, THIRDRCVY, THIRDPARTY, TLMBRSHR, TLHMOSHR, DATEAUTH, GCAUTHBY, GCOPRSCD, REVLINK, REVIND, TPRCVPND, PENDFROM, MMPROD, HMOSHRMM, TAKEUP, DATACONV, CLRATE, REFNO, UPDIND, PREMRCVY, JOBNM, USRPRF, DATIME, STLBASIS) AS
SELECT UNIQUE_NUMBER,
            CLMCOY,
            CLAMNUM,
            GCOCCNO,
            CHDRNUM,
            MBRNO,
            DPNTNO,
            CLNTCOY,
            CLNTNUM,
            GCSTS,
            CLAIMCUR,
            CRATE,
            PRODTYP,
            GRSKCLS,
            DTEVISIT,
            DTEDCHRG,
            GCDIAGCD,
            PLANNO,
            PREAUTNO,
            PROVORG,
            AREACDE,
            REFERRER,
            CLAMTYPE,
            TIMEHH,
            TIMEMM,
            ZCLTCLMREF,
            GCDTHCLM,
            APAIDAMT,
            REQNTYPE,
            CRDTCARD,
            WHOPAID,
            DTEKNOWN,
            GCFRPDTE,
            ZCLMRECD,
            MCFROM,
            MCTO,
            GDEDUCT,
            COPAY,
            MBRTYPE,
            PROVNET,
            AAD,
            THIRDRCVY,
            THIRDPARTY,
            TLMBRSHR,
            TLHMOSHR,
            DATEAUTH,
            GCAUTHBY,
            GCOPRSCD,
            REVLINK,
            REVIND,
            TPRCVPND,
            PENDFROM,
            MMPROD,
            HMOSHRMM,
            TAKEUP,
            DATACONV,
            CLRATE,
            REFNO,
            UPDIND,
            PREMRCVY,
            JOBNM,
            USRPRF,
            DATIME,
			STLBASIS

       FROM VM1DTA.GCLHPF

GO

------ GCLHPOL --------
ALTER VIEW [VM1DTA].[GCLHPOL](UNIQUE_NUMBER, CLMCOY, CHDRNUM, PRODTYP, MBRNO, DPNTNO, CLAMNUM, GCOCCNO, CLNTCOY, CLNTNUM, GCSTS, CLAIMCUR, CRATE, GRSKCLS, DTEVISIT, DTEDCHRG, GCDIAGCD, PLANNO, PREAUTNO, PROVORG, AREACDE, REFERRER, CLAMTYPE, TIMEHH, TIMEMM, ZCLTCLMREF, GCDTHCLM, APAIDAMT, REQNTYPE, CRDTCARD, WHOPAID, DTEKNOWN, GCFRPDTE, ZCLMRECD, MCFROM, MCTO, GDEDUCT, COPAY, MBRTYPE, PROVNET, AAD, THIRDRCVY, THIRDPARTY, TLMBRSHR, TLHMOSHR, DATEAUTH, GCAUTHBY, GCOPRSCD, REVLINK, REVIND, TPRCVPND, PENDFROM, MMPROD, HMOSHRMM, TAKEUP, DATACONV, CLRATE, REFNO, UPDIND, PREMRCVY, JOBNM, USRPRF, DATIME, STLBASIS) AS
SELECT UNIQUE_NUMBER,
            CLMCOY,
            CHDRNUM,
            PRODTYP,
            MBRNO,
            DPNTNO,
            CLAMNUM,
            GCOCCNO,
            CLNTCOY,
            CLNTNUM,
            GCSTS,
            CLAIMCUR,
            CRATE,
            GRSKCLS,
            DTEVISIT,
            DTEDCHRG,
            GCDIAGCD,
            PLANNO,
            PREAUTNO,
            PROVORG,
            AREACDE,
            REFERRER,
            CLAMTYPE,
            TIMEHH,
            TIMEMM,
            ZCLTCLMREF,
            GCDTHCLM,
            APAIDAMT,
            REQNTYPE,
            CRDTCARD,
            WHOPAID,
            DTEKNOWN,
            GCFRPDTE,
            ZCLMRECD,
            MCFROM,
            MCTO,
            GDEDUCT,
            COPAY,
            MBRTYPE,
            PROVNET,
            AAD,
            THIRDRCVY,
            THIRDPARTY,
            TLMBRSHR,
            TLHMOSHR,
            DATEAUTH,
            GCAUTHBY,
            GCOPRSCD,
            REVLINK,
            REVIND,
            TPRCVPND,
            PENDFROM,
            MMPROD,
            HMOSHRMM,
            TAKEUP,
            DATACONV,
            CLRATE,
            REFNO,
            UPDIND,
            PREMRCVY,
            JOBNM,
            USRPRF,
            DATIME,
			STLBASIS


       FROM VM1DTA.GCLHPF

GO

------ GCLMADV --------
ALTER VIEW [VM1DTA].[GCLMADV](UNIQUE_NUMBER, CHDRCOY, CLAMNUM, GCOCCNO, GCCLMSEQ, CHDRNUM, PRODTYP, GCCLMTNO, GCDPNTNO, CLNTPFX, CLNTCOY, CLNTNUM, GCSTS, DTECLAM, CLAIMCUR, CRATE, PLANNO, FMLYCDE, GCRMK, BNKCHARGE, SPECTRM, GCDBLIND, GCFRPDTE, GCLRPDTE, GCDTHCLM, GCCAUSCD, GCOPPVCD, GCOPBNCD, GCOPIAMT, GCOPBAMT, GCOPBNPY, GCOPDGCD, GCOPPTCD, REQNBCDE, REQNTYPE, GCAUTHBY, DATEAUTH, GCOPRSCD, GCGRSPY, GCNETPY, GCCOINA, DEDUCTPC, GCADVPY, GCTRDRPY, GCLOCCNO, MMFLG, DTEPYMTADV, DTEPYMTOTH, DTEATT, DTETRM, REVLINK, REVIND, TERMID, USER_T, TRDT, TRTM, GRSKCLS, USRPRF, JOBNM, DATIME, STLBASIS) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CLAMNUM,
            GCOCCNO,
            GCCLMSEQ,
            CHDRNUM,
            PRODTYP,
            GCCLMTNO,
            GCDPNTNO,
            CLNTPFX,
            CLNTCOY,
            CLNTNUM,
            GCSTS,
            DTECLAM,
            CLAIMCUR,
            CRATE,
            PLANNO,
            FMLYCDE,
            GCRMK,
            BNKCHARGE,
            SPECTRM,
            GCDBLIND,
            GCFRPDTE,
            GCLRPDTE,
            GCDTHCLM,
            GCCAUSCD,
            GCOPPVCD,
            GCOPBNCD,
            GCOPIAMT,
            GCOPBAMT,
            GCOPBNPY,
            GCOPDGCD,
            GCOPPTCD,
            REQNBCDE,
            REQNTYPE,
            GCAUTHBY,
            DATEAUTH,
            GCOPRSCD,
            GCGRSPY,
            GCNETPY,
            GCCOINA,
            DEDUCTPC,
            GCADVPY,
            GCTRDRPY,
            GCLOCCNO,
            MMFLG,
            DTEPYMTADV,
            DTEPYMTOTH,
            DTEATT,
            DTETRM,
            REVLINK,
            REVIND,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            GRSKCLS,
            USRPRF,
            JOBNM,
            DATIME,
			STLBASIS
       FROM VM1DTA.GCMHPF

GO

------ GCLMCLT --------
ALTER VIEW [VM1DTA].[GCLMCLT](UNIQUE_NUMBER, CHDRCOY, CLAMNUM, GCOCCNO, GCCLMSEQ, CHDRNUM, PRODTYP, GCCLMTNO, GCDPNTNO, CLNTPFX, CLNTCOY, CLNTNUM, GCSTS, DTECLAM, CLAIMCUR, CRATE, PLANNO, FMLYCDE, GCRMK, BNKCHARGE, SPECTRM, GCDBLIND, GCFRPDTE, GCLRPDTE, GCDTHCLM, GCCAUSCD, GCOPPVCD, GCOPBNCD, GCOPIAMT, GCOPBAMT, GCOPBNPY, GCOPDGCD, GCOPPTCD, REQNBCDE, REQNTYPE, GCAUTHBY, DATEAUTH, GCOPRSCD, GCGRSPY, GCNETPY, GCCOINA, DEDUCTPC, GCADVPY, GCTRDRPY, GCLOCCNO, MMFLG, DTEPYMTADV, DTEPYMTOTH, DTEATT, DTETRM, REVLINK, REVIND, TERMID, USER_T, TRDT, TRTM, GRSKCLS, USRPRF, JOBNM, DATIME, STLBASIS) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CLAMNUM,
            GCOCCNO,
            GCCLMSEQ,
            CHDRNUM,
            PRODTYP,
            GCCLMTNO,
            GCDPNTNO,
            CLNTPFX,
            CLNTCOY,
            CLNTNUM,
            GCSTS,
            DTECLAM,
            CLAIMCUR,
            CRATE,
            PLANNO,
            FMLYCDE,
            GCRMK,
            BNKCHARGE,
            SPECTRM,
            GCDBLIND,
            GCFRPDTE,
            GCLRPDTE,
            GCDTHCLM,
            GCCAUSCD,
            GCOPPVCD,
            GCOPBNCD,
            GCOPIAMT,
            GCOPBAMT,
            GCOPBNPY,
            GCOPDGCD,
            GCOPPTCD,
            REQNBCDE,
            REQNTYPE,
            GCAUTHBY,
            DATEAUTH,
            GCOPRSCD,
            GCGRSPY,
            GCNETPY,
            GCCOINA,
            DEDUCTPC,
            GCADVPY,
            GCTRDRPY,
            GCLOCCNO,
            MMFLG,
            DTEPYMTADV,
            DTEPYMTOTH,
            DTEATT,
            DTETRM,
            REVLINK,
            REVIND,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            GRSKCLS,
            USRPRF,
            JOBNM,
            DATIME,
			STLBASIS

       FROM VM1DTA.GCMHPF

GO

------ GCLMDUP --------
ALTER VIEW [VM1DTA].[GCLMDUP](UNIQUE_NUMBER, CHDRCOY, CLAMNUM, GCOCCNO, GCCLMSEQ, CHDRNUM, PRODTYP, GCCLMTNO, GCDPNTNO, CLNTPFX, CLNTCOY, CLNTNUM, GCSTS, DTECLAM, CLAIMCUR, CRATE, PLANNO, FMLYCDE, GCRMK, BNKCHARGE, SPECTRM, GCDBLIND, GCFRPDTE, GCLRPDTE, GCDTHCLM, GCCAUSCD, GCOPPVCD, GCOPBNCD, GCOPIAMT, GCOPBAMT, GCOPBNPY, GCOPDGCD, GCOPPTCD, REQNBCDE, REQNTYPE, GCAUTHBY, DATEAUTH, GCOPRSCD, GCGRSPY, GCNETPY, GCCOINA, DEDUCTPC, GCADVPY, GCTRDRPY, GCLOCCNO, MMFLG, DTEPYMTADV, DTEPYMTOTH, DTEATT, DTETRM, REVLINK, REVIND, TERMID, USER_T, TRDT, TRTM, GRSKCLS, USRPRF, JOBNM, DATIME, STLBASIS) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CLAMNUM,
            GCOCCNO,
            GCCLMSEQ,
            CHDRNUM,
            PRODTYP,
            GCCLMTNO,
            GCDPNTNO,
            CLNTPFX,
            CLNTCOY,
            CLNTNUM,
            GCSTS,
            DTECLAM,
            CLAIMCUR,
            CRATE,
            PLANNO,
            FMLYCDE,
            GCRMK,
            BNKCHARGE,
            SPECTRM,
            GCDBLIND,
            GCFRPDTE,
            GCLRPDTE,
            GCDTHCLM,
            GCCAUSCD,
            GCOPPVCD,
            GCOPBNCD,
            GCOPIAMT,
            GCOPBAMT,
            GCOPBNPY,
            GCOPDGCD,
            GCOPPTCD,
            REQNBCDE,
            REQNTYPE,
            GCAUTHBY,
            DATEAUTH,
            GCOPRSCD,
            GCGRSPY,
            GCNETPY,
            GCCOINA,
            DEDUCTPC,
            GCADVPY,
            GCTRDRPY,
            GCLOCCNO,
            MMFLG,
            DTEPYMTADV,
            DTEPYMTOTH,
            DTEATT,
            DTETRM,
            REVLINK,
            REVIND,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            GRSKCLS,
            USRPRF,
            JOBNM,
            DATIME,
			STLBASIS
       FROM VM1DTA.GCMHPF
      WHERE GCCLMSEQ = 0

GO

------ GCLMMBR --------
ALTER VIEW [VM1DTA].[GCLMMBR](UNIQUE_NUMBER, CHDRCOY, CLAMNUM, GCOCCNO, GCCLMSEQ, CHDRNUM, PRODTYP, GCCLMTNO, GCDPNTNO, CLNTPFX, CLNTCOY, CLNTNUM, GCSTS, DTECLAM, CLAIMCUR, CRATE, PLANNO, FMLYCDE, GCRMK, BNKCHARGE, SPECTRM, GCDBLIND, GCFRPDTE, GCLRPDTE, GCDTHCLM, GCCAUSCD, GCOPPVCD, GCOPBNCD, GCOPIAMT, GCOPBAMT, GCOPBNPY, GCOPDGCD, GCOPPTCD, REQNBCDE, REQNTYPE, GCAUTHBY, DATEAUTH, GCOPRSCD, GCGRSPY, GCNETPY, GCCOINA, DEDUCTPC, GCADVPY, GCTRDRPY, GCLOCCNO, MMFLG, DTEPYMTADV, DTEPYMTOTH, DTEATT, DTETRM, REVLINK, REVIND, TERMID, USER_T, TRDT, TRTM, GRSKCLS, USRPRF, JOBNM, DATIME, STLBASIS) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CLAMNUM,
            GCOCCNO,
            GCCLMSEQ,
            CHDRNUM,
            PRODTYP,
            GCCLMTNO,
            GCDPNTNO,
            CLNTPFX,
            CLNTCOY,
            CLNTNUM,
            GCSTS,
            DTECLAM,
            CLAIMCUR,
            CRATE,
            PLANNO,
            FMLYCDE,
            GCRMK,
            BNKCHARGE,
            SPECTRM,
            GCDBLIND,
            GCFRPDTE,
            GCLRPDTE,
            GCDTHCLM,
            GCCAUSCD,
            GCOPPVCD,
            GCOPBNCD,
            GCOPIAMT,
            GCOPBAMT,
            GCOPBNPY,
            GCOPDGCD,
            GCOPPTCD,
            REQNBCDE,
            REQNTYPE,
            GCAUTHBY,
            DATEAUTH,
            GCOPRSCD,
            GCGRSPY,
            GCNETPY,
            GCCOINA,
            DEDUCTPC,
            GCADVPY,
            GCTRDRPY,
            GCLOCCNO,
            MMFLG,
            DTEPYMTADV,
            DTEPYMTOTH,
            DTEATT,
            DTETRM,
            REVLINK,
            REVIND,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            GRSKCLS,
            USRPRF,
            JOBNM,
            DATIME,
			STLBASIS

       FROM VM1DTA.GCMHPF

GO

------ GCLMOTH --------
ALTER VIEW [VM1DTA].[GCLMOTH](UNIQUE_NUMBER, CHDRCOY, CLAMNUM, GCOCCNO, GCCLMSEQ, CHDRNUM, PRODTYP, GCCLMTNO, GCDPNTNO, CLNTPFX, CLNTCOY, CLNTNUM, GCSTS, DTECLAM, CLAIMCUR, CRATE, PLANNO, FMLYCDE, GCRMK, BNKCHARGE, SPECTRM, GCDBLIND, GCFRPDTE, GCLRPDTE, GCDTHCLM, GCCAUSCD, GCOPPVCD, GCOPBNCD, GCOPIAMT, GCOPBAMT, GCOPBNPY, GCOPDGCD, GCOPPTCD, REQNBCDE, REQNTYPE, GCAUTHBY, DATEAUTH, GCOPRSCD, GCGRSPY, GCNETPY, GCCOINA, DEDUCTPC, GCADVPY, GCTRDRPY, GCLOCCNO, MMFLG, DTEPYMTADV, DTEPYMTOTH, DTEATT, DTETRM, REVLINK, REVIND, TERMID, USER_T, TRDT, TRTM, GRSKCLS, USRPRF, JOBNM, DATIME, STLBASIS) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CLAMNUM,
            GCOCCNO,
            GCCLMSEQ,
            CHDRNUM,
            PRODTYP,
            GCCLMTNO,
            GCDPNTNO,
            CLNTPFX,
            CLNTCOY,
            CLNTNUM,
            GCSTS,
            DTECLAM,
            CLAIMCUR,
            CRATE,
            PLANNO,
            FMLYCDE,
            GCRMK,
            BNKCHARGE,
            SPECTRM,
            GCDBLIND,
            GCFRPDTE,
            GCLRPDTE,
            GCDTHCLM,
            GCCAUSCD,
            GCOPPVCD,
            GCOPBNCD,
            GCOPIAMT,
            GCOPBAMT,
            GCOPBNPY,
            GCOPDGCD,
            GCOPPTCD,
            REQNBCDE,
            REQNTYPE,
            GCAUTHBY,
            DATEAUTH,
            GCOPRSCD,
            GCGRSPY,
            GCNETPY,
            GCCOINA,
            DEDUCTPC,
            GCADVPY,
            GCTRDRPY,
            GCLOCCNO,
            MMFLG,
            DTEPYMTADV,
            DTEPYMTOTH,
            DTEATT,
            DTETRM,
            REVLINK,
            REVIND,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            GRSKCLS,
            USRPRF,
            JOBNM,
            DATIME,
			STLBASIS

       FROM VM1DTA.GCMHPF

GO

------ GCLMOUT --------
ALTER VIEW [VM1DTA].[GCLMOUT](UNIQUE_NUMBER, CHDRCOY, CLAMNUM, GCOCCNO, GCCLMSEQ, CHDRNUM, PRODTYP, GCCLMTNO, GCDPNTNO, CLNTPFX, CLNTCOY, CLNTNUM, GCSTS, DTECLAM, CLAIMCUR, CRATE, PLANNO, FMLYCDE, GCRMK, BNKCHARGE, SPECTRM, GCDBLIND, GCFRPDTE, GCLRPDTE, GCDTHCLM, GCCAUSCD, GCOPPVCD, GCOPBNCD, GCOPIAMT, GCOPBAMT, GCOPBNPY, GCOPDGCD, GCOPPTCD, REQNBCDE, REQNTYPE, GCAUTHBY, DATEAUTH, GCOPRSCD, GCGRSPY, GCNETPY, GCCOINA, DEDUCTPC, GCADVPY, GCTRDRPY, GCLOCCNO, MMFLG, DTEPYMTADV, DTEPYMTOTH, DTEATT, DTETRM, REVLINK, REVIND, TERMID, USER_T, TRDT, TRTM, GRSKCLS, USRPRF, JOBNM, DATIME, STLBASIS) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CLAMNUM,
            GCOCCNO,
            GCCLMSEQ,
            CHDRNUM,
            PRODTYP,
            GCCLMTNO,
            GCDPNTNO,
            CLNTPFX,
            CLNTCOY,
            CLNTNUM,
            GCSTS,
            DTECLAM,
            CLAIMCUR,
            CRATE,
            PLANNO,
            FMLYCDE,
            GCRMK,
            BNKCHARGE,
            SPECTRM,
            GCDBLIND,
            GCFRPDTE,
            GCLRPDTE,
            GCDTHCLM,
            GCCAUSCD,
            GCOPPVCD,
            GCOPBNCD,
            GCOPIAMT,
            GCOPBAMT,
            GCOPBNPY,
            GCOPDGCD,
            GCOPPTCD,
            REQNBCDE,
            REQNTYPE,
            GCAUTHBY,
            DATEAUTH,
            GCOPRSCD,
            GCGRSPY,
            GCNETPY,
            GCCOINA,
            DEDUCTPC,
            GCADVPY,
            GCTRDRPY,
            GCLOCCNO,
            MMFLG,
            DTEPYMTADV,
            DTEPYMTOTH,
            DTEATT,
            DTETRM,
            REVLINK,
            REVIND,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            GRSKCLS,
            USRPRF,
            JOBNM,
            DATIME,
			STLBASIS

       FROM VM1DTA.GCMHPF

GO

------ GCLMPND --------
ALTER VIEW [VM1DTA].[GCLMPND](UNIQUE_NUMBER, CLNTCOY, CLNTNUM, CHDRCOY, CHDRNUM, PRODTYP, CLAMNUM, GCOCCNO, GCCLMSEQ, GCCLMTNO, GCDPNTNO, GCSTS, DTECLAM, PLANNO, PACCAMT, GCGRSPY, GCADVPY, GCNETPY, TERMID, USER_T, TRDT, TRTM, GRSKCLS, USRPRF, JOBNM, DATIME, STLBASIS) AS
SELECT UNIQUE_NUMBER,
            CLNTCOY,
            CLNTNUM,
            CHDRCOY,
            CHDRNUM,
            PRODTYP,
            CLAMNUM,
            GCOCCNO,
            GCCLMSEQ,
            GCCLMTNO,
            GCDPNTNO,
            GCSTS,
            DTECLAM,
            PLANNO,
            PACCAMT,
            GCGRSPY,
            GCADVPY,
            GCNETPY,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            GRSKCLS,
            USRPRF,
            JOBNM,
            DATIME,
			STLBASIS
       FROM VM1DTA.GCMHPF
      WHERE GCSTS = 'CP' OR GCSTS = 'CV' OR GCSTS = 'AV'

GO



------ GCLMPOL --------
ALTER VIEW [VM1DTA].[GCLMPOL](UNIQUE_NUMBER, CHDRCOY, CLAMNUM, GCOCCNO, GCCLMSEQ, CHDRNUM, PRODTYP, GCCLMTNO, GCDPNTNO, CLNTPFX, CLNTCOY, CLNTNUM, GCSTS, DTECLAM, CLAIMCUR, CRATE, PLANNO, FMLYCDE, GCRMK, BNKCHARGE, SPECTRM, GCDBLIND, GCFRPDTE, GCLRPDTE, GCDTHCLM, GCCAUSCD, GCOPPVCD, GCOPBNCD, GCOPIAMT, GCOPBAMT, GCOPBNPY, GCOPDGCD, GCOPPTCD, REQNBCDE, REQNTYPE, GCAUTHBY, DATEAUTH, GCOPRSCD, GCGRSPY, GCNETPY, GCCOINA, DEDUCTPC, GCADVPY, GCTRDRPY, GCLOCCNO, MMFLG, PACCAMT, OVRLMT, DTEPYMTADV, DTEPYMTOTH, DTEATT, DTETRM, REVLINK, REVIND, TERMID, USER_T, TRDT, TRTM, ZCLTCLMREF, ZCLMRECD, ZCLNTPROC, ZCLMREF, ZFEECURR, ZFEETYP, ZCALCFEE, ZOVRDFEE, GRSKCLS, USRPRF, JOBNM, DATIME, STLBASIS) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CLAMNUM,
            GCOCCNO,
            GCCLMSEQ,
            CHDRNUM,
            PRODTYP,
            GCCLMTNO,
            GCDPNTNO,
            CLNTPFX,
            CLNTCOY,
            CLNTNUM,
            GCSTS,
            DTECLAM,
            CLAIMCUR,
            CRATE,
            PLANNO,
            FMLYCDE,
            GCRMK,
            BNKCHARGE,
            SPECTRM,
            GCDBLIND,
            GCFRPDTE,
            GCLRPDTE,
            GCDTHCLM,
            GCCAUSCD,
            GCOPPVCD,
            GCOPBNCD,
            GCOPIAMT,
            GCOPBAMT,
            GCOPBNPY,
            GCOPDGCD,
            GCOPPTCD,
            REQNBCDE,
            REQNTYPE,
            GCAUTHBY,
            DATEAUTH,
            GCOPRSCD,
            GCGRSPY,
            GCNETPY,
            GCCOINA,
            DEDUCTPC,
            GCADVPY,
            GCTRDRPY,
            GCLOCCNO,
            MMFLG,
            PACCAMT,
            OVRLMT,
            DTEPYMTADV,
            DTEPYMTOTH,
            DTEATT,
            DTETRM,
            REVLINK,
            REVIND,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            ZCLTCLMREF,
            ZCLMRECD,
            ZCLNTPROC,
            ZCLMREF,
            ZFEECURR,
            ZFEETYP,
            ZCALCFEE,
            ZOVRDFEE,
            GRSKCLS,
            USRPRF,
            JOBNM,
            DATIME,
			STLBASIS

       FROM VM1DTA.GCMHPF

GO

------ GCLMPRD --------
ALTER VIEW [VM1DTA].[GCLMPRD](UNIQUE_NUMBER, CHDRCOY, CLAMNUM, GCOCCNO, GCCLMSEQ, CHDRNUM, PRODTYP, GCCLMTNO, GCDPNTNO, CLNTPFX, CLNTCOY, CLNTNUM, GCSTS, DTECLAM, CLAIMCUR, CRATE, PLANNO, FMLYCDE, GCRMK, BNKCHARGE, SPECTRM, GCDBLIND, GCFRPDTE, GCLRPDTE, GCDTHCLM, GCCAUSCD, GCOPPVCD, GCOPBNCD, GCOPIAMT, GCOPBAMT, GCOPBNPY, GCOPDGCD, GCOPPTCD, REQNBCDE, REQNTYPE, GCAUTHBY, DATEAUTH, GCOPRSCD, GCGRSPY, GCNETPY, GCCOINA, DEDUCTPC, GCADVPY, GCTRDRPY, GCLOCCNO, MMFLG, DTEPYMTADV, DTEPYMTOTH, DTEATT, DTETRM, REVLINK, REVIND, TERMID, USER_T, TRDT, TRTM, GRSKCLS, USRPRF, JOBNM, DATIME, STLBASIS) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CLAMNUM,
            GCOCCNO,
            GCCLMSEQ,
            CHDRNUM,
            PRODTYP,
            GCCLMTNO,
            GCDPNTNO,
            CLNTPFX,
            CLNTCOY,
            CLNTNUM,
            GCSTS,
            DTECLAM,
            CLAIMCUR,
            CRATE,
            PLANNO,
            FMLYCDE,
            GCRMK,
            BNKCHARGE,
            SPECTRM,
            GCDBLIND,
            GCFRPDTE,
            GCLRPDTE,
            GCDTHCLM,
            GCCAUSCD,
            GCOPPVCD,
            GCOPBNCD,
            GCOPIAMT,
            GCOPBAMT,
            GCOPBNPY,
            GCOPDGCD,
            GCOPPTCD,
            REQNBCDE,
            REQNTYPE,
            GCAUTHBY,
            DATEAUTH,
            GCOPRSCD,
            GCGRSPY,
            GCNETPY,
            GCCOINA,
            DEDUCTPC,
            GCADVPY,
            GCTRDRPY,
            GCLOCCNO,
            MMFLG,
            DTEPYMTADV,
            DTEPYMTOTH,
            DTEATT,
            DTETRM,
            REVLINK,
            REVIND,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            GRSKCLS,
            USRPRF,
            JOBNM,
            DATIME,
			STLBASIS

       FROM VM1DTA.GCMHPF

GO


------ GCLMVAL --------
ALTER VIEW [VM1DTA].[GCLMVAL](UNIQUE_NUMBER, CHDRCOY, CLAMNUM, GCOCCNO, GCCLMSEQ, CHDRNUM, PRODTYP, GCCLMTNO, GCDPNTNO, CLNTPFX, CLNTCOY, CLNTNUM, GCSTS, DTECLAM, CLAIMCUR, CRATE, PLANNO, FMLYCDE, GCRMK, BNKCHARGE, SPECTRM, GCDBLIND, GCFRPDTE, GCLRPDTE, GCDTHCLM, GCCAUSCD, GCOPPVCD, GCOPBNCD, GCOPIAMT, GCOPBAMT, GCOPBNPY, GCOPDGCD, GCOPPTCD, REQNBCDE, REQNTYPE, GCAUTHBY, DATEAUTH, GCOPRSCD, GCGRSPY, GCNETPY, GCCOINA, DEDUCTPC, GCADVPY, GCTRDRPY, GCLOCCNO, MMFLG, DTEPYMTADV, DTEPYMTOTH, DTEATT, DTETRM, REVLINK, REVIND, TERMID, USER_T, TRDT, TRTM, GRSKCLS, USRPRF, JOBNM, DATIME, STLBASIS) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CLAMNUM,
            GCOCCNO,
            GCCLMSEQ,
            CHDRNUM,
            PRODTYP,
            GCCLMTNO,
            GCDPNTNO,
            CLNTPFX,
            CLNTCOY,
            CLNTNUM,
            GCSTS,
            DTECLAM,
            CLAIMCUR,
            CRATE,
            PLANNO,
            FMLYCDE,
            GCRMK,
            BNKCHARGE,
            SPECTRM,
            GCDBLIND,
            GCFRPDTE,
            GCLRPDTE,
            GCDTHCLM,
            GCCAUSCD,
            GCOPPVCD,
            GCOPBNCD,
            GCOPIAMT,
            GCOPBAMT,
            GCOPBNPY,
            GCOPDGCD,
            GCOPPTCD,
            REQNBCDE,
            REQNTYPE,
            GCAUTHBY,
            DATEAUTH,
            GCOPRSCD,
            GCGRSPY,
            GCNETPY,
            GCCOINA,
            DEDUCTPC,
            GCADVPY,
            GCTRDRPY,
            GCLOCCNO,
            MMFLG,
            DTEPYMTADV,
            DTEPYMTOTH,
            DTEATT,
            DTETRM,
            REVLINK,
            REVIND,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            GRSKCLS,
            USRPRF,
            JOBNM,
            DATIME,
			STLBASIS
       FROM VM1DTA.GCMHPF
      WHERE GCCLMSEQ <> 0

GO


------ GCMH --------
ALTER VIEW [VM1DTA].[GCMH](UNIQUE_NUMBER, CHDRCOY, CLAMNUM, GCOCCNO, GCCLMSEQ, CHDRNUM, PRODTYP, GCCLMTNO, GCDPNTNO, CLNTPFX, CLNTCOY, CLNTNUM, GCSTS, DTECLAM, CLAIMCUR, CRATE, PLANNO, FMLYCDE, GCRMK, BNKCHARGE, SPECTRM, GCDBLIND, GCFRPDTE, GCLRPDTE, GCDTHCLM, GCCAUSCD, GCOPPVCD, GCOPBNCD, GCOPIAMT, GCOPBAMT, GCOPBNPY, GCOPDGCD, GCOPPTCD, REQNBCDE, REQNTYPE, GCAUTHBY, DATEAUTH, GCOPRSCD, GCGRSPY, GCNETPY, GCCOINA, DEDUCTPC, GCADVPY, GCTRDRPY, GCLOCCNO, MMFLG, PACCAMT, OVRLMT, DTEPYMTADV, DTEPYMTOTH, DTEATT, DTETRM, REVLINK, REVIND, TERMID, USER_T, TRDT, TRTM, APAIDAMT, INTPAIDAMT, GCSETLMD, GCSECSTS, ZCLTCLMREF, ZCLMRECD, ZCLNTPROC, ZCLMREF, ZFEECURR, ZFEETYP, ZCALCFEE, ZOVRDFEE, GRSKCLS, BENCDE, CLAIMCOND, OUTLOAN, USRPRF, JOBNM, DATIME, DIACODE, ONDUTY, ROPENRSN, STLBASIS) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CLAMNUM,
            GCOCCNO,
            GCCLMSEQ,
            CHDRNUM,
            PRODTYP,
            GCCLMTNO,
            GCDPNTNO,
            CLNTPFX,
            CLNTCOY,
            CLNTNUM,
            GCSTS,
            DTECLAM,
            CLAIMCUR,
            CRATE,
            PLANNO,
            FMLYCDE,
            GCRMK,
            BNKCHARGE,
            SPECTRM,
            GCDBLIND,
            GCFRPDTE,
            GCLRPDTE,
            GCDTHCLM,
            GCCAUSCD,
            GCOPPVCD,
            GCOPBNCD,
            GCOPIAMT,
            GCOPBAMT,
            GCOPBNPY,
            GCOPDGCD,
            GCOPPTCD,
            REQNBCDE,
            REQNTYPE,
            GCAUTHBY,
            DATEAUTH,
            GCOPRSCD,
            GCGRSPY,
            GCNETPY,
            GCCOINA,
            DEDUCTPC,
            GCADVPY,
            GCTRDRPY,
            GCLOCCNO,
            MMFLG,
            PACCAMT,
            OVRLMT,
            DTEPYMTADV,
            DTEPYMTOTH,
            DTEATT,
            DTETRM,
            REVLINK,
            REVIND,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            APAIDAMT,
            INTPAIDAMT,
            GCSETLMD,
            GCSECSTS,
            ZCLTCLMREF,
            ZCLMRECD,
            ZCLNTPROC,
            ZCLMREF,
            ZFEECURR,
            ZFEETYP,
            ZCALCFEE,
            ZOVRDFEE,
            GRSKCLS,
            BENCDE,
            CLAIMCOND,
            OUTLOAN,
            USRPRF,
            JOBNM,
            DATIME,
			DIACODE,
			ONDUTY,
			ROPENRSN,
			STLBASIS

       FROM VM1DTA.GCMHPF


GO

------ GCMHCRV --------
ALTER VIEW [VM1DTA].[GCMHCRV](UNIQUE_NUMBER, CHDRCOY, CLAMNUM, GCOCCNO, GCCLMSEQ, CHDRNUM, PRODTYP, GCCLMTNO, GCDPNTNO, CLNTPFX, CLNTCOY, CLNTNUM, GCSTS, DTECLAM, CLAIMCUR, CRATE, PLANNO, FMLYCDE, GCRMK, BNKCHARGE, SPECTRM, GCDBLIND, GCFRPDTE, GCLRPDTE, GCDTHCLM, GCCAUSCD, GCOPPVCD, GCOPBNCD, GCOPIAMT, GCOPBAMT, GCOPBNPY, GCOPDGCD, GCOPPTCD, REQNBCDE, REQNTYPE, GCAUTHBY, DATEAUTH, GCOPRSCD, GCGRSPY, GCNETPY, GCCOINA, DEDUCTPC, GCADVPY, GCTRDRPY, MMFLG, PACCAMT, OVRLMT, DTEPYMTADV, DTEPYMTOTH, DTEATT, DTETRM, GCLOCCNO, REVLINK, REVIND, TERMID, USER_T, TRDT, TRTM, APAIDAMT, INTPAIDAMT, GCSETLMD, GCSECSTS, ZCLTCLMREF, ZCLMRECD, ZCLNTPROC, ZCLMREF, ZFEECURR, ZFEETYP, ZCALCFEE, ZOVRDFEE, GRSKCLS, GREASCD, GRNGMNT, CLMRCSYS, CLMRCUSR, RDOCNUM, USRPRF, JOBNM, DATIME, STLBASIS) AS
SELECT GCMHPF.UNIQUE_NUMBER,
            GCMHPF.CHDRCOY,
            GCMHPF.CLAMNUM,
            GCMHPF.GCOCCNO,
            GCMHPF.GCCLMSEQ,
            GCMHPF.CHDRNUM,
            GCMHPF.PRODTYP,
            GCMHPF.GCCLMTNO,
            GCMHPF.GCDPNTNO,
            GCMHPF.CLNTPFX,
            GCMHPF.CLNTCOY,
            GCMHPF.CLNTNUM,
            GCMHPF.GCSTS,
            GCMHPF.DTECLAM,
            GCMHPF.CLAIMCUR,
            GCMHPF.CRATE,
            GCMHPF.PLANNO,
            GCMHPF.FMLYCDE,
            GCMHPF.GCRMK,
            GCMHPF.BNKCHARGE,
            GCMHPF.SPECTRM,
            GCMHPF.GCDBLIND,
            GCMHPF.GCFRPDTE,
            GCMHPF.GCLRPDTE,
            GCMHPF.GCDTHCLM,
            GCMHPF.GCCAUSCD,
            GCMHPF.GCOPPVCD,
            GCMHPF.GCOPBNCD,
            GCMHPF.GCOPIAMT,
            GCMHPF.GCOPBAMT,
            GCMHPF.GCOPBNPY,
            GCMHPF.GCOPDGCD,
            GCMHPF.GCOPPTCD,
            GCMHPF.REQNBCDE,
            GCMHPF.REQNTYPE,
            GCMHPF.GCAUTHBY,
            GCMHPF.DATEAUTH,
            GCMHPF.GCOPRSCD,
            GCMHPF.GCGRSPY,
            GCMHPF.GCNETPY,
            GCMHPF.GCCOINA,
            GCMHPF.DEDUCTPC,
            GCMHPF.GCADVPY,
            GCMHPF.GCTRDRPY,
            GCMHPF.MMFLG,
            GCMHPF.PACCAMT,
            GCMHPF.OVRLMT,
            GCMHPF.DTEPYMTADV,
            GCMHPF.DTEPYMTOTH,
            GCMHPF.DTEATT,
            GCMHPF.DTETRM,
            GCMHPF.GCLOCCNO,
            GCMHPF.REVLINK,
            GCMHPF.REVIND,
            GCMHPF.TERMID,
            GCMHPF.USER_T,
            GCMHPF.TRDT,
            GCMHPF.TRTM,
            GCMHPF.APAIDAMT,
            GCMHPF.INTPAIDAMT,
            GCMHPF.GCSETLMD,
            GCMHPF.GCSECSTS,
            GCMHPF.ZCLTCLMREF,
            GCMHPF.ZCLMRECD,
            GCMHPF.ZCLNTPROC,
            GCMHPF.ZCLMREF,
            GCMHPF.ZFEECURR,
            GCMHPF.ZFEETYP,
            GCMHPF.ZCALCFEE,
            GCMHPF.ZOVRDFEE,
            GCMHPF.GRSKCLS,
            CLRVPF.GREASCD,
            CLRVPF.GRNGMNT,
            CLRVPF.CLMRCSYS,
            CLRVPF.CLMRCUSR,
            CLRVPF.RDOCNUM,
            GCMHPF.USRPRF,
            GCMHPF.JOBNM,
            GCMHPF.DATIME,
			GCMHPF.STLBASIS
       FROM VM1DTA.GCMHPF, VM1DTA.CLRVPF
      WHERE (GCMHPF.CHDRCOY = CLRVPF.CHDRCOY)
            AND (GCMHPF.CLAMNUM = CLRVPF.CLAMNUM)

GO

------ GCMHINT --------
ALTER VIEW [VM1DTA].[GCMHINT](UNIQUE_NUMBER, CHDRCOY, CLAMNUM, GCOCCNO, GCCLMSEQ, CHDRNUM, PRODTYP, GCCLMTNO, GCDPNTNO, CLNTPFX, CLNTCOY, CLNTNUM, GCSTS, DTECLAM, CLAIMCUR, CRATE, PLANNO, FMLYCDE, GCRMK, BNKCHARGE, SPECTRM, GCDBLIND, GCFRPDTE, GCLRPDTE, GCDTHCLM, GCCAUSCD, GCOPPVCD, GCOPBNCD, GCOPIAMT, GCOPBAMT, GCOPBNPY, GCOPDGCD, GCOPPTCD, REQNBCDE, REQNTYPE, GCAUTHBY, DATEAUTH, GCOPRSCD, GCGRSPY, GCNETPY, GCCOINA, DEDUCTPC, GCADVPY, GCTRDRPY, GCLOCCNO, MMFLG, PACCAMT, OVRLMT, DTEPYMTADV, DTEPYMTOTH, DTEATT, DTETRM, REVLINK, REVIND, TERMID, USER_T, TRDT, TRTM, APAIDAMT, INTPAIDAMT, GCSETLMD, GCSECSTS, GRSKCLS, USRPRF, JOBNM, DATIME, STLBASIS) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CLAMNUM,
            GCOCCNO,
            GCCLMSEQ,
            CHDRNUM,
            PRODTYP,
            GCCLMTNO,
            GCDPNTNO,
            CLNTPFX,
            CLNTCOY,
            CLNTNUM,
            GCSTS,
            DTECLAM,
            CLAIMCUR,
            CRATE,
            PLANNO,
            FMLYCDE,
            GCRMK,
            BNKCHARGE,
            SPECTRM,
            GCDBLIND,
            GCFRPDTE,
            GCLRPDTE,
            GCDTHCLM,
            GCCAUSCD,
            GCOPPVCD,
            GCOPBNCD,
            GCOPIAMT,
            GCOPBAMT,
            GCOPBNPY,
            GCOPDGCD,
            GCOPPTCD,
            REQNBCDE,
            REQNTYPE,
            GCAUTHBY,
            DATEAUTH,
            GCOPRSCD,
            GCGRSPY,
            GCNETPY,
            GCCOINA,
            DEDUCTPC,
            GCADVPY,
            GCTRDRPY,
            GCLOCCNO,
            MMFLG,
            PACCAMT,
            OVRLMT,
            DTEPYMTADV,
            DTEPYMTOTH,
            DTEATT,
            DTETRM,
            REVLINK,
            REVIND,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            APAIDAMT,
            INTPAIDAMT,
            GCSETLMD,
            GCSECSTS,
            GRSKCLS,
            USRPRF,
            JOBNM,
            DATIME,
			STLBASIS
       FROM VM1DTA.GCMHPF

GO

------ GCMHPLS --------
ALTER VIEW [VM1DTA].[GCMHPLS] (
	UNIQUE_NUMBER
	,CHDRCOY
	,CLAMNUM
	,GCOCCNO
	,GCCLMSEQ
	,CHDRNUM
	,PRODTYP
	,GCCLMTNO
	,GCDPNTNO
	,CLNTPFX
	,CLNTCOY
	,CLNTNUM
	,GCSTS
	,DTECLAM
	,CLAIMCUR
	,CRATE
	,PLANNO
	,FMLYCDE
	,GCRMK
	,BNKCHARGE
	,SPECTRM
	,GCDBLIND
	,GCFRPDTE
	,GCLRPDTE
	,GCDTHCLM
	,GCCAUSCD
	,GCOPPVCD
	,GCOPBNCD
	,GCOPIAMT
	,GCOPBAMT
	,GCOPBNPY
	,GCOPDGCD
	,GCOPPTCD
	,REQNBCDE
	,REQNTYPE
	,GCAUTHBY
	,DATEAUTH
	,GCOPRSCD
	,GCGRSPY
	,GCNETPY
	,GCCOINA
	,DEDUCTPC
	,GCADVPY
	,GCTRDRPY
	,MMFLG
	,PACCAMT
	,OVRLMT
	,DTEPYMTADV
	,DTEPYMTOTH
	,DTEATT
	,DTETRM
	,GCLOCCNO
	,REVLINK
	,REVIND
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,APAIDAMT
	,INTPAIDAMT
	,GCSETLMD
	,GCSECSTS
	,ZCLTCLMREF
	,ZCLMRECD
	,ZCLNTPROC
	,ZCLMREF
	,ZFEECURR
	,ZFEETYP
	,ZCALCFEE
	,ZOVRDFEE
	,GRSKCLS
	,BENCDE
	,CLAIMCOND
	,OUTLOAN
	,JOBNM
	,USRPRF
	,DATIME
	,STLBASIS
	)
AS
SELECT UNIQUE_NUMBER
	,CHDRCOY
	,CLAMNUM
	,GCOCCNO
	,GCCLMSEQ
	,CHDRNUM
	,PRODTYP
	,GCCLMTNO
	,GCDPNTNO
	,CLNTPFX
	,CLNTCOY
	,CLNTNUM
	,GCSTS
	,DTECLAM
	,CLAIMCUR
	,CRATE
	,PLANNO
	,FMLYCDE
	,GCRMK
	,BNKCHARGE
	,SPECTRM
	,GCDBLIND
	,GCFRPDTE
	,GCLRPDTE
	,GCDTHCLM
	,GCCAUSCD
	,GCOPPVCD
	,GCOPBNCD
	,GCOPIAMT
	,GCOPBAMT
	,GCOPBNPY
	,GCOPDGCD
	,GCOPPTCD
	,REQNBCDE
	,REQNTYPE
	,GCAUTHBY
	,DATEAUTH
	,GCOPRSCD
	,GCGRSPY
	,GCNETPY
	,GCCOINA
	,DEDUCTPC
	,GCADVPY
	,GCTRDRPY
	,MMFLG
	,PACCAMT
	,OVRLMT
	,DTEPYMTADV
	,DTEPYMTOTH
	,DTEATT
	,DTETRM
	,GCLOCCNO
	,REVLINK
	,REVIND
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,APAIDAMT
	,INTPAIDAMT
	,GCSETLMD
	,GCSECSTS
	,ZCLTCLMREF
	,ZCLMRECD
	,ZCLNTPROC
	,ZCLMREF
	,ZFEECURR
	,ZFEETYP
	,ZCALCFEE
	,ZOVRDFEE
	,GRSKCLS
	,BENCDE
	,CLAIMCOND
	,OUTLOAN
	,JOBNM
	,USRPRF
	,DATIME
	,STLBASIS
FROM VM1DTA.GCMHPF;

GO

------ GCMHPOC --------
ALTER VIEW [VM1DTA].[GCMHPOC](UNIQUE_NUMBER, CHDRCOY, CLAMNUM, GCOCCNO, GCCLMSEQ, CHDRNUM, PRODTYP, GCCLMTNO, GCDPNTNO, CLNTPFX, CLNTCOY, CLNTNUM, GCSTS, DTECLAM, CLAIMCUR, CRATE, PLANNO, FMLYCDE, GCRMK, BNKCHARGE, SPECTRM, GCDBLIND, GCFRPDTE, GCLRPDTE, GCDTHCLM, GCCAUSCD, GCOPPVCD, GCOPBNCD, GCOPIAMT, GCOPBAMT, GCOPBNPY, GCOPDGCD, GCOPPTCD, REQNBCDE, REQNTYPE, GCAUTHBY, DATEAUTH, GCOPRSCD, GCGRSPY, GCNETPY, GCCOINA, DEDUCTPC, GCADVPY, GCTRDRPY, MMFLG, PACCAMT, OVRLMT, DTEPYMTADV, DTEPYMTOTH, DTEATT, DTETRM, GCLOCCNO, REVLINK, REVIND, TERMID, USER_T, TRDT, TRTM, APAIDAMT, INTPAIDAMT, GCSETLMD, GCSECSTS, GRSKCLS, USRPRF, JOBNM, DATIME, STLBASIS) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CLAMNUM,
            GCOCCNO,
            GCCLMSEQ,
            CHDRNUM,
            PRODTYP,
            GCCLMTNO,
            GCDPNTNO,
            CLNTPFX,
            CLNTCOY,
            CLNTNUM,
            GCSTS,
            DTECLAM,
            CLAIMCUR,
            CRATE,
            PLANNO,
            FMLYCDE,
            GCRMK,
            BNKCHARGE,
            SPECTRM,
            GCDBLIND,
            GCFRPDTE,
            GCLRPDTE,
            GCDTHCLM,
            GCCAUSCD,
            GCOPPVCD,
            GCOPBNCD,
            GCOPIAMT,
            GCOPBAMT,
            GCOPBNPY,
            GCOPDGCD,
            GCOPPTCD,
            REQNBCDE,
            REQNTYPE,
            GCAUTHBY,
            DATEAUTH,
            GCOPRSCD,
            GCGRSPY,
            GCNETPY,
            GCCOINA,
            DEDUCTPC,
            GCADVPY,
            GCTRDRPY,
            MMFLG,
            PACCAMT,
            OVRLMT,
            DTEPYMTADV,
            DTEPYMTOTH,
            DTEATT,
            DTETRM,
            GCLOCCNO,
            REVLINK,
            REVIND,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            APAIDAMT,
            INTPAIDAMT,
            GCSETLMD,
            GCSECSTS,
            GRSKCLS,
            USRPRF,
            JOBNM,
            DATIME,
			STLBASIS
       FROM VM1DTA.GCMHPF

GO

------- GBID --------
ALTER VIEW [VM1DTA].[GBID] (
	UNIQUE_NUMBER
	,CHDRCOY
	,BILLNO
	,PRODTYP
	,PLANNO
	,CLASSINS
	,BPREM
	,BEXTPRM
	,BADVRFUND
	,BCOMM
	,BOVCOMM01
	,BOVCOMM02
	,DISCRATE
	,DISCAMT
	,BATCCOY
	,BATCBRN
	,BATCACTYR
	,BATCACTMN
	,BATCTRCDE
	,BATCBATCH
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,FEES
	,VALIDFLAG
	,WKLADM
	,USRPRF
	,JOBNM
	,DATIME
	,DISCAMT1  
	,DISCRATE1 
	,DISCAMT2  
	,DISCRATE2
	,ZCTAXAMT01
	,ZCTAXAMT02
	,ZCTAXAMT03 
	,RIBFEE
	,RIBFGST
	)
AS
SELECT UNIQUE_NUMBER
	,CHDRCOY
	,BILLNO
	,PRODTYP
	,PLANNO
	,CLASSINS
	,BPREM
	,BEXTPRM
	,BADVRFUND
	,BCOMM
	,BOVCOMM01
	,BOVCOMM02
	,DISCRATE
	,DISCAMT
	,BATCCOY
	,BATCBRN
	,BATCACTYR
	,BATCACTMN
	,BATCTRCDE
	,BATCBATCH
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,FEES
	,VALIDFLAG
	,WKLADM
	,USRPRF
	,JOBNM
	,DATIME
	,DISCAMT1  
	,DISCRATE1 
	,DISCAMT2  
	,DISCRATE2  
	,ZCTAXAMT01
	,ZCTAXAMT02
	,ZCTAXAMT03
	,RIBFEE
	,RIBFGST
FROM VM1DTA.GBIDPF
WHERE VALIDFLAG = '1';

GO

ALTER VIEW [VM1DTA].[GBIDALL] (
	UNIQUE_NUMBER
	,CHDRCOY
	,BILLNO
	,PRODTYP
	,PLANNO
	,CLASSINS
	,BPREM
	,BEXTPRM
	,BADVRFUND
	,BCOMM
	,BOVCOMM01
	,BOVCOMM02
	,DISCRATE
	,DISCAMT
	,BATCCOY
	,BATCBRN
	,BATCACTYR
	,BATCACTMN
	,BATCTRCDE
	,BATCBATCH
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,FEES
	,VALIDFLAG
	,WKLADM
	,USRPRF
	,JOBNM
	,DATIME
	,ZCTAXAMT01
	,ZCTAXAMT02
	,ZCTAXAMT03
	,RIBFEE
	,RIBFGST
	)
AS
SELECT UNIQUE_NUMBER
	,CHDRCOY
	,BILLNO
	,PRODTYP
	,PLANNO
	,CLASSINS
	,BPREM
	,BEXTPRM
	,BADVRFUND
	,BCOMM
	,BOVCOMM01
	,BOVCOMM02
	,DISCRATE
	,DISCAMT
	,BATCCOY
	,BATCBRN
	,BATCACTYR
	,BATCACTMN
	,BATCTRCDE
	,BATCBATCH
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,FEES
	,VALIDFLAG
	,WKLADM
	,USRPRF
	,JOBNM
	,DATIME
	,ZCTAXAMT01
	,ZCTAXAMT02
	,ZCTAXAMT03
	,RIBFEE
	,RIBFGST
FROM VM1DTA.GBIDPF;

GO

------- GBIDPLN --------
ALTER VIEW [VM1DTA].[GBIDPLN] AS 
SELECT UNIQUE_NUMBER, CHDRCOY,BILLNO,PRODTYP,PLANNO,CLASSINS,BPREM,BEXTPRM,
BADVRFUND,BCOMM,BOVCOMM01,BOVCOMM02,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,
BATCTRCDE,BATCBATCH,TERMID,USER_T,TRDT,TRTM,TRANNO,DISCRATE,DISCAMT,VALIDFLAG,
WKLADM,JOBNM,USRPRF,DATIME,ZCTAXAMT01,ZCTAXAMT02,ZCTAXAMT03,RIBFEE,RIBFGST
FROM GBIDPF  
WHERE VALIDFLAG='1' 


GO


------- GRII -----------
ALTER VIEW [VM1DTA].[GRII](
UNIQUE_NUMBER,
CHDRCOY,CHDRNUM,CEDCLNT,CEDPOLNO,CEDCESSNO,CEDCURR,CRATE,CEDFRDT,CEDTODT,JOBNM, 
USRPRF, DATIME,TRANNO,CEDAGENT,ACCNO,AGPC01,GNFLAG01,CMAMT01,RIBFGST,AGPC02,GNFLAG02,CMAMT02,
AGPC03,GNFLAG03,CMAMT03,ORISI,OURSIPER,OURSI) AS
SELECT 
UNIQUE_NUMBER,
CHDRCOY,CHDRNUM,CEDCLNT,CEDPOLNO,CEDCESSNO,CEDCURR,CRATE,CEDFRDT, CEDTODT,JOBNM,
USRPRF,DATIME,TRANNO,CEDAGENT,ACCNO,AGPC01,GNFLAG01,CMAMT01,RIBFGST,AGPC02,GNFLAG02,CMAMT02,
AGPC03,GNFLAG03,CMAMT03,ORISI,OURSIPER,OURSI
FROM VM1DTA.GRIIPF

GO
-------------GRIICOIN---------
CREATE VIEW [VM1DTA].[GRIICOIN](
UNIQUE_NUMBER,
CHDRCOY,CHDRNUM,CEDCLNT,CEDPOLNO,CEDCESSNO,CEDCURR,CRATE,CEDFRDT,CEDTODT,JOBNM, 
USRPRF, DATIME,TRANNO,CEDAGENT,ACCNO,AGPC01,GNFLAG01,CMAMT01,RIBFGST,AGPC02,GNFLAG02,CMAMT02,
AGPC03,GNFLAG03,CMAMT03,ORISI,OURSIPER,OURSI) AS
SELECT 
UNIQUE_NUMBER,
CHDRCOY,CHDRNUM,CEDCLNT,CEDPOLNO,CEDCESSNO,CEDCURR,CRATE,CEDFRDT, CEDTODT,JOBNM,
USRPRF,DATIME,TRANNO,CEDAGENT,ACCNO,AGPC01,GNFLAG01,CMAMT01,RIBFGST,AGPC02,GNFLAG02,CMAMT02,
AGPC03,GNFLAG03,CMAMT03,ORISI,OURSIPER,OURSI
FROM VM1DTA.GRIIPF

GO

------- GPMRMBR -----------
CREATE VIEW [VM1DTA].[GPMRMBR](
UNIQUE_NUMBER,
CHDRCOY, CHDRNUM, SUBSCOY,SUBSNUM, PRODTYP, PLANNO, MBRNO, DPNTNO, HEADCNTIND, TRANNO, DTEEFF, DTETER, RPFX, 
RCOY, RACC, RITYPE, CEDTYP, SEQNO, PRBILFDT, PRBILTDT, SACSCODE, SACSTYP01,SACSTYP02,SACSTYP03,SACSTYP04,SACSTYP05,SACSTYP06,
SACSTYP07,SACSTYP08,SACSTYP09,SACSTYP10,SACSTYP11,SACSTYP12,SACSTYP13,SACSTYP14,SACSTYP15, CURR, CRATE, EXTR01,
EXTR02,EXTR03,EXTR04,EXTR05,EXTR06,EXTR07,EXTR08,EXTR09,EXTR10,EXTR11,EXTR12,EXTR13,EXTR14,EXTR15,GSTRIAMT01,GSTRIAMT02,
GSTRIAMT03,GSTRIAMT04,GSTRIAMT05,GSTRIAMT06,GSTRIAMT07,GSTRIAMT08,GSTRIAMT09,GSTRIAMT10,GSTRIAMT11,GSTRIAMT12,GSTRIAMT13,
GSTRIAMT14,GSTRIAMT15, Z6TAXCDE01,Z6TAXCDE02,Z6TAXCDE03,Z6TAXCDE04,Z6TAXCDE05,Z6TAXCDE06,Z6TAXCDE07,Z6TAXCDE08,Z6TAXCDE09,
Z6TAXCDE10,Z6TAXCDE11,Z6TAXCDE12,Z6TAXCDE13,Z6TAXCDE14,Z6TAXCDE15, Z6PERCNT01,Z6PERCNT02,Z6PERCNT03,Z6PERCNT04,Z6PERCNT05,
Z6PERCNT06,Z6PERCNT07,Z6PERCNT08,Z6PERCNT09,Z6PERCNT10,Z6PERCNT11,Z6PERCNT12,Z6PERCNT13,Z6PERCNT14,Z6PERCNT15, EXTOUT, RIPPN, 
RECTYPE, BILLTYP, BILLNO, CNOTENUM, COTYPE, COPPN, USRPRF, JOBNM, DATIME) AS
SELECT UNIQUE_NUMBER, 
CHDRCOY, CHDRNUM, SUBSCOY, SUBSNUM, PRODTYP, PLANNO, MBRNO, DPNTNO, HEADCNTIND, TRANNO, DTEEFF, DTETER, RPFX,
RCOY, RACC, RITYPE, CEDTYP, SEQNO, PRBILFDT, PRBILTDT, SACSCODE, SACSTYP01,SACSTYP02,SACSTYP03,SACSTYP04,SACSTYP05,SACSTYP06,
SACSTYP07,SACSTYP08,SACSTYP09,SACSTYP10,SACSTYP11,SACSTYP12,SACSTYP13,SACSTYP14,SACSTYP15,  CURR, CRATE, EXTR01,
EXTR02,EXTR03,EXTR04,EXTR05,EXTR06,EXTR07,EXTR08,EXTR09,EXTR10,EXTR11,EXTR12,EXTR13,EXTR14,EXTR15,GSTRIAMT01,GSTRIAMT02,
GSTRIAMT03,GSTRIAMT04,GSTRIAMT05,GSTRIAMT06,GSTRIAMT07,GSTRIAMT08,GSTRIAMT09,GSTRIAMT10,GSTRIAMT11,GSTRIAMT12,GSTRIAMT13,
GSTRIAMT14,GSTRIAMT15, Z6TAXCDE01,Z6TAXCDE02,Z6TAXCDE03,Z6TAXCDE04,Z6TAXCDE05,Z6TAXCDE06,Z6TAXCDE07,Z6TAXCDE08,Z6TAXCDE09,
Z6TAXCDE10,Z6TAXCDE11,Z6TAXCDE12,Z6TAXCDE13,Z6TAXCDE14,Z6TAXCDE15, Z6PERCNT01,Z6PERCNT02,Z6PERCNT03,Z6PERCNT04,Z6PERCNT05,
Z6PERCNT06,Z6PERCNT07,Z6PERCNT08,Z6PERCNT09,Z6PERCNT10,Z6PERCNT11,Z6PERCNT12,Z6PERCNT13,Z6PERCNT14,Z6PERCNT15, EXTOUT, RIPPN, 
RECTYPE, BILLTYP, BILLNO, CNOTENUM, COTYPE, COPPN, USRPRF, JOBNM, DATIME
FROM VM1DTA.GPMRPF
Where HEADCNTIND = ' '
GO



/*
* HELPTEXT FOR SQ960
*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ960'
           ,''
           ,'1'
           ,''
           ,'1'
           ,'This screen displays information about '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ960'
           ,''
           ,'2'
           ,''
           ,'1'
           ,'loss account of co-insurer(s), reinsurer(s) '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ960'
           ,''
           ,'3'
           ,''
           ,'1'
           ,'with their own percentage from policy.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

/*
* HELPTEXT FOR SQ956
*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ956'
           ,''
           ,'1'
           ,''
           ,'1'
           ,'This screen is used to create/ maintain '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ956'
           ,''
           ,'2'
           ,''
           ,'1'
           ,'information about share, coinsurance '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 



INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ956'
           ,''
           ,'3'
           ,''
           ,'1'
           ,'account(s), commission, co-fee and other '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ956'
           ,''
           ,'4'
           ,''
           ,'1'
           ,'details of coinsurance outward and '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ956'
           ,''
           ,'5'
           ,''
           ,'1'
           ,'coinsurance inward policies'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 


/*
* HELPTEXT FOR SQ957
*/

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ957'
           ,''
           ,'1'
           ,''
           ,'1'
           ,'This screen display list of co-insurer(s) '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ957'
           ,''
           ,'2'
           ,''
           ,'1'
           ,'and/or reinsurer(s) with total premium, '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ957'
           ,''
           ,'3'
           ,''
           ,'1'
           ,'extra charges and net premium of them.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 


/*
* HELPTEXT FOR SQ958
*/

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ958'
           ,''
           ,'1'
           ,''
           ,'1'
           ,'This screen allows user to view details '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ958'
           ,''
           ,'2'
           ,''
           ,'1'
           ,'about premium, commission, extra '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ958'
           ,''
           ,'3'
           ,''
           ,'1'
           ,'charges for each selected account'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 


/*
* HELPTEXT FOR SQ959
*/

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ959'
           ,''
           ,'1'
           ,''
           ,'1'
           ,'This screen allows user to view details '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ959'
           ,''
           ,'2'
           ,''
           ,'1'
           ,'about premium, commission, extra '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ959'
           ,''
           ,'3'
           ,''
           ,'1'
           ,'charges, tax codes for each selected '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ959'
           ,''
           ,'4'
           ,''
           ,'1'
           ,'product'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

/*
* HELPTEXT S9352 
*/

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9352'
           ,'STLBASIS'
           ,'1'
           ,''
           ,'1'
           ,'An indicator which is used to denote whether insurer '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9352'
           ,'STLBASIS'
           ,'2'
           ,''
           ,'1'
           ,'company resolve that claim on their own share or full '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9352'
           ,'STLBASIS'
           ,'3'
           ,''
           ,'1'
           ,'amount of claim first and after that '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO  


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9352'
           ,'STLBASIS'
           ,'4'
           ,''
           ,'1'
           ,'recovery from coinsurance. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO  

/*
* HELPTEXT SR9BT 
*/

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9BT'
           ,'STLBASIS'
           ,'1'
           ,''
           ,'1'
           ,'An indicator which is used to denote whether insurer '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9BT'
           ,'STLBASIS'
           ,'2'
           ,''
           ,'1'
           ,'company resolve that claim on their own share or full '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9BT'
           ,'STLBASIS'
           ,'3'
           ,''
           ,'1'
           ,'amount of claim first and after that '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO  


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9BT'
           ,'STLBASIS'
           ,'4'
           ,''
           ,'1'
           ,'recovery from coinsurance. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO  


/*
* HELPTEXT SR9JU 
*/


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9JU'
           ,'RACCS'
           ,'1'
           ,''
           ,'1'
           ,'Account number of the Cedant in a '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO  


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9JU'
           ,'RACCS'
           ,'2'
           ,''
           ,'1'
           ,'Facultative Inward policy.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO  


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9JU'
           ,'RACC'
           ,'1'
           ,''
           ,'1'
           ,'Account number of the Broker if '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO  

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9JU'
           ,'RACC'
           ,'2'
           ,''
           ,'1'
           ,'inward policy is through RI Broker'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO  

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9JU'
           ,'AGPC'
           ,'1'
           ,''
           ,'1'
           ,'Fee charged by the Facultative RI Broker'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO  


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9JU'
           ,'GNFLAG'
           ,'1'
           ,''
           ,'1'
           ,'Calculation method of RI Brokerage Fee'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO  

/*
* HELPTEXT SQ960
*/

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'chdrnum'
           ,'1'
           ,''
           ,'1'
           ,'Policy number'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'ownername'
           ,'1'
           ,''
           ,'1'
           ,'Policy owner'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'prodtyp'
           ,'1'
           ,''
           ,'1'
           ,'Product code'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO	

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'prodtdesc'
           ,'1'
           ,''
           ,'1'
           ,'Product description'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO	


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'crdate'
           ,'1'
           ,''
           ,'1'
           ,'Period of cover of policy.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO	

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'ccdate'
           ,'1'
           ,''
           ,'1'
           ,'Period of cover of policy.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO	

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'clamnum'
           ,'1'
           ,''
           ,'1'
           ,'Claim number'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO	

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'clcurr'
           ,'1'
           ,''
           ,'1'
           ,'Claim Currency'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'gcoccno'
           ,'1'
           ,''
           ,'1'
           ,'Claim occurrence number'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'datocc'
           ,'1'
           ,''
           ,'1'
           ,'Incurred  date or date of visit'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'tarr'
           ,'1'
           ,''
           ,'1'
           ,'RI Treaty Arrangement code'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'tadesc'
           ,'1'
           ,''
           ,'1'
           ,'RI Treaty Arrangement description'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'racc'
           ,'1'
           ,''
           ,'1'
           ,'Co-insurer/ Reinsurer account'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'ritype'
           ,'1'
           ,''
           ,'1'
           ,'RI type'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'laccdesc'
           ,'1'
           ,''
           ,'1'
           ,'Co-insurer/ Reinsurer name'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'ricref'
           ,'1'
           ,''
           ,'1'
           ,'RI Claim Reference'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'rippn'
           ,'1'
           ,''
           ,'1'
           ,'Co-insurer/ reinsurer share.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'rbaltu'
           ,'1'
           ,''
           ,'1'
           ,'Balance outstanding amount'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'paymri'
           ,'1'
           ,''
           ,'1'
           ,'Paid amount'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'modxlw'
           ,'1'
           ,''
           ,'1'
           ,'Loss lower limit of Treaty Excess of loss account. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'modxlw'
           ,'2'
           ,''
           ,'1'
           ,'This will be checked against the upper limit to ensure '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'modxlw'
           ,'3'
           ,''
           ,'1'
           ,'that it represents a valid range. Overlapping excess of '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'modxlw'
           ,'4'
           ,''
           ,'1'
           ,'loss layers are not valid. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'modxup'
           ,'1'
           ,''
           ,'1'
           ,'Loss upper limit of Treaty Excess of loss account.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'modxup'
           ,'2'
           ,''
           ,'1'
           ,'This will be checked against the lower limit to ensure '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'modxup'
           ,'3'
           ,''
           ,'1'
           ,'that it represents a valid range. Overlapping excess of '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'modxup'
           ,'4'
           ,''
           ,'1'
           ,'loss layers are not allowed.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'gross'
           ,'1'
           ,''
           ,'1'
           ,'Total loss amount of claim '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'lossreco'
           ,'1'
           ,''
           ,'1'
           ,'Total loss amount of coinsurance/ reinsurance. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ960'
           ,'net'
           ,'1'
           ,''
           ,'1'
           ,'Total loss amount of insurer.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

/*
* HELPTEXT SQ956
*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ956'
           ,'CNTTYPE'
           ,'1'
           ,''
           ,'1'
           ,'Policy type'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ956'
           ,'CHDRNUM'
           ,'1'
           ,''
           ,'1'
           ,'Policy number'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ956'
           ,'CTYPDESC'
           ,'1'
           ,''
           ,'1'
           ,'Policy type description'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ956'
           ,'CCDATE'
           ,'1'
           ,''
           ,'1'
           ,'Period of insured'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ956'
           ,'CRDATE'
           ,'1'
           ,''
           ,'1'
           ,'Period of insured'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ956'
           ,'DTEEFF'
           ,'1'
           ,''
           ,'1'
           ,'Effective date of coinsurance.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ956'
           ,'GPPPN'
           ,'1'
           ,''
           ,'1'
           ,'Insurer percentage'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ956'
           ,'SCOITY'
           ,'1'
           ,''
           ,'1'
           ,'Insurer share type (Leader/ Follower)'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ956'
           ,'COACCT'
           ,'1'
           ,''
           ,'1'
           ,'Coinsurer account number'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ956'
           ,'COPPN'
           ,'1'
           ,''
           ,'1'
           ,'Coinsurer share in a policy.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ956'
           ,'COTYPE'
           ,'1'
           ,''
           ,'1'
           ,'Coinsurer share type (Leader/ Follower)'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ956'
           ,'GRNR'
           ,'1'
           ,''
           ,'1'
           ,'Premium calculation method of co-insurer(s)'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ956'
           ,'RICOMM'
           ,'1'
           ,''
           ,'1'
           ,'Exchange commission of co-insurer(s)'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ956'
           ,'OCP'
           ,'1'
           ,''
           ,'1'
           ,'Original Commission plus of co-insurer(s).'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ956'
           ,'COFEETYP'
           ,'1'
           ,''
           ,'1'
           ,'Premium calculation method of co-fee'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ956'
           ,'AGPC'
           ,'1'
           ,''
           ,'1'
           ,'Co-fee percentage.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ956'
           ,'RIREF'
           ,'1'
           ,''
           ,'1'
           ,'Coinsurer�s reference number.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

/*
* HELPTEXT SQ957
*/


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ957'
           ,'CNTTYPE'
           ,'1'
           ,''
           ,'1'
           ,'Policy type'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO




INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ957'
           ,'CHDRNUM'
           ,'1'
           ,''
           ,'1'
           ,'Policy number'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ957'
           ,'CTYPDESC'
           ,'1'
           ,''
           ,'1'
           ,'Policy type description'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ957'
           ,'OCCDATE'
           ,'1'
           ,''
           ,'1'
           ,'First inception date of policy.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ957'
           ,'COACCT'
           ,'1'
           ,''
           ,'1'
           ,'Co-insurer(s)/ Reinsurer(s) account'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ957'
           ,'LONGDESC'
           ,'1'
           ,''
           ,'1'
           ,'Transaction type description'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ957'
           ,'EFFDATE'
           ,'1'
           ,''
           ,'1'
           ,'Effective date'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ957'
           ,'RIGRSPREM'
           ,'1'
           ,''
           ,'1'
           ,'Total gross premium of co-insurer/ reinsurer in policy.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ957'
           ,'RICHARGE'
           ,'1'
           ,''
           ,'1'
           ,'Total extra charges amount '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ957'
           ,'RICHARGE'
           ,'2'
           ,''
           ,'1'
           ,'of co-insurer/ reinsurer in policy.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ957'
           ,'RINETPREM'
           ,'1'
           ,''
           ,'1'
           ,'Total net premium of co-insurer/ reinsurer in policy.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

/*
* HELPTEXT SQ958
*/


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ958'
           ,'CHDRNUM'
           ,'1'
           ,''
           ,'1'
           ,'Policy number'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ958'
           ,'OWNERNAME'
           ,'1'
           ,''
           ,'1'
           ,'Insured name'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ958'
           ,'AGNTNUM'
           ,'1'
           ,''
           ,'1'
           ,'Agent number'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ958'
           ,'LONGNAME'
           ,'1'
           ,''
           ,'1'
           ,'Agent name'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ958'
           ,'CNTTYPE'
           ,'1'
           ,''
           ,'1'
           ,'Policy type'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ958'
           ,'CTYPDESC'
           ,'1'
           ,''
           ,'1'
           ,'Policy type description'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ958'
           ,'BRANCH'
           ,'1'
           ,''
           ,'1'
           ,'Branch name'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ958'
           ,'STATCODE'
           ,'1'
           ,''
           ,'1'
           ,'Policy status code'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ958'
           ,'OCCDATE'
           ,'1'
           ,''
           ,'1'
           ,'First inception date of policy'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ958'
           ,'COACCT'
           ,'1'
           ,''
           ,'1'
           ,'Coinsurer/ Reinsurer account number'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ958'
           ,'ACCNAME'
           ,'1'
           ,''
           ,'1'
           ,'Coinsurer/ Reinsurer name'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ958'
           ,'PRODTYP'
           ,'1'
           ,''
           ,'1'
           ,'Product code'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ958'
           ,'LONGDESC'
           ,'1'
           ,''
           ,'1'
           ,'Product name'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ958'
           ,'PLANNO'
           ,'1'
           ,''
           ,'1'
           ,'Plan number'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ958'
           ,'RIGRSPREM'
           ,'1'
           ,''
           ,'1'
           ,'Gross premium of co-insurer/ reinsurer on a product.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ958'
           ,'RICHARGE'
           ,'1'
           ,''
           ,'1'
           ,'Extra charges of co-insurer/ reinsurer on a product.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ958'
           ,'COMAMT'
           ,'1'
           ,''
           ,'1'
           ,'Commission amount of co-insurer/ reinsurer on a product.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

/*
* HELPTEXT SQ959
*/

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ959'
           ,'DTEEFF'
           ,'1'
           ,''
           ,'1'
           ,'Effective date of that transaction.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ959'
           ,'CHDRNUM'
           ,'1'
           ,''
           ,'1'
           ,'Policy number'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ959'
           ,'OWNERNAME'
           ,'1'
           ,''
           ,'1'
           ,'Insured name'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ959'
           ,'LONGDESC'
           ,'1'
           ,''
           ,'1'
           ,'Transaction type'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ959'
           ,'PRODTYP'
           ,'1'
           ,''
           ,'1'
           ,'Product code'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ959'
           ,'BILLCURR'
           ,'1'
           ,''
           ,'1'
           ,'Billing currency'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ959'
           ,'SCRATE'
           ,'1'
           ,''
           ,'1'
           ,'Exchange rate of billing currency'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ959'
           ,'COACCT'
           ,'1'
           ,''
           ,'1'
           ,'Co-insurer/ Reinsurer account'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ959'
           ,'LONGNAME'
           ,'1'
           ,''
           ,'1'
           ,'Co-insurer/ Reinsurer name'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ959'
           ,'SHORTDS'
           ,'1'
           ,''
           ,'1'
           ,'Extra charges description'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ959'
           ,'CALMETH'
           ,'1'
           ,''
           ,'1'
           ,'Extra charges code identify'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ959'
           ,'EXEXTR'
           ,'1'
           ,''
           ,'1'
           ,'Posted amount of premium, extra charges (if any)'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO



INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ959'
           ,'TOEXTR'
           ,'1'
           ,''
           ,'1'
           ,'Total posted premium and extra charges (if any)'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ959'
           ,'GSTRIAMT'
           ,'1'
           ,''
           ,'1'
           ,'Total GST on posted premium and extra charges (if any).'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ959'
           ,'AGEXTR'
           ,'1'
           ,''
           ,'1'
           ,'Exchange commission'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ959'
           ,'OCEXTR'
           ,'1'
           ,''
           ,'1'
           ,'Original Commission plus'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ959'
           ,'Z6TAXCDE'
           ,'1'
           ,''
           ,'1'
           ,'GST tax code for premium, '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ959'
           ,'Z6TAXCDE'
           ,'2'
           ,''
           ,'1'
           ,'extra charges (if any) and commission.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO


