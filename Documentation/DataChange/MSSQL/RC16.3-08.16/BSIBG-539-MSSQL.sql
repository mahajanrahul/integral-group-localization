CREATE TABLE [VM1DTA].[GCRBPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[CLAMNUM] [nchar](8) NULL,
	[PRODTYP] [nchar](4) NULL,
	[RESCODE] [nchar](4) NULL,	
	[RSVAMT] [numeric](17, 2) NULL,
	[TOTRSVPTD] [numeric](17, 2) NULL,
	[BALANCE] [numeric](17, 2) NULL,	
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL,
 CONSTRAINT [PK_GCRBPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE VIEW [VM1DTA].[GCRB](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, CLAMNUM, PRODTYP, RESCODE, RSVAMT, TOTRSVPTD, BALANCE, USRPRF, JOBNM, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            CLAMNUM,
            PRODTYP,
            RESCODE,
            RSVAMT,
            TOTRSVPTD,
            BALANCE,
           
            USRPRF,
            JOBNM,
            DATIME
			
       FROM VM1DTA.GCRBPF
GO

ALTER TABLE [VM1DTA].[GCMHPF] ADD 
ROPENRSN NCHAR(3) NULL
GO
------ GCMH --------
ALTER VIEW [VM1DTA].[GCMH](UNIQUE_NUMBER, CHDRCOY, CLAMNUM, GCOCCNO, GCCLMSEQ, CHDRNUM, PRODTYP, GCCLMTNO, GCDPNTNO, CLNTPFX, CLNTCOY, CLNTNUM, GCSTS, DTECLAM, CLAIMCUR, CRATE, PLANNO, FMLYCDE, GCRMK, BNKCHARGE, SPECTRM, GCDBLIND, GCFRPDTE, GCLRPDTE, GCDTHCLM, GCCAUSCD, GCOPPVCD, GCOPBNCD, GCOPIAMT, GCOPBAMT, GCOPBNPY, GCOPDGCD, GCOPPTCD, REQNBCDE, REQNTYPE, GCAUTHBY, DATEAUTH, GCOPRSCD, GCGRSPY, GCNETPY, GCCOINA, DEDUCTPC, GCADVPY, GCTRDRPY, GCLOCCNO, MMFLG, PACCAMT, OVRLMT, DTEPYMTADV, DTEPYMTOTH, DTEATT, DTETRM, REVLINK, REVIND, TERMID, USER_T, TRDT, TRTM, APAIDAMT, INTPAIDAMT, GCSETLMD, GCSECSTS, ZCLTCLMREF, ZCLMRECD, ZCLNTPROC, ZCLMREF, ZFEECURR, ZFEETYP, ZCALCFEE, ZOVRDFEE, GRSKCLS, BENCDE, CLAIMCOND, OUTLOAN, USRPRF, JOBNM, DATIME, DIACODE, ONDUTY, ROPENRSN) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CLAMNUM,
            GCOCCNO,
            GCCLMSEQ,
            CHDRNUM,
            PRODTYP,
            GCCLMTNO,
            GCDPNTNO,
            CLNTPFX,
            CLNTCOY,
            CLNTNUM,
            GCSTS,
            DTECLAM,
            CLAIMCUR,
            CRATE,
            PLANNO,
            FMLYCDE,
            GCRMK,
            BNKCHARGE,
            SPECTRM,
            GCDBLIND,
            GCFRPDTE,
            GCLRPDTE,
            GCDTHCLM,
            GCCAUSCD,
            GCOPPVCD,
            GCOPBNCD,
            GCOPIAMT,
            GCOPBAMT,
            GCOPBNPY,
            GCOPDGCD,
            GCOPPTCD,
            REQNBCDE,
            REQNTYPE,
            GCAUTHBY,
            DATEAUTH,
            GCOPRSCD,
            GCGRSPY,
            GCNETPY,
            GCCOINA,
            DEDUCTPC,
            GCADVPY,
            GCTRDRPY,
            GCLOCCNO,
            MMFLG,
            PACCAMT,
            OVRLMT,
            DTEPYMTADV,
            DTEPYMTOTH,
            DTEATT,
            DTETRM,
            REVLINK,
            REVIND,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            APAIDAMT,
            INTPAIDAMT,
            GCSETLMD,
            GCSECSTS,
            ZCLTCLMREF,
            ZCLMRECD,
            ZCLNTPROC,
            ZCLMREF,
            ZFEECURR,
            ZFEETYP,
            ZCALCFEE,
            ZOVRDFEE,
            GRSKCLS,
            BENCDE,
            CLAIMCOND,
            OUTLOAN,
            USRPRF,
            JOBNM,
            DATIME,
			DIACODE,
			ONDUTY,
			ROPENRSN			

       FROM VM1DTA.GCMHPF



GO


ALTER TABLE [VM1DTA].[GCPYPF] ADD 
	RESCODE NCHAR(4) NULL
GO

/****** Object:  View [VM1DTA].[GCPYRES]    Script Date: 7/30/2016 11:06:52 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [VM1DTA].[GCPYRES](UNIQUE_NUMBER, CHDRCOY, CLAMNUM, GCOCCNO, RESCODE, GCPYSEQ, PAYMTYP, INSTLNO, PAYCOY, PAYCLT, GCTXNAMT, REQNTYPE, REQNBCDE, CHEQNO, OCHEQNO, BRANCH, REQNNO, ZPAYTYP, TERMID, USER_T, TRDT, TRTM, JOBNM, USRPRF, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CLAMNUM,
            GCOCCNO,
            RESCODE,
            GCPYSEQ,
			PAYMTYP,
            INSTLNO,
            PAYCOY,
            PAYCLT,
            GCTXNAMT,
            REQNTYPE,
            REQNBCDE,
            CHEQNO,
            OCHEQNO,
            BRANCH,
            REQNNO,
            ZPAYTYP,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            JOBNM,
            USRPRF,
            DATIME

       FROM VM1DTA.GCPYPF  
GO

/****** Object:  View [VM1DTA].[GCPY]    Script Date: 7/30/2016 1:44:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [VM1DTA].[GCPY](UNIQUE_NUMBER, CHDRCOY, CLAMNUM, GCOCCNO, INSTLNO, PAYMTYP, GCPYSEQ, PAYCOY, PAYCLT, GCTXNAMT, REQNTYPE, REQNBCDE, CHEQNO, OCHEQNO, BRANCH, REQNNO, ZPAYTYP, TERMID, USER_T, TRDT, TRTM, USRPRF, JOBNM, DATIME, RESCODE) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CLAMNUM,
            GCOCCNO,
            INSTLNO,
            PAYMTYP,
            GCPYSEQ,
            PAYCOY,
            PAYCLT,
            GCTXNAMT,
            REQNTYPE,
            REQNBCDE,
            CHEQNO,
            OCHEQNO,
            BRANCH,
            REQNNO,
            ZPAYTYP,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            USRPRF,
            JOBNM,
            DATIME,
			RESCODE
       FROM VM1DTA.GCPYPF


GO


ALTER TABLE [VM1DTA].[GCBLPF] ADD 
	GCCLMTNO NCHAR(5) NULL,
	GCDPNTNO NCHAR(2) NULL
GO

ALTER VIEW [VM1DTA].[GCBL](UNIQUE_NUMBER, CHDRCOY,CHDRNUM,PRODTYP,PLANNO,CLAMNUM,GBENCDE,GCCLMTNO,GCDPNTNO,GCLAIMAMT,HRSFROM,MINUTFROM,TIMETYP1,HRSTO,MINUTTO,TIMETYP2,TRANNO,USRPRF,JOBNM,DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
			CHDRNUM,
			PRODTYP,
			PLANNO,
			CLAMNUM,
			GBENCDE,
			GCCLMTNO, 
			GCDPNTNO,
			GCLAIMAMT,
			HRSFROM,
			MINUTFROM,
			TIMETYP1,
			HRSTO,
			MINUTTO,
			TIMETYP2,
			TRANNO,
            USRPRF,
            JOBNM,
            DATIME
       FROM VM1DTA.GCBLPF


GO

ALTER TABLE [VM1DTA].[CBNLPF] ADD 
	GCCLMTNO NCHAR(5) NULL,
	GCDPNTNO NCHAR(2) NULL
GO

ALTER VIEW [VM1DTA].[CBNL](UNIQUE_NUMBER, CHDRCOY,CHDRNUM,PRODTYP,PLANNO,CLAMNUM,BENCDE,GBENCDE,GCCLMTNO,GCDPNTNO,GCLAIMAMT,HRSFROM,MINUTFROM,TIMETYP1,HRSTO,MINUTTO,TIMETYP2,TRANNO,USRPRF,JOBNM,DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
			CHDRNUM,
			PRODTYP,
			PLANNO,
			CLAMNUM,
			BENCDE,
			GBENCDE,
			GCCLMTNO,
			GCDPNTNO,
			GCLAIMAMT,
			HRSFROM,
			MINUTFROM,
			TIMETYP1,
			HRSTO,
			MINUTTO,
			TIMETYP2,
			TRANNO,
            USRPRF,
            JOBNM,
            DATIME
       FROM VM1DTA.CBNLPF


GO
/* ERROR */
	INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFSW'
           ,'Invalid Reserve Code'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');


	INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFSY'
           ,'Breakdown reserve amount not balance'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');


	INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFSV'
           ,'No Claim Reserve'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');

	INSERT INTO [VM1DTA].[ERORPF]
    ([ERORPFX]
    ,[ERORCOY]
    ,[ERORLANG]
    ,[ERORPROG]
    ,[EROREROR]
    ,[ERORDESC]
    ,[TRDT]
    ,[TRTM]
    ,[USERID]
    ,[TERMINALID]
    ,[USRPRF]
    ,[JOBNM]
    ,[DATIME]
    ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFSU'
           ,'Status not CW/CR'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');


	INSERT INTO [VM1DTA].[ERORPF]
    ([ERORPFX]
    ,[ERORCOY]
    ,[ERORLANG]
    ,[ERORPROG]
    ,[EROREROR]
    ,[ERORDESC]
    ,[TRDT]
    ,[TRTM]
    ,[USERID]
    ,[TERMINALID]
    ,[USRPRF]
    ,[JOBNM]
    ,[DATIME]
    ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFST'
           ,'Status not CC'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');

	INSERT INTO [VM1DTA].[ERORPF]
    ([ERORPFX]
    ,[ERORCOY]
    ,[ERORLANG]
    ,[ERORPROG]
    ,[EROREROR]
    ,[ERORDESC]
    ,[TRDT]
    ,[TRTM]
    ,[USERID]
    ,[TERMINALID]
    ,[USRPRF]
    ,[JOBNM]
    ,[DATIME]
    ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFT2'
           ,'Not enough reserves'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');



	INSERT INTO [VM1DTA].[ERORPF]
    ([ERORPFX]
    ,[ERORCOY]
    ,[ERORLANG]
    ,[ERORPROG]
    ,[EROREROR]
    ,[ERORDESC]
    ,[TRDT]
    ,[TRTM]
    ,[USERID]
    ,[TERMINALID]
    ,[USRPRF]
    ,[JOBNM]
    ,[DATIME]
    ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFSX'
           ,'Reserve amount need to be clear'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');


	INSERT INTO [VM1DTA].[ERORPF]
    ([ERORPFX]
    ,[ERORCOY]
    ,[ERORLANG]
    ,[ERORPROG]
    ,[EROREROR]
    ,[ERORDESC]
    ,[TRDT]
    ,[TRTM]
    ,[USERID]
    ,[TERMINALID]
    ,[USRPRF]
    ,[JOBNM]
    ,[DATIME]
    ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFT1'
           ,'Adjust Reserve Codes Breakdown'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');


 INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
('HP',' ','E','S','SQ9A4','','1','','1','Reserve Code Breakdown Screen','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
('HP',' ','E','F','SQ9A4','CHDRNUM',1,'',1,'Policy number.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ9A4','OWNERNAME',1,'',1,'The diagnosis code for claim','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ9A4','PRODTYP',1,'',1,'This is the Product code.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ9A4','PRODNAME',1,'',1,'This is the Product name.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ9A4','RSVAMT',1,'',1,'The reserve amount for the respective Res-Code.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ9A4','RESCODE',1,'',1,'This is the Claim Reserve code','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ9A4','CLAMNUM',1,'',1,'Group claim number.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ9A4','GCOCCNO',1,'',1,'This is the gcoc no.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP)

GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
('HP',' ','E','F','','RESVCODE',1,'',1,'This is the Claim Reserve code.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','S9355','RESCODE01',1,'',1,'This is the Claim Reserve code.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','S9355','RESCODE02',1,'',1,'This is the Claim Reserve code.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','S9355','RESCODE03',1,'',1,'This is the Claim Reserve code.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','S9355','RESCODE04',1,'',1,'This is the Claim Reserve code.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','S9355','RESCODE05',1,'',1,'This is the Claim Reserve code.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','S9355','RESCODE06',1,'',1,'This is the Claim Reserve code.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','S9355','RESCODE07',1,'',1,'This is the Claim Reserve code.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','S9355','RESCODE08',1,'',1,'This is the Claim Reserve code.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP)

GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
('HP',' ','E','F','S9366','CLRREVES',5,'',1,'Indicate reserves are to be cleared','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP)

GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
('HP',' ','E','F','S9352','ROPENRSN',5,'',1,'Indicate the reopen reason.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP)

GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
('HP',' ','E','S','SQ9A5','','1','','1','Claim Reopen','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
('HP',' ','E','F','SQ9A5','CLMNM',2,'',1,'This is the Group claim number.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F',' ','CLMNM',1,'',1,'This is the Group claim number.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ9A5','GCSTS',3,'',1,'This is the Claim Status code','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ9A5','CHDRNUM',4,'',1,'This is the Policy number','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ9A5','PRODTYP',5,'',1,'This is the Product code','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ9A5','GCCLMTNO',6,'',1,'This is the claimant�s member no.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ9A5','GCDPNTNO',7,'',1,'This is the claimant�s dependent no.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ9A5','GCGRSPY',8,'',1,'This is the Total Payable amount','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ9A5','ROPENRSN',9,'',1,'This is Claim Reopen Reason','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ9A5','DTECLAM',10,'',1,'This is the Claim Date','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP)

GO




INSERT INTO [VM1DTA].[FLDDPF]
           ([FDID]
           ,[NMPG]
           ,[TYPE_T]
           ,[EDIT]
           ,[LENF]
           ,[DECP]
           ,[WNTP]
           ,[WNST]
           ,[TABW]
           ,[TCOY]
           ,[ERRCD]
           ,[PROG01]
           ,[PROG02]
           ,[PROG03]
           ,[PROG04]
           ,[CFID]
           ,[TERMID]
           ,[USER_T]
           ,[TRDT]
           ,[TRTM]
           ,[ADDF01]
           ,[ADDF02]
           ,[ADDF03]
           ,[ADDF04]
           ,[CURRFDID]
           ,[ALLOCLEN]
           ,[DBDATIME]
           ,[SFDATIME]
           ,[DBCSCAP]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('RESVCODE  '
           ,'RESVCODE  '
           ,'A'
           ,''
           ,4
           ,0
           ,'I'
           ,'S'
           ,'TR9DQ'
           ,'S'
           ,'RFSW'
           ,'P0127'
           ,''
           ,''
           ,''
           ,''
           ,''
           ,0
           ,860101
           ,120000
           ,''
           ,''
           ,''
           ,''
           ,''
           ,0
           ,CURRENT_TIMESTAMP
           ,CURRENT_TIMESTAMP
           ,'N'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[FLDTPF]
           ([FDID]
           ,[LANG]
           ,[FDTX]
           ,[COLH01]
           ,[COLH02]
           ,[COLH03]
           ,[TERMID]
           ,[USER_T]
           ,[TRDT]
           ,[TRTM]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('RESVCODE  '
           ,'E'
           ,'CLAIM RESERVE CODE'
           ,'Claim'
           ,'Reserve             '
           ,'Code                '
           ,'QPAD'
           ,1
           ,424
           ,152922
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP)
GO