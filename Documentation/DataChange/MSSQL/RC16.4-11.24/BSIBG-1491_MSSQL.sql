BEGIN TRANSACTION
IF EXISTS (SELECT * FROM VM1DTA.ERORPF WHERE EROREROR = 'RFWJ' and ERORLANG = 'E' and ERORPFX = 'ER')
BEGIN
    UPDATE VM1DTA.ERORPF SET ERORDESC = 'Enter Doc Received Date' WHERE EROREROR = 'RFWJ' and ERORLANG = 'E' and ERORPFX = 'ER';
END
ELSE
BEGIN
    INSERT INTO VM1DTA.ERORPF
           (ERORPFX
           ,ERORCOY
           ,ERORLANG
           ,ERORPROG
           ,EROREROR
           ,ERORDESC
           ,TRDT
           ,TRTM
           ,USERID
           ,TERMINALID
           ,USRPRF
           ,JOBNM
           ,DATIME
           ,ERORFILE)
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFWJ'
           ,'Enter Doc Received Date'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
END
COMMIT TRANSACTION;
GO
