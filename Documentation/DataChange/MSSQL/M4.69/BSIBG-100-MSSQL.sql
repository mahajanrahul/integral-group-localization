USE [INT77DB2014_TG2_SON]
GO

/****** Object:  Table [VM1DTA].[FWHDPF]    Script Date: 4/5/2016 2:55:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [VM1DTA].[FWHDPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[CNTTYPE] [nchar](3) NULL,
	[EMPID] [nchar](12) NULL,
	[IMMIGCODE] [nchar](8) NULL,
	[CCDATE] [int] NULL,
	[CRDATE] [int] NULL,
	[PPERIOD] [numeric](2,0) NULL,
	[IGGRPREF] [nchar](2) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL);
Go

CREATE VIEW [VM1DTA].[FWHD](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, CNTTYPE, EMPID, IMMIGCODE, CCDATE, CRDATE, PPERIOD, IGGRPREF, JOBNM, DATIME) AS
SELECT UNIQUE_NUMBER,CHDRCOY, CHDRNUM, CNTTYPE, EMPID, IMMIGCODE, CCDATE, CRDATE, PPERIOD, IGGRPREF, JOBNM, DATIME
       FROM VM1DTA.FWHDPF;
Go

INSERT INTO [VM1DTA].[FLDD]
           ([FDID]
           ,[NMPG]
           ,[TYPE_T]
           ,[EDIT]
           ,[LENF]
           ,[DECP]
           ,[WNTP]
           ,[WNST]
           ,[TABW]
           ,[TCOY]
           ,[ERRCD]
           ,[PROG01]
           ,[PROG02]
           ,[PROG03]
           ,[PROG04]
           ,[CFID]
           ,[TERMID]
           ,[USER_T]
           ,[TRDT]
           ,[TRTM]
           ,[ADDF01]
           ,[ADDF02]
           ,[ADDF03]
           ,[ADDF04]
           ,[CURRFDID]
           ,[ALLOCLEN]
           ,[DBCSCAP]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
select     'IMMIGSEL  '
           ,'IMMIGSEL                '
           ,[TYPE_T]
           ,[EDIT]
           ,[LENF]
           ,[DECP]
           ,[WNTP]
           ,[WNST]
           ,[TABW]
           ,[TCOY]
           ,[ERRCD]
           ,[PROG01]
           ,[PROG02]
           ,[PROG03]
           ,[PROG04]
           ,'IMMIGNAME '
           ,[TERMID]
           ,[USER_T]
           ,[TRDT]
           ,[TRTM]
           ,[ADDF01]
           ,[ADDF02]
           ,[ADDF03]
           ,[ADDF04]
           ,[CURRFDID]
           ,[ALLOCLEN]
           ,[DBCSCAP]
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,[DATIME] from fldd where fdid='cownsel';
go

INSERT INTO [VM1DTA].[FLDD]
           ([FDID]
           ,[NMPG]
           ,[TYPE_T]
           ,[EDIT]
           ,[LENF]
           ,[DECP]
           ,[WNTP]
           ,[WNST]
           ,[TABW]
           ,[TCOY]
           ,[ERRCD]
           ,[PROG01]
           ,[PROG02]
           ,[PROG03]
           ,[PROG04]
           ,[CFID]
           ,[TERMID]
           ,[USER_T]
           ,[TRDT]
           ,[TRTM]
           ,[ADDF01]
           ,[ADDF02]
           ,[ADDF03]
           ,[ADDF04]
           ,[CURRFDID]
           ,[ALLOCLEN]
           ,[DBCSCAP]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
select     'IMMIGNAME '
           ,'IMMIGNAME               '
           ,[TYPE_T]
           ,[EDIT]
           ,[LENF]
           ,[DECP]
           ,[WNTP]
           ,[WNST]
           ,[TABW]
           ,[TCOY]
           ,[ERRCD]
           ,[PROG01]
           ,[PROG02]
           ,[PROG03]
           ,[PROG04]
           ,[CFID]
           ,[TERMID]
           ,[USER_T]
           ,[TRDT]
           ,[TRTM]
           ,[ADDF01]
           ,[ADDF02]
           ,[ADDF03]
           ,[ADDF04]
           ,[CURRFDID]
           ,[ALLOCLEN]
           ,[DBCSCAP]
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,[DATIME] from fldd where fdid='cownname';
go

INSERT INTO [VM1DTA].[FLDD]
           ([FDID]
           ,[NMPG]
           ,[TYPE_T]
           ,[EDIT]
           ,[LENF]
           ,[DECP]
           ,[WNTP]
           ,[WNST]
           ,[TABW]
           ,[TCOY]
           ,[ERRCD]
           ,[PROG01]
           ,[PROG02]
           ,[PROG03]
           ,[PROG04]
           ,[CFID]
           ,[TERMID]
           ,[USER_T]
           ,[TRDT]
           ,[TRTM]
           ,[ADDF01]
           ,[ADDF02]
           ,[ADDF03]
           ,[ADDF04]
           ,[CURRFDID]
           ,[ALLOCLEN]
           ,[DBCSCAP]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
select     'EMPSEL    '
           ,'EMPSEL                  '
           ,[TYPE_T]
           ,[EDIT]
           ,[LENF]
           ,[DECP]
           ,[WNTP]
           ,[WNST]
           ,[TABW]
           ,[TCOY]
           ,[ERRCD]
           ,[PROG01]
           ,'PQ976'
           ,'PQ977'
           ,[PROG04]
           ,[CFID]
           ,[TERMID]
           ,[USER_T]
           ,[TRDT]
           ,[TRTM]
           ,[ADDF01]
           ,[ADDF02]
           ,[ADDF03]
           ,[ADDF04]
           ,[CURRFDID]
           ,[ALLOCLEN]
           ,[DBCSCAP]
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,[DATIME] from fldd where fdid='cownsel';
go

INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFS9'
           ,'EmployerID not exists'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
go

INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFQ9'
           ,'Invalid POI'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');


