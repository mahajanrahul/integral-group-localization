INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
VALUES ('HP','','E','S','SQ921','','1','','1','Foreign Worker Fee Screen','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','S','SQ922','','1','','1','Mandatory Fields by Policy/Product Type Screen','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ921','FEE','1','','1','The fee (SF for FWCS or MCOF for FWHS) charged per worker.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ922','YNFLG01','1','','1','Industry Code -> Policy Header (Sg004).','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ922','YNFLG02','1','','1','Employer Group -> Policy Header(Sg004)','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ922','YNFLG03','1','','1','Occupation Code -> Group Member Entry(S9106)','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ922','YNFLG04','1','','1','Occupation Class -> Group Member Entry(S9106)','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ922','YNFLG05','1','','1','Insured For -> Group Member Entry(S9106)','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ922','YNFLG06','1','','1','Insured Status -> Group Member Entry(S9106)','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ922','YNFLG07','1','','1','Employment Site -> Group Member Entry(S9106)','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ922','YNFLG08','1','','1','Immigration Code -> Policy Header(Sg004)','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ922','YNFLG09','1','','1','LOG Number -> Policy Header(Sg004)','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ922','YNFLG10','1','','1','Employment details -> Group Member (S9106)','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ922','YNFLG11','1','','1','Work Permit Effective Date -> Group Member (S9106)','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ922','YNFLG12','1','','1','Work Permit Expiry Date -> Group Member (S9106)','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ922','YNFLG13','1','','1','Work Permit No. -> Group Member (S9106)','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ922','YNFLG14','1','','1','FWCMS Reference No. -> Group Member Detail(S9106)','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','S9106','INSFOR','1','','1','This is used to store the insured for code.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','S9106','INSSTS','1','','1','This is used to store the insured status code.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','S9106','PLACEMP','1','','1','This is used to store the place of employment details.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','S9106','PMTDTE','1','','1','This is used to store the effective date of work permit.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','S9106','PMEXDT','1','','1','This is used to store the expiry date of work permit.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','S9106','PERMIT','1','','1','This is used to store the work permit number.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','S9106','FWCMSREF','1','','1','This is used to store the FWCMS reference no.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP)

/*[FWMDPF] table*/

CREATE TABLE [VM1DTA].[FWMDPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[MBRNO] [nchar](5) NULL,
	[DPNTNO] [nchar](2) NULL,
	[INSFOR] [nchar](1) NULL,
	[INSSTS] [nchar](1) NULL,
	[PLACEMP] [nchar](30) NULL,
	[PMTDTE] [int] NULL,
	[PMEXDT] [int] NULL,
	[PERMIT] [nchar](20) NULL,
	[FWCMSREF] [nchar](20) NULL,
	[USRPRF]  [nchar](10) NULL,
	[JOBNM]   [nchar](10) NULL,
	[DATIME]  [datetime2](6) NULL,
	
 CONSTRAINT [PK_FWMDPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/*[FWMD] view*/
CREATE VIEW [VM1DTA].[FWMD] (
	UNIQUE_NUMBER,
	CHDRCOY,
	CHDRNUM,
	MBRNO,
	DPNTNO,
	INSFOR,
	INSSTS,
	PLACEMP,
	PMTDTE,
	PMEXDT,
	PERMIT,
	FWCMSREF,
	USRPRF,
	JOBNM,
	DATIME
	)
AS
SELECT UNIQUE_NUMBER,
	CHDRCOY,
	CHDRNUM,
	MBRNO,
	DPNTNO,
	INSFOR,
	INSSTS,
	PLACEMP,
	PMTDTE,
	PMEXDT,
	PERMIT,
	FWCMSREF,
	USRPRF,
	JOBNM,
	DATIME
FROM VM1DTA.FWMDPF
GO