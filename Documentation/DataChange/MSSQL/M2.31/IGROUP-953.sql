CREATE VIEW [PAXUS].[LEVL] (UNIQUE_NUMBER, USERID, LLEVEL, PROGTOCALL, PROGTP, LANGUAGE) AS 
  SELECT   unique_number, userid, llevel, progtocall, progtp, LANGUAGE
       FROM PAXUS.levlpf