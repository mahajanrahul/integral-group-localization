
CREATE TABLE [VM1DTA].[GBKIPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[BILLNO] [int] NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[SUBSCOY] [nchar](1) NULL,
	[SUBSNUM] [nchar](8) NULL,
	[BILLTYP] [nchar](1) NULL,
	[PRBILFDT] [int] NULL,
	[PRBILTDT] [int] NULL,
	[TERMID] [nchar](4) NULL,
	[USER_T] [int] NULL,
	[TRDT] [int] NULL,
	[TRTM] [int] NULL,
	[TRANNO] [int] NULL,
	[INSTNO] [int] NULL,
	[PBILLNO] [int] NULL,
	[GRPGST] [numeric](9, 2) NULL,
	[GRPSDUTY] [numeric](9, 2) NULL,
	[VALIDFLAG] [nchar](1) NULL,
	[BILFLAG] [nchar](1) NULL,
	[RDOCPFX] [nchar](2) NULL,
	[RDOCCOY] [nchar](1) NULL,
	[RDOCNUM] [nchar](9) NULL,
	[NRFLG] [nchar](1) NULL,
	[TGTPCNT] [numeric](5, 2) NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL,
	[MBRNO] [nchar](5) NULL,
	[PREMOUT] [nchar](1) NULL,
	[BILLDUEDT] [int] NULL,
	[REVFLAG] [nchar](1) NULL,
	[ZGSTCOM] [numeric](9, 2) NULL,
	[ZGSTAFEE] [numeric](9, 2) NULL,
 CONSTRAINT [PK_GBKIPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
CREATE TABLE [VM1DTA].[GBDTPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[BILLNO] [int] NULL,
	[PRODTYP] [nchar](4) NULL,
	[PLANNO] [nchar](3) NULL,
	[CLASSINS] [nchar](2) NULL,
	[BPREM] [numeric](15, 2) NULL,
	[BEXTPRM] [numeric](15, 2) NULL,
	[BADVRFUND] [numeric](9, 2) NULL,
	[BCOMM] [numeric](13, 2) NULL,
	[BOVCOMM01] [numeric](9, 2) NULL,
	[BOVCOMM02] [numeric](9, 2) NULL,
	[BATCCOY] [nchar](1) NULL,
	[BATCBRN] [nchar](2) NULL,
	[BATCACTYR] [int] NULL,
	[BATCACTMN] [int] NULL,
	[BATCTRCDE] [nchar](4) NULL,
	[BATCBATCH] [nchar](5) NULL,
	[TERMID] [nchar](4) NULL,
	[USER_T] [int] NULL,
	[TRDT] [int] NULL,
	[TRTM] [int] NULL,
	[TRANNO] [int] NULL,
	[DISCRATE] [numeric](5, 2) NULL,
	[DISCAMT] [numeric](11, 2) NULL,
	[FEES] [numeric](15, 2) NULL,
	[VALIDFLAG] [nchar](1) NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL,
	[WKLADM] [numeric](13, 2) NULL,
	[DISCAMT1] [numeric](11, 2) NULL,
	[DISCRATE1] [numeric](5, 2) NULL,
	[DISCAMT2] [numeric](11, 2) NULL,
	[DISCRATE2] [numeric](5, 2) NULL,
	[CHDRNUM] [nchar](8) NULL,
 CONSTRAINT [PK_GBDTPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



------------ create view GBDT ----------------


CREATE VIEW [VM1DTA].[GBDT] (
	UNIQUE_NUMBER
	,CHDRCOY
	,BILLNO
	,PRODTYP
	,PLANNO
	,CLASSINS
	,BPREM
	,BEXTPRM
	,BADVRFUND
	,BCOMM
	,BOVCOMM01
	,BOVCOMM02
	,DISCRATE
	,DISCAMT
	,BATCCOY
	,BATCBRN
	,BATCACTYR
	,BATCACTMN
	,BATCTRCDE
	,BATCBATCH
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,FEES
	,VALIDFLAG
	,WKLADM
	,USRPRF
	,JOBNM
	,DATIME
	,DISCAMT1  
	,DISCRATE1 
	,DISCAMT2  
	,DISCRATE2 
	,CHDRNUM
	)
AS
SELECT UNIQUE_NUMBER
	,CHDRCOY
	,BILLNO
	,PRODTYP
	,PLANNO
	,CLASSINS
	,BPREM
	,BEXTPRM
	,BADVRFUND
	,BCOMM
	,BOVCOMM01
	,BOVCOMM02
	,DISCRATE
	,DISCAMT
	,BATCCOY
	,BATCBRN
	,BATCACTYR
	,BATCACTMN
	,BATCTRCDE
	,BATCBATCH
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,FEES
	,VALIDFLAG
	,WKLADM
	,USRPRF
	,JOBNM
	,DATIME
	,DISCAMT1  
	,DISCRATE1 
	,DISCAMT2  
	,DISCRATE2 
	,CHDRNUM
FROM VM1DTA.GBDTPF
WHERE VALIDFLAG = '1';



GO




CREATE TABLE [VM1DTA].[GIMDPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NOT NULL,
	[CHDRNUM] [nchar](8) NOT NULL,
	[CVNOTE] [nchar](8) NULL,
	[PRODTYP] [nchar](4) NULL,
	[SECUITYNO] [nchar](24) NULL,
	[NPPREM] [numeric](15, 2) NULL,
	[NFEES] [numeric](15, 2) NULL,
	[NTOTPREM] [numeric](15, 2) NULL,
	[TRANTYP] [nchar](1) NULL,
	[TRANNO] [int] NULL,
	[NPPREMAF] [numeric](15, 2) NULL,
	[NFEESAF] [numeric](15, 2) NULL,
	[NTOTPREMAF] [numeric](15, 2) NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL,
 CONSTRAINT [PK_GIMDPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [VM1DTA].[GIPMPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NOT NULL,
	[CHDRNUM] [nchar](8) NOT NULL,
	[CVNOTE] [nchar](8) NULL,
	[CNTTYPE] [nchar](3) NULL,
	[GPREM] [numeric](15, 2) NULL,
	[NFEES] [numeric](15, 2) NULL,
	[NSDUTY] [numeric](15, 2) NULL,
	[NGRAMT] [numeric](15, 2) NULL,
	[NBCOMM] [numeric](15, 2) NULL,
	[NETPRAMT] [numeric](15, 2) NULL,
	[GSTAMT] [numeric](15, 2) NULL,
	[TRANTYP] [nchar](1) NULL,
	[TRANNO] [int] NULL,
	[GPREMAF] [numeric](15, 2) NULL,
	[NFEESAF] [numeric](15, 2) NULL,
	[NSDUTYAF] [numeric](15, 2) NULL,
	[NGRAMTAF] [numeric](15, 2) NULL,
	[NBCOMMAF] [numeric](15, 2) NULL,
	[NETPRAMTAF] [numeric](15, 2) NULL,
	[GSTAMTAF] [numeric](15, 2) NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL,
 CONSTRAINT [PK_GIPMPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



CREATE VIEW [VM1DTA].[GIPM](
						UNIQUE_NUMBER, 
						CHDRCOY,  
						CHDRNUM,  
						CVNOTE,  
						CNTTYPE,  
						GPREM,  
						NFEES,  
						NSDUTY,  
						NGRAMT,  
						NBCOMM,  
						NETPRAMT,  
						GSTAMT,  
						TRANTYP,  
						TRANNO,  
						GPREMAF,  
						NFEESAF,  
						NSDUTYAF,  
						NGRAMTAF,  
						NBCOMMAF,  
						NETPRAMTAF,  
						GSTAMTAF,  
						USRPRF,  
						JOBNM,  
						DATIME
		) AS
SELECT UNIQUE_NUMBER,
		 CHDRCOY,  
						CHDRNUM,  
						CVNOTE,  
						CNTTYPE,  
						GPREM,  
						NFEES,  
						NSDUTY,  
						NGRAMT,  
						NBCOMM,  
						NETPRAMT,  
						GSTAMT,  
						TRANTYP,  
						TRANNO,  
						GPREMAF,  
						NFEESAF,  
						NSDUTYAF,  
						NGRAMTAF,  
						NBCOMMAF,  
						NETPRAMTAF,  
						GSTAMTAF,  
						USRPRF,  
						JOBNM,  
						DATIME
       FROM VM1DTA.GIPMPF

GO


CREATE VIEW [VM1DTA].[GIMD](
							UNIQUE_NUMBER, 
							CHDRCOY,  
							CHDRNUM,  
							CVNOTE,  
							PRODTYP,  
							SECUITYNO,  
							NPPREM,  
							NFEES,  
							NTOTPREM,  
							TRANTYP,  
							TRANNO,  
							NPPREMAF,  
							NFEESAF,  
							NTOTPREMAF,  
							USRPRF,  
							JOBNM,  
							DATIME
) AS
SELECT UNIQUE_NUMBER,
		  CHDRCOY,  
							CHDRNUM,  
							CVNOTE,  
							PRODTYP,  
							SECUITYNO,  
							NPPREM,  
							NFEES,  
							NTOTPREM,  
							TRANTYP,  
							TRANNO,  
							NPPREMAF,  
							NFEESAF,  
							NTOTPREMAF,  
							USRPRF,  
							JOBNM,  
							DATIME
       FROM VM1DTA.GIMDPF

GO

CREATE TABLE [VM1DTA].[CLPHPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NOT NULL,
	[CHDRNUM] [nchar](8) NOT NULL,
	[CLTADDR01] [nchar](60) NULL,
	[CLTADDR02] [nchar](60) NULL,
	[CLTADDR03] [nchar](60) NULL,
	[CLTADDR04] [nchar](60) NULL,
	[CLTADDR05] [nchar](60) NULL,
	[CLTADDR06] [nchar](60) NULL,
	[CLTADDR07] [nchar](60) NULL,
	[CLTADDR08] [nchar](60) NULL,
	[CLTADDR09] [nchar](60) NULL,
	[CLTADDR10] [nchar](60) NULL,
	[TRANNO] [int] NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL,
 CONSTRAINT [PK_CLPHPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



CREATE VIEW [VM1DTA].[CLPH](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, CLTADDR01,CLTADDR02,CLTADDR03,CLTADDR04,CLTADDR05,CLTADDR06,CLTADDR07,CLTADDR08,CLTADDR09,CLTADDR10, TRANNO, USRPRF, JOBNM,DATIME) AS
SELECT UNIQUE_NUMBER,
		CHDRCOY, 
		CHDRNUM, 
		CLTADDR01,
		CLTADDR02,
		CLTADDR03,
		CLTADDR04,
		CLTADDR05,
		CLTADDR06,
		CLTADDR07,
		CLTADDR08,
		CLTADDR09,
		CLTADDR10, 
		TRANNO, 
		USRPRF, 
		JOBNM,
		DATIME
       FROM VM1DTA.CLPHPF
GO


CREATE  VIEW [VM1DTA].[GBKI](UNIQUE_NUMBER, BILLNO, CHDRCOY, CHDRNUM, SUBSCOY, SUBSNUM, MBRNO, BILLTYP, PRBILFDT, PRBILTDT, INSTNO, PBILLNO, TERMID, USER_T, TRDT, TRTM, TRANNO, GRPGST, GRPSDUTY, VALIDFLAG, BILFLAG, RDOCPFX, RDOCCOY, RDOCNUM, NRFLG, TGTPCNT, PREMOUT, BILLDUEDT, REVFLAG, USRPRF, JOBNM, DATIME, ZGSTCOM, ZGSTAFEE) AS
SELECT UNIQUE_NUMBER,
            BILLNO,
            CHDRCOY,
            CHDRNUM,
            SUBSCOY,
            SUBSNUM,
            MBRNO,
            BILLTYP,
            PRBILFDT,
            PRBILTDT,
            INSTNO,
            PBILLNO,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            TRANNO,
            GRPGST,
            GRPSDUTY,
            VALIDFLAG,
            BILFLAG,
            RDOCPFX,
            RDOCCOY,
            RDOCNUM,
            NRFLG,
            TGTPCNT,
            PREMOUT,
            BILLDUEDT,
            REVFLAG,
            USRPRF,
            JOBNM,
            DATIME,
			ZGSTCOM,
			ZGSTAFEE
       FROM VM1DTA.GBKIPF
      WHERE VALIDFLAG = '1'



GO

ALTER TABLE  [VM1DTA].[GLPDPF]
ALTER COLUMN [LONGPLAN01] [nchar](60) NULL;

ALTER TABLE  [VM1DTA].[GLPDPF]
ALTER COLUMN [LONGPLAN02] [nchar](60) NULL;

ALTER TABLE  [VM1DTA].[GLPDPF]
ALTER COLUMN [LONGPLAN03] [nchar](60) NULL;

ALTER TABLE  [VM1DTA].[GLPDPF]
ALTER COLUMN [LONGPLAN04] [nchar](60) NULL;

ALTER TABLE  [VM1DTA].[GLPDPF]
ALTER COLUMN [LONGPLAN05] [nchar](60) NULL;



CREATE TABLE [VM1DTA].[GPBKPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[PRODTYP] [nchar](4) NULL,
	[HEADCNTIND] [nchar](1) NULL,
	[MBRNO] [nchar](5) NULL,
	[DPNTNO] [nchar](2) NULL,
	[TRANNO] [int] NULL,
	[RECNO] [int] NULL,
	[PLANNO] [nchar](3) NULL,
	[SUBSCOY] [nchar](1) NULL,
	[SUBSNUM] [nchar](8) NULL,
	[BILLTYP] [nchar](1) NULL,
	[BILLNO] [int] NULL,
	[EFFDATE] [int] NULL,
	[PPREM] [numeric](15, 2) NULL,
	[PEMXTPRM] [numeric](8, 2) NULL,
	[POAXTPRM] [numeric](8, 2) NULL,
	[INSTNO] [int] NULL,
	[PRMFRDT] [int] NULL,
	[PRMTODT] [int] NULL,
	[PNIND] [nchar](1) NULL,
	[MMIND] [nchar](1) NULL,
	[SRCDATA] [nchar](1) NULL,
	[BATCCOY] [nchar](1) NULL,
	[BATCBRN] [nchar](2) NULL,
	[BATCACTYR] [int] NULL,
	[BATCACTMN] [int] NULL,
	[BATCTRCD] [nchar](4) NULL,
	[BATCBATCH] [nchar](5) NULL,
	[RECTYPE] [nchar](1) NULL,
	[JOBNOUD] [int] NULL,
	[FLATFEE] [numeric](15, 2) NULL,
	[FEES] [numeric](15, 2) NULL,
	[EVNTFEE] [numeric](11, 2) NULL,
	[JOBNOISS] [int] NULL,
	[BBJOBNO] [int] NULL,
	[MFJOBNO] [int] NULL,
	[JOBNOTPA] [int] NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL,
 CONSTRAINT [PK_GPBKPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



CREATE VIEW [VM1DTA].[GPBK](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, PRODTYP, HEADCNTIND, MBRNO, DPNTNO, TRANNO, RECNO, PLANNO, SUBSCOY, SUBSNUM, BILLTYP, BILLNO, EFFDATE, PPREM, PEMXTPRM, POAXTPRM, INSTNO, PRMFRDT, PRMTODT, PNIND, MMIND, SRCDATA, BATCCOY, BATCBRN, BATCACTYR, BATCACTMN, BATCTRCD, BATCBATCH, RECTYPE, JOBNOUD, FLATFEE, FEES, EVNTFEE, MFJOBNO, JOBNOISS, BBJOBNO, JOBNOTPA, USRPRF, JOBNM, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            PRODTYP,
            HEADCNTIND,
            MBRNO,
            DPNTNO,
            TRANNO,
            RECNO,
            PLANNO,
            SUBSCOY,
            SUBSNUM,
            BILLTYP,
            BILLNO,
            EFFDATE,
            PPREM,
            PEMXTPRM,
            POAXTPRM,
            INSTNO,
            PRMFRDT,
            PRMTODT,
            PNIND,
            MMIND,
            SRCDATA,
            BATCCOY,
            BATCBRN,
            BATCACTYR,
            BATCACTMN,
            BATCTRCD,
            BATCBATCH,
            RECTYPE,
            JOBNOUD,
            FLATFEE,
            FEES,
            EVNTFEE,
            MFJOBNO,
            JOBNOISS,
            BBJOBNO,
            JOBNOTPA,
            USRPRF,
            JOBNM,
            DATIME
       FROM VM1DTA.GPBKPF


GO


ALTER TABLE  [VM1DTA].[RMEDPF]
ALTER COLUMN [OWNERNAME] [nchar](60) NULL;


ALTER TABLE  [VM1DTA].[GOWLPF]
ALTER COLUMN [RGSHTDES01] [nchar](60) NULL

ALTER TABLE  [VM1DTA].[GOWLPF]
ALTER COLUMN [RGSHTDES02] [nchar](60) NULL

ALTER TABLE  [VM1DTA].[GOWLPF]
ALTER COLUMN [RGSHTDES03] [nchar](60) NULL

ALTER TABLE  [VM1DTA].[GOWLPF]
ALTER COLUMN [RGSHTDES04] [nchar](60) NULL

ALTER TABLE  [VM1DTA].[GOWLPF]
ALTER COLUMN [RGSHTDES05] [nchar](60) NULL 


CREATE TABLE [VM1DTA].[ZTBKPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[BATCPFX] [nchar](2) NULL,
	[BATCCOY] [nchar](1) NULL,
	[BATCBRN] [nchar](2) NULL,
	[BATCACTYR] [int] NULL,
	[BATCACTMN] [int] NULL,
	[BATCTRCDE] [nchar](4) NULL,
	[BATCBATCH] [nchar](5) NULL,
	[RLDGPFX] [nchar](2) NULL,
	[RLDGCOY] [nchar](1) NULL,
	[RLDGACCT] [nchar](16) NULL,
	[ORIGCURR] [nchar](3) NULL,
	[ACCTCURR] [nchar](3) NULL,
	[CRATE] [numeric](18, 9) NULL,
	[CNTTYPE] [nchar](3) NULL,
	[TRANNO] [int] NULL,
	[CNTBRANCH] [nchar](2) NULL,
	[STCA] [nchar](3) NULL,
	[STCB] [nchar](3) NULL,
	[STCC] [nchar](3) NULL,
	[STCD] [nchar](3) NULL,
	[STCE] [nchar](3) NULL,
	[POSTMONTH] [nchar](2) NULL,
	[POSTYEAR] [nchar](4) NULL,
	[TRANDATE] [int] NULL,
	[TRANTIME] [int] NULL,
	[EFFDATE] [int] NULL,
	[SACSCODE] [nchar](2) NULL,
	[SACSTYP01] [nchar](2) NULL,
	[SACSTYP02] [nchar](2) NULL,
	[SACSTYP03] [nchar](2) NULL,
	[SACSTYP04] [nchar](2) NULL,
	[SACSTYP05] [nchar](2) NULL,
	[SACSTYP06] [nchar](2) NULL,
	[SACSTYP07] [nchar](2) NULL,
	[SACSTYP08] [nchar](2) NULL,
	[SACSTYP09] [nchar](2) NULL,
	[SACSTYP10] [nchar](2) NULL,
	[SACSTYP11] [nchar](2) NULL,
	[SACSTYP12] [nchar](2) NULL,
	[SACSTYP13] [nchar](2) NULL,
	[SACSTYP14] [nchar](2) NULL,
	[SACSTYP15] [nchar](2) NULL,
	[TRANAMT01] [numeric](17, 2) NULL,
	[TRANAMT02] [numeric](17, 2) NULL,
	[TRANAMT03] [numeric](17, 2) NULL,
	[TRANAMT04] [numeric](17, 2) NULL,
	[TRANAMT05] [numeric](17, 2) NULL,
	[TRANAMT06] [numeric](17, 2) NULL,
	[TRANAMT07] [numeric](17, 2) NULL,
	[TRANAMT08] [numeric](17, 2) NULL,
	[TRANAMT09] [numeric](17, 2) NULL,
	[TRANAMT10] [numeric](17, 2) NULL,
	[TRANAMT11] [numeric](17, 2) NULL,
	[TRANAMT12] [numeric](17, 2) NULL,
	[TRANAMT13] [numeric](17, 2) NULL,
	[TRANAMT14] [numeric](17, 2) NULL,
	[TRANAMT15] [numeric](17, 2) NULL,
	[GLSIGN01] [nchar](1) NULL,
	[GLSIGN02] [nchar](1) NULL,
	[GLSIGN03] [nchar](1) NULL,
	[GLSIGN04] [nchar](1) NULL,
	[GLSIGN05] [nchar](1) NULL,
	[GLSIGN06] [nchar](1) NULL,
	[GLSIGN07] [nchar](1) NULL,
	[GLSIGN08] [nchar](1) NULL,
	[GLSIGN09] [nchar](1) NULL,
	[GLSIGN10] [nchar](1) NULL,
	[GLSIGN11] [nchar](1) NULL,
	[GLSIGN12] [nchar](1) NULL,
	[GLSIGN13] [nchar](1) NULL,
	[GLSIGN14] [nchar](1) NULL,
	[GLSIGN15] [nchar](1) NULL,
	[GENLCUR] [nchar](3) NULL,
	[GENLPFX] [nchar](2) NULL,
	[GENLCOY] [nchar](1) NULL,
	[GLCODE01] [nchar](14) NULL,
	[GLCODE02] [nchar](14) NULL,
	[GLCODE03] [nchar](14) NULL,
	[GLCODE04] [nchar](14) NULL,
	[GLCODE05] [nchar](14) NULL,
	[GLCODE06] [nchar](14) NULL,
	[GLCODE07] [nchar](14) NULL,
	[GLCODE08] [nchar](14) NULL,
	[GLCODE09] [nchar](14) NULL,
	[GLCODE10] [nchar](14) NULL,
	[GLCODE11] [nchar](14) NULL,
	[GLCODE12] [nchar](14) NULL,
	[GLCODE13] [nchar](14) NULL,
	[GLCODE14] [nchar](14) NULL,
	[GLCODE15] [nchar](14) NULL,
	[ACPFX] [nchar](2) NULL,
	[ACCOY] [nchar](1) NULL,
	[ACNUM] [nchar](8) NULL,
	[CCDATE] [int] NULL,
	[EXDATE] [int] NULL,
	[LIFE] [nchar](2) NULL,
	[COVERAGE] [nchar](2) NULL,
	[RIDER] [nchar](2) NULL,
	[ACCTYP] [nchar](2) NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL,
	[ORAGNT] [nchar](8) NULL,
	[BANKCODE] [nchar](2) NULL,
	[DOCTPFX] [nchar](2) NULL,
	[DOCTCOY] [nchar](1) NULL,
	[DOCTNUM] [nchar](8) NULL,
	[INSTNO] [int] NULL,
	[BILLFREQ] [nchar](2) NULL,
	[STATREASN] [nchar](2) NULL,
	[PRTFLG] [nchar](2) NULL,
	[RITYPE] [nchar](2) NULL,
	[RDOCPFX] [nchar](2) NULL,
	[RDOCCOY] [nchar](1) NULL,
	[RDOCNUM] [nchar](9) NULL,
	[NOSCHCPY] [int] NULL,
 CONSTRAINT [PK_ZTBKPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


------------ create view ZTBK---------------------

CREATE VIEW [VM1DTA].[ZTBK](UNIQUE_NUMBER, BATCPFX, BATCCOY, BATCBRN, BATCACTYR, BATCACTMN, BATCTRCDE, BATCBATCH, RLDGPFX, RLDGCOY, RLDGACCT, ORIGCURR, ACCTCURR, CRATE, CNTTYPE, TRANNO, CNTBRANCH, STCA, STCB, STCC, STCD, STCE, POSTMONTH, POSTYEAR, TRANDATE, TRANTIME, EFFDATE, SACSCODE, SACSTYP01, SACSTYP02, SACSTYP03, SACSTYP04, SACSTYP05, SACSTYP06, SACSTYP07, SACSTYP08, SACSTYP09, SACSTYP10, SACSTYP11, SACSTYP12, SACSTYP13, SACSTYP14, SACSTYP15, TRANAMT01, TRANAMT02, TRANAMT03, TRANAMT04, TRANAMT05, TRANAMT06, TRANAMT07, TRANAMT08, TRANAMT09, TRANAMT10, TRANAMT11, TRANAMT12, TRANAMT13, TRANAMT14, TRANAMT15, GLSIGN01, GLSIGN02, GLSIGN03, GLSIGN04, GLSIGN05, GLSIGN06, GLSIGN07, GLSIGN08, GLSIGN09, GLSIGN10, GLSIGN11, GLSIGN12, GLSIGN13, GLSIGN14, GLSIGN15, GENLCUR, GENLPFX, GENLCOY, GLCODE01, GLCODE02, GLCODE03, GLCODE04, GLCODE05, GLCODE06, GLCODE07, GLCODE08, GLCODE09, GLCODE10, GLCODE11, GLCODE12, GLCODE13, GLCODE14, GLCODE15, ACPFX, ACCOY, ACNUM, CCDATE, EXDATE, LIFE, COVERAGE, RIDER, ACCTYP, ORAGNT, BANKCODE, DOCTPFX, DOCTCOY, DOCTNUM, INSTNO, BILLFREQ, STATREASN, PRTFLG, RITYPE, RDOCPFX, RDOCCOY, RDOCNUM, NOSCHCPY, JOBNM, USRPRF, DATIME) AS
SELECT UNIQUE_NUMBER,
            BATCPFX,
            BATCCOY,
            BATCBRN,
            BATCACTYR,
            BATCACTMN,
            BATCTRCDE,
            BATCBATCH,
            RLDGPFX,
            RLDGCOY,
            RLDGACCT,
            ORIGCURR,
            ACCTCURR,
            CRATE,
            CNTTYPE,
            TRANNO,
            CNTBRANCH,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            POSTMONTH,
            POSTYEAR,
            TRANDATE,
            TRANTIME,
            EFFDATE,
            SACSCODE,
            SACSTYP01,
            SACSTYP02,
            SACSTYP03,
            SACSTYP04,
            SACSTYP05,
            SACSTYP06,
            SACSTYP07,
            SACSTYP08,
            SACSTYP09,
            SACSTYP10,
            SACSTYP11,
            SACSTYP12,
            SACSTYP13,
            SACSTYP14,
            SACSTYP15,
            TRANAMT01,
            TRANAMT02,
            TRANAMT03,
            TRANAMT04,
            TRANAMT05,
            TRANAMT06,
            TRANAMT07,
            TRANAMT08,
            TRANAMT09,
            TRANAMT10,
            TRANAMT11,
            TRANAMT12,
            TRANAMT13,
            TRANAMT14,
            TRANAMT15,
            GLSIGN01,
            GLSIGN02,
            GLSIGN03,
            GLSIGN04,
            GLSIGN05,
            GLSIGN06,
            GLSIGN07,
            GLSIGN08,
            GLSIGN09,
            GLSIGN10,
            GLSIGN11,
            GLSIGN12,
            GLSIGN13,
            GLSIGN14,
            GLSIGN15,
            GENLCUR,
            GENLPFX,
            GENLCOY,
            GLCODE01,
            GLCODE02,
            GLCODE03,
            GLCODE04,
            GLCODE05,
            GLCODE06,
            GLCODE07,
            GLCODE08,
            GLCODE09,
            GLCODE10,
            GLCODE11,
            GLCODE12,
            GLCODE13,
            GLCODE14,
            GLCODE15,
            ACPFX,
            ACCOY,
            ACNUM,
            CCDATE,
            EXDATE,
            LIFE,
            COVERAGE,
            RIDER,
            ACCTYP,
            ORAGNT,
            BANKCODE,
            DOCTPFX,
            DOCTCOY,
            DOCTNUM,
            INSTNO,
            BILLFREQ,
            STATREASN,
            PRTFLG,
            RITYPE,
            RDOCPFX,
            RDOCCOY,
            RDOCNUM,
            NOSCHCPY,
            JOBNM,
            USRPRF,
            DATIME

       FROM VM1DTA.ZTBKPF


GO

CREATE TABLE [VM1DTA].[GTXNPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NOT NULL,
	[CHDRNUM] [nchar](8) NOT NULL,
	[CVNOTE] [nchar](8) NULL,
	[TXINCDE] [nchar](2) NULL,
	[CNTTYPE] [nchar](3) NULL,
	[TRANNO] [int] NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL,
 CONSTRAINT [PK_GTXNPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


CREATE VIEW [VM1DTA].[GTXN](
	UNIQUE_NUMBER,
	CHDRCOY,
	CHDRNUM,
	CVNOTE,
	TXINCDE,
	CNTTYPE,
	TRANNO,
	USRPRF,
	JOBNM,
	DATIME) AS
	SELECT UNIQUE_NUMBER,
	CHDRCOY,
	CHDRNUM,
	CVNOTE,
	TXINCDE,
	CNTTYPE,
	TRANNO,
	USRPRF,
	JOBNM,
	DATIME FROM VM1DTA.GTXNPF

GO


