/****** Object:  Table [VM1DTA].[GMAPPF] ******/
CREATE TABLE [VM1DTA].[GMAPPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[SUBSCOY] [nchar](1) NULL,
	[SUBSNUM] [nchar](8) NULL,
	[PRODTYP] [nchar](4) NULL,
	[PLANNO] [nchar](3) NULL,
	[MBRNO] [nchar](5) NULL,
	[DPNTNO] [nchar](2) NULL,
	[HEADCNTIND] [nchar](1) NULL,
	[TRANNO] [int] NULL,
	[DTEEFF] [int] NULL,
	[DTETER] [int] NULL,
	[VFLAG] [nchar](1) NULL,
	[TARR] [nchar](4) NULL,
	[ACURR] [nchar](3) NULL,
	[RIRETBAS] [nchar](1) NULL,
	[CEDEBS] [nchar](1) NULL,
	[SIORIG] [numeric](17, 0) NULL,
	[SILCE] [numeric](17, 0) NULL,
	[SIEXP] [numeric](17, 0) NULL,
	[SIEXPLCE] [numeric](17, 0) NULL,
	[SICURR] [nchar](3) NULL,
	[SIRAT] [numeric](18, 9) NULL,
	[PML] [numeric](5, 2) NULL,
	[NRPPN] [numeric](11, 8) NULL,
	[NRLCE] [numeric](17, 0) NULL,
	[NRORIG] [numeric](17, 0) NULL,
	[NRPML] [numeric](5, 2) NULL,
	[SQSPPN] [numeric](10, 7) NULL,
	[ZQSPML] [numeric](5, 2) NULL,
	[SRIPPN01] [numeric](10, 7) NULL,
	[SRIPPN02] [numeric](10, 7) NULL,
	[SRIPPN03] [numeric](10, 7) NULL,
	[SRIPPN04] [numeric](10, 7) NULL,
	[SRIPPN05] [numeric](10, 7) NULL,
	[SRIPPN06] [numeric](10, 7) NULL,
	[ZSPLCE01] [numeric](17, 0) NULL,
	[ZSPLCE02] [numeric](17, 0) NULL,
	[ZSPLCE03] [numeric](17, 0) NULL,
	[ZSPLCE04] [numeric](17, 0) NULL,
	[ZSPLCE05] [numeric](17, 0) NULL,
	[ZSPLCE06] [numeric](17, 0) NULL,
	[ZSPORIG01] [numeric](17, 0) NULL,
	[ZSPORIG02] [numeric](17, 0) NULL,
	[ZSPORIG03] [numeric](17, 0) NULL,
	[ZSPORIG04] [numeric](17, 0) NULL,
	[ZSPORIG05] [numeric](17, 0) NULL,
	[ZSPORIG06] [numeric](17, 0) NULL,
	[ZSPPML01] [numeric](5, 2) NULL,
	[ZSPPML02] [numeric](5, 2) NULL,
	[ZSPPML03] [numeric](5, 2) NULL,
	[ZSPPML04] [numeric](5, 2) NULL,
	[ZSPPML05] [numeric](5, 2) NULL,
	[ZSPPML06] [numeric](5, 2) NULL,
	[GRPPN] [numeric](11, 8) NULL,
	[GRLCE] [numeric](17, 0) NULL,
	[GRORIG] [numeric](17, 0) NULL,
	[ZGOVPPN] [numeric](10, 7) NULL,
	[ZGOVLCE] [numeric](17, 0) NULL,
	[ZGOVORIG] [numeric](17, 0) NULL,
	[ZGOVPML] [numeric](5, 2) NULL,
	[ZFACPPN] [numeric](10, 7) NULL,
	[ZFACLCE] [numeric](17, 0) NULL,
	[ZFACORIG] [numeric](17, 0) NULL,
	[ZGALPPN] [numeric](10, 7) NULL,
	[ZGALLCE] [numeric](17, 0) NULL,
	[ZGALORIG] [numeric](17, 0) NULL,
	[TXOLLCE] [numeric](17, 0) NULL,
	[TXOLORIG] [numeric](17, 0) NULL,
	[TXOLPRM] [numeric](17, 2) NULL,
	[TTLCE] [numeric](17, 0) NULL,
	[TTORIG] [numeric](17, 0) NULL,
	[COTYPE] [nchar](1) NULL,
	[COPPN] [numeric](11, 8) NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL)
	
Go

CREATE VIEW [VM1DTA].[GMAP](
UNIQUE_NUMBER, 
CHDRCOY, 
CHDRNUM, 
SUBSCOY, 
SUBSNUM, 
PRODTYP, 
PLANNO, 
MBRNO, 
DPNTNO, 
HEADCNTIND, 
TRANNO, 
DTEEFF, 
DTETER, 
VFLAG, 
TARR, 
ACURR, 
RIRETBAS, 
CEDEBS, 
SIORIG, 
SILCE, 
SIEXP, 
SIEXPLCE, 
SICURR, 
SIRAT, 
PML, 
NRPPN, 
NRLCE, 
NRORIG, 
NRPML, 
SQSPPN, 
ZQSPML, 
SRIPPN01,
SRIPPN02,
SRIPPN03,
SRIPPN04,
SRIPPN05,
SRIPPN06, 
ZSPLCE01,
ZSPLCE02,
ZSPLCE03,
ZSPLCE04,
ZSPLCE05,
ZSPLCE06, 
ZSPORIG01,
ZSPORIG02,
ZSPORIG03,
ZSPORIG04,
ZSPORIG05,
ZSPORIG06, 
ZSPPML01,
ZSPPML02,
ZSPPML03,
ZSPPML04,
ZSPPML05,
ZSPPML06, 
GRPPN, 
GRLCE, 
GRORIG, 
ZGOVPPN, 
ZGOVLCE, 
ZGOVORIG, 
ZGOVPML, 
ZFACPPN, 
ZFACLCE, 
ZFACORIG, 
ZGALPPN, 
ZGALLCE, 
ZGALORIG, 
TXOLLCE, 
TXOLORIG, 
TXOLPRM, 
TTLCE, 
TTORIG, 
COTYPE, 
COPPN, 
USRPRF, 
JOBNM, 
DATIME) AS
SELECT UNIQUE_NUMBER, 
CHDRCOY, 
CHDRNUM, 
SUBSCOY, 
SUBSNUM, 
PRODTYP, 
PLANNO, 
MBRNO, 
DPNTNO, 
HEADCNTIND, 
TRANNO, 
DTEEFF, 
DTETER, 
VFLAG, 
TARR, 
ACURR, 
RIRETBAS, 
CEDEBS, 
SIORIG, 
SILCE, 
SIEXP, 
SIEXPLCE, 
SICURR, 
SIRAT, 
PML, 
NRPPN, 
NRLCE, 
NRORIG, 
NRPML, 
SQSPPN, 
ZQSPML, 
SRIPPN01,
SRIPPN02,
SRIPPN03,
SRIPPN04,
SRIPPN05,
SRIPPN06, 
ZSPLCE01,
ZSPLCE02,
ZSPLCE03,
ZSPLCE04,
ZSPLCE05,
ZSPLCE06, 
ZSPORIG01,
ZSPORIG02,
ZSPORIG03,
ZSPORIG04,
ZSPORIG05,
ZSPORIG06, 
ZSPPML01,
ZSPPML02,
ZSPPML03,
ZSPPML04,
ZSPPML05,
ZSPPML06, 
GRPPN, 
GRLCE, 
GRORIG, 
ZGOVPPN, 
ZGOVLCE, 
ZGOVORIG, 
ZGOVPML, 
ZFACPPN, 
ZFACLCE, 
ZFACORIG, 
ZGALPPN, 
ZGALLCE, 
ZGALORIG, 
TXOLLCE, 
TXOLORIG, 
TXOLPRM, 
TTLCE, 
TTORIG, 
COTYPE, 
COPPN, 
USRPRF, 
JOBNM, 
DATIME
FROM VM1DTA.GMAPPF;

go
/****** Object:  Table [VM1DTA].[GTTYPF] ******/

CREATE TABLE [VM1DTA].[GTTYPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[SUBSCOY] [nchar](1) NULL,
	[SUBSNUM] [nchar](8) NULL,
	[PRODTYP] [nchar](4) NULL,
	[PLANNO] [nchar](3) NULL,
	[MBRNO] [nchar](5) NULL,
	[DPNTNO] [nchar](2) NULL,
	[HEADCNTIND] [nchar](1) NULL,
	[TRANNO] [int] NULL,
	[DTEEFF] [int] NULL,
	[DTETER] [int] NULL,
	[VFLAG] [nchar](1) NULL,
	[SEQNO] [int] NULL,
	[TPFX] [nchar](2) NULL,
	[TRCOY] [nchar](1) NULL,
	[TACC] [nchar](8) NULL,
	[TTYPE] [nchar](1) NULL,
	[RIPPN] [numeric](11, 8) NULL,
	[RIORIG] [numeric](17, 0) NULL,
	[RILCE] [numeric](17, 0) NULL,
	[SIEXP] [numeric](17, 0) NULL,
	[SIEXPLCE] [numeric](17, 0) NULL,
	[COTYPE] [nchar](1) NULL,
	[COPPN] [numeric](11, 8) NULL,
	[TOCP] [nchar](1) NULL,
	[TGRNR] [nchar](1) NULL,
	[TCOMM] [numeric](5, 3) NULL,
	[PML] [numeric](5, 2) NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL)
Go

CREATE VIEW [VM1DTA].[GTTY](
UNIQUE_NUMBER, 
CHDRCOY, 
CHDRNUM, 
SUBSCOY, 
SUBSNUM, 
PRODTYP, 
PLANNO, 
MBRNO, 
DPNTNO, 
HEADCNTIND, 
TRANNO, 
DTEEFF, 
DTETER, 
VFLAG, 
SEQNO, 
TPFX, 
TRCOY, 
TACC, 
TTYPE, 
RIPPN, 
RIORIG, 
RILCE, 
SIEXP, 
SIEXPLCE, 
COTYPE, 
COPPN, 
TOCP, 
TGRNR, 
TCOMM, 
PML, 
USRPRF, 
JOBNM, 
DATIME) AS
SELECT UNIQUE_NUMBER, 
CHDRCOY, 
CHDRNUM, 
SUBSCOY, 
SUBSNUM, 
PRODTYP, 
PLANNO, 
MBRNO, 
DPNTNO, 
HEADCNTIND, 
TRANNO, 
DTEEFF, 
DTETER, 
VFLAG, 
SEQNO, 
TPFX, 
TRCOY, 
TACC, 
TTYPE, 
RIPPN, 
RIORIG, 
RILCE, 
SIEXP, 
SIEXPLCE, 
COTYPE, 
COPPN, 
TOCP, 
TGRNR, 
TCOMM, 
PML, 
USRPRF, 
JOBNM, 
DATIME
FROM VM1DTA.GTTYPF;

go
/****** Object:  Table [VM1DTA].[GFACPF] ******/

CREATE TABLE [VM1DTA].[GFACPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[SUBSCOY] [nchar](1) NULL,
	[SUBSNUM] [nchar](8) NULL,
	[PRODTYP] [nchar](4) NULL,
	[PLANNO] [nchar](3) NULL,
	[MBRNO] [nchar](5) NULL,
	[DPNTNO] [nchar](2) NULL,
	[HEADCNTIND] [nchar](1) NULL,
	[TRANNO] [int] NULL,
	[DTEEFF] [int] NULL,
	[DTETER] [int] NULL,
	[VFLAG] [nchar](1) NULL,
	[RECNO] [numeric](3, 0) NULL,
	[RPFX] [nchar](2) NULL,
	[RCOY] [nchar](1) NULL,
	[RACC] [nchar](8) NULL,
	[RITYPE] [nchar](2) NULL,
	[RCVDATE] [int] NULL,
	[RIPPN] [numeric](11, 8) NULL,
	[RILCE] [numeric](17, 0) NULL,
	[RIORIG] [numeric](17, 0) NULL,
	[RICOMM] [numeric](5, 3) NULL,
	[GRNR] [nchar](1) NULL,
	[OCP] [nchar](1) NULL,
	[PRTRI] [nchar](1) NULL,
	[RIREF] [nchar](20) NULL,
	[DEFVAL] [nchar](30) NULL,
	[FCPPN] [numeric](11, 8) NULL,
	[RPLCE] [numeric](17, 2) NULL,
	[RPORIG] [numeric](17, 2) NULL,
	[COMMAMT] [numeric](17, 2) NULL,
	[SOLPPN] [numeric](10, 7) NULL,
	[COTYPE] [nchar](1) NULL,
	[COPPN] [numeric](11, 8) NULL,
	[ZSTAFFCD] [nchar](6) NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL)

Go

CREATE VIEW [VM1DTA].[GFAC](
UNIQUE_NUMBER, 
CHDRCOY, 
CHDRNUM, 
SUBSCOY, 
SUBSNUM, 
PRODTYP, 
PLANNO, 
MBRNO, 
DPNTNO, 
HEADCNTIND, 
TRANNO, 
DTEEFF, 
DTETER, 
VFLAG, 
RECNO, 
RPFX, 
RCOY, 
RACC, 
RITYPE, 
RCVDATE, 
RIPPN, 
RILCE, 
RIORIG, 
RICOMM, 
GRNR, 
OCP, 
PRTRI, 
RIREF, 
DEFVAL, 
FCPPN, 
RPLCE, 
RPORIG, 
COMMAMT, 
SOLPPN, 
COTYPE, 
COPPN,
ZSTAFFCD, 
USRPRF, 
JOBNM, 
DATIME) AS
SELECT UNIQUE_NUMBER, 
CHDRCOY, 
CHDRNUM, 
SUBSCOY, 
SUBSNUM, 
PRODTYP, 
PLANNO, 
MBRNO, 
DPNTNO, 
HEADCNTIND, 
TRANNO, 
DTEEFF, 
DTETER, 
VFLAG, 
RECNO, 
RPFX, 
RCOY, 
RACC, 
RITYPE, 
RCVDATE, 
RIPPN, 
RILCE, 
RIORIG, 
RICOMM, 
GRNR, 
OCP, 
PRTRI, 
RIREF, 
DEFVAL, 
FCPPN, 
RPLCE, 
RPORIG, 
COMMAMT, 
SOLPPN, 
COTYPE, 
COPPN,
ZSTAFFCD,  
USRPRF, 
JOBNM, 
DATIME
FROM VM1DTA.GFACPF;

go
/****** Object:  Table [VM1DTA].[GFPNPF] ******/

CREATE TABLE [VM1DTA].[GFPNPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[SUBSCOY] [nchar](1) NULL,
	[SUBSNUM] [nchar](8) NULL,
	[PRODTYP] [nchar](4) NULL,
	[PLANNO] [nchar](3) NULL,
	[MBRNO] [nchar](5) NULL,
	[DPNTNO] [nchar](2) NULL,
	[HEADCNTIND] [nchar](1) NULL,
	[TRANNO] [int] NULL,
	[DTEEFF] [int] NULL,
	[DTETER] [int] NULL,
	[VFLAG] [nchar](1) NULL,
	[RPFX] [nchar](2) NULL,
	[RCOY] [nchar](1) NULL,
	[RACC] [nchar](8) NULL,
	[RITYPE] [nchar](2) NULL,
	[RECNO] [numeric](3, 0) NULL,
	[CLNTPFX] [nchar](2) NULL,
	[CLNTCOY] [nchar](1) NULL,
	[CLNTNUM] [nchar](8) NULL,
	[SOLPPN] [numeric](10, 7) NULL,
	[DESCRIPT] [nchar](24) NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL)

Go

CREATE VIEW [VM1DTA].[GFPN](
UNIQUE_NUMBER, 
CHDRCOY, 
CHDRNUM, 
SUBSCOY, 
SUBSNUM, 
PRODTYP, 
PLANNO, 
MBRNO, 
DPNTNO, 
HEADCNTIND, 
TRANNO, 
DTEEFF, 
DTETER, 
VFLAG, 
RPFX, 
RCOY, 
RACC, 
RITYPE, 
RECNO, 
CLNTPFX, 
CLNTCOY, 
CLNTNUM, 
SOLPPN, 
DESCRIPT, 
USRPRF, 
JOBNM, 
DATIME) AS
SELECT UNIQUE_NUMBER, 
CHDRCOY, 
CHDRNUM, 
SUBSCOY, 
SUBSNUM, 
PRODTYP, 
PLANNO, 
MBRNO, 
DPNTNO, 
HEADCNTIND, 
TRANNO, 
DTEEFF, 
DTETER, 
VFLAG, 
RPFX, 
RCOY, 
RACC, 
RITYPE, 
RECNO, 
CLNTPFX, 
CLNTCOY, 
CLNTNUM, 
SOLPPN, 
DESCRIPT, 
USRPRF, 
JOBNM, 
DATIME
FROM VM1DTA.GFPNPF;

go
/****** Object:  Table [VM1DTA].[GFXLPF] ******/


CREATE TABLE [VM1DTA].[GFXLPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[SUBSCOY] [nchar](1) NULL,
	[SUBSNUM] [nchar](8) NULL,
	[PRODTYP] [nchar](4) NULL,
	[PLANNO] [nchar](3) NULL,
	[MBRNO] [nchar](5) NULL,
	[DPNTNO] [nchar](2) NULL,
	[HEADCNTIND] [nchar](1) NULL,
	[TRANNO] [int] NULL,
	[DTEEFF] [int] NULL,
	[DTETER] [int] NULL,
	[VFLAG] [nchar](1) NULL,
	[RECNO] [numeric](3, 0) NULL,
	[RPFX] [nchar](2) NULL,
	[RCOY] [nchar](1) NULL,
	[RACC] [nchar](8) NULL,
	[RITYPE] [nchar](2) NULL,
	[SEQNO] [int] NULL,
	[RCVDATE] [int] NULL,
	[DEFVAL] [nchar](30) NULL,
	[PREM] [numeric](17, 2) NULL,
	[LCEPRM] [numeric](17, 2) NULL,
	[ORGPRM] [numeric](17, 2) NULL,
	[LCEDED] [numeric](17, 0) NULL,
	[LCEUPP] [numeric](17, 0) NULL,
	[ORGDED] [numeric](17, 0) NULL,
	[ORGUPP] [numeric](17, 0) NULL,
	[RICOMM] [numeric](5, 3) NULL,
	[COMMAMT] [numeric](17, 2) NULL,
	[GRNR] [nchar](1) NULL,
	[OCP] [nchar](1) NULL,
	[RIREF] [nchar](20) NULL,
	[COTYPE] [nchar](1) NULL,
	[COPPN] [numeric](11, 8) NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL)
Go

CREATE VIEW [VM1DTA].[GFXL](
UNIQUE_NUMBER, 
CHDRCOY, 
CHDRNUM, 
SUBSCOY, 
SUBSNUM, 
PRODTYP, 
PLANNO, 
MBRNO, 
DPNTNO, 
HEADCNTIND, 
TRANNO, 
DTEEFF, 
DTETER, 
VFLAG, 
RECNO, 
RPFX, 
RCOY, 
RACC, 
RITYPE, 
SEQNO, 
RCVDATE, 
DEFVAL, 
PREM, 
LCEPRM, 
ORGPRM, 
LCEDED, 
LCEUPP, 
ORGDED, 
ORGUPP, 
RICOMM, 
COMMAMT, 
GRNR, 
OCP, 
RIREF, 
COTYPE, 
COPPN, 
USRPRF, 
JOBNM, 
DATIME) AS
SELECT UNIQUE_NUMBER, 
CHDRCOY, 
CHDRNUM, 
SUBSCOY, 
SUBSNUM, 
PRODTYP, 
PLANNO, 
MBRNO, 
DPNTNO, 
HEADCNTIND, 
TRANNO, 
DTEEFF, 
DTETER, 
VFLAG, 
RECNO, 
RPFX, 
RCOY, 
RACC, 
RITYPE, 
SEQNO, 
RCVDATE, 
DEFVAL, 
PREM, 
LCEPRM, 
ORGPRM, 
LCEDED, 
LCEUPP, 
ORGDED, 
ORGUPP, 
RICOMM, 
COMMAMT, 
GRNR, 
OCP, 
RIREF, 
COTYPE, 
COPPN, 
USRPRF, 
JOBNM, 
DATIME
FROM VM1DTA.GFXLPF;

go
/****** Object:  Table [VM1DTA].[GTSAPF] ******/

CREATE TABLE [VM1DTA].[GTSAPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[SUBSCOY] [nchar](1) NULL,
	[SUBSNUM] [nchar](8) NULL,
	[PRODTYP] [nchar](4) NULL,
	[PLANNO] [nchar](3) NULL,
	[MBRNO] [nchar](5) NULL,
	[DPNTNO] [nchar](2) NULL,
	[HEADCNTIND] [nchar](1) NULL,
	[TRANNO] [int] NULL,
	[DTEEFF] [int] NULL,
	[DTETER] [int] NULL,
	[VFLAG] [nchar](1) NULL,
	[SEQNO] [int] NULL,
	[RPFX] [nchar](2) NULL,
	[RCOY] [nchar](1) NULL,
	[RACC] [nchar](8) NULL,
	[XREFNO] [nchar](30) NULL,
	[ZLEVEL] [nchar](8) NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL)
	
Go

CREATE VIEW [VM1DTA].[GTSA](
UNIQUE_NUMBER, 
CHDRCOY, 
CHDRNUM, 
SUBSCOY, 
SUBSNUM, 
PRODTYP, 
PLANNO, 
MBRNO, 
DPNTNO, 
HEADCNTIND, 
TRANNO, 
DTEEFF, 
DTETER, 
VFLAG, 
SEQNO, 
RPFX, 
RCOY, 
RACC, 
XREFNO, 
ZLEVEL, 
USRPRF, 
JOBNM, 
DATIME) AS
SELECT UNIQUE_NUMBER, 
CHDRCOY, 
CHDRNUM, 
SUBSCOY, 
SUBSNUM, 
PRODTYP, 
PLANNO, 
MBRNO, 
DPNTNO, 
HEADCNTIND, 
TRANNO, 
DTEEFF, 
DTETER, 
VFLAG, 
SEQNO, 
RPFX, 
RCOY, 
RACC, 
XREFNO, 
ZLEVEL, 
USRPRF, 
JOBNM, 
DATIME
FROM VM1DTA.GTSAPF;

go
/****** Object:  Table [VM1DTA].[GRMKPF] ******/

CREATE TABLE [VM1DTA].[GRMKPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[SUBSCOY] [nchar](1) NULL,
	[SUBSNUM] [nchar](8) NULL,
	[PRODTYP] [nchar](4) NULL,
	[PLANNO] [nchar](3) NULL,
	[MBRNO] [nchar](5) NULL,
	[DPNTNO] [nchar](2) NULL,
	[HEADCNTIND] [nchar](1) NULL,
	[TRANNO] [int] NULL,
	[DTEEFF] [int] NULL,
	[DTETER] [int] NULL,
	[VFLAG] [nchar](1) NULL,
	[RPFX] [nchar](2) NULL,
	[RCOY] [nchar](1) NULL,
	[RACC] [nchar](8) NULL,
	[SEQNO] [int] NULL,
	[Z5REMARK] [nchar](255) NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL)
	
Go

CREATE VIEW [VM1DTA].[GRMK](
UNIQUE_NUMBER, 
CHDRCOY, 
CHDRNUM, 
SUBSCOY, 
SUBSNUM, 
PRODTYP, 
PLANNO, 
MBRNO, 
DPNTNO, 
HEADCNTIND, 
TRANNO, 
DTEEFF, 
DTETER, 
VFLAG, 
RPFX, 
RCOY, 
RACC, 
SEQNO, 
Z5REMARK, 
USRPRF, 
JOBNM, 
DATIME) AS
SELECT UNIQUE_NUMBER, 
CHDRCOY, 
CHDRNUM, 
SUBSCOY, 
SUBSNUM, 
PRODTYP, 
PLANNO, 
MBRNO, 
DPNTNO, 
HEADCNTIND, 
TRANNO, 
DTEEFF, 
DTETER, 
VFLAG, 
RPFX, 
RCOY, 
RACC, 
SEQNO, 
Z5REMARK, 
USRPRF, 
JOBNM, 
DATIME
FROM VM1DTA.GRMKPF;

go
/****** Object:  Table [VM1DTA].[GRMRPF] ******/

CREATE TABLE [VM1DTA].[GPMRPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[SUBSCOY] [nchar](1) NULL,
	[SUBSNUM] [nchar](8) NULL,
	[PRODTYP] [nchar](4) NULL,
	[PLANNO] [nchar](3) NULL,
	[MBRNO] [nchar](5) NULL,
	[DPNTNO] [nchar](2) NULL,
	[HEADCNTIND] [nchar](1) NULL,
	[TRANNO] [int] NULL,
	[DTEEFF] [int] NULL,
	[DTETER] [int] NULL,
	[RPFX] [nchar](2) NULL,
	[RCOY] [nchar](1) NULL,
	[RACC] [nchar](8) NULL,
	[RITYPE] [nchar](2) NULL,
	[CEDTYP] [numeric](1, 0) NULL,
	[SEQNO] [int] NULL,
	[PRBILFDT] [int] NULL,
	[PRBILTDT] [int] NULL,
	[SACSCODE] [nchar](2) NULL,
	[SACSTYP01] [nchar](2) NULL,
	[SACSTYP02] [nchar](2) NULL,
	[SACSTYP03] [nchar](2) NULL,
	[SACSTYP04] [nchar](2) NULL,
	[SACSTYP05] [nchar](2) NULL,
	[SACSTYP06] [nchar](2) NULL,
	[SACSTYP07] [nchar](2) NULL,
	[SACSTYP08] [nchar](2) NULL,
	[SACSTYP09] [nchar](2) NULL,
	[SACSTYP10] [nchar](2) NULL,
	[SACSTYP11] [nchar](2) NULL,
	[SACSTYP12] [nchar](2) NULL,
	[SACSTYP13] [nchar](2) NULL,
	[SACSTYP14] [nchar](2) NULL,
	[SACSTYP15] [nchar](2) NULL,
	[CURR] [nchar](3) NULL,
	[CRATE] [numeric](18, 9) NULL,
	[EXTR01] [numeric](17, 2) NULL,
	[EXTR02] [numeric](17, 2) NULL,
	[EXTR03] [numeric](17, 2) NULL,
	[EXTR04] [numeric](17, 2) NULL,
	[EXTR05] [numeric](17, 2) NULL,
	[EXTR06] [numeric](17, 2) NULL,
	[EXTR07] [numeric](17, 2) NULL,
	[EXTR08] [numeric](17, 2) NULL,
	[EXTR09] [numeric](17, 2) NULL,
	[EXTR10] [numeric](17, 2) NULL,
	[EXTR11] [numeric](17, 2) NULL,
	[EXTR12] [numeric](17, 2) NULL,
	[EXTR13] [numeric](17, 2) NULL,
	[EXTR14] [numeric](17, 2) NULL,
	[EXTR15] [numeric](17, 2) NULL,
	[GSTRIAMT01] [numeric](17, 2) NULL,
	[GSTRIAMT02] [numeric](17, 2) NULL,
	[GSTRIAMT03] [numeric](17, 2) NULL,
	[GSTRIAMT04] [numeric](17, 2) NULL,
	[GSTRIAMT05] [numeric](17, 2) NULL,
	[GSTRIAMT06] [numeric](17, 2) NULL,
	[GSTRIAMT07] [numeric](17, 2) NULL,
	[GSTRIAMT08] [numeric](17, 2) NULL,
	[GSTRIAMT09] [numeric](17, 2) NULL,
	[GSTRIAMT10] [numeric](17, 2) NULL,
	[GSTRIAMT11] [numeric](17, 2) NULL,
	[GSTRIAMT12] [numeric](17, 2) NULL,
	[GSTRIAMT13] [numeric](17, 2) NULL,
	[GSTRIAMT14] [numeric](17, 2) NULL,
	[GSTRIAMT15] [numeric](17, 2) NULL,
	[Z6TAXCDE01] [nchar](6) NULL,
	[Z6TAXCDE02] [nchar](6) NULL,
	[Z6TAXCDE03] [nchar](6) NULL,
	[Z6TAXCDE04] [nchar](6) NULL,
	[Z6TAXCDE05] [nchar](6) NULL,
	[Z6TAXCDE06] [nchar](6) NULL,
	[Z6TAXCDE07] [nchar](6) NULL,
	[Z6TAXCDE08] [nchar](6) NULL,
	[Z6TAXCDE09] [nchar](6) NULL,
	[Z6TAXCDE10] [nchar](6) NULL,
	[Z6TAXCDE11] [nchar](6) NULL,
	[Z6TAXCDE12] [nchar](6) NULL,
	[Z6TAXCDE13] [nchar](6) NULL,
	[Z6TAXCDE14] [nchar](6) NULL,
	[Z6TAXCDE15] [nchar](6) NULL,
	[Z6PERCNT01] [numeric](5, 3) NULL,
	[Z6PERCNT02] [numeric](5, 3) NULL,
	[Z6PERCNT03] [numeric](5, 3) NULL,
	[Z6PERCNT04] [numeric](5, 3) NULL,
	[Z6PERCNT05] [numeric](5, 3) NULL,
	[Z6PERCNT06] [numeric](5, 3) NULL,
	[Z6PERCNT07] [numeric](5, 3) NULL,
	[Z6PERCNT08] [numeric](5, 3) NULL,
	[Z6PERCNT09] [numeric](5, 3) NULL,
	[Z6PERCNT10] [numeric](5, 3) NULL,
	[Z6PERCNT11] [numeric](5, 3) NULL,
	[Z6PERCNT12] [numeric](5, 3) NULL,
	[Z6PERCNT13] [numeric](5, 3) NULL,
	[Z6PERCNT14] [numeric](5, 3) NULL,
	[Z6PERCNT15] [numeric](5, 3) NULL,
	[EXTOUT] [nchar](1) NULL,
	[RIPPN] [numeric](11, 8) NULL,
	[RECTYPE] [nchar](1) NULL,
	[BILLTYP] [nchar](1) NULL,
	[BILLNO] [int] NULL,
	[CNOTENUM] [numeric](8, 0) NULL,
	[COTYPE] [nchar](1) NULL,
	[COPPN] [numeric](11, 8) NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL)
	
Go

CREATE VIEW [VM1DTA].[GPMR](
UNIQUE_NUMBER, 
CHDRCOY, 
CHDRNUM, 
SUBSCOY, 
SUBSNUM, 
PRODTYP, 
PLANNO, 
MBRNO, 
DPNTNO, 
HEADCNTIND, 
TRANNO, 
DTEEFF, 
DTETER, 
RPFX, 
RCOY, 
RACC, 
RITYPE, 
CEDTYP, 
SEQNO, 
PRBILFDT, 
PRBILTDT, 
SACSCODE, 
SACSTYP01,
SACSTYP02,
SACSTYP03,
SACSTYP04,
SACSTYP05,
SACSTYP06,
SACSTYP07,
SACSTYP08,
SACSTYP09,
SACSTYP10,
SACSTYP11,
SACSTYP12,
SACSTYP13,
SACSTYP14,
SACSTYP15, 
CURR, 
CRATE, 
EXTR01,
EXTR02,
EXTR03,
EXTR04,
EXTR05,
EXTR06,
EXTR07,
EXTR08,
EXTR09,
EXTR10,
EXTR11,
EXTR12,
EXTR13,
EXTR14,
EXTR15,
GSTRIAMT01,
GSTRIAMT02,
GSTRIAMT03,
GSTRIAMT04,
GSTRIAMT05,
GSTRIAMT06,
GSTRIAMT07,
GSTRIAMT08,
GSTRIAMT09,
GSTRIAMT10,
GSTRIAMT11,
GSTRIAMT12,
GSTRIAMT13,
GSTRIAMT14,
GSTRIAMT15, 
Z6TAXCDE01,
Z6TAXCDE02,
Z6TAXCDE03,
Z6TAXCDE04,
Z6TAXCDE05,
Z6TAXCDE06,
Z6TAXCDE07,
Z6TAXCDE08,
Z6TAXCDE09,
Z6TAXCDE10,
Z6TAXCDE11,
Z6TAXCDE12,
Z6TAXCDE13,
Z6TAXCDE14,
Z6TAXCDE15, 
Z6PERCNT01,
Z6PERCNT02,
Z6PERCNT03,
Z6PERCNT04,
Z6PERCNT05,
Z6PERCNT06,
Z6PERCNT07,
Z6PERCNT08,
Z6PERCNT09,
Z6PERCNT10,
Z6PERCNT11,
Z6PERCNT12,
Z6PERCNT13,
Z6PERCNT14,
Z6PERCNT15, 
EXTOUT, 
RIPPN, 
RECTYPE, 
BILLTYP, 
BILLNO, 
CNOTENUM, 
COTYPE, 
COPPN, 
USRPRF, 
JOBNM, 
DATIME) AS
SELECT UNIQUE_NUMBER, 
CHDRCOY, 
CHDRNUM, 
SUBSCOY, 
SUBSNUM, 
PRODTYP, 
PLANNO, 
MBRNO, 
DPNTNO, 
HEADCNTIND, 
TRANNO, 
DTEEFF, 
DTETER, 
RPFX, 
RCOY, 
RACC, 
RITYPE, 
CEDTYP, 
SEQNO, 
PRBILFDT, 
PRBILTDT, 
SACSCODE, 
SACSTYP01,
SACSTYP02,
SACSTYP03,
SACSTYP04,
SACSTYP05,
SACSTYP06,
SACSTYP07,
SACSTYP08,
SACSTYP09,
SACSTYP10,
SACSTYP11,
SACSTYP12,
SACSTYP13,
SACSTYP14,
SACSTYP15,  
CURR, 
CRATE, 
EXTR01,
EXTR02,
EXTR03,
EXTR04,
EXTR05,
EXTR06,
EXTR07,
EXTR08,
EXTR09,
EXTR10,
EXTR11,
EXTR12,
EXTR13,
EXTR14,
EXTR15,
GSTRIAMT01,
GSTRIAMT02,
GSTRIAMT03,
GSTRIAMT04,
GSTRIAMT05,
GSTRIAMT06,
GSTRIAMT07,
GSTRIAMT08,
GSTRIAMT09,
GSTRIAMT10,
GSTRIAMT11,
GSTRIAMT12,
GSTRIAMT13,
GSTRIAMT14,
GSTRIAMT15, 
Z6TAXCDE01,
Z6TAXCDE02,
Z6TAXCDE03,
Z6TAXCDE04,
Z6TAXCDE05,
Z6TAXCDE06,
Z6TAXCDE07,
Z6TAXCDE08,
Z6TAXCDE09,
Z6TAXCDE10,
Z6TAXCDE11,
Z6TAXCDE12,
Z6TAXCDE13,
Z6TAXCDE14,
Z6TAXCDE15, 
Z6PERCNT01,
Z6PERCNT02,
Z6PERCNT03,
Z6PERCNT04,
Z6PERCNT05,
Z6PERCNT06,
Z6PERCNT07,
Z6PERCNT08,
Z6PERCNT09,
Z6PERCNT10,
Z6PERCNT11,
Z6PERCNT12,
Z6PERCNT13,
Z6PERCNT14,
Z6PERCNT15, 
EXTOUT, 
RIPPN, 
RECTYPE, 
BILLTYP, 
BILLNO, 
CNOTENUM, 
COTYPE, 
COPPN, 
USRPRF, 
JOBNM, 
DATIME
FROM VM1DTA.GPMRPF;

go
/****** Object:  Table [VM1DTA].[GLRIPF] ******/
CREATE TABLE [VM1DTA].[GLRIPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CLMCOY] [nchar](1) NULL,
	[CLAMNUM] [nchar](8) NULL,
	[GCOCCNO] [nchar](2) NULL,
	[GCCLMSEQ] [numeric](3, 0) NULL,
	[TRANNO] [int] NULL,
	[PRODTYP] [nchar](4) NULL,
	[PLANNO] [nchar](3) NULL,
	[MBRNO] [nchar](5) NULL,
	[DPNTNO] [nchar](2) NULL,
	[HEADCNTIND] [nchar](1) NULL,
	[RPFX] [nchar](2) NULL,
	[RCOY] [nchar](1) NULL,
	[RACC] [nchar](8) NULL,
	[RITYPE] [nchar](2) NULL,
	[CEDTYP] [numeric](1, 0) NULL,
	[SEQNO] [int] NULL,
	[RIPPN] [numeric](11, 8) NULL,
	[COTYPE] [nchar](1) NULL,
	[COPPN] [numeric](11, 8) NULL,
	[STLBASIS] [nchar](1) NULL,
	[VFLAG] [nchar](1) NULL,
	[CLCURR] [nchar](3) NULL,
	[CRATE] [numeric](18, 9) NULL,
	[JCODE] [nchar](2) NULL,
	[PAYMRI] [numeric](17, 2) NULL,
	[LPAYMRI] [numeric](17, 2) NULL,
	[RDOCNUM] [nchar](9) NULL,
	[RICLMREF] [nchar](20) NULL,
	[LXLLOW] [numeric](17, 0) NULL,
	[LXLUP] [numeric](17, 0) NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL)
	
Go

CREATE VIEW [VM1DTA].[GLRI](
UNIQUE_NUMBER, 
CLMCOY, 
CLAMNUM, 
GCOCCNO, 
GCCLMSEQ, 
TRANNO, 
PRODTYP, 
PLANNO, 
MBRNO, 
DPNTNO, 
HEADCNTIND, 
RPFX, 
RCOY, 
RACC, 
RITYPE, 
CEDTYP, 
SEQNO, 
RIPPN, 
COTYPE, 
COPPN, 
STLBASIS, 
VFLAG, 
CLCURR, 
CRATE, 
JCODE, 
PAYMRI, 
LPAYMRI, 
RDOCNUM, 
RICLMREF, 
LXLLOW, 
LXLUP, 
USRPRF, 
JOBNM, 
DATIME) AS
SELECT UNIQUE_NUMBER, 
CLMCOY, 
CLAMNUM, 
GCOCCNO, 
GCCLMSEQ, 
TRANNO, 
PRODTYP, 
PLANNO, 
MBRNO, 
DPNTNO, 
HEADCNTIND, 
RPFX, 
RCOY, 
RACC, 
RITYPE, 
CEDTYP, 
SEQNO, 
RIPPN, 
COTYPE, 
COPPN, 
STLBASIS, 
VFLAG, 
CLCURR, 
CRATE, 
JCODE, 
PAYMRI, 
LPAYMRI, 
RDOCNUM, 
RICLMREF, 
LXLLOW, 
LXLUP, 
USRPRF, 
JOBNM, 
DATIME
FROM VM1DTA.GLRIPF;

go
/****** Object:  Table [VM1DTA].[GCRIPF] ******/

CREATE TABLE [VM1DTA].[GCRIPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CLMCOY] [nchar](1) NULL,
	[CLAMNUM] [nchar](8) NULL,
	[GCOCCNO] [nchar](2) NULL,
	[GCCLMSEQ] [numeric](3, 0) NULL,
	[TRANNO] [int] NULL,
	[PRODTYP] [nchar](4) NULL,
	[PLANNO] [nchar](3) NULL,
	[MBRNO] [nchar](5) NULL,
	[DPNTNO] [nchar](2) NULL,
	[HEADCNTIND] [nchar](1) NULL,
	[RPFX] [nchar](2) NULL,
	[RCOY] [nchar](1) NULL,
	[RACC] [nchar](8) NULL,
	[RITYPE] [nchar](2) NULL,
	[CEDTYP] [numeric](1, 0) NULL,
	[SEQNO] [int] NULL,
	[RIPPN] [numeric](11, 8) NULL,
	[COTYPE] [nchar](1) NULL,
	[COPPN] [numeric](11, 8) NULL,
	[VFLAG] [nchar](1) NULL,
	[CLCURR] [nchar](3) NULL,
	[PAID] [numeric](17, 2) NULL,
	[LPAID] [numeric](17, 2) NULL,
	[LXLLOW] [numeric](17, 0) NULL,
	[LXLUP] [numeric](17, 0) NULL,
	[STLBASIS] [nchar](1) NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL)
	
Go

CREATE VIEW [VM1DTA].[GCRI](
UNIQUE_NUMBER, 
CLMCOY, 
CLAMNUM, 
GCOCCNO, 
GCCLMSEQ, 
TRANNO, 
PRODTYP, 
PLANNO, 
MBRNO, 
DPNTNO, 
HEADCNTIND, 
RPFX, 
RCOY, 
RACC, 
RITYPE, 
CEDTYP, 
SEQNO, 
RIPPN, 
COTYPE, 
COPPN, 
VFLAG, 
CLCURR, 
PAID, 
LPAID, 
LXLLOW, 
LXLUP, 
STLBASIS, 
USRPRF, 
JOBNM, 
DATIME) AS
SELECT UNIQUE_NUMBER, 
CLMCOY, 
CLAMNUM, 
GCOCCNO, 
GCCLMSEQ, 
TRANNO, 
PRODTYP, 
PLANNO, 
MBRNO, 
DPNTNO, 
HEADCNTIND, 
RPFX, 
RCOY, 
RACC, 
RITYPE, 
CEDTYP, 
SEQNO, 
RIPPN, 
COTYPE, 
COPPN, 
VFLAG, 
CLCURR, 
PAID, 
LPAID, 
LXLLOW, 
LXLUP, 
STLBASIS, 
USRPRF, 
JOBNM, 
DATIME
FROM VM1DTA.GCRIPF;

go
/****** Object:  Table [VM1DTA].[GCOIPF] ******/

CREATE TABLE [VM1DTA].[GCOIPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[TRANNO] [int] NULL,
	[DTEEFF] [int] NULL,
	[DTETER] [int] NULL,
	[VFLAG] [nchar](1) NULL,
	[AGNTPFX] [nchar](2) NULL,
	[AGNTCOY] [nchar](1) NULL,
	[AGNTNUM] [nchar](8) NULL,
	[COPPN] [numeric](11,8) NULL,
	[COTYPE] [nchar](1) NULL,
	[GRNR] [nchar](1) NULL,
	[RICOMM] [numeric](5,3) NULL,
	[OCP] [nchar](1) NULL,
	[COFEETYP] [nchar](1) NULL,
	[AGPC] [numeric](8,5) NULL,
	[RIREF] [nchar](20) NULL,
	[DTEISS] [int] NULL,
	[DATERETN] [int] NULL,
	[Z6ORINVREF01] [nchar](30) NULL,
	[Z6ORINVREF02] [nchar](30) NULL,
	[TERMID] [nchar](4) NULL,
	[USER_T] [int] NULL,
	[TRDT] [int] NULL,
	[TRTM] [int] NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL)
	
Go

CREATE VIEW [VM1DTA].[GCOI](
UNIQUE_NUMBER, 
CHDRCOY, 
CHDRNUM, 
TRANNO, 
DTEEFF, 
DTETER, 
VFLAG, 
AGNTPFX, 
AGNTCOY, 
AGNTNUM, 
COPPN, 
COTYPE, 
GRNR, 
RICOMM, 
OCP, 
COFEETYP, 
AGPC, 
RIREF, 
DTEISS, 
DATERETN, 
Z6ORINVREF01, 
Z6ORINVREF02, 
TERMID,
USER_T,
TRDT,
TRTM,
USRPRF, 
JOBNM, 
DATIME) AS
SELECT UNIQUE_NUMBER, 
CHDRCOY, 
CHDRNUM, 
TRANNO, 
DTEEFF, 
DTETER, 
VFLAG, 
AGNTPFX, 
AGNTCOY, 
AGNTNUM, 
COPPN, 
COTYPE, 
GRNR, 
RICOMM, 
OCP, 
COFEETYP, 
AGPC, 
RIREF, 
DTEISS, 
DATERETN, 
Z6ORINVREF01, 
Z6ORINVREF02,
TERMID,
USER_T,
TRDT,
TRTM, 
USRPRF, 
JOBNM, 
DATIME
FROM VM1DTA.GCOIPF;

go
/****** Object:  Table [VM1DTA].[GCMHPF] ******/

ALTER TABLE VM1DTA.GCMHPF
ADD DCNIND NCHAR(1),
	MEVENT NCHAR(8)

go

ALTER VIEW [VM1DTA].[GCMH](UNIQUE_NUMBER, CHDRCOY, CLAMNUM, GCOCCNO, GCCLMSEQ, CHDRNUM, PRODTYP, GCCLMTNO, GCDPNTNO, CLNTPFX, CLNTCOY, CLNTNUM, GCSTS, DTECLAM, CLAIMCUR, CRATE, PLANNO, FMLYCDE, GCRMK, BNKCHARGE, SPECTRM, GCDBLIND, GCFRPDTE, GCLRPDTE, GCDTHCLM, GCCAUSCD, GCOPPVCD, GCOPBNCD, GCOPIAMT, GCOPBAMT, GCOPBNPY, GCOPDGCD, GCOPPTCD, REQNBCDE, REQNTYPE, GCAUTHBY, DATEAUTH, GCOPRSCD, GCGRSPY, GCNETPY, GCCOINA, DEDUCTPC, GCADVPY, GCTRDRPY, GCLOCCNO, MMFLG, PACCAMT, OVRLMT, DTEPYMTADV, DTEPYMTOTH, DTEATT, DTETRM, REVLINK, REVIND, TERMID, USER_T, TRDT, TRTM, APAIDAMT, INTPAIDAMT, GCSETLMD, GCSECSTS, ZCLTCLMREF, ZCLMRECD, ZCLNTPROC, ZCLMREF, ZFEECURR, ZFEETYP, ZCALCFEE, ZOVRDFEE, GRSKCLS, BENCDE, CLAIMCOND, OUTLOAN, USRPRF, JOBNM, DATIME, DIACODE, ONDUTY, ROPENRSN, DCNIND, MEVENT) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CLAMNUM,
            GCOCCNO,
            GCCLMSEQ,
            CHDRNUM,
            PRODTYP,
            GCCLMTNO,
            GCDPNTNO,
            CLNTPFX,
            CLNTCOY,
            CLNTNUM,
            GCSTS,
            DTECLAM,
            CLAIMCUR,
            CRATE,
            PLANNO,
            FMLYCDE,
            GCRMK,
            BNKCHARGE,
            SPECTRM,
            GCDBLIND,
            GCFRPDTE,
            GCLRPDTE,
            GCDTHCLM,
            GCCAUSCD,
            GCOPPVCD,
            GCOPBNCD,
            GCOPIAMT,
            GCOPBAMT,
            GCOPBNPY,
            GCOPDGCD,
            GCOPPTCD,
            REQNBCDE,
            REQNTYPE,
            GCAUTHBY,
            DATEAUTH,
            GCOPRSCD,
            GCGRSPY,
            GCNETPY,
            GCCOINA,
            DEDUCTPC,
            GCADVPY,
            GCTRDRPY,
            GCLOCCNO,
            MMFLG,
            PACCAMT,
            OVRLMT,
            DTEPYMTADV,
            DTEPYMTOTH,
            DTEATT,
            DTETRM,
            REVLINK,
            REVIND,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            APAIDAMT,
            INTPAIDAMT,
            GCSETLMD,
            GCSECSTS,
            ZCLTCLMREF,
            ZCLMRECD,
            ZCLNTPROC,
            ZCLMREF,
            ZFEECURR,
            ZFEETYP,
            ZCALCFEE,
            ZOVRDFEE,
            GRSKCLS,
            BENCDE,
            CLAIMCOND,
            OUTLOAN,
            USRPRF,
            JOBNM,
            DATIME,
			DIACODE,
			ONDUTY,
			ROPENRSN,
			DCNIND,
			MEVENT 

       FROM VM1DTA.GCMHPF

GO

/****** Object:  Table [VM1DTA].[GHHIPF] ******/
ALTER TABLE VM1DTA.GHHIPF
ADD [HIGHSI] [numeric](13, 0) NULL

go
/****** Object:  Table [VM1DTA].[GHHIPEH] ******/

ALTER VIEW [VM1DTA].[GHHIPEH](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, HEADNO, EFFDATE, PRODTYP, PLANNO, FMLYCDE, DTEATT, DTETRM, REASONTRM, HSUMINSU, NOFMBR, AVRGAGE, SPECTRM, SUBSCOY, SUBSNUM, SUBSTDSI, TERMID, USER_T, TRDT, TRTM, TRANNO, HIGHSI, JOBNM, USRPRF, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            HEADNO,
            EFFDATE,
            PRODTYP,
            PLANNO,
            FMLYCDE,
            DTEATT,
            DTETRM,
            REASONTRM,
            HSUMINSU,
            NOFMBR,
            AVRGAGE,
            SPECTRM,
            SUBSCOY,
            SUBSNUM,
            SUBSTDSI,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            TRANNO,
			HIGHSI,
            JOBNM,
            USRPRF,
            DATIME


       FROM VM1DTA.GHHIPF

GO

ALTER TABLE VM1DTA.GPHDPF
ADD PCONVEYA NUMERIC(15, 2);

GO

ALTER VIEW [VM1DTA].[GPHD](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, PRODTYP, DTEATT, CONTYPE, AGRINDEM, DTEIDM, MEDEVD, DTETRM, REASONTRM, PRVCOND, PRVAMTS, PRVAMTM, TERMID, USER_T, TRDT, TRTM, TRANNO, PREEXIBEF, PREEXIAFT, GSTTYPE, PM06KEY, ECTBRULE, INVFMIX, BFTDSN, PAYMMODE, INVSLVL, ACBLRULE, PRMPYOPT, PRTEBFLG, TRUSTEE, GLBFEESC, ACTEXFLG, MBRFEE, EVNTFEE, TIMEFEE, FEEBTDTE, FRFUND, TRIPTYP, PCONVEYA, USRPRF, JOBNM, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            PRODTYP,
            DTEATT,
            CONTYPE,
            AGRINDEM,
            DTEIDM,
            MEDEVD,
            DTETRM,
            REASONTRM,
            PRVCOND,
            PRVAMTS,
            PRVAMTM,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            TRANNO,
            PREEXIBEF,
            PREEXIAFT,
            GSTTYPE,
            PM06KEY,
            ECTBRULE,
            INVFMIX,
            BFTDSN,
            PAYMMODE,
            INVSLVL,
            ACBLRULE,
            PRMPYOPT,
            PRTEBFLG,
            TRUSTEE,
            GLBFEESC,
            ACTEXFLG,
            MBRFEE,
            EVNTFEE,
            TIMEFEE,
            FEEBTDTE,
            FRFUND,
            TRIPTYP,
			PCONVEYA,
            USRPRF,
            JOBNM,
            DATIME
       FROM VM1DTA.GPHDPF

go

/* ERROR */
INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFSH'
           ,'High SI > Headcount SI'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
GO 
INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'Enn2'
           ,'Headcnt/mem. not existed'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
GO 

INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'Enn3'
           ,'RIPlmtDte > Prd Eff Dte'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
GO

/*TSD*/
INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFSI'
           ,'Invalid retention basis'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
GO

INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFSF'
           ,'Not Prod level RI'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
GO

INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFSG'
           ,'Prod level RI not found'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
GO


INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFSJ'
           ,'RIPlmtDte > Prd Eff Dte'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
GO

INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'K035  '
           ,'Enter either field only'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
GO
/*HELP TEXT*/

--------Grp Conveyance � Mbr SI-------
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9797'
           ,'TARGETSI'
           ,'1'
           ,''
           ,'1'
           ,'A checkbox which is used to denote whether  '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9797'
           ,'TARGETSI'
           ,'2'
           ,''
           ,'1'
           ,'product just need to calculate reinsurer '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9797'
           ,'TARGETSI'
           ,'3'
           ,''
           ,'1'
           ,'share on main members Sum Insured only.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

--------R/I at Prod Level-------
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9797'
           ,'RIPROD'
           ,'1'
           ,''
           ,'1'
           ,'A checkbox which is used to denote whether'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9797'
           ,'RIPROD'
           ,'2'
           ,''
           ,'1'
           ,'the R/I Ceding % to be applied on the total '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9797'
           ,'RIPROD'
           ,'3'
           ,''
           ,'1'
           ,'SI of all the insured members.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

--------Policy Conveyance Indicator-------

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9352'
           ,'DCNIND'
           ,'1'
           ,''
           ,'1'
           ,'A checkbox which is used to denote whether'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9352'
           ,'DCNIND'
           ,'2'
           ,''
           ,'1'
           ,'that of the policy has policy conveyance'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9352'
           ,'DCNIND'
           ,'3'
           ,''
           ,'1'
           ,'limit captured.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ991','',1,'1','This is a screen which allows user to specify treaty','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
														
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ991','',2,'1','arrangement code, RI Retention Basis for a particular','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ991','',3,'1',' date range. Users can input maximum 8 ranges for','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ991','',4,'1',' each of product code. In case, there are greater','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ991','',5,'1',' than 8 ranges, users need to create a new item','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ991','',6,'1',' and put it into Continuation field. If there is','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ991','',7,'1',' no arrangement applicable, the arrangement code','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ991','',8,'1',' should be entered as �9999�.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ981','',1,'1','This screen is used for capturing information of','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ981','',2,'1','treaty reinsurance proportional','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
	
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ982','',1,'1','This screen captures information of treaty special','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ982','',2,'1','acceptance with Level and Approval Number of tr','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ983','',1,'1','This screen captures additional note of treaty special','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ983','',2,'1','acceptance remarks.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ984','',1,'1','This screen is used to create, maintain information','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ985','',1,'1','This screen is used to create, maintain information','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ985','',2,'1','of participant for each of facultative proportional','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ985','',3,'1',' account.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ986','',1,'1','This screen is used to create, maintain information','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ987','',1,'1','This screen is used to adjust reinsurance premium,','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ987','',2,'1','commission and other charges (if any) for particular','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ974','',1,'1','This screen is used to display list of adjusted','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ974','',2,'1','premium, commission and other charges (if any) of','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ974','',3,'1',' co-insurer(s) or reinsurer(s) for inputted policy.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ975','',1,'1','This screen is used to display detail information','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ975','',2,'1','about premium journal of a selected co-insurer or','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ975','',3,'1',' reinsurer.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ970','',1,'1','This submenu displays the option to enquire on the','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ970','',2,'1','reinsurance claim recovery','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ971','',1,'1','This screen will displayed the criterial for selection','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ971','',2,'1','of the claim R/I for enquiry','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ988','',1,'1','This  Claim RI Journal Submenu displays the option','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ988','',2,'1','to create the claim R/I journal','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ989','',1,'1','This screen enables Journals to be posted to Reinsurance','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','S','SQ989','',2,'1','Accounts associated with a claim.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ911','ITEMCOT','1','1','Company code ','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
	
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ911','ITEMTABL','1','1','Table code','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
	
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ911','ITEMITEM','1','1','Item code','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ911','LONGDESC','1','1','Item long description','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ911','ITEMFRM','1','1','Start date of treaty arrangement.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ911','ITEMTO','1','1','End date of treaty arrangement','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ911','TARR','1','1','Treaty Arrangement code','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ911','RIRETBAS','1','1','Valid RI Retention Basis for product. There will','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ911','RIRETBAS','2','1','be maximum 2 slots RI Retention basis.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ911','CONTITEM','1','1','Continuation item code of current item code.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ988','CLMNM','1','1','Claim+ Occurrence Number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ989','CLMNM','1','1','Claim+ Occurrence Number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ989','PRODTYP','1','1','Product Type','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ989','PLANNO','1','1','Plan Number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ989','GCMBNAME','1','1','The name of the claimant','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ989','MBRNO','1','1','The headcount/member no of the insured member','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ989','DPNTNO','1','1','The headcount/member no of the insured member','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ989','CLAIMCUR','1','1','Policy Billing currency','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ989','TTYJLTYP','1','1','The journal code /description of the journal created','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ989','LONGDESC','1','1','The journal code /description of the journal created','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ989','RACC','1','1','The reinsurance account attached to the policy/','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ989','RACC','2','1','product /member','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ989','RITYPE','1','1','Reinsurance Type','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ989','PAYMRI','1','1','The amount to be adjusted/recovered against the','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ989','PAYMRI','2','1','reinsurance account','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ975','COWNNUM','1','1','Policy owner number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ975','OWNERNAME','1','1','Policy owner name','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ975','CHDRNUM','1','1','Policy number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ975','CNTTYPE','1','1','Policy type','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ975','CTYPDESC','1','1','Policy type description','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ975','BILLCURR','1','1','Billing currency','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ975','CRATE','1','1','Exchange rate of billing currency','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ975','CURR','1','1','Accounting currency','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ975','DTEEFF','1','1','Effective Date of journal transaction','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ975','OCCDATE','1','1','Inception Date of policy.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ975','CRDATE','1','1','Expiry Date of policy','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ975','RACC','1','1','Coinsurer/ Reinsurer number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ975','ACCNAME','1','1','Coinsurer/Reinsurer name','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ975','Z6TAXCDE','1','1','The Tax Code attached to the respective premium o','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ975','Z6TAXCDE','2','1',' charges','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ975','ORIGNET','1','1','Adjusted Net premium of reinsurer','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ974','RACC','1','1','Coinsurer/ Reinsurer number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ974','DTEEFF','1','1','Effective Date','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ974','ORIGNET','1','1','Net premium of reinsurer for journal transactio','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ974','MBRNO, ','1','1','The headcount/member no of the insured member','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ974','DPNTNO','1','1','The headcount/member no of the insured member','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ974','PLANNO','1','1','The plan no attached to the member/ product','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ974','PRODTYP','1','1','Product Type & Description','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ974','BATCBATCH','1','1','Batch No','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ974','BATCACTYR','1','1','Accounting year of journal transaction','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ974','BATCACTMN','1','1','Accounting month of journal transaction','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ974','TRANNO','1','1','Transaction number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ974','CRDATE','1','1','Renewal date','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ974','OCCDATE','1','1','Inception date','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ974','OWNERNAME','1','1','Policy owner name','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ974','COWNNUM','1','1','Policy owner number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ974','CTYPDESC','1','1','Policy type description','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ974','CNTTYPE','1','1','Policy type','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ974','CHDRNUM','1','1','Policy number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ983 ','CHDRNO','1','1','Policy number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ983 ','CHDRTY','1','1','Policy type','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ983 ','PRODTYP','1','1','Product type.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ983 ','PRODTYPDSC','1','1','Product name','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ983 ','MBRNO','1','1','Headcount or member number in policy.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ983 ','DPNTNO','1','1','Dependant number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ983','DTEEFF','1','1','Effective date of selected product.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
												
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ983 ','TRTYACC','1','1','Selected treaty account.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ983 ','INTSNO','1','1','Sequence number of the selected treaty account from','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ983 ','INTSNO','2','1','�Treaty Special Acceptance� screen.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ983 ','REMARKS','1','1','Additional information for treaty special accept','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ982','CHDRNUM','1','1','Policy number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ982','CNTTYPE','1','1','Policy type','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ982','EFFDATE','1','1','Effective date of transaction.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ982','OCCDATE','1','1','Original Commencement Date of policy.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ982','PRODTYP','1','1','Product type','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ982','PRODNAME','1','1','Product name','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ982','TADESC','1','1','Treaty Arrangement code','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ982','HEADNO','1','1','Headcount or member number in policy.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ982','DPNTNO','1','1','Dependant number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ982','SEQNO','1','1','Sequence number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ982','TRTYACC','1','1','Treaty account','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ982','APPRNUM','1','1','Approval number of the treaty special acceptance','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ982','ACCEPTLEVL','1','1','A drop down list contains last approval levels due','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ982','ACCEPTLEVL','2','1','for Special Arrangement policies which are defined','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ982','ACCEPTLEVL','3','1',' in table TM800 � Approval Level.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
	
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ971','TRANCD','1','1','The transaction code of the record','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
	
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ971','CLAIM','1','1','Claim+ Occurrence Number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ971','PRODTYP','1','1','Product Type','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ971','PLANNO','1','1','Plan Number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ971','MBRNO','1','1','The headcount/member no of the insured member','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ971','MEMNAME','1','1','The name of the claimant','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ971','RACC','1','1','The reinsurance account attached to the policy/ prod','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ971','RITYPE','1','1','The reinsurance type of the the reinsurance account','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
	
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ971','CLCURR','1','1','Currency','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ971','PAYMRI','1','1','Amount to be adjusted/ recovered','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ971','TRANDATE','1','1','The transaction date of the record','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ971','GCAUTHBY','1','1','The user id that authorized the claim','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ970','CLMNM','1','1','Claim+ Occurrence Number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ970','COWNSEL','1','1','Policy Number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ970','CLNTNO','1','1','Insured member client number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
	
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ985','DTEEFF','1','1','Effective date of transaction.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ985','CHDRNO','1','1','Policy number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ985','CHDRTY','1','1','Policy type.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ985','CHDRNODSC','1','1','Policy type long description.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ985','BILCUR','1','1','Billing Currency','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ985','DTEATT','1','1','First inception date of policy.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ985','PRODTYP','1','1','Product type','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ985','PRODTYPDSC','1','1','Product name','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ985','SRIPPN','1','1','Ceding percentage','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ985','RACC','1','1','Facultative reinsurer account number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ985','LNAME','1','1','Facultative reinsurer name','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ985','MBRNO','1','1','Headcount or member number in policy.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ985','DPNTNO','1','1','Dependant number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ985','CLNTNUM','1','1','Participant number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ985','USERNAME','1','1','Participant name','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ985','SPTPPN','1','1','Percentage of participant number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ985','DESCRIPT','1','1','Additional note for particular participant','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ985','STTPPN','1','1','Total percentage of all participant.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','CHDRNO','1','1','Policy number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','CHDRTY','1','1','Policy type.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','CHDRNODSC','1','1','Policy type long description.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','DTEEFF','1','1','Effective date of transaction','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','DTEATT','1','1','First inception date of policy','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','PRODTYP','1','1','Product type','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','PRODTYPDSC','1','1','Product name','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','BILCUR','1','1','Billing currency','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','MBRNO','1','1','Headcount or member number in policy.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','DPNTNO','1','1','Dependant number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','ORGPRM','1','1','Total Facultative XOL Sum Insured/ Premium which','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','ORGPRM','2','1','is from R/I Treaty Details in Original Currency','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','PREM','1','1','Premium amount of that reinsurer.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','LCEDED','1','1','Deductible amount of facultative XOL reinsurer in','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','LCEDED','2','1','Ledger Currency.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','LCEUPP','1','1','Upper amount of facultative XOL reinsurer in Ledger','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','ORGDED','1','1','Deductible amount of facultative XOL reinsurer in','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','ORGDED','2','1','Original Currency.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','COMMAMT','1','1','Commission amount of facultative XOL reinsurer.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','COMMAMT','2','1','System will calculate base on Commission percentage','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','COMMAMT','3','1',' or users can manually input.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','GRNR','1','1','Ceding method of facultative XOL reinsurer.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','OCP','1','1','An indicator to denote whether facultative XOL pay','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','OCP','2','1','Original Commission Plus or not.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','RIRENDES','1','1','RI Retention basis','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','CEDEBS','1','1','R/I Ceding Basis','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
 	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','SILCE','1','1','Gross Acceptance Limit which is from R/I Treaty','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','SILCE','2','1','Details in Ledger Currency.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','SSIRAT','1','1','Exchange rate of billing currency.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','SIORIG','1','1','Gross Acceptance Limit which is from R/I Treaty','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','SIORIG','2','1','Details in Original Currency','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','NFLCE','1','1','Total Facultative XOL Sum Insured/ Premium which','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','NFLCE','2','1','is from R/I Treaty Details in Ledger Currency.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','TXOLPRM','1','1','Total Fac XOL premium which is from R/I Treaty Deta','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','RACCS','1','1','Facultative XOL Reinsurer account.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','RCVDATE','1','1','Reinsurance Placement Completion Date','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','DEFVAL','1','1','The staff who validated the RI Placement','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','RICOMM','1','1','Commission percentage of facultative XOL reinsure','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','RIREF','1','1','Reference number of facultative XOL reinsurer.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ986','TLCPRM','1','1','Total premium of all facultative reinsurer.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','COWNNUM','1','1','Policy owner number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','CTONAM','1','1','Policy owner name','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','CHDRNO','1','1','Policy number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','CNTTYP','1','1','Policy type','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','LONGDESC','1','1','Policy type description','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','PRODTYP','1','1','Product Type & Description','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','BILCUR','1','1','Billing currency','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','LEDGCURR','1','1','Accounting currency','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','OCCDATE','1','1','First inception date of policy','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','EFFDATE','1','1','Effective Date of journal transaction','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','CCDATE','1','1','Inception Date of policy','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','CRDATE','1','1','Expiry Date of policy','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','RACC','1','1','Coinsurer/ Reinsurer number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','LNAME','1','1','Coinsurer/ Reinsurer name','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','MBRNO','1','1','The headcount/member no of the insured member','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','EXTR','1','1','Adjusted ceded premium','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','SCRATE','1','1','Adjusted ceded extra charges','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','TOTDISC','1','1','Total number of page for journal transaction','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','PLANNO','1','1','The plan no attached to the member/ product','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','TOTPRE','1','1','Total ceded premium','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','DISCOUNT','1','1','Adjusted ceded discount','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','TDISCOUNT','1','1','Total ceded discount','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','AGANNL','1','1','Adjusted ceded commission','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','TAGANNL','1','1','Total ceded commission','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','ORANNL','1','1','Adjusted ceded overriding commission','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','TORANNL','1','1','Total ceded overriding commission','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','TOANNL','1','1','Adjusted ceded net premium','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','TTOANNL','1','1','Total ceded net premium','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ987','DISCNO','1','1','Current page','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','CHDRNO','1','1','Policy number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','CHDRTY','1','1','Policy type','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','CHDRNODSC','1','1','Policy type description','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','DTEEFF','1','1','Effective date of transaction.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','DTEATT','1','1','Original Commencement Date of policy.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','PRODTYP','1','1','Product type','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','PRODTYPDSC','1','1','Product name','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','BILCUR','1','1','Billing currency','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
														
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','MBRNO','1','1','Headcount or member number in policy.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','DPNTNO','1','1','Dependant number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','FACPPN','1','1','Total facultative percentage','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','FACCURR','1','1','Original ceded Sum Insured or premium currency.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','FACORIG','1','1','Ceded Sum Insured or premium in Original Currency.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','FACLCE','1','1','Ceded Sum Insured or premium In Ledger Currency.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','SSIPPN','1','1','Total Sum/ Premium percentage of Net Retention, ','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','SSIPPN','2','1','reaty R/I.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','SNFPPN','1','1','Total Sum/ Premium  percentage  of  Facultative','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','SNFPPN','2','1',' proportional','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','RIRENDES','1','1','RI Retention Basis','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','CEDEBS','1','1','R/I Ceding Basis','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
											
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','SICURR','1','1','Sum Insured or premium original currency.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','SIORIG','1','1','Total Sum Insured or premium amount of Net Retention,','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','SIORIG','2','1','Treaty R/I which is from R/I Treaty Detail screen','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','SIORIG','3','1',' in Original Currency','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','NFORIG','1','1','Total Sum Insured or premium amount of Facultative','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','NFORIG','2','1','proportional which is from R/I Treaty Detail screen','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','NFORIG','3','1',' in Original Currency','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
	
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','NFLCE','1','1','Total Sum Insured or premium amount of Facultative','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','NFLCE','2','1','proportional which is from R/I Treaty Detail screen','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','NFLCE','3','1',' in Ledger Currency.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','SSIRAT','1','1','Exchange rate of original Sum Insured/ Premium cur','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','RACC','1','1','Reinsurer account','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','LNAME','1','1','Reinsurer account name.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','LXOPTIND','1','1','Facultative participant check box which is used','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','LXOPTIND','2','1','to access to �Product RI � Fac. Participant� screen.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','RIREF','1','1','Facultative Reinsurer reference number.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															

	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','RCVDATE','1','1','Reinsurance Placement Completion Date','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','DEFVAL','1','1','The staff who validated the RI Placement','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','SRIPPN','1','1','Ceded percentage on Sum Insured','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','SFCPPN','1','1','Ceded percentage on premium','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','RILCE','1','1','Ceded Sum Insured in Ledger Currency.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','RPLCE','1','1','Ceded Premium in Ledger Currency.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','RIORIG','1','1','Ceded Sum Insured in Original Currency.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','RPORIG','1','1','Ceded Premium in Original Currency.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','RICOMM','1','1','Commission percentage','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','GRNR','1','1','Ceding method of facultative reinsurer(s)','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','COMMAMT','1','1','Commission amount','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','OCP','1','1','An indicator to denote whether facultative reinsurer','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','OCP','2','1','pay Original Commission Plus or not','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','PRTRI','1','1','An indicator to denote whether RI application will','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','PRTRI','2','1','be generated for particular facultative reinsurer(s) ','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ984','PRTRI','3','1','or not','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','CHDRNO','1','1','Policy number','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','CHDRTY','1','1','Policy type','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','CHDRTYDSC','1','1','Policy type long description','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','DTEEFF','1','1','Effective date of transaction.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','OCCDATE','1','1','First inception date of policy.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','PRODTYP','1','1','Product type','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','PRODTYPDSC','1','1','Product name.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','BILCUR','1','1','Billing Currency','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','ACURR','1','1','Treaty Arrangement code','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','RITWOC','1','1','Treaty Arrangement currency','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','SICURR','1','1','Original Sum Insured or annualized premium currency.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','SIORIG','1','1','Total Sum Insured or annualized premium of member,','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','SIORIG','2','1','dependant or headcount for that product or Target','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','SIORIG','3','1',' SIlimit in Original Currency.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','SILCE','1','1','Exposure Sum Insured or annualized premium in Original','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','MBRNO','1','1','Headcount or member number in policy.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','DPNTNO','1','1','Dependant number ','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','LCCCY','1','1','Ledger Sum Insured or annualized premium currency.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','SIEXP','1','1','Total Sum Insured or annualized premium of member,','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','SIEXP','2','1','dependant or headcount for that product or Target','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','SIEXP','3','1',' SI in Ledger Currency.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','SIEXPLCE','1','1','Exposure Sum Insured or annualized premium in Ledger','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','RIRENDES','1','1','RI Retention Basis','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','SIRAT','1','1','Exchange Rate','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','CEDEBS','1','1','RI Ceding Basis.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','PML10','1','1','Treaty Arrangement Possible Maximum Loss percent','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','SRIPPN01','1','1','Net Retention percentage.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','RILCE01','1','1','Net Retention amount in ledger currency.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','RIORIG01','1','1','Net Retention amount in original currency.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','PML01','1','1','Net Retention Possible Maximum Loss percentage.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','QACC','1','1','Quota share account from Treaty Arrangement.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','SQSPPN','1','1','Quota Share percentage','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','PML02','1','1','Quota Share Possible Maximum Loss percentage.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','SACC02','1','1','1st Surplus Treaty account.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','SRIPPN03','1','1','1st Surplus Treaty percentage or Total percentage','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','SRIPPN03','2','1','of all treaty surplus when Treaty Arrangement has','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','SRIPPN03','3','1',' many surplus account.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','RILCE03','1','1','1st Surplus Treaty amount in Ledger Currency or','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','RILCE03','2','1','Total amount of all treaty surplus in Ledger currency','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','RILCE03','3','1',' when Treaty Arrangement has many surplus account','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','RIORIG03','1','1','1st Surplus Treaty amount in Original Currency or','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','RIORIG03','2','1','total amount of all treaty surplus in Original currency','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','RIORIG03','3','1',' when Treaty Arrangement has many surplus account.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','PML03','1','1','1st Surplus Treaty Possible Maximum Loss percentage.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','PML03','2','1','This field is available when Treaty Arrangement','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','PML03','3','1',' doesn�t have or just only one treaty surplus.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','SRIPPN08','1','1','Gross retention percentage.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','RILCE08','1','1','Gross Retention amount in Ledger currency.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','RIORIG08','1','1','Gross Retention amount in Original currency.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','GACC','1','1','Government Treaty account','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','SRIPPN09','1','1','Government Treaty percentage','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','RILCE09','1','1','Government Treaty amount in Ledger currency.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','RIORIG09','1','1','Government Treaty amount in Original currency.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','PML09','1','1','Government Treaty Possible Maximum Loss percenta','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','SRIPPN10','1','1','Facultative Proportional percentage','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','RILCE10','1','1','Facultative Proportional amount in Ledger currency','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','RIORIG10','1','1','Facultative Proportional amount in Original Curren','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','SRIPPN11','1','1','Total percentage of Net Retention, Treaty R/I and','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','SRIPPN11','2','1','Facultative Proportional','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','RILCE11','1','1','Total amount of Net Retention, Treaty R/I and Facultative','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','RILCE11','2','1','Proportional in Ledger Currency','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','RIORIG11','1','1','Total amount of Net Retention, Treaty R/I and Facultative','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','RIORIG11','2','1','Proportional in Original Currency.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','RILCE12','1','1','Facultative XOL amount in Ledger Currency.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','RIORIG12','1','1','Facultative XOL amount in Original Currency.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','TXOLPRM','1','1','Total premium of Facultative XOL.','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','RILCE13','1','1','Total amount of Net Retention, Treaty R/I, Facultative','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','RILCE13','2','1','Proportional and Facultative XOL in Ledger Currency','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','RIORIG13','1','1','Total amount of Net Retention, Treaty R/I, Facultative','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																
	INSERT INTO VM1DTA.HELPPF ([TRANID],[HELPPFX],[HELPCOY],[HELPLANG],[HELPTYPE],[HELPPROG],[HELPITEM],[HELPSEQ],[VALIDFLAG],[HELPLINE],[USRPRF],[JOBNM],[DATIME]) 															
	VALUES ('','HP',' ','E','F','SQ981','RIORIG13','2','1','Proportional and Facultative XOL in Original Curren','UNDERWR1  ','UNDERWR1',CURRENT_TIMESTAMP);															
																



