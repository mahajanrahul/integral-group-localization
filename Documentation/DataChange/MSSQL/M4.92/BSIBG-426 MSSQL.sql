INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'Sr95a'
           ,'OPTAUTORNW'
           ,'1'
           ,''
           ,'1'
           ,'An indicator to denote whether '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'Sr95a'
           ,'OPTAUTORNW'
           ,'2'
           ,''
           ,'1'
           ,'that policy can be renewal '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'Sr95a'
           ,'OPTAUTORNW'
           ,'3'
           ,''
           ,'1'
           ,'without send renewal invitation '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'Sr95a'
           ,'OPTAUTORNW'
           ,'4'
           ,''
           ,'1'
           ,'confirmation from policy owner.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'Sq9a8'
           ,''
           ,'1'
           ,''
           ,'1'
           ,'This screen is used to define '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'Sq9a8'
           ,''
           ,'2'
           ,''
           ,'1'
           ,'loss ratio in current year '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'Sq9a8'
           ,''
           ,'3'
           ,''
           ,'1'
           ,'and overall which can be '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'Sq9a8'
           ,''
           ,'4'
           ,''
           ,'1'
           ,'generated renewal notice.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'Sq9a8'
           ,'CURYEAR'
           ,'1'
           ,''
           ,'1'
           ,'Loss ratio in current year that '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'Sq9a8'
           ,'CURYEAR'
           ,'2'
           ,''
           ,'1'
           ,'can be sent renewal notice.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'Sq9a8'
           ,'OVERALL'
           ,'1'
           ,''
           ,'1'
           ,'Loss ratio in overall that can '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'Sq9a8'
           ,'OVERALL'
           ,'2'
           ,''
           ,'1'
           ,'be sent renewal notice.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO