ALTER TABLE [VM1DTA].[GCHIPF]
ADD [TPAFLG] [nchar](1)
GO

/*VIEW*/
/*ALTER VIEW GCHI*/
ALTER VIEW [VM1DTA].[GCHI] (
	UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMECH01
	,TIMECH02
	,ECNV
	,CVNTYPE
	,COVERNT
	,TPAFLG
	)
AS
SELECT UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMECH01
	,TIMECH02
	,ECNV
	,CVNTYPE
	,COVERNT
	,TPAFLG

FROM VM1DTA.GCHIPF;

GO

/****** Object:  View [VM1DTA].[GCHITPA]    Script Date: 08/18/2016 11:20:48 ******/

ALTER VIEW [VM1DTA].[GCHITPA] (
	UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMECH01
	,TIMECH02
	,ECNV
	,CVNTYPE
	,COVERNT
	,TPAFLG
	)
AS
SELECT UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMECH01
	,TIMECH02
	,ECNV
	,CVNTYPE
	,COVERNT
	,TPAFLG
FROM VM1DTA.GCHIPF
WHERE TPA <>'';


GO