DECLARE @batchNum nvarchar(4) = (SELECT FORMAT(BLASTSCHNO, 'D4') FROM [VM1DTA].[BSNR] WHERE BSCHEDNAM = 'G3CREDITS')
DECLARE @table_name_pref varchar(20)
SET @table_name_pref = 'BACSDC'
DECLARE @tableName nvarchar(256) = CONCAT(@table_name_pref , @batchNum)

SELECT PT_TABLE_NAME into #table_member_component	
FROM [QUIPOZ].[MEMBER_COMPONENT] with (nolock)
WHERE substring(PT_TABLE_NAME, 0, len(PT_TABLE_NAME) - 3 )  = @table_name_pref 
	and cast( (right(PT_TABLE_NAME,4)) as int )  > cast ( @batchNum as int )
order by PT_TABLE_NAME 

SELECT table_name into #table_member_control	
FROM [QUIPOZ].[MEMBER_CONTROL] with (nolock)
WHERE substring(table_name, 0, len(table_name) - 3 )  = @table_name_pref 
	and cast( (right(table_name,4)) as int )  > cast ( @batchNum as int )
order by table_name 

SELECT table_name into #table_member_table	
FROM [QUIPOZ].[MEMBER_TABLE] with (nolock)
WHERE substring(table_name, 0, len(table_name) - 3 )  = @table_name_pref 
	and cast( (right(table_name,4)) as int )  > cast ( @batchNum as int )
ORDER BY table_name 

DELETE FROM [QUIPOZ].[MEMBER_COMPONENT] WHERE PT_TABLE_NAME IN ( select PT_TABLE_NAME from #table_member_component)
DELETE FROM [QUIPOZ].[MEMBER_TABLE] WHERE TABLE_NAME  IN ( select table_name from #table_member_table)
DELETE FROM [QUIPOZ].[MEMBER_CONTROL] WHERE TABLE_NAME IN ( select table_name from #table_member_control)
DROP TABLE #table_member_component
DROP TABLE #table_member_control
DROP TABLE #table_member_table