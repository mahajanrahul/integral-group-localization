ALTER TABLE [VM1DTA].[PM13PF]
ADD [ADPREMNT] [numeric](15, 2) NULL,
	[ADPREMTYP] [nchar](1) NULL ;

ALTER TABLE [VM1DTA].[GPHIPF]
ADD [TRIPTYP] [nchar](2)  NULL,
	[PREMPRATE] [nchar](1) NULL ;

ALTER TABLE [VM1DTA].[GCBNPF]
ADD [HRSFROM] [numeric](2, 0) NULL,
	[MINUTFROM] [numeric](2, 0) NULL,
	[TIMETYP1] [nchar](2) NULL,
	[HRSTO] [numeric](2, 0) NULL,
	[MINUTTO] [numeric](2, 0) NULL,
	[TIMETYP2] [nchar](2) NULL;


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [VM1DTA].[BFNLPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[PRODTYP] [nchar](4) NULL,
	[PLANNO] [nchar](3) NULL,
	[BENCDE] [nchar](4) NULL,
	[CHILDLIMIT] [numeric](15, 2) NULL,
	[ADULTLIMIT1] [numeric](15, 2) NULL,
	[ADULTLIMIT2] [numeric](15, 2) NULL,
	[AGEUPTO] [int] NULL,
	[AGEABOV] [int] NULL,
	[FAMLIMIT] [numeric](15, 2)  NULL,
	[GBENCDE] [nchar](4) NULL,
	[NOHOURS] [int] NULL,
	[AMOUNT] [bigint] NULL,
	[ADNOHOURS] [int] NULL,
	[ADAMOUNT] [bigint] NULL,
	[TRANNO] [int] NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL,
 CONSTRAINT [PK_BFNLPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [VM1DTA].[BFGLPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[PRODTYP] [nchar](4) NULL,
	[PLANNO] [nchar](3) NULL,
	[GBENCDE] [nchar](4) NULL,
	[CHILDLIMIT] [numeric](15, 2) NULL,
	[ADULTLIMIT1] [numeric](15, 2) NULL,
	[ADULTLIMIT2] [numeric](15, 2) NULL,
	[AGEUPTO] [int] NULL,
	[AGEABOV] [int] NULL,
	[FAMLIMIT] [numeric](15, 2)  NULL,
	[TRANNO] [int] NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL,
 CONSTRAINT [PK_BFGLPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
	

CREATE TABLE [VM1DTA].[GCBLPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[PRODTYP] [nchar](4) NULL,
	[PLANNO] [nchar](3) NULL,
	[CLAMNUM] [nchar](8) NULL,
	[GBENCDE] [nchar](4) NULL,
	[GCLAIMAMT] [numeric](15, 2) NULL,
	[HRSFROM] [numeric](2, 0) NULL,
	[MINUTFROM] [numeric](2, 0) NULL,
	[TIMETYP1] [nchar](2) NULL,
	[HRSTO] [numeric](2, 0) NULL,
	[MINUTTO] [numeric](2, 0) NULL,
	[TIMETYP2] [nchar](2) NULL,
	[TRANNO] [int] NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL,
 CONSTRAINT [PK_GCBLPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [VM1DTA].[CBNLPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[PRODTYP] [nchar](4) NULL,
	[PLANNO] [nchar](3) NULL,
	[CLAMNUM] [nchar](8) NULL,
	[BENCDE] [nchar](4) NULL,
	[GBENCDE] [nchar](4) NULL,
	[GCLAIMAMT] [numeric](15, 2) NULL,
	[HRSFROM] [numeric](2, 0) NULL,
	[MINUTFROM] [numeric](2, 0) NULL,
	[TIMETYP1] [nchar](2) NULL,
	[HRSTO] [numeric](2, 0) NULL,
	[MINUTTO] [numeric](2, 0) NULL,
	[TIMETYP2] [nchar](2) NULL,
	[TRANNO] [int] NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL,
 CONSTRAINT [PK_CBNLPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

DROP VIEW [VM1DTA].[PM13]
GO

/****** Object:  View [VM1DTA].[PM13]    Script Date: 4/13/2016 3:01:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [VM1DTA].[PM13](UNIQUE_NUMBER, CHDRCOY, GPOLTYP, PRODTYP, PLANNO, TRIPTYP, TRVLZON, IFIND, EFFDATE, TOAGE, TRVLDUR, APREM, RATETYP,ADPREMNT,ADPREMTYP, JOBNM, USRPRF, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            GPOLTYP,
            PRODTYP,
            PLANNO,
            TRIPTYP,
            TRVLZON,
            IFIND,
            EFFDATE,
            TOAGE,
            TRVLDUR,
            APREM,
            RATETYP,
			ADPREMNT,
			ADPREMTYP,
            JOBNM,
            USRPRF,
            DATIME
       FROM VM1DTA.PM13PF

GO


DROP VIEW [VM1DTA].[GPHI]
GO

/****** Object:  View [VM1DTA].[GPHI]    Script Date: 4/13/2016 3:04:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [VM1DTA].[GPHI](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, PRODTYP, EFFDATE, DTETRM, CMRATE, DISCRATE, DISCAMT, SPECTRM, RFUNDFLG, RFUNDPC01, RFUNDPC02, ADEXPFLG, ARFUNDPC, COVLMT01, COVLMT02, COVLMT03, COVLMT04, COVAGE01, COVAGE02, COVAGE03, COVAGE04, TERMID, USER_T, TRDT, TRTM, TRANNO, GCLRPERD, LMTPCT, RNCMRATE, NBFDFEE, RNFDFEE, CTBFRQ, INVSRULE, RIDISCPE, MANDAYS, DMDAYDED, MINMANDAYS, BILMANDAYS, PRMAMTRT,TRIPTYP,PREMPRATE, USRPRF, JOBNM, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            PRODTYP,
            EFFDATE,
            DTETRM,
            CMRATE,
            DISCRATE,
            DISCAMT,
            SPECTRM,
            RFUNDFLG,
            RFUNDPC01,
            RFUNDPC02,
            ADEXPFLG,
            ARFUNDPC,
            COVLMT01,
            COVLMT02,
            COVLMT03,
            COVLMT04,
            COVAGE01,
            COVAGE02,
            COVAGE03,
            COVAGE04,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            TRANNO,
            GCLRPERD,
            LMTPCT,
            RNCMRATE,
            NBFDFEE,
            RNFDFEE,
            CTBFRQ,
            INVSRULE,
            RIDISCPE,
            MANDAYS,
            DMDAYDED,
            MINMANDAYS,
            BILMANDAYS,
            PRMAMTRT,
			TRIPTYP,
			PREMPRATE,
            USRPRF,
            JOBNM,
            DATIME
       FROM VM1DTA.GPHIPF

GO

DROP VIEW [VM1DTA].[GCBN]
GO

/****** Object:  View [VM1DTA].[GCBN]    Script Date: 4/13/2016 3:06:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [VM1DTA].[GCBN](UNIQUE_NUMBER, CHDRCOY, CLAMNUM, GCOCCNO, GCBENSEQ, BENCDE, GCINCAMT, BENAMT, BENBASIS, BENLMT, BENLMTB, GCBENPY, GCSYSPY, GCSYSRP, GCRDRPY, HSUMINSU, BENPC, RASCOY01, RASCOY02, RASCOY03, RASCOY04, RASCOY05, ACCNUM01, ACCNUM02, ACCNUM03, ACCNUM04, ACCNUM05, GRHCQSSI01, GRHCQSSI02, GRHCQSSI03, GRHCQSSI04, GRHCQSSI05, TERMID, USER_T, TRDT, TRTM, GCPROVCD,HRSFROM,MINUTFROM,TIMETYP1,HRSTO,MINUTTO,TIMETYP2, USRPRF, JOBNM, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CLAMNUM,
            GCOCCNO,
            GCBENSEQ,
            BENCDE,
            GCINCAMT,
            BENAMT,
            BENBASIS,
            BENLMT,
            BENLMTB,
            GCBENPY,
            GCSYSPY,
            GCSYSRP,
            GCRDRPY,
            HSUMINSU,
            BENPC,
            RASCOY01,
            RASCOY02,
            RASCOY03,
            RASCOY04,
            RASCOY05,
            ACCNUM01,
            ACCNUM02,
            ACCNUM03,
            ACCNUM04,
            ACCNUM05,
            GRHCQSSI01,
            GRHCQSSI02,
            GRHCQSSI03,
            GRHCQSSI04,
            GRHCQSSI05,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            GCPROVCD,
			HRSFROM,
			MINUTFROM,
			TIMETYP1,
			HRSTO,
			MINUTTO,
			TIMETYP2,
            USRPRF,
            JOBNM,
            DATIME
       FROM VM1DTA.GCBNPF

GO


CREATE VIEW [VM1DTA].[BFNL](UNIQUE_NUMBER, CHDRCOY,CHDRNUM,PRODTYP,PLANNO,BENCDE,CHILDLIMIT,ADULTLIMIT1,ADULTLIMIT2,AGEUPTO,AGEABOV,FAMLIMIT,GBENCDE,NOHOURS,AMOUNT,ADNOHOURS,ADAMOUNT,TRANNO,USRPRF, JOBNM, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
			CHDRNUM,
			PRODTYP,
			PLANNO,
			BENCDE,
			CHILDLIMIT,
			ADULTLIMIT1,
			ADULTLIMIT2,
			AGEUPTO,
			AGEABOV,
			FAMLIMIT,
			GBENCDE,
			NOHOURS,
			AMOUNT,
			ADNOHOURS,
			ADAMOUNT,
			TRANNO,
            USRPRF,
            JOBNM,
            DATIME
       FROM VM1DTA.BFNLPF

GO

CREATE VIEW [VM1DTA].[BFGL](UNIQUE_NUMBER, CHDRCOY,CHDRNUM,PRODTYP,PLANNO,GBENCDE,CHILDLIMIT,ADULTLIMIT1,ADULTLIMIT2,AGEUPTO,AGEABOV,FAMLIMIT,TRANNO,USRPRF, JOBNM, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
			CHDRNUM,
			PRODTYP,
			PLANNO,
			GBENCDE,
			CHILDLIMIT,
			ADULTLIMIT1,
			ADULTLIMIT2,
			AGEUPTO,
			AGEABOV,
			FAMLIMIT,
			TRANNO,
            USRPRF,
            JOBNM,
            DATIME
       FROM VM1DTA.BFGLPF

GO

CREATE VIEW [VM1DTA].[GCBL](UNIQUE_NUMBER, CHDRCOY,CHDRNUM,PRODTYP,PLANNO,CLAMNUM,GBENCDE,GCLAIMAMT,HRSFROM,MINUTFROM,TIMETYP1,HRSTO,MINUTTO,TIMETYP2,TRANNO,USRPRF,JOBNM,DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
			CHDRNUM,
			PRODTYP,
			PLANNO,
			CLAMNUM,
			GBENCDE,
			GCLAIMAMT,
			HRSFROM,
			MINUTFROM,
			TIMETYP1,
			HRSTO,
			MINUTTO,
			TIMETYP2,
			TRANNO,
            USRPRF,
            JOBNM,
            DATIME
       FROM VM1DTA.GCBLPF

GO

CREATE VIEW [VM1DTA].[CBNL](UNIQUE_NUMBER, CHDRCOY,CHDRNUM,PRODTYP,PLANNO,CLAMNUM,BENCDE,GBENCDE,GCLAIMAMT,HRSFROM,MINUTFROM,TIMETYP1,HRSTO,MINUTTO,TIMETYP2,TRANNO,USRPRF,JOBNM,DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
			CHDRNUM,
			PRODTYP,
			PLANNO,
			CLAMNUM,
			BENCDE,
			GBENCDE,
			GCLAIMAMT,
			HRSFROM,
			MINUTFROM,
			TIMETYP1,
			HRSTO,
			MINUTTO,
			TIMETYP2,
			TRANNO,
            USRPRF,
            JOBNM,
            DATIME
       FROM VM1DTA.CBNLPF

GO



 /*Error Codes */

INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
('ER','','E','','RFQK','Provide Additional Prem','0','0','000036','UNDERWR1','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,''),
('ER','','E','','RFQL','Sel Additional Prem Typ','0','0','000036','UNDERWR1','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,''),
('ER','','E','','RFQM','Provide TripTyp','0','0','000036','UNDERWR1','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,''),
('ER','','E','','RFQN','Provide Prem Pro-rate','0','0','000036','UNDERWR1','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,''),
('ER','','E','','RFQO','Provide Time From','0','0','000036','UNDERWR1','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,''),
('ER','','E','','RFQP','Provide Time To','0','0','000036','UNDERWR1','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,''),
('ER','','E','','RFQQ','Provide Amount','0','0','000036','UNDERWR1','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,''),
('ER','','E','','RFQS','Select AM/PM','0','0','000036','UNDERWR1','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,''),
('ER','','E','','RFT6','Time from > Time to','0','0','000036','UNDERWR1','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,''),
('ER','','E','','RFQT','Provide Start and End Dt','0','0','000036','UNDERWR1','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP,'')
GO



/*New Screen */
 INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
('HP','3','E','S','SQ914','','1','','1','This screen is used to default Adult,','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','S','SQ914','','2','','1','Child and Family Limits for Benefit codes','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','S','SQ914','','3','','1','from TQ919� Benefit List Limits table.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','S','SQ914','','4','','1','Hourly Limits are also displayed for Travel Benefits.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','S','SQ915','','1','','1','This screen is used to Modify/Display','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','S','SQ915','','2','','1','the defaulted Adult, Child Family Limits','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','S','SQ915','','3','','1','and Hourly for selected Benefit code.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','S','SQ916','','1','','1','This screen is used to default Adult, Child ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','S','SQ916','','2','','1','and Family Limits for Benefit Group from .','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','S','SQ916','','3','','1','TQ920 � Benefit List Grouping Limits table.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','S','SQ917','','1','','1','This screen is used to Modify/Display','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','S','SQ917','','2','','1','the defaulted Adult, Child and Family','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','S','SQ917','','3','','1','Limits for selected Benefit Group.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP)
GO


/*New Field*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
('HP','3','E','F','Sr9xu','adpremnt','1','','1','This is a numeric field used to define','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sr9xu','adpremnt','2','','1','Additional Premium amount','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sr9xu','adpremtyp','1','','1','This is a drop down field with a �W- Weekly� ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sr9xu','adpremtyp','2','','1','as the available value to be selected','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sr9xu','adpremtyp','3','','1','for defining Additional Premium','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),

('HP','3','E','F','Sr9xt','adpremnt','1','','1','This is a numeric field that will display','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sr9xt','adpremnt','2','','1','Additional Premium amount','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sr9xt','adpremtyp','1','','1','This is a drop down field with a �W- Weekly� ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sr9xt','adpremtyp','2','','1','as the basis for Additional Premium.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),

('HP','3','E','F','S9114','premprate','1','','1','This is an alphanumeric field.This field','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','S9114','premprate','2','','1','is defaulted to N.The value of �N� indicates','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','S9114','premprate','3','','1','premium amount will not be Pro-rated','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','S9114','premprate','4','','1','to override current base processing.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','S9114','triptyp','1','','1','This is an dropdown field.This field is','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','S9114','triptyp','2','','1','accessed during premium calculation','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','S9114','triptyp','3','','1','using Premium Method 13','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),

('HP','3','E','F','Sr9gb','gmaspol','1','','1','This is a new field to perform enquiry','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sr9gb','gmaspol','2','','1','on the Child policies associated','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sr9gb','gmaspol','3','','1','with a particular Master policy','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),

('HP','3','E','F','Sq914','chdrnum','1','','1','This is a Protected field which displays ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','chdrnum','2','','1','the Policy Number.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','gpoltyp','1','','1','This is a Protected field which displays','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','gpoltyp','2','','1','the product code.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','desca','1','','1','This is a Protected field which displays','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','desca','2','','1','the product code description.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','effdate','1','','1','This field is for Effective date','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','ccdate','1','','1','This is a protected field for','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','ccdate','2','','1','displaying Original Commencement Date','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','ccdate','3','','1','of the contract.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','crdate','1','','1','This field is for Termination date.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','select','1','','1','This is a checkbox to select any','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','select','2','','1','of the displayed records.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','bencde','1','','1','This field displays the user ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','bencde','2','','1','defined Benefit Code.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','adbdes','1','','1','This field displays the description','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','adbdes','2','','1','of the Benefit Code.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','childlimit','1','','1','This field displays the user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','childlimit','2','','1','defined Child Limit.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','adultlimit','1','','1','This field displays the user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','adultlimit','2','','1','defined Adult Limit.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','ageupto','1','','1','This field displays the user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','ageupto','2','','1','defined  Age in Age Upto field.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','ageabov','1','','1','This field displays the user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','ageabov','2','','1','defined Age in Age above field.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','famlimit','1','','1','This field displays the user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','famlimit','2','','1','defined Family Limit.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','bnftgrp','1','','1','This field displays the user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','bnftgrp','2','','1','defined Benefit Group.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','nohours','1','','1','This field displays No of Hours ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','nohours','2','','1','for applicable Travel Benefits.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','amount','1','','1','This field displays the Limit ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','amount','2','','1','for No of Hours.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','adnohours','1','','1','This field displays Additional No of','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','adnohours','2','','1','Hours for applicable Travel Benefits.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','adamount','1','','1','This field displays Limit for','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq914','adamount','2','','1','Additional No of Hours.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),

('HP','3','E','F','Sq916','chdrnum','1','','1','This is a Protected field which displays','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq916','chdrnum','2','','1','the Policy Number.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq916','gpoltyp','1','','1','This is a Protected field which displays','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq916','gpoltyp','2','','1','the product code.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq916','desca','1','','1','This is a Protected field which','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq916','desca','2','','1','displays the product code description.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq916','effdate','1','','1','This field is for Effective date','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq916','ccdate','1','','1','This is a protected field for','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq916','ccdate','2','','1','displaying Original Commencement Date','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq916','ccdate','3','','1','of the contract.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq916','crdate','1','','1','This field is for Termination date.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq916','select','1','','1','This is a checkbox to select any','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq916','select','2','','1','of the displayed records.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq916','bencde','1','','1','This field displays the user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq916','bencde','2','','1','defined Benefit Code.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq916','childlimit','1','','1','This field displays the user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq916','childlimit','2','','1','defined Child Limit.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq916','adultlimit','1','','1','This field displays the user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq916','adultlimit','2','','1','defined Adult Limit.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq916','ageupto','1','','1','This field displays the user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq916','ageupto','2','','1','defined  Age in Age Upto field.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq916','ageabov','1','','1','This field displays the user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq916','ageabov','2','','1','defined Age in Age above field.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq916','famlimit','1','','1','This field displays the user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq916','famlimit','2','','1','defined Family Limit.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq916','bnftgrp','1','','1','This field displays the user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq916','bnftgrp','2','','1','defined Benefit Group.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),

('HP','3','E','F','Sq915','bencde','1','','1','This field displays the Benefit Code.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq915','childlimit','1','','1','This field displays the Child Limit ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq915','childlimit','2','','1','and it is editable when','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq915','childlimit','3','','1','user selects the Modify button.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq915','adultlimit','1','','1','This field displays the Adult Limit ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq915','adultlimit','2','','1','and it is editable when','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq915','adultlimit','3','','1','user selects Modify button','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq915','ageupto','1','','1','This field displays age in Age Upto','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq915','ageupto','2','','1','field and it is editable when.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq915','ageupto','3','','1','when user selects Modify button.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq915','ageabove','1','','1','This field displays age in Age above','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq915','ageabove','2','','1','field and it is editable when.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq915','ageabove','3','','1','when user selects Modify button.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq915','famlimit','1','','1','This field displays the Family Limit and it','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq915','famlimit','2','','1','is editable when user selects Modify button','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq915','bnftgrp','1','','1','This is a protected field displays','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq915','bnftgrp','2','','1','Benefit Group.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq915','nohours','1','','1','This field displays No of Hours for','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq915','nohours','2','','1','applicable Travel Benefits and it is editable','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq915','nohours','3','','1','when user selects the Modify button','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq915','amount','1','','1','This field displays the Limit for No of','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq915','amount','2','','1','Hours and it is editable when user selects','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq915','amount','3','','1','the Modify button.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq915','adnohours','1','','1','This field displays Additional No of Hours','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq915','adnohours','2','','1','for applicable Travel Benefits.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq915','adamount','1','','1','This field displays the Limit for Additional ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq915','adamount','2','','1','No of Hours and it is editable when user','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq915','adamount','3','','1','selects the Modify button.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),

('HP','3','E','F','Sq917','bencde','1','','1','This field displays the Benefit Code.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq917','childlimit','1','','1','This field displays the Child Limit ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq917','childlimit','2','','1','and it is editable when','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq917','childlimit','3','','1','user selects the Modify button.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq917','adultlimit','1','','1','This field displays the Adult Limit ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq917','adultlimit','2','','1','and it is editable when','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq917','adultlimit','3','','1','user selects Modify button','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq917','ageupto','1','','1','This field displays age in Age Upto','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq917','ageupto','2','','1','field and it is editable when.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq917','ageupto','3','','1','when user selects Modify button.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq917','ageabove','1','','1','This field displays age in Age above','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq917','ageabove','2','','1','field and it is editable when.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq917','ageabove','3','','1','when user selects Modify button.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq917','famlimit','1','','1','This field displays the Family Limit and it','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','3','E','F','Sq917','famlimit','2','','1','is editable when user selects Modify button','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP)
GO
