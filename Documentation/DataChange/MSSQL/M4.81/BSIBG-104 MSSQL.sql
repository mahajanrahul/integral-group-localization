/* ERROR */

	INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RGDV'
           ,'Quot.not belongs to Pol'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');

	INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'Enn1'
           ,'Renewal not existed'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
		   
	

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'Sq942'
           ,''
           ,'1'
           ,''
           ,'1'
           ,'This screen is used to define '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'Sq942'
           ,''
           ,'2'
           ,''
           ,'1'
           ,'loss ratio in current year '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'Sq942'
           ,''
           ,'3'
           ,''
           ,'1'
           ,'and overall which can be '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'Sq942'
           ,''
           ,'4'
           ,''
           ,'1'
           ,'generated renewal notice.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'Sr9a5'
           ,'OPTAUTORNW'
           ,'1'
           ,''
           ,'1'
           ,'An indicator to denote whether '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'Sr9a5'
           ,'OPTAUTORNW'
           ,'2'
           ,''
           ,'1'
           ,'that policy can be renewal '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'Sr9a5'
           ,'OPTAUTORNW'
           ,'3'
           ,''
           ,'1'
           ,'without send renewal invitation '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'Sr9a5'
           ,'OPTAUTORNW'
           ,'4'
           ,''
           ,'1'
           ,'confirmation from policy owner.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'Sr9k4'
           ,'YEAR'
           ,'1'
           ,''
           ,'1'
           ,'Year that users want to filter.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'Sr9k4'
           ,'MONTH'
           ,'1'
           ,''
           ,'1'
           ,'Month that users want to filter.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'Sr9kn'
           ,'PRTOPT'
           ,'1'
           ,''
           ,'1'
           ,'A new checkbox for user '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'Sr9kn'
           ,'PRTOPT'
           ,'2'
           ,''
           ,'1'
           ,'to indicate if Quotation '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'Sr9kn'
           ,'PRTOPT'
           ,'3'
           ,''
           ,'1'
           ,'Policy Header, S9103 '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'Sr9kn'
           ,'PRTOPT'
           ,'4'
           ,''
           ,'1'
           ,'Text is to be extracted in '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'Sr9kn'
           ,'PRTOPT'
           ,'5'
           ,''
           ,'1'
           ,'as a report which is generated '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'Sr9kn'
           ,'PRTOPT'
           ,'6'
           ,''
           ,'1'
           ,'from G1RNWRVW3.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'Sq942'
           ,'CURYEAR'
           ,'1'
           ,''
           ,'1'
           ,'Loss ratio in current year that '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'Sq942'
           ,'CURYEAR'
           ,'2'
           ,''
           ,'1'
           ,'can be sent renewal notice.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'Sq942'
           ,'OVERALL'
           ,'1'
           ,''
           ,'1'
           ,'Loss ratio in overall that can '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'Sq942'
           ,'OVERALL'
           ,'2'
           ,''
           ,'1'
           ,'be sent renewal notice.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO

ALTER TABLE [VM1DTA].[GCHPPF] 
ADD OPTAUTORNW NCHAR(1) ;  
GO

ALTER VIEW [VM1DTA].[GCHP](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, EXBRKNM, EXUNDNM, BRKSRVAC, REFNO, MBRDATA, ADMNRULE, DEFPLANDI, DEFCLMPYE, EMPGRP, INWINCTYP, AREACOD, INDUSTRY, MAJORMET, BULKIND, FFEEWHOM, PRODMIX, FEELVL, CTBEFFDT, EXBFML, EXBLDAYS, CTBFML, CTBNDAYS, EFAIS, EFADP, NOREM, FSTRMFML, FSTRMDAY, SNDRMFML, SNDRMDAY, TRDRMFML, TRDRMDAY, MBRIDFLD, EXBDUEDT, CTBDUEDT, LSTEXBFR, LSTEXBTO, LSTCTBFR, LSTCTBTO, LSTEBPDT, FSTRMPDT, SNDRMPDT, TRDRMPDT, POLANV, CTBRULE, ACBLRULE, FMCRULE, SWTRANNO, FEEWHO, ZSRCEBUS, CALCMTHD, AGEBASIS, FCLLVL, PRMPYOPT, PRMBRLVL, TOLRULE, CERTINFM, FMC2RULE, LMBRPFX, LOYBNFLG, SWCFLG, AUTORNW, GAPLPFX, NMLVAR, EXTFMLY, PINFDTE, CASHLESS, LOCATION, SUBLOCN, OPTAUTORNW, TTDATE, JOBNM, USRPRF, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            EXBRKNM,
            EXUNDNM,
            BRKSRVAC,
            REFNO,
            MBRDATA,
            ADMNRULE,
            DEFPLANDI,
            DEFCLMPYE,
            EMPGRP,
            INWINCTYP,
            AREACOD,
            INDUSTRY,
            MAJORMET,
            BULKIND,
            FFEEWHOM,
            PRODMIX,
            FEELVL,
            CTBEFFDT,
            EXBFML,
            EXBLDAYS,
            CTBFML,
            CTBNDAYS,
            EFAIS,
            EFADP,
            NOREM,
            FSTRMFML,
            FSTRMDAY,
            SNDRMFML,
            SNDRMDAY,
            TRDRMFML,
            TRDRMDAY,
            MBRIDFLD,
            EXBDUEDT,
            CTBDUEDT,
            LSTEXBFR,
            LSTEXBTO,
            LSTCTBFR,
            LSTCTBTO,
            LSTEBPDT,
            FSTRMPDT,
            SNDRMPDT,
            TRDRMPDT,
            POLANV,
            CTBRULE,
            ACBLRULE,
            FMCRULE,
            SWTRANNO,
            FEEWHO,
            ZSRCEBUS,
            CALCMTHD,
            AGEBASIS,
            FCLLVL,
            PRMPYOPT,
            PRMBRLVL,
            TOLRULE,
            CERTINFM,
            FMC2RULE,
            LMBRPFX,
            LOYBNFLG,
            SWCFLG,
            AUTORNW,
            GAPLPFX,
            NMLVAR,
            EXTFMLY,
            PINFDTE,
            CASHLESS,
            LOCATION,
            SUBLOCN,
			OPTAUTORNW,
            TTDATE,
            JOBNM,
            USRPRF,
            DATIME

       FROM VM1DTA.GCHPPF

GO 
ALTER TABLE [VM1DTA].[RNRWPF]
ADD OPTAUTORNW NCHAR(1) ;  
GO

CREATE TABLE [VM1DTA].[RNNTPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[CNTBRANCH] [nchar](2) NULL
	)