/****** Object:  View [VM1DTA].[GCHD]    Script Date: 7/21/2016 5:08:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [VM1DTA].[GCHD](UNIQUE_NUMBER,CHDRCOY,CHDRNUM,CHDRPFX,CNTTYPE,COWNPFX,COWNCOY,COWNNUM,STATCODE,PNDATE,SUBSFLG,OCCDATE,HRSKIND,CNTCURR,BILLCURR,TAKOVRFLG,GPRNLTYP,GPRMNTHS,RNLNOTTO,SRCEBUS,COYSRVAC,MRKSRVAC,DESPPFX,DESPCOY,DESPNUM,POLSCHPFLG,BTDATE,ADJDATE,PTDATE,PTDATEAB,TRANLUSED,LMBRNO,LHEADNO,EFFDCLDT,SERVUNIT,PNTRCDE,PROCID,TRANID,TRANNO,VALIDFLAG,SPECIND,TAXFLAG,AGEDEF,TERMAGE,PERSONCOV,ENROLLTYP,SPLITSUBS,AVLISU,MPLPFX,MPLCOY,MPLNUM,USRPRF,JOBNM,DATIME,IGRASP,IEXPLAIN,IDATE,MIDJOIN,CVISAIND,COVERNT,CNTISS, REPNUM,
REPTYPE, PAYRCOY, PAYRNUM, PAYRPFX) AS
SELECT UNIQUE_NUMBER,
			CHDRCOY,
			CHDRNUM,
			CHDRPFX,
			CNTTYPE,
			COWNPFX,
			COWNCOY,
			COWNNUM,
			STATCODE,
			PNDATE,
			SUBSFLG,
			OCCDATE,
			HRSKIND,
			CNTCURR,
			BILLCURR,
			TAKOVRFLG,
			GPRNLTYP,
			GPRMNTHS,
			RNLNOTTO,
			SRCEBUS,
			COYSRVAC,
			MRKSRVAC,
			DESPPFX,
			DESPCOY,
			DESPNUM,
			POLSCHPFLG,
			BTDATE,
			ADJDATE,
			PTDATE,
			PTDATEAB,
			TRANLUSED,
			LMBRNO,
			LHEADNO,
			EFFDCLDT,
			SERVUNIT,
			PNTRCDE,
			PROCID,
			TRANID,
			TRANNO,
			VALIDFLAG,
			SPECIND,
			TAXFLAG,
			AGEDEF,
			TERMAGE,
			PERSONCOV,
			ENROLLTYP,
			SPLITSUBS,
			AVLISU,
			MPLPFX,
			MPLCOY,
			MPLNUM,
			USRPRF,
			JOBNM,
			DATIME,
			IGRASP,
			IEXPLAIN,
			IDATE,
			MIDJOIN,
			CVISAIND,
			COVERNT,
			CNTISS,
			REPNUM,
			REPTYPE,
			PAYRCOY,
			PAYRNUM,
			PAYRPFX
       FROM VM1DTA.CHDRPF
      WHERE SERVUNIT = 'GP'

GO


