-- INSERT HELP TEXT
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
			/* SR9GO Discount*/
           ('HP' 
           ,''
           ,'E'
           ,'F'
           ,'SR9GO'
           ,'DISCOUNT'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to store discount (in figures)'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP' 
           ,''
           ,'E'
           ,'F'
           ,'SR9GO'
           ,'DISCOUNT'
           ,'2'
           ,''
           ,'1'
           ,' given on R&B.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR9BT LOG Number*/
		   ('HP' 
           ,''
           ,'E'
           ,'F'
           ,'SR9BT'
           ,'LOGNUM'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to capture LOG Number'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP' 
           ,''
           ,'E'
           ,'F'
           ,'SR9BT'
           ,'LOGNUM'
           ,'2'
           ,''
           ,'1'
           ,' during claim registration.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR9BX 'Co-payment applies?'*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR9BX'
           ,'CPYAPLYFLG'
           ,'1'
           ,''
           ,'1'
           ,'This checkbox is used to identify if the particular claim'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR9BX'
           ,'CPYAPLYFLG'
           ,'2'
           ,''
           ,'1'
           ,' has the upgraded R&B co-payment rule applicable'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR9BX'
           ,'CPYAPLYFLG'
           ,'3'
           ,''
           ,'1'
           ,' for other benefits except R&B.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IC Screen*/
		   ('HP'
           ,''
           ,'E'
           ,'S'		   
           ,'SR4IC'
           ,''
           ,'1'
           ,''
           ,'1'
           ,'This screen will be used for creating or enquiring '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'S'		   
           ,'SR4IC'
           ,''
           ,'2'
           ,''
           ,'1'
           ,'Letter of Guarantee (LOG).'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IC Policy Number*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'CHDRNUM'
           ,'1'
           ,''
           ,'1'
           ,'This field must contain a valid Policy Number.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'CHDRNUM'
           ,'2'
           ,''
           ,'1'
           ,' If the Policy Number is not known, a Policy Owner window'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'CHDRNUM'
           ,'3'
           ,''
           ,'1'
           ,' may be invoked from which the relevant policy can be'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'CHDRNUM'
           ,'4'
           ,''
           ,'1'
           ,' selected. It is mandatory for the action – ‘Create LOG’ and'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'CHDRNUM'
           ,'5'
           ,''
           ,'1'
           ,' ‘Enquiry by Policy Number’ but not required'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'CHDRNUM'
           ,'6'
           ,''
           ,'1'
           ,' for the action – ‘Enquiry by LOG Number’'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IC Hospital Type*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'HOSPTYP'
           ,'1'
           ,''
           ,'1'
           ,'It can have values either ‘G’ for Govt.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'HOSPTYP'
           ,'2'
           ,''
           ,'1'
           ,' Hospitals or ‘P’ for Pvt Hospitals.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'HOSPTYP'
           ,'3'
           ,''
           ,'1'
           ,' This field is mandatory for the action – ‘Create LOG’'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IC Number of LOGs*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'NOFLOGS'
           ,'1'
           ,''
           ,'1'
           ,'It is used to capture the number of LOGs to be generated.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'NOFLOGS'
           ,'2'
           ,''
           ,'1'
           ,' It is mandatory for the action ‘Create LOG”.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IC LOG Number*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'LOGNUM'
           ,'1'
           ,''
           ,'1'
           ,'It is used to capture LOG Number for enquiry purposes.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'LOGNUM'
           ,'2'
           ,''
           ,'1'
           ,' It is mandatory for the action ‘Enquiry by LOG Number’.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IC Valid To*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'VDTETO'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to capture valid to date.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'VDTETO'
           ,'2'
           ,''
           ,'1'
           ,' It is mandatory for the action ‘Create LOG’.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4ID Screen*/
		   ('HP'
           ,''
           ,'E'
           ,'S'		   
           ,'SR4ID'
           ,''
           ,'1'
           ,''
           ,'1'
           ,'This screen will be used for the confirmation about'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'S'		   
           ,'SR4ID'
           ,''
           ,'2'
           ,''
           ,'1'
           ,'Issuance of the screen.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4ID Policy Number*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4ID'
           ,'CHDRNUM'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to show policy number'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4ID'
           ,'CHDRNUM'
           ,'2'
           ,''
           ,'1'
           ,' as entered on the LOG Maintenance screen, ‘SR4ID’.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4ID Policy Owner*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4ID'
           ,'OWNERNAME'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display the owner'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4ID'
           ,'OWNERNAME'
           ,'2'
           ,''
           ,'1'
           ,' of the policy.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4ID LOG Number*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4ID'
           ,'LOGNUM'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display LOG Numbers'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4ID'
           ,'LOGNUM'
           ,'2'
           ,''
           ,'1'
           ,' to be issued.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4ID Valid From date*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4ID'
           ,'VDTEFRM'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display valid from'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4ID'
           ,'VDTEFRM'
           ,'2'
           ,''
           ,'1'
           ,' date for the particular LOG.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4ID Valid To date*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4ID'
           ,'VDTETO'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display valid to'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4ID'
           ,'VDTETO'
           ,'2'
           ,''
           ,'1'
           ,' date for the particular LOG.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4ID Print confirmation field*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4ID'
           ,'MGPTXTE02'
           ,'1'
           ,''
           ,'1'
           ,'This field is editable and is used to capture response from'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4ID'
           ,'MGPTXTE02'
           ,'2'
           ,''
           ,'1'
           ,' user whether the user want to print the LOGs or not.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4ID'
           ,'MGPTXTE02'
           ,'3'
           ,''
           ,'1'
           ,' This field has values either ‘Y’ or ‘N’.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IE Screen*/
		   ('HP'
           ,''
           ,'E'
           ,'S'		   
           ,'SR4IE'
           ,''
           ,'1'
           ,''
           ,'1'
           ,'This screen will be used to enquire about'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'S'		   
           ,'SR4IE'
           ,''
           ,'2'
           ,''
           ,'1'
           ,'Letter of Guarantee (LOG).'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IE Policy Number*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'CHDRNUM'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and have same value as entered on'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'CHDRNUM'
           ,'2'
           ,''
           ,'1'
           ,' the screen ‘SR4IE’. It is mandatory for action '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'CHDRNUM'
           ,'3'
           ,''
           ,'1'
           ,'‘Enquiry by Policy Number’ whereas it is not required for'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'CHDRNUM'
           ,'4'
           ,''
           ,'1'
           ,' action ‘Enquiry by LOG Number’.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IE LOG Number (in Table)*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'LOGNUM'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and have same value as entered on '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'LOGNUM'
           ,'2'
           ,''
           ,'1'
           ,'the screen ‘SR4IE’. It is mandatory for action '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'LOGNUM'
           ,'3'
           ,''
           ,'1'
           ,'‘Enquiry by LOG Number’ whereas it is not required for'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'LOGNUM'
           ,'4'
           ,''
           ,'1'
           ,' action ‘Enquiry by Policy Number’.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IE Valid From*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'DTEFRM'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and contains the valid'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'DTEFRM'
           ,'2'
           ,''
           ,'1'
           ,' from for the particular LOG.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IE Valid To*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'VDTETO'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and contains the valid'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'VDTETO'
           ,'2'
           ,''
           ,'1'
           ,' to date for the particular LOG.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IE Hospital Type*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'HOSPTYP'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and contains the hospital type for'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'HOSPTYP'
           ,'2'
           ,''
           ,'1'
           ,' the particular LOG.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IE Claim Number*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'CLAMNUM'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and contains the Claim No. against'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'CLAMNUM'
           ,'2'
           ,''
           ,'1'
           ,' that particular LOG. This field contains value only if the'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'CLAMNUM'
           ,'3'
           ,''
           ,'1'
           ,' status is ‘Used’ else it is blank.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IF Screen*/
		   ('HP'
           ,''
           ,'E'
           ,'S'		   
           ,'SR4IF'
           ,''
           ,'1'
           ,''
           ,'1'
           ,'This screen will be used to list out the related'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'S'		   
           ,'SR4IF'
           ,''
           ,'2'
           ,''
           ,'1'
           ,'service codes that are mapped with the benefit codes'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'S'		   
           ,'SR4IF'
           ,''
           ,'3'
           ,''
           ,'1'
           ,'of the product selected on the claim registration screen.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IF Locate*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IF'
           ,'SERVCODE'
           ,'1'
           ,''
           ,'1'
           ,'This field is editable and is used to enter service code'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IF'
           ,'SERVCODE'
           ,'2'
           ,''
           ,'1'
           ,' initials to search the service code.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IF Description (in header)*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IF'
           ,'PDESCA'
           ,'1'
           ,''
           ,'1'
           ,'This field is editable and is used to enter description'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IF'
           ,'PDESCA'
           ,'2'
           ,''
           ,'1'
           ,' initials to search the service code.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IF Service Code*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IF'
           ,'SRVCCODE'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IF'
           ,'SRVCCODE'
           ,'2'
           ,''
           ,'1'
           ,' Service Codes.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IF Description*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IF'
           ,'PDESC'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display description'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IF'
           ,'PDESC'
           ,'2'
           ,''
           ,'1'
           ,' of the service codes.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IF Incurred*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IF'
           ,'INCURRED'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to enter incurred amount corresponding to'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IF'
           ,'INCURRED'
           ,'2'
           ,''
           ,'1'
           ,' to the particular service code.This field is editable and'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IF'
           ,'INCURRED'
           ,'3'
           ,''
           ,'1'
           ,' contains numeric characters.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IF Discount*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IF'
           ,'DISCOUNT'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to store discount (in figures) given on'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IF'
           ,'DISCOUNT'
           ,'2'
           ,''
           ,'1'
           ,' R&B.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IG Screen*/
		   ('HP'
           ,''
           ,'E'
           ,'S'		   
           ,'SR4IG'
           ,''
           ,'1'
           ,''
           ,'1'
           ,'This screen will be used to print Claim Payment Advice.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IG Claim Number*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'CLAMNUM'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display Claim Number'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'CLAMNUM'
           ,'2'
           ,''
           ,'1'
           ,' of the claim and the occurrence of the claim.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IG Date Visit*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'DTEVISIT'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'DTEVISIT'
           ,'2'
           ,''
           ,'1'
           ,' Date of Visit.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IG Status*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'CLAIMSTZ'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display the status'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'CLAIMSTZ'
           ,'2'
           ,''
           ,'1'
           ,' of the claim. All valid status are defined in T9690.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IG Member*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'MBRNO'
           ,'1'
           ,''
           ,'1'
           ,'Member is used to represent the member number, dependent'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'MBRNO'
           ,'2'
           ,''
           ,'1'
           ,' number and member name of the particular member for which'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'MBRNO'
           ,'3'
           ,''
           ,'1'
           ,' claim is filed in that policy. This field is protected.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IG Policy*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'CHDRNUM'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display the policy'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'CHDRNUM'
           ,'2'
           ,''
           ,'1'
           ,' number and corresponding policy owner.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IG Provider Orgn*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'PRVORGN'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display the provider'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'PRVORGN'
           ,'2'
           ,''
           ,'1'
           ,' organization number and its name.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IG Product code*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'PRODTYP'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected. This is the Product code. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'PRODTYP'
           ,'2'
           ,''
           ,'1'
           ,'All valid product codes have to be defined in table T9797.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SR4IG Plan Number*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'PLANNO'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display plan number.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SQ902 Screen*/
		   ('HP'
           ,''
           ,'E'
           ,'S'		   
           ,'SQ902'
           ,''
           ,'1'
           ,''
           ,'1'
           ,'This screen will be used to display all the Surgery Codes'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'S'		   
           ,'SQ902'
           ,''
           ,'2'
           ,''
           ,'1'
           ,'attached to the claim.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SQ902 Select*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SQ902'
           ,'SELECT'
           ,'1'
           ,''
           ,'1'
           ,'It is used to select the multiple Surgery Codes to delete.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SQ902 Surgery Code*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SQ902'
           ,'SURGCATG'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SQ902'
           ,'SURGCATG'
           ,'2'
           ,''
           ,'1'
           ,' Surgery Codes.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SQ902 Surgery Description*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SQ902'
           ,'LONGDESC'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display description'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SQ902'
           ,'LONGDESC'
           ,'2'
           ,''
           ,'1'
           ,' of the surgery codes.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SQ903 Screen*/
		   ('HP'
           ,''
           ,'E'
           ,'S'		   
           ,'SQ903'
           ,''
           ,'1'
           ,''
           ,'1'
           ,'This screen will be used to list out all the surgery codes'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'S'		   
           ,'SQ903'
           ,''
           ,'2'
           ,''
           ,'1'
           ,'mentioned in the table T9698.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SQ903 Locate*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SQ903'
           ,'SURGCATG'
           ,'1'
           ,''
           ,'1'
           ,'This field is editable and is used to enter surgery code'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SQ903'
           ,'SURGCATG'
           ,'2'
           ,''
           ,'1'
           ,' initials to search the surgery code.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SQ903 Description (in header)*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SQ903'
           ,'PDESCA'
           ,'1'
           ,''
           ,'1'
           ,'This field is editable and is used to enter description'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SQ903'
           ,'PDESCA'
           ,'2'
           ,''
           ,'1'
           ,' initials to search the surgery code.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SQ903 Select*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SQ903'
           ,'SELECT'
           ,'1'
           ,''
           ,'1'
           ,'It is used to select the multiple Surgery Codes to add.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SQ903 Surgery Code*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SQ903'
           ,'SURGCODE'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SQ903'
           ,'SURGCODE'
           ,'2'
           ,''
           ,'1'
           ,'Surgery Codes.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   /*SQ903 Surgery Description*/
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SQ903'
           ,'LONGDESC'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display description '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP),
		   ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SQ903'
           ,'LONGDESC'
           ,'2'
           ,''
           ,'1'
           ,'of the surgery codes.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
GO
--Error Codes--
INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'GP0059'
           ,'Print LOGs?'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
GO
INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'GP0060'
           ,'Post Dt rule exceeded Continue? (Y/N)'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
GO
INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFPW'
           ,'LOG is in used'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
GO
INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFPD'
           ,'Invalid Hospital Type'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
GO
INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFPC'
           ,'Invalid LOG Number'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
GO
	INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'CTRE60'
           ,'LOGs created successful'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
GO

INSERT INTO VM1DTA.FLDDPF (FDID) VALUES ('HOSPTYP');
GO
INSERT INTO VM1DTA.FLDTPF (FDID, LANG, FDTX, COLH01) VALUES ('HOSPTYP', 'E', 'Hospital Type', 'Hospital Type');
GO

-- CREATE & ALTER TABLE
ALTER TABLE VM1DTA.GCLDPF
ADD DISCOUNT NUMERIC; -- Hospital Discount
GO
ALTER TABLE VM1DTA.GCLHPF
ADD CLRRSVFLAG NCHAR(1), -- Clear Reserve Flag
	CPYAPLYFLG NCHAR(1), -- Co-payment applied flag
	CPYAPLYIND NCHAR(1), -- Co-payment applied indicator
	CLMPROCID NCHAR(10), -- Claim Process User ID
	CLMAPRVID NCHAR(10); -- Claim Approval User ID
GO
ALTER TABLE VM1DTA.GCMHPF
ADD CLMPROCID NCHAR(10), -- Claim Process User ID
	CLMAPRVID NCHAR(10); -- Claim Approval User ID
GO
CREATE TABLE VM1DTA.LOGIPF 
	(
	UNIQUE_NUMBER BIGINT IDENTITY(1,1) PRIMARY KEY NOT NULL, -- Unique Number
	CHDRPFX NCHAR(2), -- Policy Header Prefix
	CHDRCOY NCHAR(1), -- Policy Header Company
	CHDRNUM NCHAR(8), -- Policy Number
	LOGNUM  NCHAR(15), -- LOG Number
	HOSPTYP NCHAR(1), -- Hospital Type
	CLAMNUM NCHAR(8), -- Claim Number
	GCOCCNO NCHAR(2), -- Claim Occurrence No
	VDTEFRM INT, -- Valid Date From
	VDTETO INT, -- Valid Date To
	VALIDFLAG NCHAR(1), -- Valid Flag
	USRPRF NCHAR(10), -- User Profile
	JOBNM NCHAR(10), -- Job Name
	DATIME DATETIME2
	); -- Timestamp
GO
CREATE TABLE VM1DTA.CPAXPF 
	(
	UNIQUE_NUMBER BIGINT IDENTITY(1,1) PRIMARY KEY NOT NULL, -- Unique Number
	CHDRPFX NCHAR(2), -- Policy Header Prefix
	CHDRCOY NCHAR(1), -- Policy Header Company
	CHDRNUM NCHAR(8), -- Policy Number
	CNTTYPE NCHAR(3), -- Policy Type
	AGNTPFX NCHAR(2), -- Agent Prefix
	AGNTCOY NCHAR(1), -- Agent Company
	AGNTNUM NCHAR(5), -- Agent Code
	CLMCOY NCHAR(1), -- Claim Company
	CLAMNUM NCHAR(8), -- Claim Number
	GCOCCNO NCHAR(2), -- Claim Occurrence No
	EMPNO NCHAR(12), -- Employee ID
	EMPNUM NCHAR(8), -- Employee Number
	CLNTPFX NCHAR(2), -- Client Prefix
	CLNTCOY NCHAR(1), -- Client Company
	CLNTNUM NCHAR(8), -- ID of Insured
	CLTSEX NCHAR(1), -- Gender of Insured
	CLTDOB INT, -- Date of Birth of Insured
	DPNTNO NCHAR(8), -- Dependent No
	DEPT NCHAR(10), -- Department 
	CCDATE INT, -- Insured From Date
	CRDATE INT, -- Insured To Date
	DTEATT INT, -- Member Start Date
	DATEFRM INT, -- Admission Date
	DATETO INT, -- Discharge Date
	PLANNO NCHAR(3), -- Plan Number
	PROVORG NCHAR(8), -- Provider Organization
	CC01 NCHAR(5), -- Agent Code
	CC02 NCHAR(3), -- Marketing Staff Code
	GCBENSEQ INT -- Amount Paid
	); 
GO
CREATE TABLE VM1DTA.GCSCPF
	(
	UNIQUE_NUMBER BIGINT IDENTITY(1,1) PRIMARY KEY NOT NULL, -- Unique Number
	CLMCOY NCHAR(1), 	-- Claim Company
	CLAMNUM NCHAR(8), 	-- Claim Number
	GCOCCNO NCHAR(2),	-- Claim Occurrence No
	GCBENSEQ INT,		-- Benefit Sequence No
	SURGCDE NCHAR(8),	-- Surgery Codes
	USRPRF NCHAR(10),	-- User Profile
	JOBNM NCHAR(10),	-- Job Name
	DATIME DATETIME2	-- Timestamp
	);
GO
-- CREATE & ALTER VIEW
-- ALTER VIEW GCLD

ALTER VIEW [VM1DTA].[GCLD]
	(UNIQUE_NUMBER, CLMCOY, CLAMNUM, GCOCCNO, GCBENSEQ, DATEFRM, DATETO, NOFDAY, SRVCCODE, NOFUNIT, POS, INCURRED, SYPYPROV, PAYPROV, GDEDUCT, COPAY, SYMBRSHR, MBRSHARE, SYHMOSHR, HMOSHARE, BENCDE, FEESCHID, WHRULE, WHSLAB, FEEBASIS, CLNTCOY, PROVIND, PROVCAP, NETLEVEL, FESCHMTH, NETDKEY, PFSDKEY, BENCDEFLG, HMOSHRMM, BNFTGRP, OUBNFGP, ZDAYCOV, ZCHRGDAY, ZHSLMTUP, MMINBFGP, MMOUBFGP, AMTDISAL, TPADIFF, INVOICENO, EXTRMTYP, GCDIAGCD, JOBNM, USRPRF, DATIME,
	DISCOUNT)
AS SELECT 
	UNIQUE_NUMBER,
	CLMCOY,
	CLAMNUM,
	GCOCCNO,
	GCBENSEQ,
	DATEFRM,
	DATETO,
	NOFDAY,
	SRVCCODE,
	NOFUNIT,
	POS,
	INCURRED,
	SYPYPROV,
	PAYPROV,
	GDEDUCT,
	COPAY,
	SYMBRSHR,
	MBRSHARE,
	SYHMOSHR,
	HMOSHARE,
	BENCDE,
	FEESCHID,
	WHRULE,
	WHSLAB,
	FEEBASIS,
	CLNTCOY,
	PROVIND,
	PROVCAP,
	NETLEVEL,
	FESCHMTH,
	NETDKEY,
	PFSDKEY,
	BENCDEFLG,
	HMOSHRMM,
	BNFTGRP,
	OUBNFGP,
	ZDAYCOV,
	ZCHRGDAY,
	ZHSLMTUP,
	MMINBFGP,
	MMOUBFGP,
	AMTDISAL,
	TPADIFF,
	INVOICENO,
	EXTRMTYP,
	GCDIAGCD,
	JOBNM,
	USRPRF,
	DATIME,
	DISCOUNT
FROM VM1DTA.GCLDPF;
GO
-- ALTER VIEW GCLDDUP

ALTER VIEW [VM1DTA].[GCLDDUP]
	(UNIQUE_NUMBER
	,CLMCOY
	,CLAMNUM
	,GCOCCNO
	,GCBENSEQ
	,DATEFRM
	,DATETO
	,NOFDAY
	,SRVCCODE
	,NOFUNIT
	,CLNTCOY
	,PROVIND
	,POS
	,INCURRED
	,SYPYPROV
	,PAYPROV
	,GDEDUCT
	,COPAY
	,SYMBRSHR
	,MBRSHARE
	,SYHMOSHR
	,HMOSHARE
	,BENCDE
	,FEESCHID
	,WHRULE
	,WHSLAB
	,FEEBASIS
	,PROVCAP
	,NETLEVEL
	,FESCHMTH
	,NETDKEY
	,PFSDKEY
	,BENCDEFLG
	,HMOSHRMM
	,BNFTGRP
	,OUBNFGP
	,ZDAYCOV
	,ZCHRGDAY
	,ZHSLMTUP
	,MMINBFGP
	,MMOUBFGP
	,JOBNM
	,USRPRF
	,DATIME
	,AMTDISAL
	,TPADIFF
	,INVOICENO
	,EXTRMTYP
	,GCDIAGCD
	,DISCOUNT
	)
AS SELECT UNIQUE_NUMBER
	,CLMCOY
	,CLAMNUM
	,GCOCCNO
	,GCBENSEQ
	,DATEFRM
	,DATETO
	,NOFDAY
	,SRVCCODE
	,NOFUNIT
	,CLNTCOY
	,PROVIND
	,POS
	,INCURRED
	,SYPYPROV
	,PAYPROV
	,GDEDUCT
	,COPAY
	,SYMBRSHR
	,MBRSHARE
	,SYHMOSHR
	,HMOSHARE
	,BENCDE
	,FEESCHID
	,WHRULE
	,WHSLAB
	,FEEBASIS
	,PROVCAP
	,NETLEVEL
	,FESCHMTH
	,NETDKEY
	,PFSDKEY
	,BENCDEFLG
	,HMOSHRMM
	,BNFTGRP
	,OUBNFGP
	,ZDAYCOV
	,ZCHRGDAY
	,ZHSLMTUP
	,MMINBFGP
	,MMOUBFGP
	,JOBNM
	,USRPRF
	,DATIME
	,AMTDISAL
	,TPADIFF
	,INVOICENO
	,EXTRMTYP
	,GCDIAGCD
	,DISCOUNT
FROM VM1DTA.GCLDPF;
GO
-- ALTER VIEW GCLH

ALTER VIEW [VM1DTA].[GCLH] (
	UNIQUE_NUMBER
	,CLMCOY
	,CLAMNUM
	,GCOCCNO
	,CHDRNUM
	,MBRNO
	,DPNTNO
	,CLNTCOY
	,CLNTNUM
	,GCSTS
	,CLAIMCUR
	,CRATE
	,PRODTYP
	,GRSKCLS
	,DTEVISIT
	,DTEDCHRG
	,GCDIAGCD
	,PLANNO
	,PREAUTNO
	,PROVORG
	,AREACDE
	,REFERRER
	,CLAMTYPE
	,TIMEHH
	,TIMEMM
	,ZCLTCLMREF
	,GCDTHCLM
	,APAIDAMT
	,REQNTYPE
	,CRDTCARD
	,WHOPAID
	,DTEKNOWN
	,GCFRPDTE
	,ZCLMRECD
	,MCFROM
	,MCTO
	,GDEDUCT
	,COPAY
	,MBRTYPE
	,PROVNET
	,AAD
	,THIRDRCVY
	,THIRDPARTY
	,TLMBRSHR
	,TLHMOSHR
	,DATEAUTH
	,GCAUTHBY
	,GCOPRSCD
	,REVLINK
	,REVIND
	,TPRCVPND
	,PENDFROM
	,MMPROD
	,HMOSHRMM
	,TAKEUP
	,DATACONV
	,CLRATE
	,REFNO
	,UPDIND
	,PREMRCVY
	,DATEAUTHF
	,DATEAUTHS
	,GCAUTHBYF
	,GCAUTHBYS
	,CASHLESS
	,TPAREFNO
	,INWARDNO
	,ICD101L
	,ICD102L
	,ICD103L
	,REGCLM
	,GCCAUSCD
	,JOBNM
	,USRPRF
	,DATIME
	,TLCFAMT
	,CLMPREVLMT
	,CLRRSVFLAG
	,CPYAPLYFLG
	,CLMPROCID
	,CLMAPRVID
	,CPYAPLYIND
	)
AS
SELECT UNIQUE_NUMBER
	,CLMCOY
	,CLAMNUM
	,GCOCCNO
	,CHDRNUM
	,MBRNO
	,DPNTNO
	,CLNTCOY
	,CLNTNUM
	,GCSTS
	,CLAIMCUR
	,CRATE
	,PRODTYP
	,GRSKCLS
	,DTEVISIT
	,DTEDCHRG
	,GCDIAGCD
	,PLANNO
	,PREAUTNO
	,PROVORG
	,AREACDE
	,REFERRER
	,CLAMTYPE
	,TIMEHH
	,TIMEMM
	,ZCLTCLMREF
	,GCDTHCLM
	,APAIDAMT
	,REQNTYPE
	,CRDTCARD
	,WHOPAID
	,DTEKNOWN
	,GCFRPDTE
	,ZCLMRECD
	,MCFROM
	,MCTO
	,GDEDUCT
	,COPAY
	,MBRTYPE
	,PROVNET
	,AAD
	,THIRDRCVY
	,THIRDPARTY
	,TLMBRSHR
	,TLHMOSHR
	,DATEAUTH
	,GCAUTHBY
	,GCOPRSCD
	,REVLINK
	,REVIND
	,TPRCVPND
	,PENDFROM
	,MMPROD
	,HMOSHRMM
	,TAKEUP
	,DATACONV
	,CLRATE
	,REFNO
	,UPDIND
	,PREMRCVY
	,DATEAUTHF
	,DATEAUTHS
	,GCAUTHBYF
	,GCAUTHBYS
	,CASHLESS
	,TPAREFNO
	,INWARDNO
	,ICD101L
	,ICD102L
	,ICD103L
	,REGCLM
	,GCCAUSCD
	,JOBNM
	,USRPRF
	,DATIME
	,TLCFAMT
	,CLMPREVLMT
	,CLRRSVFLAG
	,CPYAPLYFLG
	,CLMPROCID
	,CLMAPRVID
	,CPYAPLYIND
FROM VM1DTA.GCLHPF;
GO
-- ALTER VIEW LOGI

CREATE VIEW [VM1DTA].[LOGI]
	(
	UNIQUE_NUMBER, -- Unique Number
	CHDRPFX, -- Policy Header Prefix
	CHDRCOY, -- Policy Header Company
	CHDRNUM, -- Policy Number 
	LOGNUM,  -- LOG Number
	HOSPTYP, -- Hospital Type 
	CLAMNUM, -- Claim Number
	VALIDFLAG, -- Valid flag
	GCOCCNO, -- Claim Occurrence No
	VDTEFRM, -- Valid date from 
	VDTETO, -- Valid date to
	USRPRF, -- User Profile
	JOBNM, -- Job name
	DATIME -- Timestamp
	)
AS SELECT 
	UNIQUE_NUMBER, -- Unique Number
	CHDRPFX, -- Policy Header Prefix
	CHDRCOY, -- Policy Header Company 
	CHDRNUM, -- Policy Number 
	LOGNUM,  -- LOG Number 
	HOSPTYP, -- Hospital Type
	CLAMNUM, -- Claim Number
	VALIDFLAG, -- Valid flag
	GCOCCNO, -- Claim Occurrence No 
	VDTEFRM, -- Valid date from
	VDTETO,  -- Valid date to
	USRPRF, -- User Profile
	JOBNM, -- Job name
	DATIME -- Timestamp
FROM LOGIPF;
GO

CREATE VIEW [VM1DTA].[CPAX]
	(
	UNIQUE_NUMBER, -- Unique Number
	CHDRPFX,        -- Policy Header Prefix
	CHDRCOY,        -- Policy Header Company
	CHDRNUM,        -- Policy Number
	CNTTYPE,        -- Policy Type
	AGNTPFX,        -- Agent Prefix
	AGNTCOY,        -- Agent Company
	AGNTNUM,        -- Agent Code
	CLMCOY,         -- Claim Company
	CLAMNUM,        -- Claim Number
	GCOCCNO,        -- Claim Occurrence No
	EMPNO,          -- Employee ID
	EMPNUM,         -- Employee Number
	CLNTPFX,        -- Client Prefix
	CLNTCOY,        -- Client Company
	CLNTNUM,        -- ID of Insured
	CLTSEX,         -- Gender of Insured
	CLTDOB,         -- Date of Birth of Insured
	DPNTNO,         -- Dependent No
	DEPT,           -- Department
	CCDATE,         -- Insured From Date
	CRDATE,         -- Insured To Date
	DTEATT,         -- Insured To Date
	DATEFRM,        -- Admission Date
	DATETO,         -- Discharge Date
	PLANNO,         -- Discharge Date
	PROVORG,        -- Provider Organization
	CC01,           -- Agent Code
	CC02,           -- Marketing Staff Code
	GCBENSEQ
	)
AS SELECT 
	UNIQUE_NUMBER, -- Unique Number
	CHDRPFX,        -- Policy Header Prefix
	CHDRCOY,        -- Policy Header Company
	CHDRNUM,        -- Policy Number
	CNTTYPE,        -- Policy Type
	AGNTPFX,        -- Agent Prefix
	AGNTCOY,        -- Agent Company
	AGNTNUM,        -- Agent Code
	CLMCOY,         -- Claim Company
	CLAMNUM,        -- Claim Number
	GCOCCNO,        -- Claim Occurrence No
	EMPNO,          -- Employee ID
	EMPNUM,         -- Employee Number
	CLNTPFX,        -- Client Prefix
	CLNTCOY,        -- Client Company
	CLNTNUM,        -- ID of Insured
	CLTSEX,         -- Gender of Insured
	CLTDOB,         -- Date of Birth of Insured
	DPNTNO,         -- Dependent No
	DEPT,           -- Department
	CCDATE,         -- Insured From Date
	CRDATE,         -- Insured To Date
	DTEATT,         -- Insured To Date
	DATEFRM,        -- Admission Date
	DATETO,         -- Discharge Date
	PLANNO,         -- Discharge Date
	PROVORG,        -- Provider Organization
	CC01,           -- Agent Code
	CC02,           	-- Marketing Staff Code
	GCBENSEQ
FROM CPAXPF;
GO
CREATE VIEW [VM1DTA].[GCSC]
	(
	UNIQUE_NUMBER, 	-- Unique Number
	CLMCOY,			-- Claim Company
	CLAMNUM,		-- Claim Number
	GCOCCNO,		-- Claim Occurrence No
	GCBENSEQ,		-- Benefit Sequence No
	SURGCDE,		-- Surgery Codes
	USRPRF,			-- User Profile
	JOBNM,			-- Job Name
	DATIME			-- Timestamp
	)
AS SELECT
	UNIQUE_NUMBER, 	-- Unique Number
	CLMCOY,			-- Claim Company
	CLAMNUM,		-- Claim Number
	GCOCCNO,		-- Claim Occurrence No
	GCBENSEQ,		-- Benefit Sequence No
	SURGCDE,		-- Surgery Codes
	USRPRF,			-- User Profile
	JOBNM,			-- Job Name
	DATIME			-- Timestamp
FROM GCSCPF;

GO

