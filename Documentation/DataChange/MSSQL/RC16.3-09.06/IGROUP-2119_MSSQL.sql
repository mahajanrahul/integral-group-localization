﻿update descpf set SHORTDESC='Grasp', longdesc='Grasp' where DESCTABL='TQ9A9' and DESCITEM = '1' and DESCCOY = '3' and LANGUAGE='E';
update descpf set SHORTDESC=N'把握', longdesc=N'把握' where DESCTABL='TQ9A9' and DESCITEM = '1' and DESCCOY = '3' and LANGUAGE='J';
update descpf set SHORTDESC='Not', longdesc='Not supported' where DESCTABL='TQ9A9' and DESCITEM = '9' and DESCCOY = '3' and LANGUAGE='E';
update descpf set SHORTDESC=N'未対応', longdesc=N'未対応' where DESCTABL='TQ9A9' and DESCITEM = '9' and DESCCOY = '3' and LANGUAGE='J';
update descpf set SHORTDESC='Implement', longdesc='Implementation' where DESCTABL='TQ9AA' and DESCITEM = '1' and DESCCOY = '3' and LANGUAGE='E' ;
update descpf set SHORTDESC=N'実施', longdesc=N'実施' where DESCTABL='TQ9AA' and DESCITEM = '1' and DESCCOY = '3' and LANGUAGE='J';
update descpf set SHORTDESC='Not', longdesc='Not supported' where DESCTABL='TQ9AA' and DESCITEM = '9' and LANGUAGE='E';
update descpf set SHORTDESC=N'未実施', longdesc=N'未実施' where DESCTABL='TQ9AA' and DESCITEM = '9' and DESCCOY = '3' and LANGUAGE='J';
update erorpf set ERORDESC = N'1:意向の把握' where eroreror= 'RFS4  ' and erorlang='J';
update erorpf set ERORDESC = '1:Grasp of Intention' where eroreror= 'RFS4  ' and erorlang='E';
update erorpf set ERORDESC = N'1:意向に沿った提案・説明' where eroreror= 'RFS5  ' and erorlang='J';
update erorpf set ERORDESC = '1:Proposed ・ Explained along the Intention' where eroreror= 'RFS5  ' and erorlang='E';
update erorpf set ERORDESC = 'Intention Confirmation Date' where eroreror = 'RFS6  ' and erorlang='E';
update erorpf set ERORDESC = N'意向確認日' where eroreror = 'RFS6  ' and erorlang='J';

