

/****** Object:  View [VM1DTA].[GIPM]    Script Date: 8/22/2016 3:04:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER TABLE [VM1DTA].[GIPMPF]
ADD [BPREM] [numeric](15, 2) NULL,
	[DISCRATE] [numeric](5, 2) NULL,
	[CMRATE] [numeric](5, 2) NULL,
	[TOTPREM] [numeric](15, 2) NULL,
	[DISCAMT] [numeric](11, 2) NULL,
	[BPREMAF] [numeric](15, 2) NULL,
	[DISCRATEAF] [numeric](5, 2) NULL,
	[CMRATEAF] [numeric](5, 2) NULL,
	[TOTPREMAF] [numeric](15, 2) NULL,
	[DISCAMTAF] [numeric](11, 2) NULL;
	

ALTER VIEW [VM1DTA].[GIPM](
						UNIQUE_NUMBER, 
						CHDRCOY,  
						CHDRNUM,  
						CVNOTE,  
						CNTTYPE,  
						GPREM,  
						NFEES,  
						NSDUTY,  
						NGRAMT,  
						NBCOMM,  
						NETPRAMT,  
						GSTAMT,
						BPREM,
						DISCRATE,
						DISCAMT,
						CMRATE,
						TOTPREM, 
						TRANTYP,  
						TRANNO,  
						GPREMAF,  
						NFEESAF,  
						NSDUTYAF,  
						NGRAMTAF,  
						NBCOMMAF,  
						NETPRAMTAF,  
						GSTAMTAF,
						BPREMAF,
						DISCRATEAF,
						DISCAMTAF,
						CMRATEAF,
						TOTPREMAF,  
						USRPRF,  
						JOBNM,  
						DATIME
		) AS
SELECT UNIQUE_NUMBER,
		CHDRCOY,  
						CHDRNUM,  
						CVNOTE,  
						CNTTYPE,  
						GPREM,  
						NFEES,  
						NSDUTY,  
						NGRAMT,  
						NBCOMM,  
						NETPRAMT,  
						GSTAMT,
						BPREM,
						DISCRATE,
						DISCAMT,
						CMRATE,
						TOTPREM, 
						TRANTYP,  
						TRANNO,  
						GPREMAF,  
						NFEESAF,  
						NSDUTYAF,  
						NGRAMTAF,  
						NBCOMMAF,  
						NETPRAMTAF,  
						GSTAMTAF,
						BPREMAF,
						DISCRATEAF,
						DISCAMTAF,
						CMRATEAF,
						TOTPREMAF,  
						USRPRF,  
						JOBNM,  
						DATIME
       FROM VM1DTA.GIPMPF


GO


