
ALTER VIEW [VM1DTA].[GCMH](UNIQUE_NUMBER, CHDRCOY, CLAMNUM, GCOCCNO, GCCLMSEQ, CHDRNUM, PRODTYP, GCCLMTNO, GCDPNTNO, CLNTPFX, CLNTCOY, CLNTNUM, GCSTS, DTECLAM, CLAIMCUR, CRATE, PLANNO, FMLYCDE, GCRMK, BNKCHARGE, SPECTRM, GCDBLIND, GCFRPDTE, GCLRPDTE, GCDTHCLM, GCCAUSCD, GCOPPVCD, GCOPBNCD, GCOPIAMT, GCOPBAMT, GCOPBNPY, GCOPDGCD, GCOPPTCD, REQNBCDE, REQNTYPE, GCAUTHBY, DATEAUTH, GCOPRSCD, GCGRSPY, GCNETPY, GCCOINA, DEDUCTPC, GCADVPY, GCTRDRPY, GCLOCCNO, MMFLG, PACCAMT, OVRLMT, DTEPYMTADV, DTEPYMTOTH, DTEATT, DTETRM, REVLINK, REVIND, TERMID, USER_T, TRDT, TRTM, APAIDAMT, INTPAIDAMT, GCSETLMD, GCSECSTS, ZCLTCLMREF, ZCLMRECD, ZCLNTPROC, ZCLMREF, ZFEECURR, ZFEETYP, ZCALCFEE, ZOVRDFEE, GRSKCLS, BENCDE, CLAIMCOND, OUTLOAN, USRPRF, JOBNM, DATIME,CLMPROCID,CLMAPRVID, DIACODE, ONDUTY, ROPENRSN, DCNIND, MEVENT) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CLAMNUM,
            GCOCCNO,
            GCCLMSEQ,
            CHDRNUM,
            PRODTYP,
            GCCLMTNO,
            GCDPNTNO,
            CLNTPFX,
            CLNTCOY,
            CLNTNUM,
            GCSTS,
            DTECLAM,
            CLAIMCUR,
            CRATE,
            PLANNO,
            FMLYCDE,
            GCRMK,
            BNKCHARGE,
            SPECTRM,
            GCDBLIND,
            GCFRPDTE,
            GCLRPDTE,
            GCDTHCLM,
            GCCAUSCD,
            GCOPPVCD,
            GCOPBNCD,
            GCOPIAMT,
            GCOPBAMT,
            GCOPBNPY,
            GCOPDGCD,
            GCOPPTCD,
            REQNBCDE,
            REQNTYPE,
            GCAUTHBY,
            DATEAUTH,
            GCOPRSCD,
            GCGRSPY,
            GCNETPY,
            GCCOINA,
            DEDUCTPC,
            GCADVPY,
            GCTRDRPY,
            GCLOCCNO,
            MMFLG,
            PACCAMT,
            OVRLMT,
            DTEPYMTADV,
            DTEPYMTOTH,
            DTEATT,
            DTETRM,
            REVLINK,
            REVIND,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            APAIDAMT,
            INTPAIDAMT,
            GCSETLMD,
            GCSECSTS,
            ZCLTCLMREF,
            ZCLMRECD,
            ZCLNTPROC,
            ZCLMREF,
            ZFEECURR,
            ZFEETYP,
            ZCALCFEE,
            ZOVRDFEE,
            GRSKCLS,
            BENCDE,
            CLAIMCOND,
            OUTLOAN,
            USRPRF,
            JOBNM,
            DATIME,
			CLMPROCID,
			CLMAPRVID,
			DIACODE,
			ONDUTY,
			ROPENRSN,
			DCNIND,
			MEVENT 

       FROM VM1DTA.GCMHPF

GO


