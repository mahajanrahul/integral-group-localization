/* add column [SERVUNIT],[PRODTYP] into [VM1DTA].[COVNPF] */
ALTER TABLE [VM1DTA].[COVNPF]
ADD [SERVUNIT] [nchar](2),[PRODTYP] [nchar](4)
GO



ALTER VIEW [VM1DTA].[COVN](UNIQUE_NUMBER, COMPANY, CNOTNUM, BRANCH, BOOKNUM, STATUS, DTEISS, DTERCV, REGNO, CHASSI, ENGINE, CANRSN, DTEADV, CNTTYPE, KOB, SUMINS, CCDATE, CRDATE, REMARKS01, REMARKS02, REMARKS03, CHDRCOY, CHDRNUM, TRANNO, TRCDE, RSKTYPE, RSKNO, CLNTPFX, CLNTCOY, CLNTNUM, AGNTPFX, AGNTCOY, AGNTNUM, USERID, USRPRF, JOBNM, DATIME, 
		LOSSDT, AMTPAID, SUBUMSAGE,SUBMPLTYP,SUBMDTISS,SUBMINSFRM,SUBMINSTO,SUBMINSNM,SUBMUPDATEDBY,SUBMENTRYDT,ZSTAFFCD, SERVUNIT, PRODTYP) AS
SELECT UNIQUE_NUMBER,
            COMPANY,
            CNOTNUM,
            BRANCH,
            BOOKNUM,
            STATUS,
            DTEISS,
            DTERCV,
            REGNO,
            CHASSI,
            ENGINE,
            CANRSN,
            DTEADV,
            CNTTYPE,
            KOB,
            SUMINS,
            CCDATE,
            CRDATE,
            REMARKS01,
            REMARKS02,
            REMARKS03,
            CHDRCOY,
            CHDRNUM,
            TRANNO,
            TRCDE,
            RSKTYPE,
            RSKNO,
            CLNTPFX,
            CLNTCOY,
            CLNTNUM,
            AGNTPFX,
            AGNTCOY,
            AGNTNUM,
            USERID,
            USRPRF,
            JOBNM,
            DATIME,
			LOSSDT, 
			AMTPAID, 
			SUBUMSAGE,
			SUBMPLTYP,
			SUBMDTISS,
			SUBMINSFRM,
			SUBMINSTO,
			SUBMINSNM,
			SUBMUPDATEDBY,
			SUBMENTRYDT,
			ZSTAFFCD,
			SERVUNIT,
			PRODTYP

       FROM VM1DTA.COVNPF


GO


ALTER VIEW [VM1DTA].[COVNBOK](UNIQUE_NUMBER, COMPANY, CNOTNUM, BRANCH, BOOKNUM, STATUS, DTEISS, DTERCV, REGNO, CHASSI, ENGINE, CANRSN, DTEADV, CNTTYPE, KOB, SUMINS, CCDATE, CRDATE, REMARKS01, REMARKS02, REMARKS03, CHDRCOY, CHDRNUM, TRANNO, TRCDE, RSKTYPE, RSKNO, CLNTPFX, CLNTCOY, CLNTNUM, AGNTPFX, AGNTCOY, AGNTNUM, USERID, USRPRF, JOBNM, DATIME,SERVUNIT, PRODTYP) AS
SELECT UNIQUE_NUMBER,
            COMPANY,
            CNOTNUM,
            BRANCH,
            BOOKNUM,
            [STATUS],
            DTEISS,
            DTERCV,
            REGNO,
            CHASSI,
            ENGINE,
            CANRSN,
            DTEADV,
            CNTTYPE,
            KOB,
            SUMINS,
            CCDATE,
            CRDATE,
            REMARKS01,
            REMARKS02,
            REMARKS03,
            CHDRCOY,
            CHDRNUM,
            TRANNO,
            TRCDE,
            RSKTYPE,
            RSKNO,
            CLNTPFX,
            CLNTCOY,
            CLNTNUM,
            AGNTPFX,
            AGNTCOY,
            AGNTNUM,
            USERID,
            USRPRF,
            JOBNM,
            DATIME,
			SERVUNIT,
			PRODTYP

       FROM VM1DTA.COVNPF

GO




ALTER VIEW [VM1DTA].[COVNREG](UNIQUE_NUMBER, COMPANY, CNOTNUM, BRANCH, BOOKNUM, [STATUS], DTEISS, DTERCV, REGNO, CHASSI, ENGINE, CANRSN, DTEADV, CNTTYPE, KOB, SUMINS, CCDATE, CRDATE, REMARKS01, REMARKS02, REMARKS03, CHDRCOY, CHDRNUM, TRANNO, TRCDE, RSKTYPE, RSKNO, CLNTPFX, CLNTCOY, CLNTNUM, AGNTPFX, AGNTCOY, AGNTNUM, USERID, USRPRF, JOBNM, DATIME, SERVUNIT, PRODTYP) AS
SELECT UNIQUE_NUMBER,
            COMPANY,
            CNOTNUM,
            BRANCH,
            BOOKNUM,
            [STATUS],
            DTEISS,
            DTERCV,
            REGNO,
            CHASSI,
            ENGINE,
            CANRSN,
            DTEADV,
            CNTTYPE,
            KOB,
            SUMINS,
            CCDATE,
            CRDATE,
            REMARKS01,
            REMARKS02,
            REMARKS03,
            CHDRCOY,
            CHDRNUM,
            TRANNO,
            TRCDE,
            RSKTYPE,
            RSKNO,
            CLNTPFX,
            CLNTCOY,
            CLNTNUM,
            AGNTPFX,
            AGNTCOY,
            AGNTNUM,
            USERID,
            USRPRF,
            JOBNM,
            DATIME,
			SERVUNIT,
			PRODTYP

       FROM VM1DTA.COVNPF

GO




ALTER VIEW [VM1DTA].[COVNRSK](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, RSKNO, TRANNO, COMPANY, CNOTNUM, TRCDE, [STATUS], KOB, DTEISS, DTERCV, JOBNM, USRPRF, DATIME, SERVUNIT, PRODTYP) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            RSKNO,
            TRANNO,
            COMPANY,
            CNOTNUM,
            TRCDE,
            [STATUS],
            KOB,
            DTEISS,
            DTERCV,
            JOBNM,
            USRPRF,
            DATIME,
			SERVUNIT,
			PRODTYP

       FROM VM1DTA.COVNPF

GO



/*add column ECNV, [CVNTTYPE],[COVERNT] into [VM1DTA].[GCHIPF] */
ALTER TABLE [VM1DTA].[GCHIPF]
ADD [ECNV] [nchar](1),[CVNTYPE] [nchar](2),[COVERNT] [nchar](8)
GO


CREATE VIEW [VM1DTA].[GCHICNN](
	[UNIQUE_NUMBER],
	[CHDRCOY],
	[CHDRNUM],
	[EFFDATE],
	[CCDATE],
	[CRDATE],
	[PRVBILFLG],
	[BILLFREQ],
	[GADJFREQ],
	[PAYRPFX],
	[PAYRCOY],
	[PAYRNUM],
	[AGNTPFX],
	[AGNTCOY],
	[AGNTNUM],
	[CNTBRANCH],
	[STCA],
	[STCB],
	[STCC],
	[STCD],
	[STCE],
	[BTDATENR],
	[NRISDATE],
	[TERMID],
	[USER_T],
	[TRDT],
	[TRTM],
	[TRANNO],
	[CRATE],
	[TERNMPRM],
	[SURGSCHMV],
	[AREACDEMV],
	[MEDPRVDR],
	[SPSMBR],
	[CHILDMBR],
	[SPSMED],
	[CHILDMED],
	[BANKCODE],
	[BILLCHNL],
	[MANDREF],
	[RIMTHVCD],
	[PRMRVWDT],
	[APPLTYP],
	[RIIND],
	[USRPRF],
	[JOBNM],
	[DATIME],
	[CFLIMIT],
	[POLBREAK],
	[CFTYPE],
	[LMTDRL],
	[NOFCLAIM],
	[TPA],
	[WKLADRT],
	[WKLCMRT],
	[NOFMBR],
	[TIMEHH01],
	[TIMEHH02],
	[ECNV],
	[CVNTYPE],
	[COVERNT]) AS 
SELECT [UNIQUE_NUMBER],
	[CHDRCOY],
	[CHDRNUM],
	[EFFDATE],
	[CCDATE],
	[CRDATE],
	[PRVBILFLG],
	[BILLFREQ],
	[GADJFREQ],
	[PAYRPFX],
	[PAYRCOY],
	[PAYRNUM],
	[AGNTPFX],
	[AGNTCOY],
	[AGNTNUM],
	[CNTBRANCH],
	[STCA],
	[STCB],
	[STCC],
	[STCD],
	[STCE],
	[BTDATENR],
	[NRISDATE],
	[TERMID],
	[USER_T],
	[TRDT],
	[TRTM],
	[TRANNO],
	[CRATE],
	[TERNMPRM],
	[SURGSCHMV],
	[AREACDEMV],
	[MEDPRVDR],
	[SPSMBR],
	[CHILDMBR],
	[SPSMED],
	[CHILDMED],
	[BANKCODE],
	[BILLCHNL],
	[MANDREF],
	[RIMTHVCD],
	[PRMRVWDT],
	[APPLTYP],
	[RIIND],
	[USRPRF],
	[JOBNM],
	[DATIME],
	[CFLIMIT],
	[POLBREAK],
	[CFTYPE],
	[LMTDRL],
	[NOFCLAIM],
	[TPA],
	[WKLADRT],
	[WKLCMRT],
	[NOFMBR],
	[TIMEHH01],
	[TIMEHH02],
	[ECNV],
	[CVNTYPE],
	[COVERNT]

	FROM [VM1DTA].[GCHIPF]

GO

/*VIEW*/
/*ALTER VIEW GCHI*/
ALTER VIEW [VM1DTA].[GCHI] (
	UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMEHH01
	,TIMEHH02
	,ECNV
	,CVNTYPE
	,COVERNT
	)
AS
SELECT UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMEHH01
	,TIMEHH02
	,ECNV
	,CVNTYPE
	,COVERNT

FROM VM1DTA.GCHIPF;


GO





/*VIEW*/
/*ALTER VIEW GCHITRN*/
ALTER VIEW [VM1DTA].[GCHITRN] (
	UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMEHH01
	,TIMEHH02
	,ECNV
	,CVNTYPE
	,COVERNT
	)
AS
SELECT UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMEHH01
	,TIMEHH02
	,ECNV
	,CVNTYPE
	,COVERNT
FROM VM1DTA.GCHIPF;


GO



ALTER VIEW [VM1DTA].[GCHD](
	UNIQUE_NUMBER
		,CHDRCOY
		,CHDRNUM
		,CHDRPFX
		,CNTTYPE
		,COWNPFX
		,COWNCOY
		,COWNNUM
		,STATCODE
		,PNDATE
		,SUBSFLG
		,OCCDATE
		,HRSKIND
		,CNTCURR
		,BILLCURR
		,TAKOVRFLG
		,GPRNLTYP
		,GPRMNTHS
		,RNLNOTTO
		,SRCEBUS
		,COYSRVAC
		,MRKSRVAC
		,DESPPFX
		,DESPCOY
		,DESPNUM
		,POLSCHPFLG
		,BTDATE
		,ADJDATE
		,PTDATE
		,PTDATEAB
		,TRANLUSED
		,LMBRNO
		,LHEADNO
		,EFFDCLDT
		,SERVUNIT
		,PNTRCDE
		,PROCID
		,TRANID
		,TRANNO
		,VALIDFLAG
		,SPECIND
		,TAXFLAG
		,AGEDEF
		,TERMAGE
		,PERSONCOV
		,ENROLLTYP
		,SPLITSUBS
		,AVLISU
		,MPLPFX
		,MPLCOY
		,MPLNUM
		,USRPRF
		,JOBNM
		,DATIME
		,IGRASP
		,IEXPLAIN
		,IDATE
		,MIDJOIN
		,CVISAIND
		,COVERNT
		,CNTISS
	) AS
SELECT UNIQUE_NUMBER
		,CHDRCOY
		,CHDRNUM
		,CHDRPFX
		,CNTTYPE
		,COWNPFX
		,COWNCOY
		,COWNNUM
		,STATCODE
		,PNDATE
		,SUBSFLG
		,OCCDATE
		,HRSKIND
		,CNTCURR
		,BILLCURR
		,TAKOVRFLG
		,GPRNLTYP
		,GPRMNTHS
		,RNLNOTTO
		,SRCEBUS
		,COYSRVAC
		,MRKSRVAC
		,DESPPFX
		,DESPCOY
		,DESPNUM
		,POLSCHPFLG
		,BTDATE
		,ADJDATE
		,PTDATE
		,PTDATEAB
		,TRANLUSED
		,LMBRNO
		,LHEADNO
		,EFFDCLDT
		,SERVUNIT
		,PNTRCDE
		,PROCID
		,TRANID
		,TRANNO
		,VALIDFLAG
		,SPECIND
		,TAXFLAG
		,AGEDEF
		,TERMAGE
		,PERSONCOV
		,ENROLLTYP
		,SPLITSUBS
		,AVLISU
		,MPLPFX
		,MPLCOY
		,MPLNUM
		,USRPRF
		,JOBNM
		,DATIME
		,IGRASP
		,IEXPLAIN
		,IDATE
		,MIDJOIN
		,CVISAIND
		,COVERNT
		,CNTISS
       FROM VM1DTA.CHDRPF
	WHERE SERVUNIT = 'GP'
GO



CREATE VIEW [VM1DTA].[GPHDDTE]
	([UNIQUE_NUMBER],
	[CHDRCOY],
	[CHDRNUM],
	[PRODTYP],
	[DTEATT],
	[CONTYPE],
	[AGRINDEM],
	[DTEIDM],
	[MEDEVD],
	[DTETRM],
	[REASONTRM],
	[TERMID],
	[USER_T],
	[TRDT],
	[TRTM],
	[TRANNO],
	[PRVCOND],
	[PRVAMTS],
	[PRVAMTM],
	[PREEXIBEF],
	[PREEXIAFT],
	[GSTTYPE],
	[PM06KEY],
	[ECTBRULE],
	[INVFMIX],
	[BFTDSN],
	[PAYMMODE],
	[INVSLVL],
	[ACBLRULE],
	[PRMPYOPT],
	[PRTEBFLG],
	[TRUSTEE],
	[GLBFEESC],
	[ACTEXFLG],
	[MBRFEE],
	[FRFUND],
	[EVNTFEE],
	[TIMEFEE],
	[FEEBTDTE],
	[TRIPTYP],
	[USRPRF],
	[JOBNM],
	[DATIME])
	AS
	SELECT 	[UNIQUE_NUMBER],
	[CHDRCOY],
	[CHDRNUM],
	[PRODTYP],
	[DTEATT],
	[CONTYPE],
	[AGRINDEM],
	[DTEIDM],
	[MEDEVD],
	[DTETRM],
	[REASONTRM],
	[TERMID],
	[USER_T],
	[TRDT],
	[TRTM],
	[TRANNO],
	[PRVCOND],
	[PRVAMTS],
	[PRVAMTM],
	[PREEXIBEF],
	[PREEXIAFT],
	[GSTTYPE],
	[PM06KEY],
	[ECTBRULE],
	[INVFMIX],
	[BFTDSN],
	[PAYMMODE],
	[INVSLVL],
	[ACBLRULE],
	[PRMPYOPT],
	[PRTEBFLG],
	[TRUSTEE],
	[GLBFEESC],
	[ACTEXFLG],
	[MBRFEE],
	[FRFUND],
	[EVNTFEE],
	[TIMEFEE],
	[FEEBTDTE],
	[TRIPTYP],
	[USRPRF],
	[JOBNM],
	[DATIME] 
	FROM VM1DTA.GPHDPF

	GO
	
	
	/*add column [OTHPFX], [OTHCOY] , [OTHNUM] into [VM1DTA].[Stmtpf] */
ALTER TABLE [VM1DTA].Stmtpf
ADD [OTHPFX] [nchar](2),[OTHCOY] [nchar](1),[OTHNUM] [nchar](14)
GO


/*add column [OTHPFX], [OTHCOY] , [OTHNUM] into [VM1DTA].[Ucshpf] */
ALTER TABLE [VM1DTA].Ucshpf
ADD [OTHPFX] [nchar](2),[OTHCOY] [nchar](1),[OTHNUM] [nchar](14)
GO




/*VIEW*/
/*ALTER VIEW GCAG*/
ALTER VIEW [VM1DTA].[GCAG] (
	UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMEHH01
	,TIMEHH02
	,ECNV
	,CVNTYPE
	,COVERNT
	)
AS
SELECT UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMEHH01
	,TIMEHH02
	,ECNV
	,CVNTYPE
	,COVERNT
FROM VM1DTA.GCHIPF;


GO




/*VIEW*/
/*ALTER VIEW GCHIASC*/
ALTER VIEW [VM1DTA].[GCHIASC] (
	UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMEHH01
	,TIMEHH02
	,ECNV
	,CVNTYPE
	,COVERNT
	)
AS
SELECT UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMEHH01
	,TIMEHH02
	,ECNV
	,CVNTYPE
	,COVERNT
FROM VM1DTA.GCHIPF;


GO




/*VIEW*/
/*ALTER VIEW GCHIPYR*/
ALTER VIEW [VM1DTA].[GCHIPYR] (
	UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMEHH01
	,TIMEHH02
	,ECNV
	,CVNTYPE
	,COVERNT
	)
AS
SELECT UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMEHH01
	,TIMEHH02
	,ECNV
	,CVNTYPE
	,COVERNT
FROM VM1DTA.GCHIPF;


GO




/*VIEW*/
/*ALTER VIEW GCHIRNL*/
ALTER VIEW [VM1DTA].[GCHIRNL] (
	UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMEHH01
	,TIMEHH02
	,ECNV
	,CVNTYPE
	,COVERNT
	)
AS
SELECT UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMEHH01
	,TIMEHH02
	,ECNV
	,CVNTYPE
	,COVERNT
FROM VM1DTA.GCHIPF;


GO



/*VIEW*/
/*ALTER VIEW GCHITPA*/
ALTER VIEW [VM1DTA].[GCHITPA] (
	UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMEHH01
	,TIMEHH02
	,ECNV
	,CVNTYPE
	,COVERNT
	)
AS
SELECT UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMEHH01
	,TIMEHH02
	,ECNV
	,CVNTYPE
	,COVERNT
FROM VM1DTA.GCHIPF;


GO



ALTER VIEW [VM1DTA].[STDN](UNIQUE_NUMBER, TRNID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, JOBID, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, OTPFX, OTCOY, OTNUM, OTCCY, AMT01, AMT02, AMT03, AMT04, AMT05, AMT06, AMT07, AMT08, AMT09, AMT10, AMT11, AMT12, AMT13, AMT14, AMT15, LCE01, LCE02, LCE03, LCE04, LCE05, LCE06, LCE07, LCE08, LCE09, LCE10, LCE11, LCE12, LCE13, LCE14, LCE15, GPAMT, CMAMT, NTAMT, GRSLCE, CMLCE, NTLCE, MYFLG, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, MARYACTYR, MARYACTMN, RITYPE, TRANNO, JOBNM, USRPRF, DATIME,OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            JOBID,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            AMT01,
            AMT02,
            AMT03,
            AMT04,
            AMT05,
            AMT06,
            AMT07,
            AMT08,
            AMT09,
            AMT10,
            AMT11,
            AMT12,
            AMT13,
            AMT14,
            AMT15,
            LCE01,
            LCE02,
            LCE03,
            LCE04,
            LCE05,
            LCE06,
            LCE07,
            LCE08,
            LCE09,
            LCE10,
            LCE11,
            LCE12,
            LCE13,
            LCE14,
            LCE15,
            GPAMT,
            CMAMT,
            NTAMT,
            GRSLCE,
            CMLCE,
            NTLCE,
            MYFLG,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            MARYACTYR,
            MARYACTMN,
            RITYPE,
            TRANNO,
            JOBNM,
            USRPRF,
            DATIME,
			OTHPFX,
			OTHCOY,
			OTHNUM
       FROM VM1DTA.STMTPF
      WHERE MYFLG = ' ' AND OTPFX = 'GI'

GO



ALTER VIEW [VM1DTA].[STMA](UNIQUE_NUMBER, TRNID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, JOBID, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, OTPFX, OTCOY, OTNUM, OTCCY, AMT01, AMT02, AMT03, AMT04, AMT05, AMT06, AMT07, AMT08, AMT09, AMT10, AMT11, AMT12, AMT13, AMT14, AMT15, LCE01, LCE02, LCE03, LCE04, LCE05, LCE06, LCE07, LCE08, LCE09, LCE10, LCE11, LCE12, LCE13, LCE14, LCE15, GPAMT, CMAMT, NTAMT, GRSLCE, CMLCE, NTLCE, MYFLG, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, MARYACTYR, MARYACTMN, RITYPE, USRPRF, JOBNM, DATIME, TRANNO, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            JOBID,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            AMT01,
            AMT02,
            AMT03,
            AMT04,
            AMT05,
            AMT06,
            AMT07,
            AMT08,
            AMT09,
            AMT10,
            AMT11,
            AMT12,
            AMT13,
            AMT14,
            AMT15,
            LCE01,
            LCE02,
            LCE03,
            LCE04,
            LCE05,
            LCE06,
            LCE07,
            LCE08,
            LCE09,
            LCE10,
            LCE11,
            LCE12,
            LCE13,
            LCE14,
            LCE15,
            GPAMT,
            CMAMT,
            NTAMT,
            GRSLCE,
            CMLCE,
            NTLCE,
            MYFLG,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            MARYACTYR,
            MARYACTMN,
            RITYPE,
            USRPRF,
            JOBNM,
            DATIME,
            TRANNO,
			OTHPFX,
			OTHCOY,
			OTHNUM

       FROM VM1DTA.STMTPF

GO





ALTER VIEW [VM1DTA].[STMT](UNIQUE_NUMBER, TRNID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, JOBID, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, OTPFX, OTCOY, OTNUM, OTCCY, AMT01, AMT02, AMT03, AMT04, AMT05, AMT06, AMT07, AMT08, AMT09, AMT10, AMT11, AMT12, AMT13, AMT14, AMT15, LCE01, LCE02, LCE03, LCE04, LCE05, LCE06, LCE07, LCE08, LCE09, LCE10, LCE11, LCE12, LCE13, LCE14, LCE15, GPAMT, CMAMT, NTAMT, GRSLCE, CMLCE, NTLCE, MYFLG, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, MARYACTYR, MARYACTMN, RITYPE, USRPRF, JOBNM, DATIME, TRANNO, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            JOBID,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            AMT01,
            AMT02,
            AMT03,
            AMT04,
            AMT05,
            AMT06,
            AMT07,
            AMT08,
            AMT09,
            AMT10,
            AMT11,
            AMT12,
            AMT13,
            AMT14,
            AMT15,
            LCE01,
            LCE02,
            LCE03,
            LCE04,
            LCE05,
            LCE06,
            LCE07,
            LCE08,
            LCE09,
            LCE10,
            LCE11,
            LCE12,
            LCE13,
            LCE14,
            LCE15,
            GPAMT,
            CMAMT,
            NTAMT,
            GRSLCE,
            CMLCE,
            NTLCE,
            MYFLG,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            MARYACTYR,
            MARYACTMN,
            RITYPE,
            USRPRF,
            JOBNM,
            DATIME,
            TRANNO,
			OTHPFX,
			OTHCOY,
			OTHNUM


       FROM VM1DTA.STMTPF

GO



ALTER VIEW [VM1DTA].[STMTCHQ](UNIQUE_NUMBER, TRNID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, JOBID, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, OTPFX, OTCOY, OTNUM, OTCCY, AMT01, AMT02, AMT03, AMT04, AMT05, AMT06, AMT07, AMT08, AMT09, AMT10, AMT11, AMT12, AMT13, AMT14, AMT15, LCE01, LCE02, LCE03, LCE04, LCE05, LCE06, LCE07, LCE08, LCE09, LCE10, LCE11, LCE12, LCE13, LCE14, LCE15, GPAMT, CMAMT, NTAMT, GRSLCE, CMLCE, NTLCE, MYFLG, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, MARYACTYR, MARYACTMN, RITYPE, TRANNO, JOBNM, USRPRF, DATIME, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            JOBID,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            AMT01,
            AMT02,
            AMT03,
            AMT04,
            AMT05,
            AMT06,
            AMT07,
            AMT08,
            AMT09,
            AMT10,
            AMT11,
            AMT12,
            AMT13,
            AMT14,
            AMT15,
            LCE01,
            LCE02,
            LCE03,
            LCE04,
            LCE05,
            LCE06,
            LCE07,
            LCE08,
            LCE09,
            LCE10,
            LCE11,
            LCE12,
            LCE13,
            LCE14,
            LCE15,
            GPAMT,
            CMAMT,
            NTAMT,
            GRSLCE,
            CMLCE,
            NTLCE,
            MYFLG,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            MARYACTYR,
            MARYACTMN,
            RITYPE,
            TRANNO,
            JOBNM,
            USRPRF,
            DATIME,
			OTHPFX,
			OTHCOY,
			OTHNUM

       FROM VM1DTA.STMTPF
      WHERE CHPFX = 'GI' AND MYFLG = ' ' AND SACCT = 'GR'

GO




ALTER VIEW [VM1DTA].[STMTMDT](UNIQUE_NUMBER, TRNID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, JOBID, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, OTPFX, OTCOY, OTNUM, OTCCY, AMT01, AMT02, AMT03, AMT04, AMT05, AMT06, AMT07, AMT08, AMT09, AMT10, AMT11, AMT12, AMT13, AMT14, AMT15, LCE01, LCE02, LCE03, LCE04, LCE05, LCE06, LCE07, LCE08, LCE09, LCE10, LCE11, LCE12, LCE13, LCE14, LCE15, GPAMT, CMAMT, NTAMT, GRSLCE, CMLCE, NTLCE, MYFLG, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, MARYACTYR, MARYACTMN, RITYPE, TRANNO, JOBNM, USRPRF, DATIME, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            JOBID,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            AMT01,
            AMT02,
            AMT03,
            AMT04,
            AMT05,
            AMT06,
            AMT07,
            AMT08,
            AMT09,
            AMT10,
            AMT11,
            AMT12,
            AMT13,
            AMT14,
            AMT15,
            LCE01,
            LCE02,
            LCE03,
            LCE04,
            LCE05,
            LCE06,
            LCE07,
            LCE08,
            LCE09,
            LCE10,
            LCE11,
            LCE12,
            LCE13,
            LCE14,
            LCE15,
            GPAMT,
            CMAMT,
            NTAMT,
            GRSLCE,
            CMLCE,
            NTLCE,
            MYFLG,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            MARYACTYR,
            MARYACTMN,
            RITYPE,
            TRANNO,
            JOBNM,
            USRPRF,
            DATIME,
			OTHPFX,
			OTHCOY,
			OTHNUM

       FROM VM1DTA.STMTPF
      WHERE SACCT = 'FG' AND SAC01 = 'P' OR SACCT = 'DG' AND SAC01 = 'P'

GO



ALTER VIEW [VM1DTA].[STMTOTH](UNIQUE_NUMBER, TRNID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, JOBID, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, OTPFX, OTCOY, OTNUM, OTCCY, AMT01, AMT02, AMT03, AMT04, AMT05, AMT06, AMT07, AMT08, AMT09, AMT10, AMT11, AMT12, AMT13, AMT14, AMT15, LCE01, LCE02, LCE03, LCE04, LCE05, LCE06, LCE07, LCE08, LCE09, LCE10, LCE11, LCE12, LCE13, LCE14, LCE15, GPAMT, CMAMT, NTAMT, GRSLCE, CMLCE, NTLCE, MYFLG, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, MARYACTYR, MARYACTMN, RITYPE, TRANNO, JOBNM, USRPRF, DATIME, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            JOBID,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            AMT01,
            AMT02,
            AMT03,
            AMT04,
            AMT05,
            AMT06,
            AMT07,
            AMT08,
            AMT09,
            AMT10,
            AMT11,
            AMT12,
            AMT13,
            AMT14,
            AMT15,
            LCE01,
            LCE02,
            LCE03,
            LCE04,
            LCE05,
            LCE06,
            LCE07,
            LCE08,
            LCE09,
            LCE10,
            LCE11,
            LCE12,
            LCE13,
            LCE14,
            LCE15,
            GPAMT,
            CMAMT,
            NTAMT,
            GRSLCE,
            CMLCE,
            NTLCE,
            MYFLG,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            MARYACTYR,
            MARYACTMN,
            RITYPE,
            TRANNO,
            JOBNM,
            USRPRF,
            DATIME,
			OTHPFX,
			OTHCOY,
			OTHNUM
       FROM VM1DTA.STMTPF
      WHERE MYFLG = ' ' AND OTPFX = 'CV'

GO



ALTER VIEW [VM1DTA].[STMTPOL](UNIQUE_NUMBER, TRNID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, JOBID, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, OTPFX, OTCOY, OTNUM, OTCCY, AMT01, AMT02, AMT03, AMT04, AMT05, AMT06, AMT07, AMT08, AMT09, AMT10, AMT11, AMT12, AMT13, AMT14, AMT15, LCE01, LCE02, LCE03, LCE04, LCE05, LCE06, LCE07, LCE08, LCE09, LCE10, LCE11, LCE12, LCE13, LCE14, LCE15, GPAMT, CMAMT, NTAMT, GRSLCE, CMLCE, NTLCE, MYFLG, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, MARYACTYR, MARYACTMN, RITYPE, TRANNO, JOBNM, USRPRF, DATIME, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            JOBID,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            AMT01,
            AMT02,
            AMT03,
            AMT04,
            AMT05,
            AMT06,
            AMT07,
            AMT08,
            AMT09,
            AMT10,
            AMT11,
            AMT12,
            AMT13,
            AMT14,
            AMT15,
            LCE01,
            LCE02,
            LCE03,
            LCE04,
            LCE05,
            LCE06,
            LCE07,
            LCE08,
            LCE09,
            LCE10,
            LCE11,
            LCE12,
            LCE13,
            LCE14,
            LCE15,
            GPAMT,
            CMAMT,
            NTAMT,
            GRSLCE,
            CMLCE,
            NTLCE,
            MYFLG,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            MARYACTYR,
            MARYACTMN,
            RITYPE,
            TRANNO,
            JOBNM,
            USRPRF,
            DATIME,
			OTHPFX,
			OTHCOY,
			OTHNUM

       FROM vm1dta.STMTPF

GO



ALTER VIEW [VM1DTA].[STMTPRM](UNIQUE_NUMBER, CLPFX, CLCOY, CLNUM, PLPFX, PLCOY, PLNUM, CHPFX, MYFLG, TRNID, VFLAG, ACPFX, ACCOY, ACNUM, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, JOBID, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, OTPFX, OTCOY, OTNUM, OTCCY, AMT01, AMT02, AMT03, AMT04, AMT05, AMT06, AMT07, AMT08, AMT09, AMT10, AMT11, AMT12, AMT13, AMT14, AMT15, LCE01, LCE02, LCE03, LCE04, LCE05, LCE06, LCE07, LCE08, LCE09, LCE10, LCE11, LCE12, LCE13, LCE14, LCE15, GPAMT, CMAMT, NTAMT, GRSLCE, CMLCE, NTLCE, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, MARYACTYR, MARYACTMN, RITYPE, TRANNO, USRPRF, DATIME, JOBNM, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            CLPFX,
            CLCOY,
            CLNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CHPFX,
            MYFLG,
            TRNID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            JOBID,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            AMT01,
            AMT02,
            AMT03,
            AMT04,
            AMT05,
            AMT06,
            AMT07,
            AMT08,
            AMT09,
            AMT10,
            AMT11,
            AMT12,
            AMT13,
            AMT14,
            AMT15,
            LCE01,
            LCE02,
            LCE03,
            LCE04,
            LCE05,
            LCE06,
            LCE07,
            LCE08,
            LCE09,
            LCE10,
            LCE11,
            LCE12,
            LCE13,
            LCE14,
            LCE15,
            GPAMT,
            CMAMT,
            NTAMT,
            GRSLCE,
            CMLCE,
            NTLCE,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            MARYACTYR,
            MARYACTMN,
            RITYPE,
            TRANNO,
            USRPRF,
            DATIME,
            JOBNM,
			OTHPFX,
			OTHCOY,
			OTHNUM

       FROM vm1dta.STMTPF

GO


ALTER VIEW [VM1DTA].[STMTRIN](UNIQUE_NUMBER, TRNID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, JOBID, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, OTPFX, OTCOY, OTNUM, OTCCY, AMT01, AMT02, AMT03, AMT04, AMT05, AMT06, AMT07, AMT08, AMT09, AMT10, AMT11, AMT12, AMT13, AMT14, AMT15, LCE01, LCE02, LCE03, LCE04, LCE05, LCE06, LCE07, LCE08, LCE09, LCE10, LCE11, LCE12, LCE13, LCE14, LCE15, GPAMT, CMAMT, NTAMT, GRSLCE, CMLCE, NTLCE, MYFLG, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, MARYACTYR, MARYACTMN, RITYPE, TRANNO, JOBNM, USRPRF, DATIME, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            JOBID,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            AMT01,
            AMT02,
            AMT03,
            AMT04,
            AMT05,
            AMT06,
            AMT07,
            AMT08,
            AMT09,
            AMT10,
            AMT11,
            AMT12,
            AMT13,
            AMT14,
            AMT15,
            LCE01,
            LCE02,
            LCE03,
            LCE04,
            LCE05,
            LCE06,
            LCE07,
            LCE08,
            LCE09,
            LCE10,
            LCE11,
            LCE12,
            LCE13,
            LCE14,
            LCE15,
            GPAMT,
            CMAMT,
            NTAMT,
            GRSLCE,
            CMLCE,
            NTLCE,
            MYFLG,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            MARYACTYR,
            MARYACTMN,
            RITYPE,
            TRANNO,
            JOBNM,
            USRPRF,
            DATIME,
			OTHPFX,
			OTHCOY,
			OTHNUM
       FROM vm1dta.STMTPF
      WHERE SACCT = 'RP' AND SAC01 = 'P' AND MYFLG = ' '
            OR SACCT = 'CO' AND SAC01 = 'P' AND MYFLG = ' '

GO



ALTER VIEW [VM1DTA].[STOS] (
	UNIQUE_NUMBER
	,ACCOY
	,ACPFX
	,ACNUM
	,ORCCY
	,MYFLG
	,TRNDT
	,BTACY
	,BTACM
	,PLPFX
	,PLCOY
	,PLNUM
	,CNTYP
	,RECORDFORMAT
	,NONKEYS
	)
AS
SELECT A.UNIQUE_NUMBER
	,A.ACCOY
	,A.ACPFX
	,A.ACNUM
	,A.ORCCY
	,A.MYFLG
	,A.TRNDT
	,A.BTACY
	,A.BTACM
	,A.PLPFX
	,A.PLCOY
	,A.PLNUM
	,A.CNTYP
	,A.RECORDFORMAT
	,LEFT(ISNULL(A.NONKEYS, ' ') + REPLICATE(' ', 1169), 1169) AS NONKEYS
FROM (
	SELECT STOS_STOSREC.UNIQUE_NUMBER
		,STOS_STOSREC.ACCOY
		,STOS_STOSREC.ACPFX
		,STOS_STOSREC.ACNUM
		,STOS_STOSREC.ORCCY
		,STOS_STOSREC.MYFLG
		,STOS_STOSREC.TRNDT
		,STOS_STOSREC.BTACY
		,STOS_STOSREC.BTACM
		,STOS_STOSREC.PLPFX
		,STOS_STOSREC.PLCOY
		,STOS_STOSREC.PLNUM
		,STOS_STOSREC.CNTYP
		,STOS_STOSREC.RECORDFORMAT
		,STOS_STOSREC.NONKEYS
	FROM VM1DTA.STOS_STOSREC
	
	UNION ALL
	
	SELECT STOS_UCSHREC.UNIQUE_NUMBER
		,STOS_UCSHREC.ACCOY
		,STOS_UCSHREC.ACPFX
		,STOS_UCSHREC.ACNUM
		,STOS_UCSHREC.ORCCY
		,STOS_UCSHREC.MYFLG
		,STOS_UCSHREC.TRNDT
		,STOS_UCSHREC.BTACY
		,STOS_UCSHREC.BTACM
		,STOS_UCSHREC.PLPFX
		,STOS_UCSHREC.PLCOY
		,STOS_UCSHREC.PLNUM
		,STOS_UCSHREC.CNTYP
		,STOS_UCSHREC.RECORDFORMAT
		,STOS_UCSHREC.NONKEYS
	FROM VM1DTA.STOS_UCSHREC
	) AS A;

GO




ALTER VIEW [VM1DTA].[STOS_STOSREC] (
	UNIQUE_NUMBER
	,ACCOY
	,ACPFX
	,ACNUM
	,ORCCY
	,MYFLG
	,TRNDT
	,BTACY
	,BTACM
	,PLPFX
	,PLCOY
	,PLNUM
	,CNTYP
	,RECORDFORMAT
	,NONKEYS
	)
AS
SELECT STMTPF.UNIQUE_NUMBER
	,STMTPF.ACCOY
	,STMTPF.ACPFX
	,STMTPF.ACNUM
	,STMTPF.ORCCY
	,STMTPF.MYFLG
	,STMTPF.TRNDT
	,STMTPF.BTACY
	,STMTPF.BTACM
	,STMTPF.PLPFX
	,STMTPF.PLCOY
	,STMTPF.PLNUM
	,STMTPF.CNTYP
	,'STOSREC' AS RECORDFORMAT
	,LEFT(isnull(CAST(STMTPF.TRNID AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.VFLAG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.ACPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.ACCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.ACNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.PLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.PLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.PLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.CLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.CLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.CHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.CHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CHNUM, ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(STMTPF.UNIQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.RECNC, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.DCODE, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SDCRA, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(STMTPF.SDCRB, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.SDCRC, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(STMTPF.SDCRD, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(STMTPF.CNTYP, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.TRNDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(STMTPF.EFFDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(STMTPF.TRCDE, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(STMTPF.SACCT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC01, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC02, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC03, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC04, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC05, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC06, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC07, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC08, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC09, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC10, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC11, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC12, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC13, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC14, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC15, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.BTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.BTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.BTBRN, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(CAST(STMTPF.BTACY AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(STMTPF.BTACM AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.BTTCD, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(STMTPF.BTBCH, ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(STMTPF.DUEDT, ' ') + REPLICATE(' ', 6), 6) + LEFT(isnull(STMTPF.AGECD, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.RSKCL, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.RSKNR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(STMTPF.BRKRF, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(CAST(STMTPF.AMT01 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT02 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT03 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT04 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT05 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT06 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT07 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT08 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT09 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT10 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT11 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT12 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT13 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT14 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT15 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE01 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE02 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE03 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE04 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE05 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE06 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE07 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE08 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE09 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE10 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE11 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE12 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE13 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE14 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE15 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.GPAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.CMAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.NTAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.GRSLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.CMLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.NTLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.MYFLG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(CAST(STMTPF.MYUQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.ORCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.LCCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.CRATE AS VARCHAR(max)), ' ') + REPLICATE(' ', 20), 20) + LEFT(isnull(STMTPF.STCA, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCB, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCC, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCD, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCE, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.MARYACTYR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(STMTPF.MARYACTMN AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.RITYPE, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.ORDTF, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(STMTPF.JOBID, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.USRPRF, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(STMTPF.JOBNM, ' ') + REPLICATE(' ', 10), 10) + CONVERT(VARCHAR, DATIME, 121) + LEFT(isnull(STMTPF.KEYSP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.ENDTP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CREQT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.JNLFG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.OTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.OTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.OTNUM, ' ') + REPLICATE(' ', 14), 14) + LEFT(isnull(STMTPF.OTCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.TRANNO AS VARCHAR(max)), ' ') + REPLICATE(' ', 6), 6) + LEFT(isnull(STMTPF.OTHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.OTHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.OTHNUM, ' ') + REPLICATE(' ', 14), 14)  AS NONKEYS
FROM VM1DTA.STMTPF;

GO


ALTER VIEW [VM1DTA].[STOS_UCSHREC] (
	UNIQUE_NUMBER
	,ACCOY
	,ACPFX
	,ACNUM
	,ORCCY
	,MYFLG
	,TRNDT
	,BTACY
	,BTACM
	,PLPFX
	,PLCOY
	,PLNUM
	,CNTYP
	,RECORDFORMAT
	,NONKEYS
	)
AS
SELECT UCSHPF.UNIQUE_NUMBER
	,UCSHPF.ACCOY
	,UCSHPF.ACPFX
	,UCSHPF.ACNUM
	,UCSHPF.ORCCY
	,UCSHPF.MYFLG
	,UCSHPF.TRNDT
	,UCSHPF.BTACY
	,UCSHPF.BTACM
	,UCSHPF.PLPFX
	,UCSHPF.PLCOY
	,UCSHPF.PLNUM
	,UCSHPF.CNTYP
	,'UCSHREC' AS RECORDFORMAT
	,LEFT(isnull(CAST(UCSHPF.TRNID AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(UCSHPF.VFLAG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.ACPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.ACCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.ACNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.PLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.PLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.PLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.CLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.CLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.CLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.CHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.CHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.CHNUM, ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(UCSHPF.UNIQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(UCSHPF.RECNC, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.DCODE, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SDCRA, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(UCSHPF.SDCRB, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.SDCRC, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(UCSHPF.SDCRD, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(UCSHPF.CNTYP, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.TRNDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(UCSHPF.EFFDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(UCSHPF.TRCDE, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(UCSHPF.SACCT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC01, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC02, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC03, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC04, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC05, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC06, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC07, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC08, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC09, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC10, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC11, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC12, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC13, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC14, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC15, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.BTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.BTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.BTBRN, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(CAST(UCSHPF.BTACY AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(UCSHPF.BTACM AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.BTTCD, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(UCSHPF.BTBCH, ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(UCSHPF.DUEDT, ' ') + REPLICATE(' ', 6), 6) + LEFT(isnull(UCSHPF.AGECD, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.RSKCL, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.RSKNR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(UCSHPF.BRKRF, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(CAST(UCSHPF.ORCSH AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.OCLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.NTAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.NTLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.CSHMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.CSLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.CSHDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(UCSHPF.MYUQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(UCSHPF.ORCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.LCCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.CRATE AS VARCHAR(max)), ' ') + REPLICATE(' ', 20), 20) + LEFT(isnull(UCSHPF.STCA, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.STCB, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.STCC, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.STCD, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.STCE, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.MARYACTYR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(UCSHPF.MARYACTMN AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.JOBID, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.MYFLG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.USRPRF, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(UCSHPF.JOBNM, ' ') + REPLICATE(' ', 10), 10) + CONVERT(VARCHAR, DATIME, 121) + LEFT(isnull(UCSHPF.TRMID, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(UCSHPF.KEYSP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.ENDTP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.CREQT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.ORDTF, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(UCSHPF.JNLFG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.OTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.OTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.OTNUM, ' ') + REPLICATE(' ', 14), 14) + LEFT(isnull(UCSHPF.OTCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.RTOLN AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.RTLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(UCSHPF.OTHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.OTHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.OTHNUM, ' ') + REPLICATE(' ', 14), 14)  AS NONKEYS
FROM VM1DTA.UCSHPF;

GO

ALTER VIEW [VM1DTA].[STOSCLT] (
	UNIQUE_NUMBER
	,CLPFX
	,CLCOY
	,CLNUM
	,ORCCY
	,ACCOY
	,ACPFX
	,ACNUM
	,RECORDFORMAT
	,NONKEYS
	)
AS
SELECT A.UNIQUE_NUMBER
	,A.CLPFX
	,A.CLCOY
	,A.CLNUM
	,A.ORCCY
	,A.ACCOY
	,A.ACPFX
	,A.ACNUM
	,A.RECORDFORMAT
	,LEFT(ISNULL(A.NONKEYS, ' ') + REPLICATE(' ', 1169), 1169) AS NONKEYS
FROM (
	SELECT STOSCLT_STOSCLTREC.UNIQUE_NUMBER
		,STOSCLT_STOSCLTREC.CLPFX
		,STOSCLT_STOSCLTREC.CLCOY
		,STOSCLT_STOSCLTREC.CLNUM
		,STOSCLT_STOSCLTREC.ORCCY
		,STOSCLT_STOSCLTREC.ACCOY
		,STOSCLT_STOSCLTREC.ACPFX
		,STOSCLT_STOSCLTREC.ACNUM
		,STOSCLT_STOSCLTREC.RECORDFORMAT
		,STOSCLT_STOSCLTREC.NONKEYS
	FROM VM1DTA.STOSCLT_STOSCLTREC
	
	UNION ALL
	
	SELECT STOSCLT_UCSHCLTREC.UNIQUE_NUMBER
		,STOSCLT_UCSHCLTREC.CLPFX
		,STOSCLT_UCSHCLTREC.CLCOY
		,STOSCLT_UCSHCLTREC.CLNUM
		,STOSCLT_UCSHCLTREC.ORCCY
		,STOSCLT_UCSHCLTREC.ACCOY
		,STOSCLT_UCSHCLTREC.ACPFX
		,STOSCLT_UCSHCLTREC.ACNUM
		,STOSCLT_UCSHCLTREC.RECORDFORMAT
		,STOSCLT_UCSHCLTREC.NONKEYS
	FROM VM1DTA.STOSCLT_UCSHCLTREC
	) AS A;

GO




ALTER VIEW [VM1DTA].[STOSCLT_STOSCLTREC] (
	UNIQUE_NUMBER
	,CLPFX
	,CLCOY
	,CLNUM
	,ORCCY
	,ACCOY
	,ACPFX
	,ACNUM
	,RECORDFORMAT
	,NONKEYS
	)
AS
SELECT STMTPF.UNIQUE_NUMBER
	,STMTPF.CLPFX
	,STMTPF.CLCOY
	,STMTPF.CLNUM
	,STMTPF.ORCCY
	,STMTPF.ACCOY
	,STMTPF.ACPFX
	,STMTPF.ACNUM
	,'STOSCLTREC' AS RECORDFORMAT
	,LEFT(isnull(CAST(STMTPF.TRNID AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.VFLAG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.ACPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.ACCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.ACNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.PLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.PLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.PLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.CLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.CLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.CHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.CHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CHNUM, ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(STMTPF.UNIQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.RECNC, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.DCODE, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SDCRA, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(STMTPF.SDCRB, ' ') + REPLICATE(' ', 8), 8) + 
	LEFT(isnull(STMTPF.SDCRC, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(STMTPF.SDCRD, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(STMTPF.KEYSP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CNTYP, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.TRNDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(STMTPF.EFFDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(STMTPF.ENDTP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CREQT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.ORDTF, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(STMTPF.TRCDE, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(STMTPF.SACCT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC01, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC02, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC03, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC04, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC05, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC06, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC07, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC08, ' ') + REPLICATE(' ', 2), 2) 
	+ LEFT(isnull(STMTPF.SAC09, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC10, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC11, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC12, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC13, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC14, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC15, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.JNLFG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.JOBID, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.BTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.BTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.BTBRN, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(CAST(STMTPF.BTACY AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(STMTPF.BTACM AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.BTTCD, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(STMTPF.BTBCH, ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(STMTPF.DUEDT, ' ') + REPLICATE(' ', 6), 6) + LEFT(isnull(STMTPF.AGECD, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.RSKCL, ' ') + REPLICATE(' ', 3), 3) + LEFT
	(isnull(CAST(STMTPF.RSKNR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(STMTPF.BRKRF, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.OTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.OTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.OTNUM, ' ') + REPLICATE(' ', 14), 14) + LEFT(isnull(STMTPF.OTCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.AMT01 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT02 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT03 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT04 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT05 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT06 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT07 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT08 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT09 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT10 
				AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT11 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT12 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT13 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT14 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT15 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE01 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE02 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE03 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE04 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE05 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE06 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE07 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE08 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT
	(isnull(CAST(STMTPF.LCE09 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE10 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE11 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE12 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE13 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE14 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE15 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.GPAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.CMAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.NTAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.GRSLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.CMLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.NTLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.MYFLG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(
			CAST(STMTPF.MYUQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.ORCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.OTHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.OTHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.OTHNUM, ' ') + REPLICATE(' ', 14), 14) + LEFT(isnull(STMTPF.LCCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.CRATE AS VARCHAR(max)), ' ') + REPLICATE(' ', 20), 20) + LEFT(isnull(STMTPF.STCA, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCB, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCC, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCD, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCE, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.MARYACTYR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(STMTPF.MARYACTMN AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.RITYPE, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(CAST(STMTPF.TRANNO AS VARCHAR(max)), ' ') + REPLICATE(' ', 6), 6) + LEFT(isnull(STMTPF.JOBNM, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(STMTPF.USRPRF, ' ') + REPLICATE(' ', 10), 10) + CONVERT(VARCHAR, DATIME, 121) AS NONKEYS
FROM VM1DTA.STMTPF;


GO




ALTER VIEW [VM1DTA].[STOSCLT_UCSHCLTREC] (
	UNIQUE_NUMBER
	,CLPFX
	,CLCOY
	,CLNUM
	,ORCCY
	,ACCOY
	,ACPFX
	,ACNUM
	,RECORDFORMAT
	,NONKEYS
	)
AS
SELECT UCSHPF.UNIQUE_NUMBER
	,UCSHPF.CLPFX
	,UCSHPF.CLCOY
	,UCSHPF.CLNUM
	,UCSHPF.ORCCY
	,UCSHPF.ACCOY
	,UCSHPF.ACPFX
	,UCSHPF.ACNUM
	,'UCSHCLTREC' AS RECORDFORMAT
	,LEFT(isnull(CAST(UCSHPF.TRNID AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(UCSHPF.TRMID, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(UCSHPF.VFLAG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.ACPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.ACCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.ACNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.PLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.PLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.PLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.CLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.CLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.CLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.CHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.CHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.CHNUM, ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(UCSHPF.UNIQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(UCSHPF.RECNC, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.DCODE, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SDCRA, ' ') + REPLICATE(' ', 4), 4) + 
	LEFT(isnull(UCSHPF.SDCRB, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.SDCRC, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(UCSHPF.SDCRD, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(UCSHPF.KEYSP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.CNTYP, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.TRNDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(UCSHPF.EFFDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(UCSHPF.ENDTP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.CREQT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.ORDTF, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(UCSHPF.TRCDE, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(UCSHPF.SACCT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC01, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC02, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC03, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC04, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC05, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC06, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC07, ' ') + REPLICATE(' ', 2), 2) 
	+ LEFT(isnull(UCSHPF.SAC08, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC09, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC10, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC11, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC12, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC13, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC14, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC15, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.JNLFG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.BTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.BTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.BTBRN, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(CAST(UCSHPF.BTACY AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(UCSHPF.BTACM AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.BTTCD, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(UCSHPF.BTBCH, ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(UCSHPF.DUEDT, ' ') + REPLICATE(' ', 6), 6) + LEFT(isnull(UCSHPF.AGECD, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.RSKCL, ' ') + REPLICATE(' ', 3), 3) + LEFT
	(isnull(CAST(UCSHPF.RSKNR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(UCSHPF.BRKRF, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.JOBID, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.OTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.OTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.OTNUM, ' ') + REPLICATE(' ', 14), 14) + LEFT(isnull(UCSHPF.OTCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.ORCSH AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.OCLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.NTAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.NTLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.CSHMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.CSLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.CSHDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(UCSHPF.RTOLN AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.RTLCE AS VARCHAR(max)), ' ') + 
		REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.MYUQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(UCSHPF.ORCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.LCCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.CRATE AS VARCHAR(max)), ' ') + REPLICATE(' ', 20), 20) + LEFT(isnull(UCSHPF.STCA, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.STCB, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.STCC, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.STCD, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.STCE, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.MARYACTYR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(UCSHPF.MARYACTMN AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.MYFLG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.JOBNM, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(UCSHPF.OTHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.OTHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.OTHNUM, ' ') + REPLICATE(' ', 14), 14) + LEFT(isnull(UCSHPF.USRPRF, ' ') + REPLICATE(' ', 10), 10) + CONVERT(VARCHAR, DATIME, 121) AS NONKEYS
FROM VM1DTA.UCSHPF;

GO


ALTER VIEW [VM1DTA].[STOSPOL] (
	UNIQUE_NUMBER
	,ACCOY
	,ACPFX
	,ACNUM
	,ORCCY
	,MYFLG
	,BTACY
	,BTACM
	,PLPFX
	,PLCOY
	,PLNUM
	,TRNDT
	,RECORDFORMAT
	,NONKEYS
	)
AS
SELECT A.UNIQUE_NUMBER
	,A.ACCOY
	,A.ACPFX
	,A.ACNUM
	,A.ORCCY
	,A.MYFLG
	,A.BTACY
	,A.BTACM
	,A.PLPFX
	,A.PLCOY
	,A.PLNUM
	,A.TRNDT
	,A.RECORDFORMAT
	,LEFT(ISNULL(A.NONKEYS, ' ') + REPLICATE(' ', 1169), 1169) AS NONKEYS
FROM (
	SELECT STOSPOL_STOSPOLREC.UNIQUE_NUMBER
		,STOSPOL_STOSPOLREC.ACCOY
		,STOSPOL_STOSPOLREC.ACPFX
		,STOSPOL_STOSPOLREC.ACNUM
		,STOSPOL_STOSPOLREC.ORCCY
		,STOSPOL_STOSPOLREC.MYFLG
		,STOSPOL_STOSPOLREC.BTACY
		,STOSPOL_STOSPOLREC.BTACM
		,STOSPOL_STOSPOLREC.PLPFX
		,STOSPOL_STOSPOLREC.PLCOY
		,STOSPOL_STOSPOLREC.PLNUM
		,STOSPOL_STOSPOLREC.TRNDT
		,STOSPOL_STOSPOLREC.RECORDFORMAT
		,STOSPOL_STOSPOLREC.NONKEYS
	FROM VM1DTA.STOSPOL_STOSPOLREC
	
	UNION ALL
	
	SELECT STOSPOL_UCSHPOLREC.UNIQUE_NUMBER
		,STOSPOL_UCSHPOLREC.ACCOY
		,STOSPOL_UCSHPOLREC.ACPFX
		,STOSPOL_UCSHPOLREC.ACNUM
		,STOSPOL_UCSHPOLREC.ORCCY
		,STOSPOL_UCSHPOLREC.MYFLG
		,STOSPOL_UCSHPOLREC.BTACY
		,STOSPOL_UCSHPOLREC.BTACM
		,STOSPOL_UCSHPOLREC.PLPFX
		,STOSPOL_UCSHPOLREC.PLCOY
		,STOSPOL_UCSHPOLREC.PLNUM
		,STOSPOL_UCSHPOLREC.TRNDT
		,STOSPOL_UCSHPOLREC.RECORDFORMAT
		,STOSPOL_UCSHPOLREC.NONKEYS
	FROM VM1DTA.STOSPOL_UCSHPOLREC
	) AS A;

GO


ALTER VIEW [VM1DTA].[STOSPOL_STOSPOLREC] (
	UNIQUE_NUMBER
	,ACCOY
	,ACPFX
	,ACNUM
	,ORCCY
	,MYFLG
	,BTACY
	,BTACM
	,PLPFX
	,PLCOY
	,PLNUM
	,TRNDT
	,RECORDFORMAT
	,NONKEYS
	)
AS
SELECT STMTPF.UNIQUE_NUMBER
	,STMTPF.ACCOY
	,STMTPF.ACPFX
	,STMTPF.ACNUM
	,STMTPF.ORCCY
	,STMTPF.MYFLG
	,STMTPF.BTACY
	,STMTPF.BTACM
	,STMTPF.PLPFX
	,STMTPF.PLCOY
	,STMTPF.PLNUM
	,STMTPF.TRNDT
	,'STOSPOLREC' AS RECORDFORMAT
	,LEFT(isnull(CAST(STMTPF.TRNID AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.VFLAG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.ACPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.ACCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.ACNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.PLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.PLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.PLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.CLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.CLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.CHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.CHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CHNUM, ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(STMTPF.UNIQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.RECNC, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.DCODE, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SDCRA, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(STMTPF.SDCRB, ' ') + REPLICATE(' ', 8), 8) + 
	LEFT(isnull(STMTPF.SDCRC, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(STMTPF.SDCRD, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(STMTPF.KEYSP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CNTYP, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.TRNDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(STMTPF.EFFDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(STMTPF.ENDTP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CREQT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.ORDTF, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(STMTPF.TRCDE, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(STMTPF.SACCT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC01, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC02, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC03, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC04, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC05, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC06, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC07, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC08, ' ') + REPLICATE(' ', 2), 2) 
	+ LEFT(isnull(STMTPF.SAC09, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC10, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC11, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC12, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC13, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC14, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC15, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.JNLFG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.JOBID, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.BTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.BTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.BTBRN, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(CAST(STMTPF.BTACY AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(STMTPF.BTACM AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.BTTCD, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(STMTPF.BTBCH, ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(STMTPF.DUEDT, ' ') + REPLICATE(' ', 6), 6) + LEFT(isnull(STMTPF.AGECD, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.RSKCL, ' ') + REPLICATE(' ', 3), 3) + LEFT
	(isnull(CAST(STMTPF.RSKNR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(STMTPF.BRKRF, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.OTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.OTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.OTNUM, ' ') + REPLICATE(' ', 14), 14) + LEFT(isnull(STMTPF.OTCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.AMT01 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT02 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT03 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT04 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT05 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT06 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT07 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT08 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT09 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT10 
				AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT11 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT12 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT13 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT14 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT15 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE01 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE02 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE03 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE04 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE05 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE06 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE07 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE08 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT
	(isnull(CAST(STMTPF.LCE09 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE10 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE11 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE12 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE13 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE14 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE15 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.GPAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.CMAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.NTAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.GRSLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.CMLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.NTLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.MYFLG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(
			CAST(STMTPF.MYUQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.ORCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.LCCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.CRATE AS VARCHAR(max)), ' ') + REPLICATE(' ', 20), 20) + LEFT(isnull(STMTPF.STCA, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCB, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCC, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCD, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCE, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.MARYACTYR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(STMTPF.MARYACTMN AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.RITYPE, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(CAST(STMTPF.TRANNO AS VARCHAR(max)), ' ') + REPLICATE(' ', 6), 6) + LEFT(isnull(STMTPF.OTHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.OTHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.OTHNUM, ' ') + REPLICATE(' ', 14), 14) + LEFT(isnull(STMTPF.JOBNM, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(STMTPF.USRPRF, ' ') + REPLICATE(' ', 10), 10) + CONVERT(VARCHAR, DATIME, 121) AS NONKEYS
FROM VM1DTA.STMTPF;

GO



ALTER VIEW [VM1DTA].[STOSPOL_UCSHPOLREC] (
	UNIQUE_NUMBER
	,ACCOY
	,ACPFX
	,ACNUM
	,ORCCY
	,MYFLG
	,BTACY
	,BTACM
	,PLPFX
	,PLCOY
	,PLNUM
	,TRNDT
	,RECORDFORMAT
	,NONKEYS
	)
AS
SELECT UCSHPF.UNIQUE_NUMBER
	,UCSHPF.ACCOY
	,UCSHPF.ACPFX
	,UCSHPF.ACNUM
	,UCSHPF.ORCCY
	,UCSHPF.MYFLG
	,UCSHPF.BTACY
	,UCSHPF.BTACM
	,UCSHPF.PLPFX
	,UCSHPF.PLCOY
	,UCSHPF.PLNUM
	,UCSHPF.TRNDT
	,'UCSHPOLREC' AS RECORDFORMAT
	,LEFT(isnull(CAST(UCSHPF.TRNID AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(UCSHPF.TRMID, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(UCSHPF.VFLAG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.ACPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.ACCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.ACNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.PLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.PLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.PLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.CLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.CLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.CLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.CHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.CHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.CHNUM, ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(UCSHPF.UNIQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(UCSHPF.RECNC, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.DCODE, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SDCRA, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(UCSHPF.SDCRB, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.SDCRC, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(UCSHPF.SDCRD, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(UCSHPF.KEYSP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.CNTYP, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.TRNDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(UCSHPF.EFFDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(UCSHPF.ENDTP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.CREQT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.ORDTF, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(UCSHPF.TRCDE, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(UCSHPF.SACCT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC01, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC02, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC03, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC04, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC05, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC06, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC07, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC08, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC09, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC10, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC11, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC12, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC13, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC14, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC15, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.JNLFG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.BTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.BTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.BTBRN, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(CAST(UCSHPF.BTACY AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(UCSHPF.BTACM AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.BTTCD, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(UCSHPF.BTBCH, ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(UCSHPF.DUEDT, ' ') + REPLICATE(' ', 6), 6) + LEFT(isnull(UCSHPF.AGECD, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.RSKCL, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.RSKNR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(UCSHPF.BRKRF, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.JOBID, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.OTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.OTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.OTNUM, ' ') + REPLICATE(' ', 14), 14) + LEFT(isnull(UCSHPF.OTCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.ORCSH AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.OCLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.NTAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.NTLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.CSHMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.CSLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.CSHDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(UCSHPF.RTOLN AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.RTLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.MYUQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(UCSHPF.ORCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.LCCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.CRATE AS VARCHAR(max)), ' ') + REPLICATE(' ', 20), 20) + LEFT(isnull(UCSHPF.STCA, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.STCB, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.STCC, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.STCD, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.STCE, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.MARYACTYR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(UCSHPF.MARYACTMN AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.MYFLG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.OTHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.OTHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.OTHNUM, ' ') + REPLICATE(' ', 14), 14) + LEFT(isnull(UCSHPF.JOBNM, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(UCSHPF.USRPRF, ' ') + REPLICATE(' ', 10), 10) + CONVERT(VARCHAR, DATIME, 121) AS NONKEYS
FROM VM1DTA.UCSHPF;

GO


ALTER VIEW [VM1DTA].[STOSTRN] (
	UNIQUE_NUMBER
	,ACCOY
	,ACPFX
	,ACNUM
	,ORCCY
	,MYFLG
	,BTACY
	,BTACM
	,TRNDT
	,PLPFX
	,PLCOY
	,PLNUM
	,RECORDFORMAT
	,NONKEYS
	)
AS
SELECT A.UNIQUE_NUMBER
	,A.ACCOY
	,A.ACPFX
	,A.ACNUM
	,A.ORCCY
	,A.MYFLG
	,A.BTACY
	,A.BTACM
	,A.TRNDT
	,A.PLPFX
	,A.PLCOY
	,A.PLNUM
	,A.RECORDFORMAT
	,LEFT(ISNULL(A.NONKEYS, ' ') + REPLICATE(' ', 1169), 1169) AS NONKEYS
FROM (
	SELECT STOSTRN_STOSTRNREC.UNIQUE_NUMBER
		,STOSTRN_STOSTRNREC.ACCOY
		,STOSTRN_STOSTRNREC.ACPFX
		,STOSTRN_STOSTRNREC.ACNUM
		,STOSTRN_STOSTRNREC.ORCCY
		,STOSTRN_STOSTRNREC.MYFLG
		,STOSTRN_STOSTRNREC.BTACY
		,STOSTRN_STOSTRNREC.BTACM
		,STOSTRN_STOSTRNREC.TRNDT
		,STOSTRN_STOSTRNREC.PLPFX
		,STOSTRN_STOSTRNREC.PLCOY
		,STOSTRN_STOSTRNREC.PLNUM
		,STOSTRN_STOSTRNREC.RECORDFORMAT
		,STOSTRN_STOSTRNREC.NONKEYS
	FROM VM1DTA.STOSTRN_STOSTRNREC
	
	UNION ALL
	
	SELECT STOSTRN_UCSHTRNREC.UNIQUE_NUMBER
		,STOSTRN_UCSHTRNREC.ACCOY
		,STOSTRN_UCSHTRNREC.ACPFX
		,STOSTRN_UCSHTRNREC.ACNUM
		,STOSTRN_UCSHTRNREC.ORCCY
		,STOSTRN_UCSHTRNREC.MYFLG
		,STOSTRN_UCSHTRNREC.BTACY
		,STOSTRN_UCSHTRNREC.BTACM
		,STOSTRN_UCSHTRNREC.TRNDT
		,STOSTRN_UCSHTRNREC.PLPFX
		,STOSTRN_UCSHTRNREC.PLCOY
		,STOSTRN_UCSHTRNREC.PLNUM
		,STOSTRN_UCSHTRNREC.RECORDFORMAT
		,STOSTRN_UCSHTRNREC.NONKEYS
	FROM VM1DTA.STOSTRN_UCSHTRNREC
	) AS A;

GO



ALTER VIEW [VM1DTA].[STOSTRN_STOSTRNREC] (
	UNIQUE_NUMBER
	,ACCOY
	,ACPFX
	,ACNUM
	,ORCCY
	,MYFLG
	,BTACY
	,BTACM
	,TRNDT
	,PLPFX
	,PLCOY
	,PLNUM
	,RECORDFORMAT
	,NONKEYS
	)
AS
SELECT STMTPF.UNIQUE_NUMBER
	,STMTPF.ACCOY
	,STMTPF.ACPFX
	,STMTPF.ACNUM
	,STMTPF.ORCCY
	,STMTPF.MYFLG
	,STMTPF.BTACY
	,STMTPF.BTACM
	,STMTPF.TRNDT
	,STMTPF.PLPFX
	,STMTPF.PLCOY
	,STMTPF.PLNUM
	,'STOSTRNREC' AS RECORDFORMAT
	,LEFT(isnull(CAST(STMTPF.TRNID AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.VFLAG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.ACPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.ACCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.ACNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.PLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.PLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.PLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.CLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.CLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.CHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.CHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CHNUM, ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(STMTPF.UNIQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.RECNC, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.DCODE, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SDCRA, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(STMTPF.SDCRB, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.SDCRC, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(STMTPF.SDCRD, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(STMTPF.KEYSP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CNTYP, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.TRNDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(STMTPF.EFFDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(STMTPF.ENDTP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CREQT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.ORDTF, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(STMTPF.TRCDE, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(STMTPF.SACCT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC01, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC02, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC03, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC04, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC05, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC06, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC07, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC08, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC09, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC10, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC11, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC12, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC13, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC14, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC15, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.JNLFG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.JOBID, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.BTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.BTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.BTBRN, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(CAST(STMTPF.BTACY AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(STMTPF.BTACM AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.BTTCD, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(STMTPF.BTBCH, ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(STMTPF.DUEDT, ' ') + REPLICATE(' ', 6), 6) + LEFT(isnull(STMTPF.AGECD, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.RSKCL, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.RSKNR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(STMTPF.BRKRF, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.OTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.OTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.OTNUM, ' ') + REPLICATE(' ', 14), 14) + LEFT(isnull(STMTPF.OTCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.AMT01 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT02 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT03 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT04 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT05 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT06 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT07 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT08 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT09 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT10 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT11 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT12 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT13 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT14 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT15 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE01 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE02 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE03 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE04 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE05 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE06 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE07 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE08 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE09 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE10 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE11 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE12 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE13 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE14 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE15 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.GPAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.CMAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.NTAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.GRSLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.CMLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.NTLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.MYFLG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(CAST(STMTPF.MYUQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.ORCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.LCCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.CRATE AS VARCHAR(max)), ' ') + REPLICATE(' ', 20), 20) + LEFT(isnull(STMTPF.STCA, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCB, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCC, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCD, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCE, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.MARYACTYR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(STMTPF.MARYACTMN AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.RITYPE, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(CAST(STMTPF.TRANNO AS VARCHAR(max)), ' ') + REPLICATE(' ', 6), 6) + LEFT(isnull(STMTPF.JOBNM, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(STMTPF.OTHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.OTHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.OTHNUM, ' ') + REPLICATE(' ', 14), 14) + LEFT(isnull(STMTPF.USRPRF, ' ') + REPLICATE(' ', 10), 10) + CONVERT(VARCHAR, DATIME, 121) AS NONKEYS
FROM VM1DTA.STMTPF;

GO


ALTER VIEW [VM1DTA].[STOSTRN_UCSHTRNREC] (
	UNIQUE_NUMBER
	,ACCOY
	,ACPFX
	,ACNUM
	,ORCCY
	,MYFLG
	,BTACY
	,BTACM
	,TRNDT
	,PLPFX
	,PLCOY
	,PLNUM
	,RECORDFORMAT
	,NONKEYS
	)
AS
SELECT UCSHPF.UNIQUE_NUMBER
	,UCSHPF.ACCOY
	,UCSHPF.ACPFX
	,UCSHPF.ACNUM
	,UCSHPF.ORCCY
	,UCSHPF.MYFLG
	,UCSHPF.BTACY
	,UCSHPF.BTACM
	,UCSHPF.TRNDT
	,UCSHPF.PLPFX
	,UCSHPF.PLCOY
	,UCSHPF.PLNUM
	,'UCSHTRNREC' AS RECORDFORMAT
	,LEFT(isnull(CAST(UCSHPF.TRNID AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(UCSHPF.TRMID, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(UCSHPF.VFLAG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.ACPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.ACCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.ACNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.PLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.PLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.PLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.CLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.CLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.CLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.CHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.CHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.CHNUM, ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(UCSHPF.UNIQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(UCSHPF.RECNC, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.DCODE, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SDCRA, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(UCSHPF.SDCRB, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.SDCRC, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(UCSHPF.SDCRD, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(UCSHPF.KEYSP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.CNTYP, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.TRNDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(UCSHPF.EFFDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(UCSHPF.ENDTP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.CREQT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.ORDTF, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(UCSHPF.TRCDE, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(UCSHPF.SACCT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC01, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC02, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC03, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC04, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC05, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC06, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC07, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC08, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC09, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC10, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC11, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC12, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC13, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC14, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC15, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.JNLFG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.BTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.BTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.BTBRN, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(CAST(UCSHPF.BTACY AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(UCSHPF.BTACM AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.BTTCD, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(UCSHPF.BTBCH, ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(UCSHPF.DUEDT, ' ') + REPLICATE(' ', 6), 6) + LEFT(isnull(UCSHPF.AGECD, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.RSKCL, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.RSKNR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(UCSHPF.BRKRF, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.JOBID, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.OTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.OTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.OTNUM, ' ') + REPLICATE(' ', 14), 14) + LEFT(isnull(UCSHPF.OTCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.ORCSH AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.OCLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.NTAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.NTLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.CSHMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.CSLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.CSHDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(UCSHPF.RTOLN AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.RTLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.MYUQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(UCSHPF.ORCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.LCCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.CRATE AS VARCHAR(max)), ' ') + REPLICATE(' ', 20), 20) + LEFT(isnull(UCSHPF.STCA, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.STCB, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.STCC, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.STCD, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.STCE, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.MARYACTYR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(UCSHPF.MARYACTMN AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.MYFLG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.JOBNM, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(UCSHPF.OTHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.OTHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.OTHNUM, ' ') + REPLICATE(' ', 14), 14) + LEFT(isnull(UCSHPF.USRPRF, ' ') + REPLICATE(' ', 10), 10) + CONVERT(VARCHAR, DATIME, 121) AS NONKEYS
FROM VM1DTA.UCSHPF;

GO



ALTER VIEW [VM1DTA].[STPL](UNIQUE_NUMBER, TRNID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, JOBID, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, OTPFX, OTCOY, OTNUM, OTCCY, AMT01, AMT02, AMT03, AMT04, AMT05, AMT06, AMT07, AMT08, AMT09, AMT10, AMT11, AMT12, AMT13, AMT14, AMT15, LCE01, LCE02, LCE03, LCE04, LCE05, LCE06, LCE07, LCE08, LCE09, LCE10, LCE11, LCE12, LCE13, LCE14, LCE15, GPAMT, CMAMT, NTAMT, GRSLCE, CMLCE, NTLCE, MYFLG, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, USRPRF, JOBNM, DATIME, MARYACTYR, MARYACTMN, RITYPE, TRANNO, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            JOBID,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            AMT01,
            AMT02,
            AMT03,
            AMT04,
            AMT05,
            AMT06,
            AMT07,
            AMT08,
            AMT09,
            AMT10,
            AMT11,
            AMT12,
            AMT13,
            AMT14,
            AMT15,
            LCE01,
            LCE02,
            LCE03,
            LCE04,
            LCE05,
            LCE06,
            LCE07,
            LCE08,
            LCE09,
            LCE10,
            LCE11,
            LCE12,
            LCE13,
            LCE14,
            LCE15,
            GPAMT,
            CMAMT,
            NTAMT,
            GRSLCE,
            CMLCE,
            NTLCE,
            MYFLG,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            USRPRF,
            JOBNM,
            DATIME,
            MARYACTYR,
            MARYACTMN,
            RITYPE,
            TRANNO,
			OTHPFX,
			OTHCOY,
			OTHNUM
       FROM VM1DTA.STMTPF

GO


ALTER VIEW [VM1DTA].[STSQ](UNIQUE_NUMBER, TRNID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, JOBID, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, OTPFX, OTCOY, OTNUM, OTCCY, AMT01, AMT02, AMT03, AMT04, AMT05, AMT06, AMT07, AMT08, AMT09, AMT10, AMT11, AMT12, AMT13, AMT14, AMT15, LCE01, LCE02, LCE03, LCE04, LCE05, LCE06, LCE07, LCE08, LCE09, LCE10, LCE11, LCE12, LCE13, LCE14, LCE15, GPAMT, CMAMT, NTAMT, GRSLCE, CMLCE, NTLCE, MYFLG, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, MARYACTYR, MARYACTMN, USRPRF, JOBNM, DATIME, RITYPE, TRANNO, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            JOBID,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            AMT01,
            AMT02,
            AMT03,
            AMT04,
            AMT05,
            AMT06,
            AMT07,
            AMT08,
            AMT09,
            AMT10,
            AMT11,
            AMT12,
            AMT13,
            AMT14,
            AMT15,
            LCE01,
            LCE02,
            LCE03,
            LCE04,
            LCE05,
            LCE06,
            LCE07,
            LCE08,
            LCE09,
            LCE10,
            LCE11,
            LCE12,
            LCE13,
            LCE14,
            LCE15,
            GPAMT,
            CMAMT,
            NTAMT,
            GRSLCE,
            CMLCE,
            NTLCE,
            MYFLG,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            MARYACTYR,
            MARYACTMN,
            USRPRF,
            JOBNM,
            DATIME,
            RITYPE,
            TRANNO,
			OTHPFX,
			OTHCOY,
			OTHNUM

       FROM VM1DTA.STMTPF

GO



ALTER VIEW [VM1DTA].[STUC] (
	UNIQUE_NUMBER
	,ACCOY
	,ACPFX
	,ACNUM
	,ORCCY
	,TRNDT
	,PLPFX
	,PLCOY
	,PLNUM
	,CNTYP
	,RECORDFORMAT
	,NONKEYS
	)
AS
SELECT A.UNIQUE_NUMBER
	,A.ACCOY
	,A.ACPFX
	,A.ACNUM
	,A.ORCCY
	,A.TRNDT
	,A.PLPFX
	,A.PLCOY
	,A.PLNUM
	,A.CNTYP
	,A.RECORDFORMAT
	,LEFT(ISNULL(A.NONKEYS, ' ') + REPLICATE(' ', 1169), 1169) AS NONKEYS
FROM (
	SELECT STUC_STMTREC.UNIQUE_NUMBER
		,STUC_STMTREC.ACCOY
		,STUC_STMTREC.ACPFX
		,STUC_STMTREC.ACNUM
		,STUC_STMTREC.ORCCY
		,STUC_STMTREC.TRNDT
		,STUC_STMTREC.PLPFX
		,STUC_STMTREC.PLCOY
		,STUC_STMTREC.PLNUM
		,STUC_STMTREC.CNTYP
		,STUC_STMTREC.RECORDFORMAT
		,STUC_STMTREC.NONKEYS
	FROM VM1DTA.STUC_STMTREC
	
	UNION ALL
	
	SELECT STUC_UCSHREC.UNIQUE_NUMBER
		,STUC_UCSHREC.ACCOY
		,STUC_UCSHREC.ACPFX
		,STUC_UCSHREC.ACNUM
		,STUC_UCSHREC.ORCCY
		,STUC_UCSHREC.TRNDT
		,STUC_UCSHREC.PLPFX
		,STUC_UCSHREC.PLCOY
		,STUC_UCSHREC.PLNUM
		,STUC_UCSHREC.CNTYP
		,STUC_UCSHREC.RECORDFORMAT
		,STUC_UCSHREC.NONKEYS
	FROM VM1DTA.STUC_UCSHREC
	) AS A;

GO


ALTER VIEW [VM1DTA].[STUCBCH] (
	UNIQUE_NUMBER
	,BTCOY
	,BTACY
	,BTACM
	,BTBRN
	,BTTCD
	,BTBCH
	,RECORDFORMAT
	,NONKEYS
	)
AS
SELECT A.UNIQUE_NUMBER
	,A.BTCOY
	,A.BTACY
	,A.BTACM
	,A.BTBRN
	,A.BTTCD
	,A.BTBCH
	,A.RECORDFORMAT
	,LEFT(ISNULL(A.NONKEYS, ' ') + REPLICATE(' ', 1169), 1169) AS NONKEYS
FROM (
	SELECT STUCBCH_STMTREC.UNIQUE_NUMBER
		,STUCBCH_STMTREC.BTCOY
		,STUCBCH_STMTREC.BTACY
		,STUCBCH_STMTREC.BTACM
		,STUCBCH_STMTREC.BTBRN
		,STUCBCH_STMTREC.BTTCD
		,STUCBCH_STMTREC.BTBCH
		,STUCBCH_STMTREC.RECORDFORMAT
		,STUCBCH_STMTREC.NONKEYS
	FROM VM1DTA.STUCBCH_STMTREC
	
	UNION ALL
	
	SELECT STUCBCH_UCSHREC.UNIQUE_NUMBER
		,STUCBCH_UCSHREC.BTCOY
		,STUCBCH_UCSHREC.BTACY
		,STUCBCH_UCSHREC.BTACM
		,STUCBCH_UCSHREC.BTBRN
		,STUCBCH_UCSHREC.BTTCD
		,STUCBCH_UCSHREC.BTBCH
		,STUCBCH_UCSHREC.RECORDFORMAT
		,STUCBCH_UCSHREC.NONKEYS
	FROM VM1DTA.STUCBCH_UCSHREC
	) AS A;

GO




ALTER VIEW [VM1DTA].[STUCBCH_STMTREC] (
	UNIQUE_NUMBER
	,BTCOY
	,BTACY
	,BTACM
	,BTBRN
	,BTTCD
	,BTBCH
	,RECORDFORMAT
	,NONKEYS
	)
AS
SELECT STMTPF.UNIQUE_NUMBER
	,STMTPF.BTCOY
	,STMTPF.BTACY
	,STMTPF.BTACM
	,STMTPF.BTBRN
	,STMTPF.BTTCD
	,STMTPF.BTBCH
	,'STMTREC' AS RECORDFORMAT
	,LEFT(isnull(CAST(STMTPF.TRNID AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.VFLAG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.ACPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.ACCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.ACNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.PLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.PLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.PLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.CLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.CLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.CHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.CHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CHNUM, ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(STMTPF.UNIQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.RECNC, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.DCODE, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SDCRA, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(STMTPF.SDCRB, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.SDCRC, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(STMTPF.SDCRD, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(STMTPF.KEYSP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CNTYP, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.TRNDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(STMTPF.EFFDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(STMTPF.ENDTP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CREQT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.ORDTF, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(STMTPF.TRCDE, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(STMTPF.SACCT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC01, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC02, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC03, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC04, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC05, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC06, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC07, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC08, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC09, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC10, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC11, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC12, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC13, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC14, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC15, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.JNLFG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.JOBID, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.BTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.BTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.BTBRN, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(CAST(STMTPF.BTACY AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(STMTPF.BTACM AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.BTTCD, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(STMTPF.BTBCH, ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(STMTPF.DUEDT, ' ') + REPLICATE(' ', 6), 6) + LEFT(isnull(STMTPF.AGECD, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.RSKCL, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.RSKNR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(STMTPF.BRKRF, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.OTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.OTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.OTNUM, ' ') + REPLICATE(' ', 14), 14) + LEFT(isnull(STMTPF.OTCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.AMT01 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT02 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT03 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT04 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT05 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT06 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT07 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT08 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT09 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT10 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT11 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT12 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT13 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT14 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT15 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE01 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE02 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE03 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE04 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE05 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE06 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE07 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE08 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE09 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE10 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE11 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE12 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE13 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE14 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE15 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.GPAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.CMAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.NTAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.GRSLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.CMLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.NTLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.MYFLG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(CAST(STMTPF.MYUQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.ORCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.LCCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.CRATE AS VARCHAR(max)), ' ') + REPLICATE(' ', 20), 20) + LEFT(isnull(STMTPF.STCA, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCB, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCC, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCD, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCE, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.MARYACTYR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(STMTPF.MARYACTMN AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.RITYPE, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(CAST(STMTPF.TRANNO AS VARCHAR(max)), ' ') + REPLICATE(' ', 6), 6) + LEFT(isnull(STMTPF.USRPRF, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(STMTPF.OTHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.OTHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.OTHNUM, ' ') + REPLICATE(' ', 14), 14) + LEFT(isnull(STMTPF.JOBNM, ' ') + REPLICATE(' ', 10), 10) + CONVERT(VARCHAR, DATIME, 121) AS NONKEYS
FROM VM1DTA.STMTPF;

GO




ALTER VIEW [VM1DTA].[STUCBCH_UCSHREC] (
	UNIQUE_NUMBER
	,BTCOY
	,BTACY
	,BTACM
	,BTBRN
	,BTTCD
	,BTBCH
	,RECORDFORMAT
	,NONKEYS
	)
AS
SELECT UCSHPF.UNIQUE_NUMBER
	,UCSHPF.BTCOY
	,UCSHPF.BTACY
	,UCSHPF.BTACM
	,UCSHPF.BTBRN
	,UCSHPF.BTTCD
	,UCSHPF.BTBCH
	,'UCSHREC' AS RECORDFORMAT
	,LEFT(isnull(CAST(UCSHPF.TRNID AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(UCSHPF.TRMID, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(UCSHPF.VFLAG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.ACPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.ACCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.ACNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.PLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.PLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.PLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.CLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.CLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.CLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.CHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.CHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.CHNUM, ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(UCSHPF.UNIQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(UCSHPF.RECNC, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.DCODE, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SDCRA, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(UCSHPF.SDCRB, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.SDCRC, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(UCSHPF.SDCRD, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(UCSHPF.KEYSP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.CNTYP, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.TRNDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(UCSHPF.EFFDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(UCSHPF.ENDTP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.CREQT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.ORDTF, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(UCSHPF.TRCDE, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(UCSHPF.SACCT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC01, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC02, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC03, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC04, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC05, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC06, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC07, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC08, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC09, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC10, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC11, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC12, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC13, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC14, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC15, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.JNLFG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.BTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.BTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.BTBRN, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(CAST(UCSHPF.BTACY AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(UCSHPF.BTACM AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.BTTCD, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(UCSHPF.BTBCH, ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(UCSHPF.DUEDT, ' ') + REPLICATE(' ', 6), 6) + LEFT(isnull(UCSHPF.AGECD, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.RSKCL, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.RSKNR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(UCSHPF.BRKRF, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.JOBID, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.OTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.OTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.OTNUM, ' ') + REPLICATE(' ', 14), 14) + LEFT(isnull(UCSHPF.OTCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.ORCSH AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.OCLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.NTAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.NTLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.CSHMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.CSLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.CSHDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(UCSHPF.RTOLN AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.RTLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.MYUQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(UCSHPF.ORCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.LCCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.CRATE AS VARCHAR(max)), ' ') + REPLICATE(' ', 20), 20) + LEFT(isnull(UCSHPF.STCA, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.STCB, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.STCC, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.STCD, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.STCE, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.MARYACTYR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(UCSHPF.MARYACTMN AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.USRPRF, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(UCSHPF.JOBNM, ' ') + REPLICATE(' ', 10), 10) + CONVERT(VARCHAR, DATIME, 121) + LEFT(isnull(UCSHPF.MYFLG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.OTHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.OTHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.OTHNUM, ' ') + REPLICATE(' ', 14), 14) AS NONKEYS
FROM VM1DTA.UCSHPF;

GO



ALTER VIEW [VM1DTA].[STUC_STMTREC] (
	UNIQUE_NUMBER
	,ACCOY
	,ACPFX
	,ACNUM
	,ORCCY
	,TRNDT
	,PLPFX
	,PLCOY
	,PLNUM
	,CNTYP
	,RECORDFORMAT
	,NONKEYS
	)
AS
SELECT STMTPF.UNIQUE_NUMBER
	,STMTPF.ACCOY
	,STMTPF.ACPFX
	,STMTPF.ACNUM
	,STMTPF.ORCCY
	,STMTPF.TRNDT
	,STMTPF.PLPFX
	,STMTPF.PLCOY
	,STMTPF.PLNUM
	,STMTPF.CNTYP
	,'STMTREC' AS RECORDFORMAT
	,LEFT(isnull(CAST(STMTPF.TRNID AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.VFLAG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.ACPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.ACCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.ACNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.PLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.PLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.PLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.CLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.CLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.CHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.CHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CHNUM, ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(STMTPF.UNIQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.RECNC, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.DCODE, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SDCRA, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(STMTPF.SDCRB, ' ') + REPLICATE(' ', 8), 8) + 
	LEFT(isnull(STMTPF.SDCRC, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(STMTPF.SDCRD, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(STMTPF.CNTYP, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.TRNDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(STMTPF.EFFDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(STMTPF.TRCDE, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(STMTPF.SACCT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC01, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC02, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC03, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC04, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC05, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC06, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC07, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC08, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC09, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC10, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC11, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC12, ' ') + REPLICATE(' ', 2), 2) + 
	LEFT(isnull(STMTPF.SAC13, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC14, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC15, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.BTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.BTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.BTBRN, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(CAST(STMTPF.BTACY AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(STMTPF.BTACM AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.BTTCD, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(STMTPF.BTBCH, ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(STMTPF.DUEDT, ' ') + REPLICATE(' ', 6), 6) + LEFT(isnull(STMTPF.AGECD, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.RSKCL, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.RSKNR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(STMTPF.BRKRF, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(CAST(STMTPF.AMT01 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT02 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT03 AS VARCHAR(
					max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT04 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT05 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT06 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT07 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT08 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT09 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT10 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT11 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT12 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT13 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT14 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT15 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE01 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(
		isnull(CAST(STMTPF.LCE02 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE03 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE04 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE05 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE06 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE07 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE08 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE09 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE10 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE11 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE12 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE13 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE14 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE15 AS VARCHAR(max)), ' ') + REPLICATE(
			' ', 19), 19) + LEFT(isnull(CAST(STMTPF.GPAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.CMAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.NTAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.GRSLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.CMLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.NTLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.MYFLG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(CAST(STMTPF.MYUQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.ORCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.LCCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.CRATE AS VARCHAR(max)), ' ') + REPLICATE(' ', 20), 20) + LEFT(isnull(STMTPF.STCA, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCB, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCC, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCD, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCE, ' ') + REPLICATE(' ', 3), 3) + 
	LEFT(isnull(CAST(STMTPF.MARYACTYR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(STMTPF.MARYACTMN AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.RITYPE, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(CAST(STMTPF.TRANNO AS VARCHAR(max)), ' ') + REPLICATE(' ', 6), 6) + LEFT(isnull(STMTPF.USRPRF, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(STMTPF.JOBNM, ' ') + REPLICATE(' ', 10), 10) + CONVERT(VARCHAR, DATIME, 121) + LEFT(isnull(STMTPF.KEYSP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.ENDTP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CREQT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.ORDTF, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(STMTPF.JNLFG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.JOBID, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.OTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.OTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.OTNUM, ' ') + REPLICATE(' ', 14), 14) + LEFT(isnull(STMTPF.OTCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.OTHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.OTHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.OTHNUM, ' ') + REPLICATE(' ', 14), 14) AS NONKEYS
FROM VM1DTA.STMTPF;

GO



ALTER VIEW [VM1DTA].[STUC_UCSHREC] (
	UNIQUE_NUMBER
	,ACCOY
	,ACPFX
	,ACNUM
	,ORCCY
	,TRNDT
	,PLPFX
	,PLCOY
	,PLNUM
	,CNTYP
	,RECORDFORMAT
	,NONKEYS
	)
AS
SELECT UCSHPF.UNIQUE_NUMBER
	,UCSHPF.ACCOY
	,UCSHPF.ACPFX
	,UCSHPF.ACNUM
	,UCSHPF.ORCCY
	,UCSHPF.TRNDT
	,UCSHPF.PLPFX
	,UCSHPF.PLCOY
	,UCSHPF.PLNUM
	,UCSHPF.CNTYP
	,'UCSHREC' AS RECORDFORMAT
	,LEFT(isnull(CAST(UCSHPF.TRNID AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(UCSHPF.VFLAG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.ACPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.ACCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.ACNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.PLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.PLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.PLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.CLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.CLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.CLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.CHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.CHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.CHNUM, ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(UCSHPF.UNIQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(UCSHPF.RECNC, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.DCODE, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SDCRA, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(UCSHPF.SDCRB, ' ') + REPLICATE(' ', 8), 8) + 	LEFT(isnull(UCSHPF.SDCRC, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(UCSHPF.SDCRD, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(UCSHPF.CNTYP, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.TRNDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(UCSHPF.EFFDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(UCSHPF.TRCDE, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(UCSHPF.SACCT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC01, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC02, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC03, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC04, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC05, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC06, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC07, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC08, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC09, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC10, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC11, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC12, ' ') + REPLICATE(' ', 2), 2) + 	LEFT(isnull(UCSHPF.SAC13, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC14, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC15, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.BTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.BTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.BTBRN, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(CAST(UCSHPF.BTACY AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(UCSHPF.BTACM AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.BTTCD, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(UCSHPF.BTBCH, ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(UCSHPF.DUEDT, ' ') + REPLICATE(' ', 6), 6) + LEFT(isnull(UCSHPF.AGECD, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.RSKCL, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.RSKNR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(UCSHPF.BRKRF, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(CAST(UCSHPF.ORCSH AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.OCLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.NTAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.NTLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.CSHMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.CSLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.CSHDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(UCSHPF.MYUQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(UCSHPF.ORCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.LCCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.CRATE AS VARCHAR(max)), ' ') + REPLICATE(' ', 20), 20) + LEFT(isnull(UCSHPF.STCA, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.STCB, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.STCC, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.STCD, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.STCE, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.MARYACTYR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(UCSHPF.MARYACTMN AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.USRPRF, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(UCSHPF.JOBNM, ' ') + REPLICATE(' ', 10), 10) + CONVERT(VARCHAR, DATIME, 121) + LEFT(isnull(UCSHPF.TRMID, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(UCSHPF.KEYSP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.ENDTP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.CREQT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.ORDTF, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(UCSHPF.JNLFG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.JOBID, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.OTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.OTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.OTNUM, ' ') + REPLICATE(' ', 14), 14) + LEFT(isnull(UCSHPF.OTCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.RTOLN AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.RTLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(UCSHPF.MYFLG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.OTHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.OTHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.OTHNUM, ' ') + REPLICATE(' ', 14), 14) AS NONKEYS
FROM VM1DTA.UCSHPF;

GO



ALTER VIEW [VM1DTA].[SUCL] (
	UNIQUE_NUMBER
	,CLPFX
	,CLCOY
	,CLNUM
	,ORCCY
	,PLPFX
	,PLCOY
	,PLNUM
	,CNTYP
	,TRNDT
	,ACPFX
	,ACCOY
	,ACNUM
	,UNIQK
	,RECORDFORMAT
	,NONKEYS
	)
AS
SELECT A.UNIQUE_NUMBER
	,A.CLPFX
	,A.CLCOY
	,A.CLNUM
	,A.ORCCY
	,A.PLPFX
	,A.PLCOY
	,A.PLNUM
	,A.CNTYP
	,A.TRNDT
	,A.ACPFX
	,A.ACCOY
	,A.ACNUM
	,A.UNIQK
	,A.RECORDFORMAT
	,LEFT(ISNULL(A.NONKEYS, ' ') + REPLICATE(' ', 1169), 1169) AS NONKEYS
FROM (
	SELECT SUCL_STCLREC.UNIQUE_NUMBER
		,SUCL_STCLREC.CLPFX
		,SUCL_STCLREC.CLCOY
		,SUCL_STCLREC.CLNUM
		,SUCL_STCLREC.ORCCY
		,SUCL_STCLREC.PLPFX
		,SUCL_STCLREC.PLCOY
		,SUCL_STCLREC.PLNUM
		,SUCL_STCLREC.CNTYP
		,SUCL_STCLREC.TRNDT
		,SUCL_STCLREC.ACPFX
		,SUCL_STCLREC.ACCOY
		,SUCL_STCLREC.ACNUM
		,SUCL_STCLREC.UNIQK
		,SUCL_STCLREC.RECORDFORMAT
		,SUCL_STCLREC.NONKEYS
	FROM VM1DTA.SUCL_STCLREC
	
	UNION ALL
	
	SELECT SUCL_UCCLREC.UNIQUE_NUMBER
		,SUCL_UCCLREC.CLPFX
		,SUCL_UCCLREC.CLCOY
		,SUCL_UCCLREC.CLNUM
		,SUCL_UCCLREC.ORCCY
		,SUCL_UCCLREC.PLPFX
		,SUCL_UCCLREC.PLCOY
		,SUCL_UCCLREC.PLNUM
		,SUCL_UCCLREC.CNTYP
		,SUCL_UCCLREC.TRNDT
		,SUCL_UCCLREC.ACPFX
		,SUCL_UCCLREC.ACCOY
		,SUCL_UCCLREC.ACNUM
		,SUCL_UCCLREC.UNIQK
		,SUCL_UCCLREC.RECORDFORMAT
		,SUCL_UCCLREC.NONKEYS
	FROM VM1DTA.SUCL_UCCLREC
	) AS A;

GO



ALTER VIEW [VM1DTA].[SUCL_STCLREC] (
	UNIQUE_NUMBER
	,CLPFX
	,CLCOY
	,CLNUM
	,ORCCY
	,PLPFX
	,PLCOY
	,PLNUM
	,CNTYP
	,TRNDT
	,ACPFX
	,ACCOY
	,ACNUM
	,UNIQK
	,RECORDFORMAT
	,NONKEYS
	)
AS
SELECT STMTPF.UNIQUE_NUMBER
	,STMTPF.CLPFX
	,STMTPF.CLCOY
	,STMTPF.CLNUM
	,STMTPF.ORCCY
	,STMTPF.PLPFX
	,STMTPF.PLCOY
	,STMTPF.PLNUM
	,STMTPF.CNTYP
	,STMTPF.TRNDT
	,STMTPF.ACPFX
	,STMTPF.ACCOY
	,STMTPF.ACNUM
	,STMTPF.UNIQK
	,'STCLREC' AS RECORDFORMAT
	,LEFT(isnull(CAST(STMTPF.TRNID AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.VFLAG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.ACPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.ACCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.ACNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.PLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.PLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.PLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.CLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.CLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.CHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.CHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CHNUM, ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(STMTPF.UNIQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.RECNC, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.DCODE, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SDCRA, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(STMTPF.SDCRB, ' ') + REPLICATE(' ', 8), 8) + 	LEFT(isnull(STMTPF.SDCRC, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(STMTPF.SDCRD, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(STMTPF.CNTYP, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.TRNDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(STMTPF.EFFDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(STMTPF.TRCDE, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(STMTPF.SACCT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC01, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC02, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC03, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC04, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC05, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC06, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC07, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC08, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC09, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC10, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC11, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC12, ' ') + REPLICATE(' ', 2), 2) + 	LEFT(isnull(STMTPF.SAC13, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC14, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC15, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.BTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.BTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.BTBRN, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(CAST(STMTPF.BTACY AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(STMTPF.BTACM AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.BTTCD, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(STMTPF.BTBCH, ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(STMTPF.DUEDT, ' ') + REPLICATE(' ', 6), 6) + LEFT(isnull(STMTPF.AGECD, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.RSKCL, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.RSKNR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(STMTPF.BRKRF, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(CAST(STMTPF.AMT01 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT02 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT03 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT04 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT05 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT06 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT07 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT08 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT09 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT10 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT11 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT12 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT13 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT14 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT15 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE01 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE02 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE03 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE04 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE05 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE06 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE07 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE08 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE09 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE10 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE11 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE12 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE13 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE14 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE15 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.GPAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.CMAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.NTAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.GRSLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.CMLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.NTLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.MYFLG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(CAST(STMTPF.MYUQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.ORCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.LCCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.CRATE AS VARCHAR(max)), ' ') + REPLICATE(' ', 20), 20) + LEFT(isnull(STMTPF.USRPRF, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(STMTPF.JOBNM, ' ') + REPLICATE(' ', 10), 10) + CONVERT(VARCHAR, DATIME, 121) + LEFT(isnull(STMTPF.KEYSP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.ENDTP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CREQT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.ORDTF, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(STMTPF.JNLFG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.JOBID, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.OTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.OTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.OTNUM, ' ') + REPLICATE(' ', 14), 14) + LEFT(isnull(STMTPF.OTCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCA, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCB, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCC, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCD, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCE, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.MARYACTYR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(STMTPF.MARYACTMN AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.RITYPE, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(CAST(STMTPF.TRANNO AS VARCHAR(max)), ' ') + REPLICATE(' ', 6), 6) + LEFT(isnull(STMTPF.OTHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.OTHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.OTHNUM, ' ') + REPLICATE(' ', 14), 14) AS NONKEYS
FROM VM1DTA.STMTPF;

GO



ALTER VIEW [VM1DTA].[SUCL_UCCLREC] (
	UNIQUE_NUMBER
	,CLPFX
	,CLCOY
	,CLNUM
	,ORCCY
	,PLPFX
	,PLCOY
	,PLNUM
	,CNTYP
	,TRNDT
	,ACPFX
	,ACCOY
	,ACNUM
	,UNIQK
	,RECORDFORMAT
	,NONKEYS
	)
AS
SELECT UCSHPF.UNIQUE_NUMBER
	,UCSHPF.CLPFX
	,UCSHPF.CLCOY
	,UCSHPF.CLNUM
	,UCSHPF.ORCCY
	,UCSHPF.PLPFX
	,UCSHPF.PLCOY
	,UCSHPF.PLNUM
	,UCSHPF.CNTYP
	,UCSHPF.TRNDT
	,UCSHPF.ACPFX
	,UCSHPF.ACCOY
	,UCSHPF.ACNUM
	,UCSHPF.UNIQK
	,'UCCLREC' AS RECORDFORMAT
	,LEFT(isnull(CAST(UCSHPF.TRNID AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(UCSHPF.VFLAG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.ACPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.ACCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.ACNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.PLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.PLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.PLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.CLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.CLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.CLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.CHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.CHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.CHNUM, ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(UCSHPF.UNIQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(UCSHPF.RECNC, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.DCODE, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SDCRA, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(UCSHPF.SDCRB, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.SDCRC, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(UCSHPF.SDCRD, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(UCSHPF.CNTYP, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.TRNDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(UCSHPF.EFFDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(UCSHPF.TRCDE, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(UCSHPF.SACCT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC01, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC02, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC03, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC04, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC05, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC06, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC07, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC08, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC09, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC10, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC11, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC12, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC13, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC14, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.SAC15, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.BTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.BTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.BTBRN, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(CAST(UCSHPF.BTACY AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(UCSHPF.BTACM AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.BTTCD, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(UCSHPF.BTBCH, ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(UCSHPF.DUEDT, ' ') + REPLICATE(' ', 6), 6) + LEFT(isnull(UCSHPF.AGECD, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.RSKCL, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.RSKNR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(UCSHPF.BRKRF, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(CAST(UCSHPF.ORCSH AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.OCLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.NTAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.NTLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.CSHMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.CSLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.CSHDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(UCSHPF.MYUQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(UCSHPF.ORCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.LCCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.CRATE AS VARCHAR(max)), ' ') + REPLICATE(' ', 20), 20) + LEFT(isnull(UCSHPF.USRPRF, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(UCSHPF.JOBNM, ' ') + REPLICATE(' ', 10), 10) + CONVERT(VARCHAR, DATIME, 121) + LEFT(isnull(UCSHPF.TRMID, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(UCSHPF.KEYSP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.ENDTP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.CREQT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.ORDTF, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(UCSHPF.JNLFG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.JOBID, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(UCSHPF.OTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.OTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.OTNUM, ' ') + REPLICATE(' ', 14), 14) + LEFT(isnull(UCSHPF.OTCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.RTOLN AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(UCSHPF.RTLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(UCSHPF.STCA, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.STCB, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.STCC, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.STCD, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.STCE, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(UCSHPF.MARYACTYR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(UCSHPF.MARYACTMN AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(UCSHPF.MYFLG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.OTHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(UCSHPF.OTHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(UCSHPF.OTHNUM, ' ') + REPLICATE(' ', 14), 14)  AS NONKEYS
FROM VM1DTA.UCSHPF;

GO


ALTER VIEW [VM1DTA].[SUPL] (
	UNIQUE_NUMBER
	,PLPFX
	,PLCOY
	,PLNUM
	,TRNDT
	,CLPFX
	,CLCOY
	,CLNUM
	,CHPFX
	,MYFLG
	,RECORDFORMAT
	,NONKEYS
	)
AS
SELECT A.UNIQUE_NUMBER
	,A.PLPFX
	,A.PLCOY
	,A.PLNUM
	,A.TRNDT
	,A.CLPFX
	,A.CLCOY
	,A.CLNUM
	,A.CHPFX
	,A.MYFLG
	,A.RECORDFORMAT
	,LEFT(NONKEYS + REPLICATE(' ',1489), 1489)
FROM (
	SELECT SUPL_STPLREC.UNIQUE_NUMBER
		,SUPL_STPLREC.PLPFX
		,SUPL_STPLREC.PLCOY
		,SUPL_STPLREC.PLNUM
		,SUPL_STPLREC.TRNDT
		,SUPL_STPLREC.CLPFX
		,SUPL_STPLREC.CLCOY
		,SUPL_STPLREC.CLNUM
		,SUPL_STPLREC.CHPFX
		,SUPL_STPLREC.MYFLG
		,SUPL_STPLREC.RECORDFORMAT
		,SUPL_STPLREC.NONKEYS
	FROM VM1DTA.SUPL_STPLREC
	
	UNION ALL
	
	SELECT SUPL_UCPLREC.UNIQUE_NUMBER
		,SUPL_UCPLREC.PLPFX
		,SUPL_UCPLREC.PLCOY
		,SUPL_UCPLREC.PLNUM
		,SUPL_UCPLREC.TRNDT
		,SUPL_UCPLREC.CLPFX
		,SUPL_UCPLREC.CLCOY
		,SUPL_UCPLREC.CLNUM
		,SUPL_UCPLREC.CHPFX
		,SUPL_UCPLREC.MYFLG
		,SUPL_UCPLREC.RECORDFORMAT
		,SUPL_UCPLREC.NONKEYS
	FROM VM1DTA.SUPL_UCPLREC
	) AS A;

GO



ALTER VIEW [VM1DTA].[SUPL_STPLREC] (
	UNIQUE_NUMBER
	,PLPFX
	,PLCOY
	,PLNUM
	,TRNDT
	,CLPFX
	,CLCOY
	,CLNUM
	,CHPFX
	,MYFLG
	,RECORDFORMAT
	,NONKEYS
	)
AS
 SELECT stmtpf.unique_number,
       stmtpf.plpfx,
       stmtpf.plcoy,
       stmtpf.plnum,
       stmtpf.trndt,
       stmtpf.clpfx,
       stmtpf.clcoy,
       stmtpf.clnum,
       stmtpf.chpfx,
       stmtpf.myflg,
       'STPLREC'                       AS RECORDFORMAT,
       RIGHT(REPLICATE(' ', 19) + ISNULL(Cast(stmtpf.trnid AS VARCHAR(max)), ' ') , 19)
       + LEFT(Isnull(stmtpf.vflag, ' ') + REPLICATE(' ', 1), 1)
       + LEFT(Isnull(stmtpf.acpfx, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(stmtpf.accoy, ' ') + REPLICATE(' ', 1), 1)
       + LEFT(Isnull(stmtpf.acnum, ' ') + REPLICATE(' ', 8), 8)
       + LEFT(Isnull(stmtpf.plpfx, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(stmtpf.plcoy, ' ') + REPLICATE(' ', 1), 1)
       + LEFT(Isnull(stmtpf.plnum, ' ') + REPLICATE(' ', 8), 8)
       + LEFT(Isnull(stmtpf.clpfx, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(stmtpf.clcoy, ' ') + REPLICATE(' ', 1), 1)
       + LEFT(Isnull(stmtpf.clnum, ' ') + REPLICATE(' ', 8), 8)
       + LEFT(Isnull(stmtpf.chpfx, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(stmtpf.chcoy, ' ') + REPLICATE(' ', 1), 1)
       + LEFT(Isnull(stmtpf.chnum, ' ') + REPLICATE(' ', 9), 9)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.uniqk AS VARCHAR(max)), ' '), 19)
       + LEFT(Isnull(stmtpf.recnc, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(stmtpf.dcode, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(stmtpf.sdcra, ' ') + REPLICATE(' ', 4), 4)
       + LEFT(Isnull(stmtpf.sdcrb, ' ') + REPLICATE(' ', 8), 8)
       + LEFT(Isnull(stmtpf.sdcrc, ' ') + REPLICATE(' ', 50), 50)
       + LEFT(Isnull(stmtpf.sdcrd, ' ') + REPLICATE(' ', 50), 50)
       + LEFT(Isnull(stmtpf.keysp, ' ') + REPLICATE(' ', 1), 1)
       + LEFT(Isnull(stmtpf.cntyp, ' ') + REPLICATE(' ', 3), 3)
       + RIGHT(REPLICATE(' ', 9) + Isnull(Cast(stmtpf.trndt AS VARCHAR(max)), ' '), 9)
       + RIGHT(REPLICATE(' ', 9) + Isnull(Cast(stmtpf.effdt AS VARCHAR(max)), ' '), 9)
       + LEFT(Isnull(stmtpf.endtp, ' ') + REPLICATE(' ', 1), 1)
       + LEFT(Isnull(stmtpf.creqt, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(stmtpf.ordtf, ' ') + REPLICATE(' ', 10), 10)
       + LEFT(Isnull(stmtpf.trcde, ' ') + REPLICATE(' ', 4), 4)
       + LEFT(Isnull(stmtpf.sacct, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(stmtpf.sac01, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(stmtpf.sac02, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(stmtpf.sac03, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(stmtpf.sac04, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(stmtpf.sac05, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(stmtpf.sac06, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(stmtpf.sac07, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(stmtpf.sac08, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(stmtpf.sac09, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(stmtpf.sac10, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(stmtpf.sac11, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(stmtpf.sac12, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(stmtpf.sac13, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(stmtpf.sac14, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(stmtpf.sac15, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(stmtpf.jnlfg, ' ') + REPLICATE(' ', 1), 1)
       + LEFT(Isnull(stmtpf.jobid, ' ') + REPLICATE(' ', 8), 8)
       + LEFT(Isnull(stmtpf.btpfx, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(stmtpf.btcoy, ' ') + REPLICATE(' ', 1), 1)
       + LEFT(Isnull(stmtpf.btbrn, ' ') + REPLICATE(' ', 2), 2)
       + RIGHT(REPLICATE(' ', 5) + Isnull(Cast(stmtpf.btacy AS VARCHAR(max)), ' '), 5)
       + RIGHT(REPLICATE(' ', 3) + Isnull(Cast(stmtpf.btacm AS VARCHAR(max)), ' '), 3)
       + LEFT(Isnull(stmtpf.bttcd, ' ') + REPLICATE(' ', 4), 4)
       + LEFT(Isnull(stmtpf.btbch, ' ') + REPLICATE(' ', 5), 5)
       + LEFT(Isnull(stmtpf.duedt, ' ') + REPLICATE(' ', 6), 6)
       + LEFT(Isnull(stmtpf.agecd, ' ') + REPLICATE(' ', 1), 1)
       + LEFT(Isnull(stmtpf.rskcl, ' ') + REPLICATE(' ', 3), 3)
       + RIGHT(REPLICATE(' ', 5) + Isnull(Cast(stmtpf.rsknr AS VARCHAR(max)), ' '), 5)
       + LEFT(Isnull(stmtpf.brkrf, ' ') + REPLICATE(' ', 8), 8)
       + LEFT(Isnull(stmtpf.otpfx, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(stmtpf.otcoy, ' ') + REPLICATE(' ', 1), 1)
       + LEFT(Isnull(stmtpf.otnum, ' ') + REPLICATE(' ', 14), 14)
       + LEFT(Isnull(stmtpf.otccy, ' ') + REPLICATE(' ', 3), 3)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.amt01 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.amt02 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.amt03 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.amt04 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.amt05 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.amt06 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.amt07 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.amt08 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.amt09 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.amt10 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.amt11 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.amt12 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.amt13 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.amt14 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.amt15 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.lce01 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.lce02 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.lce03 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.lce04 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.lce05 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.lce06 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.lce07 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.lce08 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.lce09 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.lce10 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.lce11 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.lce12 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.lce13 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.lce14 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.lce15 AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.gpamt AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.cmamt AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.ntamt AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.grslce AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.cmlce AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.ntlce AS VARCHAR(max)), ' '), 19)
       + LEFT(Isnull(stmtpf.myflg, ' ') + REPLICATE(' ', 1), 1)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(stmtpf.myuqk AS VARCHAR(max)), ' '), 19)
       + LEFT(Isnull(stmtpf.orccy, ' ') + REPLICATE(' ', 3), 3)
       + LEFT(Isnull(stmtpf.lcccy, ' ') + REPLICATE(' ', 3), 3)
       + RIGHT(REPLICATE(' ', 20) + Isnull(Cast(stmtpf.crate AS VARCHAR(max)), ' '), 20)
       + LEFT(Isnull(stmtpf.stca, ' ') + REPLICATE(' ', 3), 3)
       + LEFT(Isnull(stmtpf.stcb, ' ') + REPLICATE(' ', 3), 3)
       + LEFT(Isnull(stmtpf.stcc, ' ') + REPLICATE(' ', 3), 3)
       + LEFT(Isnull(stmtpf.stcd, ' ') + REPLICATE(' ', 3), 3)
       + LEFT(Isnull(stmtpf.stce, ' ') + REPLICATE(' ', 3), 3)
       + RIGHT(REPLICATE(' ', 5) + Isnull(Cast(stmtpf.maryactyr AS VARCHAR(max)), ' '), 5)
       + RIGHT(REPLICATE(' ', 3) + Isnull(Cast(stmtpf.maryactmn AS VARCHAR(max)), ' '), 3)
       + LEFT(Isnull(stmtpf.ritype, ' ') + REPLICATE(' ', 2), 2)
       + RIGHT(REPLICATE(' ', 6) + Isnull(Cast(stmtpf.tranno AS VARCHAR(max)), ' '), 6)
       + LEFT(Isnull(stmtpf.jobnm, ' ') + REPLICATE(' ', 10), 10)
	   + LEFT(isnull(STMTPF.OTHPFX, ' ') + REPLICATE(' ', 2), 2) 
	   + LEFT(isnull(STMTPF.OTHCOY, ' ') + REPLICATE(' ', 1), 1) 
	   + LEFT(isnull(STMTPF.OTHNUM, ' ') + REPLICATE(' ', 14), 14) 
       + LEFT(Isnull(stmtpf.usrprf, ' ') + REPLICATE(' ', 10), 10)
       + CONVERT(VARCHAR, datime, 121) AS NONKEYS
FROM   vm1dta.stmtpf;  

GO



ALTER VIEW [VM1DTA].[SUPL_UCPLREC] (
	UNIQUE_NUMBER
	,PLPFX
	,PLCOY
	,PLNUM
	,TRNDT
	,CLPFX
	,CLCOY
	,CLNUM
	,CHPFX
	,MYFLG
	,RECORDFORMAT
	,NONKEYS
	)
AS
 SELECT ucshpf.unique_number,
       ucshpf.plpfx,
       ucshpf.plcoy,
       ucshpf.plnum,
       ucshpf.trndt,
       ucshpf.clpfx,
       ucshpf.clcoy,
       ucshpf.clnum,
       ucshpf.chpfx,
       ucshpf.myflg,
       'UCPLREC'                       AS RECORDFORMAT,
       RIGHT(REPLICATE(' ', 19) + Isnull(Cast(ucshpf.trnid AS VARCHAR(max)), ' '), 19)
       + LEFT(Isnull(ucshpf.trmid, ' ') + REPLICATE(' ', 4), 4)
       + LEFT(Isnull(ucshpf.vflag, ' ') + REPLICATE(' ', 1), 1)
       + LEFT(Isnull(ucshpf.acpfx, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(ucshpf.accoy, ' ') + REPLICATE(' ', 1), 1)
       + LEFT(Isnull(ucshpf.acnum, ' ') + REPLICATE(' ', 8), 8)
       + LEFT(Isnull(ucshpf.plpfx, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(ucshpf.plcoy, ' ') + REPLICATE(' ', 1), 1)
       + LEFT(Isnull(ucshpf.plnum, ' ') + REPLICATE(' ', 8), 8)
       + LEFT(Isnull(ucshpf.clpfx, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(ucshpf.clcoy, ' ') + REPLICATE(' ', 1), 1)
       + LEFT(Isnull(ucshpf.clnum, ' ') + REPLICATE(' ', 8), 8)
       + LEFT(Isnull(ucshpf.chpfx, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(ucshpf.chcoy, ' ') + REPLICATE(' ', 1), 1)
       + LEFT(Isnull(ucshpf.chnum, ' ') + REPLICATE(' ', 9), 9)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(ucshpf.uniqk AS VARCHAR(max)), ' '), 19)
       + LEFT(Isnull(ucshpf.recnc, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(ucshpf.dcode, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(ucshpf.sdcra, ' ') + REPLICATE(' ', 4), 4)
       + LEFT(Isnull(ucshpf.sdcrb, ' ') + REPLICATE(' ', 8), 8)
       + LEFT(Isnull(ucshpf.sdcrc, ' ') + REPLICATE(' ', 50), 50)
       + LEFT(Isnull(ucshpf.sdcrd, ' ') + REPLICATE(' ', 50), 50)
       + LEFT(Isnull(ucshpf.keysp, ' ') + REPLICATE(' ', 1), 1)
       + LEFT(Isnull(ucshpf.cntyp, ' ') + REPLICATE(' ', 3), 3)
       + RIGHT(REPLICATE(' ', 9) + Isnull(Cast(ucshpf.trndt AS VARCHAR(max)), ' '), 9)
       + RIGHT(REPLICATE(' ', 9) + Isnull(Cast(ucshpf.effdt AS VARCHAR(max)), ' '), 9)
       + LEFT(Isnull(ucshpf.endtp, ' ') + REPLICATE(' ', 1), 1)
       + LEFT(Isnull(ucshpf.creqt, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(ucshpf.ordtf, ' ') + REPLICATE(' ', 10), 10)
       + LEFT(Isnull(ucshpf.trcde, ' ') + REPLICATE(' ', 4), 4)
       + LEFT(Isnull(ucshpf.sacct, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(ucshpf.sac01, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(ucshpf.sac02, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(ucshpf.sac03, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(ucshpf.sac04, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(ucshpf.sac05, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(ucshpf.sac06, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(ucshpf.sac07, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(ucshpf.sac08, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(ucshpf.sac09, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(ucshpf.sac10, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(ucshpf.sac11, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(ucshpf.sac12, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(ucshpf.sac13, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(ucshpf.sac14, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(ucshpf.sac15, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(ucshpf.jnlfg, ' ') + REPLICATE(' ', 1), 1)
       + LEFT(Isnull(ucshpf.btpfx, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(ucshpf.btcoy, ' ') + REPLICATE(' ', 1), 1)
       + LEFT(Isnull(ucshpf.btbrn, ' ') + REPLICATE(' ', 2), 2)
       + RIGHT(REPLICATE(' ', 5) + Isnull(Cast(ucshpf.btacy AS VARCHAR(max)), ' '), 5)
       + RIGHT(REPLICATE(' ', 3) + Isnull(Cast(ucshpf.btacm AS VARCHAR(max)), ' '), 3)
       + LEFT(Isnull(ucshpf.bttcd, ' ') + REPLICATE(' ', 4), 4)
       + LEFT(Isnull(ucshpf.btbch, ' ') + REPLICATE(' ', 5), 5)
       + LEFT(Isnull(ucshpf.duedt, ' ') + REPLICATE(' ', 6), 6)
       + LEFT(Isnull(ucshpf.agecd, ' ') + REPLICATE(' ', 1), 1)
       + LEFT(Isnull(ucshpf.rskcl, ' ') + REPLICATE(' ', 3), 3)
       + RIGHT(REPLICATE(' ', 5) + Isnull(Cast(ucshpf.rsknr AS VARCHAR(max)), ' '), 5)
       + LEFT(Isnull(ucshpf.brkrf, ' ') + REPLICATE(' ', 8), 8)
       + LEFT(Isnull(ucshpf.jobid, ' ') + REPLICATE(' ', 8), 8)
       + LEFT(Isnull(ucshpf.otpfx, ' ') + REPLICATE(' ', 2), 2)
       + LEFT(Isnull(ucshpf.otcoy, ' ') + REPLICATE(' ', 1), 1)
       + LEFT(Isnull(ucshpf.otnum, ' ') + REPLICATE(' ', 14), 14)
       + LEFT(Isnull(ucshpf.otccy, ' ') + REPLICATE(' ', 3), 3)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(ucshpf.orcsh AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(ucshpf.oclce AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(ucshpf.ntamt AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(ucshpf.ntlce AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(ucshpf.cshmt AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(ucshpf.cslce AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 9) + Isnull(Cast(ucshpf.cshdt AS VARCHAR(max)), ' '), 9)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(ucshpf.rtoln AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(ucshpf.rtlce AS VARCHAR(max)), ' '), 19)
       + RIGHT(REPLICATE(' ', 19) + Isnull(Cast(ucshpf.myuqk AS VARCHAR(max)), ' '), 19)
       + LEFT(Isnull(ucshpf.orccy, ' ') + REPLICATE(' ', 3), 3)
       + LEFT(Isnull(ucshpf.lcccy, ' ') + REPLICATE(' ', 3), 3)
       + RIGHT(REPLICATE(' ', 20) + Isnull(Cast(ucshpf.crate AS VARCHAR(max)), ' '), 20)
       + LEFT(Isnull(ucshpf.stca, ' ') + REPLICATE(' ', 3), 3)
       + LEFT(Isnull(ucshpf.stcb, ' ') + REPLICATE(' ', 3), 3)
       + LEFT(Isnull(ucshpf.stcc, ' ') + REPLICATE(' ', 3), 3)
       + LEFT(Isnull(ucshpf.stcd, ' ') + REPLICATE(' ', 3), 3)
       + LEFT(Isnull(ucshpf.stce, ' ') + REPLICATE(' ', 3), 3)
       + RIGHT(REPLICATE(' ', 5) + Isnull(Cast(ucshpf.maryactyr AS VARCHAR(max)), ' '), 5)
       + RIGHT(REPLICATE(' ', 3) + Isnull(Cast(ucshpf.maryactmn AS VARCHAR(max)), ' '), 3)
       + LEFT(Isnull(ucshpf.myflg, ' ') + REPLICATE(' ', 1), 1)
       + LEFT(Isnull(ucshpf.jobnm, ' ') + REPLICATE(' ', 10), 10)
	   + LEFT(isnull(UCSHPF.OTHPFX, ' ') + REPLICATE(' ', 2), 2) 
	   + LEFT(isnull(UCSHPF.OTHCOY, ' ') + REPLICATE(' ', 1), 1) 
	   + LEFT(isnull(UCSHPF.OTHNUM, ' ') + REPLICATE(' ', 14), 14)
       + LEFT(Isnull(ucshpf.usrprf, ' ') + REPLICATE(' ', 10), 10)
       + CONVERT(VARCHAR, datime, 121) AS NONKEYS
FROM   vm1dta.ucshpf;  

GO



ALTER VIEW [VM1DTA].[SUSQ] (
	UNIQUE_NUMBER
	,ACPFX
	,ACCOY
	,ACNUM
	,UNIQK
	,RECORDFORMAT
	,NONKEYS
	)
AS
SELECT A.UNIQUE_NUMBER
	,A.ACPFX
	,A.ACCOY
	,A.ACNUM
	,A.UNIQK
	,A.RECORDFORMAT
	,LEFT(ISNULL(A.NONKEYS, ' ') + REPLICATE(' ', 1179), 1179) AS NONKEYS
FROM (
	SELECT SUSQ_STMTREC.UNIQUE_NUMBER
		,SUSQ_STMTREC.ACPFX
		,SUSQ_STMTREC.ACCOY
		,SUSQ_STMTREC.ACNUM
		,SUSQ_STMTREC.UNIQK
		,SUSQ_STMTREC.RECORDFORMAT
		,SUSQ_STMTREC.NONKEYS
	FROM VM1DTA.SUSQ_STMTREC
	
	UNION ALL
	
	SELECT SUSQ_UCSHREC.UNIQUE_NUMBER
		,SUSQ_UCSHREC.ACPFX
		,SUSQ_UCSHREC.ACCOY
		,SUSQ_UCSHREC.ACNUM
		,SUSQ_UCSHREC.UNIQK
		,SUSQ_UCSHREC.RECORDFORMAT
		,SUSQ_UCSHREC.NONKEYS
	FROM VM1DTA.SUSQ_UCSHREC
	) AS A;


GO


ALTER VIEW [VM1DTA].[SUSQ_STMTREC] (
                UNIQUE_NUMBER
                ,ACPFX
                ,ACCOY
                ,ACNUM
                ,UNIQK
                ,RECORDFORMAT
                ,NONKEYS
                )
AS
SELECT STMTPF.UNIQUE_NUMBER
                ,STMTPF.ACPFX
                ,STMTPF.ACCOY
                ,STMTPF.ACNUM
                ,STMTPF.UNIQK
                ,'STMTREC' AS RECORDFORMAT
                ,LEFT(isnull(CAST(STMTPF.TRNID AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                + LEFT(isnull(STMTPF.VFLAG, ' ') + REPLICATE(' ', 1), 1) 
                + LEFT(isnull(STMTPF.ACPFX, ' ') + REPLICATE(' ', 2), 2) 
                + LEFT(isnull(STMTPF.ACCOY, ' ') + REPLICATE(' ', 1), 1) 
                + LEFT(isnull(STMTPF.ACNUM, ' ') + REPLICATE(' ', 8), 8) 
                + LEFT(isnull(STMTPF.PLPFX, ' ') + REPLICATE(' ', 2), 2) 
                + LEFT(isnull(STMTPF.PLCOY, ' ') + REPLICATE(' ', 1), 1) 
                + LEFT(isnull(STMTPF.PLNUM, ' ') + REPLICATE(' ', 8), 8) 
                + LEFT(isnull(STMTPF.CLPFX, ' ') + REPLICATE(' ', 2), 2) 
                + LEFT(isnull(STMTPF.CLCOY, ' ') + REPLICATE(' ', 1), 1) 
                + LEFT(isnull(STMTPF.CLNUM, ' ') + REPLICATE(' ', 8), 8) 
                + LEFT(isnull(STMTPF.CHPFX, ' ') + REPLICATE(' ', 2), 2) 
                + LEFT(isnull(STMTPF.CHCOY, ' ') + REPLICATE(' ', 1), 1) 
                + LEFT(isnull(STMTPF.CHNUM, ' ') + REPLICATE(' ', 9), 9) 
                + LEFT(isnull(CAST(STMTPF.UNIQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                + LEFT(isnull(STMTPF.RECNC, ' ') + REPLICATE(' ', 2), 2) 
                + LEFT(isnull(STMTPF.DCODE, ' ') + REPLICATE(' ', 2), 2) 
                + LEFT(isnull(STMTPF.SDCRA, ' ') + REPLICATE(' ', 4), 4) 
                + LEFT(isnull(STMTPF.SDCRB, ' ') + REPLICATE(' ', 8), 8) 
                + LEFT(isnull(STMTPF.SDCRC, ' ') + REPLICATE(' ', 50), 50) 
                + LEFT(isnull(STMTPF.SDCRD, ' ') + REPLICATE(' ', 50), 50)               
    + LEFT(isnull(STMTPF.KEYSP, ' ') + REPLICATE(' ', 1), 1) 
                + LEFT(isnull(STMTPF.CNTYP, ' ') + REPLICATE(' ', 3), 3) 
                + LEFT(isnull(CAST(STMTPF.TRNDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) 
                + LEFT(isnull(CAST(STMTPF.EFFDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9)                
                + LEFT(isnull(STMTPF.ENDTP, ' ') + REPLICATE(' ', 1), 1) 
                 + LEFT(isnull(STMTPF.CREQT, ' ') + REPLICATE(' ', 2), 2) 
                 + LEFT(isnull(STMTPF.ORDTF, ' ') + REPLICATE(' ', 10), 10)              
     + LEFT(isnull(STMTPF.TRCDE, ' ') + REPLICATE(' ', 4), 4) 
                 + LEFT(isnull(STMTPF.SACCT, ' ') + REPLICATE(' ', 2), 2) 
                 + LEFT(isnull(STMTPF.SAC01, ' ') + REPLICATE(' ', 2), 2) 
                 + LEFT(isnull(STMTPF.SAC02, ' ') + REPLICATE(' ', 2), 2) 
                 + LEFT(isnull(STMTPF.SAC03, ' ') + REPLICATE(' ', 2), 2) 
                 + LEFT(isnull(STMTPF.SAC04, ' ') + REPLICATE(' ', 2), 2) 
                 + LEFT(isnull(STMTPF.SAC05, ' ') + REPLICATE(' ', 2), 2) 
                 + LEFT(isnull(STMTPF.SAC06, ' ') + REPLICATE(' ', 2), 2) 
                 + LEFT(isnull(STMTPF.SAC07, ' ') + REPLICATE(' ', 2), 2) 
                 + LEFT(isnull(STMTPF.SAC08, ' ') + REPLICATE(' ', 2), 2) 
                 + LEFT(isnull(STMTPF.SAC09, ' ') + REPLICATE(' ', 2), 2) 
                 + LEFT(isnull(STMTPF.SAC10, ' ') + REPLICATE(' ', 2), 2) 
                 + LEFT(isnull(STMTPF.SAC11, ' ') + REPLICATE(' ', 2), 2) 
                 + LEFT(isnull(STMTPF.SAC12, ' ') + REPLICATE(' ', 2), 2) 
                 + LEFT(isnull(STMTPF.SAC13, ' ') + REPLICATE(' ', 2), 2) 
                 + LEFT(isnull(STMTPF.SAC14, ' ') + REPLICATE(' ', 2), 2) 
                 + LEFT(isnull(STMTPF.SAC15, ' ') + REPLICATE(' ', 2), 2)     
                 + LEFT(isnull(STMTPF.JNLFG, ' ') + REPLICATE(' ', 1), 1) 
     + LEFT(isnull(STMTPF.JOBID, ' ') + REPLICATE(' ', 8), 8) 
     + LEFT(isnull(STMTPF.BTPFX, ' ') + REPLICATE(' ', 2), 2) 
                 + LEFT(isnull(STMTPF.BTCOY, ' ') + REPLICATE(' ', 1), 1) 
                 + LEFT(isnull(STMTPF.BTBRN, ' ') + REPLICATE(' ', 2), 2) 
                 + LEFT(isnull(CAST(STMTPF.BTACY AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) 
                 + LEFT(isnull(CAST(STMTPF.BTACM AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) 
                 + LEFT(isnull(STMTPF.BTTCD, ' ') + REPLICATE(' ', 4), 4) 
                 + LEFT(isnull(STMTPF.BTBCH, ' ') + REPLICATE(' ', 5), 5) 
                 + LEFT(isnull(STMTPF.DUEDT, ' ') + REPLICATE(' ', 6), 6) 
                 + LEFT(isnull(STMTPF.AGECD, ' ') + REPLICATE(' ', 1), 1) 
                 + LEFT(isnull(STMTPF.RSKCL, ' ') + REPLICATE(' ', 3), 3) 
                 + LEFT(isnull(CAST(STMTPF.RSKNR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) 
                 + LEFT(isnull(STMTPF.BRKRF, ' ') + REPLICATE(' ', 8), 8)     
     + LEFT(Isnull(stmtpf.OTPFX, ' ') + Replicate(' ', 2), 2)  
                 + LEFT(isnull(STMTPF.OTCOY, ' ') + REPLICATE(' ', 1), 1) 
                 + LEFT(isnull(STMTPF.OTNUM, ' ') + REPLICATE(' ', 14), 14) 
                 + LEFT(isnull(STMTPF.OTCCY, ' ') + REPLICATE(' ', 3), 3)   
     + LEFT(isnull(CAST(STMTPF.AMT01 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.AMT02 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.AMT03 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.AMT04 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.AMT05 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.AMT06 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.AMT07 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.AMT08 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.AMT09 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.AMT10 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.AMT11 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.AMT12 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.AMT13 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.AMT14 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.AMT15 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.LCE01 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.LCE02 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.LCE03 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.LCE04 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.LCE05 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.LCE06 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.LCE07 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.LCE08 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.LCE09 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.LCE10 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.LCE11 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.LCE12 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.LCE13 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.LCE14 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.LCE15 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.GPAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19)  
                 + LEFT(Isnull(Cast(stmtpf.CMAMT AS VARCHAR(max)), ' ') + Replicate(' ', 19), 19) 
                 + LEFT(Isnull(Cast(stmtpf.NTAMT AS VARCHAR(max)), ' ') + Replicate(' ', 19), 19)  
                 + LEFT(isnull(CAST(STMTPF.GRSLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.CMLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(CAST(STMTPF.NTLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(STMTPF.MYFLG, ' ') + REPLICATE(' ', 1), 1) 
                 + LEFT(isnull(CAST(STMTPF.MYUQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) 
                 + LEFT(isnull(STMTPF.ORCCY, ' ') + REPLICATE(' ', 3), 3) 
                 + LEFT(isnull(STMTPF.LCCCY, ' ') + REPLICATE(' ', 3), 3) 
                 + LEFT(isnull(CAST(STMTPF.CRATE AS VARCHAR(max)), ' ')          + REPLICATE(' ', 20), 20) 
                 + LEFT(isnull(STMTPF.STCA, ' ')  + REPLICATE(' ', 3), 3) 
                 + LEFT(isnull(STMTPF.STCB, ' ') + REPLICATE(' ', 3), 3) 
                 + LEFT(isnull(STMTPF.STCC, ' ') + REPLICATE(' ', 3), 3)     
                 + LEFT(isnull(STMTPF.STCD, ' ') + REPLICATE(' ', 3), 3) 
                 + LEFT(isnull(STMTPF.STCE, ' ') + REPLICATE(' ', 3), 3) 
                 + LEFT(isnull(CAST(STMTPF.MARYACTYR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) 
                 + LEFT(isnull(CAST(STMTPF.MARYACTMN AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) 
                 + LEFT(isnull(STMTPF.RITYPE, ' ') + REPLICATE(' ', 2), 2)      
                 + LEFT(isnull(STMTPF.USRPRF, ' ') + REPLICATE(' ', 10), 10) 
                 + LEFT(isnull(STMTPF.JOBNM, ' ') + REPLICATE(' ', 10), 10) 
                 + CONVERT(VARCHAR, DATIME, 121)         
                 + LEFT(isnull(CAST(STMTPF.TRANNO AS VARCHAR(max)), ' ') + REPLICATE(' ', 6), 6)
                 + LEFT(isnull(STMTPF.ZCOMMSTAT, ' ') + REPLICATE(' ', 1), 1)      
				 + LEFT(isnull(STMTPF.OTHPFX, ' ') + REPLICATE(' ', 2), 2) 
				 + LEFT(isnull(STMTPF.OTHCOY, ' ') + REPLICATE(' ', 1), 1) 
				 + LEFT(isnull(STMTPF.OTHNUM, ' ') + REPLICATE(' ', 14), 14)
                 + LEFT(isnull(STMTPF.ZCOMMREF, ' ') + REPLICATE(' ', 9), 9)  AS NONKEYS
FROM VM1DTA.STMTPF;

GO




ALTER VIEW [VM1DTA].[SUSQ_UCSHREC] (
	UNIQUE_NUMBER
	,ACPFX
	,ACCOY
	,ACNUM
	,UNIQK
	,RECORDFORMAT
	,NONKEYS
	)
AS
SELECT UCSHPF.UNIQUE_NUMBER
	,UCSHPF.ACPFX
	,UCSHPF.ACCOY
	,UCSHPF.ACNUM
	,UCSHPF.UNIQK
	,'UCSHREC' AS RECORDFORMAT
	    ,  LEFT(ISNULL(CAST(UCSHPF.TRNID AS VARCHAR(MAX)), ' ') + REPLICATE(' ',19), 19)       
		+ LEFT(ISNULL(UCSHPF.VFLAG, ' ') + REPLICATE(' ', 1), 1)
        + LEFT(ISNULL(UCSHPF.ACPFX, ' ') + REPLICATE(' ', 2), 2)
        + LEFT(ISNULL(UCSHPF.ACCOY, ' ') + REPLICATE(' ', 1), 1)
        + LEFT(ISNULL(UCSHPF.ACNUM, ' ') + REPLICATE(' ', 8), 8)
        + LEFT(ISNULL(UCSHPF.PLPFX, ' ') + REPLICATE(' ', 2), 2)
        + LEFT(ISNULL(UCSHPF.PLCOY, ' ') + REPLICATE(' ', 1), 1)
        + LEFT(ISNULL(UCSHPF.PLNUM, ' ') + REPLICATE(' ', 8), 8)
        + LEFT(ISNULL(UCSHPF.CLPFX, ' ') + REPLICATE(' ', 2), 2)
        + LEFT(ISNULL(UCSHPF.CLCOY, ' ') + REPLICATE(' ', 1), 1)
        + LEFT(ISNULL(UCSHPF.CLNUM, ' ') + REPLICATE(' ', 8), 8)
        + LEFT(ISNULL(UCSHPF.CHPFX, ' ') + REPLICATE(' ', 2), 2)
        + LEFT(ISNULL(UCSHPF.CHCOY, ' ') + REPLICATE(' ', 1), 1)
        + LEFT(ISNULL(UCSHPF.CHNUM, ' ') + REPLICATE(' ', 9), 9)
        + LEFT(ISNULL(CAST(UCSHPF.UNIQK AS VARCHAR(MAX)), ' ') + REPLICATE(' ',19), 19)
        + LEFT(ISNULL(UCSHPF.RECNC, ' ') + REPLICATE(' ', 2), 2)
        + LEFT(ISNULL(UCSHPF.DCODE, ' ') + REPLICATE(' ', 2), 2)
        + LEFT(ISNULL(UCSHPF.SDCRA, ' ') + REPLICATE(' ', 4), 4)
        + LEFT(ISNULL(UCSHPF.SDCRB, ' ') + REPLICATE(' ', 8), 8)
        + LEFT(ISNULL(UCSHPF.SDCRC, ' ') + REPLICATE(' ', 50), 50)
        + LEFT(ISNULL(UCSHPF.SDCRD, ' ') + REPLICATE(' ', 50), 50)
        + LEFT(ISNULL(UCSHPF.KEYSP, ' ') + REPLICATE(' ', 1), 1)
		+ LEFT(ISNULL(UCSHPF.CNTYP, ' ') + REPLICATE(' ', 3), 3)
        + LEFT(ISNULL(CAST(UCSHPF.TRNDT AS VARCHAR(MAX)), ' ') + REPLICATE(' ',9), 9)
        + LEFT(ISNULL(CAST(UCSHPF.EFFDT AS VARCHAR(MAX)), ' ') + REPLICATE(' ',9), 9)
        + LEFT(ISNULL(UCSHPF.ENDTP, ' ') + REPLICATE(' ', 1), 1)
        + LEFT(ISNULL(UCSHPF.CREQT, ' ') + REPLICATE(' ', 2), 2)
        + LEFT(ISNULL(UCSHPF.ORDTF, ' ') + REPLICATE(' ', 10), 10)        
		+ LEFT(ISNULL(UCSHPF.TRCDE, ' ') + REPLICATE(' ', 4), 4)
        + LEFT(ISNULL(UCSHPF.SACCT, ' ') + REPLICATE(' ', 2), 2)
        + LEFT(ISNULL(UCSHPF.SAC01, ' ') + REPLICATE(' ', 2), 2)
        + LEFT(ISNULL(UCSHPF.SAC02, ' ') + REPLICATE(' ', 2), 2)
        + LEFT(ISNULL(UCSHPF.SAC03, ' ') + REPLICATE(' ', 2), 2)
        + LEFT(ISNULL(UCSHPF.SAC04, ' ') + REPLICATE(' ', 2), 2)
        + LEFT(ISNULL(UCSHPF.SAC05, ' ') + REPLICATE(' ', 2), 2)
        + LEFT(ISNULL(UCSHPF.SAC06, ' ') + REPLICATE(' ', 2), 2)
        + LEFT(ISNULL(UCSHPF.SAC07, ' ') + REPLICATE(' ', 2), 2)
        + LEFT(ISNULL(UCSHPF.SAC08, ' ') + REPLICATE(' ', 2), 2)
        + LEFT(ISNULL(UCSHPF.SAC09, ' ') + REPLICATE(' ', 2), 2)
        + LEFT(ISNULL(UCSHPF.SAC10, ' ') + REPLICATE(' ', 2), 2)
        + LEFT(ISNULL(UCSHPF.SAC11, ' ') + REPLICATE(' ', 2), 2)
        + LEFT(ISNULL(UCSHPF.SAC12, ' ') + REPLICATE(' ', 2), 2)
        + LEFT(ISNULL(UCSHPF.SAC13, ' ') + REPLICATE(' ', 2), 2)
        + LEFT(ISNULL(UCSHPF.SAC14, ' ') + REPLICATE(' ', 2), 2)
        + LEFT(ISNULL(UCSHPF.SAC15, ' ') + REPLICATE(' ', 2), 2)
		+ LEFT(ISNULL(UCSHPF.JNLFG, ' ') + REPLICATE(' ', 1), 1)        
		+ LEFT(ISNULL(UCSHPF.JOBID, ' ') + REPLICATE(' ', 8), 8)
     	+ LEFT(ISNULL(UCSHPF.BTPFX, ' ') + REPLICATE(' ', 2), 2)
        + LEFT(ISNULL(UCSHPF.BTCOY, ' ') + REPLICATE(' ', 1), 1)
        + LEFT(ISNULL(UCSHPF.BTBRN, ' ') + REPLICATE(' ', 2), 2)
        + LEFT(ISNULL(CAST(UCSHPF.BTACY AS VARCHAR(MAX)), ' ') + REPLICATE(' ',5), 5)
        + LEFT(ISNULL(CAST(UCSHPF.BTACM AS VARCHAR(MAX)), ' ') + REPLICATE(' ',3), 3)
        + LEFT(ISNULL(UCSHPF.BTTCD, ' ') + REPLICATE(' ', 4), 4)
        + LEFT(ISNULL(UCSHPF.BTBCH, ' ') + REPLICATE(' ', 5), 5)
        + LEFT(ISNULL(UCSHPF.DUEDT, ' ') + REPLICATE(' ', 6), 6)
        + LEFT(ISNULL(UCSHPF.AGECD, ' ') + REPLICATE(' ', 1), 1)
        + LEFT(ISNULL(UCSHPF.RSKCL, ' ') + REPLICATE(' ', 3), 3)
        + LEFT(ISNULL(CAST(UCSHPF.RSKNR AS VARCHAR(MAX)), ' ') + REPLICATE(' ',5), 5)
        + LEFT(ISNULL(UCSHPF.BRKRF, ' ') + REPLICATE(' ', 8), 8)        
        + LEFT(ISNULL(UCSHPF.OTPFX, ' ') + REPLICATE(' ', 2), 2)
        + LEFT(ISNULL(UCSHPF.OTCOY, ' ') + REPLICATE(' ', 1), 1)
        + LEFT(ISNULL(UCSHPF.OTNUM, ' ') + REPLICATE(' ', 14), 14)
        + LEFT(ISNULL(UCSHPF.OTCCY, ' ') + REPLICATE(' ', 3), 3)
		+ LEFT(ISNULL(CAST(UCSHPF.ORCSH AS VARCHAR(MAX)), ' ') + REPLICATE(' ',19), 19)
        + LEFT(ISNULL(CAST(UCSHPF.OCLCE AS VARCHAR(MAX)), ' ') + REPLICATE(' ',19), 19)
        + LEFT(ISNULL(CAST(UCSHPF.NTAMT AS VARCHAR(MAX)), ' ') + REPLICATE(' ',19), 19)
        + LEFT(ISNULL(CAST(UCSHPF.NTLCE AS VARCHAR(MAX)), ' ') + REPLICATE(' ',19), 19)
        + LEFT(ISNULL(CAST(UCSHPF.CSHMT AS VARCHAR(MAX)), ' ') + REPLICATE(' ',19), 19)
        + LEFT(ISNULL(CAST(UCSHPF.CSLCE AS VARCHAR(MAX)), ' ') + REPLICATE(' ',19), 19)
        + LEFT(ISNULL(CAST(UCSHPF.CSHDT AS VARCHAR(MAX)), ' ') + REPLICATE(' ',9), 9)
        + LEFT(ISNULL(CAST(UCSHPF.RTOLN AS VARCHAR(MAX)), ' ') + REPLICATE(' ',19), 19)
        + LEFT(ISNULL(CAST(UCSHPF.RTLCE AS VARCHAR(MAX)), ' ') + REPLICATE(' ',19), 19)
        + LEFT(ISNULL(CAST(UCSHPF.MYUQK AS VARCHAR(MAX)), ' ') + REPLICATE(' ',19), 19)
        + LEFT(ISNULL(UCSHPF.ORCCY, ' ') + REPLICATE(' ', 3), 3)
        + LEFT(ISNULL(UCSHPF.LCCCY, ' ') + REPLICATE(' ', 3), 3)
        + LEFT(ISNULL(CAST(UCSHPF.CRATE AS VARCHAR(MAX)), ' ') + REPLICATE(' ',20), 20)
        + LEFT(ISNULL(UCSHPF.STCA, ' ') + REPLICATE(' ', 3), 3)
        + LEFT(ISNULL(UCSHPF.STCB, ' ') + REPLICATE(' ', 3), 3)
        + LEFT(ISNULL(UCSHPF.STCC, ' ') + REPLICATE(' ', 3), 3)
        + LEFT(ISNULL(UCSHPF.STCD, ' ') + REPLICATE(' ', 3), 3)
        + LEFT(ISNULL(UCSHPF.STCE, ' ') + REPLICATE(' ', 3), 3)
        + LEFT(ISNULL(CAST(UCSHPF.MARYACTYR AS VARCHAR(MAX)), ' ') + REPLICATE(' ', 5), 5)
        + LEFT(ISNULL(CAST(UCSHPF.MARYACTMN AS VARCHAR(MAX)), ' ') + REPLICATE(' ', 3), 3) 
        + LEFT(ISNULL(UCSHPF.USRPRF, ' ') + REPLICATE(' ', 10), 10)
        + LEFT(ISNULL(UCSHPF.JOBNM, ' ')  + REPLICATE(' ', 10), 10)
        + CONVERT(VARCHAR, DATIME, 121) 		        
 		+ LEFT(ISNULL(UCSHPF.TRMID, ' ') + REPLICATE(' ', 4), 4)
		+ LEFT(isnull(UCSHPF.OTHPFX, ' ') + REPLICATE(' ', 2), 2) 
		+ LEFT(isnull(UCSHPF.OTHCOY, ' ') + REPLICATE(' ', 1), 1) 
		+ LEFT(isnull(UCSHPF.OTHNUM, ' ') + REPLICATE(' ', 14), 14)
        + LEFT(ISNULL(UCSHPF.MYFLG, ' ') + REPLICATE(' ', 1), 1) AS NONKEYS
FROM VM1DTA.UCSHPF


GO



ALTER VIEW [VM1DTA].[UCDN](UNIQUE_NUMBER, TRNID, TRMID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, JOBID, OTPFX, OTCOY, OTNUM, OTCCY, ORCSH, OCLCE, NTAMT, NTLCE, CSHMT, CSLCE, CSHDT, RTOLN, RTLCE, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, MARYACTYR, MARYACTMN, MYFLG, JOBNM, USRPRF, DATIME, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            TRMID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            JOBID,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            ORCSH,
            OCLCE,
            NTAMT,
            NTLCE,
            CSHMT,
            CSLCE,
            CSHDT,
            RTOLN,
            RTLCE,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            MARYACTYR,
            MARYACTMN,
            MYFLG,
            JOBNM,
            USRPRF,
            DATIME,
			OTHPFX,
			OTHCOY,
			OTHNUM
       FROM VM1DTA.UCSHPF
      WHERE OTPFX = 'GI'

GO



ALTER VIEW [VM1DTA].[UCPL](UNIQUE_NUMBER, TRNID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, JOBID, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, OTPFX, OTCOY, OTNUM, OTCCY, ORCSH, OCLCE, NTAMT, NTLCE, CSHMT, CSLCE, CSHDT, RTOLN, RTLCE, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, USRPRF, JOBNM, DATIME, TRMID, MARYACTYR, MARYACTMN, MYFLG, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            JOBID,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            ORCSH,
            OCLCE,
            NTAMT,
            NTLCE,
            CSHMT,
            CSLCE,
            CSHDT,
            RTOLN,
            RTLCE,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            USRPRF,
            JOBNM,
            DATIME,
            TRMID,
            MARYACTYR,
            MARYACTMN,
            MYFLG,
			OTHPFX,
			OTHCOY,
			OTHNUM
   
       FROM VM1DTA.UCSHPF

GO




ALTER VIEW [VM1DTA].[UCSH](UNIQUE_NUMBER, TRNID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, JOBID, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, OTPFX, OTCOY, OTNUM, OTCCY, ORCSH, OCLCE, NTAMT, NTLCE, CSHMT, CSLCE, CSHDT, RTOLN, RTLCE, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, MARYACTYR, MARYACTMN, USRPRF, JOBNM, DATIME, TRMID, MYFLG, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            JOBID,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            ORCSH,
            OCLCE,
            NTAMT,
            NTLCE,
            CSHMT,
            CSLCE,
            CSHDT,
            RTOLN,
            RTLCE,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            MARYACTYR,
            MARYACTMN,
            USRPRF,
            JOBNM,
            DATIME,
            TRMID,
            MYFLG,
			OTHPFX,
			OTHCOY,
			OTHNUM

   
       FROM VM1DTA.UCSHPF

GO


ALTER VIEW [VM1DTA].[UCSHCHQ](UNIQUE_NUMBER, TRNID, TRMID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, JOBID, OTPFX, OTCOY, OTNUM, OTCCY, ORCSH, OCLCE, NTAMT, NTLCE, CSHMT, CSLCE, CSHDT, RTOLN, RTLCE, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, MARYACTYR, MARYACTMN, MYFLG, JOBNM, USRPRF, DATIME, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            TRMID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            JOBID,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            ORCSH,
            OCLCE,
            NTAMT,
            NTLCE,
            CSHMT,
            CSLCE,
            CSHDT,
            RTOLN,
            RTLCE,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            MARYACTYR,
            MARYACTMN,
            MYFLG,
            JOBNM,
            USRPRF,
            DATIME,
			OTHPFX,
			OTHCOY,
			OTHNUM

   
       FROM VM1DTA.UCSHPF

GO


ALTER VIEW [VM1DTA].[UCSHOTH](UNIQUE_NUMBER, TRNID, TRMID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, JOBID, OTPFX, OTCOY, OTNUM, OTCCY, ORCSH, OCLCE, NTAMT, NTLCE, CSHMT, CSLCE, CSHDT, RTOLN, RTLCE, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, MARYACTYR, MARYACTMN, MYFLG, JOBNM, USRPRF, DATIME, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            TRMID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            JOBID,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            ORCSH,
            OCLCE,
            NTAMT,
            NTLCE,
            CSHMT,
            CSLCE,
            CSHDT,
            RTOLN,
            RTLCE,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            MARYACTYR,
            MARYACTMN,
            MYFLG,
            JOBNM,
            USRPRF,
            DATIME,
			OTHPFX,
			OTHCOY,
			OTHNUM

       FROM VM1DTA.UCSHPF
      WHERE OTPFX = 'CV'

GO



ALTER VIEW [VM1DTA].[STMTCLM] (
	UNIQUE_NUMBER
	,ACCOY
	,ACNUM
	,PLPFX
	,PLCOY
	,PLNUM
	,RECORDFORMAT
	,NONKEYS
	)
AS
SELECT A.UNIQUE_NUMBER
	,A.ACCOY
	,A.ACNUM
	,A.PLPFX
	,A.PLCOY
	,A.PLNUM
	,A.RECORDFORMAT
	,LEFT(ISNULL(A.NONKEYS, ' ') + REPLICATE(' ', 1169), 1169) AS NONKEYS
FROM (
	SELECT STMTCLM_STMTREC.UNIQUE_NUMBER
		,STMTCLM_STMTREC.ACCOY
		,STMTCLM_STMTREC.ACNUM
		,STMTCLM_STMTREC.PLPFX
		,STMTCLM_STMTREC.PLCOY
		,STMTCLM_STMTREC.PLNUM
		,STMTCLM_STMTREC.RECORDFORMAT
		,STMTCLM_STMTREC.NONKEYS
	FROM VM1DTA.STMTCLM_STMTREC
	
	UNION ALL
	
	SELECT STMTCLM_UCSHREC.UNIQUE_NUMBER
		,STMTCLM_UCSHREC.ACCOY
		,STMTCLM_UCSHREC.ACNUM
		,STMTCLM_UCSHREC.PLPFX
		,STMTCLM_UCSHREC.PLCOY
		,STMTCLM_UCSHREC.PLNUM
		,STMTCLM_UCSHREC.RECORDFORMAT
		,STMTCLM_UCSHREC.NONKEYS
	FROM VM1DTA.STMTCLM_UCSHREC
	) AS A;

GO



ALTER VIEW [VM1DTA].[STMTCLM_STMTREC] (
	UNIQUE_NUMBER
	,ACCOY
	,ACNUM
	,PLPFX
	,PLCOY
	,PLNUM
	,RECORDFORMAT
	,NONKEYS
	)
AS
SELECT STMTPF.UNIQUE_NUMBER
	,STMTPF.ACCOY
	,STMTPF.ACNUM
	,STMTPF.PLPFX
	,STMTPF.PLCOY
	,STMTPF.PLNUM
	,'STMTREC' AS RECORDFORMAT
	,LEFT(isnull(CAST(STMTPF.TRNID AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.VFLAG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.ACPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.ACCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.ACNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.PLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.PLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.PLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.CLPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.CLCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CLNUM, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.CHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.CHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CHNUM, ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(STMTPF.UNIQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.RECNC, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.DCODE, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SDCRA, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(STMTPF.SDCRB, ' ') + REPLICATE(' ', 8), 8) + 
	LEFT(isnull(STMTPF.SDCRC, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(STMTPF.SDCRD, ' ') + REPLICATE(' ', 50), 50) + LEFT(isnull(STMTPF.KEYSP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CNTYP, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.TRNDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(CAST(STMTPF.EFFDT AS VARCHAR(max)), ' ') + REPLICATE(' ', 9), 9) + LEFT(isnull(STMTPF.ENDTP, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.CREQT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.ORDTF, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(STMTPF.TRCDE, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(STMTPF.SACCT, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC01, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC02, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC03, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC04, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC05, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC06, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC07, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC08, ' ') + REPLICATE(' ', 2), 2) 
	+ LEFT(isnull(STMTPF.SAC09, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC10, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC11, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC12, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC13, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC14, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.SAC15, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.JNLFG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.JOBID, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.BTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.BTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.BTBRN, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(CAST(STMTPF.BTACY AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(STMTPF.BTACM AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.BTTCD, ' ') + REPLICATE(' ', 4), 4) + LEFT(isnull(STMTPF.BTBCH, ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(STMTPF.DUEDT, ' ') + REPLICATE(' ', 6), 6) + LEFT(isnull(STMTPF.AGECD, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.RSKCL, ' ') + REPLICATE(' ', 3), 3) + LEFT
	(isnull(CAST(STMTPF.RSKNR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(STMTPF.BRKRF, ' ') + REPLICATE(' ', 8), 8) + LEFT(isnull(STMTPF.OTPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.OTCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.OTNUM, ' ') + REPLICATE(' ', 14), 14) + LEFT(isnull(STMTPF.OTCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.AMT01 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT02 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT03 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT04 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT05 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT06 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT07 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT08 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT09 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT10 
				AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT11 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT12 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT13 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT14 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.AMT15 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE01 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE02 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE03 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE04 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE05 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE06 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE07 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE08 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT
	(isnull(CAST(STMTPF.LCE09 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE10 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE11 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE12 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE13 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE14 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.LCE15 AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.GPAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.CMAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.NTAMT AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.GRSLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.CMLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(CAST(STMTPF.NTLCE AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.MYFLG, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(
			CAST(STMTPF.MYUQK AS VARCHAR(max)), ' ') + REPLICATE(' ', 19), 19) + LEFT(isnull(STMTPF.ORCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.LCCCY, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.CRATE AS VARCHAR(max)), ' ') + REPLICATE(' ', 20), 20) + LEFT(isnull(STMTPF.STCA, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCB, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCC, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCD, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.STCE, ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(CAST(STMTPF.MARYACTYR AS VARCHAR(max)), ' ') + REPLICATE(' ', 5), 5) + LEFT(isnull(CAST(STMTPF.MARYACTMN AS VARCHAR(max)), ' ') + REPLICATE(' ', 3), 3) + LEFT(isnull(STMTPF.RITYPE, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(CAST(STMTPF.TRANNO AS VARCHAR(max)), ' ') + REPLICATE(' ', 6), 6) + LEFT(isnull(STMTPF.JOBNM, ' ') + REPLICATE(' ', 10), 10) + LEFT(isnull(STMTPF.OTHPFX, ' ') + REPLICATE(' ', 2), 2) + LEFT(isnull(STMTPF.OTHCOY, ' ') + REPLICATE(' ', 1), 1) + LEFT(isnull(STMTPF.OTHNUM, ' ') + REPLICATE(' ', 14), 14) + LEFT(isnull(STMTPF.USRPRF, ' ') + REPLICATE(' ', 10), 10) + CONVERT(VARCHAR, DATIME, 121) AS NONKEYS
FROM VM1DTA.STMTPF
WHERE MYFLG = ' '
	AND PLPFX = 'CL'
	OR MYFLG = ' '
	AND PLPFX = 'CL';

GO



ALTER VIEW [VM1DTA].[stmtclm_ucshrec] ( unique_number, accoy, acnum, plpfx, plcoy, plnum, recordformat, nonkeys )
AS 
  SELECT ucshpf.unique_number, 
         ucshpf.accoy, 
         ucshpf.acnum, 
         ucshpf.plpfx, 
         ucshpf.plcoy, 
         ucshpf.plnum, 
         'UCSHREC'                       AS RECORDFORMAT, 
         LEFT(Isnull(Cast(ucshpf.trnid AS VARCHAR(max)), ' ') + Replicate(' ', 19), 19) 
         + LEFT(Isnull(ucshpf.trmid, ' ') + Replicate(' ', 4), 4) 
         + LEFT(Isnull(ucshpf.vflag, ' ') + Replicate(' ', 1), 1) 
         + LEFT(Isnull(ucshpf.acpfx, ' ') + Replicate(' ', 2), 2) 
         + LEFT(Isnull(ucshpf.accoy, ' ') + Replicate(' ', 1), 1) 
         + LEFT(Isnull(ucshpf.acnum, ' ') + Replicate(' ', 8), 8) 
         + LEFT(Isnull(ucshpf.plpfx, ' ') + Replicate(' ', 2), 2) 
         + LEFT(Isnull(ucshpf.plcoy, ' ') + Replicate(' ', 1), 1) 
         + LEFT(Isnull(ucshpf.plnum, ' ') + Replicate(' ', 8), 8) 
         + LEFT(Isnull(ucshpf.clpfx, ' ') + Replicate(' ', 2), 2) 
         + LEFT(Isnull(ucshpf.clcoy, ' ') + Replicate(' ', 1), 1) 
         + LEFT(Isnull(ucshpf.clnum, ' ') + Replicate(' ', 8), 8) 
         + LEFT(Isnull(ucshpf.chpfx, ' ') + Replicate(' ', 2), 2) 
         + LEFT(Isnull(ucshpf.chcoy, ' ') + Replicate(' ', 1), 1) 
         + LEFT(Isnull(ucshpf.chnum, ' ') + Replicate(' ', 9), 9) 
         + LEFT(Isnull(Cast(ucshpf.uniqk AS VARCHAR(max)), ' ') + Replicate(' ', 19), 19) 
         + LEFT(Isnull(ucshpf.recnc, ' ') + Replicate(' ', 2), 2) 
         + LEFT(Isnull(ucshpf.dcode, ' ') + Replicate(' ', 2), 2) 
         + LEFT(Isnull(ucshpf.sdcra, ' ') + Replicate(' ', 4), 4) 
         + LEFT(Isnull(ucshpf.sdcrb, ' ') + Replicate(' ', 8), 8) 
         + LEFT(Isnull(ucshpf.sdcrc, ' ') + Replicate(' ', 50), 50) 
         + LEFT(Isnull(ucshpf.sdcrd, ' ') + Replicate(' ', 50), 50) 
         + LEFT(Isnull(ucshpf.keysp, ' ') + Replicate(' ', 1), 1) 
         + LEFT(Isnull(ucshpf.cntyp, ' ') + Replicate(' ', 3), 3) 
         + LEFT(Isnull(Cast(ucshpf.trndt AS VARCHAR(max)), ' ') + Replicate(' ', 9), 9) 
         + LEFT(Isnull(Cast(ucshpf.effdt AS VARCHAR(max)), ' ') + Replicate(' ', 9), 9) 
         + LEFT(Isnull(ucshpf.endtp, ' ') + Replicate(' ', 1), 1) 
         + LEFT(Isnull(ucshpf.creqt, ' ') + Replicate(' ', 2), 2) 
         + LEFT(Isnull(ucshpf.ordtf, ' ') + Replicate(' ', 10), 10) 
         + LEFT(Isnull(ucshpf.trcde, ' ') + Replicate(' ', 4), 4) 
         + LEFT(Isnull(ucshpf.sacct, ' ') + Replicate(' ', 2), 2) 
         + LEFT(Isnull(ucshpf.sac01, ' ') + Replicate(' ', 2), 2) 
         + LEFT(Isnull(ucshpf.sac02, ' ') + Replicate(' ', 2), 2) 
         + LEFT(Isnull(ucshpf.sac03, ' ') + Replicate(' ', 2), 2) 
         + LEFT(Isnull(ucshpf.sac04, ' ') + Replicate(' ', 2), 2) 
         + LEFT(Isnull(ucshpf.sac05, ' ') + Replicate(' ', 2), 2) 
         + LEFT(Isnull(ucshpf.sac06, ' ') + Replicate(' ', 2), 2) 
         + LEFT(Isnull(ucshpf.sac07, ' ') + Replicate(' ', 2), 2) 
         + LEFT(Isnull(ucshpf.sac08, ' ') + Replicate(' ', 2), 2) 
         + LEFT(Isnull(ucshpf.sac09, ' ') + Replicate(' ', 2), 2) 
         + LEFT(Isnull(ucshpf.sac10, ' ') + Replicate(' ', 2), 2) 
         + LEFT(Isnull(ucshpf.sac11, ' ') + Replicate(' ', 2), 2) 
         + LEFT(Isnull(ucshpf.sac12, ' ') + Replicate(' ', 2), 2) 
         + LEFT(Isnull(ucshpf.sac13, ' ') + Replicate(' ', 2), 2) 
         + LEFT(Isnull(ucshpf.sac14, ' ') + Replicate(' ', 2), 2) 
         + LEFT(Isnull(ucshpf.sac15, ' ') + Replicate(' ', 2), 2) 
         + LEFT(Isnull(ucshpf.jnlfg, ' ') + Replicate(' ', 1), 1) 
         + LEFT(Isnull(ucshpf.btpfx, ' ') + Replicate(' ', 2), 2) 
         + LEFT(Isnull(ucshpf.btcoy, ' ') + Replicate(' ', 1), 1) 
         + LEFT(Isnull(ucshpf.btbrn, ' ') + Replicate(' ', 2), 2) 
         + LEFT(Isnull(Cast(ucshpf.btacy AS VARCHAR(max)), ' ') + Replicate(' ', 5), 5) 
         + LEFT(Isnull(Cast(ucshpf.btacm AS VARCHAR(max)), ' ') + Replicate(' ', 3), 3) 
         + LEFT(Isnull(ucshpf.bttcd, ' ') + Replicate(' ', 4), 4) 
         + LEFT(Isnull(ucshpf.btbch, ' ') + Replicate(' ', 5), 5) 
         + LEFT(Isnull(ucshpf.duedt, ' ') + Replicate(' ', 6), 6) 
         + LEFT(Isnull(ucshpf.duedt, ' ') + Replicate(' ', 6), 6) 
         + LEFT(Isnull(ucshpf.agecd, ' ') + Replicate(' ', 1), 1) 
         + LEFT (Isnull(ucshpf.rskcl, ' ') + Replicate(' ', 3), 3) 
         + LEFT(Isnull(Cast(ucshpf.rsknr AS VARCHAR(max)), ' ') + Replicate(' ', 5), 5) 
         + LEFT(Isnull(ucshpf.brkrf, ' ') + Replicate(' ', 8), 8) 
         + LEFT(Isnull(ucshpf.jobid, ' ') + Replicate(' ', 8), 8) 
         + LEFT(Isnull(ucshpf.otpfx, ' ') + Replicate(' ', 2), 2) 
         + LEFT(Isnull(ucshpf.otcoy, ' ') + Replicate(' ', 1), 1) 
         + LEFT(Isnull(ucshpf.otnum, ' ') + Replicate(' ', 14), 14) 
         + LEFT(Isnull(ucshpf.otccy, ' ') + Replicate(' ', 3), 3) 
         + LEFT(Isnull(Cast(ucshpf.orcsh AS VARCHAR(max)), ' ') + Replicate(' ', 19), 19) 
         + LEFT(Isnull(Cast(ucshpf.oclce AS VARCHAR(max)), ' ') + Replicate(' ', 19), 19) 
         + LEFT(Isnull(Cast(ucshpf.ntamt AS VARCHAR(max)), ' ') + Replicate(' ', 19), 19) 
         + LEFT(Isnull(Cast(ucshpf.ntlce AS VARCHAR(max)), ' ') + Replicate(' ', 19), 19) 
         + LEFT(Isnull(Cast(ucshpf.cshmt AS VARCHAR(max)), ' ') + Replicate(' ', 19), 19) 
         + LEFT(Isnull(Cast(ucshpf.cslce AS VARCHAR(max)), ' ') + Replicate(' ', 19), 19) 
         + LEFT(Isnull(Cast(ucshpf.cshdt AS VARCHAR(max)), ' ') + Replicate(' ', 9), 9) 
         + LEFT(Isnull(Cast(ucshpf.rtoln AS VARCHAR(max)), ' ') + Replicate(' ', 19), 19) 
         + LEFT(Isnull(Cast(ucshpf.rtlce AS VARCHAR(max)), ' ') + Replicate(' ', 19), 19) 
         + LEFT(Isnull(Cast(ucshpf.myuqk AS VARCHAR(max)), ' ') + Replicate(' ', 19), 19) 
         + LEFT(Isnull(ucshpf.orccy, ' ') + Replicate(' ', 3), 3) 
         + LEFT(Isnull(ucshpf.lcccy, ' ') + Replicate(' ', 3), 3) 
         + LEFT(Isnull(Cast(ucshpf.crate AS VARCHAR(max)), ' ') + Replicate(' ', 20), 20) 
         + LEFT(Isnull(ucshpf.stca, ' ') + Replicate(' ', 3), 3) 
         + LEFT(Isnull(ucshpf.stcb, ' ') + Replicate(' ', 3), 3) 
         + LEFT(Isnull(ucshpf.stcc, ' ') + Replicate(' ', 3), 3) 
         + LEFT(Isnull(ucshpf.stcd, ' ') + Replicate(' ', 3), 3) 
         + LEFT(Isnull(ucshpf.stce, ' ') + Replicate(' ', 3), 3) 
         + LEFT(Isnull(Cast(ucshpf.maryactyr AS VARCHAR(max)), ' ') + Replicate(' ', 5), 5) 
         + LEFT(Isnull(Cast(ucshpf.maryactmn AS VARCHAR(max)), ' ') + Replicate(' ', 3), 3) 
         + LEFT(Isnull(ucshpf.myflg, ' ') + Replicate(' ', 1), 1) 
         + LEFT(Isnull(ucshpf.jobnm, ' ') + Replicate(' ', 10), 10) 
		 + LEFT(isnull(UCSHPF.OTHPFX, ' ') + REPLICATE(' ', 2), 2) 
		 + LEFT(isnull(UCSHPF.OTHCOY, ' ') + REPLICATE(' ', 1), 1) 
		 + LEFT(isnull(UCSHPF.OTHNUM, ' ') + REPLICATE(' ', 14), 14)
         + LEFT(Isnull(ucshpf.usrprf, ' ') + Replicate(' ', 10), 10) 
         + CONVERT(VARCHAR, datime, 121) AS NONKEYS 
  FROM   vm1dta.ucshpf 
  WHERE  myflg = ' ' 
         AND plpfx = 'CL' 
          OR myflg = ' ' 
             AND plpfx = ' '; 

GO


ALTER VIEW [VM1DTA].[GCHIOWN] (
	UNIQUE_NUMBER
	,COWNPFX
	,COWNCOY
	,COWNNUM
	,MANDREF
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,BANKCODE
	,BILLCHNL
	,RIMTHVCD
	,PRMRVWDT
	,JOBNM
	,USRPRF
	,DATIME
	,ZMANDREF
	,REQNTYPE
	,PAYCLT
	,ECNV
	,CVNTYPE
	,COVERNT
	)
AS
SELECT CHDRPF.UNIQUE_NUMBER
	,CHDRPF.COWNPFX
	,CHDRPF.COWNCOY
	,CHDRPF.COWNNUM
	,GCHIPF.MANDREF
	,GCHIPF.CHDRCOY
	,CHDRPF.CHDRNUM
	,GCHIPF.EFFDATE
	,GCHIPF.CCDATE
	,GCHIPF.CRDATE
	,GCHIPF.PRVBILFLG
	,GCHIPF.BILLFREQ
	,GCHIPF.GADJFREQ
	,GCHIPF.PAYRPFX
	,GCHIPF.PAYRCOY
	,GCHIPF.PAYRNUM
	,GCHIPF.AGNTPFX
	,GCHIPF.AGNTCOY
	,GCHIPF.AGNTNUM
	,GCHIPF.CNTBRANCH
	,GCHIPF.STCA
	,GCHIPF.STCB
	,GCHIPF.STCC
	,GCHIPF.STCD
	,GCHIPF.STCE
	,GCHIPF.BTDATENR
	,GCHIPF.NRISDATE
	,GCHIPF.TERMID
	,GCHIPF.USER_T
	,GCHIPF.TRDT
	,GCHIPF.TRTM
	,GCHIPF.TRANNO
	,GCHIPF.CRATE
	,GCHIPF.TERNMPRM
	,GCHIPF.SURGSCHMV
	,GCHIPF.AREACDEMV
	,GCHIPF.MEDPRVDR
	,GCHIPF.SPSMBR
	,GCHIPF.CHILDMBR
	,GCHIPF.SPSMED
	,GCHIPF.CHILDMED
	,GCHIPF.BANKCODE
	,GCHIPF.BILLCHNL
	,GCHIPF.RIMTHVCD
	,GCHIPF.PRMRVWDT
	,GCHIPF.ECNV
	,GCHIPF.CVNTYPE
	,GCHIPF.COVERNT
	,CHDRPF.JOBNM
	,CHDRPF.USRPRF
	,CHDRPF.DATIME
	,CHDRPF.ZMANDREF
	,CHDRPF.REQNTYPE
	,CHDRPF.PAYCLT
FROM VM1DTA.CHDRPF
	,GCHIPF
WHERE (CHDRPF.CHDRCOY = GCHIPF.CHDRCOY)
	AND (CHDRPF.CHDRNUM = GCHIPF.CHDRNUM);


GO


INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFSK'
           ,'Invalid Service Unit'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
GO


CREATE VIEW [VM1DTA].[GCHDCV](
	UNIQUE_NUMBER
		,CHDRCOY
		,CHDRNUM
		,CHDRPFX
		,CNTTYPE
		,COWNPFX
		,COWNCOY
		,COWNNUM
		,STATCODE
		,PNDATE
		,SUBSFLG
		,OCCDATE
		,HRSKIND
		,CNTCURR
		,BILLCURR
		,TAKOVRFLG
		,GPRNLTYP
		,GPRMNTHS
		,RNLNOTTO
		,SRCEBUS
		,COYSRVAC
		,MRKSRVAC
		,DESPPFX
		,DESPCOY
		,DESPNUM
		,POLSCHPFLG
		,BTDATE
		,ADJDATE
		,PTDATE
		,PTDATEAB
		,TRANLUSED
		,LMBRNO
		,LHEADNO
		,EFFDCLDT
		,SERVUNIT
		,PNTRCDE
		,PROCID
		,TRANID
		,TRANNO
		,VALIDFLAG
		,SPECIND
		,TAXFLAG
		,AGEDEF
		,TERMAGE
		,PERSONCOV
		,ENROLLTYP
		,SPLITSUBS
		,AVLISU
		,MPLPFX
		,MPLCOY
		,MPLNUM
		,USRPRF
		,JOBNM
		,DATIME
		,IGRASP
		,IEXPLAIN
		,IDATE
		,COVERNT
		,CNTISS
	) AS
SELECT UNIQUE_NUMBER
		,CHDRCOY
		,CHDRNUM
		,CHDRPFX
		,CNTTYPE
		,COWNPFX
		,COWNCOY
		,COWNNUM
		,STATCODE
		,PNDATE
		,SUBSFLG
		,OCCDATE
		,HRSKIND
		,CNTCURR
		,BILLCURR
		,TAKOVRFLG
		,GPRNLTYP
		,GPRMNTHS
		,RNLNOTTO
		,SRCEBUS
		,COYSRVAC
		,MRKSRVAC
		,DESPPFX
		,DESPCOY
		,DESPNUM
		,POLSCHPFLG
		,BTDATE
		,ADJDATE
		,PTDATE
		,PTDATEAB
		,TRANLUSED
		,LMBRNO
		,LHEADNO
		,EFFDCLDT
		,SERVUNIT
		,PNTRCDE
		,PROCID
		,TRANID
		,TRANNO
		,VALIDFLAG
		,SPECIND
		,TAXFLAG
		,AGEDEF
		,TERMAGE
		,PERSONCOV
		,ENROLLTYP
		,SPLITSUBS
		,AVLISU
		,MPLPFX
		,MPLCOY
		,MPLNUM
		,USRPRF
		,JOBNM
		,DATIME
		,IGRASP
		,IEXPLAIN
		,IDATE
		,COVERNT
		,CNTISS
       FROM VM1DTA.CHDRPF

GO



