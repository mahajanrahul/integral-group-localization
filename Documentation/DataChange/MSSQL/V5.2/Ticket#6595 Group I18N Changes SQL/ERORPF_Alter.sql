

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE VM1DTA.Tmp_ERORPF
	(
	UNIQUE_NUMBER bigint NOT NULL IDENTITY (1, 1),
	ERORPFX nchar(2) NULL,
	ERORCOY nchar(1) NULL,
	ERORLANG nchar(1) NULL,
	ERORPROG nchar(10) NULL,
	EROREROR nchar(6) NULL,
	ERORDESC nchar(250) NULL,
	TRDT int NULL,
	TRTM int NULL,
	USERID nchar(10) NULL,
	TERMINALID nchar(10) NULL,
	USRPRF nchar(10) NULL,
	JOBNM nchar(10) NULL,
	DATIME datetime2(6) NULL,
	ERORFILE nchar(6) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE VM1DTA.Tmp_ERORPF SET (LOCK_ESCALATION = TABLE)
GO

SET IDENTITY_INSERT VM1DTA.Tmp_ERORPF ON
GO
IF EXISTS(SELECT * FROM VM1DTA.ERORPF)
	 EXEC('INSERT INTO VM1DTA.Tmp_ERORPF (UNIQUE_NUMBER, ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE)
		SELECT UNIQUE_NUMBER, ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE FROM VM1DTA.ERORPF WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT VM1DTA.Tmp_ERORPF OFF
GO
DROP TABLE VM1DTA.ERORPF
GO
EXECUTE sp_rename N'VM1DTA.Tmp_ERORPF', N'ERORPF', 'OBJECT' 
GO
ALTER TABLE VM1DTA.ERORPF ADD CONSTRAINT
	PK_ERORPF PRIMARY KEY CLUSTERED 
	(
	UNIQUE_NUMBER
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
DECLARE @v sql_variant 
SET @v = N'VM1DTA.ERORPF.PK_ERORPF'
EXECUTE sp_addextendedproperty N'MS_SSMA_SOURCE', @v, N'SCHEMA', N'VM1DTA', N'TABLE', N'ERORPF', N'CONSTRAINT', N'PK_ERORPF'
GO
CREATE NONCLUSTERED INDEX ERON ON VM1DTA.ERORPF
	(
	ERORLANG,
	ERORDESC
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DECLARE @v sql_variant 
SET @v = N'VM1DTA.ERORPF.ERON'
EXECUTE sp_addextendedproperty N'MS_SSMA_SOURCE', @v, N'SCHEMA', N'VM1DTA', N'TABLE', N'ERORPF', N'INDEX', N'ERON'
GO
CREATE UNIQUE NONCLUSTERED INDEX EROR ON VM1DTA.ERORPF
	(
	ERORPFX,
	ERORCOY,
	ERORLANG,
	ERORPROG,
	EROREROR
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DECLARE @v sql_variant 
SET @v = N'VM1DTA.ERORPF.EROR'
EXECUTE sp_addextendedproperty N'MS_SSMA_SOURCE', @v, N'SCHEMA', N'VM1DTA', N'TABLE', N'ERORPF', N'INDEX', N'EROR'
GO
CREATE NONCLUSTERED INDEX ERORLNG ON VM1DTA.ERORPF
	(
	ERORPFX,
	ERORCOY,
	ERORPROG,
	EROREROR,
	ERORLANG
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DECLARE @v sql_variant 
SET @v = N'VM1DTA.ERORPF.ERORLNG'
EXECUTE sp_addextendedproperty N'MS_SSMA_SOURCE', @v, N'SCHEMA', N'VM1DTA', N'TABLE', N'ERORPF', N'INDEX', N'ERORLNG'
GO
CREATE UNIQUE NONCLUSTERED INDEX UQ_EROR ON VM1DTA.ERORPF
	(
	ERORPFX,
	ERORCOY,
	ERORLANG,
	ERORPROG,
	EROREROR
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX UQ_ERORPF ON VM1DTA.ERORPF
	(
	ERORPFX,
	ERORCOY,
	ERORLANG,
	ERORPROG,
	EROREROR
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
COMMIT

----------------------------



EXEC sp_refreshview 'VM1DTA.ERON'
EXEC sp_refreshview 'VM1DTA.EROR'
EXEC sp_refreshview 'VM1DTA.ERORLNG'

----------------------------

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [VM1DTA].[EROR]
AS
SELECT     UNIQUE_NUMBER, ERORPFX, ERORCOY, ERORLANG, ERORPROG, EROREROR, ERORDESC, TRDT, TRTM, USERID, TERMINALID, USRPRF, JOBNM, DATIME, ERORFILE
FROM         VM1DTA.ERORPF

GO

