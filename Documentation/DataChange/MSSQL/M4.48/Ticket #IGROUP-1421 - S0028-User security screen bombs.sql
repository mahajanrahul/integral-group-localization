/* Execute this query to get the max value of usernum of usrd table */
select max(usernum) from vm1dta.usrd
/* Execute this query to reset IDENT_CURRENT using max value of usernum as above 
   For example if Max is X value. Thus, we do next step as below
*/
DBCC CHECKIDENT ('QUIPOZ.USER_NUM_SEQ',RESEED,X)
/* Re-check whether IDENT was changed successfully or not */
SELECT IDENT_CURRENT('QUIPOZ.USER_NUM_SEQ')
