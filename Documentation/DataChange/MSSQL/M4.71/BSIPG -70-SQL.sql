/* GPHIPF AL */
 /*TABLE*/
/*ALTER TABLE*/

Alter Table vm1dta.GPHIPF Add
DISCRATE1 NUMERIC(5,2) NULL,
DISCRATE2  NUMERIC(5,2) NULL,
DESDISC1  NCHAR(25) Null,
NOINS1    INT NULL,
ISSTAFF1  NCHAR(1) NULL,
DESDISC2  NCHAR(25) NULL,
NOINS2	  INT NULL,
ISSTAFF2  NCHAR(1) NULL,
ABPREFLG  NCHAR(1) NULL
go

ALTER VIEW [VM1DTA].[GPHI](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, PRODTYP, EFFDATE, DTETRM, CMRATE, DISCRATE, DISCAMT, SPECTRM, RFUNDFLG, RFUNDPC01, RFUNDPC02, ADEXPFLG, ARFUNDPC, COVLMT01, COVLMT02, COVLMT03, COVLMT04, COVAGE01, COVAGE02, COVAGE03, COVAGE04, TERMID, USER_T, TRDT, TRTM, TRANNO, GCLRPERD, LMTPCT, RNCMRATE, NBFDFEE, RNFDFEE, CTBFRQ, INVSRULE, RIDISCPE, MANDAYS, DMDAYDED, MINMANDAYS, BILMANDAYS, PRMAMTRT, USRPRF, JOBNM, DATIME, 
DISCRATE1, DISCRATE2, DESDISC1, NOINS1, ISSTAFF1, DESDISC2,NOINS2, ISSTAFF2,ABPREFLG) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            PRODTYP,
            EFFDATE,
            DTETRM,
            CMRATE,
            DISCRATE,
            DISCAMT,
            SPECTRM,
            RFUNDFLG,
            RFUNDPC01,
            RFUNDPC02,
            ADEXPFLG,
            ARFUNDPC,
            COVLMT01,
            COVLMT02,
            COVLMT03,
            COVLMT04,
            COVAGE01,
            COVAGE02,
            COVAGE03,
            COVAGE04,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            TRANNO,
            GCLRPERD,
            LMTPCT,
            RNCMRATE,
            NBFDFEE,
            RNFDFEE,
            CTBFRQ,
            INVSRULE,
            RIDISCPE,
            MANDAYS,
            DMDAYDED,
            MINMANDAYS,
            BILMANDAYS,
            PRMAMTRT,
            USRPRF,
            JOBNM,
            DATIME,
			DISCRATE1,
			DISCRATE2,
			DESDISC1,
			NOINS1,
			ISSTAFF1,
			DESDISC2,
			NOINS2,
			ISSTAFF2,
			ABPREFLG
       FROM VM1DTA.GPHIPF

GO




/* DFPLPF */

Alter Table vm1dta.DFPLPF Add
ABPREFLG NCHAR(1) NULL
GO

ALTER VIEW [VM1DTA].[DFPL](UNIQUE_NUMBER, CHDRCOY, "TEMPLATE", PREMMTHD, REASONTRM, GSALFREQ, CARDTYPE01, CARDTYPE02, BNFTTMPLA, BNFTTMPLB, BNFTTMPLC, GATEKEEP, LMTWHOM, PROVNET, MBRTYPE, PREAUTH, DFTBFT, BNFTGRTMA, BNFTGRTMB, MFPLANNO, ELIGPERD, CLMWAIT, DEFPLAN, CREQ, HEADCNTFLG, ASCBFLG, ASCBLIMIT, ASCBLTBS, BENCDE, CLASSINS, RILDGIND, AUTHFLG, CTBRULE, RTARULE, DPNTRULE, JOBNM, USRPRF, DATIME, ABPREFLG) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            TEMPLATE,
            PREMMTHD,
            REASONTRM,
            GSALFREQ,
            CARDTYPE01,
            CARDTYPE02,
            BNFTTMPLA,
            BNFTTMPLB,
            BNFTTMPLC,
            GATEKEEP,
            LMTWHOM,
            PROVNET,
            MBRTYPE,
            PREAUTH,
            DFTBFT,
            BNFTGRTMA,
            BNFTGRTMB,
            MFPLANNO,
            ELIGPERD,
            CLMWAIT,
            DEFPLAN,
            CREQ,
            HEADCNTFLG,
            ASCBFLG,
            ASCBLIMIT,
            ASCBLTBS,
            BENCDE,
            CLASSINS,
            RILDGIND,
            AUTHFLG,
            CTBRULE,
            RTARULE,
            DPNTRULE,
            JOBNM,
            USRPRF,
            DATIME,
			ABPREFLG
       FROM VM1DTA.DFPLPF

GO



/* GMHIPF */

Alter Table vm1dta.GMHIPF Add
ISSTAFF NCHAR(1) NULL
GO


ALTER VIEW [VM1DTA].[GMHI](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, MBRNO, DPNTNO, EFFDATE, DTETRM, SUBSCOY, SUBSNUM, OCCPCODE, SALARY, DTEAPP, SBSTDL, TERMID, USER_T, TRDT, TRTM, TRANNO, FUPFLG, CLIENT, PERSONCOV, MLVLPLAN, CLNTCOY, PCCCLNT, APCCCLNT, EARNING, CTBPRCNT, CTBAMT, USRPRF, JOBNM, DATIME, ISSTAFF) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            MBRNO,
            DPNTNO,
            EFFDATE,
            DTETRM,
            SUBSCOY,
            SUBSNUM,
            OCCPCODE,
            SALARY,
            DTEAPP,
            SBSTDL,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            TRANNO,
            FUPFLG,
            CLIENT,
            PERSONCOV,
            MLVLPLAN,
            CLNTCOY,
            PCCCLNT,
            APCCCLNT,
            EARNING,
            CTBPRCNT,
            CTBAMT,
            USRPRF,
            JOBNM,
            DATIME,
			ISSTAFF
       FROM VM1DTA.GMHIPF

GO


/*  GXHIPF */

Alter Table vm1dta.GXHIPF Add
LOADREASON NCHAR(50) NULL;
GO


ALTER VIEW [VM1DTA].[GXHIPLN](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, MBRNO, PRODTYP, PLANNO, EFFDATE, FMLYCDE, DTEATT, DTETRM, REASONTRM, XCESSSI, APRVDATE, ACCPTDTE, SPECTRM, EXTRPRM, SUMINSU, DECFLG, USERSI, TERMID, USER_T, TRDT, TRTM, TRANNO, HEADNO, DPNTNO, EMLOAD, OALOAD, BILLACTN, IMPAIRCD01, IMPAIRCD02, IMPAIRCD03, RIEMLOAD, RIOALOAD, STDPRMLOAD, DTECLAM, JOBNM, USRPRF, DATIME,LOADREASON) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            MBRNO,
            PRODTYP,
            PLANNO,
            EFFDATE,
            FMLYCDE,
            DTEATT,
            DTETRM,
            REASONTRM,
            XCESSSI,
            APRVDATE,
            ACCPTDTE,
            SPECTRM,
            EXTRPRM,
            SUMINSU,
            DECFLG,
            USERSI,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            TRANNO,
            HEADNO,
            DPNTNO,
            EMLOAD,
            OALOAD,
            BILLACTN,
            IMPAIRCD01,
            IMPAIRCD02,
            IMPAIRCD03,
            RIEMLOAD,
            RIOALOAD,
            STDPRMLOAD,
            DTECLAM,
            JOBNM,
            USRPRF,
            DATIME,
			LOADREASON
       FROM VM1DTA.GXHIPF

GO

ALTER VIEW [VM1DTA].[GXHIMDP](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, MBRNO, PRODTYP, PLANNO, EFFDATE, HEADNO, FMLYCDE, DTEATT, DTETRM, REASONTRM, XCESSSI, APRVDATE, ACCPTDTE, SPECTRM, EXTRPRM, SUMINSU,NCBSI, USERSI, DECFLG, DPNTNO, TERMID, USER_T, TRDT, TRTM, TRANNO, EMLOAD, OALOAD, BILLACTN, IMPAIRCD01, IMPAIRCD02, IMPAIRCD03, RIEMLOAD, RIOALOAD, RIPROCDT, STDPRMLOAD, DTECLAM, USRPRF, JOBNM, DATIME,LOADREASON) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            MBRNO,
            PRODTYP,
            PLANNO,
            EFFDATE,
            HEADNO,
            FMLYCDE,
            DTEATT,
            DTETRM,
            REASONTRM,
            XCESSSI,
            APRVDATE,
            ACCPTDTE,
            SPECTRM,
            EXTRPRM,
            SUMINSU,
			NCBSI,
            USERSI,
            DECFLG,
            DPNTNO,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            TRANNO,
            EMLOAD,
            OALOAD,
            BILLACTN,
            IMPAIRCD01,
            IMPAIRCD02,
            IMPAIRCD03,
            RIEMLOAD,
            RIOALOAD,
            RIPROCDT,
            STDPRMLOAD,
            DTECLAM,
            USRPRF,
            JOBNM,
            DATIME,
			LOADREASON

       FROM VM1DTA.GXHIPF

GO

ALTER VIEW [VM1DTA].[GXHI](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, MBRNO, PRODTYP, PLANNO, EFFDATE, HEADNO, FMLYCDE, DTEATT, DTETRM, REASONTRM, XCESSSI, APRVDATE, ACCPTDTE, SPECTRM, EXTRPRM, SUMINSU, USERSI, DECFLG, DPNTNO, TERMID, USER_T, TRDT, TRTM, TRANNO, EMLOAD, OALOAD, BILLACTN, IMPAIRCD01, IMPAIRCD02, IMPAIRCD03, RIEMLOAD, RIOALOAD, STDPRMLOAD, DTECLAM, USRPRF, JOBNM, DATIME, LOADREASON) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            MBRNO,
            PRODTYP,
            PLANNO,
            EFFDATE,
            HEADNO,
            FMLYCDE,
            DTEATT,
            DTETRM,
            REASONTRM,
            XCESSSI,
            APRVDATE,
            ACCPTDTE,
            SPECTRM,
            EXTRPRM,
            SUMINSU,
            USERSI,
            DECFLG,
            DPNTNO,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            TRANNO,
            EMLOAD,
            OALOAD,
            BILLACTN,
            IMPAIRCD01,
            IMPAIRCD02,
            IMPAIRCD03,
            RIEMLOAD,
            RIOALOAD,
            STDPRMLOAD,
            DTECLAM,
            USRPRF,
            JOBNM,
            DATIME,
			LOADREASON
       FROM VM1DTA.GXHIPF

GO

/*  GBIDPF  */

Alter Table vm1dta.GBIDPF Add
DISCAMT1  NUMERIC(11, 2),
DISCRATE1 NUMERIC(5, 2),
DISCAMT2  NUMERIC(11, 2),
DISCRATE2 NUMERIC(5, 2)
GO


ALTER VIEW [VM1DTA].[GBID] (
	UNIQUE_NUMBER
	,CHDRCOY
	,BILLNO
	,PRODTYP
	,PLANNO
	,CLASSINS
	,BPREM
	,BEXTPRM
	,BADVRFUND
	,BCOMM
	,BOVCOMM01
	,BOVCOMM02
	,DISCRATE
	,DISCAMT
	,BATCCOY
	,BATCBRN
	,BATCACTYR
	,BATCACTMN
	,BATCTRCDE
	,BATCBATCH
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,FEES
	,VALIDFLAG
	,WKLADM
	,USRPRF
	,JOBNM
	,DATIME
	,DISCAMT1  
	,DISCRATE1 
	,DISCAMT2  
	,DISCRATE2 
	)
AS
SELECT UNIQUE_NUMBER
	,CHDRCOY
	,BILLNO
	,PRODTYP
	,PLANNO
	,CLASSINS
	,BPREM
	,BEXTPRM
	,BADVRFUND
	,BCOMM
	,BOVCOMM01
	,BOVCOMM02
	,DISCRATE
	,DISCAMT
	,BATCCOY
	,BATCBRN
	,BATCACTYR
	,BATCACTMN
	,BATCTRCDE
	,BATCBATCH
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,FEES
	,VALIDFLAG
	,WKLADM
	,USRPRF
	,JOBNM
	,DATIME
	,DISCAMT1  
	,DISCRATE1 
	,DISCAMT2  
	,DISCRATE2 
FROM VM1DTA.GBIDPF
WHERE VALIDFLAG = '1';

GO

/*  EAGHPF */


CREATE TABLE [VM1DTA].[EAGHPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[PRODTYP] [nchar](4) NULL,
	[PLANNO]  [nchar](3) NULL,
	[MBRFROM] [int] NULL,
	[MBRTO]   [int] NULL,
	[MBRUPTO] [int] NULL,
	[SPOFROM] [int] NULL,
	[SPOTO]	  [int] NULL,
	[SPOUPTO] [int] NULL,
	[CHIFROM] [int] NULL,
	[CHITO]   [int] NULL,
	[CHIUPTO] [int] NULL,
	[TRANNO]  [int] NULL,
	[USRPRF]  [nchar](10) NULL,
	[JOBNM]   [nchar](10) NULL,
	[DATIME]  [datetime2](6) NULL,
	
 CONSTRAINT [PK_EAGHPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



CREATE VIEW [VM1DTA].[EAGH] (
	UNIQUE_NUMBER,
	CHDRCOY,
	CHDRNUM,
	PRODTYP,
	PLANNO,
	MBRFROM,
	MBRTO,
	MBRUPTO,
	SPOFROM,
	SPOTO,
	SPOUPTO,
	CHIFROM,
	CHITO,
	CHIUPTO,
	TRANNO,
	USRPRF,
	JOBNM,
	DATIME
	)
AS
SELECT UNIQUE_NUMBER,
	CHDRCOY,
	CHDRNUM,
	PRODTYP,
	PLANNO,
	MBRFROM,
	MBRTO,
	MBRUPTO,
	SPOFROM,
	SPOTO,
	SPOUPTO,
	CHIFROM,
	CHITO,
	CHIUPTO,
	TRANNO,
	USRPRF,
	JOBNM,
	DATIME
FROM VM1DTA.EAGHPF
GO


/*  GLHDPF */

Alter Table vm1dta.GLHIPF Add
ABPREFLG  NCHAR(1)
GO


ALTER VIEW [VM1DTA].[GLHI](UNIQUE_NUMBER, CHDRCOY, CHDRNUM, PRODTYP, PLANNO, EFFDATE, DTETRM, ELIGPERD, CLMWAIT, DEFPLAN, CREQ, HEADCNTFLG, COINP, COING, ASCBFLG, ASCBLIMIT, ASCBLTBS, DEDUCTPC, DEDUCTTYP, BENCDE, COVBASIS, FIXSI, YRMULT, MNMULT, TERMAGE, DEFPERD, BENPERD, BENPBASIS, BENPC, BENBASIS, ESCBEN, FMLYCDE01, FMLYCDE02, FMLYCDE03, FMLYCDE04, FMLYCDE05, FMLYCDE06, FMLYCDE07, FMLYCDE08, PRMAMTRT01, PRMAMTRT02, PRMAMTRT03, PRMAMTRT04, PRMAMTRT05, PRMAMTRT06, PRMAMTRT07, PRMAMTRT08, INDIC, TERMID, USER_T, TRDT, TRTM, TRANNO, CLASSINS, LODG, DISCRATE, RILDGIND, RILODG, AUTHFLG, PRMRATFC, PRFBY, PRFDATE, CTBRULE, RTARULE, NOFYEAR, RELPROD, MAXAGE, NPFUND, COVLMTPLN, AGEMIN, AGEMAX, SUMINMIN, SUMINMAX, COVBSISD, [PERCENT], PCNTBPRD, DPNTRULE, USRPRF, JOBNM, DATIME,ABPREFLG) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            PRODTYP,
            PLANNO,
            EFFDATE,
            DTETRM,
            ELIGPERD,
            CLMWAIT,
            DEFPLAN,
            CREQ,
            HEADCNTFLG,
            COINP,
            COING,
            ASCBFLG,
            ASCBLIMIT,
            ASCBLTBS,
            DEDUCTPC,
            DEDUCTTYP,
            BENCDE,
            COVBASIS,
            FIXSI,
            YRMULT,
            MNMULT,
            TERMAGE,
            DEFPERD,
            BENPERD,
            BENPBASIS,
            BENPC,
            BENBASIS,
            ESCBEN,
            FMLYCDE01,
            FMLYCDE02,
            FMLYCDE03,
            FMLYCDE04,
            FMLYCDE05,
            FMLYCDE06,
            FMLYCDE07,
            FMLYCDE08,
            PRMAMTRT01,
            PRMAMTRT02,
            PRMAMTRT03,
            PRMAMTRT04,
            PRMAMTRT05,
            PRMAMTRT06,
            PRMAMTRT07,
            PRMAMTRT08,
            INDIC,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            TRANNO,
            CLASSINS,
            LODG,
            DISCRATE,
            RILDGIND,
            RILODG,
            AUTHFLG,
            PRMRATFC,
            PRFBY,
            PRFDATE,
            CTBRULE,
            RTARULE,
            NOFYEAR,
            RELPROD,
            MAXAGE,
            NPFUND,
            COVLMTPLN,
            AGEMIN,
            AGEMAX,
            SUMINMIN,
            SUMINMAX,
            COVBSISD,
            [PERCENT],
            PCNTBPRD,
            DPNTRULE,
            USRPRF,
            JOBNM,
            DATIME,
			ABPREFLG
       FROM VM1DTA.GLHIPF

GO




/* ERROR TEXT */
 INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'ENN1'
           ,'Age exceed renewable age defined'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');

 INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFPQ'
           ,'Discount plus Commission > Allowed limit'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');

INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'GPRFPQ'
           ,'Discount plus Commission > Allowed limit. Continue? (Y/N)'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');


 INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFPY'
           ,'Age exceed renewable age defined'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');

INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'SPRFR5'
           ,'Discount plus Commission > Allowed limit. Continue? (Y/N)'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');


INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'SPRFR6'
           ,'Age not within min/max age defined. Continue? (Y/N)'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');


/* HELP TEST  */
/*SR9B1*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'DISCRATE1'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to capture the Discount1 percentage'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'DESDISC1'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to describe Discount 1'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'NOINS1'
           ,'1'
           ,''
           ,'1'
           ,'This field reflects the first discount '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'NOINS1'
           ,'2'
           ,''
           ,'1'
           ,'to be applied on the policy�s premium.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 





INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'DISCRATE2'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to capture the Discount1 percentage'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'DESDISC2'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to describe Discount 2'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'NOINS2'
           ,'1'
           ,''
           ,'1'
           ,'This field reflects the first discount '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'NOINS2'
           ,'2'
           ,''
           ,'1'
           ,'to be applied on the policy�s premium.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 



INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'ISSTAFF1'
           ,'1'
           ,''
           ,'1'
           ,'This checkbox is to indicate for staff discount.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'ISSTAFF2'
           ,'1'
           ,''
           ,'1'
           ,'This checkbox is to indicate for staff discount.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'ABPREFLG'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to indicate if Absolute Premium '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 



INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'ABPREFLG'
           ,'2'
           ,''
           ,'1'
           ,'is applicable for the Product/Plan.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 


/* S9114 */

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'DISCRATE1'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to capture the Discount1 percentage'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'DESDISC1'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to describe Discount 1'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'NOINS1'
           ,'1'
           ,''
           ,'1'
           ,'This field reflects the first discount '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'NOINS1'
           ,'2'
           ,''
           ,'1'
           ,'to be applied on the policy�s premium.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 





INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'DISCRATE2'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to capture the Discount1 percentage'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'DESDISC2'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to describe Discount 2'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'NOINS2'
           ,'1'
           ,''
           ,'1'
           ,'This field reflects the first discount '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'NOINS2'
           ,'2'
           ,''
           ,'1'
           ,'to be applied on the policy�s premium.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 



INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'ISSTAFF1'
           ,'1'
           ,''
           ,'1'
           ,'This checkbox is to indicate for staff discount.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'ISSTAFF2'
           ,'1'
           ,''
           ,'1'
           ,'This checkbox is to indicate for staff discount.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'ABPREFLG'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to indicate if Absolute Premium '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 



INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'ABPREFLG'
           ,'2'
           ,''
           ,'1'
           ,'is applicable for the Product/Plan.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 


/*S9106*/

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9106'
           ,'LOADREASON'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to capture the loading reason.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9106'
           ,'ISSTAFF'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to indicate if '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9106'
           ,'ISSTAFF'
           ,'2'
           ,''
           ,'1'
           ,'a member/dependent is a staff.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 


/*PQ905*/


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'PLANNO'
           ,'1'
           ,''
           ,'1'
           ,'Protected field to display the plan number'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'MBRFROM'
           ,'1'
           ,''
           ,'1'
           ,'Protected fields to display the min/max entry'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'MBRFROM'
           ,'2'
           ,''
           ,'1'
           ,'age and renewal up to age for main member.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'MBRTO'
           ,'1'
           ,''
           ,'1'
           ,'Protected fields to display the min/max entry  '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'MBRTO'
           ,'2'
           ,''
           ,'1'
           ,'age and renewal up to age for main member. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 



INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'MBRUPTO'
           ,'1'
           ,''
           ,'1'
           ,'Protected fields to display the min/max entry  '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'MBRUPTO'
           ,'2'
           ,''
           ,'1'
           ,'age and renewal up to age for main member. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'SPOFROM'
           ,'1'
           ,''
           ,'1'
           ,'Protected fields to display the min/max entry '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'SPOFROM'
           ,'2'
           ,''
           ,'1'
           ,'age and renewal up to age spouse. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 



INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'SPOTO'
           ,'1'
           ,''
           ,'1'
           ,'Protected fields to display the min/max entry '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'SPOTO'
           ,'2'
           ,''
           ,'1'
           ,'age and renewal up to age spouse. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 




INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'SPOUPTO'
           ,'1'
           ,''
           ,'1'
           ,'Protected fields to display the min/max entry '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'SPOUPTO'
           ,'2'
           ,''
           ,'1'
           ,'age and renewal up to age spouse. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 




INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'CHIFROM'
           ,'1'
           ,''
           ,'1'
           , 'Protected fields to display the min/max entry  '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'CHIFROM'
           ,'2'
           ,''
           ,'1'
           ,'age and renewal up to age for child.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 



INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'CHITO'
           ,'1'
           ,''
           ,'1'
           , 'Protected fields to display the min/max entry  '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'CHITO'
           ,'2'
           ,''
           ,'1'
           ,'age and renewal up to age for child.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 


INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'CHIUPTO'
           ,'1'
           ,''
           ,'1'
           , 'Protected fields to display the min/max entry  '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 

INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'CHIUPTO'
           ,'2'
           ,''
           ,'1'
           ,'age and renewal up to age for child.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
GO 


select * from vm1dta.eror where eroreror='rfpy'
go

DELETE FROM vm1dta.SLCK
go

select * 
	from VM1DTA.[GMOVFUN] WITH (READPAST)  
	WHERE 1=1 and CHDRCOY='3' and CHDRNUM='35002462' and TRANNO=1 and REFKEY='335002462                       ' and FUNCCODE='00000'  and unique_number>2045916  
	order by CHDRCOY ASC, CHDRNUM ASC, TRANNO ASC, REFKEY ASC, FUNCCODE ASC, UNIQUE_NUMBER ASC
go
	