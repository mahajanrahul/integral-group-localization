

DECLARE @userid nchar(10);

SET @userid = 'INDIADEV';

BEGIN
	BEGIN TRANSACTION
	-- Error: Company not sanctioned 
	IF NOT EXISTS(SELECT 1 FROM VM1DTA.USCYPF WHERE LTRIM(RTRIM(USERID))='INDIADEV' AND COMPANY = 3)
		BEGIN
			PRINT(N'INDIADEV don''t have permission to login Group');
			INSERT INTO [VM1DTA].[USCYPF]
					   ([USERID]
					   ,[COMPANY]
					   ,[USRPRF]
					   ,[JOBNM]
					   ,[DATIME])
			VALUES ('INDIADEV  ', '3', 'INDIADEV  ', 'INDIADEV  ', GETDATE());
		END

	-- Error: Branch not sanctioned
	IF NOT EXISTS(SELECT 1 FROM VM1DTA.USBRPF WHERE LTRIM(RTRIM(USERID))='INDIADEV' AND COMPANY = 3)
		BEGIN
			PRINT(N'INDIADEV don''t have permission to login Group with Branch 10');
			INSERT INTO [VM1DTA].[USBRPF]
					   ([USERID]
					   ,[COMPANY]
					   ,[BRANCH]
					   ,[USRPRF]
					   ,[JOBNM]
					   ,[DATIME])
			VALUES ('INDIADEV  ', '3', '10', 'INDIADEV  ', 'INDIADEV  ', GETDATE());
		END
	COMMIT;
END