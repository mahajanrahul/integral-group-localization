/*ALTER TABLE*/
Alter table VM1DTA.GCBNPF
ADD NCBSI [numeric](17,2) null
GO

/****** Object:  View [VM1DTA].[GCBN]    Script Date: 8/12/2016 1:30:27 PM ******/
DROP VIEW [VM1DTA].[GCBN]
GO

/****** Object:  View [VM1DTA].[GCBN]    Script Date: 8/12/2016 1:30:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [VM1DTA].[GCBN](UNIQUE_NUMBER, CHDRCOY, CLAMNUM, GCOCCNO, GCBENSEQ, BENCDE, GCINCAMT, BENAMT, BENBASIS, BENLMT, BENLMTB, GCBENPY, GCSYSPY, GCSYSRP, GCRDRPY, HSUMINSU, BENPC, RASCOY01, RASCOY02, RASCOY03, RASCOY04, RASCOY05, ACCNUM01, ACCNUM02, ACCNUM03, ACCNUM04, ACCNUM05, GRHCQSSI01, GRHCQSSI02, GRHCQSSI03, GRHCQSSI04, GRHCQSSI05, TERMID, USER_T, TRDT, TRTM, GCPROVCD, Z6TAXTYP, INVOICENO, DATESTART, DATEEND, NCBSI, USRPRF, JOBNM, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CLAMNUM,
            GCOCCNO,
            GCBENSEQ,
            BENCDE,
            GCINCAMT,
            BENAMT,
            BENBASIS,
            BENLMT,
            BENLMTB,
            GCBENPY,
            GCSYSPY,
            GCSYSRP,
            GCRDRPY,
            HSUMINSU,
            BENPC,
            RASCOY01,
            RASCOY02,
            RASCOY03,
            RASCOY04,
            RASCOY05,
            ACCNUM01,
            ACCNUM02,
            ACCNUM03,
            ACCNUM04,
            ACCNUM05,
            GRHCQSSI01,
            GRHCQSSI02,
            GRHCQSSI03,
            GRHCQSSI04,
            GRHCQSSI05,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            GCPROVCD,
			Z6TAXTYP,
			INVOICENO,
			DATESTART, 
			DATEEND,
			NCBSI,
            USRPRF,
            JOBNM,
            DATIME
       FROM VM1DTA.GCBNPF



GO


