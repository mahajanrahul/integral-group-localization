ALTER VIEW VM1DTA.GHHI(UNIQUE_NUMBER, CHDRCOY, CHDRNUM, HEADNO, EFFDATE, PRODTYP, PLANNO, FMLYCDE, DTEATT, DTETRM, REASONTRM, HSUMINSU, NOFMBR, AVRGAGE, SPECTRM, SUBSCOY, SUBSNUM, SUBSTDSI, TERMID, USER_T, TRDT, TRTM, TRANNO, HIGHSI, USRPRF, JOBNM, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            HEADNO,
            EFFDATE,
            PRODTYP,
            PLANNO,
            FMLYCDE,
            DTEATT,
            DTETRM,
            REASONTRM,
            HSUMINSU,
            NOFMBR,
            AVRGAGE,
            SPECTRM,
            SUBSCOY,
            SUBSNUM,
            SUBSTDSI,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            TRANNO,
			HIGHSI,
            USRPRF,
            JOBNM,
            DATIME
       FROM VM1DTA.GHHIPF



