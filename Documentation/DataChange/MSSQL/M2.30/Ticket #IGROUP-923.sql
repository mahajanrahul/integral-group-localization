ALTER VIEW [VM1DTA].[CMNHBAL] AS
SELECT UNIQUE_NUMBER,
            GDOCCOY,
            CHDRNUM,
            SUBSNUM,
            GDOCPFX,
            GDOCNUM,
            COMDUEDT,
            PRMRCVDT,
            LICVALID,
            ALLCHQREA,
            AGNTNUM,
            ORAGNT,
            BALANCE,
            BCOMM,
            BOVCOMM,
            PRVFLG,
            REQNNO01,
            REQNNO02,
            JOBNO,
            JOBNM,
            USRPRF,
            DATIME
       FROM VM1DTA.CMNHPF
      WHERE BALANCE > 0
GO 