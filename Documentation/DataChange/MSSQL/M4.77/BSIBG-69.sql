
/****** Object:  Table [VM1DTA].[FWMDPF]    Script Date: 5/16/2016 5:22:45 PM ******/
DROP TABLE [VM1DTA].[FWMDPF]
GO

/****** Object:  Table [VM1DTA].[FWMDPF]    Script Date: 5/16/2016 5:22:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [VM1DTA].[FWMDPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CHDRNUM] [nchar](8) NULL,
	[MBRNO] [nchar](5) NULL,
	[DPNTNO] [nchar](2) NULL,
	[INSFOR] [nchar](1) NULL,
	[INSSTS] [nchar](1) NULL,
	[PLACEMP] [nchar](30) NULL,
	[CSTCNTRE] [nchar](30) NULL,
	[PMTDTE] [int] NULL,
	[PMEXDT] [int] NULL,
	[PERMIT] [nchar](20) NULL,
	[FWCMSREF] [nchar](20) NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL,
 CONSTRAINT [PK_FWMDPF] PRIMARY KEY CLUSTERED 
(
	[UNIQUE_NUMBER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



/****** Object:  View [VM1DTA].[FWMD]    Script Date: 5/16/2016 5:24:40 PM ******/
DROP VIEW [VM1DTA].[FWMD]
GO

/****** Object:  View [VM1DTA].[FWMD]    Script Date: 5/16/2016 5:24:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [VM1DTA].[FWMD] (
	UNIQUE_NUMBER,
	CHDRCOY,
	CHDRNUM,
	MBRNO,
	DPNTNO,
	INSFOR,
	INSSTS,
	PLACEMP,
	CSTCNTRE,
	PMTDTE,
	PMEXDT,
	PERMIT,
	FWCMSREF,
	USRPRF,
	JOBNM,
	DATIME
	)
AS
SELECT UNIQUE_NUMBER,
	CHDRCOY,
	CHDRNUM,
	MBRNO,
	DPNTNO,
	INSFOR,
	INSSTS,
	PLACEMP,
	CSTCNTRE,
	PMTDTE,
	PMEXDT,
	PERMIT,
	FWCMSREF,
	USRPRF,
	JOBNM,
	DATIME
FROM VM1DTA.FWMDPF
Go


ALTER TABLE VM1DTA.CHDRPF ADD
	MIDJOIN		nchar(01) NULL 
GO


/****** Object:  View [VM1DTA].[GCHD]   ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [VM1DTA].[GCHD](
	UNIQUE_NUMBER
		,CHDRCOY
		,CHDRNUM
		,CHDRPFX
		,CNTTYPE
		,COWNPFX
		,COWNCOY
		,COWNNUM
		,STATCODE
		,PNDATE
		,SUBSFLG
		,OCCDATE
		,HRSKIND
		,CNTCURR
		,BILLCURR
		,TAKOVRFLG
		,GPRNLTYP
		,GPRMNTHS
		,RNLNOTTO
		,SRCEBUS
		,COYSRVAC
		,MRKSRVAC
		,DESPPFX
		,DESPCOY
		,DESPNUM
		,POLSCHPFLG
		,BTDATE
		,ADJDATE
		,PTDATE
		,PTDATEAB
		,TRANLUSED
		,LMBRNO
		,LHEADNO
		,EFFDCLDT
		,SERVUNIT
		,PNTRCDE
		,PROCID
		,TRANID
		,TRANNO
		,VALIDFLAG
		,SPECIND
		,TAXFLAG
		,AGEDEF
		,TERMAGE
		,PERSONCOV
		,ENROLLTYP
		,SPLITSUBS
		,AVLISU
		,MPLPFX
		,MPLCOY
		,MPLNUM
		,USRPRF
		,JOBNM
		,DATIME
		,MIDJOIN
	) AS
SELECT UNIQUE_NUMBER
		,CHDRCOY
		,CHDRNUM
		,CHDRPFX
		,CNTTYPE
		,COWNPFX
		,COWNCOY
		,COWNNUM
		,STATCODE
		,PNDATE
		,SUBSFLG
		,OCCDATE
		,HRSKIND
		,CNTCURR
		,BILLCURR
		,TAKOVRFLG
		,GPRNLTYP
		,GPRMNTHS
		,RNLNOTTO
		,SRCEBUS
		,COYSRVAC
		,MRKSRVAC
		,DESPPFX
		,DESPCOY
		,DESPNUM
		,POLSCHPFLG
		,BTDATE
		,ADJDATE
		,PTDATE
		,PTDATEAB
		,TRANLUSED
		,LMBRNO
		,LHEADNO
		,EFFDCLDT
		,SERVUNIT
		,PNTRCDE
		,PROCID
		,TRANID
		,TRANNO
		,VALIDFLAG
		,SPECIND
		,TAXFLAG
		,AGEDEF
		,TERMAGE
		,PERSONCOV
		,ENROLLTYP
		,SPLITSUBS
		,AVLISU
		,MPLPFX
		,MPLCOY
		,MPLNUM
		,USRPRF
		,JOBNM
		,DATIME
		,MIDJOIN
       FROM VM1DTA.CHDRPF

GO

/*[MENDPF] Table*/
CREATE TABLE [VM1DTA].[MENDPF](
	[CHDRCOY] [nchar](01) NULL,
	[CHDRNUM] [nchar](08) NULL,
	[MBRNO]   [nchar](05)NULL,
	[DPNTNO]  [nchar](02)NULL,
	[TRANNO]  [int] NULL,
	[ENDRSN]  [nchar](02)NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL,
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
)
GO


/*[MEND] View*/
CREATE VIEW [VM1DTA].[MEND]([CHDRCOY], [CHDRNUM], [MBRNO], [DPNTNO], [TRANNO], [ENDRSN], [USRPRF],[JOBNM],[DATIME],[UNIQUE_NUMBER]) AS
SELECT [CHDRCOY],
       [CHDRNUM],
       [MBRNO],
       [DPNTNO],
       [TRANNO],
       [ENDRSN],
	   [USRPRF],
	   [JOBNM],
	   [DATIME],
	   [UNIQUE_NUMBER]
 FROM VM1DTA.MENDPF
GO

	
/*[PLAPPF] Table*/
CREATE TABLE [VM1DTA].[PLAPPF](
	[CHDRCOY] [nchar](01) NULL,
	[CHDRNUM] [nchar](08) NULL,
	[LAPRSN]  [varchar](04)NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL,
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
)
GO	

/*[PLAP] View*/
CREATE VIEW [VM1DTA].[PLAP]([CHDRCOY], [CHDRNUM], [LAPRSN],[USRPRF],[JOBNM],[DATIME],[UNIQUE_NUMBER]) AS
SELECT [CHDRCOY],
       [CHDRNUM],
       [LAPRSN],
	   [USRPRF],
	   [JOBNM],
	   [DATIME],
	   [UNIQUE_NUMBER]
 FROM VM1DTA.PLAPPF
GO


/*New Field SM901*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
('HP','','E','F','S9103','MIDJOIN','1','','1','Check box Field:If it is ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','S9103','MIDJOIN','2','','1','ticked for then Full premium','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','S9103','MIDJOIN','3','','1','will be collected from','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','S9103','MIDJOIN','4','','1',' the mid joiner and','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','S9103','MIDJOIN','5','','1',', if it is un-tick then pro-rata method will','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','S9103','MIDJOIN','6','','1',',be used for premium collection by default.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP) ,
('HP','','E','F','S9130','LAPRSN','1','','1','Dropdown list which contains ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','S9130','LAPRSN','2','','1','Reason Code for Policy Lapse.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','S9106','ENDRSN','1','','1','Dropdown list which contains ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','S9106','ENDRSN','2','','1','Reason Code for Endorsement.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM901','COWNSEL','1','','1','This field contain a valid Policy Number','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM901','COWNSEL','2','','1',' If the Policy Number is not known, ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM901','COWNSEL','3','','1','a Policy Owner window may be invoked','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM901','COWNSEL','4','','1','from which the relevant ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM901','COWNSEL','5','','1',' policy can be selected.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM901','ACTION','1','','1','Action A on continue, will display ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM901','ACTION','2','','1','the new screen SM902 � ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM901','ACTION','3','','1',' � Policy Journal Entry Screen','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM901','ACTION','4','','1','Action B which when selected','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM901','ACTION','5','','1','will allow user to view remarks or ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM901','ACTION','6','','1','description (if any) related ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM901','ACTION','7','','1','to the policy journal.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP)
Go

/*New Field SM902*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
('HP','','E','F','SM902','COWNSEL','1','','1','This field contain a valid Policy Number.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM902','COWNSEL','2','','1',' If the Policy Number is not known, ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM902','COWNSEL','3','','1','a Policy Owner window may be invoked','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM902','COWNSEL','4','','1','from which the relevant ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM902','COWNSEL','5','','1','policy can be selected.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM902','ACTION','1','','1','Action A on continue, will display ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM902','ACTION','2','','1','the new screen SM902 ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM902','ACTION','3','','1','� Policy Journal Entry Screen','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM902','ACTION','4','','1','Action B which when selected','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM902','ACTION','5','','1','will allow user to view remarks or ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM902','ACTION','6','','1','description (if any) related ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM902','ACTION','7','','1','to the policy journal.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP)
Go

/*New Field SM903*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
('HP','','E','F','SM903','polnum','1','','1','This field displays the policy number','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM903','prodtyp','1','','1','This field displays the policy type','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM903','effdate','1','','1','This field displays the effective ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM903','effdate','2','','1','of the transaction. ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM903','dtetrm','1','','1','This field displays the expiry date','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM903','dtetrm','2','','1','of the transaction. ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM903','gpline','1','','1','Space provided to user to add ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM903','gpline','2','','1','additional comments or remarks if any.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP)
Go

/*New Field SM904*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
('HP','','E','F','SM904','chdrnum','1','','1','A protected field which','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM904','chdrnum','2','','1','displays policy number.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM904','cnttype','1','','1','A protected field which','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM904','cnttype','2','','1','displays type.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM904','longname','1','','1','A protected field which','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM904','longname','2','','1','displays policy long description.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM904','clntname','1','','1','This is the Client number ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM904','clntname','2','','1','of the Contract Owner.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM904','clntname','3','','1','It is entered in the Contract ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM904','clntname','4','','1','Header Screen, and may only be ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM904','clntname','5','','1','modified on that Screen. ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM904','clntname','6','','1','It is displayed here for information ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM904','clntname','7','','1','only and may not be changed','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM904','qtranno','1','','1','A protected field which ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM904','qtranno','2','','1','displays the transaction no.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP)
GO

/*New Field SM905*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
('HP','','E','F','SM905','cownsel','1','','1','The field is used to select  ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM905','cownsel','2','','1','the Policy number.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM905','dteeff','1','','1','This is the effective date ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SM905','dteeff','2','','1','of the input transaction.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP)
 GO


  
 /*New Screen */
 INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
('HP','','E','S','SM901','','1','','1','This screen is created as a sub-menu for the ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','S','SM901','','2','','1','creation of manual journal ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','S','SM901','','3','','1','for Experience Refund and Stop Loss.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','S','SM902','','1','','1','This screen facilitates the manual creation ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','S','SM902','','2','','1','of the experience refund credit note and','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','S','SM902','','3','','1','stop loss debit note. The commission and ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','S','SM902','','4','','1','if any will be calculated automatically once','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','S','SM902','','5','','1','the transaction is issued.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','S','SM903','','1','','1','This screen allows users to enter any  ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','S','SM903','','2','','1','additional text if required.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','S','SM904','','1','','1','This screen allows user to select the Policy Journal ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','S','SM904','','2','','1','Journal transaction against which the user defined','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','S','SM904','','3','','1','additional text needs to be enquired.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','S','SM905','','1','','1','This screen allows manual extension of the policy ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','S','SM905','','2','','1','on short term basis wherein the premium calculation','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','S','SM905','','3','','1','is done using percentage set-up within the table.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP)
GO


INSERT INTO [VM1DTA].[ERORPF]
           ([ERORPFX]
           ,[ERORCOY]
           ,[ERORLANG]
           ,[ERORPROG]
           ,[EROREROR]
           ,[ERORDESC]
           ,[TRDT]
           ,[TRTM]
           ,[USERID]
           ,[TERMINALID]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME]
           ,[ERORFILE])
     VALUES
 ('ER','3','E','','RFR7','Item not exist in TM906','0','0','000036','UNDERWR1  ','UNDERWR1  ','UNDERWR1  ',CURRENT_TIMESTAMP,''),
 ('ER','3','E','','H946','Invalid Contract','0','0','000036','UNDERWR1  ','UNDERWR1  ','UNDERWR1  ',CURRENT_TIMESTAMP,''),
  ('ER','3','E','','W253','Sht Term Ext not allowed','0','0','000036','UNDERWR1  ','UNDERWR1  ','UNDERWR1  ',CURRENT_TIMESTAMP,'')
  Go