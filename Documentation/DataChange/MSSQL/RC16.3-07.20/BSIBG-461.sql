/*New Field*/
INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
     VALUES
('HP',' ','E','F','SQ945','hrsfrom',1,'',1,'This field is used to capture Time From','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP',' ','E','F','SQ945','hrsto',1,'',1,'This field is used to capture Time To','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP)
GO

/****** Object:  Table [VM1DTA].[GCBAPF]    Script Date: 7/15/2016 10:15:40 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [VM1DTA].[GCBAPF](
	[UNIQUE_NUMBER] [bigint] IDENTITY(1,1) NOT NULL,
	[CHDRCOY] [nchar](1) NULL,
	[CLAMNUM] [nchar](8) NULL,
	[GCOCCNO] [nchar](2) NULL,
	[GCBENSEQ] [int] NULL,
	[BENCDE] [nchar](4) NULL,
	[CLAIMCOND] [nchar](8) NULL,
	[GCINCAMT] [numeric](12, 2) NULL,
	[BENAMT] [bigint] NULL,
	[BENBASIS] [nchar](4) NULL,
	[BENLMT] [bigint] NULL,
	[BENLMTB] [nchar](4) NULL,
	[GCBENPY] [numeric](12, 2) NULL,
	[STRTDT] [int] NULL,
	[ENDDT] [int] NULL,
	[GCSYSPY] [numeric](15, 2) NULL,
	[GCSYSRP] [numeric](15, 2) NULL,
	[GCRDRPY] [numeric](12, 2) NULL,
	[HSUMINSU] [bigint] NULL,
	[SUMINSU] [numeric](15, 2) NULL,
	[BENPC] [int] NULL,
	[RASCOY01] [nchar](1) NULL,
	[RASCOY02] [nchar](1) NULL,
	[RASCOY03] [nchar](1) NULL,
	[RASCOY04] [nchar](1) NULL,
	[RASCOY05] [nchar](1) NULL,
	[ACCNUM01] [nchar](8) NULL,
	[ACCNUM02] [nchar](8) NULL,
	[ACCNUM03] [nchar](8) NULL,
	[ACCNUM04] [nchar](8) NULL,
	[ACCNUM05] [nchar](8) NULL,
	[GRHCQSSI01] [bigint] NULL,
	[GRHCQSSI02] [bigint] NULL,
	[GRHCQSSI03] [bigint] NULL,
	[GRHCQSSI04] [bigint] NULL,
	[GRHCQSSI05] [bigint] NULL,
	[HRSFROM] [numeric](2, 0) NULL,
	[MINUTFROM] [numeric](2, 0) NULL,
	[TIMETYP1] [nchar](2) NULL,
	[HRSTO] [numeric](2, 0) NULL,
	[MINUTTO] [numeric](2, 0) NULL,
	[TIMETYP2] [nchar](2) NULL,
	[TERMID] [nchar](4) NULL,
	[USER_T] [int] NULL,
	[TRDT] [int] NULL,
	[TRTM] [int] NULL,
	[GCPROVCD] [nchar](5) NULL,
	[USRPRF] [nchar](10) NULL,
	[JOBNM] [nchar](10) NULL,
	[DATIME] [datetime2](6) NULL
) ON [PRIMARY]

GO

/****** Object:  View [VM1DTA].[GCBA]    Script Date: 7/15/2016 10:14:14 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [VM1DTA].[GCBA](UNIQUE_NUMBER, CHDRCOY, CLAMNUM, GCOCCNO, GCBENSEQ, BENCDE,CLAIMCOND, GCINCAMT, BENAMT, BENBASIS, BENLMT, BENLMTB, GCBENPY, STRTDT, ENDDT, GCSYSPY, GCSYSRP, GCRDRPY, HSUMINSU, SUMINSU, BENPC, RASCOY01, RASCOY02, RASCOY03, RASCOY04, RASCOY05, ACCNUM01, ACCNUM02, ACCNUM03, ACCNUM04, ACCNUM05, GRHCQSSI01, GRHCQSSI02, GRHCQSSI03, GRHCQSSI04, GRHCQSSI05,HRSFROM,MINUTFROM,TIMETYP1,HRSTO,MINUTTO,TIMETYP2, TERMID, USER_T, TRDT, TRTM, GCPROVCD, USRPRF, JOBNM, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CLAMNUM,
            GCOCCNO,
            GCBENSEQ,
            BENCDE,
			CLAIMCOND,
            GCINCAMT,
            BENAMT,
            BENBASIS,
            BENLMT,
            BENLMTB,
            GCBENPY,
			STRTDT,
			ENDDT,
            GCSYSPY,
            GCSYSRP,
            GCRDRPY,
            HSUMINSU,
			SUMINSU,
            BENPC,
            RASCOY01,
            RASCOY02,
            RASCOY03,
            RASCOY04,
            RASCOY05,
            ACCNUM01,
            ACCNUM02,
            ACCNUM03,
            ACCNUM04,
            ACCNUM05,
            GRHCQSSI01,
            GRHCQSSI02,
            GRHCQSSI03,
            GRHCQSSI04,
            GRHCQSSI05,
			HRSFROM,
			MINUTFROM,
			TIMETYP1,
			HRSTO,
			MINUTTO,
			TIMETYP2,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            GCPROVCD,
		    USRPRF,
            JOBNM,
            DATIME
       FROM VM1DTA.GCBAPF
GO




