/****** Create new VIEW named [PROCX] ******/
CREATE VIEW [VM1DTA].[PROCX](UNIQUE_NUMBER, CHDRCOY, SRVCCODE, PROCSDES, PROCLDES, PROCCLS, PLDESC, USRPRF, JOBNM, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            SRVCCODE,
            PROCSDES,
            PROCLDES,
            PROCCLS,
            PLDESC,
            USRPRF,
            JOBNM,
            DATIME
       FROM vm1dta.PROCPF
GO

/****** Drop existed VIEW named [PROC] ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[VM1DTA].[PROC]'))
DROP VIEW [VM1DTA].[PROC]
GO