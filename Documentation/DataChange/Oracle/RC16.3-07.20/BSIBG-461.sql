--Help Text for New fields

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
('HP','3','E','F','SQ945','hrsfrom',1,'','1','This field is used to capture Time From.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
('HP','3','E','F','SQ945','minutfrom',1,'','1','This field is used to capture Time From.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
('HP','3','E','F','SQ945','timetyp1',1,'','1','This field is used to capture Time From.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
('HP','3','E','F','SQ945','hrsto',1,'','1','This field is used to capture Time To.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
('HP','3','E','F','SQ945','minutto',1,'','1','This field is used to capture Time To.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
('HP','3','E','F','SQ945','timetyp2',1,'','1','This field is used to capture Time To.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

CREATE TABLE VM1DTA.GCBAPF(
	UNIQUE_NUMBER NUMBER(18,0) NOT NULL,
	CHDRCOY nchar(1) NULL,
	CLAMNUM nchar(8) NULL,
	GCOCCNO nchar(2) NULL,
	GCBENSEQ int NULL,
	BENCDE nchar(4) NULL,
	CLAIMCOND nchar(8) NULL,
	GCINCAMT NUMBER(12, 2) NULL,
	BENAMT NUMBER(18,0) NULL,
	BENBASIS nchar(4) NULL,
	BENLMT NUMBER(18,0) NULL,
	BENLMTB nchar(4) NULL,
	GCBENPY NUMBER(12, 2) NULL,
	STRTDT int NULL,
	ENDDT int NULL,
	GCSYSPY NUMBER(15, 2) NULL,
	GCSYSRP NUMBER(15, 2) NULL,
	GCRDRPY NUMBER(12, 2) NULL,
	HSUMINSU NUMBER(18,0) NULL,
	SUMINSU NUMBER(15, 2) NULL,
	BENPC int NULL,
	RASCOY01 nchar(1) NULL,
	RASCOY02 nchar(1) NULL,
	RASCOY03 nchar(1) NULL,
	RASCOY04 nchar(1) NULL,
	RASCOY05 nchar(1) NULL,
	ACCNUM01 nchar(8) NULL,
	ACCNUM02 nchar(8) NULL,
	ACCNUM03 nchar(8) NULL,
	ACCNUM04 nchar(8) NULL,
	ACCNUM05 nchar(8) NULL,
	GRHCQSSI01 NUMBER(18,0) NULL,
	GRHCQSSI02 NUMBER(18,0) NULL,
	GRHCQSSI03 NUMBER(18,0) NULL,
	GRHCQSSI04 NUMBER(18,0) NULL,
	GRHCQSSI05 NUMBER(18,0) NULL,
	HRSFROM NUMBER(2, 0) NULL,
	MINUTFROM NUMBER(2, 0) NULL,
	TIMETYP1 nchar(2) NULL,
	HRSTO NUMBER(2, 0) NULL,
	MINUTTO NUMBER(2, 0) NULL,
	TIMETYP2 nchar(2) NULL,
	TERMID nchar(4) NULL,
	USER_T int NULL,
	TRDT int NULL,
	TRTM int NULL,
	GCPROVCD nchar(5) NULL,
	USRPRF nchar(10) NULL,
	JOBNM nchar(10) NULL,
	DATIME TIMESTAMP(6) NULL
);


ALTER TABLE VM1DTA.GCBAPF add CONSTRAINT PK_GCBAPF PRIMARY KEY (UNIQUE_NUMBER);

CREATE SEQUENCE VM1DTA.SEQ_GCBAPF MINVALUE 1000000 MAXVALUE 99999999999999999999 INCREMENT BY 1 START WITH 3002058 NOCACHE NOORDER NOCYCLE;


CREATE OR REPLACE TRIGGER VM1DTA.TR_GCBAPF before
  INSERT ON GCBAPF FOR EACH row DECLARE v_pkValue NUMBER;
BEGIN
  SELECT SEQ_GCBAPF.nextval INTO v_pkValue FROM dual;
  :New.unique_number := v_pkValue;
END TR_GCBAPF;
/


CREATE VIEW VM1DTA.GCBA(UNIQUE_NUMBER, CHDRCOY, CLAMNUM, GCOCCNO, GCBENSEQ, BENCDE,CLAIMCOND, GCINCAMT, BENAMT, BENBASIS, BENLMT, BENLMTB, 
GCBENPY, STRTDT, ENDDT, GCSYSPY, GCSYSRP, GCRDRPY, HSUMINSU, SUMINSU, BENPC, RASCOY01, RASCOY02, RASCOY03, RASCOY04, RASCOY05, ACCNUM01, 
ACCNUM02, ACCNUM03, ACCNUM04, ACCNUM05, GRHCQSSI01, GRHCQSSI02, GRHCQSSI03, GRHCQSSI04, GRHCQSSI05,HRSFROM,MINUTFROM,TIMETYP1,HRSTO,
MINUTTO,TIMETYP2, TERMID, USER_T, TRDT, TRTM, GCPROVCD, USRPRF, JOBNM, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CLAMNUM,
            GCOCCNO,
            GCBENSEQ,
            BENCDE,
			CLAIMCOND,
            GCINCAMT,
            BENAMT,
            BENBASIS,
            BENLMT,
            BENLMTB,
            GCBENPY,
			STRTDT,
			ENDDT,
            GCSYSPY,
            GCSYSRP,
            GCRDRPY,
            HSUMINSU,
			SUMINSU,
            BENPC,
            RASCOY01,
            RASCOY02,
            RASCOY03,
            RASCOY04,
            RASCOY05,
            ACCNUM01,
            ACCNUM02,
            ACCNUM03,
            ACCNUM04,
            ACCNUM05,
            GRHCQSSI01,
            GRHCQSSI02,
            GRHCQSSI03,
            GRHCQSSI04,
            GRHCQSSI05,
			HRSFROM,
			MINUTFROM,
			TIMETYP1,
			HRSTO,
			MINUTTO,
			TIMETYP2,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            GCPROVCD,
		    USRPRF,
            JOBNM,
            DATIME
       FROM VM1DTA.GCBAPF;

commit;