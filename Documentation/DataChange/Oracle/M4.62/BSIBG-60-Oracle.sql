INSERT INTO [VM1DTA].HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9106'
           ,'NCBSI1'
           ,'1'
           ,''
           ,'1'
           ,'This field will be used to store Renewal'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSTIMESTAMP)
GO
INSERT INTO [VM1DTA].HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9106'
           ,'NCBSI1'
           ,'2'
           ,''
           ,'1'
           ,'Bonus Sum Insured amount. User can edit this field.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSTIMESTAMP)

GO
INSERT INTO [VM1DTA].HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9369'
           ,'NCBSI'
           ,'1'
           ,''
           ,'1'
           ,'This field will be used to display Renewal'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSTIMESTAMP)
GO
INSERT INTO [VM1DTA].HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'F'
           ,'S'
           ,'S9369'
           ,'NCBSI'
           ,'2'
           ,''
           ,'1'
           ,'Bonus Sum Insured amount.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSTIMESTAMP)




ALTER TABLE VM1DTA.GXHIPF ADD
NCBSI nchar(17) NULL 


Drop View VM1DTA.GXHIMP;

CREATE VIEW VM1DTA.GXHIMPD(UNIQUE_NUMBER, CHDRCOY, CHDRNUM, HEADNO, MBRNO, PRODTYP, PLANNO, EFFDATE, FMLYCDE, DTEATT, DTETRM, REASONTRM, XCESSSI, APRVDATE, ACCPTDTE, SPECTRM, EXTRPRM, SUMINSU, USERSI, DECFLG, DPNTNO, TERMID, USER_T, TRDT, TRTM, TRANNO, STDPRMLOAD, DTECLAM, EMLOAD, OALOAD, NCBSI, USRPRF, JOBNM, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            HEADNO,
            MBRNO,
            PRODTYP,
            PLANNO,
            EFFDATE,
            FMLYCDE,
            DTEATT,
            DTETRM,
            REASONTRM,
            XCESSSI,
            APRVDATE,
            ACCPTDTE,
            SPECTRM,
            EXTRPRM,
            SUMINSU,
            USERSI,
            DECFLG,
            DPNTNO,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            TRANNO,
            STDPRMLOAD,
            DTECLAM,
            EMLOAD,
            OALOAD,
            NCBSI,
            USRPRF,
            JOBNM,
            DATIME
	 FROM VM1DTA.GXHIPF;

Drop View VM1DTA.GXHIMDP;



 

CREATE VIEW VM1DTA.GXHIMDP(UNIQUE_NUMBER, CHDRCOY, CHDRNUM, MBRNO, PRODTYP, PLANNO, EFFDATE, HEADNO, FMLYCDE, DTEATT, DTETRM, REASONTRM, XCESSSI, APRVDATE, ACCPTDTE, SPECTRM, EXTRPRM, SUMINSU,NCBSI, USERSI, DECFLG, DPNTNO, TERMID, USER_T, TRDT, TRTM, TRANNO, EMLOAD, OALOAD, BILLACTN, IMPAIRCD01, IMPAIRCD02, IMPAIRCD03, RIEMLOAD, RIOALOAD, RIPROCDT, STDPRMLOAD, DTECLAM, USRPRF, JOBNM, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            MBRNO,
            PRODTYP,
            PLANNO,
            EFFDATE,
            HEADNO,
            FMLYCDE,
            DTEATT,
            DTETRM,
            REASONTRM,
            XCESSSI,
            APRVDATE,
            ACCPTDTE,
            SPECTRM,
            EXTRPRM,
            SUMINSU,
            NCBSI,
            USERSI,
            DECFLG,
            DPNTNO,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            TRANNO,
            EMLOAD,
            OALOAD,
            BILLACTN,
            IMPAIRCD01,
            IMPAIRCD02,
            IMPAIRCD03,
            RIEMLOAD,
            RIOALOAD,
            RIPROCDT,
            STDPRMLOAD,
            DTECLAM,
            USRPRF,
            JOBNM,
            DATIME

       FROM VM1DTA.GXHIPF;



Drop View VM1DTA.GXHI;

 

CREATE VIEW VM1DTA.GXHI(UNIQUE_NUMBER, CHDRCOY, CHDRNUM, HEADNO, MBRNO, PRODTYP, PLANNO, EFFDATE, FMLYCDE, DTEATT, DTETRM, REASONTRM, XCESSSI, APRVDATE, ACCPTDTE, SPECTRM, EXTRPRM, SUMINSU, USERSI, DECFLG, DPNTNO, TERMID, USER_T, TRDT, TRTM, TRANNO, STDPRMLOAD, DTECLAM, EMLOAD, OALOAD, NCBSI, USRPRF, JOBNM, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            HEADNO,
            MBRNO,
            PRODTYP,
            PLANNO,
            EFFDATE,
            FMLYCDE,
            DTEATT,
            DTETRM,
            REASONTRM,
            XCESSSI,
            APRVDATE,
            ACCPTDTE,
            SPECTRM,
            EXTRPRM,
            SUMINSU,
            USERSI,
            DECFLG,
            DPNTNO,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            TRANNO,
            STDPRMLOAD,
            DTECLAM,
            EMLOAD,
            OALOAD,
            NCBSI,
            USRPRF,
            JOBNM,
            DATIME
	 FROM VM1DTA.GXHIPF;

