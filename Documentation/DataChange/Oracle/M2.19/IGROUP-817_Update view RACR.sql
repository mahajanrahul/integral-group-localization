
  CREATE OR REPLACE FORCE VIEW "VM1DTA"."RACR" ("UNIQUE_NUMBER", "CLNTPFX", "CLNTCOY", "CLNTNUM", "LRKCLS", "CURRFROM", "CURRTO", "RSTAT", "CURRCODE", "VALIDFLAG", "RETN", "DISCRETN", "USRPRF", "JOBNM", "DATIME") AS 
  SELECT UNIQUE_NUMBER,
            CLNTPFX,
            CLNTCOY,
            CLNTNUM,
            LRKCLS,
            CURRFROM,
            CURRTO,
            RSTAT,
            CURRCODE,
            VALIDFLAG,
            RETN,
            DISCRETN,
            USRPRF,
            JOBNM,
            DATIME
       FROM RACRPF
      WHERE RSTAT = 'A' and
            VALIDFLAG = '1'
   ORDER BY CLNTPFX,
            CLNTCOY,
            CLNTNUM,
            LRKCLS,
            CURRFROM,
            UNIQUE_NUMBER DESC
 ;
 commit;
