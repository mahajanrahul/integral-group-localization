drop trigger "VM1DTA"."TR_CLNTPF";

CREATE OR REPLACE TRIGGER "VM1DTA"."TR_CLNTPF" before insert on CLNTPF for each row
declare
       v_pkValue  number;
     begin
     select SEQ_CLNTPF.nextval into v_pkValue from dual;
     :New.unique_number := v_pkValue;
   end TR_CLNTPF;

drop index "VM1DTA"."CLNT";

exec dbms_stats.unlock_table_stats('vm1dta', 'CLNTPF');

CREATE INDEX "VM1DTA"."CLNT" ON "VM1DTA"."CLNTPF" ("CLNTPFX", "CLNTCOY", "CLNTNUM", "UNIQUE_NUMBER" DESC) 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;

exec dbms_stats.lock_table_stats('vm1dta', 'CLNTPF');

commit;