﻿set define off;
update EROR set erordesc = '処理が取り消されました。' WHERE 1=1  and ERORPFX='ER' and ERORCOY=' ' and ERORLANG='J' and ERORPROG='          ' and EROREROR='AT0000' ;	
update EROR set erordesc = 'Claims Recovery &1 を照会しました' WHERE 1=1  and ERORPFX='ER' and ERORCOY=' ' and ERORLANG='J' and ERORPROG='          ' and EROREROR='CT4AB0' ;
update EROR set erordesc = 'Claims Recovery &1 を更新しました' WHERE 1=1  and ERORPFX='ER' and ERORCOY=' ' and ERORLANG='J' and ERORPROG='          ' and EROREROR='CT4AA0';
update EROR set erordesc = 'Claims Recovery &1 を作成しました' WHERE 1=1  and ERORPFX='ER' and ERORCOY=' ' and ERORLANG='J' and ERORPROG='          ' and EROREROR='CT4A90';
update EROR set erordesc = 'Register &1 を照会しました' WHERE 1=1  and ERORPFX='ER' and ERORCOY=' ' and ERORLANG='J' and ERORPROG='          ' and EROREROR='CT4A20';
update EROR set erordesc = 'Register &1 を更新しました' WHERE 1=1  and ERORPFX='ER' and ERORCOY=' ' and ERORLANG='J' and ERORPROG='          ' and EROREROR='CT4A10';
update EROR set erordesc = 'Register &1 を作成しました' WHERE 1=1  and ERORPFX='ER' and ERORCOY=' ' and ERORLANG='J' and ERORPROG='          ' and EROREROR='CT4A00';
update EROR set erordesc = 'プレフィックスは”D”です。' WHERE 1=1  and ERORPFX='ER' and ERORCOY=' ' and ERORLANG='J' and ERORPROG='          ' and EROREROR='G707';

update descpf set longdesc = '入金入力画面（現金）' where desctabl = 'T1688' and descitem = 'T204' and desccoy = '3' and language = 'J';
set define on;