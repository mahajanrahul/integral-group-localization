/* ERROR */

	Insert into VM1DTA.ERORPF
           (ERORPFX
           ,ERORCOY
           ,ERORLANG
           ,ERORPROG
           ,EROREROR
           ,ERORDESC
           ,TRDT
           ,TRTM
           ,USERID
           ,TERMINALID
           ,USRPRF
           ,JOBNM
           ,DATIME
           ,ERORFILE)
     values
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFS8'
           ,'From Date must > Last Run date in TM955'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');



-- Help lines for Screen SQ954
Insert into VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     values
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ954'
           ,''
           ,'1'
           ,''
           ,'1'
           ,'This table is to store the '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);

Insert into VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     values
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ954'
           ,''
           ,'2'
           ,''
           ,'1'
           ,'GL codes associated to the '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);		   


Insert into VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     values
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ954'
           ,''
           ,'3'
           ,''
           ,'1'
           ,'reverse charging mechanism. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);

Insert into VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     values
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ954'
           ,''
           ,'4'
           ,''
           ,'1'
           ,'It will be referenced by the '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
Insert into VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     values
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ954'
           ,''
           ,'5'
           ,''
           ,'1'
           ,'Auto-journal for Reverse Charge batch job.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);


-- Help lines for Z6TAXTYP screen SQ954
Insert into VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     values
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ954'
           ,'Z6TAXTYP'
           ,'1'
           ,''
           ,'1'
           ,'This is Tax Type used for '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);

Insert into VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     values
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ954'
           ,'Z6TAXTYP'
           ,'2'
           ,''
           ,'1'
           ,'MY GST Reverse Charge. It must be '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);		   

Insert into VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     values
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ954'
           ,'Z6TAXTYP'
           ,'3'
           ,''
           ,'1'
           ,'a valid item in TM207 table.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);


-- Help lines for SACSCODE screen SQ954
Insert into VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     values
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ954'
           ,'SACSCODE'
           ,'1'
           ,''
           ,'1'
           ,'This is the Sub Account Code '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);

Insert into VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     values
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ954'
           ,'SACSCODE'
           ,'2'
           ,''
           ,'1'
           ,'used for MY GST Reverse Charge. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
Insert into VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     values
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ954'
           ,'SACSCODE'
           ,'2'
           ,''
           ,'1'
           ,'It must be a valid entry in T3616 table.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);


-- Help lines for SACSTYP screen SQ954
Insert into VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     values
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ954'
           ,'SACSTYP'
           ,'1'
           ,''
           ,'1'
           ,'This is the Sub Account Type '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);

Insert into VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     values
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ954'
           ,'SACSTYP'
           ,'1'
           ,''
           ,''
           ,'used for MY GST Reverse Charge. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
           
Insert into VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     values
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ954'
           ,'SACSTYP'
           ,'2'
           ,''
           ,'1'
           ,'It must be a valid item in T3695 table.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);


-- Help lines for Z6GLCDA screen SQ954
Insert into VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     values
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ954'
           ,'Z6GLCDA'
           ,'1'
           ,''
           ,'1'
           ,'This is the GL Code to be '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);

Insert into VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     values
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ954'
           ,'Z6GLCDA'
           ,'2'
           ,''
           ,'1'
           ,'defaulted for MY GST Reverse '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
Insert into VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     values
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ954'
           ,'Z6GLCDA'
		   ,'3'
           ,''
           ,'1'
           ,'Charge. It must be a valid GL Account Code.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);


-- Help lines for Z6GLCDB screen SQ954
Insert into VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     values
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ954'
           ,'Z6GLCDB'
           ,'1'
           ,''
           ,'1'
           ,'This is the GL Code to be '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);

Insert into VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     values
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ954'
           ,'Z6GLCDB'
           ,'2'
           ,''
           ,'1'
           ,'defaulted for MY GST Reverse '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
Insert into VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     values
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ954'
           ,'Z6GLCDB'
		   ,'3'
           ,''
           ,'1'
           ,'Charge. It must be a valid GL Account Code.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);


-- Help lines for Z6TAXCDE screen SQ954
Insert into VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     values
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ954'
           ,'Z6TAXCDE'
           ,'1'
           ,''
           ,'1'
           ,'This is Tax Code to be used '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);

Insert into VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     values
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ954'
           ,'Z6TAXCDE'
           ,'2'
           ,''
           ,'1'
           ,'for MY GST Reverse Charge. It '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);		   

Insert into VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     values
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ954'
           ,'Z6TAXCDE'
           ,'2'
           ,''
		   ,'1'
           ,'must be a valid item in TM202 table.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);



-----create view ZTRNGTD

CREATE VIEW VM1DTA.ZTRNGTD(UNIQUE_NUMBER, BATCPFX, BATCCOY, BATCBRN, BATCACTYR, BATCACTMN, BATCTRCDE, BATCBATCH, RLDGPFX, RLDGCOY, RLDGACCT, ORIGCURR, ACCTCURR, CRATE, CNTTYPE, TRANNO, CNTBRANCH, STCA, STCB, STCC, STCD, STCE, POSTMONTH, POSTYEAR, TRANDATE, TRANTIME, EFFDATE, SACSCODE, SACSTYP01, SACSTYP02, SACSTYP03, SACSTYP04, SACSTYP05, SACSTYP06, SACSTYP07, SACSTYP08, SACSTYP09, SACSTYP10, SACSTYP11, SACSTYP12, SACSTYP13, SACSTYP14, SACSTYP15, TRANAMT01, TRANAMT02, TRANAMT03, TRANAMT04, TRANAMT05, TRANAMT06, TRANAMT07, TRANAMT08, TRANAMT09, TRANAMT10, TRANAMT11, TRANAMT12, TRANAMT13, TRANAMT14, TRANAMT15, GLSIGN01, GLSIGN02, GLSIGN03, GLSIGN04, GLSIGN05, GLSIGN06, GLSIGN07, GLSIGN08, GLSIGN09, GLSIGN10, GLSIGN11, GLSIGN12, GLSIGN13, GLSIGN14, GLSIGN15, GENLCUR, GENLPFX, GENLCOY, GLCODE01, GLCODE02, GLCODE03, GLCODE04, GLCODE05, GLCODE06, GLCODE07, GLCODE08, GLCODE09, GLCODE10, GLCODE11, GLCODE12, GLCODE13, GLCODE14, GLCODE15, ACPFX, ACCOY, ACNUM, CCDATE, EXDATE, LIFE, COVERAGE, RIDER, ACCTYP, ORAGNT, BANKCODE, DOCTPFX, DOCTCOY, DOCTNUM, INSTNO, BILLFREQ, STATREASN, PRTFLG, RITYPE, RDOCPFX, RDOCCOY, RDOCNUM, NOSCHCPY, JOBNM, USRPRF, DATIME) AS
SELECT UNIQUE_NUMBER,
            BATCPFX,
            BATCCOY,
            BATCBRN,
            BATCACTYR,
            BATCACTMN,
            BATCTRCDE,
            BATCBATCH,
            RLDGPFX,
            RLDGCOY,
            RLDGACCT,
            ORIGCURR,
            ACCTCURR,
            CRATE,
            CNTTYPE,
            TRANNO,
            CNTBRANCH,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            POSTMONTH,
            POSTYEAR,
            TRANDATE,
            TRANTIME,
            EFFDATE,
            SACSCODE,
            SACSTYP01,
            SACSTYP02,
            SACSTYP03,
            SACSTYP04,
            SACSTYP05,
            SACSTYP06,
            SACSTYP07,
            SACSTYP08,
            SACSTYP09,
            SACSTYP10,
            SACSTYP11,
            SACSTYP12,
            SACSTYP13,
            SACSTYP14,
            SACSTYP15,
            TRANAMT01,
            TRANAMT02,
            TRANAMT03,
            TRANAMT04,
            TRANAMT05,
            TRANAMT06,
            TRANAMT07,
            TRANAMT08,
            TRANAMT09,
            TRANAMT10,
            TRANAMT11,
            TRANAMT12,
            TRANAMT13,
            TRANAMT14,
            TRANAMT15,
            GLSIGN01,
            GLSIGN02,
            GLSIGN03,
            GLSIGN04,
            GLSIGN05,
            GLSIGN06,
            GLSIGN07,
            GLSIGN08,
            GLSIGN09,
            GLSIGN10,
            GLSIGN11,
            GLSIGN12,
            GLSIGN13,
            GLSIGN14,
            GLSIGN15,
            GENLCUR,
            GENLPFX,
            GENLCOY,
            GLCODE01,
            GLCODE02,
            GLCODE03,
            GLCODE04,
            GLCODE05,
            GLCODE06,
            GLCODE07,
            GLCODE08,
            GLCODE09,
            GLCODE10,
            GLCODE11,
            GLCODE12,
            GLCODE13,
            GLCODE14,
            GLCODE15,
            ACPFX,
            ACCOY,
            ACNUM,
            CCDATE,
            EXDATE,
            LIFE,
            COVERAGE,
            RIDER,
            ACCTYP,
            ORAGNT,
            BANKCODE,
            DOCTPFX,
            DOCTCOY,
            DOCTNUM,
            INSTNO,
            BILLFREQ,
            STATREASN,
            PRTFLG,
            RITYPE,
            RDOCPFX,
            RDOCCOY,
            RDOCNUM,
            NOSCHCPY,
            JOBNM,
            USRPRF,
            DATIME
       FROM VM1DTA.ZTRNPF 
	   WHERE (SACSCODE = '' AND STCB = 'FOO') OR (SACSCODE = 'GR' AND STCB = 'FIO');




		   