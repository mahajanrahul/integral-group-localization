
/*TABLE*/
/*ALTER TABLE BNFHPF*/
Alter Table BNFHPF Add
(OPTBENEFIT NCHAR(1) NULL);


/*CREATE TABLE RSLLPF*/

CREATE TABLE RSLLPF(
	UNIQUE_NUMBER NUMBER(18,0) NOT NULL,
	CHDRCOY nchar(1) NULL,
	CHDRNO nchar(8) NULL,
	VFLAG nchar(1) NULL,
	CNTTYP nchar(3) NULL,
	MBRNO nchar(5) NULL,
	DPNTNO nchar(2) NULL,
	CLNTNUM nchar(8) NULL,
	SUBSNUM nchar(8) NULL,
	STDATE INT NULL,
	PRODTYP NCHAR(4) NULL,
	REOVANLIM int NULL,
	STLOANLIM01 INT NULL,
	STLOANLIM02 INT NULL,
	STLOANLIM03 INT NULL,
	STLOANLIM04 INT NULL,
	STLOANLIM05 INT NULL,
	GDIAGCD01 nchar(5) NULL,
	GDIAGCD02 nchar(5) NULL,
	GDIAGCD03 nchar(5) NULL,
	GDIAGCD04 nchar(5) NULL,
	GDIAGCD05 nchar(5) NULL,
	USRPRF nchar(10) NULL,
	JOBNM nchar(10) NULL,
	DATIME TIMESTAMP(6) NULL,
CONSTRAINT PK_RSLLPF PRIMARY KEY(	UNIQUE_NUMBER));

CREATE SEQUENCE SEQ_RSLLPF MINVALUE 1000000 MAXVALUE 99999999999999999999 INCREMENT BY 1 START WITH 3002058 NOCACHE NOORDER NOCYCLE;


CREATE OR REPLACE TRIGGER TR_RSLLPF before
  INSERT ON RSLLPF FOR EACH row DECLARE v_pkValue NUMBER;
BEGIN
  SELECT SEQ_RSLLPF.nextval INTO v_pkValue FROM dual;
  :New.unique_number := v_pkValue;
END TR_RSLLPF;
/

/*CREATE TABLE ADLMPF*/
CREATE TABLE ADLMPF(
	UNIQUE_NUMBER NUMBER(18,0) NOT NULL,
	CHDRCOY nchar(1) NULL,
	CHDRNO nchar(8) NULL,
	MBRNO nchar(5) NULL,
	CNTTYP nchar(3) NULL,
	CLMNO nchar(8) NULL,	
	CLNTNUM nchar(8) NULL,
	SUBSNUM nchar(8) NULL,
	GCOCCNO nchar(2) NULL,
	DPNTNO nchar(2) NULL,
	CCDATE INT NULL,
	CRDATE INT NULL,
	REOVANLIM INT NULL,
	STLOANLIM01 INT NULL,
	STLOANLIM02 INT NULL,
	STLOANLIM03 INT NULL,
	STLOANLIM04 INT NULL,
	STLOANLIM05 INT NULL,
	GDIAGCD01 nchar(5) NULL,
	GDIAGCD02 nchar(5) NULL,
	GDIAGCD03 nchar(5) NULL,
	GDIAGCD04 nchar(5) NULL,
	GDIAGCD05 nchar(5) NULL,
	AMTPAY01 INT NULL,
	AMTPAY02 INT NULL,
	AMTPAY03 INT NULL,
	AMTPAY04 INT NULL,
	AMTPAY05 INT NULL,
	BALANCE INT NULL,
	BALANCE01 INT NULL,
	BALANCE02 INT NULL,
	BALANCE03 INT NULL,
	BALANCE04 INT NULL,
	BALANCE05 INT NULL,
	USRPRF nchar(10) NULL,
	JOBNM nchar(10) NULL,
	DATIME TIMESTAMP(6) NULL,
CONSTRAINT PK_ADLMPF PRIMARY KEY(	UNIQUE_NUMBER));

CREATE SEQUENCE SEQ_ADLMPF MINVALUE 1000000 MAXVALUE 99999999999999999999 INCREMENT BY 1 START WITH 3002058 NOCACHE NOORDER NOCYCLE;


CREATE OR REPLACE TRIGGER TR_ADLMPF before
  INSERT ON ADLMPF FOR EACH row DECLARE v_pkValue NUMBER;
BEGIN
  SELECT SEQ_ADLMPF.nextval INTO v_pkValue FROM dual;
  :New.unique_number := v_pkValue;
END TR_ADLMPF;
/

/*CREATE TABLE OPBNPF*/
CREATE TABLE OPBNPF(
	UNIQUE_NUMBER NUMBER(18,0) NOT NULL,
	CHDRCOY nchar(1) NULL,
	CHDRNO nchar(8) NULL,
	VFLAG nchar(1) NULL,
	CNTTYP nchar(3) NULL,
	MBRNO nchar(5) NULL,
	DPNTNO nchar(2) NULL,
	CLNTNUM nchar(8) NULL,
	SUBSNUM nchar(8) NULL,
	STDATE INT NULL,
    SFSTDATE  INT NULL,
	SFENDDATE INT NULL,
	PRODTYP NCHAR(4) NULL,
	PLANNO NCHAR(3) NULL,
	BENCDE NCHAR(4) NULL,
	SIAMT NUMERIC(6,2) NULL,
	WEEKSIAMT NUMERIC(6,2) NULL,
	MINSIWEEK NUMERIC(6,2) NULL,
	PREMAMT NUMERIC(6,2) NULL,
	PREMRATE NUMERIC(6,2) NULL,
	PREMCALC NUMERIC(6,2) NULL,
	ACTION nchar(1) NULL,
	TYPE  nchar(1) NULL,
	USRPRF nchar(10) NULL,
	JOBNM nchar(10) NULL,
	DATIME TIMESTAMP(6) NULL,
CONSTRAINT PK_OPBNPF PRIMARY KEY(UNIQUE_NUMBER));

CREATE SEQUENCE SEQ_OPBNPF MINVALUE 1000000 MAXVALUE 99999999999999999999 INCREMENT BY 1 START WITH 3002058 NOCACHE NOORDER NOCYCLE;


CREATE OR REPLACE TRIGGER TR_OPBNPF before
  INSERT ON OPBNPF FOR EACH row DECLARE v_pkValue NUMBER;
BEGIN
  SELECT SEQ_OPBNPF.nextval INTO v_pkValue FROM dual;
  :New.unique_number := v_pkValue;
END TR_OPBNPF;
/

/*CREATE TABLE DIWPPF*/
CREATE TABLE DIWPPF(
	UNIQUE_NUMBER NUMBER(18,0) NOT NULL,
	CHDRCOY nchar(1) NULL,
	CHDRNO nchar(8) NULL,
	VFLAG nchar(1) NULL,
	MBRNO nchar(5) NULL,
	DPNTNO nchar(2) NULL,
	CLNTNUM nchar(8) NULL,
	SUBSNUM nchar(8) NULL,
	STDATE INT NULL,
	GDIAGCD01 nchar(5) NULL,
	GDIAGCD02 nchar(5) NULL,
	GDIAGCD03 nchar(5) NULL,
	GDIAGCD04 nchar(5) NULL,
	GDIAGCD05 nchar(5) NULL,
	WAITPERI01 INT NULL,
	WAITPERI02 INT NULL,
	WAITPERI03 INT NULL,
	WAITPERI04 INT NULL,
	WAITPERI05 INT NULL,
	USRPRF nchar(10) NULL,
	JOBNM nchar(10) NULL,
	DATIME TIMESTAMP(6) NULL,
CONSTRAINT PK_DIWPPF PRIMARY KEY (UNIQUE_NUMBER));


CREATE SEQUENCE SEQ_DIWPPF MINVALUE 1000000 MAXVALUE 99999999999999999999 INCREMENT BY 1 START WITH 3002058 NOCACHE NOORDER NOCYCLE;


CREATE OR REPLACE TRIGGER TR_DIWPPF before
  INSERT ON DIWPPF FOR EACH row DECLARE v_pkValue NUMBER;
BEGIN
  SELECT SEQ_DIWPPF.nextval INTO v_pkValue FROM dual;
  :New.unique_number := v_pkValue;
END TR_DIWPPF;
/

/*VIEW*/

/*ALTER VIEW BNFH*/

CREATE OR REPLACE VIEW BNFH(UNIQUE_NUMBER, CHDRCOY, CHDRNUM, PRODTYP, PLANNO, BENCDE, BNFTLST, STRTDT, ENDDT, DEDUCTYPE, NOVISITS, TRANNO, USRPRF, JOBNM, OPTBENEFIT, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            PRODTYP,
            PLANNO,
            BENCDE,
            BNFTLST,
            STRTDT,
            ENDDT,
            DEDUCTYPE,
            NOVISITS,
            TRANNO,
            USRPRF,
            JOBNM,
			OPTBENEFIT,
            DATIME
       FROM BNFHPF;



/*CREATE VIEW RSLL*/

CREATE VIEW RSLL(UNIQUE_NUMBER, CHDRCOY, CHDRNO, VFLAG, CNTTYP, MBRNO, 
DPNTNO, CLNTNUM, SUBSNUM, STDATE, PRODTYP, REOVANLIM, STLOANLIM01, STLOANLIM02, 
STLOANLIM03, STLOANLIM04, STLOANLIM05, GDIAGCD01, GDIAGCD02, GDIAGCD03, GDIAGCD04, GDIAGCD05, USRPRF, JOBNM, DATIME) AS
SELECT UNIQUE_NUMBER
      ,CHDRCOY
      ,CHDRNO
      ,VFLAG
      ,CNTTYP
      ,MBRNO
      ,DPNTNO
      ,CLNTNUM
      ,SUBSNUM
      ,STDATE
      ,PRODTYP
      ,REOVANLIM
      ,STLOANLIM01
      ,STLOANLIM02
      ,STLOANLIM03
      ,STLOANLIM04
      ,STLOANLIM05
      ,GDIAGCD01
      ,GDIAGCD02
      ,GDIAGCD03
      ,GDIAGCD04
      ,GDIAGCD05
      ,USRPRF
      ,JOBNM
      ,DATIME
  FROM RSLLPF;

/*CREATE VIEW ADLM*/

CREATE VIEW ADLM(UNIQUE_NUMBER,CHDRCOY,CHDRNO,CNTTYP,MBRNO,DPNTNO,CLNTNUM,SUBSNUM,CLMNO,GCOCCNO,CCDATE,CRDATE,REOVANLIM,STLOANLIM01,
STLOANLIM02,STLOANLIM03,STLOANLIM04,STLOANLIM05,GDIAGCD01,GDIAGCD02,GDIAGCD03,GDIAGCD04,GDIAGCD05,AMTPAY01,AMTPAY02,AMTPAY03,AMTPAY04,AMTPAY05,
BALANCE,BALANCE01,BALANCE02,BALANCE03,BALANCE04,BALANCE05,USRPRF,JOBNM,DATIME) 
AS
SELECT UNIQUE_NUMBER
      ,CHDRCOY
      ,CHDRNO
      ,CNTTYP
      ,MBRNO
      ,DPNTNO
      ,CLNTNUM
      ,SUBSNUM
      ,CLMNO
      ,GCOCCNO
      ,CCDATE
      ,CRDATE
	  ,REOVANLIM
      ,STLOANLIM01
      ,STLOANLIM02
      ,STLOANLIM03
      ,STLOANLIM04
      ,STLOANLIM05
      ,GDIAGCD01
      ,GDIAGCD02
      ,GDIAGCD03
      ,GDIAGCD04
      ,GDIAGCD05
      ,AMTPAY01
      ,AMTPAY02
      ,AMTPAY03
      ,AMTPAY04
      ,AMTPAY05
      ,BALANCE
      ,BALANCE01
      ,BALANCE02
      ,BALANCE03
      ,BALANCE04
      ,BALANCE05
      ,USRPRF
      ,JOBNM
      ,DATIME
  FROM ADLMPF;
/*CREATE VIEW OPBN*/

CREATE VIEW OPBN (UNIQUE_NUMBER,CHDRCOY,CHDRNO,VFLAG,CNTTYP,MBRNO,DPNTNO,CLNTNUM,SUBSNUM,STDATE,PRODTYP,PLANNO,BENCDE,SIAMT,WEEKSIAMT,MINSIWEEK,PREMAMT,PREMRATE,PREMCALC,SFSTDATE,SFENDDATE,ACTION,TYPE,USRPRF,JOBNM,DATIME)
AS
SELECT UNIQUE_NUMBER
      ,CHDRCOY
      ,CHDRNO
      ,VFLAG
      ,CNTTYP
      ,MBRNO
      ,DPNTNO
      ,CLNTNUM
      ,SUBSNUM
      ,STDATE
      ,PRODTYP
      ,PLANNO
      ,BENCDE
      ,SIAMT
      ,WEEKSIAMT
      ,MINSIWEEK
      ,PREMAMT
      ,PREMRATE
      ,PREMCALC
	  ,SFSTDATE 
	  ,SFENDDATE
      ,ACTION
	  ,TYPE
      ,USRPRF
      ,JOBNM
      ,DATIME
  FROM OPBNPF;
/*CREATE VIEW DIWP*/

CREATE VIEW DIWP (UNIQUE_NUMBER,CHDRCOY,CHDRNO,DPNTNO,VFLAG,MBRNO,CLNTNUM,SUBSNUM,STDATE,GDIAGCD01,GDIAGCD02,GDIAGCD03,GDIAGCD04,
GDIAGCD05,WAITPERI01,WAITPERI02,WAITPERI03,WAITPERI04,WAITPERI05,USRPRF,JOBNM,DATIME)
AS
SELECT UNIQUE_NUMBER
      ,CHDRCOY
      ,CHDRNO
	  ,DPNTNO
      ,VFLAG
      ,MBRNO 
      ,CLNTNUM
      ,SUBSNUM
      ,STDATE
      ,GDIAGCD01
      ,GDIAGCD02
      ,GDIAGCD03
      ,GDIAGCD04
      ,GDIAGCD05
      ,WAITPERI01
      ,WAITPERI02
      ,WAITPERI03
      ,WAITPERI04
      ,WAITPERI05
      ,USRPRF
      ,JOBNM
      ,DATIME
  FROM DIWPPF;

/* ERROR */

	INSERT INTO ERORPF
           (ERORPFX
           ,ERORCOY
           ,ERORLANG
           ,ERORPROG
           ,EROREROR
           ,ERORDESC
           ,TRDT
           ,TRTM
           ,USERID
           ,TERMINALID
           ,USRPRF
           ,JOBNM
           ,DATIME
           ,ERORFILE)
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFQ5'
           ,'Visit Reduce/Stop Loss Limits'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,SYSDATE
           ,'');


INSERT INTO ERORPF
           (ERORPFX
           ,ERORCOY
           ,ERORLANG
           ,ERORPROG
           ,EROREROR
           ,ERORDESC
           ,TRDT
           ,TRTM
           ,USERID
           ,TERMINALID
           ,USRPRF
           ,JOBNM
           ,DATIME
           ,ERORFILE)
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFRY'
           ,'Adjust Limits'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,SYSDATE
           ,'');


INSERT INTO ERORPF
           (ERORPFX
           ,ERORCOY
           ,ERORLANG
           ,ERORPROG
           ,EROREROR
           ,ERORDESC
           ,TRDT
           ,TRTM
           ,USERID
           ,TERMINALID
           ,USRPRF
           ,JOBNM
           ,DATIME
           ,ERORFILE)
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFRZ'
           ,'Invalid for Health Benefit'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,SYSDATE
           ,'');

/*HELP TEXT*/
/*SQ931*/
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ931'
           ,''
           ,'1'
           ,''
           ,'1'
           ,'This screen is used to maintain '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);

INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ931'
           ,''
           ,'2'
           ,''
           ,'1'
           ,'the Reduced Overall Annual Limit '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);

INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ931'
           ,''
           ,'3'
           ,''
           ,'1'
           ,'and/or the Stop Loss Limit(s) '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);

INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ931'
           ,''
           ,'4'
           ,''
           ,'1'
           ,'of the selected member.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);


/*SQ932*/
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ932'
           ,''
           ,'1'
           ,''
           ,'1'
           ,'This screen will display the original '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);

INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ932'
           ,''
           ,'2'
           ,''
           ,'1'
           ,'Reduced Overall Annual Limit and/or  '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);

INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ932'
           ,''
           ,'3'
           ,''
           ,'1'
           ,'the Stop Loss Limit(s) and the respective '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);

INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'
           ,'SQ931'
           ,''
           ,'4'
           ,''
           ,'1'
           ,'balance of the claimant.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);


/*FIELD IN SQ931*/
/*Policy */
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'CHDRNO'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field which displays'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);

INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'CHDRNO'
           ,'2'
           ,''
           ,'1'
           ,'the Policy Number, Policy Type'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);

INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'CHDRNO'
           ,'3'
           ,''
           ,'1'
           ,'and Policy Type description.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
/*Member*/
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'MBRNO'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field for which displays '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'MBRNO'
           ,'2'
           ,''
           ,'1'
           ,'the Member No. and member�s name of the  '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'MBRNO'
           ,'3'
           ,''
           ,'1'
           ,'selected member.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
/*Subsidiary*/
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'SUBSNUM'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field which displays the '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'SUBSNUM'
           ,'2'
           ,''
           ,'1'
           ,'Subsidiary which the member is under (if any).'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
/*Start Date*/
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'CCDATE'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field which displays the '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'CCDATE'
           ,'2'
           ,''
           ,'1'
           ,'Start Date of the member. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
/*Reduced Overall Annual Limit*/
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'REOVANLIM'
           ,'1'
           ,''
           ,'1'
           ,'This field is for user to enter in the Reduced  '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'REOVANLIM'
           ,'2'
           ,''
           ,'1'
           ,'Overall Annual Limit of the member.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
/*Stop Loss Annual Limit*/
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'STLOANLIM'
           ,'1'
           ,''
           ,'1'
           ,'This field is for user to enter in the Stop'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'STLOANLIM'
           ,'2'
           ,''
           ,'1'
           ,'Loss Annual Limit of the member.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
/*Diagnosis Code*/
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'GDIAGCD'
           ,'1'
           ,''
           ,'1'
           ,'This field is for user to enter in the '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'GDIAGCD'
           ,'2'
           ,''
           ,'1'
           ,'Diagnosis Code against the respective Stop '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'GDIAGCD'
           ,'3'
           ,''
           ,'1'
           ,'Loss Annual Limit of the member. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'GDIAGCD'
           ,'4'
           ,''
           ,'1'
           ,'A search can be performed which will '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ931'
           ,'GDIAGCD'
           ,'5'
           ,''
           ,'1'
           ,'display the Diagnosis Code T9697 search screen, SR9IY. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);

/*Field for SQ932*/
/*Policy */
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'CHDRNO'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field which displays'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);

INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'CHDRNO'
           ,'2'
           ,''
           ,'1'
           ,'the Policy Number, Policy Type'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);

INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'CHDRNO'
           ,'3'
           ,''
           ,'1'
           ,'and Policy Type description.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
/*Member*/
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'MBRNO'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field for which displays'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'MBRNO'
           ,'2'
           ,''
           ,'1'
           ,'the Member No. with the dependant no. and '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'MBRNO'
           ,'3'
           ,''
           ,'1'
           ,'member�s name of the selected member.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
/*Subsidiary*/
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'SUBSNUM'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field which displays the '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'SUBSNUM'
           ,'2'
           ,''
           ,'1'
           ,'Subsidiary which the member is under (if any).'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
/*Claim Number*/
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'CLMNO'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field which displays the '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);

INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'CLMNO'
           ,'2'
           ,''
           ,'1'
           ,'Claim Number of the current claim. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
/*Reduced Overall Annual Limit*/
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'REOVANLIM'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field which displays  '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'REOVANLIM'
           ,'2'
           ,''
           ,'1'
           ,'Reduced Overall Annual Limit of the member.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
/*Stop Loss Annual Limit*/
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'STLOANLIM'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field which displays Stop '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'STLOANLIM'
           ,'2'
           ,''
           ,'1'
           ,'Loss Annual Limit of the member.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
/*Diagnosis Code*/
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'GDIAGCD'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field which displays the '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'GDIAGCD'
           ,'2'
           ,''
           ,'1'
           ,'Diagnosis Code and the description of the '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'GDIAGCD'
           ,'3'
           ,''
           ,'1'
           ,'respective Stop Loss Annual Limit.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
/*Balance*/
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'BALANCE'
           ,'1'
           ,''
           ,'1'
           ,'This field will display the balance of the'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);

INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'BALANCE'
           ,'2'
           ,''
           ,'1'
           ,'Reduced Overall Annual Limit/ Stop Loss '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ932'
           ,'BALANCE'
           ,'3'
           ,''
           ,'1'
           ,'Annual Limit based on the previous Approved Claim.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);

/*Field for SQ934*/
/*Policy */
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'CHDRNO'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field which displays'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);

INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'CHDRNO'
           ,'2'
           ,''
           ,'1'
           ,'the Policy Number, Policy Type'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);

INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'CHDRNO'
           ,'3'
           ,''
           ,'1'
           ,'and Policy Type description.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
/*Member*/
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'MBRNO'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field for which displays'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'MBRNO'
           ,'2'
           ,''
           ,'1'
           ,'the Member No. with the dependant no. and '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'MBRNO'
           ,'3'
           ,''
           ,'1'
           ,'member�s name of the selected member.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
/*Subsidiary*/
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'SUBSNUM'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field which displays the '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'SUBSNUM'
           ,'2'
           ,''
           ,'1'
           ,'Subsidiary which the member is under (if any).'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
/*Start Date*/
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'STDATE'
           ,'1'
           ,''
           ,'1'
           ,'This is a Protected field which displays the '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'CCDATE'
           ,'2'
           ,''
           ,'1'
           ,'Start Date of the member.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
/*Diagnosis Code*/
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'GDIAGCD'
           ,'1'
           ,''
           ,'1'
           ,'This field is for user to enter in the  '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'GDIAGCD'
           ,'2'
           ,''
           ,'1'
           ,'Diagnosis Code against the respective Stop'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'GDIAGCD'
           ,'3'
           ,''
           ,'1'
           ,'Loss Annual Limit of the member. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);

INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'GDIAGCD'
           ,'4'
           ,''
           ,'1'
           ,'A search can be performed which will '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'GDIAGCD'
           ,'5'
           ,''
           ,'1'
           ,'display the Diagnosis Code T9697 search screen, SR9IY. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
/*Waiting Period*/
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'WAITPERI'
           ,'1'
           ,''
           ,'1'
           ,'Waiting Period in number of days to be '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
INSERT INTO HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ934'
           ,'WAITPERI'
           ,'2'
           ,''
           ,'1'
           ,'entered for the respective Diagnosis Code.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,SYSDATE);
COMMIT;
