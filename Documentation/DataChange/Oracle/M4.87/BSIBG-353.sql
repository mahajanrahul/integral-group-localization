DELETE FROM HELPPF WHERE HELPTYPE = 'F' AND HELPPROG = 'SR9B1' AND HELPITEM IN ('DISCRATE1', 'DESDISC1', 'NOINS1', 'ISSTAFF1', 'DISCRATE2', 'DESDISC2', 'NOINS2', 'ISSTAFF2', 'ABPREFLG')
DELETE FROM HELPPF WHERE HELPTYPE = 'F' AND HELPPROG = 'S9114' AND HELPITEM IN ('DISCRATE1', 'DESDISC1', 'NOINS1', 'ISSTAFF1', 'DISCRATE2', 'DESDISC2', 'NOINS2', 'ISSTAFF2', 'ABPREFLG')

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'DISCRATEA'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to capture the Discount 1 percentage'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'DESCAA'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to describe Discount 1.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'NOINSA'
           ,'1'
           ,''
           ,'1'
           ,'This field reflects the first discount'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'NOINSA'
           ,'2'
           ,''
           ,'1'
           ,'to be applied on the policy’s premium.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'DISCRATEB'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to capture the Discount 2 percentage.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'DESCAB'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to describe Discount 2.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'NOINSB'
           ,'1'
           ,''
           ,'1'
           ,'This field reflects the second discount'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'NOINSB'
           ,'2'
           ,''
           ,'1'
           ,'to be applied on the policy’s premium.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'ISSTAFF'
           ,'1'
           ,''
           ,'1'
           ,'This checkbox is to indicate for staff discount.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'ABPREFLG'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to indicate if Absolute Premium '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'ABPREFLG'
           ,'2'
           ,''
           ,'1'
           ,'is applicable for the Product/Plan.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
/* S9114 */

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'DISCRATEA'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to capture the Discount 1 percentage.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'DESCAA'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to describe Discount 1.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP) 

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'NOINSA'
           ,'1'
           ,''
           ,'1'
           ,'This field reflects the first discount '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'NOINSA'
           ,'2'
           ,''
           ,'1'
           ,'to be applied on the policy’s premium.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'DISCRATEB'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to capture the Discount 1 percentage.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'DESCAB'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to describe Discount 2'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'NOINSB'
           ,'1'
           ,''
           ,'1'
           ,'This field reflects the first discount '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'NOINSB'
           ,'2'
           ,''
           ,'1'
           ,'to be applied on the policy’s premium.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'ISSTAFF'
           ,'1'
           ,''
           ,'1'
           ,'This checkbox is to indicate for staff discount.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'ABPREFLG'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to indicate if Absolute Premium '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'ABPREFLG'
           ,'2'
           ,''
           ,'1'
           ,'is applicable for the Product/Plan.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP)
		   
	INSERT INTO FLDTPF(FDID,LANG,FDTX,COLH01,COLH02,COLH03,TERMID,USER_T,TRDT,TRTM,USRPRF,JOBNM,DATIME)
	VALUES('DISCRATEA','E','Discount 1','Discount 1','','','','','','','','',CURRENT_TIMESTAMP);
	INSERT INTO FLDTPF(FDID,LANG,FDTX,COLH01,COLH02,COLH03,TERMID,USER_T,TRDT,TRTM,USRPRF,JOBNM,DATIME)
	VALUES('DISCRATEB','E','Discount 2','Discount 2','','','','','','','','',CURRENT_TIMESTAMP); 
	INSERT INTO FLDTPF(FDID,LANG,FDTX,COLH01,COLH02,COLH03,TERMID,USER_T,TRDT,TRTM,USRPRF,JOBNM,DATIME)
	VALUES('DESCAA','E','Description of Discount 1','Description of ','Discount 1','','','','','','','',CURRENT_TIMESTAMP); 
	INSERT INTO FLDTPF(FDID,LANG,FDTX,COLH01,COLH02,COLH03,TERMID,USER_T,TRDT,TRTM,USRPRF,JOBNM,DATIME)
	VALUES('DESCAB','E','Description of Discount 2','Description of ','Discount 2','','','','','','','',CURRENT_TIMESTAMP);
	INSERT INTO FLDTPF(FDID,LANG,FDTX,COLH01,COLH02,COLH03,TERMID,USER_T,TRDT,TRTM,USRPRF,JOBNM,DATIME)
	VALUES('NOINSA','E','No. of insured family (Disc 1)','No. of insured ','family (Disc 1)','','','','','','','',CURRENT_TIMESTAMP);
	INSERT INTO FLDTPF(FDID,LANG,FDTX,COLH01,COLH02,COLH03,TERMID,USER_T,TRDT,TRTM,USRPRF,JOBNM,DATIME)
	VALUES('NOINSB','E','No. of insured family (Disc 2)','No. of insured ','family (Disc 2)','','','','','','','',CURRENT_TIMESTAMP); 

COMMIT