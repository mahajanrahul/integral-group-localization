/* add column SERVUNIT,PRODTYP into VM1DTA.COVNPF */
ALTER TABLE VM1DTA.COVNPF
ADD (SERVUNIT char(2),PRODTYP char(4));




CREATE OR REPLACE VIEW VM1DTA.COVN(UNIQUE_NUMBER, COMPANY, CNOTNUM, BRANCH, BOOKNUM, STATUS, DTEISS, DTERCV, REGNO, CHASSI, ENGINE, CANRSN, DTEADV, CNTTYPE, KOB, SUMINS, CCDATE, CRDATE, REMARKS01, REMARKS02, REMARKS03, CHDRCOY, CHDRNUM, TRANNO, TRCDE, RSKTYPE, RSKNO, CLNTPFX, CLNTCOY, CLNTNUM, AGNTPFX, AGNTCOY, AGNTNUM, USERID, USRPRF, JOBNM, DATIME, 
		LOSSDT, AMTPAID, SUBUMSAGE,SUBMPLTYP,SUBMDTISS,SUBMINSFRM,SUBMINSTO,SUBMINSNM,SUBMUPDATEDBY,SUBMENTRYDT, ZSTAFFCD, SERVUNIT, PRODTYP) AS
SELECT UNIQUE_NUMBER,
            COMPANY,
            CNOTNUM,
            BRANCH,
            BOOKNUM,
            STATUS,
            DTEISS,
            DTERCV,
            REGNO,
            CHASSI,
            ENGINE,
            CANRSN,
            DTEADV,
            CNTTYPE,
            KOB,
            SUMINS,
            CCDATE,
            CRDATE,
            REMARKS01,
            REMARKS02,
            REMARKS03,
            CHDRCOY,
            CHDRNUM,
            TRANNO,
            TRCDE,
            RSKTYPE,
            RSKNO,
            CLNTPFX,
            CLNTCOY,
            CLNTNUM,
            AGNTPFX,
            AGNTCOY,
            AGNTNUM,
            USERID,
            USRPRF,
            JOBNM,
            DATIME,
			LOSSDT, 
			AMTPAID, 
			SUBUMSAGE,
			SUBMPLTYP,
			SUBMDTISS,
			SUBMINSFRM,
			SUBMINSTO,
			SUBMINSNM,
			SUBMUPDATEDBY,
			SUBMENTRYDT,
			ZSTAFFCD,
			SERVUNIT,
			PRODTYP

       FROM VM1DTA.COVNPF;





CREATE OR REPLACE VIEW VM1DTA.COVNBOK(UNIQUE_NUMBER, COMPANY, CNOTNUM, BRANCH, BOOKNUM, STATUS, DTEISS, DTERCV, REGNO, CHASSI, ENGINE, CANRSN, DTEADV, CNTTYPE, KOB, SUMINS, CCDATE, CRDATE, REMARKS01, REMARKS02, REMARKS03, CHDRCOY, CHDRNUM, TRANNO, TRCDE, RSKTYPE, RSKNO, CLNTPFX, CLNTCOY, CLNTNUM, AGNTPFX, AGNTCOY, AGNTNUM, USERID, USRPRF, JOBNM, DATIME,SERVUNIT, PRODTYP) AS
SELECT UNIQUE_NUMBER,
            COMPANY,
            CNOTNUM,
            BRANCH,
            BOOKNUM,
            STATUS,
            DTEISS,
            DTERCV,
            REGNO,
            CHASSI,
            ENGINE,
            CANRSN,
            DTEADV,
            CNTTYPE,
            KOB,
            SUMINS,
            CCDATE,
            CRDATE,
            REMARKS01,
            REMARKS02,
            REMARKS03,
            CHDRCOY,
            CHDRNUM,
            TRANNO,
            TRCDE,
            RSKTYPE,
            RSKNO,
            CLNTPFX,
            CLNTCOY,
            CLNTNUM,
            AGNTPFX,
            AGNTCOY,
            AGNTNUM,
            USERID,
            USRPRF,
            JOBNM,
            DATIME,
			SERVUNIT,
			PRODTYP

       FROM VM1DTA.COVNPF;






CREATE OR REPLACE VIEW VM1DTA.COVNREG(UNIQUE_NUMBER, COMPANY, CNOTNUM, BRANCH, BOOKNUM, STATUS, DTEISS, DTERCV, REGNO, CHASSI, ENGINE, CANRSN, DTEADV, CNTTYPE, KOB, SUMINS, CCDATE, CRDATE, REMARKS01, REMARKS02, REMARKS03, CHDRCOY, CHDRNUM, TRANNO, TRCDE, RSKTYPE, RSKNO, CLNTPFX, CLNTCOY, CLNTNUM, AGNTPFX, AGNTCOY, AGNTNUM, USERID, USRPRF, JOBNM, DATIME, SERVUNIT, PRODTYP) AS
SELECT UNIQUE_NUMBER,
            COMPANY,
            CNOTNUM,
            BRANCH,
            BOOKNUM,
            STATUS,
            DTEISS,
            DTERCV,
            REGNO,
            CHASSI,
            ENGINE,
            CANRSN,
            DTEADV,
            CNTTYPE,
            KOB,
            SUMINS,
            CCDATE,
            CRDATE,
            REMARKS01,
            REMARKS02,
            REMARKS03,
            CHDRCOY,
            CHDRNUM,
            TRANNO,
            TRCDE,
            RSKTYPE,
            RSKNO,
            CLNTPFX,
            CLNTCOY,
            CLNTNUM,
            AGNTPFX,
            AGNTCOY,
            AGNTNUM,
            USERID,
            USRPRF,
            JOBNM,
            DATIME,
			SERVUNIT,
			PRODTYP

       FROM VM1DTA.COVNPF;






CREATE OR REPLACE VIEW VM1DTA.COVNRSK(UNIQUE_NUMBER, CHDRCOY, CHDRNUM, RSKNO, TRANNO, COMPANY, CNOTNUM, TRCDE, STATUS, KOB, DTEISS, DTERCV, JOBNM, USRPRF, DATIME, SERVUNIT, PRODTYP) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            RSKNO,
            TRANNO,
            COMPANY,
            CNOTNUM,
            TRCDE,
            STATUS,
            KOB,
            DTEISS,
            DTERCV,
            JOBNM,
            USRPRF,
            DATIME,
			SERVUNIT,
			PRODTYP

       FROM VM1DTA.COVNPF;





/*add column ECNV, CVNTTYPE,COVERNT into VM1DTA.GCHIPF */
ALTER TABLE VM1DTA.GCHIPF
ADD (ECNV char(1),CVNTYPE char(2),COVERNT char(8));



CREATE OR REPLACE VIEW VM1DTA.GCHICNN(
	UNIQUE_NUMBER,
	CHDRCOY,
	CHDRNUM,
	EFFDATE,
	CCDATE,
	CRDATE,
	PRVBILFLG,
	BILLFREQ,
	GADJFREQ,
	PAYRPFX,
	PAYRCOY,
	PAYRNUM,
	AGNTPFX,
	AGNTCOY,
	AGNTNUM,
	CNTBRANCH,
	STCA,
	STCB,
	STCC,
	STCD,
	STCE,
	BTDATENR,
	NRISDATE,
	TERMID,
	USER_T,
	TRDT,
	TRTM,
	TRANNO,
	CRATE,
	TERNMPRM,
	SURGSCHMV,
	AREACDEMV,
	MEDPRVDR,
	SPSMBR,
	CHILDMBR,
	SPSMED,
	CHILDMED,
	BANKCODE,
	BILLCHNL,
	MANDREF,
	RIMTHVCD,
	PRMRVWDT,
	APPLTYP,
	RIIND,
	USRPRF,
	JOBNM,
	DATIME,
	CFLIMIT,
	POLBREAK,
	CFTYPE,
	LMTDRL,
	NOFCLAIM,
	TPA,
	WKLADRT,
	WKLCMRT,
	NOFMBR,
	TIMEHH01,
	TIMEHH02,
	ECNV,
	CVNTYPE,
	COVERNT ) AS 
SELECT UNIQUE_NUMBER,
	CHDRCOY,
	CHDRNUM,
	EFFDATE,
	CCDATE,
	CRDATE,
	PRVBILFLG,
	BILLFREQ,
	GADJFREQ,
	PAYRPFX,
	PAYRCOY,
	PAYRNUM,
	AGNTPFX,
	AGNTCOY,
	AGNTNUM,
	CNTBRANCH,
	STCA,
	STCB,
	STCC,
	STCD,
	STCE,
	BTDATENR,
	NRISDATE,
	TERMID,
	USER_T,
	TRDT,
	TRTM,
	TRANNO,
	CRATE,
	TERNMPRM,
	SURGSCHMV,
	AREACDEMV,
	MEDPRVDR,
	SPSMBR,
	CHILDMBR,
	SPSMED,
	CHILDMED,
	BANKCODE,
	BILLCHNL,
	MANDREF,
	RIMTHVCD,
	PRMRVWDT,
	APPLTYP,
	RIIND,
	USRPRF,
	JOBNM,
	DATIME,
	CFLIMIT,
	POLBREAK,
	CFTYPE,
	LMTDRL,
	NOFCLAIM,
	TPA,
	WKLADRT,
	WKLCMRT,
	NOFMBR,
	TIMEHH01,
	TIMEHH02,
	ECNV,
	CVNTYPE,
	COVERNT
	FROM VM1DTA.GCHIPF;

CREATE OR REPLACE VIEW VM1DTA.GCHI (
	UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMEHH01
	,TIMEHH02
	,ECNV
	,CVNTYPE
	,COVERNT
	)
AS
SELECT UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMEHH01
	,TIMEHH02
	,ECNV
	,CVNTYPE
	,COVERNT

FROM VM1DTA.GCHIPF;

/*VIEW*/
/*CREATE OR REPLACE VIEW GCHITRN*/
CREATE OR REPLACE VIEW VM1DTA.GCHITRN (
	UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMEHH01
	,TIMEHH02
	,ECNV
	,CVNTYPE
	,COVERNT
	)
AS
SELECT UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMEHH01
	,TIMEHH02
	,ECNV
	,CVNTYPE
	,COVERNT
FROM VM1DTA.GCHIPF;



CREATE OR REPLACE VIEW VM1DTA.GCHD(
	UNIQUE_NUMBER
		,CHDRCOY
		,CHDRNUM
		,CHDRPFX
		,CNTTYPE
		,COWNPFX
		,COWNCOY
		,COWNNUM
		,STATCODE
		,PNDATE
		,SUBSFLG
		,OCCDATE
		,HRSKIND
		,CNTCURR
		,BILLCURR
		,TAKOVRFLG
		,GPRNLTYP
		,GPRMNTHS
		,RNLNOTTO
		,SRCEBUS
		,COYSRVAC
		,MRKSRVAC
		,DESPPFX
		,DESPCOY
		,DESPNUM
		,POLSCHPFLG
		,BTDATE
		,ADJDATE
		,PTDATE
		,PTDATEAB
		,TRANLUSED
		,LMBRNO
		,LHEADNO
		,EFFDCLDT
		,SERVUNIT
		,PNTRCDE
		,PROCID
		,TRANID
		,TRANNO
		,VALIDFLAG
		,SPECIND
		,TAXFLAG
		,AGEDEF
		,TERMAGE
		,PERSONCOV
		,ENROLLTYP
		,SPLITSUBS
		,AVLISU
		,MPLPFX
		,MPLCOY
		,MPLNUM
		,USRPRF
		,JOBNM
		,DATIME
		,IGRASP
		,IEXPLAIN
		,IDATE
		,MIDJOIN
		,CVISAIND
		,COVERNT
		,CNTISS
	) AS
SELECT UNIQUE_NUMBER
		,CHDRCOY
		,CHDRNUM
		,CHDRPFX
		,CNTTYPE
		,COWNPFX
		,COWNCOY
		,COWNNUM
		,STATCODE
		,PNDATE
		,SUBSFLG
		,OCCDATE
		,HRSKIND
		,CNTCURR
		,BILLCURR
		,TAKOVRFLG
		,GPRNLTYP
		,GPRMNTHS
		,RNLNOTTO
		,SRCEBUS
		,COYSRVAC
		,MRKSRVAC
		,DESPPFX
		,DESPCOY
		,DESPNUM
		,POLSCHPFLG
		,BTDATE
		,ADJDATE
		,PTDATE
		,PTDATEAB
		,TRANLUSED
		,LMBRNO
		,LHEADNO
		,EFFDCLDT
		,SERVUNIT
		,PNTRCDE
		,PROCID
		,TRANID
		,TRANNO
		,VALIDFLAG
		,SPECIND
		,TAXFLAG
		,AGEDEF
		,TERMAGE
		,PERSONCOV
		,ENROLLTYP
		,SPLITSUBS
		,AVLISU
		,MPLPFX
		,MPLCOY
		,MPLNUM
		,USRPRF
		,JOBNM
		,DATIME
		,IGRASP
		,IEXPLAIN
		,IDATE
		,MIDJOIN
		,CVISAIND
		,COVERNT
		,CNTISS
       FROM VM1DTA.CHDRPF
	    WHERE SERVUNIT = 'GP';


CREATE OR REPLACE VIEW VM1DTA.GPHDDTE
	(UNIQUE_NUMBER,
	CHDRCOY,
	CHDRNUM,
	PRODTYP,
	DTEATT,
	CONTYPE,
	AGRINDEM,
	DTEIDM,
	MEDEVD,
	DTETRM,
	REASONTRM,
	TERMID,
	USER_T,
	TRDT,
	TRTM,
	TRANNO,
	PRVCOND,
	PRVAMTS,
	PRVAMTM,
	PREEXIBEF,
	PREEXIAFT,
	GSTTYPE,
	PM06KEY,
	ECTBRULE,
	INVFMIX,
	BFTDSN,
	PAYMMODE,
	INVSLVL,
	ACBLRULE,
	PRMPYOPT,
	PRTEBFLG,
	TRUSTEE,
	GLBFEESC,
	ACTEXFLG,
	MBRFEE,
	FRFUND,
	EVNTFEE,
	TIMEFEE,
	FEEBTDTE,
	TRIPTYP,
	USRPRF,
	JOBNM,
	DATIME)
	AS
	SELECT 	UNIQUE_NUMBER,
	CHDRCOY,
	CHDRNUM,
	PRODTYP,
	DTEATT,
	CONTYPE,
	AGRINDEM,
	DTEIDM,
	MEDEVD,
	DTETRM,
	REASONTRM,
	TERMID,
	USER_T,
	TRDT,
	TRTM,
	TRANNO,
	PRVCOND,
	PRVAMTS,
	PRVAMTM,
	PREEXIBEF,
	PREEXIAFT,
	GSTTYPE,
	PM06KEY,
	ECTBRULE,
	INVFMIX,
	BFTDSN,
	PAYMMODE,
	INVSLVL,
	ACBLRULE,
	PRMPYOPT,
	PRTEBFLG,
	TRUSTEE,
	GLBFEESC,
	ACTEXFLG,
	MBRFEE,
	FRFUND,
	EVNTFEE,
	TIMEFEE,
	FEEBTDTE,
	TRIPTYP,
	USRPRF,
	JOBNM,
	DATIME 
	FROM VM1DTA.GPHDPF;

	/*add column OTHPFX, OTHCOY , OTHNUM into VM1DTA.Stmtpf */
ALTER TABLE VM1DTA.Stmtpf
ADD (OTHPFX char(2),OTHCOY char(1),OTHNUM char(14));



/*add column OTHPFX, OTHCOY , OTHNUM into VM1DTA.Ucshpf */
ALTER TABLE VM1DTA.Ucshpf
ADD (OTHPFX char(2),OTHCOY char(1),OTHNUM char(14));



/*VIEW*/
/*CREATE OR REPLACE VIEW GCAG*/
CREATE OR REPLACE VIEW VM1DTA.GCAG (
	UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMEHH01
	,TIMEHH02
	,ECNV
	,CVNTYPE
	,COVERNT
	)
AS
SELECT UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMEHH01
	,TIMEHH02
	,ECNV
	,CVNTYPE
	,COVERNT
FROM VM1DTA.GCHIPF;



/*VIEW*/
/*CREATE OR REPLACE VIEW GCHIASC*/
CREATE OR REPLACE VIEW VM1DTA.GCHIASC (
	UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMEHH01
	,TIMEHH02
	,ECNV
	,CVNTYPE
	,COVERNT
	)
AS
SELECT UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMEHH01
	,TIMEHH02
	,ECNV
	,CVNTYPE
	,COVERNT
FROM VM1DTA.GCHIPF;


/*VIEW*/
/*CREATE OR REPLACE VIEW GCHIPYR*/
CREATE OR REPLACE VIEW VM1DTA.GCHIPYR (
	UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMEHH01
	,TIMEHH02
	,ECNV
	,CVNTYPE
	,COVERNT
	)
AS
SELECT UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMEHH01
	,TIMEHH02
	,ECNV
	,CVNTYPE
	,COVERNT
FROM VM1DTA.GCHIPF;



/*VIEW*/
/*CREATE OR REPLACE VIEW GCHIRNL*/
CREATE OR REPLACE VIEW VM1DTA.GCHIRNL (
	UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMEHH01
	,TIMEHH02
	,ECNV
	,CVNTYPE
	,COVERNT
	)
AS
SELECT UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMEHH01
	,TIMEHH02
	,ECNV
	,CVNTYPE
	,COVERNT
FROM VM1DTA.GCHIPF;


/*VIEW*/
/*CREATE OR REPLACE VIEW GCHITPA*/
CREATE OR REPLACE VIEW VM1DTA.GCHITPA (
	UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMEHH01
	,TIMEHH02
	,ECNV
	,CVNTYPE
	,COVERNT
	)
AS
SELECT UNIQUE_NUMBER
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,BANKCODE
	,BILLCHNL
	,MANDREF
	,RIMTHVCD
	,PRMRVWDT
	,APPLTYP
	,RIIND
	,POLBREAK
	,CFTYPE
	,LMTDRL
	,CFLIMIT
	,NOFCLAIM
	,TPA
	,WKLADRT
	,WKLCMRT
	,NOFMBR
	,USRPRF
	,JOBNM
	,DATIME
	,TIMEHH01
	,TIMEHH02
	,ECNV
	,CVNTYPE
	,COVERNT
FROM VM1DTA.GCHIPF;



CREATE OR REPLACE VIEW VM1DTA.STDN(UNIQUE_NUMBER, TRNID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, JOBID, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, OTPFX, OTCOY, OTNUM, OTCCY, AMT01, AMT02, AMT03, AMT04, AMT05, AMT06, AMT07, AMT08, AMT09, AMT10, AMT11, AMT12, AMT13, AMT14, AMT15, LCE01, LCE02, LCE03, LCE04, LCE05, LCE06, LCE07, LCE08, LCE09, LCE10, LCE11, LCE12, LCE13, LCE14, LCE15, GPAMT, CMAMT, NTAMT, GRSLCE, CMLCE, NTLCE, MYFLG, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, MARYACTYR, MARYACTMN, RITYPE, TRANNO, JOBNM, USRPRF, DATIME,OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            JOBID,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            AMT01,
            AMT02,
            AMT03,
            AMT04,
            AMT05,
            AMT06,
            AMT07,
            AMT08,
            AMT09,
            AMT10,
            AMT11,
            AMT12,
            AMT13,
            AMT14,
            AMT15,
            LCE01,
            LCE02,
            LCE03,
            LCE04,
            LCE05,
            LCE06,
            LCE07,
            LCE08,
            LCE09,
            LCE10,
            LCE11,
            LCE12,
            LCE13,
            LCE14,
            LCE15,
            GPAMT,
            CMAMT,
            NTAMT,
            GRSLCE,
            CMLCE,
            NTLCE,
            MYFLG,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            MARYACTYR,
            MARYACTMN,
            RITYPE,
            TRANNO,
            JOBNM,
            USRPRF,
            DATIME,
			OTHPFX,
			OTHCOY,
			OTHNUM
       FROM VM1DTA.STMTPF
      WHERE MYFLG = ' ' AND OTPFX = 'GI';


CREATE OR REPLACE VIEW VM1DTA.STMA(UNIQUE_NUMBER, TRNID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, JOBID, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, OTPFX, OTCOY, OTNUM, OTCCY, AMT01, AMT02, AMT03, AMT04, AMT05, AMT06, AMT07, AMT08, AMT09, AMT10, AMT11, AMT12, AMT13, AMT14, AMT15, LCE01, LCE02, LCE03, LCE04, LCE05, LCE06, LCE07, LCE08, LCE09, LCE10, LCE11, LCE12, LCE13, LCE14, LCE15, GPAMT, CMAMT, NTAMT, GRSLCE, CMLCE, NTLCE, MYFLG, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, MARYACTYR, MARYACTMN, RITYPE, USRPRF, JOBNM, DATIME, TRANNO, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            JOBID,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            AMT01,
            AMT02,
            AMT03,
            AMT04,
            AMT05,
            AMT06,
            AMT07,
            AMT08,
            AMT09,
            AMT10,
            AMT11,
            AMT12,
            AMT13,
            AMT14,
            AMT15,
            LCE01,
            LCE02,
            LCE03,
            LCE04,
            LCE05,
            LCE06,
            LCE07,
            LCE08,
            LCE09,
            LCE10,
            LCE11,
            LCE12,
            LCE13,
            LCE14,
            LCE15,
            GPAMT,
            CMAMT,
            NTAMT,
            GRSLCE,
            CMLCE,
            NTLCE,
            MYFLG,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            MARYACTYR,
            MARYACTMN,
            RITYPE,
            USRPRF,
            JOBNM,
            DATIME,
            TRANNO,
			OTHPFX,
			OTHCOY,
			OTHNUM

       FROM VM1DTA.STMTPF;



CREATE OR REPLACE VIEW VM1DTA.STMT(UNIQUE_NUMBER, TRNID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, JOBID, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, OTPFX, OTCOY, OTNUM, OTCCY, AMT01, AMT02, AMT03, AMT04, AMT05, AMT06, AMT07, AMT08, AMT09, AMT10, AMT11, AMT12, AMT13, AMT14, AMT15, LCE01, LCE02, LCE03, LCE04, LCE05, LCE06, LCE07, LCE08, LCE09, LCE10, LCE11, LCE12, LCE13, LCE14, LCE15, GPAMT, CMAMT, NTAMT, GRSLCE, CMLCE, NTLCE, MYFLG, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, MARYACTYR, MARYACTMN, RITYPE, USRPRF, JOBNM, DATIME, TRANNO, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            JOBID,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            AMT01,
            AMT02,
            AMT03,
            AMT04,
            AMT05,
            AMT06,
            AMT07,
            AMT08,
            AMT09,
            AMT10,
            AMT11,
            AMT12,
            AMT13,
            AMT14,
            AMT15,
            LCE01,
            LCE02,
            LCE03,
            LCE04,
            LCE05,
            LCE06,
            LCE07,
            LCE08,
            LCE09,
            LCE10,
            LCE11,
            LCE12,
            LCE13,
            LCE14,
            LCE15,
            GPAMT,
            CMAMT,
            NTAMT,
            GRSLCE,
            CMLCE,
            NTLCE,
            MYFLG,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            MARYACTYR,
            MARYACTMN,
            RITYPE,
            USRPRF,
            JOBNM,
            DATIME,
            TRANNO,
			OTHPFX,
			OTHCOY,
			OTHNUM

       FROM VM1DTA.STMTPF;




CREATE OR REPLACE VIEW VM1DTA.STMTCHQ(UNIQUE_NUMBER, TRNID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, JOBID, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, OTPFX, OTCOY, OTNUM, OTCCY, AMT01, AMT02, AMT03, AMT04, AMT05, AMT06, AMT07, AMT08, AMT09, AMT10, AMT11, AMT12, AMT13, AMT14, AMT15, LCE01, LCE02, LCE03, LCE04, LCE05, LCE06, LCE07, LCE08, LCE09, LCE10, LCE11, LCE12, LCE13, LCE14, LCE15, GPAMT, CMAMT, NTAMT, GRSLCE, CMLCE, NTLCE, MYFLG, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, MARYACTYR, MARYACTMN, RITYPE, TRANNO, JOBNM, USRPRF, DATIME, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            JOBID,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            AMT01,
            AMT02,
            AMT03,
            AMT04,
            AMT05,
            AMT06,
            AMT07,
            AMT08,
            AMT09,
            AMT10,
            AMT11,
            AMT12,
            AMT13,
            AMT14,
            AMT15,
            LCE01,
            LCE02,
            LCE03,
            LCE04,
            LCE05,
            LCE06,
            LCE07,
            LCE08,
            LCE09,
            LCE10,
            LCE11,
            LCE12,
            LCE13,
            LCE14,
            LCE15,
            GPAMT,
            CMAMT,
            NTAMT,
            GRSLCE,
            CMLCE,
            NTLCE,
            MYFLG,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            MARYACTYR,
            MARYACTMN,
            RITYPE,
            TRANNO,
            JOBNM,
            USRPRF,
            DATIME,
			OTHPFX,
			OTHCOY,
			OTHNUM

       FROM VM1DTA.STMTPF
      WHERE CHPFX = 'GI' AND MYFLG = ' ' AND SACCT = 'GR';






CREATE OR REPLACE VIEW VM1DTA.STMTMDT(UNIQUE_NUMBER, TRNID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, JOBID, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, OTPFX, OTCOY, OTNUM, OTCCY, AMT01, AMT02, AMT03, AMT04, AMT05, AMT06, AMT07, AMT08, AMT09, AMT10, AMT11, AMT12, AMT13, AMT14, AMT15, LCE01, LCE02, LCE03, LCE04, LCE05, LCE06, LCE07, LCE08, LCE09, LCE10, LCE11, LCE12, LCE13, LCE14, LCE15, GPAMT, CMAMT, NTAMT, GRSLCE, CMLCE, NTLCE, MYFLG, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, MARYACTYR, MARYACTMN, RITYPE, TRANNO, JOBNM, USRPRF, DATIME, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            JOBID,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            AMT01,
            AMT02,
            AMT03,
            AMT04,
            AMT05,
            AMT06,
            AMT07,
            AMT08,
            AMT09,
            AMT10,
            AMT11,
            AMT12,
            AMT13,
            AMT14,
            AMT15,
            LCE01,
            LCE02,
            LCE03,
            LCE04,
            LCE05,
            LCE06,
            LCE07,
            LCE08,
            LCE09,
            LCE10,
            LCE11,
            LCE12,
            LCE13,
            LCE14,
            LCE15,
            GPAMT,
            CMAMT,
            NTAMT,
            GRSLCE,
            CMLCE,
            NTLCE,
            MYFLG,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            MARYACTYR,
            MARYACTMN,
            RITYPE,
            TRANNO,
            JOBNM,
            USRPRF,
            DATIME,
			OTHPFX,
			OTHCOY,
			OTHNUM

       FROM VM1DTA.STMTPF
      WHERE SACCT = 'FG' AND SAC01 = 'P' OR SACCT = 'DG' AND SAC01 = 'P';


CREATE OR REPLACE VIEW VM1DTA.STMTOTH(UNIQUE_NUMBER, TRNID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, JOBID, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, OTPFX, OTCOY, OTNUM, OTCCY, AMT01, AMT02, AMT03, AMT04, AMT05, AMT06, AMT07, AMT08, AMT09, AMT10, AMT11, AMT12, AMT13, AMT14, AMT15, LCE01, LCE02, LCE03, LCE04, LCE05, LCE06, LCE07, LCE08, LCE09, LCE10, LCE11, LCE12, LCE13, LCE14, LCE15, GPAMT, CMAMT, NTAMT, GRSLCE, CMLCE, NTLCE, MYFLG, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, MARYACTYR, MARYACTMN, RITYPE, TRANNO, JOBNM, USRPRF, DATIME, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            JOBID,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            AMT01,
            AMT02,
            AMT03,
            AMT04,
            AMT05,
            AMT06,
            AMT07,
            AMT08,
            AMT09,
            AMT10,
            AMT11,
            AMT12,
            AMT13,
            AMT14,
            AMT15,
            LCE01,
            LCE02,
            LCE03,
            LCE04,
            LCE05,
            LCE06,
            LCE07,
            LCE08,
            LCE09,
            LCE10,
            LCE11,
            LCE12,
            LCE13,
            LCE14,
            LCE15,
            GPAMT,
            CMAMT,
            NTAMT,
            GRSLCE,
            CMLCE,
            NTLCE,
            MYFLG,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            MARYACTYR,
            MARYACTMN,
            RITYPE,
            TRANNO,
            JOBNM,
            USRPRF,
            DATIME,
			OTHPFX,
			OTHCOY,
			OTHNUM
       FROM VM1DTA.STMTPF
      WHERE MYFLG = ' ' AND OTPFX = 'CV';


CREATE OR REPLACE VIEW VM1DTA.STMTPOL(UNIQUE_NUMBER, TRNID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, JOBID, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, OTPFX, OTCOY, OTNUM, OTCCY, AMT01, AMT02, AMT03, AMT04, AMT05, AMT06, AMT07, AMT08, AMT09, AMT10, AMT11, AMT12, AMT13, AMT14, AMT15, LCE01, LCE02, LCE03, LCE04, LCE05, LCE06, LCE07, LCE08, LCE09, LCE10, LCE11, LCE12, LCE13, LCE14, LCE15, GPAMT, CMAMT, NTAMT, GRSLCE, CMLCE, NTLCE, MYFLG, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, MARYACTYR, MARYACTMN, RITYPE, TRANNO, JOBNM, USRPRF, DATIME, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            JOBID,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            AMT01,
            AMT02,
            AMT03,
            AMT04,
            AMT05,
            AMT06,
            AMT07,
            AMT08,
            AMT09,
            AMT10,
            AMT11,
            AMT12,
            AMT13,
            AMT14,
            AMT15,
            LCE01,
            LCE02,
            LCE03,
            LCE04,
            LCE05,
            LCE06,
            LCE07,
            LCE08,
            LCE09,
            LCE10,
            LCE11,
            LCE12,
            LCE13,
            LCE14,
            LCE15,
            GPAMT,
            CMAMT,
            NTAMT,
            GRSLCE,
            CMLCE,
            NTLCE,
            MYFLG,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            MARYACTYR,
            MARYACTMN,
            RITYPE,
            TRANNO,
            JOBNM,
            USRPRF,
            DATIME,
			OTHPFX,
			OTHCOY,
			OTHNUM

       FROM vm1dta.STMTPF;



CREATE OR REPLACE VIEW VM1DTA.STMTPRM(UNIQUE_NUMBER, CLPFX, CLCOY, CLNUM, PLPFX, PLCOY, PLNUM, CHPFX, MYFLG, TRNID, VFLAG, ACPFX, ACCOY, ACNUM, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, JOBID, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, OTPFX, OTCOY, OTNUM, OTCCY, AMT01, AMT02, AMT03, AMT04, AMT05, AMT06, AMT07, AMT08, AMT09, AMT10, AMT11, AMT12, AMT13, AMT14, AMT15, LCE01, LCE02, LCE03, LCE04, LCE05, LCE06, LCE07, LCE08, LCE09, LCE10, LCE11, LCE12, LCE13, LCE14, LCE15, GPAMT, CMAMT, NTAMT, GRSLCE, CMLCE, NTLCE, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, MARYACTYR, MARYACTMN, RITYPE, TRANNO, USRPRF, DATIME, JOBNM, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            CLPFX,
            CLCOY,
            CLNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CHPFX,
            MYFLG,
            TRNID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            JOBID,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            AMT01,
            AMT02,
            AMT03,
            AMT04,
            AMT05,
            AMT06,
            AMT07,
            AMT08,
            AMT09,
            AMT10,
            AMT11,
            AMT12,
            AMT13,
            AMT14,
            AMT15,
            LCE01,
            LCE02,
            LCE03,
            LCE04,
            LCE05,
            LCE06,
            LCE07,
            LCE08,
            LCE09,
            LCE10,
            LCE11,
            LCE12,
            LCE13,
            LCE14,
            LCE15,
            GPAMT,
            CMAMT,
            NTAMT,
            GRSLCE,
            CMLCE,
            NTLCE,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            MARYACTYR,
            MARYACTMN,
            RITYPE,
            TRANNO,
            USRPRF,
            DATIME,
            JOBNM,
			OTHPFX,
			OTHCOY,
			OTHNUM

       FROM vm1dta.STMTPF;




CREATE OR REPLACE VIEW VM1DTA.STMTRIN(UNIQUE_NUMBER, TRNID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, JOBID, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, OTPFX, OTCOY, OTNUM, OTCCY, AMT01, AMT02, AMT03, AMT04, AMT05, AMT06, AMT07, AMT08, AMT09, AMT10, AMT11, AMT12, AMT13, AMT14, AMT15, LCE01, LCE02, LCE03, LCE04, LCE05, LCE06, LCE07, LCE08, LCE09, LCE10, LCE11, LCE12, LCE13, LCE14, LCE15, GPAMT, CMAMT, NTAMT, GRSLCE, CMLCE, NTLCE, MYFLG, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, MARYACTYR, MARYACTMN, RITYPE, TRANNO, JOBNM, USRPRF, DATIME, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            JOBID,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            AMT01,
            AMT02,
            AMT03,
            AMT04,
            AMT05,
            AMT06,
            AMT07,
            AMT08,
            AMT09,
            AMT10,
            AMT11,
            AMT12,
            AMT13,
            AMT14,
            AMT15,
            LCE01,
            LCE02,
            LCE03,
            LCE04,
            LCE05,
            LCE06,
            LCE07,
            LCE08,
            LCE09,
            LCE10,
            LCE11,
            LCE12,
            LCE13,
            LCE14,
            LCE15,
            GPAMT,
            CMAMT,
            NTAMT,
            GRSLCE,
            CMLCE,
            NTLCE,
            MYFLG,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            MARYACTYR,
            MARYACTMN,
            RITYPE,
            TRANNO,
            JOBNM,
            USRPRF,
            DATIME,
			OTHPFX,
			OTHCOY,
			OTHNUM
       FROM vm1dta.STMTPF
      WHERE SACCT = 'RP' AND SAC01 = 'P' AND MYFLG = ' '
            OR SACCT = 'CO' AND SAC01 = 'P' AND MYFLG = ' ';





CREATE OR REPLACE VIEW VM1DTA.STPL(UNIQUE_NUMBER, TRNID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, JOBID, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, OTPFX, OTCOY, OTNUM, OTCCY, AMT01, AMT02, AMT03, AMT04, AMT05, AMT06, AMT07, AMT08, AMT09, AMT10, AMT11, AMT12, AMT13, AMT14, AMT15, LCE01, LCE02, LCE03, LCE04, LCE05, LCE06, LCE07, LCE08, LCE09, LCE10, LCE11, LCE12, LCE13, LCE14, LCE15, GPAMT, CMAMT, NTAMT, GRSLCE, CMLCE, NTLCE, MYFLG, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, USRPRF, JOBNM, DATIME, MARYACTYR, MARYACTMN, RITYPE, TRANNO, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            JOBID,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            AMT01,
            AMT02,
            AMT03,
            AMT04,
            AMT05,
            AMT06,
            AMT07,
            AMT08,
            AMT09,
            AMT10,
            AMT11,
            AMT12,
            AMT13,
            AMT14,
            AMT15,
            LCE01,
            LCE02,
            LCE03,
            LCE04,
            LCE05,
            LCE06,
            LCE07,
            LCE08,
            LCE09,
            LCE10,
            LCE11,
            LCE12,
            LCE13,
            LCE14,
            LCE15,
            GPAMT,
            CMAMT,
            NTAMT,
            GRSLCE,
            CMLCE,
            NTLCE,
            MYFLG,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            USRPRF,
            JOBNM,
            DATIME,
            MARYACTYR,
            MARYACTMN,
            RITYPE,
            TRANNO,
			OTHPFX,
			OTHCOY,
			OTHNUM
       FROM VM1DTA.STMTPF;


CREATE OR REPLACE VIEW VM1DTA.STSQ(UNIQUE_NUMBER, TRNID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, JOBID, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, OTPFX, OTCOY, OTNUM, OTCCY, AMT01, AMT02, AMT03, AMT04, AMT05, AMT06, AMT07, AMT08, AMT09, AMT10, AMT11, AMT12, AMT13, AMT14, AMT15, LCE01, LCE02, LCE03, LCE04, LCE05, LCE06, LCE07, LCE08, LCE09, LCE10, LCE11, LCE12, LCE13, LCE14, LCE15, GPAMT, CMAMT, NTAMT, GRSLCE, CMLCE, NTLCE, MYFLG, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, MARYACTYR, MARYACTMN, USRPRF, JOBNM, DATIME, RITYPE, TRANNO, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            JOBID,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            AMT01,
            AMT02,
            AMT03,
            AMT04,
            AMT05,
            AMT06,
            AMT07,
            AMT08,
            AMT09,
            AMT10,
            AMT11,
            AMT12,
            AMT13,
            AMT14,
            AMT15,
            LCE01,
            LCE02,
            LCE03,
            LCE04,
            LCE05,
            LCE06,
            LCE07,
            LCE08,
            LCE09,
            LCE10,
            LCE11,
            LCE12,
            LCE13,
            LCE14,
            LCE15,
            GPAMT,
            CMAMT,
            NTAMT,
            GRSLCE,
            CMLCE,
            NTLCE,
            MYFLG,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            MARYACTYR,
            MARYACTMN,
            USRPRF,
            JOBNM,
            DATIME,
            RITYPE,
            TRANNO,
			OTHPFX,
			OTHCOY,
			OTHNUM

       FROM VM1DTA.STMTPF;




CREATE OR REPLACE VIEW VM1DTA.UCDN(UNIQUE_NUMBER, TRNID, TRMID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, JOBID, OTPFX, OTCOY, OTNUM, OTCCY, ORCSH, OCLCE, NTAMT, NTLCE, CSHMT, CSLCE, CSHDT, RTOLN, RTLCE, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, MARYACTYR, MARYACTMN, MYFLG, JOBNM, USRPRF, DATIME, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            TRMID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            JOBID,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            ORCSH,
            OCLCE,
            NTAMT,
            NTLCE,
            CSHMT,
            CSLCE,
            CSHDT,
            RTOLN,
            RTLCE,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            MARYACTYR,
            MARYACTMN,
            MYFLG,
            JOBNM,
            USRPRF,
            DATIME,
			OTHPFX,
			OTHCOY,
			OTHNUM
       FROM VM1DTA.UCSHPF
      WHERE OTPFX = 'GI';



CREATE OR REPLACE VIEW VM1DTA.UCPL(UNIQUE_NUMBER, TRNID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, JOBID, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, OTPFX, OTCOY, OTNUM, OTCCY, ORCSH, OCLCE, NTAMT, NTLCE, CSHMT, CSLCE, CSHDT, RTOLN, RTLCE, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, USRPRF, JOBNM, DATIME, TRMID, MARYACTYR, MARYACTMN, MYFLG, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            JOBID,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            ORCSH,
            OCLCE,
            NTAMT,
            NTLCE,
            CSHMT,
            CSLCE,
            CSHDT,
            RTOLN,
            RTLCE,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            USRPRF,
            JOBNM,
            DATIME,
            TRMID,
            MARYACTYR,
            MARYACTMN,
            MYFLG,
			OTHPFX,
			OTHCOY,
			OTHNUM
   
       FROM VM1DTA.UCSHPF;




CREATE OR REPLACE VIEW VM1DTA.UCSH(UNIQUE_NUMBER, TRNID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, JOBID, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, OTPFX, OTCOY, OTNUM, OTCCY, ORCSH, OCLCE, NTAMT, NTLCE, CSHMT, CSLCE, CSHDT, RTOLN, RTLCE, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, MARYACTYR, MARYACTMN, USRPRF, JOBNM, DATIME, TRMID, MYFLG, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            JOBID,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            ORCSH,
            OCLCE,
            NTAMT,
            NTLCE,
            CSHMT,
            CSLCE,
            CSHDT,
            RTOLN,
            RTLCE,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            MARYACTYR,
            MARYACTMN,
            USRPRF,
            JOBNM,
            DATIME,
            TRMID,
            MYFLG,
			OTHPFX,
			OTHCOY,
			OTHNUM

   
       FROM VM1DTA.UCSHPF;


CREATE OR REPLACE VIEW VM1DTA.UCSHCHQ(UNIQUE_NUMBER, TRNID, TRMID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, JOBID, OTPFX, OTCOY, OTNUM, OTCCY, ORCSH, OCLCE, NTAMT, NTLCE, CSHMT, CSLCE, CSHDT, RTOLN, RTLCE, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, MARYACTYR, MARYACTMN, MYFLG, JOBNM, USRPRF, DATIME, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            TRMID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            JOBID,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            ORCSH,
            OCLCE,
            NTAMT,
            NTLCE,
            CSHMT,
            CSLCE,
            CSHDT,
            RTOLN,
            RTLCE,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            MARYACTYR,
            MARYACTMN,
            MYFLG,
            JOBNM,
            USRPRF,
            DATIME,
			OTHPFX,
			OTHCOY,
			OTHNUM
   
       FROM VM1DTA.UCSHPF;


CREATE OR REPLACE VIEW VM1DTA.UCSHOTH(UNIQUE_NUMBER, TRNID, TRMID, VFLAG, ACPFX, ACCOY, ACNUM, PLPFX, PLCOY, PLNUM, CLPFX, CLCOY, CLNUM, CHPFX, CHCOY, CHNUM, UNIQK, RECNC, DCODE, SDCRA, SDCRB, SDCRC, SDCRD, KEYSP, CNTYP, TRNDT, EFFDT, ENDTP, CREQT, ORDTF, TRCDE, SACCT, SAC01, SAC02, SAC03, SAC04, SAC05, SAC06, SAC07, SAC08, SAC09, SAC10, SAC11, SAC12, SAC13, SAC14, SAC15, JNLFG, BTPFX, BTCOY, BTBRN, BTACY, BTACM, BTTCD, BTBCH, DUEDT, AGECD, RSKCL, RSKNR, BRKRF, JOBID, OTPFX, OTCOY, OTNUM, OTCCY, ORCSH, OCLCE, NTAMT, NTLCE, CSHMT, CSLCE, CSHDT, RTOLN, RTLCE, MYUQK, ORCCY, LCCCY, CRATE, STCA, STCB, STCC, STCD, STCE, MARYACTYR, MARYACTMN, MYFLG, JOBNM, USRPRF, DATIME, OTHPFX, OTHCOY, OTHNUM) AS
SELECT UNIQUE_NUMBER,
            TRNID,
            TRMID,
            VFLAG,
            ACPFX,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            CHCOY,
            CHNUM,
            UNIQK,
            RECNC,
            DCODE,
            SDCRA,
            SDCRB,
            SDCRC,
            SDCRD,
            KEYSP,
            CNTYP,
            TRNDT,
            EFFDT,
            ENDTP,
            CREQT,
            ORDTF,
            TRCDE,
            SACCT,
            SAC01,
            SAC02,
            SAC03,
            SAC04,
            SAC05,
            SAC06,
            SAC07,
            SAC08,
            SAC09,
            SAC10,
            SAC11,
            SAC12,
            SAC13,
            SAC14,
            SAC15,
            JNLFG,
            BTPFX,
            BTCOY,
            BTBRN,
            BTACY,
            BTACM,
            BTTCD,
            BTBCH,
            DUEDT,
            AGECD,
            RSKCL,
            RSKNR,
            BRKRF,
            JOBID,
            OTPFX,
            OTCOY,
            OTNUM,
            OTCCY,
            ORCSH,
            OCLCE,
            NTAMT,
            NTLCE,
            CSHMT,
            CSLCE,
            CSHDT,
            RTOLN,
            RTLCE,
            MYUQK,
            ORCCY,
            LCCCY,
            CRATE,
            STCA,
            STCB,
            STCC,
            STCD,
            STCE,
            MARYACTYR,
            MARYACTMN,
            MYFLG,
            JOBNM,
            USRPRF,
            DATIME,
			OTHPFX,
			OTHCOY,
			OTHNUM

       FROM VM1DTA.UCSHPF
      WHERE OTPFX = 'CV';




CREATE OR REPLACE VIEW VM1DTA.GCHIOWN (
	UNIQUE_NUMBER
	,COWNPFX
	,COWNCOY
	,COWNNUM
	,MANDREF
	,CHDRCOY
	,CHDRNUM
	,EFFDATE
	,CCDATE
	,CRDATE
	,PRVBILFLG
	,BILLFREQ
	,GADJFREQ
	,PAYRPFX
	,PAYRCOY
	,PAYRNUM
	,AGNTPFX
	,AGNTCOY
	,AGNTNUM
	,CNTBRANCH
	,STCA
	,STCB
	,STCC
	,STCD
	,STCE
	,BTDATENR
	,NRISDATE
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,CRATE
	,TERNMPRM
	,SURGSCHMV
	,AREACDEMV
	,MEDPRVDR
	,SPSMBR
	,CHILDMBR
	,SPSMED
	,CHILDMED
	,BANKCODE
	,BILLCHNL
	,RIMTHVCD
	,PRMRVWDT
	,JOBNM
	,USRPRF
	,DATIME
	,ZMANDREF
	,REQNTYPE
	,PAYCLT
	,ECNV
	,CVNTYPE
	,COVERNT
	)
AS
SELECT CHDRPF.UNIQUE_NUMBER
	,CHDRPF.COWNPFX
	,CHDRPF.COWNCOY
	,CHDRPF.COWNNUM
	,GCHIPF.MANDREF
	,GCHIPF.CHDRCOY
	,CHDRPF.CHDRNUM
	,GCHIPF.EFFDATE
	,GCHIPF.CCDATE
	,GCHIPF.CRDATE
	,GCHIPF.PRVBILFLG
	,GCHIPF.BILLFREQ
	,GCHIPF.GADJFREQ
	,GCHIPF.PAYRPFX
	,GCHIPF.PAYRCOY
	,GCHIPF.PAYRNUM
	,GCHIPF.AGNTPFX
	,GCHIPF.AGNTCOY
	,GCHIPF.AGNTNUM
	,GCHIPF.CNTBRANCH
	,GCHIPF.STCA
	,GCHIPF.STCB
	,GCHIPF.STCC
	,GCHIPF.STCD
	,GCHIPF.STCE
	,GCHIPF.BTDATENR
	,GCHIPF.NRISDATE
	,GCHIPF.TERMID
	,GCHIPF.USER_T
	,GCHIPF.TRDT
	,GCHIPF.TRTM
	,GCHIPF.TRANNO
	,GCHIPF.CRATE
	,GCHIPF.TERNMPRM
	,GCHIPF.SURGSCHMV
	,GCHIPF.AREACDEMV
	,GCHIPF.MEDPRVDR
	,GCHIPF.SPSMBR
	,GCHIPF.CHILDMBR
	,GCHIPF.SPSMED
	,GCHIPF.CHILDMED
	,GCHIPF.BANKCODE
	,GCHIPF.BILLCHNL
	,GCHIPF.RIMTHVCD
	,GCHIPF.PRMRVWDT
	,GCHIPF.ECNV
	,GCHIPF.CVNTYPE
	,GCHIPF.COVERNT
	,CHDRPF.JOBNM
	,CHDRPF.USRPRF
	,CHDRPF.DATIME
	,CHDRPF.ZMANDREF
	,CHDRPF.REQNTYPE
	,CHDRPF.PAYCLT
FROM VM1DTA.CHDRPF
	,GCHIPF
WHERE (CHDRPF.CHDRCOY = GCHIPF.CHDRCOY)
	AND (CHDRPF.CHDRNUM = GCHIPF.CHDRNUM);

INSERT INTO VM1DTA.ERORPF
           (ERORPFX
           ,ERORCOY
           ,ERORLANG
           ,ERORPROG
           ,EROREROR
           ,ERORDESC
           ,TRDT
           ,TRTM
           ,USERID
           ,TERMINALID
           ,USRPRF
           ,JOBNM
           ,DATIME
           ,ERORFILE)
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFSK'
           ,'Invalid Service Unit'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');



  CREATE OR REPLACE FORCE VIEW "VM1DTA"."STOS" ("UNIQUE_NUMBER", "ACCOY", "ACPFX", "ACNUM", "ORCCY", "MYFLG", "TRNDT", "BTACY", "BTACM", "PLPFX", "PLCOY", "PLNUM", "CNTYP", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
            ACCOY,
            ACPFX,
            ACNUM,
            ORCCY,
            MYFLG,
            TRNDT,
            BTACY,
            BTACM,
            PLPFX,
            PLCOY,
            PLNUM,
            CNTYP,
            RECORDFORMAT,
            SUBSTR (NONKEYS || RPAD (' ', 1169, ' '), 1, 1169) AS "NONKEYS"
       FROM (SELECT * FROM STOS_STOSREC
             UNION ALL
             SELECT * FROM STOS_UCSHREC) A
   ORDER BY ACCOY,
            ACPFX,
            ACNUM,
            ORCCY,
            MYFLG DESC,
            TRNDT,
            BTACY DESC,
            BTACM DESC,
            PLPFX,
            PLCOY,
            PLNUM,
            CNTYP,
            UNIQUE_NUMBER DESC ;
 
		   
		   
		   
  CREATE OR REPLACE FORCE VIEW "VM1DTA"."STOS_STOSREC" ("UNIQUE_NUMBER", "ACCOY", "ACPFX", "ACNUM", "ORCCY", "MYFLG", "TRNDT", "BTACY", "BTACM", "PLPFX", "PLCOY", "PLNUM", "CNTYP", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
          ACCOY,
          ACPFX,
          ACNUM,
          ORCCY,
          MYFLG,
          TRNDT,
          BTACY,
          BTACM,
          PLPFX,
          PLCOY,
          PLNUM,
          CNTYP,
          'STOSREC' AS RECORDFORMAT,
          SUBSTR (NVL (TO_CHAR (TRNID), ' ') || LPAD (' ', 19, ' '), 1, 19)
          || SUBSTR (NVL (VFLAG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ACCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (PLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (PLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (PLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CHNUM, ' ') || RPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (UNIQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (RECNC, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (DCODE, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SDCRA, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SDCRB, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (SDCRC, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (SDCRD, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (CNTYP, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (TRNDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (EFFDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TRCDE, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SACCT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC01, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC02, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC03, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC04, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC05, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC06, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC07, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC08, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC09, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC10, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC11, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC12, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC13, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC14, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC15, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (BTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (BTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (BTBRN, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (TO_CHAR (BTACY), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (TO_CHAR (BTACM), ' ') || LPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (BTTCD, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (BTBCH, ' ') || RPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (DUEDT, ' ') || RPAD (' ', 6, ' '), 1, 6)
          || SUBSTR (NVL (AGECD, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (RSKCL, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (RSKNR), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (BRKRF, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (TO_CHAR (AMT01), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT02), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT03), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT04), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT05), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT06), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT07), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT08), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT09), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT10), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT11), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT12), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT13), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT14), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT15), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE01), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE02), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE03), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE04), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE05), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE06), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE07), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE08), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE09), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE10), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE11), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE12), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE13), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE14), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE15), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (GPAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CMAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (GRSLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CMLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (MYFLG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (TO_CHAR (MYUQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (ORCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (LCCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (CRATE), ' ') || LPAD (' ', 20, ' '),
                     1,
                     20)
          || SUBSTR (NVL (STCA, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCB, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCC, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCD, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCE, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (MARYACTYR), ' ') || LPAD (' ', 5, ' '),
                     1,
                     5)
          || SUBSTR (NVL (TO_CHAR (MARYACTMN), ' ') || LPAD (' ', 3, ' '),
                     1,
                     3)
          || SUBSTR (NVL (RITYPE, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ORDTF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (JOBID, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (USRPRF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (JOBNM, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || NVL (TO_CHAR (DATIME, 'YYYY-MM-DD-HH24.MI.SS.ff6'),
                  '                          ')
          || SUBSTR (NVL (KEYSP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ENDTP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CREQT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (JNLFG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (OTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
          || SUBSTR (NVL (OTCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (TRANNO), ' ') || LPAD (' ', 6, ' '), 1, 6)
		  || SUBSTR (NVL (OTHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2) 
		  || SUBSTR (NVL (OTHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTHNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
          
             AS "NONKEYS"
     FROM STMTPF;
	 
	 
  CREATE OR REPLACE FORCE VIEW "VM1DTA"."STOS_UCSHREC" ("UNIQUE_NUMBER", "ACCOY", "ACPFX", "ACNUM", "ORCCY", "MYFLG", "TRNDT", "BTACY", "BTACM", "PLPFX", "PLCOY", "PLNUM", "CNTYP", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
          ACCOY,
          ACPFX,
          ACNUM,
          ORCCY,
          MYFLG,
          TRNDT,
          BTACY,
          BTACM,
          PLPFX,
          PLCOY,
          PLNUM,
          CNTYP,
          'UCSHREC' AS RECORDFORMAT,
          SUBSTR (NVL (TO_CHAR (TRNID), ' ') || LPAD (' ', 19, ' '), 1, 19)
          || SUBSTR (NVL (VFLAG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ACCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (PLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (PLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (PLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CHNUM, ' ') || RPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (UNIQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (RECNC, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (DCODE, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SDCRA, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SDCRB, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (SDCRC, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (SDCRD, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (CNTYP, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (TRNDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (EFFDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TRCDE, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SACCT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC01, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC02, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC03, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC04, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC05, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC06, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC07, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC08, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC09, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC10, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC11, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC12, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC13, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC14, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC15, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (BTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (BTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (BTBRN, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (TO_CHAR (BTACY), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (TO_CHAR (BTACM), ' ') || LPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (BTTCD, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (BTBCH, ' ') || RPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (DUEDT, ' ') || RPAD (' ', 6, ' '), 1, 6)
          || SUBSTR (NVL (AGECD, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (RSKCL, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (RSKNR), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (BRKRF, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (TO_CHAR (ORCSH), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (OCLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CSHMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CSLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CSHDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (MYUQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (ORCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (LCCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (CRATE), ' ') || LPAD (' ', 20, ' '),
                     1,
                     20)
          || SUBSTR (NVL (STCA, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCB, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCC, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCD, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCE, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (MARYACTYR), ' ') || LPAD (' ', 5, ' '),
                     1,
                     5)
          || SUBSTR (NVL (TO_CHAR (MARYACTMN), ' ') || LPAD (' ', 3, ' '),
                     1,
                     3)
          || SUBSTR (NVL (JOBID, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (MYFLG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (USRPRF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (JOBNM, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || NVL (TO_CHAR (DATIME, 'YYYY-MM-DD-HH24.MI.SS.ff6'),
                  '                          ')
          || SUBSTR (NVL (TRMID, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (KEYSP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ENDTP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CREQT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ORDTF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (JNLFG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (OTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
          || SUBSTR (NVL (OTCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (RTOLN), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (RTLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
		  || SUBSTR (NVL (OTHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2) 
		  || SUBSTR (NVL (OTHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTHNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
             AS "NONKEYS"
     FROM UCSHPF;
	 
	 
	 

  CREATE OR REPLACE FORCE VIEW "VM1DTA"."STOSCLT" ("UNIQUE_NUMBER", "CLPFX", "CLCOY", "CLNUM", "ORCCY", "ACCOY", "ACPFX", "ACNUM", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
            CLPFX,
            CLCOY,
            CLNUM,
            ORCCY,
            ACCOY,
            ACPFX,
            ACNUM,
            RECORDFORMAT,
            SUBSTR (NONKEYS || RPAD (' ', 1169, ' '), 1, 1169) AS "NONKEYS"
       FROM (SELECT * FROM STOSCLT_STOSCLTREC
             UNION ALL
             SELECT * FROM STOSCLT_UCSHCLTREC) A
   ORDER BY CLPFX,
            CLCOY,
            CLNUM,
            ORCCY,
            ACCOY,
            ACPFX,
            ACNUM ;
 
	 
	 
	 
  CREATE OR REPLACE FORCE VIEW "VM1DTA"."STOSCLT_STOSCLTREC" ("UNIQUE_NUMBER", "CLPFX", "CLCOY", "CLNUM", "ORCCY", "ACCOY", "ACPFX", "ACNUM", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
          CLPFX,
          CLCOY,
          CLNUM,
          ORCCY,
          ACCOY,
          ACPFX,
          ACNUM,
          'STOSCLTREC' AS RECORDFORMAT,
          SUBSTR (NVL (TO_CHAR (TRNID), ' ') || LPAD (' ', 19, ' '), 1, 19)
          || SUBSTR (NVL (VFLAG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ACCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (PLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (PLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (PLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CHNUM, ' ') || RPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (UNIQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (RECNC, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (DCODE, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SDCRA, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SDCRB, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (SDCRC, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (SDCRD, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (KEYSP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CNTYP, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (TRNDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (EFFDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (ENDTP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CREQT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ORDTF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (TRCDE, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SACCT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC01, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC02, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC03, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC04, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC05, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC06, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC07, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC08, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC09, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC10, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC11, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC12, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC13, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC14, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC15, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (JNLFG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (JOBID, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (BTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (BTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (BTBRN, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (TO_CHAR (BTACY), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (TO_CHAR (BTACM), ' ') || LPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (BTTCD, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (BTBCH, ' ') || RPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (DUEDT, ' ') || RPAD (' ', 6, ' '), 1, 6)
          || SUBSTR (NVL (AGECD, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (RSKCL, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (RSKNR), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (BRKRF, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (OTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (OTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
          || SUBSTR (NVL (OTCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (AMT01), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT02), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT03), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT04), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT05), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT06), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT07), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT08), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT09), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT10), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT11), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT12), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT13), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT14), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT15), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE01), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE02), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE03), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE04), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE05), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE06), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE07), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE08), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE09), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE10), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE11), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE12), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE13), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE14), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE15), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (GPAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CMAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (GRSLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CMLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (MYFLG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (TO_CHAR (MYUQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (ORCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (LCCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (CRATE), ' ') || LPAD (' ', 20, ' '),
                     1,
                     20)
          || SUBSTR (NVL (STCA, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCB, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCC, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCD, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCE, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (MARYACTYR), ' ') || LPAD (' ', 5, ' '),
                     1,
                     5)
          || SUBSTR (NVL (TO_CHAR (MARYACTMN), ' ') || LPAD (' ', 3, ' '),
                     1,
                     3)
          || SUBSTR (NVL (RITYPE, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (TO_CHAR (TRANNO), ' ') || LPAD (' ', 6, ' '), 1, 6)
          || SUBSTR (NVL (JOBNM, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (USRPRF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || NVL (TO_CHAR (DATIME, 'YYYY-MM-DD-HH24.MI.SS.ff6'),
                  '                          ')
		  || SUBSTR (NVL (OTHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2) 
		  || SUBSTR (NVL (OTHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTHNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
		  
             AS "NONKEYS"
     
	 FROM STMTPF;
 

 
 
  CREATE OR REPLACE FORCE VIEW "VM1DTA"."STOSCLT_UCSHCLTREC" ("UNIQUE_NUMBER", "CLPFX", "CLCOY", "CLNUM", "ORCCY", "ACCOY", "ACPFX", "ACNUM", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
          CLPFX,
          CLCOY,
          CLNUM,
          ORCCY,
          ACCOY,
          ACPFX,
          ACNUM,
          'UCSHCLTREC' AS RECORDFORMAT,
          SUBSTR (NVL (TO_CHAR (TRNID), ' ') || LPAD (' ', 19, ' '), 1, 19)
          || SUBSTR (NVL (TRMID, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (VFLAG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ACCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (PLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (PLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (PLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CHNUM, ' ') || RPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (UNIQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (RECNC, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (DCODE, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SDCRA, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SDCRB, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (SDCRC, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (SDCRD, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (KEYSP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CNTYP, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (TRNDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (EFFDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (ENDTP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CREQT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ORDTF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (TRCDE, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SACCT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC01, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC02, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC03, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC04, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC05, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC06, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC07, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC08, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC09, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC10, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC11, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC12, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC13, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC14, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC15, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (JNLFG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (BTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (BTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (BTBRN, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (TO_CHAR (BTACY), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (TO_CHAR (BTACM), ' ') || LPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (BTTCD, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (BTBCH, ' ') || RPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (DUEDT, ' ') || RPAD (' ', 6, ' '), 1, 6)
          || SUBSTR (NVL (AGECD, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (RSKCL, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (RSKNR), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (BRKRF, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (JOBID, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (OTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (OTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
          || SUBSTR (NVL (OTCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (ORCSH), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (OCLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CSHMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CSLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CSHDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (RTOLN), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (RTLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (MYUQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (ORCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (LCCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (CRATE), ' ') || LPAD (' ', 20, ' '),
                     1,
                     20)
          || SUBSTR (NVL (STCA, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCB, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCC, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCD, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCE, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (MARYACTYR), ' ') || LPAD (' ', 5, ' '),
                     1,
                     5)
          || SUBSTR (NVL (TO_CHAR (MARYACTMN), ' ') || LPAD (' ', 3, ' '),
                     1,
                     3)
          || SUBSTR (NVL (MYFLG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (JOBNM, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (USRPRF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || NVL (TO_CHAR (DATIME, 'YYYY-MM-DD-HH24.MI.SS.ff6'),
                  '                          ')
		  || SUBSTR (NVL (OTHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2) 
		  || SUBSTR (NVL (OTHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTHNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
				  
             AS "NONKEYS"
     FROM UCSHPF;
	 


  CREATE OR REPLACE FORCE VIEW "VM1DTA"."STOSPOL" ("UNIQUE_NUMBER", "ACCOY", "ACPFX", "ACNUM", "ORCCY", "MYFLG", "BTACY", "BTACM", "PLPFX", "PLCOY", "PLNUM", "TRNDT", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
            ACCOY,
            ACPFX,
            ACNUM,
            ORCCY,
            MYFLG,
            BTACY,
            BTACM,
            PLPFX,
            PLCOY,
            PLNUM,
            TRNDT,
            RECORDFORMAT,
            SUBSTR (NONKEYS || RPAD (' ', 1169, ' '), 1, 1169) AS "NONKEYS"
       FROM (SELECT * FROM STOSPOL_STOSPOLREC
             UNION ALL
             SELECT * FROM STOSPOL_UCSHPOLREC) A
   ORDER BY ACCOY,
            ACPFX,
            ACNUM,
            ORCCY,
            MYFLG DESC,
            BTACY,
            BTACM,
            PLPFX,
            PLCOY,
            PLNUM,
            TRNDT;
 


	 
	 
  CREATE OR REPLACE FORCE VIEW "VM1DTA"."STOSPOL_STOSPOLREC" ("UNIQUE_NUMBER", "ACCOY", "ACPFX", "ACNUM", "ORCCY", "MYFLG", "BTACY", "BTACM", "PLPFX", "PLCOY", "PLNUM", "TRNDT", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
          ACCOY,
          ACPFX,
          ACNUM,
          ORCCY,
          MYFLG,
          BTACY,
          BTACM,
          PLPFX,
          PLCOY,
          PLNUM,
          TRNDT,
          'STOSPOLREC' AS RECORDFORMAT,
          SUBSTR (NVL (TO_CHAR (TRNID), ' ') || LPAD (' ', 19, ' '), 1, 19)
          || SUBSTR (NVL (VFLAG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ACCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (PLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (PLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (PLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CHNUM, ' ') || RPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (UNIQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (RECNC, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (DCODE, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SDCRA, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SDCRB, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (SDCRC, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (SDCRD, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (KEYSP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CNTYP, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (TRNDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (EFFDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (ENDTP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CREQT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ORDTF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (TRCDE, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SACCT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC01, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC02, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC03, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC04, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC05, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC06, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC07, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC08, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC09, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC10, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC11, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC12, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC13, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC14, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC15, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (JNLFG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (JOBID, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (BTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (BTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (BTBRN, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (TO_CHAR (BTACY), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (TO_CHAR (BTACM), ' ') || LPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (BTTCD, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (BTBCH, ' ') || RPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (DUEDT, ' ') || RPAD (' ', 6, ' '), 1, 6)
          || SUBSTR (NVL (AGECD, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (RSKCL, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (RSKNR), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (BRKRF, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (OTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (OTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
          || SUBSTR (NVL (OTCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (AMT01), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT02), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT03), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT04), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT05), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT06), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT07), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT08), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT09), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT10), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT11), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT12), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT13), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT14), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT15), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE01), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE02), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE03), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE04), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE05), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE06), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE07), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE08), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE09), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE10), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE11), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE12), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE13), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE14), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE15), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (GPAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CMAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (GRSLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CMLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (MYFLG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (TO_CHAR (MYUQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (ORCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (LCCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (CRATE), ' ') || LPAD (' ', 20, ' '),
                     1,
                     20)
          || SUBSTR (NVL (STCA, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCB, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCC, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCD, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCE, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (MARYACTYR), ' ') || LPAD (' ', 5, ' '),
                     1,
                     5)
          || SUBSTR (NVL (TO_CHAR (MARYACTMN), ' ') || LPAD (' ', 3, ' '),
                     1,
                     3)
          || SUBSTR (NVL (RITYPE, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (TO_CHAR (TRANNO), ' ') || LPAD (' ', 6, ' '), 1, 6)
          || SUBSTR (NVL (JOBNM, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (USRPRF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || NVL (TO_CHAR (DATIME, 'YYYY-MM-DD-HH24.MI.SS.ff6'),
                  '                          ')
				  
		 || SUBSTR (NVL (OTHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2) 
		  || SUBSTR (NVL (OTHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTHNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
		  
             AS "NONKEYS"
     FROM STMTPF ;
	 
	 
	 
  CREATE OR REPLACE FORCE VIEW "VM1DTA"."STOSPOL_UCSHPOLREC" ("UNIQUE_NUMBER", "ACCOY", "ACPFX", "ACNUM", "ORCCY", "MYFLG", "BTACY", "BTACM", "PLPFX", "PLCOY", "PLNUM", "TRNDT", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
          ACCOY,
          ACPFX,
          ACNUM,
          ORCCY,
          MYFLG,
          BTACY,
          BTACM,
          PLPFX,
          PLCOY,
          PLNUM,
          TRNDT,
          'UCSHPOLREC' AS RECORDFORMAT,
          SUBSTR (NVL (TO_CHAR (TRNID), ' ') || LPAD (' ', 19, ' '), 1, 19)
          || SUBSTR (NVL (TRMID, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (VFLAG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ACCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (PLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (PLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (PLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CHNUM, ' ') || RPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (UNIQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (RECNC, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (DCODE, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SDCRA, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SDCRB, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (SDCRC, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (SDCRD, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (KEYSP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CNTYP, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (TRNDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (EFFDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (ENDTP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CREQT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ORDTF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (TRCDE, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SACCT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC01, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC02, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC03, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC04, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC05, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC06, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC07, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC08, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC09, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC10, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC11, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC12, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC13, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC14, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC15, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (JNLFG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (BTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (BTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (BTBRN, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (TO_CHAR (BTACY), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (TO_CHAR (BTACM), ' ') || LPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (BTTCD, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (BTBCH, ' ') || RPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (DUEDT, ' ') || RPAD (' ', 6, ' '), 1, 6)
          || SUBSTR (NVL (AGECD, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (RSKCL, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (RSKNR), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (BRKRF, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (JOBID, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (OTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (OTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
          || SUBSTR (NVL (OTCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (ORCSH), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (OCLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CSHMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CSLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CSHDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (RTOLN), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (RTLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (MYUQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (ORCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (LCCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (CRATE), ' ') || LPAD (' ', 20, ' '),
                     1,
                     20)
          || SUBSTR (NVL (STCA, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCB, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCC, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCD, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCE, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (MARYACTYR), ' ') || LPAD (' ', 5, ' '),
                     1,
                     5)
          || SUBSTR (NVL (TO_CHAR (MARYACTMN), ' ') || LPAD (' ', 3, ' '),
                     1,
                     3)
          || SUBSTR (NVL (MYFLG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (JOBNM, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (USRPRF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || NVL (TO_CHAR (DATIME, 'YYYY-MM-DD-HH24.MI.SS.ff6'),
                  '                          ')
				  
		|| SUBSTR (NVL (OTHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2) 
		  || SUBSTR (NVL (OTHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTHNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
             AS "NONKEYS"
     FROM UCSHPF ;


  CREATE OR REPLACE FORCE VIEW "VM1DTA"."STOSTRN" ("UNIQUE_NUMBER", "ACCOY", "ACPFX", "ACNUM", "ORCCY", "MYFLG", "BTACY", "BTACM", "TRNDT", "PLPFX", "PLCOY", "PLNUM", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
            ACCOY,
            ACPFX,
            ACNUM,
            ORCCY,
            MYFLG,
            BTACY,
            BTACM,
            TRNDT,
            PLPFX,
            PLCOY,
            PLNUM,
            RECORDFORMAT,
            SUBSTR (NONKEYS || RPAD (' ', 1169, ' '), 1, 1169) AS "NONKEYS"
       FROM (SELECT * FROM STOSTRN_STOSTRNREC
             UNION ALL
             SELECT * FROM STOSTRN_UCSHTRNREC) A
   ORDER BY ACCOY,
            ACPFX,
            ACNUM,
            ORCCY,
            MYFLG DESC,
            BTACY,
            BTACM,
            TRNDT,
            PLPFX,
            PLCOY,
            PLNUM ;
 
	 
	 
  CREATE OR REPLACE FORCE VIEW "VM1DTA"."STOSTRN_STOSTRNREC" ("UNIQUE_NUMBER", "ACCOY", "ACPFX", "ACNUM", "ORCCY", "MYFLG", "BTACY", "BTACM", "TRNDT", "PLPFX", "PLCOY", "PLNUM", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
          ACCOY,
          ACPFX,
          ACNUM,
          ORCCY,
          MYFLG,
          BTACY,
          BTACM,
          TRNDT,
          PLPFX,
          PLCOY,
          PLNUM,
          'STOSTRNREC' AS RECORDFORMAT,
          SUBSTR (NVL (TO_CHAR (TRNID), ' ') || LPAD (' ', 19, ' '), 1, 19)
          || SUBSTR (NVL (VFLAG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ACCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (PLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (PLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (PLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CHNUM, ' ') || RPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (UNIQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (RECNC, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (DCODE, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SDCRA, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SDCRB, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (SDCRC, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (SDCRD, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (KEYSP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CNTYP, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (TRNDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (EFFDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (ENDTP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CREQT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ORDTF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (TRCDE, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SACCT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC01, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC02, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC03, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC04, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC05, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC06, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC07, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC08, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC09, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC10, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC11, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC12, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC13, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC14, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC15, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (JNLFG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (JOBID, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (BTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (BTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (BTBRN, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (TO_CHAR (BTACY), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (TO_CHAR (BTACM), ' ') || LPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (BTTCD, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (BTBCH, ' ') || RPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (DUEDT, ' ') || RPAD (' ', 6, ' '), 1, 6)
          || SUBSTR (NVL (AGECD, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (RSKCL, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (RSKNR), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (BRKRF, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (OTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (OTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
          || SUBSTR (NVL (OTCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (AMT01), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT02), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT03), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT04), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT05), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT06), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT07), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT08), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT09), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT10), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT11), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT12), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT13), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT14), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT15), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE01), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE02), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE03), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE04), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE05), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE06), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE07), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE08), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE09), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE10), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE11), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE12), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE13), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE14), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE15), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (GPAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CMAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (GRSLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CMLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (MYFLG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (TO_CHAR (MYUQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (ORCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (LCCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (CRATE), ' ') || LPAD (' ', 20, ' '),
                     1,
                     20)
          || SUBSTR (NVL (STCA, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCB, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCC, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCD, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCE, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (MARYACTYR), ' ') || LPAD (' ', 5, ' '),
                     1,
                     5)
          || SUBSTR (NVL (TO_CHAR (MARYACTMN), ' ') || LPAD (' ', 3, ' '),
                     1,
                     3)
          || SUBSTR (NVL (RITYPE, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (TO_CHAR (TRANNO), ' ') || LPAD (' ', 6, ' '), 1, 6)
          || SUBSTR (NVL (JOBNM, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (USRPRF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || NVL (TO_CHAR (DATIME, 'YYYY-MM-DD-HH24.MI.SS.ff6'),
                  '                          ')
		|| SUBSTR (NVL (OTHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2) 
		  || SUBSTR (NVL (OTHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTHNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)				  
             AS "NONKEYS"
     FROM STMTPF ;
	 
	 
	 
	 
	 
  CREATE OR REPLACE FORCE VIEW "VM1DTA"."STOSTRN_UCSHTRNREC" ("UNIQUE_NUMBER", "ACCOY", "ACPFX", "ACNUM", "ORCCY", "MYFLG", "BTACY", "BTACM", "TRNDT", "PLPFX", "PLCOY", "PLNUM", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
          ACCOY,
          ACPFX,
          ACNUM,
          ORCCY,
          MYFLG,
          BTACY,
          BTACM,
          TRNDT,
          PLPFX,
          PLCOY,
          PLNUM,
          'UCSHTRNREC' AS RECORDFORMAT,
          SUBSTR (NVL (TO_CHAR (TRNID), ' ') || LPAD (' ', 19, ' '), 1, 19)
          || SUBSTR (NVL (TRMID, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (VFLAG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ACCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (PLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (PLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (PLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CHNUM, ' ') || RPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (UNIQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (RECNC, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (DCODE, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SDCRA, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SDCRB, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (SDCRC, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (SDCRD, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (KEYSP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CNTYP, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (TRNDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (EFFDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (ENDTP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CREQT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ORDTF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (TRCDE, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SACCT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC01, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC02, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC03, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC04, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC05, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC06, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC07, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC08, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC09, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC10, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC11, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC12, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC13, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC14, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC15, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (JNLFG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (BTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (BTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (BTBRN, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (TO_CHAR (BTACY), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (TO_CHAR (BTACM), ' ') || LPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (BTTCD, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (BTBCH, ' ') || RPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (DUEDT, ' ') || RPAD (' ', 6, ' '), 1, 6)
          || SUBSTR (NVL (AGECD, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (RSKCL, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (RSKNR), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (BRKRF, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (JOBID, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (OTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (OTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
          || SUBSTR (NVL (OTCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (ORCSH), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (OCLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CSHMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CSLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CSHDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (RTOLN), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (RTLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (MYUQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (ORCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (LCCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (CRATE), ' ') || LPAD (' ', 20, ' '),
                     1,
                     20)
          || SUBSTR (NVL (STCA, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCB, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCC, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCD, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCE, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (MARYACTYR), ' ') || LPAD (' ', 5, ' '),
                     1,
                     5)
          || SUBSTR (NVL (TO_CHAR (MARYACTMN), ' ') || LPAD (' ', 3, ' '),
                     1,
                     3)
          || SUBSTR (NVL (MYFLG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (JOBNM, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (USRPRF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || NVL (TO_CHAR (DATIME, 'YYYY-MM-DD-HH24.MI.SS.ff6'),
                  '                          ')
		|| SUBSTR (NVL (OTHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2) 
		  || SUBSTR (NVL (OTHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTHNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
             AS "NONKEYS"
     FROM UCSHPF ;
	 

  CREATE OR REPLACE FORCE VIEW "VM1DTA"."STUCBCH" ("UNIQUE_NUMBER", "BTCOY", "BTACY", "BTACM", "BTBRN", "BTTCD", "BTBCH", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
            BTCOY,
            BTACY,
            BTACM,
            BTBRN,
            BTTCD,
            BTBCH,
            RECORDFORMAT,
            SUBSTR (NONKEYS || RPAD (' ', 1169, ' '), 1, 1169) AS "NONKEYS"
       FROM (SELECT * FROM STUCBCH_STMTREC
             UNION ALL
             SELECT * FROM STUCBCH_UCSHREC) A
   ORDER BY BTCOY,
            BTACY,
            BTACM,
            BTBRN,
            BTTCD,
            BTBCH ;
 
	 
	 
	 
  CREATE OR REPLACE FORCE VIEW "VM1DTA"."STUCBCH_STMTREC" ("UNIQUE_NUMBER", "BTCOY", "BTACY", "BTACM", "BTBRN", "BTTCD", "BTBCH", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
          BTCOY,
          BTACY,
          BTACM,
          BTBRN,
          BTTCD,
          BTBCH,
          'STMTREC' AS RECORDFORMAT,
          SUBSTR (NVL (TO_CHAR (TRNID), ' ') || LPAD (' ', 19, ' '), 1, 19)
          || SUBSTR (NVL (VFLAG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ACCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (PLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (PLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (PLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CHNUM, ' ') || RPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (UNIQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (RECNC, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (DCODE, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SDCRA, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SDCRB, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (SDCRC, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (SDCRD, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (KEYSP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CNTYP, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (TRNDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (EFFDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (ENDTP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CREQT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ORDTF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (TRCDE, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SACCT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC01, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC02, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC03, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC04, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC05, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC06, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC07, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC08, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC09, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC10, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC11, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC12, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC13, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC14, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC15, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (JNLFG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (JOBID, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (BTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (BTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (BTBRN, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (TO_CHAR (BTACY), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (TO_CHAR (BTACM), ' ') || LPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (BTTCD, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (BTBCH, ' ') || RPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (DUEDT, ' ') || RPAD (' ', 6, ' '), 1, 6)
          || SUBSTR (NVL (AGECD, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (RSKCL, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (RSKNR), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (BRKRF, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (OTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (OTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
          || SUBSTR (NVL (OTCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (AMT01), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT02), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT03), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT04), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT05), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT06), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT07), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT08), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT09), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT10), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT11), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT12), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT13), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT14), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT15), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE01), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE02), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE03), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE04), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE05), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE06), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE07), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE08), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE09), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE10), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE11), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE12), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE13), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE14), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE15), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (GPAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CMAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (GRSLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CMLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (MYFLG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (TO_CHAR (MYUQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (ORCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (LCCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (CRATE), ' ') || LPAD (' ', 20, ' '),
                     1,
                     20)
          || SUBSTR (NVL (STCA, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCB, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCC, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCD, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCE, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (MARYACTYR), ' ') || LPAD (' ', 5, ' '),
                     1,
                     5)
          || SUBSTR (NVL (TO_CHAR (MARYACTMN), ' ') || LPAD (' ', 3, ' '),
                     1,
                     3)
          || SUBSTR (NVL (RITYPE, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (TO_CHAR (TRANNO), ' ') || LPAD (' ', 6, ' '), 1, 6)
          || SUBSTR (NVL (USRPRF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (JOBNM, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || NVL (TO_CHAR (DATIME, 'YYYY-MM-DD-HH24.MI.SS.ff6'),
                  '                          ')
		|| SUBSTR (NVL (OTHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2) 
		  || SUBSTR (NVL (OTHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTHNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
             AS "NONKEYS"
     FROM STMTPF ;
 
 
 
  CREATE OR REPLACE FORCE VIEW "VM1DTA"."STUCBCH_UCSHREC" ("UNIQUE_NUMBER", "BTCOY", "BTACY", "BTACM", "BTBRN", "BTTCD", "BTBCH", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
          BTCOY,
          BTACY,
          BTACM,
          BTBRN,
          BTTCD,
          BTBCH,
          'UCSHREC' AS RECORDFORMAT,
          SUBSTR (NVL (TO_CHAR (TRNID), ' ') || LPAD (' ', 19, ' '), 1, 19)
          || SUBSTR (NVL (TRMID, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (VFLAG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ACCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (PLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (PLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (PLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CHNUM, ' ') || RPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (UNIQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (RECNC, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (DCODE, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SDCRA, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SDCRB, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (SDCRC, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (SDCRD, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (KEYSP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CNTYP, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (TRNDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (EFFDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (ENDTP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CREQT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ORDTF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (TRCDE, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SACCT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC01, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC02, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC03, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC04, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC05, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC06, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC07, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC08, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC09, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC10, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC11, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC12, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC13, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC14, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC15, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (JNLFG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (BTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (BTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (BTBRN, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (TO_CHAR (BTACY), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (TO_CHAR (BTACM), ' ') || LPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (BTTCD, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (BTBCH, ' ') || RPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (DUEDT, ' ') || RPAD (' ', 6, ' '), 1, 6)
          || SUBSTR (NVL (AGECD, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (RSKCL, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (RSKNR), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (BRKRF, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (JOBID, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (OTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (OTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
          || SUBSTR (NVL (OTCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (ORCSH), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (OCLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CSHMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CSLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CSHDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (RTOLN), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (RTLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (MYUQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (ORCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (LCCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (CRATE), ' ') || LPAD (' ', 20, ' '),
                     1,
                     20)
          || SUBSTR (NVL (STCA, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCB, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCC, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCD, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCE, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (MARYACTYR), ' ') || LPAD (' ', 5, ' '),
                     1,
                     5)
          || SUBSTR (NVL (TO_CHAR (MARYACTMN), ' ') || LPAD (' ', 3, ' '),
                     1,
                     3)
          || SUBSTR (NVL (USRPRF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (JOBNM, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || NVL (TO_CHAR (DATIME, 'YYYY-MM-DD-HH24.MI.SS.ff6'),
                  '                          ')
          || SUBSTR (NVL (MYFLG, ' ') || RPAD (' ', 1, ' '), 1, 1)
		  || SUBSTR (NVL (OTHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2) 
		  || SUBSTR (NVL (OTHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTHNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
             AS "NONKEYS"
     FROM UCSHPF ;
	 

  CREATE OR REPLACE FORCE VIEW "VM1DTA"."STUC" ("UNIQUE_NUMBER", "ACCOY", "ACPFX", "ACNUM", "ORCCY", "TRNDT", "PLPFX", "PLCOY", "PLNUM", "CNTYP", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
            ACCOY,
            ACPFX,
            ACNUM,
            ORCCY,
            TRNDT,
            PLPFX,
            PLCOY,
            PLNUM,
            CNTYP,
            RECORDFORMAT,
            SUBSTR (NONKEYS || RPAD (' ', 1169, ' '), 1, 1169) AS "NONKEYS"
       FROM (SELECT * FROM STUC_STMTREC
             UNION ALL
             SELECT * FROM STUC_UCSHREC) A
   ORDER BY ACCOY,
            ACPFX,
            ACNUM,
            ORCCY,
            TRNDT,
            PLPFX,
            PLCOY,
            PLNUM,
            CNTYP ;
 
	 
	 
  CREATE OR REPLACE FORCE VIEW "VM1DTA"."STUC_STMTREC" ("UNIQUE_NUMBER", "ACCOY", "ACPFX", "ACNUM", "ORCCY", "TRNDT", "PLPFX", "PLCOY", "PLNUM", "CNTYP", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
          ACCOY,
          ACPFX,
          ACNUM,
          ORCCY,
          TRNDT,
          PLPFX,
          PLCOY,
          PLNUM,
          CNTYP,
          'STMTREC' AS RECORDFORMAT,
          SUBSTR (NVL (TO_CHAR (TRNID), ' ') || LPAD (' ', 19, ' '), 1, 19)
          || SUBSTR (NVL (VFLAG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ACCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (PLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (PLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (PLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CHNUM, ' ') || RPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (UNIQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (RECNC, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (DCODE, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SDCRA, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SDCRB, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (SDCRC, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (SDCRD, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (CNTYP, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (TRNDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (EFFDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TRCDE, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SACCT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC01, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC02, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC03, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC04, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC05, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC06, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC07, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC08, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC09, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC10, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC11, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC12, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC13, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC14, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC15, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (BTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (BTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (BTBRN, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (TO_CHAR (BTACY), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (TO_CHAR (BTACM), ' ') || LPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (BTTCD, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (BTBCH, ' ') || RPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (DUEDT, ' ') || RPAD (' ', 6, ' '), 1, 6)
          || SUBSTR (NVL (AGECD, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (RSKCL, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (RSKNR), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (BRKRF, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (TO_CHAR (AMT01), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT02), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT03), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT04), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT05), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT06), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT07), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT08), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT09), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT10), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT11), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT12), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT13), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT14), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT15), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE01), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE02), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE03), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE04), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE05), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE06), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE07), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE08), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE09), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE10), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE11), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE12), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE13), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE14), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE15), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (GPAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CMAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (GRSLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CMLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (MYFLG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (TO_CHAR (MYUQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (ORCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (LCCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (CRATE), ' ') || LPAD (' ', 20, ' '),
                     1,
                     20)
          || SUBSTR (NVL (STCA, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCB, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCC, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCD, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCE, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (MARYACTYR), ' ') || LPAD (' ', 5, ' '),
                     1,
                     5)
          || SUBSTR (NVL (TO_CHAR (MARYACTMN), ' ') || LPAD (' ', 3, ' '),
                     1,
                     3)
          || SUBSTR (NVL (RITYPE, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (TO_CHAR (TRANNO), ' ') || LPAD (' ', 6, ' '), 1, 6)
          || SUBSTR (NVL (USRPRF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (JOBNM, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || NVL (TO_CHAR (DATIME, 'YYYY-MM-DD-HH24.MI.SS.ff6'),
                  '                          ')
          || SUBSTR (NVL (KEYSP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ENDTP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CREQT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ORDTF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (JNLFG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (JOBID, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (OTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (OTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
          || SUBSTR (NVL (OTCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
		  || SUBSTR (NVL (OTHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2) 
		  || SUBSTR (NVL (OTHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTHNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
		  
             AS "NONKEYS"
     FROM STMTPF ;
 
 
 
 
  CREATE OR REPLACE FORCE VIEW "VM1DTA"."STUC_UCSHREC" ("UNIQUE_NUMBER", "ACCOY", "ACPFX", "ACNUM", "ORCCY", "TRNDT", "PLPFX", "PLCOY", "PLNUM", "CNTYP", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
          ACCOY,
          ACPFX,
          ACNUM,
          ORCCY,
          TRNDT,
          PLPFX,
          PLCOY,
          PLNUM,
          CNTYP,
          'UCSHREC' AS RECORDFORMAT,
          SUBSTR (NVL (TO_CHAR (TRNID), ' ') || LPAD (' ', 19, ' '), 1, 19)
          || SUBSTR (NVL (VFLAG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ACCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (PLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (PLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (PLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CHNUM, ' ') || RPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (UNIQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (RECNC, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (DCODE, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SDCRA, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SDCRB, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (SDCRC, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (SDCRD, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (CNTYP, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (TRNDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (EFFDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TRCDE, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SACCT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC01, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC02, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC03, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC04, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC05, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC06, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC07, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC08, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC09, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC10, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC11, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC12, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC13, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC14, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC15, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (BTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (BTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (BTBRN, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (TO_CHAR (BTACY), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (TO_CHAR (BTACM), ' ') || LPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (BTTCD, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (BTBCH, ' ') || RPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (DUEDT, ' ') || RPAD (' ', 6, ' '), 1, 6)
          || SUBSTR (NVL (AGECD, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (RSKCL, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (RSKNR), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (BRKRF, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (TO_CHAR (ORCSH), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (OCLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CSHMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CSLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CSHDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (MYUQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (ORCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (LCCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (CRATE), ' ') || LPAD (' ', 20, ' '),
                     1,
                     20)
          || SUBSTR (NVL (STCA, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCB, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCC, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCD, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCE, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (MARYACTYR), ' ') || LPAD (' ', 5, ' '),
                     1,
                     5)
          || SUBSTR (NVL (TO_CHAR (MARYACTMN), ' ') || LPAD (' ', 3, ' '),
                     1,
                     3)
          || SUBSTR (NVL (USRPRF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (JOBNM, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || NVL (TO_CHAR (DATIME, 'YYYY-MM-DD-HH24.MI.SS.ff6'),
                  '                          ')
          || SUBSTR (NVL (TRMID, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (KEYSP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ENDTP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CREQT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ORDTF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (JNLFG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (JOBID, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (OTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (OTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
          || SUBSTR (NVL (OTCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (RTOLN), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (RTLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (MYFLG, ' ') || RPAD (' ', 1, ' '), 1, 1)
		   || SUBSTR (NVL (OTHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2) 
		  || SUBSTR (NVL (OTHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTHNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
             AS "NONKEYS"
     FROM UCSHPF ;
 
 
  CREATE OR REPLACE FORCE VIEW "VM1DTA"."SUCL" ("UNIQUE_NUMBER", "CLPFX", "CLCOY", "CLNUM", "ORCCY", "PLPFX", "PLCOY", "PLNUM", "CNTYP", "TRNDT", "ACPFX", "ACCOY", "ACNUM", "UNIQK", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
            CLPFX,
            CLCOY,
            CLNUM,
            ORCCY,
            PLPFX,
            PLCOY,
            PLNUM,
            CNTYP,
            TRNDT,
            ACPFX,
            ACCOY,
            ACNUM,
            UNIQK,
            RECORDFORMAT,
            SUBSTR (NONKEYS || RPAD (' ', 1169, ' '), 1, 1169) AS "NONKEYS"
       FROM (SELECT * FROM SUCL_STCLREC
             UNION ALL
             SELECT * FROM SUCL_UCCLREC) A
   ORDER BY CLPFX,
            CLCOY,
            CLNUM,
            ORCCY,
            PLPFX,
            PLCOY,
            PLNUM,
            CNTYP,
            TRNDT,
            ACPFX,
            ACCOY,
            ACNUM,
            UNIQK ;
 

 
 
  CREATE OR REPLACE FORCE VIEW "VM1DTA"."SUCL_STCLREC" ("UNIQUE_NUMBER", "CLPFX", "CLCOY", "CLNUM", "ORCCY", "PLPFX", "PLCOY", "PLNUM", "CNTYP", "TRNDT", "ACPFX", "ACCOY", "ACNUM", "UNIQK", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
          CLPFX,
          CLCOY,
          CLNUM,
          ORCCY,
          PLPFX,
          PLCOY,
          PLNUM,
          CNTYP,
          TRNDT,
          ACPFX,
          ACCOY,
          ACNUM,
          UNIQK,
          'STCLREC' AS RECORDFORMAT,
          SUBSTR (NVL (TO_CHAR (TRNID), ' ') || LPAD (' ', 19, ' '), 1, 19)
          || SUBSTR (NVL (VFLAG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ACCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (PLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (PLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (PLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CHNUM, ' ') || RPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (UNIQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (RECNC, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (DCODE, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SDCRA, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SDCRB, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (SDCRC, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (SDCRD, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (CNTYP, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (TRNDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (EFFDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TRCDE, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SACCT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC01, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC02, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC03, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC04, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC05, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC06, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC07, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC08, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC09, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC10, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC11, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC12, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC13, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC14, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC15, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (BTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (BTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (BTBRN, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (TO_CHAR (BTACY), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (TO_CHAR (BTACM), ' ') || LPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (BTTCD, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (BTBCH, ' ') || RPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (DUEDT, ' ') || RPAD (' ', 6, ' '), 1, 6)
          || SUBSTR (NVL (AGECD, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (RSKCL, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (RSKNR), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (BRKRF, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (TO_CHAR (AMT01), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT02), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT03), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT04), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT05), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT06), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT07), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT08), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT09), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT10), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT11), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT12), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT13), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT14), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT15), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE01), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE02), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE03), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE04), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE05), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE06), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE07), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE08), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE09), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE10), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE11), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE12), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE13), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE14), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE15), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (GPAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CMAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (GRSLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CMLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (MYFLG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (TO_CHAR (MYUQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (ORCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (LCCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (CRATE), ' ') || LPAD (' ', 20, ' '),
                     1,
                     20)
          || SUBSTR (NVL (USRPRF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (JOBNM, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || NVL (TO_CHAR (DATIME, 'YYYY-MM-DD-HH24.MI.SS.ff6'),
                  '                          ')
          || SUBSTR (NVL (KEYSP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ENDTP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CREQT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ORDTF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (JNLFG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (JOBID, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (OTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (OTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
          || SUBSTR (NVL (OTCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCA, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCB, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCC, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCD, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCE, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (MARYACTYR), ' ') || LPAD (' ', 5, ' '),
                     1,
                     5)
          || SUBSTR (NVL (TO_CHAR (MARYACTMN), ' ') || LPAD (' ', 3, ' '),
                     1,
                     3)
          || SUBSTR (NVL (RITYPE, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (TO_CHAR (TRANNO), ' ') || LPAD (' ', 6, ' '), 1, 6)
		    || SUBSTR (NVL (OTHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2) 
		  || SUBSTR (NVL (OTHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTHNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
             AS "NONKEYS"
     FROM STMTPF ;
	 
	 
	 
	 
  CREATE OR REPLACE FORCE VIEW "VM1DTA"."SUCL_UCCLREC" ("UNIQUE_NUMBER", "CLPFX", "CLCOY", "CLNUM", "ORCCY", "PLPFX", "PLCOY", "PLNUM", "CNTYP", "TRNDT", "ACPFX", "ACCOY", "ACNUM", "UNIQK", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
          CLPFX,
          CLCOY,
          CLNUM,
          ORCCY,
          PLPFX,
          PLCOY,
          PLNUM,
          CNTYP,
          TRNDT,
          ACPFX,
          ACCOY,
          ACNUM,
          UNIQK,
          'UCCLREC' AS RECORDFORMAT,
          SUBSTR (NVL (TO_CHAR (TRNID), ' ') || LPAD (' ', 19, ' '), 1, 19)
          || SUBSTR (NVL (VFLAG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ACCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (PLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (PLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (PLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CHNUM, ' ') || RPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (UNIQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (RECNC, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (DCODE, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SDCRA, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SDCRB, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (SDCRC, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (SDCRD, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (CNTYP, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (TRNDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (EFFDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TRCDE, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SACCT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC01, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC02, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC03, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC04, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC05, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC06, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC07, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC08, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC09, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC10, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC11, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC12, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC13, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC14, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC15, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (BTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (BTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (BTBRN, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (TO_CHAR (BTACY), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (TO_CHAR (BTACM), ' ') || LPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (BTTCD, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (BTBCH, ' ') || RPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (DUEDT, ' ') || RPAD (' ', 6, ' '), 1, 6)
          || SUBSTR (NVL (AGECD, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (RSKCL, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (RSKNR), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (BRKRF, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (TO_CHAR (ORCSH), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (OCLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CSHMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CSLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CSHDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (MYUQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (ORCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (LCCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (CRATE), ' ') || LPAD (' ', 20, ' '),
                     1,
                     20)
          || SUBSTR (NVL (USRPRF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (JOBNM, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || NVL (TO_CHAR (DATIME, 'YYYY-MM-DD-HH24.MI.SS.ff6'),
                  '                          ')
          || SUBSTR (NVL (TRMID, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (KEYSP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ENDTP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CREQT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ORDTF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (JNLFG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (JOBID, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (OTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (OTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
          || SUBSTR (NVL (OTCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (RTOLN), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (RTLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (STCA, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCB, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCC, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCD, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCE, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (MARYACTYR), ' ') || LPAD (' ', 5, ' '),
                     1,
                     5)
          || SUBSTR (NVL (TO_CHAR (MARYACTMN), ' ') || LPAD (' ', 3, ' '),
                     1,
                     3)
          || SUBSTR (NVL (MYFLG, ' ') || RPAD (' ', 1, ' '), 1, 1)
		   || SUBSTR (NVL (OTHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2) 
		  || SUBSTR (NVL (OTHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTHNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
             AS "NONKEYS"
     FROM UCSHPF ;
	 

  CREATE OR REPLACE FORCE VIEW "VM1DTA"."SUPL" ("UNIQUE_NUMBER", "PLPFX", "PLCOY", "PLNUM", "TRNDT", "CLPFX", "CLCOY", "CLNUM", "CHPFX", "MYFLG", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
            PLPFX,
            PLCOY,
            PLNUM,
            TRNDT,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            MYFLG,
            RECORDFORMAT,
            RPAD (NONKEYS, 1489, ' ') AS "NONKEYS"
       FROM (SELECT * FROM SUPL_STPLREC
             UNION ALL
             SELECT * FROM SUPL_UCPLREC) A
   ORDER BY PLPFX,
            PLCOY,
            PLNUM,
            TRNDT,
            CLPFX,
            CLCOY,
            CLNUM,
            CHPFX,
            MYFLG ;
 
	 
	 
  CREATE OR REPLACE FORCE VIEW "VM1DTA"."SUPL_STPLREC" ("UNIQUE_NUMBER", "PLPFX", "PLCOY", "PLNUM", "TRNDT", "CLPFX", "CLCOY", "CLNUM", "CHPFX", "MYFLG", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
          PLPFX,
          PLCOY,
          PLNUM,
          TRNDT,
          CLPFX,
          CLCOY,
          CLNUM,
          CHPFX,
          MYFLG,
          'STPLREC' AS RECORDFORMAT,
             LPAD (NVL (TO_CHAR (TRNID), ' '), 19, ' ')
          || RPAD (NVL (VFLAG, ' '), 1, ' ')
          || RPAD (NVL (ACPFX, ' '), 2, ' ')
          || RPAD (NVL (ACCOY, ' '), 1, ' ')
          || RPAD (NVL (ACNUM, ' '), 8, ' ')
          || RPAD (NVL (PLPFX, ' '), 2, ' ')
          || RPAD (NVL (PLCOY, ' '), 1, ' ')
          || RPAD (NVL (PLNUM, ' '), 8, ' ')
          || RPAD (NVL (CLPFX, ' '), 2, ' ')
          || RPAD (NVL (CLCOY, ' '), 1, ' ')
          || RPAD (NVL (CLNUM, ' '), 8, ' ')
          || RPAD (NVL (CHPFX, ' '), 2, ' ')
          || RPAD (NVL (CHCOY, ' '), 1, ' ')
          || RPAD (NVL (CHNUM, ' '), 9, ' ')
          || LPAD (NVL (TO_CHAR (UNIQK), ' '), 19, ' ')
          || RPAD (NVL (RECNC, ' '), 2, ' ')
          || RPAD (NVL (DCODE, ' '), 2, ' ')
          || RPAD (NVL (SDCRA, ' '), 4, ' ')
          || RPAD (NVL (SDCRB, ' '), 8, ' ')
          || RPAD (NVL (SDCRC, ' '), 50, ' ')
          || RPAD (NVL (SDCRD, ' '), 50, ' ')
          || RPAD (NVL (KEYSP, ' '), 1, ' ')
          || RPAD (NVL (CNTYP, ' '), 3, ' ')
          || LPAD (NVL (TO_CHAR (TRNDT), ' '), 9, ' ')
          || LPAD (NVL (TO_CHAR (EFFDT), ' '), 9, ' ')
          || RPAD (NVL (ENDTP, ' '), 1, ' ')
          || RPAD (NVL (CREQT, ' '), 2, ' ')
          || RPAD (NVL (ORDTF, ' '), 10, ' ')
          || RPAD (NVL (TRCDE, ' '), 4, ' ')
          || RPAD (NVL (SACCT, ' '), 2, ' ')
          || RPAD (NVL (SAC01, ' '), 2, ' ')
          || RPAD (NVL (SAC02, ' '), 2, ' ')
          || RPAD (NVL (SAC03, ' '), 2, ' ')
          || RPAD (NVL (SAC04, ' '), 2, ' ')
          || RPAD (NVL (SAC05, ' '), 2, ' ')
          || RPAD (NVL (SAC06, ' '), 2, ' ')
          || RPAD (NVL (SAC07, ' '), 2, ' ')
          || RPAD (NVL (SAC08, ' '), 2, ' ')
          || RPAD (NVL (SAC09, ' '), 2, ' ')
          || RPAD (NVL (SAC10, ' '), 2, ' ')
          || RPAD (NVL (SAC11, ' '), 2, ' ')
          || RPAD (NVL (SAC12, ' '), 2, ' ')
          || RPAD (NVL (SAC13, ' '), 2, ' ')
          || RPAD (NVL (SAC14, ' '), 2, ' ')
          || RPAD (NVL (SAC15, ' '), 2, ' ')
          || RPAD (NVL (JNLFG, ' '), 1, ' ')
          || RPAD (NVL (JOBID, ' '), 8, ' ')
          || RPAD (NVL (BTPFX, ' '), 2, ' ')
          || RPAD (NVL (BTCOY, ' '), 1, ' ')
          || RPAD (NVL (BTBRN, ' '), 2, ' ')
          || LPAD (NVL (TO_CHAR (BTACY), ' '), 5, ' ')
          || LPAD (NVL (TO_CHAR (BTACM), ' '), 3, ' ')
          || RPAD (NVL (BTTCD, ' '), 4, ' ')
          || RPAD (NVL (BTBCH, ' '), 5, ' ')
          || RPAD (NVL (DUEDT, ' '), 6, ' ')
          || RPAD (NVL (AGECD, ' '), 1, ' ')
          || RPAD (NVL (RSKCL, ' '), 3, ' ')
          || LPAD (NVL (TO_CHAR (RSKNR), ' '), 5, ' ')
          || RPAD (NVL (BRKRF, ' '), 8, ' ')
          || RPAD (NVL (OTPFX, ' '), 2, ' ')
          || RPAD (NVL (OTCOY, ' '), 1, ' ')
          || RPAD (NVL (OTNUM, ' '), 14, ' ')
          || RPAD (NVL (OTCCY, ' '), 3, ' ')
          || LPAD (NVL (TO_CHAR (AMT01), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (AMT02), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (AMT03), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (AMT04), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (AMT05), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (AMT06), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (AMT07), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (AMT08), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (AMT09), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (AMT10), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (AMT11), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (AMT12), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (AMT13), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (AMT14), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (AMT15), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (LCE01), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (LCE02), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (LCE03), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (LCE04), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (LCE05), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (LCE06), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (LCE07), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (LCE08), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (LCE09), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (LCE10), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (LCE11), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (LCE12), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (LCE13), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (LCE14), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (LCE15), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (GPAMT), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (CMAMT), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (NTAMT), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (GRSLCE), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (CMLCE), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (NTLCE), ' '), 19, ' ')
          || RPAD (NVL (MYFLG, ' '), 1, ' ')
          || LPAD (NVL (TO_CHAR (MYUQK), ' '), 19, ' ')
          || RPAD (NVL (ORCCY, ' '), 3, ' ')
          || RPAD (NVL (LCCCY, ' '), 3, ' ')
          || LPAD (NVL (TO_CHAR (CRATE), ' '), 20, ' ')
          || RPAD (NVL (STCA, ' '), 3, ' ')
          || RPAD (NVL (STCB, ' '), 3, ' ')
          || RPAD (NVL (STCC, ' '), 3, ' ')
          || RPAD (NVL (STCD, ' '), 3, ' ')
          || RPAD (NVL (STCE, ' '), 3, ' ')
          || LPAD (NVL (TO_CHAR (MARYACTYR), ' '), 5, ' ')
          || LPAD (NVL (TO_CHAR (MARYACTMN), ' '), 3, ' ')
          || RPAD (NVL (RITYPE, ' '), 2, ' ')
          || LPAD (NVL (TO_CHAR (TRANNO), ' '), 6, ' ')
          || RPAD (NVL (JOBNM, ' '), 10, ' ')
          || RPAD (NVL (USRPRF, ' '), 10, ' ')
          || NVL (TO_CHAR (DATIME, 'YYYY-MM-DD-HH24.MI.SS.ff6'),
                  '                          ')
		|| SUBSTR (NVL (OTHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2) 
		  || SUBSTR (NVL (OTHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTHNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
             AS "NONKEYS"
     FROM STMTPF;
 

 
  CREATE OR REPLACE FORCE VIEW "VM1DTA"."SUPL_UCPLREC" ("UNIQUE_NUMBER", "PLPFX", "PLCOY", "PLNUM", "TRNDT", "CLPFX", "CLCOY", "CLNUM", "CHPFX", "MYFLG", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
          PLPFX,
          PLCOY,
          PLNUM,
          TRNDT,
          CLPFX,
          CLCOY,
          CLNUM,
          CHPFX,
          MYFLG,
          'UCPLREC' AS RECORDFORMAT,
             LPAD (NVL (TO_CHAR (TRNID), ' '), 19, ' ')
          || RPAD (NVL (TRMID, ' '), 4, ' ')
          || RPAD (NVL (VFLAG, ' '), 1, ' ')
          || RPAD (NVL (ACPFX, ' '), 2, ' ')
          || RPAD (NVL (ACCOY, ' '), 1, ' ')
          || RPAD (NVL (ACNUM, ' '), 8, ' ')
          || RPAD (NVL (PLPFX, ' '), 2, ' ')
          || RPAD (NVL (PLCOY, ' '), 1, ' ')
          || RPAD (NVL (PLNUM, ' '), 8, ' ')
          || RPAD (NVL (CLPFX, ' '), 2, ' ')
          || RPAD (NVL (CLCOY, ' '), 1, ' ')
          || RPAD (NVL (CLNUM, ' '), 8, ' ')
          || RPAD (NVL (CHPFX, ' '), 2, ' ')
          || RPAD (NVL (CHCOY, ' '), 1, ' ')
          || RPAD (NVL (CHNUM, ' '), 9, ' ')
          || LPAD (NVL (TO_CHAR (UNIQK), ' '), 19, ' ')
          || RPAD (NVL (RECNC, ' '), 2, ' ')
          || RPAD (NVL (DCODE, ' '), 2, ' ')
          || RPAD (NVL (SDCRA, ' '), 4, ' ')
          || RPAD (NVL (SDCRB, ' '), 8, ' ')
          || RPAD (NVL (SDCRC, ' '), 50, ' ')
          || RPAD (NVL (SDCRD, ' '), 50, ' ')
          || RPAD (NVL (KEYSP, ' '), 1, ' ')
          || RPAD (NVL (CNTYP, ' '), 3, ' ')
          || LPAD (NVL (TO_CHAR (TRNDT), ' '), 9, ' ')
          || LPAD (NVL (TO_CHAR (EFFDT), ' '), 9, ' ')
          || RPAD (NVL (ENDTP, ' '), 1, ' ')
          || RPAD (NVL (CREQT, ' '), 2, ' ')
          || RPAD (NVL (ORDTF, ' '), 10, ' ')
          || RPAD (NVL (TRCDE, ' '), 4, ' ')
          || RPAD (NVL (SACCT, ' '), 2, ' ')
          || RPAD (NVL (SAC01, ' '), 2, ' ')
          || RPAD (NVL (SAC02, ' '), 2, ' ')
          || RPAD (NVL (SAC03, ' '), 2, ' ')
          || RPAD (NVL (SAC04, ' '), 2, ' ')
          || RPAD (NVL (SAC05, ' '), 2, ' ')
          || RPAD (NVL (SAC06, ' '), 2, ' ')
          || RPAD (NVL (SAC07, ' '), 2, ' ')
          || RPAD (NVL (SAC08, ' '), 2, ' ')
          || RPAD (NVL (SAC09, ' '), 2, ' ')
          || RPAD (NVL (SAC10, ' '), 2, ' ')
          || RPAD (NVL (SAC11, ' '), 2, ' ')
          || RPAD (NVL (SAC12, ' '), 2, ' ')
          || RPAD (NVL (SAC13, ' '), 2, ' ')
          || RPAD (NVL (SAC14, ' '), 2, ' ')
          || RPAD (NVL (SAC15, ' '), 2, ' ')
          || RPAD (NVL (JNLFG, ' '), 1, ' ')
          || RPAD (NVL (BTPFX, ' '), 2, ' ')
          || RPAD (NVL (BTCOY, ' '), 1, ' ')
          || RPAD (NVL (BTBRN, ' '), 2, ' ')
          || LPAD (NVL (TO_CHAR (BTACY), ' '), 5, ' ')
          || LPAD (NVL (TO_CHAR (BTACM), ' '), 3, ' ')
          || RPAD (NVL (BTTCD, ' '), 4, ' ')
          || RPAD (NVL (BTBCH, ' '), 5, ' ')
          || RPAD (NVL (DUEDT, ' '), 6, ' ')
          || RPAD (NVL (AGECD, ' '), 1, ' ')
          || RPAD (NVL (RSKCL, ' '), 3, ' ')
          || LPAD (NVL (TO_CHAR (RSKNR), ' '), 5, ' ')
          || RPAD (NVL (BRKRF, ' '), 8, ' ')
          || RPAD (NVL (JOBID, ' '), 8, ' ')
          || RPAD (NVL (OTPFX, ' '), 2, ' ')
          || RPAD (NVL (OTCOY, ' '), 1, ' ')
          || RPAD (NVL (OTNUM, ' '), 14, ' ')
          || RPAD (NVL (OTCCY, ' '), 3, ' ')
          || LPAD (NVL (TO_CHAR (ORCSH), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (OCLCE), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (NTAMT), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (NTLCE), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (CSHMT), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (CSLCE), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (CSHDT), ' '), 9, ' ')
          || LPAD (NVL (TO_CHAR (RTOLN), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (RTLCE), ' '), 19, ' ')
          || LPAD (NVL (TO_CHAR (MYUQK), ' '), 19, ' ')
          || RPAD (NVL (ORCCY, ' '), 3, ' ')
          || RPAD (NVL (LCCCY, ' '), 3, ' ')
          || LPAD (NVL (TO_CHAR (CRATE), ' '), 20, ' ')
          || RPAD (NVL (STCA, ' '), 3, ' ')
          || RPAD (NVL (STCB, ' '), 3, ' ')
          || RPAD (NVL (STCC, ' '), 3, ' ')
          || RPAD (NVL (STCD, ' '), 3, ' ')
          || RPAD (NVL (STCE, ' '), 3, ' ')
          || LPAD (NVL (TO_CHAR (MARYACTYR), ' '), 5, ' ')
          || LPAD (NVL (TO_CHAR (MARYACTMN), ' '), 3, ' ')
          || RPAD (NVL (MYFLG, ' '), 1, ' ')
          || RPAD (NVL (JOBNM, ' '), 10, ' ')
          || RPAD (NVL (USRPRF, ' '), 10, ' ')
          || NVL (TO_CHAR (DATIME, 'YYYY-MM-DD-HH24.MI.SS.ff6'),
                  '                          ')
		 || SUBSTR (NVL (OTHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2) 
		  || SUBSTR (NVL (OTHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTHNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
             AS "NONKEYS"
     FROM UCSHPF;
 
 
 
  CREATE OR REPLACE FORCE VIEW "VM1DTA"."SUSQ" ("UNIQUE_NUMBER", "ACPFX", "ACCOY", "ACNUM", "UNIQK", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
    ACPFX,
    ACCOY,
    ACNUM,
    UNIQK,
    RECORDFORMAT,
    SUBSTR (NONKEYS
    || RPAD (' ', 1179, ' '), 1, 1179) AS "NONKEYS"
  FROM
    (SELECT * FROM SUSQ_STMTREC
    UNION ALL
    SELECT * FROM SUSQ_UCSHREC
    ) A
  ORDER BY ACPFX,
    ACCOY,
    ACNUM,
    UNIQK
 ;
 

 
  CREATE OR REPLACE FORCE VIEW "VM1DTA"."SUSQ_STMTREC" ("UNIQUE_NUMBER", "ACPFX", "ACCOY", "ACNUM", "UNIQK", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
    ACPFX,
    ACCOY,
    ACNUM,
    UNIQK,
    'STMTREC' AS RECORDFORMAT,
    SUBSTR (NVL (TO_CHAR (TRNID), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (VFLAG, ' ')
    || RPAD (' ', 1, ' '), 1, 1)
    || SUBSTR (NVL (ACPFX, ' ')
    || RPAD (' ', 2, ' '), 1, 2)
    || SUBSTR (NVL (ACCOY, ' ')
    || RPAD (' ', 1, ' '), 1, 1)
    || SUBSTR (NVL (ACNUM, ' ')
    || RPAD (' ', 8, ' '), 1, 8)
    || SUBSTR (NVL (PLPFX, ' ')
    || RPAD (' ', 2, ' '), 1, 2)
    || SUBSTR (NVL (PLCOY, ' ')
    || RPAD (' ', 1, ' '), 1, 1)
    || SUBSTR (NVL (PLNUM, ' ')
    || RPAD (' ', 8, ' '), 1, 8)
    || SUBSTR (NVL (CLPFX, ' ')
    || RPAD (' ', 2, ' '), 1, 2)
    || SUBSTR (NVL (CLCOY, ' ')
    || RPAD (' ', 1, ' '), 1, 1)
    || SUBSTR (NVL (CLNUM, ' ')
    || RPAD (' ', 8, ' '), 1, 8)
    || SUBSTR (NVL (CHPFX, ' ')
    || RPAD (' ', 2, ' '), 1, 2)
    || SUBSTR (NVL (CHCOY, ' ')
    || RPAD (' ', 1, ' '), 1, 1)
    || SUBSTR (NVL (CHNUM, ' ')
    || RPAD (' ', 9, ' '), 1, 9)
    || SUBSTR (NVL (TO_CHAR (UNIQK), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (RECNC, ' ')
    || RPAD (' ', 2, ' '), 1, 2)
    || SUBSTR (NVL (DCODE, ' ')
    || RPAD (' ', 2, ' '), 1, 2)
    || SUBSTR (NVL (SDCRA, ' ')
    || RPAD (' ', 4, ' '), 1, 4)
    || SUBSTR (NVL (SDCRB, ' ')
    || RPAD (' ', 8, ' '), 1, 8)
    || SUBSTR (NVL (SDCRC, ' ')
    || RPAD (' ', 50, ' '), 1, 50)
    || SUBSTR (NVL (SDCRD, ' ')
    || RPAD (' ', 50, ' '), 1, 50)
    || SUBSTR (NVL (KEYSP, ' ')
    || RPAD (' ', 1, ' '), 1, 1)
    || SUBSTR (NVL (CNTYP, ' ')
    || RPAD (' ', 3, ' '), 1, 3)
    || SUBSTR (NVL (TO_CHAR (TRNDT), ' ')
    || LPAD (' ', 9, ' '), 1, 9)
    || SUBSTR (NVL (TO_CHAR (EFFDT), ' ')
    || LPAD (' ', 9, ' '), 1, 9)
    || SUBSTR (NVL (ENDTP, ' ')
    || RPAD (' ', 1, ' '), 1, 1)
    || SUBSTR (NVL (CREQT, ' ')
    || RPAD (' ', 2, ' '), 1, 2)
    || SUBSTR (NVL (ORDTF, ' ')
    || RPAD (' ', 10, ' '), 1, 10)
    || SUBSTR (NVL (TRCDE, ' ')
    || RPAD (' ', 4, ' '), 1, 4)
    || SUBSTR (NVL (SACCT, ' ')
    || RPAD (' ', 2, ' '), 1, 2)
    || SUBSTR (NVL (SAC01, ' ')
    || RPAD (' ', 2, ' '), 1, 2)
    || SUBSTR (NVL (SAC02, ' ')
    || RPAD (' ', 2, ' '), 1, 2)
    || SUBSTR (NVL (SAC03, ' ')
    || RPAD (' ', 2, ' '), 1, 2)
    || SUBSTR (NVL (SAC04, ' ')
    || RPAD (' ', 2, ' '), 1, 2)
    || SUBSTR (NVL (SAC05, ' ')
    || RPAD (' ', 2, ' '), 1, 2)
    || SUBSTR (NVL (SAC06, ' ')
    || RPAD (' ', 2, ' '), 1, 2)
    || SUBSTR (NVL (SAC07, ' ')
    || RPAD (' ', 2, ' '), 1, 2)
    || SUBSTR (NVL (SAC08, ' ')
    || RPAD (' ', 2, ' '), 1, 2)
    || SUBSTR (NVL (SAC09, ' ')
    || RPAD (' ', 2, ' '), 1, 2)
    || SUBSTR (NVL (SAC10, ' ')
    || RPAD (' ', 2, ' '), 1, 2)
    || SUBSTR (NVL (SAC11, ' ')
    || RPAD (' ', 2, ' '), 1, 2)
    || SUBSTR (NVL (SAC12, ' ')
    || RPAD (' ', 2, ' '), 1, 2)
    || SUBSTR (NVL (SAC13, ' ')
    || RPAD (' ', 2, ' '), 1, 2)
    || SUBSTR (NVL (SAC14, ' ')
    || RPAD (' ', 2, ' '), 1, 2)
    || SUBSTR (NVL (SAC15, ' ')
    || RPAD (' ', 2, ' '), 1, 2)
    || SUBSTR (NVL (JNLFG, ' ')
    || RPAD (' ', 1, ' '), 1, 1)
    || SUBSTR (NVL (JOBID, ' ')
    || RPAD (' ', 8, ' '), 1, 8)
    || SUBSTR (NVL (BTPFX, ' ')
    || RPAD (' ', 2, ' '), 1, 2)
    || SUBSTR (NVL (BTCOY, ' ')
    || RPAD (' ', 1, ' '), 1, 1)
    || SUBSTR (NVL (BTBRN, ' ')
    || RPAD (' ', 2, ' '), 1, 2)
    || SUBSTR (NVL (TO_CHAR (BTACY), ' ')
    || LPAD (' ', 5, ' '), 1, 5)
    || SUBSTR (NVL (TO_CHAR (BTACM), ' ')
    || LPAD (' ', 3, ' '), 1, 3)
    || SUBSTR (NVL (BTTCD, ' ')
    || RPAD (' ', 4, ' '), 1, 4)
    || SUBSTR (NVL (BTBCH, ' ')
    || RPAD (' ', 5, ' '), 1, 5)
    || SUBSTR (NVL (DUEDT, ' ')
    || RPAD (' ', 6, ' '), 1, 6)
    || SUBSTR (NVL (AGECD, ' ')
    || RPAD (' ', 1, ' '), 1, 1)
    || SUBSTR (NVL (RSKCL, ' ')
    || RPAD (' ', 3, ' '), 1, 3)
    || SUBSTR (NVL (TO_CHAR (RSKNR), ' ')
    || LPAD (' ', 5, ' '), 1, 5)
    || SUBSTR (NVL (BRKRF, ' ')
    || RPAD (' ', 8, ' '), 1, 8)
    || SUBSTR (NVL (OTPFX, ' ')
    || RPAD (' ', 2, ' '), 1, 2)
    || SUBSTR (NVL (OTCOY, ' ')
    || RPAD (' ', 1, ' '), 1, 1)
    || SUBSTR (NVL (OTNUM, ' ')
    || RPAD (' ', 14, ' '), 1, 14)
    || SUBSTR (NVL (OTCCY, ' ')
    || RPAD (' ', 3, ' '), 1, 3)
    || SUBSTR (NVL (TO_CHAR (AMT01), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (AMT02), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (AMT03), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (AMT04), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (AMT05), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (AMT06), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (AMT07), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (AMT08), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (AMT09), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (AMT10), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (AMT11), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (AMT12), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (AMT13), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (AMT14), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (AMT15), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (LCE01), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (LCE02), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (LCE03), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (LCE04), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (LCE05), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (LCE06), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (LCE07), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (LCE08), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (LCE09), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (LCE10), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (LCE11), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (LCE12), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (LCE13), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (LCE14), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (LCE15), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (GPAMT), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (CMAMT), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (NTAMT), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (GRSLCE), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (CMLCE), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (TO_CHAR (NTLCE), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (MYFLG, ' ')
    || RPAD (' ', 1, ' '), 1, 1)
    || SUBSTR (NVL (TO_CHAR (MYUQK), ' ')
    || LPAD (' ', 19, ' '), 1, 19)
    || SUBSTR (NVL (ORCCY, ' ')
    || RPAD (' ', 3, ' '), 1, 3)
    || SUBSTR (NVL (LCCCY, ' ')
    || RPAD (' ', 3, ' '), 1, 3)
    || SUBSTR (NVL (TO_CHAR (CRATE), ' ')
    || LPAD (' ', 20, ' '), 1, 20)
    || SUBSTR (NVL (STCA, ' ')
    || RPAD (' ', 3, ' '), 1, 3)
    || SUBSTR (NVL (STCB, ' ')
    || RPAD (' ', 3, ' '), 1, 3)
    || SUBSTR (NVL (STCC, ' ')
    || RPAD (' ', 3, ' '), 1, 3)
    || SUBSTR (NVL (STCD, ' ')
    || RPAD (' ', 3, ' '), 1, 3)
    || SUBSTR (NVL (STCE, ' ')
    || RPAD (' ', 3, ' '), 1, 3)
    || SUBSTR (NVL (TO_CHAR (MARYACTYR), ' ')
    || LPAD (' ', 5, ' '), 1, 5)
    || SUBSTR (NVL (TO_CHAR (MARYACTMN), ' ')
    || LPAD (' ', 3, ' '), 1, 3)
    || SUBSTR (NVL (RITYPE, ' ')
    || RPAD (' ', 2, ' '), 1, 2)
    || SUBSTR (NVL (USRPRF, ' ')
    || RPAD (' ', 10, ' '), 1, 10)
    || SUBSTR (NVL (JOBNM, ' ')
    || RPAD (' ', 10, ' '), 1, 10)
    || NVL (TO_CHAR (DATIME, 'YYYY-MM-DD-HH24.MI.SS.ff6'), '                          ')
    || SUBSTR (NVL (TO_CHAR (TRANNO), ' ')
    || LPAD (' ', 6, ' '), 1, 6) 
    || SUBSTR (NVL (ZCOMMSTAT, ' ')
    || RPAD (' ', 1, ' '), 1, 1)
    || SUBSTR (NVL (ZCOMMREF, ' ')
    || RPAD (' ', 9, ' '), 1, 9)    
	|| SUBSTR (NVL (OTHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2) 
    || SUBSTR (NVL (OTHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
     || SUBSTR (NVL (OTHNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)

AS "NONKEYS"
  FROM STMTPF
 ;
 
 
 
  CREATE OR REPLACE FORCE VIEW "VM1DTA"."SUSQ_UCSHREC" ("UNIQUE_NUMBER", "ACPFX", "ACCOY", "ACNUM", "UNIQK", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
          ACPFX,
          ACCOY,
          ACNUM,
          UNIQK,
          'UCSHREC' AS RECORDFORMAT,
          SUBSTR (NVL (TO_CHAR (TRNID), ' ') || LPAD (' ', 19, ' '), 1, 19)
          || SUBSTR (NVL (VFLAG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ACCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (PLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (PLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (PLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CHNUM, ' ') || RPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (UNIQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (RECNC, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (DCODE, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SDCRA, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SDCRB, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (SDCRC, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (SDCRD, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (KEYSP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CNTYP, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (TRNDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (EFFDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (ENDTP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CREQT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ORDTF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (TRCDE, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SACCT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC01, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC02, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC03, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC04, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC05, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC06, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC07, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC08, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC09, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC10, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC11, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC12, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC13, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC14, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC15, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (JNLFG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (JOBID, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (BTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (BTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (BTBRN, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (TO_CHAR (BTACY), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (TO_CHAR (BTACM), ' ') || LPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (BTTCD, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (BTBCH, ' ') || RPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (DUEDT, ' ') || RPAD (' ', 6, ' '), 1, 6)
          || SUBSTR (NVL (AGECD, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (RSKCL, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (RSKNR), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (BRKRF, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (OTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (OTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
          || SUBSTR (NVL (OTCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (ORCSH), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (OCLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CSHMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CSLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CSHDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (RTOLN), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (RTLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (MYUQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (ORCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (LCCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (CRATE), ' ') || LPAD (' ', 20, ' '),
                     1,
                     20)
          || SUBSTR (NVL (STCA, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCB, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCC, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCD, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCE, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (MARYACTYR), ' ') || LPAD (' ', 5, ' '),
                     1,
                     5)
          || SUBSTR (NVL (TO_CHAR (MARYACTMN), ' ') || LPAD (' ', 3, ' '),
                     1,
                     3)
          || SUBSTR (NVL (USRPRF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (JOBNM, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || NVL (TO_CHAR (DATIME, 'YYYY-MM-DD-HH24.MI.SS.ff6'),
                  '                          ')
          || SUBSTR (NVL (TRMID, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (MYFLG, ' ') || RPAD (' ', 1, ' '), 1, 1)
		  || SUBSTR (NVL (OTHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2) 
		  || SUBSTR (NVL (OTHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
		  || SUBSTR (NVL (OTHNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
             AS "NONKEYS"
     FROM UCSHPF;
	 
	
  CREATE OR REPLACE FORCE VIEW "VM1DTA"."STMTCLM" ("UNIQUE_NUMBER", "ACCOY", "ACNUM", "PLPFX", "PLCOY", "PLNUM", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
            ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM,
            RECORDFORMAT,
            SUBSTR (NONKEYS || RPAD (' ', 1169, ' '), 1, 1169) AS "NONKEYS"
       FROM (SELECT * FROM STMTCLM_STMTREC
             UNION ALL
             SELECT * FROM STMTCLM_UCSHREC) A
   ORDER BY ACCOY,
            ACNUM,
            PLPFX,
            PLCOY,
            PLNUM ;
 
 
	 
	 
  CREATE OR REPLACE FORCE VIEW "VM1DTA"."STMTCLM_STMTREC" ("UNIQUE_NUMBER", "ACCOY", "ACNUM", "PLPFX", "PLCOY", "PLNUM", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
          ACCOY,
          ACNUM,
          PLPFX,
          PLCOY,
          PLNUM,
          'STMTREC' AS RECORDFORMAT,
          SUBSTR (NVL (TO_CHAR (TRNID), ' ') || LPAD (' ', 19, ' '), 1, 19)
          || SUBSTR (NVL (VFLAG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ACCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (PLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (PLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (PLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CHNUM, ' ') || RPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (UNIQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (RECNC, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (DCODE, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SDCRA, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SDCRB, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (SDCRC, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (SDCRD, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (KEYSP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CNTYP, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (TRNDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (EFFDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (ENDTP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CREQT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ORDTF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (TRCDE, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SACCT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC01, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC02, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC03, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC04, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC05, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC06, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC07, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC08, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC09, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC10, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC11, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC12, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC13, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC14, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC15, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (JNLFG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (JOBID, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (BTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (BTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (BTBRN, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (TO_CHAR (BTACY), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (TO_CHAR (BTACM), ' ') || LPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (BTTCD, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (BTBCH, ' ') || RPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (DUEDT, ' ') || RPAD (' ', 6, ' '), 1, 6)
          || SUBSTR (NVL (AGECD, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (RSKCL, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (RSKNR), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (BRKRF, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (OTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (OTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
          || SUBSTR (NVL (OTCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (AMT01), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT02), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT03), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT04), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT05), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT06), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT07), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT08), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT09), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT10), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT11), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT12), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT13), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT14), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (AMT15), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE01), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE02), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE03), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE04), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE05), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE06), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE07), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE08), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE09), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE10), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE11), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE12), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE13), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE14), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (LCE15), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (GPAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CMAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (GRSLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CMLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (MYFLG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (TO_CHAR (MYUQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (ORCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (LCCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (CRATE), ' ') || LPAD (' ', 20, ' '),
                     1,
                     20)
          || SUBSTR (NVL (STCA, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCB, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCC, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCD, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCE, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (MARYACTYR), ' ') || LPAD (' ', 5, ' '),
                     1,
                     5)
          || SUBSTR (NVL (TO_CHAR (MARYACTMN), ' ') || LPAD (' ', 3, ' '),
                     1,
                     3)
          || SUBSTR (NVL (RITYPE, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (TO_CHAR (TRANNO), ' ') || LPAD (' ', 6, ' '), 1, 6)
          || SUBSTR (NVL (JOBNM, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (USRPRF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || NVL (TO_CHAR (DATIME, 'YYYY-MM-DD-HH24.MI.SS.ff6'),
                  '                          ')
		|| SUBSTR (NVL (OTHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2) 
		  || SUBSTR (NVL (OTHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
		  || SUBSTR (NVL (OTHNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
             AS "NONKEYS"
     FROM STMTPF
    WHERE MYFLG = ' ' AND PLPFX = 'CL' OR MYFLG = ' ' AND PLPFX = 'CL' ;
	
	
	
	
  CREATE OR REPLACE FORCE VIEW "VM1DTA"."STMTCLM_UCSHREC" ("UNIQUE_NUMBER", "ACCOY", "ACNUM", "PLPFX", "PLCOY", "PLNUM", "RECORDFORMAT", "NONKEYS") AS 
  SELECT UNIQUE_NUMBER,
          ACCOY,
          ACNUM,
          PLPFX,
          PLCOY,
          PLNUM,
          'UCSHREC' AS RECORDFORMAT,
          SUBSTR (NVL (TO_CHAR (TRNID), ' ') || LPAD (' ', 19, ' '), 1, 19)
          || SUBSTR (NVL (TRMID, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (VFLAG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ACCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (ACNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (PLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (PLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (PLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CLPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CLCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CLNUM, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (CHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (CHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CHNUM, ' ') || RPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (UNIQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (RECNC, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (DCODE, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SDCRA, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SDCRB, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (SDCRC, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (SDCRD, ' ') || RPAD (' ', 50, ' '), 1, 50)
          || SUBSTR (NVL (KEYSP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CNTYP, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (TRNDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (EFFDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (ENDTP, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (CREQT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (ORDTF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (TRCDE, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (SACCT, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC01, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC02, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC03, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC04, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC05, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC06, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC07, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC08, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC09, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC10, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC11, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC12, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC13, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC14, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (SAC15, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (JNLFG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (BTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (BTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (BTBRN, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (TO_CHAR (BTACY), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (TO_CHAR (BTACM), ' ') || LPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (BTTCD, ' ') || RPAD (' ', 4, ' '), 1, 4)
          || SUBSTR (NVL (BTBCH, ' ') || RPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (DUEDT, ' ') || RPAD (' ', 6, ' '), 1, 6)
          || SUBSTR (NVL (AGECD, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (RSKCL, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (RSKNR), ' ') || LPAD (' ', 5, ' '), 1, 5)
          || SUBSTR (NVL (BRKRF, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (JOBID, ' ') || RPAD (' ', 8, ' '), 1, 8)
          || SUBSTR (NVL (OTPFX, ' ') || RPAD (' ', 2, ' '), 1, 2)
          || SUBSTR (NVL (OTCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (OTNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
          || SUBSTR (NVL (OTCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (ORCSH), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (OCLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTAMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (NTLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CSHMT), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CSLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (CSHDT), ' ') || LPAD (' ', 9, ' '), 1, 9)
          || SUBSTR (NVL (TO_CHAR (RTOLN), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (RTLCE), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (TO_CHAR (MYUQK), ' ') || LPAD (' ', 19, ' '),
                     1,
                     19)
          || SUBSTR (NVL (ORCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (LCCCY, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (CRATE), ' ') || LPAD (' ', 20, ' '),
                     1,
                     20)
          || SUBSTR (NVL (STCA, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCB, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCC, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCD, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (STCE, ' ') || RPAD (' ', 3, ' '), 1, 3)
          || SUBSTR (NVL (TO_CHAR (MARYACTYR), ' ') || LPAD (' ', 5, ' '),
                     1,
                     5)
          || SUBSTR (NVL (TO_CHAR (MARYACTMN), ' ') || LPAD (' ', 3, ' '),
                     1,
                     3)
          || SUBSTR (NVL (MYFLG, ' ') || RPAD (' ', 1, ' '), 1, 1)
          || SUBSTR (NVL (JOBNM, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || SUBSTR (NVL (USRPRF, ' ') || RPAD (' ', 10, ' '), 1, 10)
          || NVL (TO_CHAR (DATIME, 'YYYY-MM-DD-HH24.MI.SS.ff6'),
                  '                          ')
				  || SUBSTR (NVL (OTHPFX, ' ') || RPAD (' ', 2, ' '), 1, 2) 
		  || SUBSTR (NVL (OTHCOY, ' ') || RPAD (' ', 1, ' '), 1, 1)
		  || SUBSTR (NVL (OTHNUM, ' ') || RPAD (' ', 14, ' '), 1, 14)
             AS "NONKEYS"
     FROM UCSHPF
    WHERE MYFLG = ' ' AND PLPFX = 'CL' OR MYFLG = ' ' AND PLPFX = ' ';
 

 

CREATE OR REPLACE VIEW VM1DTA.GCHDCV(
	UNIQUE_NUMBER
		,CHDRCOY
		,CHDRNUM
		,CHDRPFX
		,CNTTYPE
		,COWNPFX
		,COWNCOY
		,COWNNUM
		,STATCODE
		,PNDATE
		,SUBSFLG
		,OCCDATE
		,HRSKIND
		,CNTCURR
		,BILLCURR
		,TAKOVRFLG
		,GPRNLTYP
		,GPRMNTHS
		,RNLNOTTO
		,SRCEBUS
		,COYSRVAC
		,MRKSRVAC
		,DESPPFX
		,DESPCOY
		,DESPNUM
		,POLSCHPFLG
		,BTDATE
		,ADJDATE
		,PTDATE
		,PTDATEAB
		,TRANLUSED
		,LMBRNO
		,LHEADNO
		,EFFDCLDT
		,SERVUNIT
		,PNTRCDE
		,PROCID
		,TRANID
		,TRANNO
		,VALIDFLAG
		,SPECIND
		,TAXFLAG
		,AGEDEF
		,TERMAGE
		,PERSONCOV
		,ENROLLTYP
		,SPLITSUBS
		,AVLISU
		,MPLPFX
		,MPLCOY
		,MPLNUM
		,USRPRF
		,JOBNM
		,DATIME
		,IGRASP
		,IEXPLAIN
		,IDATE
		,COVERNT
		,CNTISS
	) AS
SELECT UNIQUE_NUMBER
		,CHDRCOY
		,CHDRNUM
		,CHDRPFX
		,CNTTYPE
		,COWNPFX
		,COWNCOY
		,COWNNUM
		,STATCODE
		,PNDATE
		,SUBSFLG
		,OCCDATE
		,HRSKIND
		,CNTCURR
		,BILLCURR
		,TAKOVRFLG
		,GPRNLTYP
		,GPRMNTHS
		,RNLNOTTO
		,SRCEBUS
		,COYSRVAC
		,MRKSRVAC
		,DESPPFX
		,DESPCOY
		,DESPNUM
		,POLSCHPFLG
		,BTDATE
		,ADJDATE
		,PTDATE
		,PTDATEAB
		,TRANLUSED
		,LMBRNO
		,LHEADNO
		,EFFDCLDT
		,SERVUNIT
		,PNTRCDE
		,PROCID
		,TRANID
		,TRANNO
		,VALIDFLAG
		,SPECIND
		,TAXFLAG
		,AGEDEF
		,TERMAGE
		,PERSONCOV
		,ENROLLTYP
		,SPLITSUBS
		,AVLISU
		,MPLPFX
		,MPLCOY
		,MPLNUM
		,USRPRF
		,JOBNM
		,DATIME
		,IGRASP
		,IEXPLAIN
		,IDATE
		,COVERNT
		,CNTISS
       FROM VM1DTA.CHDRPF;
	   
 commit





 



 


 

 

 

 

 

 

 





