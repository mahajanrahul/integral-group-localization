--  DDL for Index BUSD 
--------------------------------------------------------

  CREATE UNIQUE INDEX "BUSD" ON "BUSDPF" ("BUSDKEY","UNIQUE_NUMBER" DESC)  
  ;
--------------------------------------------------------
--  DDL for Index CHAG
--------------------------------------------------------

  CREATE INDEX "CHAG" ON "CHDRPF" ("AGNTPFX", "AGNTCOY", "AGNTNUM", "CHDRPFX", "CHDRCOY", "CHDRNUM", "CURRFROM" DESC,"UNIQUE_NUMBER" DESC)   
  ;
--------------------------------------------------------

--  DDL for Index CHAGWEB
--------------------------------------------------------

  CREATE INDEX "CHAGWEB" ON "CHDRPF" ("AGNTPFX", "AGNTCOY", "AGNTNUM", "CHDRPFX", "CHDRCOY", "CHDRNUM", "CURRFROM","UNIQUE_NUMBER" DESC)    
  ;
--------------------------------------------------------
--  DDL for Index CHBA
--------------------------------------------------------

  CREATE INDEX "CHBA" ON "CHDRPF" ("FACTHOUS", "BANKKEY", "BANKACCKEY","UNIQUE_NUMBER")  
  ;
--------------------------------------------------------
--  DDL for Index CHBM
--------------------------------------------------------

  CREATE INDEX "CHBM" ON "CHDRPF" ("SERVUNIT", "BILLCHNL", "ACCTMETH", "CNTCURR", "FACTHOUS","UNIQUE_NUMBER")  
  ;
--------------------------------------------------------
--  DDL for Index CHBN
--------------------------------------------------------

  CREATE INDEX "CHBN" ON "CHDRPF" ("CHDRCOY", "SERVUNIT", "BILLCHNL", "FACTHOUS", "CNTCURR", "BTDATE" DESC,"UNIQUE_NUMBER" DESC)    
  ;
--------------------------------------------------------
--  DDL for Index CHDR
--------------------------------------------------------

  CREATE INDEX "CHDR" ON "CHDRPF" ("CHDRPFX", "CHDRCOY", "CHDRNUM", "VALIDFLAG", "CURRFROM" DESC,"UNIQUE_NUMBER" DESC)    
  ;
--------------------------------------------------------
--  DDL for Index CHDROWN
--------------------------------------------------------

  CREATE INDEX "CHDROWN" ON "CHDRPF" ("COWNPFX", "COWNCOY", "COWNNUM", "MANDREF", "CHDRNUM", "CCDATE" DESC, "TRANNO" DESC,"UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index CHDRPYR
--------------------------------------------------------

  CREATE INDEX "CHDRPYR" ON "CHDRPF" ("PAYRPFX", "PAYRCOY", "PAYRNUM", "MANDREF", "CHDRNUM", "CCDATE" DESC, "TRANNO" DESC,"UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GCHDOWN
--------------------------------------------------------

  CREATE INDEX "GCHDOWN" ON "CHDRPF" ("COWNCOY", "COWNNUM","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index PRCC
--------------------------------------------------------

  CREATE INDEX "PRCC" ON "CHDRPF" ("CHDRPFX", "CHDRCOY", "CHDRNUM", "STATCODE", "CURRFROM" DESC, "TRANNO" DESC,"UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index PRCT
--------------------------------------------------------

  CREATE INDEX "PRCT" ON "CHDRPF" ("CHDRPFX", "CHDRCOY", "CHDRNUM", "VALIDFLAG","UNIQUE_NUMBER")
  ;
--------------------------------------------------------
--  DDL for Index CLEX
--------------------------------------------------------

  CREATE INDEX "CLEX" ON "CLEXPF" ("CLNTPFX", "CLNTCOY", "CLNTNUM","UNIQUE_NUMBER" DESC)
  ;
--------------------------------------------------------
--  DDL for Index CLEXIND
--------------------------------------------------------

  CREATE INDEX "CLEXIND" ON "CLEXPF" ("CLNTPFX", "CLNTCOY", "ZSPECIND", "CLNTNUM","UNIQUE_NUMBER" DESC)
  ;
--------------------------------------------------------
--  DDL for Index CLEXOID
--------------------------------------------------------

  CREATE INDEX "CLEXOID" ON "CLEXPF" ("CLNTCOY", "OLDIDNO","UNIQUE_NUMBER" DESC)
  ;
--------------------------------------------------------
--  DDL for Index CLNT
--------------------------------------------------------

  CREATE INDEX "CLNT" ON "CLNTPF" ("CLNTPFX", "CLNTCOY", "CLNTNUM","UNIQUE_NUMBER" DESC)
  ;
--------------------------------------------------------
--  DDL for Index CLNTSSN
--------------------------------------------------------

  CREATE INDEX "CLNTSSN" ON "CLNTPF" ("SECUITYNO","UNIQUE_NUMBER" DESC)
  ;
--------------------------------------------------------
--  DDL for Index CLSDBNM
--------------------------------------------------------

  CREATE INDEX "CLSDBNM" ON "CLNTPF" ("CLTSEX", "CLTDOB", "SURNAME","UNIQUE_NUMBER")
  ;
--------------------------------------------------------
--  DDL for Index CLAG
--------------------------------------------------------

  CREATE INDEX "CLAG" ON "CLNTPF" ("CLNTPFX", "CLNTCOY", "SURNAME", "GIVNAME", "CLNTNUM","UNIQUE_NUMBER" DESC)
  ;
--------------------------------------------------------
--  DDL for Index DESH
--------------------------------------------------------

  CREATE INDEX "DESH" ON "DESCPF" ("DESCCOY", "LANGUAGE", "LONGDESC","UNIQUE_NUMBER")
  ;
--------------------------------------------------------
--  DDL for Index DESN
--------------------------------------------------------

  CREATE INDEX "DESN" ON "DESCPF" ("DESCCOY", "DESCTABL", "LANGUAGE", "LONGDESC","UNIQUE_NUMBER")
  ;
--------------------------------------------------------
--  DDL for Index ELOG
--------------------------------------------------------

  CREATE UNIQUE INDEX "ELOG" ON "ELOGPF" ("ERRNUM" DESC,"UNIQUE_NUMBER" DESC)
  ;
--------------------------------------------------------
--  DDL for Index FLOG
--------------------------------------------------------

  CREATE UNIQUE INDEX "FLOG" ON "ELOGPF" ("TERMINALID", "ERRNUM" DESC,"UNIQUE_NUMBER" DESC)
  ;
--------------------------------------------------------
--  DDL for Index ERON
--------------------------------------------------------

  CREATE INDEX "ERON" ON "ERORPF" ("ERORLANG", "ERORDESC","UNIQUE_NUMBER")
  ;
--------------------------------------------------------
--  DDL for Index EROR
--------------------------------------------------------

  CREATE UNIQUE INDEX "EROR" ON "ERORPF" ("ERORPFX", "ERORCOY", "ERORLANG", "ERORPROG", "EROREROR","UNIQUE_NUMBER" DESC)
  ;
--------------------------------------------------------
--  DDL for Index ERORLNG
--------------------------------------------------------

  CREATE INDEX "ERORLNG" ON "ERORPF" ("ERORPFX", "ERORCOY", "ERORPROG", "EROREROR", "ERORLANG","UNIQUE_NUMBER" DESC)
  ;
--------------------------------------------------------
--  DDL for Index GCAG
--------------------------------------------------------

  CREATE INDEX "GCAG" ON "GCHIPF" ("AGNTPFX", "AGNTCOY", "AGNTNUM", "CHDRCOY", "CHDRNUM", "EFFDATE" DESC,"UNIQUE_NUMBER") 
  ;
--------------------------------------------------------

--  DDL for Index GCHI
--------------------------------------------------------

  CREATE INDEX "GCHI" ON "GCHIPF" ("CHDRCOY", "CHDRNUM", "EFFDATE" DESC,"UNIQUE_NUMBER")
  ;
--------------------------------------------------------
--  DDL for Index GCHIASC
--------------------------------------------------------

  CREATE INDEX "GCHIASC" ON "GCHIPF" ("CHDRCOY", "CHDRNUM", "EFFDATE","UNIQUE_NUMBER")
  ;
--------------------------------------------------------
--  DDL for Index GCHIPYR
--------------------------------------------------------

  CREATE INDEX "GCHIPYR" ON "GCHIPF" ("PAYRPFX", "PAYRCOY", "PAYRNUM", "MANDREF", "CHDRNUM", "CCDATE" DESC,"UNIQUE_NUMBER")
  ;
--------------------------------------------------------
--  DDL for Index GCHIRNL
--------------------------------------------------------

  CREATE INDEX "GCHIRNL" ON "GCHIPF" ("CHDRCOY", "CRDATE" DESC, "CNTBRANCH", "CHDRNUM","UNIQUE_NUMBER")
  ;
--------------------------------------------------------
--  DDL for Index GCHITRN
--------------------------------------------------------

  CREATE INDEX "GCHITRN" ON "GCHIPF" ("CHDRCOY", "CHDRNUM", "TRANNO","UNIQUE_NUMBER")
  ;
--------------------------------------------------------
--  DDL for Index GMHD
--------------------------------------------------------

  CREATE UNIQUE INDEX "GMHD" ON "GMHDPF" ("CHDRCOY", "CHDRNUM", "MBRNO", "DPNTNO","UNIQUE_NUMBER" DESC)
  ;
--------------------------------------------------------
--  DDL for Index GMHDCLI
--------------------------------------------------------

  CREATE INDEX "GMHDCLI" ON "GMHDPF" ("CLNTPFX", "FSUCO", "CLNTNUM", "HEADCNT", "CHDRCOY", "CHDRNUM","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GMHDCLN
--------------------------------------------------------

  CREATE INDEX "GMHDCLN" ON "GMHDPF" ("CHDRCOY", "CHDRNUM", "CLNTPFX", "FSUCO", "CLNTNUM", "HEADCNT","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GMHDCLT
--------------------------------------------------------

  CREATE INDEX "GMHDCLT" ON "GMHDPF" ("CHDRCOY", "CHDRNUM", "CLNTPFX", "FSUCO", "CLNTNUM", "HEADCNT", "DTEATT", "MBRNO", "DPNTNO","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GMHDEMP
--------------------------------------------------------

  CREATE INDEX "GMHDEMP" ON "GMHDPF" ("CHDRCOY", "CHDRNUM", "EMPNO", "DPNTNO","UNIQUE_NUMBER" DESC)
  ;
--------------------------------------------------------
--  DDL for Index GMHDENO
--------------------------------------------------------

  CREATE INDEX "GMHDENO" ON "GMHDPF" ("CHDRCOY", "EMPNO", "CHDRNUM", "MBRNO", "DPNTNO","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------

--  DDL for Index ITDM
--------------------------------------------------------

  CREATE INDEX "ITDM" ON "ITEMPF" ("ITEMCOY", "ITEMTABL", "ITEMITEM", "ITMFRM" DESC,"UNIQUE_NUMBER")
  ;
--------------------------------------------------------
--  DDL for Index ITDMPRM
--------------------------------------------------------

  CREATE INDEX "ITDMPRM" ON "ITEMPF" ("ITEMPFX", "ITEMCOY", "ITEMTABL", "ITEMITEM", "ITMFRM" DESC,"UNIQUE_NUMBER")
  ;
--------------------------------------------------------
--  DDL for Index ITEM
--------------------------------------------------------

  CREATE INDEX "ITEM" ON "ITEMPF" ("ITEMPFX", "ITEMCOY", "ITEMTABL", "ITEMITEM", "ITEMSEQ","UNIQUE_NUMBER" DESC)
  ;
--------------------------------------------------------
--  DDL for Index SLCK
--------------------------------------------------------

  CREATE UNIQUE INDEX "SLCK" ON "SLCKPF" ("COMPANY", "ENTTYP", "ENTITY","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index SLJB
--------------------------------------------------------

  CREATE INDEX "SLJB" ON "SLCKPF" ("SYSJOB", "SYSUSER", "SYSNBR","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index USBR
--------------------------------------------------------

  CREATE UNIQUE INDEX "USBR" ON "USBRPF" ("USERID", "COMPANY", "BRANCH","UNIQUE_NUMBER" DESC)  
  ;
--------------------------------------------------------
--  DDL for Index USCY
--------------------------------------------------------

  CREATE UNIQUE INDEX "USCY" ON "USCYPF" ("USERID", "COMPANY","UNIQUE_NUMBER" DESC)   
  ;
--------------------------------------------------------
--  DDL for Index USRX
--------------------------------------------------------

  CREATE UNIQUE INDEX "USRX" ON "USRDPF" ("USERNUM","UNIQUE_NUMBER" DESC)    
  ;
--------------------------------------------------------
--  DDL for Index UTRC
--------------------------------------------------------

  CREATE UNIQUE INDEX "UTRC" ON "UTRCPF" ("USERID", "COMPANY", "TRANSCD","UNIQUE_NUMBER" DESC)    
  ;
--------------------------------------------------------
--  DDL for Index GMHI
--------------------------------------------------------

  CREATE UNIQUE INDEX "GMHI" ON "GMHIPF" ("CHDRCOY", "CHDRNUM", "MBRNO", "DPNTNO", "EFFDATE" DESC,"UNIQUE_NUMBER" DESC)    
  ;
--------------------------------------------------------
--  DDL for Index GMHIEFF
--------------------------------------------------------

  CREATE INDEX "GMHIEFF" ON "GMHIPF" ("CHDRCOY", "CHDRNUM", "EFFDATE" DESC,"UNIQUE_NUMBER" DESC)    
  ;
--------------------------------------------------------
--  DDL for Index GMHISUB
--------------------------------------------------------

  CREATE UNIQUE INDEX "GMHISUB" ON "GMHIPF" ("CHDRCOY", "CHDRNUM", "SUBSCOY", "SUBSNUM", "MBRNO", "DPNTNO", "EFFDATE" DESC,"UNIQUE_NUMBER" DESC)     
  ;
--------------------------------------------------------
--  DDL for Index GMHISUM
--------------------------------------------------------

  CREATE INDEX "GMHISUM" ON "GMHIPF" ("CHDRCOY", "CHDRNUM", "SUBSCOY", "SUBSNUM", "MBRNO", "DPNTNO","UNIQUE_NUMBER")    
  ;
--------------------------------------------------------
--  DDL for Index BPRD
--------------------------------------------------------

  CREATE UNIQUE INDEX "BPRD" ON "BPRDPF" ("COMPANY", "BPROCESNAM","UNIQUE_NUMBER")    
  ;
--------------------------------------------------------
--  DDL for Index BSPR
--------------------------------------------------------

  CREATE UNIQUE INDEX "BSPR" ON "BSPRPF" ("BSCHEDNAM", "BSCHEDNUM", "COMPANY", "BPROCESNAM", "BPRCOCCNO","UNIQUE_NUMBER" DESC)     
  ;
--------------------------------------------------------
--  DDL for Index BSPRPTY
--------------------------------------------------------

  CREATE INDEX "BSPRPTY" ON "BSPRPF" ("BSCHEDNAM", "BSCHEDNUM", "BSCHEDPRTY","UNIQUE_NUMBER")    
  ;
--------------------------------------------------------
--  DDL for Index BSPRRTS
--------------------------------------------------------

  CREATE INDEX "BSPRRTS" ON "BSPRPF" ("BSCHEDNAM", "BSCHEDNUM", "BPDATMSTRT","UNIQUE_NUMBER")    
  ;
--------------------------------------------------------
--  DDL for Index BSPRWRK
--------------------------------------------------------

  CREATE INDEX "BSPRWRK" ON "BSPRPF" ("BSCHEDNAM", "BSCHEDNUM", "BPRCSTATUS", "BPROCESNAM","UNIQUE_NUMBER" DESC)     
  ;
--------------------------------------------------------
--  DDL for Index GBID
--------------------------------------------------------

  CREATE INDEX "GBID" ON "GBIDPF" ("CHDRCOY", "BILLNO", "PRODTYP", "PLANNO", "CLASSINS","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GBIDPLN
--------------------------------------------------------

  CREATE INDEX "GBIDPLN" ON "GBIDPF" ("CHDRCOY", "BILLNO", "PLANNO", "PRODTYP","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GCBN
--------------------------------------------------------

  CREATE INDEX "GCBN" ON "GCBNPF" ("CHDRCOY", "CLAMNUM", "GCOCCNO", "GCBENSEQ","UNIQUE_NUMBER" DESC) 
  ;

--------------------------------------------------------

--  DDL for Index BSSC
--------------------------------------------------------

  CREATE UNIQUE INDEX "BSSC" ON "BSSCPF" ("BSCHEDNAM", "BSCHEDNUM","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index BSSCUSR
--------------------------------------------------------

  CREATE INDEX "BSSCUSR" ON "BSSCPF" ("BSUSERNAME", "BSHDSTATUS", "BPRCEFFDAT" DESC,"UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index BSSCWRK
--------------------------------------------------------

  CREATE INDEX "BSSCWRK" ON "BSSCPF" ("BSHDSTATUS", "BPRCEFFDAT" DESC,"UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------

--  DDL for Index GAPH
--------------------------------------------------------

  CREATE INDEX "GAPH" ON "GAPHPF" ("CHDRCOY", "CHDRNUM", "HEADCNTIND", "MBRNO", "DPNTNO", "PRODTYP", "EFFDATE", "TRANNO" DESC,"UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GAPHBIL
--------------------------------------------------------

  CREATE INDEX "GAPHBIL" ON "GAPHPF" ("CHDRCOY", "CHDRNUM", "HEADCNTIND", "MBRNO", "DPNTNO", "PRODTYP", "EFFDATE" DESC, "TRANNO" DESC,"UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GAPHEFF
--------------------------------------------------------

  CREATE INDEX "GAPHEFF" ON "GAPHPF" ("CHDRCOY", "CHDRNUM", "HEADCNTIND", "MBRNO", "DPNTNO", "PRODTYP", "EFFDATE","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GAPHMEF
--------------------------------------------------------

  CREATE INDEX "GAPHMEF" ON "GAPHPF" ("CHDRCOY", "CHDRNUM", "MBRNO", "DPNTNO", "EFFDATE" DESC,"UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GAPHMPP
--------------------------------------------------------

  CREATE INDEX "GAPHMPP" ON "GAPHPF" ("CHDRCOY", "CHDRNUM", "EFFDATE" DESC, "HEADCNTIND", "MBRNO", "DPNTNO","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index BNFL
--------------------------------------------------------

  CREATE INDEX "BNFL" ON "BNFLPF" ("CHDRCOY", "CHDRNUM", "PRODTYP", "PLANNO", "BENCDE", "NETLEVEL", "CLAMTYPE", "EFFDATE" DESC,"UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index BNFLBNG
--------------------------------------------------------

  CREATE INDEX "BNFLBNG" ON "BNFLPF" ("CHDRCOY", "CHDRNUM", "PRODTYP", "PLANNO", "BENCDE", "BNFTGRP", "NETLEVEL", "CLAMTYPE", "EFFDATE" DESC,"UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index BNFLEFF
--------------------------------------------------------

  CREATE INDEX "BNFLEFF" ON "BNFLPF" ("CHDRCOY", "CHDRNUM", "EFFDATE" DESC,"UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index BNFLGR2
--------------------------------------------------------

  CREATE INDEX "BNFLGR2" ON "BNFLPF" ("CHDRCOY", "CHDRNUM", "PRODTYP", "PLANNO", "NETLEVEL", "CLAMTYPE", "BNFTGRP", "EFFDATE","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index BNFLGRP
--------------------------------------------------------

  CREATE INDEX "BNFLGRP" ON "BNFLPF" ("CHDRCOY", "CHDRNUM", "PRODTYP", "PLANNO", "NETLEVEL", "CLAMTYPE", "BNFTGRP", "EFFDATE" DESC,"UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index BNFLPL2
--------------------------------------------------------

  CREATE INDEX "BNFLPL2" ON "BNFLPF" ("CHDRCOY", "CHDRNUM", "PRODTYP", "PLANNO", "NETLEVEL", "EFFDATE" DESC,"UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index BNFLPL3
--------------------------------------------------------

  CREATE INDEX "BNFLPL3" ON "BNFLPF" ("CHDRCOY", "CHDRNUM", "PRODTYP", "PLANNO", "EFFDATE" DESC,"UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index BNFLPLN
--------------------------------------------------------

  CREATE INDEX "BNFLPLN" ON "BNFLPF" ("CHDRCOY", "CHDRNUM", "PRODTYP", "PLANNO", "NETLEVEL", "CLAMTYPE", "EFFDATE" DESC,"UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index BNFLWBF
--------------------------------------------------------

  CREATE INDEX "BNFLWBF" ON "BNFLPF" ("CHDRCOY", "CHDRNUM", "PRODTYP", "PLANNO", "BENCDE", "NETLEVEL","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index ANUM
--------------------------------------------------------

  CREATE UNIQUE INDEX "ANUM" ON "ANUMPF" ("PREFIX", "COMPANY", "GENKEY", "AUTONUM","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index GCHP
--------------------------------------------------------

  CREATE UNIQUE INDEX "GCHP" ON "GCHPPF" ("CHDRCOY", "CHDRNUM","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index RDOC
--------------------------------------------------------

  CREATE INDEX "RDOC" ON "RTRNPF" ("RDOCPFX", "RDOCCOY", "RDOCNUM", "TRANSEQ","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index RGNL
--------------------------------------------------------

  CREATE INDEX "RGNL" ON "RTRNPF" ("BATCPFX", "BATCCOY", "BATCBRN", "BATCACTYR", "BATCACTMN", "BATCTRCDE", "BATCBATCH", "GENLPFX", "GENLCOY", "GENLCUR", "GLCODE","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index RLDG
--------------------------------------------------------

  CREATE INDEX "RLDG" ON "RTRNPF" ("RLDGPFX", "RLDGCOY", "RLDGACCT","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index RLDGDOC
--------------------------------------------------------

  CREATE INDEX "RLDGDOC" ON "RTRNPF" ("RLDGPFX", "RLDGCOY", "RLDGACCT", "TRANNO", "RDOCPFX", "RDOCCOY", "RDOCNUM","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index RTRN
--------------------------------------------------------

  CREATE INDEX "RTRN" ON "RTRNPF" ("BATCPFX", "BATCCOY", "BATCBRN", "BATCACTYR", "BATCACTMN", "BATCTRCDE", "BATCBATCH","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index RTRNACC
--------------------------------------------------------

  CREATE INDEX "RTRNACC" ON "RTRNPF" ("ACCPFX", "ACCCOY", "ACCNO", "ORIGCCY","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index RTRNDTE
--------------------------------------------------------

  CREATE INDEX "RTRNDTE" ON "RTRNPF" ("RDOCCOY", "TRANDATE" DESC,"UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index RTRNGRI
--------------------------------------------------------

  CREATE INDEX "RTRNGRI" ON "RTRNPF" ("BATCPFX", "BATCCOY", "BATCACTYR", "BATCACTMN", "TRANSEQ", "SACSCODE", "SACSTYP", "BATCBRN","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index RTRNGRP
--------------------------------------------------------

  CREATE INDEX "RTRNGRP" ON "RTRNPF" ("BATCPFX", "BATCCOY", "BATCACTYR", "BATCACTMN", "BATCBRN", "BATCTRCDE", "BATCBATCH","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index RTRNSRE
--------------------------------------------------------

  CREATE INDEX "RTRNSRE" ON "RTRNPF" ("BATCPFX", "BATCCOY", "BATCBRN", "BATCACTYR", "BATCACTMN", "BATCTRCDE", "BATCBATCH", "RDOCNUM","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index AGNT
--------------------------------------------------------

  CREATE INDEX "AGNT" ON "AGNTPF" ("AGNTPFX", "AGNTCOY", "AGNTNUM","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index AGRP
--------------------------------------------------------

  CREATE INDEX "AGRP" ON "AGNTPF" ("REPORTAG01", "REPORTAG02", "REPORTAG03", "REPORTAG04", "REPORTAG05", "REPORTAG06","UNIQUE_NUMBER" DESC)  
  ;
--------------------------------------------------------

--  DDL for Index BABR
--------------------------------------------------------

  CREATE UNIQUE INDEX "BABR" ON "BABRPF" ("BANKKEY","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index BABRBBM
--------------------------------------------------------

  CREATE INDEX "BABRBBM" ON "BABRPF" ("BABRDC","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index BKGU
--------------------------------------------------------

  CREATE UNIQUE INDEX "BKGU" ON "BKGUPF" ("CHDRCOY", "BANKGNO" DESC,"UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index BKGUAGT
--------------------------------------------------------

  CREATE INDEX "BKGUAGT" ON "BKGUPF" ("AGNTPFX", "AGNTCOY", "AGNTNUM","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index BKGUCLT
--------------------------------------------------------

  CREATE INDEX "BKGUCLT" ON "BKGUPF" ("CLNTPFX", "CLNTCOY", "CLNTNUM","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index BRUP
--------------------------------------------------------

  CREATE INDEX "BRUP" ON "BRUPPF" ("CLNTCOY", "CLNTNUM", "BRUPDTE","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index BRUPDTE
--------------------------------------------------------

  CREATE INDEX "BRUPDTE" ON "BRUPPF" ("CLNTCOY", "CLNTNUM", "BRUPDTE" DESC,"UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index CHEQ
--------------------------------------------------------

  CREATE UNIQUE INDEX "CHEQ" ON "CHEQPF" ("REQNCOY", "REQNNO","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index CHEQALL
--------------------------------------------------------

  CREATE INDEX "CHEQALL" ON "CHEQPF" ("REQNCOY", "BRANCH", "CAPNAME","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index CHEQBNK
--------------------------------------------------------

  CREATE UNIQUE INDEX "CHEQBNK" ON "CHEQPF" ("REQNCOY", "BRANCH", "REQNBCDE", "CAPNAME", "REQNNO","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index CHEQBRN
--------------------------------------------------------

  CREATE INDEX "CHEQBRN" ON "CHEQPF" ("REQNCOY", "REQNBCDE", "BRANCH", "CAPNAME","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index CHEQCHQ
--------------------------------------------------------

  CREATE INDEX "CHEQCHQ" ON "CHEQPF" ("REQNCOY", "CHEQNO","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index CHEQOST
--------------------------------------------------------

  CREATE INDEX "CHEQOST" ON "CHEQPF" ("REQNCOY", "PROCIND", "REQNNO","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index CHEQPRD
--------------------------------------------------------

  CREATE INDEX "CHEQPRD" ON "CHEQPF" ("REQNCOY", "REQNBCDE", "CAPNAME","UNIQUE_NUMBER" ) 
  ;
--------------------------------------------------------
--  DDL for Index CHEQRQN
--------------------------------------------------------

  CREATE INDEX "CHEQRQN" ON "CHEQPF" ("REQNCOY", "REQNBCDE", "BRANCH", "REQNNO", "CAPNAME","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index CHEQUPC
--------------------------------------------------------

  CREATE INDEX "CHEQUPC" ON "CHEQPF" ("REQNCOY", "REQNBCDE", "PAYDATE" DESC,"UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index CQBA
--------------------------------------------------------

  CREATE INDEX "CQBA" ON "CHEQPF" ("REQNCOY", "BRANCH", "REQNBCDE", "CAPNAME","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index HBCDCQN
--------------------------------------------------------

  CREATE INDEX "HBCDCQN" ON "CHEQPF" ("REQNCOY", "REQNBCDE", "CHEQNO","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index CLRF
--------------------------------------------------------

  CREATE INDEX "CLRF" ON "CLRRPF" ("FOREPFX", "FORECOY", "FORENUM", "CLRRROLE","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index CLRR
--------------------------------------------------------

  CREATE UNIQUE INDEX "CLRR" ON "CLRRPF" ("CLNTPFX", "CLNTCOY", "CLNTNUM", "CLRRROLE", "FOREPFX", "FORECOY", "FORENUM","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index CLRRFCC
--------------------------------------------------------

  CREATE INDEX "CLRRFCC" ON "CLRRPF" ("FORECOY", "CLNTNUM", "CLRRROLE","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index CLRRFOR
--------------------------------------------------------

  CREATE INDEX "CLRRFOR" ON "CLRRPF" ("FORECOY", "FORENUM", "CLRRROLE","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index CLRRROL
--------------------------------------------------------

  CREATE INDEX "CLRRROL" ON "CLRRPF" ("CLRRROLE", "FOREPFX", "FORECOY", "FORENUM","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Index GCLMADV
--------------------------------------------------------

  CREATE UNIQUE INDEX "GCLMADV" ON "GCMHPF" ("CHDRCOY", "DTEPYMTADV", "CLAMNUM", "GCOCCNO", "GCCLMSEQ","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index GCLMCLT
--------------------------------------------------------

  CREATE INDEX "GCLMCLT" ON "GCMHPF" ("CLNTCOY", "CLNTNUM", "CHDRCOY", "CHDRNUM", "CLAMNUM", "GCOCCNO", "GCCLMSEQ","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GCLMDUP
--------------------------------------------------------

  CREATE INDEX "GCLMDUP" ON "GCMHPF" ("CHDRCOY", "CHDRNUM", "GCCLMTNO", "GCDPNTNO", "DTECLAM", "GCOPDGCD","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GCLMMBR
--------------------------------------------------------

  CREATE INDEX "GCLMMBR" ON "GCMHPF" ("CHDRCOY", "CLNTNUM", "DTECLAM" DESC,"UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GCLMOTH
--------------------------------------------------------

  CREATE UNIQUE INDEX "GCLMOTH" ON "GCMHPF" ("CHDRCOY", "DTEPYMTOTH", "CLAMNUM", "GCOCCNO", "GCCLMSEQ","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index GCLMOUT
--------------------------------------------------------

  CREATE INDEX "GCLMOUT" ON "GCMHPF" ("CHDRCOY", "CLAMNUM", "GCOCCNO", "GCCLMTNO", "GCDPNTNO", "DTECLAM", "GCOPBNCD","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GCLMPND
--------------------------------------------------------

  CREATE INDEX "GCLMPND" ON "GCMHPF" ("CLNTCOY", "CLNTNUM", "CHDRCOY", "CHDRNUM", "PRODTYP","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GCLMPOL
--------------------------------------------------------

  CREATE INDEX "GCLMPOL" ON "GCMHPF" ("CHDRCOY", "CHDRNUM", "CLAMNUM", "GCOCCNO", "GCCLMSEQ","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GCLMPRD
--------------------------------------------------------

  CREATE INDEX "GCLMPRD" ON "GCMHPF" ("CHDRCOY", "PRODTYP", "CLAIMCUR", "CLAMNUM", "GCOCCNO","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GCLMVAL
--------------------------------------------------------

  CREATE INDEX "GCLMVAL" ON "GCMHPF" ("CHDRCOY", "CHDRNUM", "GCCLMTNO", "GCDPNTNO", "DTECLAM", "GCOPBNCD","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GCMH
--------------------------------------------------------

  CREATE UNIQUE INDEX "GCMH" ON "GCMHPF" ("CHDRCOY", "CLAMNUM", "GCOCCNO", "GCCLMSEQ","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index GCMHINT
--------------------------------------------------------

  CREATE INDEX "GCMHINT" ON "GCMHPF" ("CHDRCOY", "CHDRNUM", "GCCLMTNO", "GCDPNTNO", "PRODTYP","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GCMHPOC
--------------------------------------------------------

  CREATE INDEX "GCMHPOC" ON "GCMHPF" ("CHDRCOY", "CHDRNUM", "CLNTCOY", "CLNTNUM","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GPHD
--------------------------------------------------------

  CREATE UNIQUE INDEX "GPHD" ON "GPHDPF" ("CHDRCOY", "CHDRNUM", "PRODTYP","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index GIDT
--------------------------------------------------------

  CREATE UNIQUE INDEX "GIDT" ON "GIDTPF" ("BATCTRCDE", "ISSDATE", "CHDRCOY", "CHDRNUM", "TRANNO","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index GIDTASC
--------------------------------------------------------

  CREATE UNIQUE INDEX "GIDTASC" ON "GIDTPF" ("CHDRCOY", "CHDRNUM", "TRANNO","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index GIDTBIL
--------------------------------------------------------

  CREATE INDEX "GIDTBIL" ON "GIDTPF" ("FRDOCNO","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GIDTCDE
--------------------------------------------------------

  CREATE INDEX "GIDTCDE" ON "GIDTPF" ("CHDRCOY", "CHDRNUM", "BATCTRCDE","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GIDTDTE
--------------------------------------------------------

  CREATE INDEX "GIDTDTE" ON "GIDTPF" ("ISSDATE", "BATCTRCDE", "CHDRCOY", "CHDRNUM", "TRANNO","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GIDTPND
--------------------------------------------------------

  CREATE INDEX "GIDTPND" ON "GIDTPF" ("CHDRCOY", "CHDRNUM","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GIDTPOL
--------------------------------------------------------

  CREATE INDEX "GIDTPOL" ON "GIDTPF" ("CHDRCOY", "CHDRNUM", "TRANNO" DESC,"UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GLHD
--------------------------------------------------------

  CREATE UNIQUE INDEX "GLHD" ON "GLHDPF" ("CHDRCOY", "CHDRNUM", "PRODTYP", "PLANNO","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index GLHDMFT
--------------------------------------------------------

  CREATE INDEX "GLHDMFT" ON "GLHDPF" ("CHDRCOY", "CHDRNUM", "PRODTYP", "MFPLANNO","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GLHDNET
--------------------------------------------------------

  CREATE INDEX "GLHDNET" ON "GLHDPF" ("CHDRCOY", "PROVNET","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index GLHDPLN
--------------------------------------------------------

  CREATE INDEX "GLHDPLN" ON "GLHDPF" ("CHDRCOY", "CHDRNUM", "PLANNO","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GLHDPPP
--------------------------------------------------------

  CREATE UNIQUE INDEX "GLHDPPP" ON "GLHDPF" ("CHDRCOY", "CHDRNUM", "PRODTYP", "PREMMTHD", "PLANNO","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index GLHDPRM
--------------------------------------------------------

  CREATE INDEX "GLHDPRM" ON "GLHDPF" ("CHDRCOY", "CHDRNUM", "PREMMTHD","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GXHI
--------------------------------------------------------

  CREATE UNIQUE INDEX "GXHI" ON "GXHIPF" ("CHDRCOY", "CHDRNUM", "MBRNO", "DPNTNO", "PRODTYP", "PLANNO", "EFFDATE" DESC,"UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index GXHIDTE
--------------------------------------------------------

  CREATE UNIQUE INDEX "GXHIDTE" ON "GXHIPF" ("CHDRCOY", "CHDRNUM", "MBRNO", "DPNTNO", "EFFDATE", "PRODTYP", "PLANNO","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index GXHIHDC
--------------------------------------------------------

  CREATE INDEX "GXHIHDC" ON "GXHIPF" ("CHDRCOY", "CHDRNUM", "HEADNO", "MBRNO", "DPNTNO", "PRODTYP", "PLANNO", "EFFDATE","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GXHIMBR
--------------------------------------------------------

  CREATE INDEX "GXHIMBR" ON "GXHIPF" ("CHDRCOY", "CHDRNUM", "MBRNO", "DPNTNO", "PRODTYP", "PLANNO","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GXHIMDE
--------------------------------------------------------

  CREATE INDEX "GXHIMDE" ON "GXHIPF" ("CHDRCOY", "CHDRNUM", "MBRNO", "DPNTNO", "EFFDATE" DESC, "PRODTYP", "PLANNO","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GXHIMPD
--------------------------------------------------------

  CREATE INDEX "GXHIMPD" ON "GXHIPF" ("CHDRCOY", "CHDRNUM", "MBRNO", "DPNTNO", "PRODTYP", "EFFDATE" DESC, "PLANNO","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GXHINEW
--------------------------------------------------------

  CREATE INDEX "GXHINEW" ON "GXHIPF" ("CHDRCOY", "CHDRNUM", "MBRNO", "DPNTNO", "PRODTYP", "EFFDATE" DESC,"UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GXHIPEM
--------------------------------------------------------

  CREATE INDEX "GXHIPEM" ON "GXHIPF" ("CHDRCOY", "CHDRNUM", "PRODTYP", "PLANNO", "EFFDATE", "MBRNO", "DPNTNO","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GXHIPLN
--------------------------------------------------------

  CREATE INDEX "GXHIPLN" ON "GXHIPF" ("CHDRCOY", "CHDRNUM", "MBRNO", "DPNTNO", "PLANNO", "PRODTYP","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GXHIPMP
--------------------------------------------------------

  CREATE INDEX "GXHIPMP" ON "GXHIPF" ("CHDRCOY", "CHDRNUM", "PRODTYP", "MBRNO", "DPNTNO", "PLANNO","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GXHIPRD
--------------------------------------------------------

  CREATE INDEX "GXHIPRD" ON "GXHIPF" ("CHDRCOY", "CHDRNUM", "PRODTYP", "PLANNO", "MBRNO", "DPNTNO","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index GXHITRN
--------------------------------------------------------

  CREATE INDEX "GXHITRN" ON "GXHIPF" ("CHDRCOY", "CHDRNUM", "PLANNO", "TRANNO","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index HELP
--------------------------------------------------------

  CREATE UNIQUE INDEX "HELP" ON "HELPPF" ("HELPPFX", "HELPCOY", "HELPLANG", "HELPTYPE", "HELPPROG", "HELPITEM", "HELPSEQ","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index HLPR
--------------------------------------------------------

  CREATE UNIQUE INDEX "HLPR" ON "HELPPF" ("HELPLANG", "HELPTYPE", "HELPPROG", "HELPITEM", "HELPSEQ" DESC,"UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index MTRN
--------------------------------------------------------

  CREATE INDEX "MTRN" ON "MTRNPF" ("CHDRCOY", "CHDRNUM", "MBRNO", "DPNTNO", "TRANNO", "RLDPNTNO","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index MTRNCHE
--------------------------------------------------------

  CREATE INDEX "MTRNCHE" ON "MTRNPF" ("CHDRCOY", "CHDRNUM", "MBRNO", "DPNTNO", "CHGTYPE","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index MTRNRIE
--------------------------------------------------------

  CREATE INDEX "MTRNRIE" ON "MTRNPF" ("CHDRCOY", "CHDRNUM", "MBRNO", "DPNTNO", "TRANNO","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index MTRNTNO
--------------------------------------------------------

  CREATE INDEX "MTRNTNO" ON "MTRNPF" ("CHDRCOY", "CHDRNUM", "TRANNO","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index PCLM
--------------------------------------------------------

  CREATE INDEX "PCLM" ON "PREQPF" ("RLDGCOY", "RLDGACCT","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index PREQ
--------------------------------------------------------

  CREATE INDEX "PREQ" ON "PREQPF" ("RDOCPFX", "RDOCCOY", "RDOCNUM", "JRNSEQ","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index PREQRLG
--------------------------------------------------------

  CREATE INDEX "PREQRLG" ON "PREQPF" ("RLDGCOY", "RLDGACCT", "SACSCODE", "SACSTYP", "ORIGCCY","UNIQUE_NUMBER") 
  ;
--------------------------------------------------------
--  DDL for Index RACR
--------------------------------------------------------

  CREATE INDEX "RACR" ON "RACRPF" ("CLNTPFX", "CLNTCOY", "CLNTNUM", "LRKCLS", "CURRFROM","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index SOIN
--------------------------------------------------------

  CREATE INDEX "SOIN" ON "SOINPF" ("CLNTCOY", "CLNTNUM", "INCSEQNO","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index UFAB
--------------------------------------------------------

  CREATE UNIQUE INDEX "UFAB" ON "UFABPF" ("USERID", "COMPANY", "BANKCODE","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
--  DDL for Index USTG
--------------------------------------------------------

  CREATE UNIQUE INDEX "USTG" ON "USTGPF" ("USERID", "TABLGRP","UNIQUE_NUMBER" DESC) 
  ;
--------------------------------------------------------
