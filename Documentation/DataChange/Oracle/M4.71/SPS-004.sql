/*TABLE*/
/*ALTER TABLE*/
ALTER TABLE VM1DTA.GPHIPF 
  ADD(
DISCRATE1 NUMERIC(5,2) ,
DISCRATE2  NUMERIC(5,2) ,
DESDISC1  NCHAR(25) ,
NOINS1    INT ,
ISSTAFF1  NCHAR(1) ,
DESDISC2  NCHAR(25) ,
NOINS2	  INT ,
ISSTAFF2  NCHAR(1) ,
ABPREFLG  NCHAR(1) 
);

COMMIT;

/*ALTER VIEW GPHI*/
CREATE OR REPLACE FORCE VIEW GPHI (UNIQUE_NUMBER, CHDRCOY, CHDRNUM, PRODTYP, EFFDATE, DTETRM, CMRATE, DISCRATE, DISCAMT, SPECTRM, RFUNDFLG, RFUNDPC01, RFUNDPC02, ADEXPFLG, ARFUNDPC, COVLMT01, COVLMT02, COVLMT03, COVLMT04, COVAGE01, COVAGE02, COVAGE03, COVAGE04, TERMID, USER_T, TRDT, TRTM, TRANNO, GCLRPERD, LMTPCT, RNCMRATE, NBFDFEE, RNFDFEE, CTBFRQ, INVSRULE, RIDISCPE, MANDAYS, DMDAYDED, MINMANDAYS, BILMANDAYS, PRMAMTRT, USRPRF, JOBNM, DATIME, 
DISCRATE1, DISCRATE2, DESDISC1, NOINS1, ISSTAFF1, DESDISC2,NOINS2, ISSTAFF2,ABPREFLG) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            PRODTYP,
            EFFDATE,
            DTETRM,
            CMRATE,
            DISCRATE,
            DISCAMT,
            SPECTRM,
            RFUNDFLG,
            RFUNDPC01,
            RFUNDPC02,
            ADEXPFLG,
            ARFUNDPC,
            COVLMT01,
            COVLMT02,
            COVLMT03,
            COVLMT04,
            COVAGE01,
            COVAGE02,
            COVAGE03,
            COVAGE04,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            TRANNO,
            GCLRPERD,
            LMTPCT,
            RNCMRATE,
            NBFDFEE,
            RNFDFEE,
            CTBFRQ,
            INVSRULE,
            RIDISCPE,
            MANDAYS,
            DMDAYDED,
            MINMANDAYS,
            BILMANDAYS,
            PRMAMTRT,
            USRPRF,
            JOBNM,
            DATIME,
			DISCRATE1,
			DISCRATE2,
			DESDISC1,
			NOINS1,
			ISSTAFF1,
			DESDISC2,
			NOINS2,
			ISSTAFF2,
			ABPREFLG
      FROM VM1DTA.GPHIPF;
      
 COMMIT;     


/* DFPLPF */

ALTER TABLE VM1DTA.DFPLPF 
  ADD(
ABPREFLG NCHAR(1)
);

COMMIT;

/* DFPL */
CREATE OR REPLACE FORCE VIEW DFPL (UNIQUE_NUMBER, CHDRCOY, "TEMPLATE", PREMMTHD, REASONTRM, GSALFREQ, CARDTYPE01, CARDTYPE02, BNFTTMPLA, BNFTTMPLB, BNFTTMPLC, GATEKEEP, LMTWHOM, PROVNET, MBRTYPE, PREAUTH, DFTBFT, BNFTGRTMA, BNFTGRTMB, MFPLANNO, ELIGPERD, CLMWAIT, DEFPLAN, CREQ, HEADCNTFLG, ASCBFLG, ASCBLIMIT, ASCBLTBS, BENCDE, CLASSINS, RILDGIND, AUTHFLG, CTBRULE, RTARULE, DPNTRULE, JOBNM, USRPRF, DATIME, ABPREFLG) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            TEMPLATE,
            PREMMTHD,
            REASONTRM,
            GSALFREQ,
            CARDTYPE01,
            CARDTYPE02,
            BNFTTMPLA,
            BNFTTMPLB,
            BNFTTMPLC,
            GATEKEEP,
            LMTWHOM,
            PROVNET,
            MBRTYPE,
            PREAUTH,
            DFTBFT,
            BNFTGRTMA,
            BNFTGRTMB,
            MFPLANNO,
            ELIGPERD,
            CLMWAIT,
            DEFPLAN,
            CREQ,
            HEADCNTFLG,
            ASCBFLG,
            ASCBLIMIT,
            ASCBLTBS,
            BENCDE,
            CLASSINS,
            RILDGIND,
            AUTHFLG,
            CTBRULE,
            RTARULE,
            DPNTRULE,
            JOBNM,
            USRPRF,
            DATIME,
            ABPREFLG
      FROM VM1DTA.DFPLPF;
      
 COMMIT;   
 
 /* GMHIPF */
 
 
 ALTER TABLE VM1DTA.GMHIPF 
  ADD(
ISSTAFF NCHAR(1)
);
 COMMIT;  
  /* GMHI */
 CREATE OR REPLACE FORCE VIEW GMHI (UNIQUE_NUMBER, CHDRCOY, CHDRNUM, MBRNO, DPNTNO, EFFDATE, DTETRM, SUBSCOY, SUBSNUM, OCCPCODE, SALARY, DTEAPP, SBSTDL, TERMID, USER_T, TRDT, TRTM, TRANNO, FUPFLG, CLIENT, PERSONCOV, MLVLPLAN, CLNTCOY, PCCCLNT, APCCCLNT, EARNING, CTBPRCNT, CTBAMT, USRPRF, JOBNM, DATIME, ISSTAFF) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            MBRNO,
            DPNTNO,
            EFFDATE,
            DTETRM,
            SUBSCOY,
            SUBSNUM,
            OCCPCODE,
            SALARY,
            DTEAPP,
            SBSTDL,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            TRANNO,
            FUPFLG,
            CLIENT,
            PERSONCOV,
            MLVLPLAN,
            CLNTCOY,
            PCCCLNT,
            APCCCLNT,
            EARNING,
            CTBPRCNT,
            CTBAMT,
            USRPRF,
            JOBNM,
            DATIME,
            ISSTAFF
      FROM VM1DTA.GMHIPF;
      
 COMMIT;   
 
/*  GXHIPF */
 ALTER TABLE VM1DTA.GXHIPF 
  ADD(
LOADREASON NCHAR(50)
);
 COMMIT; 


/*  GXHI */
 CREATE OR REPLACE FORCE VIEW GXHI (UNIQUE_NUMBER, CHDRCOY, CHDRNUM, MBRNO, PRODTYP, PLANNO, EFFDATE, HEADNO, FMLYCDE, DTEATT, DTETRM, REASONTRM, XCESSSI, APRVDATE, ACCPTDTE, SPECTRM, EXTRPRM, SUMINSU, USERSI, DECFLG, DPNTNO, TERMID, USER_T, TRDT, TRTM, TRANNO, EMLOAD, OALOAD, BILLACTN, IMPAIRCD01, IMPAIRCD02, IMPAIRCD03, RIEMLOAD, RIOALOAD, STDPRMLOAD, DTECLAM, USRPRF, JOBNM, DATIME, LOADREASON) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            MBRNO,
            PRODTYP,
            PLANNO,
            EFFDATE,
            HEADNO,
            FMLYCDE,
            DTEATT,
            DTETRM,
            REASONTRM,
            XCESSSI,
            APRVDATE,
            ACCPTDTE,
            SPECTRM,
            EXTRPRM,
            SUMINSU,
            USERSI,
            DECFLG,
            DPNTNO,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            TRANNO,
            EMLOAD,
            OALOAD,
            BILLACTN,
            IMPAIRCD01,
            IMPAIRCD02,
            IMPAIRCD03,
            RIEMLOAD,
            RIOALOAD,
            STDPRMLOAD,
            DTECLAM,
            USRPRF,
            JOBNM,
            DATIME,
			LOADREASON
       FROM VM1DTA.GXHIPF;
      
 COMMIT;  
 
 
 
 
 /*  GBIDPF  */
  ALTER TABLE VM1DTA.GBIDPF 
  ADD(
DISCAMT1  NUMERIC(11, 2),
DISCRATE1 NUMERIC(5, 2),
DISCAMT2  NUMERIC(11, 2),
DISCRATE2 NUMERIC(5, 2)
);
 COMMIT; 
 
 /*  GBID  */
 
 CREATE OR REPLACE FORCE VIEW GBID (UNIQUE_NUMBER
	,CHDRCOY
	,BILLNO
	,PRODTYP
	,PLANNO
	,CLASSINS
	,BPREM
	,BEXTPRM
	,BADVRFUND
	,BCOMM
	,BOVCOMM01
	,BOVCOMM02
	,DISCRATE
	,DISCAMT
	,BATCCOY
	,BATCBRN
	,BATCACTYR
	,BATCACTMN
	,BATCTRCDE
	,BATCBATCH
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,FEES
	,VALIDFLAG
	,WKLADM
	,USRPRF
	,JOBNM
	,DATIME
	,DISCAMT1  
	,DISCRATE1 
	,DISCAMT2  
	,DISCRATE2 
	)
AS
SELECT UNIQUE_NUMBER
	,CHDRCOY
	,BILLNO
	,PRODTYP
	,PLANNO
	,CLASSINS
	,BPREM
	,BEXTPRM
	,BADVRFUND
	,BCOMM
	,BOVCOMM01
	,BOVCOMM02
	,DISCRATE
	,DISCAMT
	,BATCCOY
	,BATCBRN
	,BATCACTYR
	,BATCACTMN
	,BATCTRCDE
	,BATCBATCH
	,TERMID
	,USER_T
	,TRDT
	,TRTM
	,TRANNO
	,FEES
	,VALIDFLAG
	,WKLADM
	,USRPRF
	,JOBNM
	,DATIME
	,DISCAMT1  
	,DISCRATE1 
	,DISCAMT2  
	,DISCRATE2 
FROM VM1DTA.GBIDPF;
      
 COMMIT;  
 
 
 
 /*  EAGHPF */
 
 CREATE TABLE VM1DTA.EAGHPF 
  (
    UNIQUE_NUMBER Number(19)  NOT NULL,
	CHDRCOY nchar(1) NULL,
	CHDRNUM nchar(8) NULL,
	PRODTYP nchar(4) NULL,
	PLANNO  nchar(3) NULL,
	MBRFROM Number(10) NULL,
	MBRTO   Number(10) NULL,
	MBRUPTO Number(10) NULL,
	SPOFROM Number(10) NULL,
	SPOTO	  Number(10) NULL,
	SPOUPTO Number(10) NULL,
	CHIFROM Number(10) NULL,
	CHITO   Number(10) NULL,
	CHIUPTO Number(10) NULL,
	TRANNO  Number(10) NULL,
	USRPRF  nchar(10) NULL,
	JOBNM   nchar(10) NULL,
	DATIME  date NULL,
    CONSTRAINT EAGHPF_PK PRIMARY KEY (UNIQUE_NUMBER)
);

CREATE SEQUENCE "VM1DTA"."SEQ_EAGHPF" MINVALUE 1000000 MAXVALUE 99999999999999999999 INCREMENT BY 1 START WITH 3002058 NOCACHE NOORDER NOCYCLE;


CREATE OR REPLACE TRIGGER "VM1DTA"."TR_EAGHPF" before
  INSERT ON EAGHPF FOR EACH row DECLARE v_pkValue NUMBER;
BEGIN
  SELECT SEQ_EAGHPF.nextval INTO v_pkValue FROM dual;
  :New.unique_number := v_pkValue;
END TR_EAGHPF;
/
ALTER TRIGGER VM1DTA.TR_EAGHPF ENABLE;


 
 /*  GLHDPF */
   ALTER TABLE VM1DTA.GLHDPF 
  ADD(
ABPREFLG  NCHAR(1)
);
 COMMIT; 
 
  /*  GLHD */
  CREATE OR REPLACE FORCE VIEW GLHD (UNIQUE_NUMBER, CHDRCOY, CHDRNUM, PRODTYP, PLANNO, PREMMTHD, DESCN, DTEATT, DTETRM, REASONTRM, GSALFREQ, TERMID, USER_T, TRDT, TRTM, TRANNO, CARDTYPE01, CARDTYPE02, BNFTTMPLA, BNFTTMPLB, BNFTTMPLC, GATEKEEP, LMTWHOM, PROVNET, MBRTYPE, AAD, PREAUTH, DFTBFT, BNFTGRTMA, BNFTGRTMB, MFPLANNO, CFALLOW, USRPRF, JOBNM, DATIME, ABPREFLG) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CHDRNUM,
            PRODTYP,
            PLANNO,
            PREMMTHD,
            DESCN,
            DTEATT,
            DTETRM,
            REASONTRM,
            GSALFREQ,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            TRANNO,
            CARDTYPE01,
            CARDTYPE02,
            BNFTTMPLA,
            BNFTTMPLB,
            BNFTTMPLC,
            GATEKEEP,
            LMTWHOM,
            PROVNET,
            MBRTYPE,
            AAD,
            PREAUTH,
            DFTBFT,
            BNFTGRTMA,
            BNFTGRTMB,
            MFPLANNO,
            CFALLOW,
            USRPRF,
            JOBNM,
            DATIME,
            ABPREFLG
       FROM VM1DTA.GLHDPF;
      
 COMMIT;  
 
 /* ERROR TEXT */
 
 
 INSERT INTO VM1DTA.ERORPF
           (ERORPFX
           ,ERORCOY
           ,ERORLANG
           ,ERORPROG
           ,EROREROR
           ,ERORDESC
           ,TRDT
           ,TRTM
           ,USERID
           ,TERMINALID
           ,USRPRF
           ,JOBNM
           ,DATIME
           ,ERORFILE)
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'ENN1'
           ,'Age exceed renewable age defined'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
COMMIT;           
    
    
 INSERT INTO VM1DTA.ERORPF
           (ERORPFX
           ,ERORCOY
           ,ERORLANG
           ,ERORPROG
           ,EROREROR
           ,ERORDESC
           ,TRDT
           ,TRTM
           ,USERID
           ,TERMINALID
           ,USRPRF
           ,JOBNM
           ,DATIME
           ,ERORFILE)
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFPQ'
           ,'Discount plus Commission > Allowed limit'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
COMMIT;      
 /*Hepl Text*/
 
 
 /*SR9B1*/
 INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'DISCRATE1'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to capture the Discount1 percentage'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
           
 
 
 INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'DESDISC1'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to describe Discount 1'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);           
  
  INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'NOINS1'
           ,'1'
           ,''
           ,'1'
           ,'This field reflects the first discount '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);  
 
   INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'NOINS1'
           ,'2'
           ,''
           ,'1'
           ,'to be applied on the policy�s premium.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);  
    
 /* */
  INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'DISCRATE2'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to capture the Discount2 percentage'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
           
 
 
 INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'DESDISC2'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to describe Discount 2'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);           
  
  INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'NOINS2'
           ,'1'
           ,''
           ,'1'
           ,'This field reflects the first discount '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);  
 
   INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'NOINS2'
           ,'2'
           ,''
           ,'1'
           ,'to be applied on the policy�s premium.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);  
           
           
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'ISSTAFF1'
           ,'1'
           ,''
           ,'1'
           ,'This checkbox is to indicate for staff discount.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);  
           
            
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'ISSTAFF2'
           ,'1'
           ,''
           ,'1'
           ,'This checkbox is to indicate for staff discount.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);    
           
             
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'ABPREFLG'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to indicate if Absolute Premium '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);    
                     
 INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SR9B1'
           ,'ABPREFLG'
           ,'2'
           ,''
           ,'1'
           ,'is applicable for the Product/Plan.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);    
            
            
                               
  /*S9114*/
   INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'DISCRATE1'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to capture the Discount1 percentage'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
           
 
 
 INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'DESDISC1'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to describe Discount 1'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);           
  
  INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'NOINS1'
           ,'1'
           ,''
           ,'1'
           ,'This field reflects the first discount '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);  
 
   INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'NOINS1'
           ,'2'
           ,''
           ,'1'
           ,'to be applied on the policy�s premium.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);  
    
 /* */
  INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'DISCRATE2'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to capture the Discount2 percentage'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
           
 
 
 INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'DESDISC2'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to describe Discount 2'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);           
  
  INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'NOINS2'
           ,'1'
           ,''
           ,'1'
           ,'This field reflects the first discount '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);  
 
   INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'NOINS2'
           ,'2'
           ,''
           ,'1'
           ,'to be applied on the policy�s premium.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);  
           
           
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'ISSTAFF1'
           ,'1'
           ,''
           ,'1'
           ,'This checkbox is to indicate for staff discount.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);  
           
            
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'ISSTAFF2'
           ,'1'
           ,''
           ,'1'
           ,'This checkbox is to indicate for staff discount.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);    
           
             
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'ABPREFLG'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to indicate if Absolute Premium '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);    
                     
 INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'S9114'
           ,'ABPREFLG'
           ,'2'
           ,''
           ,'1'
           ,'is applicable for the Product/Plan.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP); 
           
           
  
/*PQ905*/
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'PLANNO'
           ,'1'
           ,''
           ,'1'
           ,'Protected field to display the plan number.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP); 
           
           
           
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'MBRFROM'
           ,'1'
           ,''
           ,'1'
           ,'Protected fields to display the min/max entry'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP); 


INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'MBRFROM'
           ,'2'
           ,''
           ,'1'
           ,'age and renewal up to age for main member.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP); 

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'MBRTO'
           ,'1'
           ,''
           ,'1'
           ,'Protected fields to display the min/max entry'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP); 


INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'MBRTO'
           ,'2'
           ,''
           ,'1'
           ,'age and renewal up to age for main member.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP); 


INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'MBRUPTO'
           ,'1'
           ,''
           ,'1'
           ,'Protected fields to display the min/max entry'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP); 


INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'MBRUPTO'
           ,'2'
           ,''
           ,'1'
           ,'age and renewal up to age for main member.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP); 


INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'SPOFROM'
           ,'1'
           ,''
           ,'1'
           ,'Protected fields to display the min/max entry'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP); 


INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'SPOTO'
           ,'2'
           ,''
           ,'1'
           ,'age and renewal up to age spouse. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP); 

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'SPOFROM'
           ,'1'
           ,''
           ,'1'
           ,'Protected fields to display the min/max entry'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP); 


INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'SPOTO'
           ,'2'
           ,''
           ,'1'
           ,'age and renewal up to age spouse. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP); 


INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'SPOTO'
           ,'2'
           ,''
           ,'1'
           ,'age and renewal up to age spouse. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP); 

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'SPOUPTO'
           ,'1'
           ,''
           ,'1'
           ,'Protected fields to display the min/max entry'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP); 


INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'SPOUPTO'
           ,'2'
           ,''
           ,'1'
           ,'age and renewal up to age spouse. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP); 




INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'CHIFROM'
           ,'1'
           ,''
           ,'1'
           ,'Protected fields to display the min/max entry '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP); 


INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'CHIFROM'
           ,'2'
           ,''
           ,'1'
           ,'age and renewal up to age for child. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP); 




INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'CHITO'
           ,'1'
           ,''
           ,'1'
           ,'Protected fields to display the min/max entry '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP); 


INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'CHITO'
           ,'2'
           ,''
           ,'1'
           ,'age and renewal up to age for child. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP); 


INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'CHIUPTO'
           ,'1'
           ,''
           ,'1'
           ,'Protected fields to display the min/max entry '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP); 


INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'
           ,'SQ905'
           ,'CHIUPTO'
           ,'2'
           ,''
           ,'1'
           ,'age and renewal up to age for child. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP); 


