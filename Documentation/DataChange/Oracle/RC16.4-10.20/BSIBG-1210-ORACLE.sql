DECLARE 
  cnt number(2,1);
BEGIN
  cnt := 0;
  SELECT count(*) into cnt FROM USER_TAB_COLUMNS where table_name = 'GCMHPF' and column_name ='WHOPAID' ;   
  IF (cnt = 0 ) THEN
    EXECUTE IMMEDIATE (' ALTER TABLE VM1DTA.GCMHPF ADD WHOPAID nchar(1)');
  END IF;
END;
/	

CREATE OR REPLACE VIEW VM1DTA.GCMH(UNIQUE_NUMBER, CHDRCOY, CLAMNUM, GCOCCNO, GCCLMSEQ, CHDRNUM, PRODTYP, GCCLMTNO, GCDPNTNO, CLNTPFX, CLNTCOY, CLNTNUM, GCSTS, DTECLAM, CLAIMCUR, CRATE, PLANNO, FMLYCDE, GCRMK, BNKCHARGE, SPECTRM, GCDBLIND, GCFRPDTE, GCLRPDTE, GCDTHCLM, GCCAUSCD, GCOPPVCD, GCOPBNCD, GCOPIAMT, GCOPBAMT, GCOPBNPY, GCOPDGCD, GCOPPTCD, REQNBCDE, REQNTYPE, GCAUTHBY, DATEAUTH, GCOPRSCD, GCGRSPY, GCNETPY, GCCOINA, DEDUCTPC, GCADVPY, GCTRDRPY, GCLOCCNO, MMFLG, PACCAMT, OVRLMT, DTEPYMTADV, DTEPYMTOTH, DTEATT, DTETRM, REVLINK, REVIND, TERMID, USER_T, TRDT, TRTM, APAIDAMT, INTPAIDAMT, GCSETLMD, GCSECSTS, ZCLTCLMREF, ZCLMRECD, ZCLNTPROC, ZCLMREF, ZFEECURR, ZFEETYP, ZCALCFEE, ZOVRDFEE, GRSKCLS, BENCDE, CLAIMCOND, OUTLOAN, USRPRF, JOBNM, DATIME,CLMPROCID,CLMAPRVID, DIACODE, ONDUTY, ROPENRSN, DCNIND, MEVENT, WHOPAID) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CLAMNUM,
            GCOCCNO,
            GCCLMSEQ,
            CHDRNUM,
            PRODTYP,
            GCCLMTNO,
            GCDPNTNO,
            CLNTPFX,
            CLNTCOY,
            CLNTNUM,
            GCSTS,
            DTECLAM,
            CLAIMCUR,
            CRATE,
            PLANNO,
            FMLYCDE,
            GCRMK,
            BNKCHARGE,
            SPECTRM,
            GCDBLIND,
            GCFRPDTE,
            GCLRPDTE,
            GCDTHCLM,
            GCCAUSCD,
            GCOPPVCD,
            GCOPBNCD,
            GCOPIAMT,
            GCOPBAMT,
            GCOPBNPY,
            GCOPDGCD,
            GCOPPTCD,
            REQNBCDE,
            REQNTYPE,
            GCAUTHBY,
            DATEAUTH,
            GCOPRSCD,
            GCGRSPY,
            GCNETPY,
            GCCOINA,
            DEDUCTPC,
            GCADVPY,
            GCTRDRPY,
            GCLOCCNO,
            MMFLG,
            PACCAMT,
            OVRLMT,
            DTEPYMTADV,
            DTEPYMTOTH,
            DTEATT,
            DTETRM,
            REVLINK,
            REVIND,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            APAIDAMT,
            INTPAIDAMT,
            GCSETLMD,
            GCSECSTS,
            ZCLTCLMREF,
            ZCLMRECD,
            ZCLNTPROC,
            ZCLMREF,
            ZFEECURR,
            ZFEETYP,
            ZCALCFEE,
            ZOVRDFEE,
            GRSKCLS,
            BENCDE,
            CLAIMCOND,
            OUTLOAN,
            USRPRF,
            JOBNM,
            DATIME,
			CLMPROCID,
			CLMAPRVID,
			DIACODE,
			ONDUTY,
			ROPENRSN,
			DCNIND,
			MEVENT,
			WHOPAID
       FROM VM1DTA.GCMHPF;
/

DECLARE 
  cnt number(2,1);
begin
  SELECT count(*) into cnt FROM user_tables where table_name = 'GNMTPF' ;
IF ( cnt = 0  ) THEN  
	EXECUTE IMMediate ('CREATE TABLE VM1DTA.GNMTPF(
	UNIQUE_NUMBER NUMBER(18,0), 
	CLMCOY nchar(1) NULL,
	CLAMNUM nchar(8) NULL,
	GCOCCNO nchar(2) NULL,
	ZCLNTPROC nchar(8) NULL,
	ZCLMREF nchar(20) NULL,
	USRPRF nchar(10) NULL,
	JOBNM nchar(10) NULL,
	DATIME TIMESTAMP (6) NULL,
  CONSTRAINT PK_GNMTPF PRIMARY KEY ("UNIQUE_NUMBER") USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT) TABLESPACE "USERS" ENABLE    
  )'
    ) ;    
end if ;
end;
/

DECLARE 
  cnt number(2,1);
BEGIN
  cnt := 0;
  SELECT count(*) into cnt FROM dba_objects where object_name = 'SEQ_GNMTPF' and object_type= 'SEQUENCE';   
  IF (cnt = 0 ) THEN
    EXECUTE IMMEDIATE (' CREATE SEQUENCE "VM1DTA"."SEQ_GNMTPF" MINVALUE 1000000 MAXVALUE 99999999999999999999 INCREMENT BY 1 START WITH 3002058 NOCACHE NOORDER NOCYCLE');
  END IF;
END;
/

CREATE OR REPLACE TRIGGER "VM1DTA"."TR_GNMTPF" before
  INSERT ON VM1DTA.GNMTPF FOR EACH row DECLARE v_pkValue NUMBER;
BEGIN
  SELECT SEQ_GNMTPF.nextval INTO v_pkValue FROM dual;
  :New.unique_number := v_pkValue;
END TR_GNMTPF;
/

ALTER TRIGGER VM1DTA.TR_GNMTPF ENABLE;


CREATE OR REPLACE VIEW VM1DTA.GNMT (
	UNIQUE_NUMBER
	,CLMCOY
	,CLAMNUM
	,GCOCCNO
	,ZCLNTPROC
	,ZCLMREF
	,USRPRF
	,JOBNM
	,DATIME)
	AS
	SELECT UNIQUE_NUMBER
	,CLMCOY
	,CLAMNUM
	,GCOCCNO
	,ZCLNTPROC
	,ZCLMREF
	,USRPRF
	,JOBNM
	,DATIME
	FROM VM1DTA.GNMTPF;
  /
