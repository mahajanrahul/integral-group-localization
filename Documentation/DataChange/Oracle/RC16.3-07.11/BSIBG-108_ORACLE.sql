CREATE TABLE VM1DTA.GGCLPF(
	UNIQUE_NUMBER NUMBER(18,0) NOT NULL,
	CLMCOY nchar(1) NULL,
	CHDRNUM nchar(1) NULL,
	CLAMNUM nchar(8) NULL,
	GCOCCNO nchar(2) NULL,
	GCCLMSEQ int NULL,
	Z6TAXCDE nchar(6) NULL,
	Z6TAXTYP nchar(1) NULL,
	PAYAMT NUMBER(18, 2) NULL,
	ZGSTCLAMT NUMBER(18, 2) NULL,
	SACSCODE nchar(2) NULL,
	SACSTYPE nchar(2) NULL,
	RDOCNUM nchar(8) NULL,
	PAYORNM nchar(47) NULL,
	TRANDESC nchar(30) NULL,
	BATCACTYR int NULL,
	BATCACTMN int NULL,
	ORIGCCY nchar(3) NULL,
	CRATE NUMBER(18, 9) NULL,
	PROCIND nchar(1) NULL,
	USRPRF nchar(10) NULL,
	JOBNM nchar(10) NULL,
	DATIME TIMESTAMP(6) NULL
) ;

ALTER TABLE VM1DTA.GGCLPF add CONSTRAINT PK_GGCLPF PRIMARY KEY (UNIQUE_NUMBER);

CREATE SEQUENCE VM1DTA.SEQ_GGCLPF MINVALUE 1000000 MAXVALUE 99999999999999999999 INCREMENT BY 1 START WITH 3002058 NOCACHE NOORDER NOCYCLE;


CREATE OR REPLACE TRIGGER VM1DTA.TR_GGCLPF before
  INSERT ON GGCLPF FOR EACH row DECLARE v_pkValue NUMBER;
BEGIN
  SELECT SEQ_GGCLPF.nextval INTO v_pkValue FROM dual;
  :New.unique_number := v_pkValue;
END TR_GGCLPF;
/

CREATE VIEW VM1DTA.GGCL
(UNIQUE_NUMBER,CLMCOY,CHDRNUM,CLAMNUM,GCOCCNO,GCCLMSEQ,Z6TAXCDE,Z6TAXTYP,PAYAMT,ZGSTCLAMT,SACSCODE,SACSTYPE,RDOCNUM,PAYORNM,TRANDESC,BATCACTYR,BATCACTMN,ORIGCCY,CRATE,PROCIND,USRPRF,JOBNM,DATIME) AS
SELECT UNIQUE_NUMBER,
CLMCOY,
CHDRNUM,
CLAMNUM,
GCOCCNO,
GCCLMSEQ,
Z6TAXCDE,
Z6TAXTYP,
PAYAMT,
ZGSTCLAMT,
SACSCODE,
SACSTYPE,
RDOCNUM,
PAYORNM,
TRANDESC,
BATCACTYR,
BATCACTMN,
ORIGCCY,
CRATE,
PROCIND,
USRPRF,
JOBNM,
DATIME
FROM VM1DTA.GGCLPF
WHERE PROCIND='0';


CREATE VIEW VM1DTA.GGCLP
(UNIQUE_NUMBER,CLMCOY,CHDRNUM,CLAMNUM,GCOCCNO,GCCLMSEQ,Z6TAXCDE,Z6TAXTYP,PAYAMT,ZGSTCLAMT,SACSCODE,SACSTYPE,RDOCNUM,PAYORNM,TRANDESC,BATCACTYR,BATCACTMN,ORIGCCY,CRATE,PROCIND,USRPRF,JOBNM,DATIME) AS
SELECT UNIQUE_NUMBER,
CLMCOY,
CHDRNUM,
CLAMNUM,
GCOCCNO,
GCCLMSEQ,
Z6TAXCDE,
Z6TAXTYP,
PAYAMT,
ZGSTCLAMT,
SACSCODE,
SACSTYPE,
RDOCNUM,
PAYORNM,
TRANDESC,
BATCACTYR,
BATCACTMN,
ORIGCCY,
CRATE,
PROCIND,
USRPRF,
JOBNM,
DATIME
FROM VM1DTA.GGCLPF
WHERE PROCIND='1';




CREATE TABLE VM1DTA.GGRIPF(
	UNIQUE_NUMBER NUMBER(18,0) NOT NULL,
	DOCTPFX nchar(2) NULL,
	CHDRCOY nchar(1) NULL,
	CHDRNUM nchar(8) NULL,
	PRODTYP nchar(4) NULL,
	BILLNO nchar(9) NULL,
	TRANCD nchar(4) NULL,
	TRANNO int NULL,
	TAXINVNO nchar(24) NULL,
	TAXINVDT int NULL,
	SACSCODE nchar(2) NULL,
	SACSTYP01 nchar(2) NULL,
	SACSTYP02 nchar(2) NULL,
	SACSTYP03 nchar(2) NULL,
	SACSTYP04 nchar(2) NULL,
	SACSTYP05 nchar(2) NULL,
	SACSTYP06 nchar(2) NULL,
	SACSTYP07 nchar(2) NULL,
	SACSTYP08 nchar(2) NULL,
	SACSTYP09 nchar(2) NULL,
	SACSTYP10 nchar(2) NULL,
	SACSTYP11 nchar(2) NULL,
	SACSTYP12 nchar(2) NULL,
	SACSTYP13 nchar(2) NULL,
	SACSTYP14 nchar(2) NULL,
	SACSTYP15 nchar(2) NULL,
	AGNTACCT nchar(3) NULL,
	TRANAMT01 NUMBER(17, 2) NULL,
	TRANAMT02 NUMBER(17, 2) NULL,
	TRANAMT03 NUMBER(17, 2) NULL,
	TRANAMT04 NUMBER(17, 2) NULL,
	TRANAMT05 NUMBER(17, 2) NULL,
	TRANAMT06 NUMBER(17, 2) NULL,
	TRANAMT07 NUMBER(17, 2) NULL,
	TRANAMT08 NUMBER(17, 2) NULL,
	TRANAMT09 NUMBER(17, 2) NULL,
	TRANAMT10 NUMBER(17, 2) NULL,
	TRANAMT11 NUMBER(17, 2) NULL,
	TRANAMT12 NUMBER(17, 2) NULL,
	TRANAMT13 NUMBER(17, 2) NULL,
	TRANAMT14 NUMBER(17, 2) NULL,
	TRANAMT15 NUMBER(17, 2) NULL,
	TRANAMT16 NUMBER(17, 2) NULL,
	TRANAMT17 NUMBER(17, 2) NULL,
	TRANAMT18 NUMBER(17, 2) NULL,
	TRANAMT19 NUMBER(17, 2) NULL,
	TRANAMT20 NUMBER(17, 2) NULL,
	TRANAMT21 NUMBER(17, 2) NULL,
	TRANAMT22 NUMBER(17, 2) NULL,
	TRANAMT23 NUMBER(17, 2) NULL,
	POLTYPE nchar(3) NULL,
	TAXCODE01 nchar(6) NULL,
	TAXCODE02 nchar(6) NULL,
	TAXCODE03 nchar(6) NULL,
	TAXCODE04 nchar(6) NULL,
	TAXCODE05 nchar(6) NULL,
	TAXCODE06 nchar(6) NULL,
	TAXCODE07 nchar(6) NULL,
	TAXCODE08 nchar(6) NULL,
	TAXCODE09 nchar(6) NULL,
	TAXCODE10 nchar(6) NULL,
	TAXCODE11 nchar(6) NULL,
	TAXCODE12 nchar(6) NULL,
	TAXCODE13 nchar(6) NULL,
	TAXCODE14 nchar(6) NULL,
	TAXCODE15 nchar(6) NULL,
	BATCACTYR int NULL,
	BATCACTMN int NULL,
	ACCTCURR nchar(3) NULL,
	CRATE NUMBER(18, 9) NULL,
	SUBSNO nchar(8) NULL,
	RISBILL nchar(30) NULL,
	RISBDTE int NULL,
	GREASCD nchar(8) NULL,
	GRICDNAME nchar(47) NULL,
	GRNGMNT nchar(4) NULL,
	GRSKCLS nchar(4) NULL,
	TIEXCLIND nchar(1) NULL,
	PROCIND nchar(1) NULL,
	USRPRF nchar(10) NULL,
	JOBNM nchar(10) NULL,
	DATIME TIMESTAMP(6) NULL
) ;

ALTER TABLE VM1DTA.GGRIPF add CONSTRAINT PK_GGRIPF PRIMARY KEY (UNIQUE_NUMBER);

CREATE SEQUENCE VM1DTA.SEQ_GGRIPF MINVALUE 1000000 MAXVALUE 99999999999999999999 INCREMENT BY 1 START WITH 3002058 NOCACHE NOORDER NOCYCLE;


CREATE OR REPLACE TRIGGER VM1DTA.TR_GGRIPF before
  INSERT ON GGRIPF FOR EACH row DECLARE v_pkValue NUMBER;
BEGIN
  SELECT SEQ_GGRIPF.nextval INTO v_pkValue FROM dual;
  :New.unique_number := v_pkValue;
END TR_GGRIPF;
/


CREATE VIEW VM1DTA.GGRI
(UNIQUE_NUMBER,DOCTPFX,CHDRCOY,CHDRNUM,PRODTYP,BILLNO,TRANCD,TRANNO,TAXINVNO,TAXINVDT,SACSCODE,SACSTYP01,SACSTYP02,SACSTYP03,SACSTYP04,SACSTYP05,SACSTYP06,SACSTYP07,SACSTYP08,SACSTYP09,SACSTYP10,SACSTYP11,SACSTYP12,SACSTYP13,SACSTYP14,SACSTYP15,AGNTACCT,TRANAMT01,TRANAMT02,TRANAMT03,TRANAMT04,TRANAMT05,TRANAMT06,TRANAMT07,TRANAMT08,TRANAMT09,TRANAMT10,TRANAMT11,TRANAMT12,TRANAMT13,TRANAMT14,TRANAMT15,TRANAMT16,TRANAMT17,TRANAMT18,TRANAMT19,TRANAMT20,TRANAMT21,TRANAMT22,TRANAMT23,POLTYPE,TAXCODE01,TAXCODE02,TAXCODE03,TAXCODE04,TAXCODE05,TAXCODE06,TAXCODE07,TAXCODE08,TAXCODE09,TAXCODE10,TAXCODE11,TAXCODE12,TAXCODE13,TAXCODE14,TAXCODE15,BATCACTYR,BATCACTMN,ACCTCURR,CRATE,SUBSNO,RISBILL,RISBDTE,GREASCD,GRICDNAME,GRNGMNT,GRSKCLS,TIEXCLIND,PROCIND,USRPRF,JOBNM,DATIME) AS
SELECT UNIQUE_NUMBER,
DOCTPFX,
CHDRCOY,
CHDRNUM,
PRODTYP,
BILLNO,
TRANCD,
TRANNO,
TAXINVNO,
TAXINVDT,
SACSCODE,
SACSTYP01,
SACSTYP02,
SACSTYP03,
SACSTYP04,
SACSTYP05,
SACSTYP06,
SACSTYP07,
SACSTYP08,
SACSTYP09,
SACSTYP10,
SACSTYP11,
SACSTYP12,
SACSTYP13,
SACSTYP14,
SACSTYP15,
AGNTACCT,
TRANAMT01,
TRANAMT02,
TRANAMT03,
TRANAMT04,
TRANAMT05,
TRANAMT06,
TRANAMT07,
TRANAMT08,
TRANAMT09,
TRANAMT10,
TRANAMT11,
TRANAMT12,
TRANAMT13,
TRANAMT14,
TRANAMT15,
TRANAMT16,
TRANAMT17,
TRANAMT18,
TRANAMT19,
TRANAMT20,
TRANAMT21,
TRANAMT22,
TRANAMT23,
POLTYPE,
TAXCODE01,
TAXCODE02,
TAXCODE03,
TAXCODE04,
TAXCODE05,
TAXCODE06,
TAXCODE07,
TAXCODE08,
TAXCODE09,
TAXCODE10,
TAXCODE11,
TAXCODE12,
TAXCODE13,
TAXCODE14,
TAXCODE15,
BATCACTYR,
BATCACTMN,
ACCTCURR,
CRATE,
SUBSNO,
RISBILL,
RISBDTE,
GREASCD,
GRICDNAME,
GRNGMNT,
GRSKCLS,
TIEXCLIND,
PROCIND,
USRPRF,
JOBNM,
DATIME		
FROM VM1DTA.GGRIPF
WHERE PROCIND = '0';


CREATE VIEW VM1DTA.GGRIP
(UNIQUE_NUMBER,DOCTPFX,CHDRCOY,CHDRNUM,PRODTYP,BILLNO,TRANCD,TRANNO,TAXINVNO,TAXINVDT,SACSCODE,SACSTYP01,SACSTYP02,SACSTYP03,SACSTYP04,SACSTYP05,SACSTYP06,SACSTYP07,SACSTYP08,SACSTYP09,SACSTYP10,SACSTYP11,SACSTYP12,SACSTYP13,SACSTYP14,SACSTYP15,AGNTACCT,TRANAMT01,TRANAMT02,TRANAMT03,TRANAMT04,TRANAMT05,TRANAMT06,TRANAMT07,TRANAMT08,TRANAMT09,TRANAMT10,TRANAMT11,TRANAMT12,TRANAMT13,TRANAMT14,TRANAMT15,TRANAMT16,TRANAMT17,TRANAMT18,TRANAMT19,TRANAMT20,TRANAMT21,TRANAMT22,TRANAMT23,POLTYPE,TAXCODE01,TAXCODE02,TAXCODE03,TAXCODE04,TAXCODE05,TAXCODE06,TAXCODE07,TAXCODE08,TAXCODE09,TAXCODE10,TAXCODE11,TAXCODE12,TAXCODE13,TAXCODE14,TAXCODE15,BATCACTYR,BATCACTMN,ACCTCURR,CRATE,SUBSNO,RISBILL,RISBDTE,GREASCD,GRICDNAME,GRNGMNT,GRSKCLS,TIEXCLIND,PROCIND,USRPRF,JOBNM,DATIME) AS
SELECT UNIQUE_NUMBER,
DOCTPFX,
CHDRCOY,
CHDRNUM,
PRODTYP,
BILLNO,
TRANCD,
TRANNO,
TAXINVNO,
TAXINVDT,
SACSCODE,
SACSTYP01,
SACSTYP02,
SACSTYP03,
SACSTYP04,
SACSTYP05,
SACSTYP06,
SACSTYP07,
SACSTYP08,
SACSTYP09,
SACSTYP10,
SACSTYP11,
SACSTYP12,
SACSTYP13,
SACSTYP14,
SACSTYP15,
AGNTACCT,
TRANAMT01,
TRANAMT02,
TRANAMT03,
TRANAMT04,
TRANAMT05,
TRANAMT06,
TRANAMT07,
TRANAMT08,
TRANAMT09,
TRANAMT10,
TRANAMT11,
TRANAMT12,
TRANAMT13,
TRANAMT14,
TRANAMT15,
TRANAMT16,
TRANAMT17,
TRANAMT18,
TRANAMT19,
TRANAMT20,
TRANAMT21,
TRANAMT22,
TRANAMT23,
POLTYPE,
TAXCODE01,
TAXCODE02,
TAXCODE03,
TAXCODE04,
TAXCODE05,
TAXCODE06,
TAXCODE07,
TAXCODE08,
TAXCODE09,
TAXCODE10,
TAXCODE11,
TAXCODE12,
TAXCODE13,
TAXCODE14,
TAXCODE15,
BATCACTYR,
BATCACTMN,
ACCTCURR,
CRATE,
SUBSNO,
RISBILL,
RISBDTE,
GREASCD,
GRICDNAME,
GRNGMNT,
GRSKCLS,
TIEXCLIND,
PROCIND,
USRPRF,
JOBNM,
DATIME		
FROM VM1DTA.GGRIPF
WHERE PROCIND = '1';



CREATE VIEW VM1DTA.GGSTP
(UNIQUE_NUMBER,DOCTPFX,CHDRCOY,CHDRNUM,PRODTYP,BILLNO,TRANCD,TRANNO,TAXINVNO,TAXINVDT,SACSCODE,SACSTYP01,SACSTYP02,SACSTYP03,SACSTYP04,SACSTYP05,SACSTYP06,SACSTYP07,SACSTYP08,SACSTYP09,SACSTYP10,SACSTYP11,SACSTYP12,SACSTYP13,SACSTYP14,SACSTYP15,AGNTACCT,TRANAMT01,TRANAMT02,TRANAMT03,TRANAMT04,TRANAMT05,TRANAMT06,TRANAMT07,TRANAMT08,TRANAMT09,TRANAMT10,TRANAMT11,TRANAMT12,TRANAMT13,TRANAMT14,TRANAMT15,TRANAMT16,TRANAMT17,TRANAMT18,TRANAMT19,TRANAMT20,TRANAMT21,TRANAMT22,TRANAMT23,POLTYPE,TAXCODE01,TAXCODE02,TAXCODE03,TAXCODE04,TAXCODE05,TAXCODE06,TAXCODE07,TAXCODE08,TAXCODE09,TAXCODE10,TAXCODE11,TAXCODE12,TAXCODE13,TAXCODE14,TAXCODE15,BATCACTYR,BATCACTMN,ACCTCURR,CRATE,SUBSNO,SUBSNAME,PROCIND,USRPRF,JOBNM,DATIME) AS
SELECT UNIQUE_NUMBER,
DOCTPFX,
CHDRCOY,
CHDRNUM,
PRODTYP,
BILLNO,
TRANCD,
TRANNO,
TAXINVNO,
TAXINVDT,
SACSCODE,
SACSTYP01,
SACSTYP02,
SACSTYP03,
SACSTYP04,
SACSTYP05,
SACSTYP06,
SACSTYP07,
SACSTYP08,
SACSTYP09,
SACSTYP10,
SACSTYP11,
SACSTYP12,
SACSTYP13,
SACSTYP14,
SACSTYP15,
AGNTACCT,
TRANAMT01,
TRANAMT02,
TRANAMT03,
TRANAMT04,
TRANAMT05,
TRANAMT06,
TRANAMT07,
TRANAMT08,
TRANAMT09,
TRANAMT10,
TRANAMT11,
TRANAMT12,
TRANAMT13,
TRANAMT14,
TRANAMT15,
TRANAMT16,
TRANAMT17,
TRANAMT18,
TRANAMT19,
TRANAMT20,
TRANAMT21,
TRANAMT22,
TRANAMT23,
POLTYPE,
TAXCODE01,
TAXCODE02,
TAXCODE03,
TAXCODE04,
TAXCODE05,
TAXCODE06,
TAXCODE07,
TAXCODE08,
TAXCODE09,
TAXCODE10,
TAXCODE11,
TAXCODE12,
TAXCODE13,
TAXCODE14,
TAXCODE15,
BATCACTYR,
BATCACTMN,
ACCTCURR,
CRATE,
SUBSNO,
SUBSNAME,
PROCIND,
USRPRF,
JOBNM,
DATIME		
FROM VM1DTA.GGSTPF
WHERE PROCIND = '1';


CREATE VIEW VM1DTA.GSTINV
AS
SELECT UNIQUE_NUMBER,
DOCTPFX,
CHDRCOY,
CHDRNUM,
BILLNO,
TRANCD,
TRANNO,
TAXINVNO,
TAXINVDT,
GSTREG,
CLNTNUM,
CLNTNAME,
CLTADDR1,
CLTADDR2,
CLTADDR3,
CLTADDR4,
CLTADDR5,
Z6GSTRGN,
AGNTNUM,
AGNTNAME,
POLTYPE,
POLDESC,
CCDATE,
CRDATE,
BILCURR,
BILCURRD,
GROSPREM,
DICAMT,
ZDISPCT,
TRNSFEE,
BNKCHARGE,
SERVCHRG,
MONENQFEE,
TPCAFEE,
TOTEXGST,
GSTAMT,
GSTRATE,
STMDUTY,
TOAMTPY,
TAXCODE,
TIDBCRNO,
DBCRNO,
DBCRPOLNO,
DBCRDT,
ENDNUM,
EFFDATE,
RSNDESC,
COMMAMT,
COMMPER,
SERNAME,
PROCIND,
ZREFNUM,
AGNTACCT,
ACCNO,
ORGTINO,
ORGTIDT,
CEDCLNT,
CEDPOLNO,
CEDCESSNO,
ZINSNAME,
ORGTOTSI,
RISIRATE,
RISIAMT,
CRATE,
RICOMRATE,
RICOMAMT,
GSTCOMRT,
GSTCOMAMT,
USRPRF,
JOBNM,
DATIME
FROM VM1DTA.GSTIPF
WHERE PROCIND='2' and TRANCD in ('T903', 'T928', 'T927', 'T925', 'B920');



CREATE VIEW VM1DTA.GSTIP AS
SELECT UNIQUE_NUMBER,
DOCTPFX,
CHDRCOY,
CHDRNUM,
BILLNO,
TRANCD,
TRANNO,
TAXINVNO,
TAXINVDT,
GSTREG,
CLNTNUM,
CLNTNAME,
CLTADDR1,
CLTADDR2,
CLTADDR3,
CLTADDR4,
CLTADDR5,
Z6GSTRGN,
AGNTNUM,
AGNTNAME,
POLTYPE,
POLDESC,
CCDATE,
CRDATE,
BILCURR,
BILCURRD,
GROSPREM,
DICAMT,
ZDISPCT,
TRNSFEE,
BNKCHARGE,
SERVCHRG,
MONENQFEE,
TPCAFEE,
TOTEXGST,
GSTAMT,
GSTRATE,
STMDUTY,
TOAMTPY,
TAXCODE,
TIDBCRNO,
DBCRNO,
DBCRPOLNO,
DBCRDT,
ENDNUM,
EFFDATE,
RSNDESC,
COMMAMT,
COMMPER,
SERNAME,
PROCIND,
ZREFNUM,
AGNTACCT,
ACCNO,
ORGTINO,
ORGTIDT,
CEDCLNT,
CEDPOLNO,
CEDCESSNO,
ZINSNAME,
ORGTOTSI,
RISIRATE,
RISIAMT,
CRATE,
RICOMRATE,
RICOMAMT,
GSTCOMRT,
GSTCOMAMT,
USRPRF,
JOBNM,
DATIME
FROM VM1DTA.GSTIPF
WHERE PROCIND='1';


CREATE VIEW VM1DTA.GSTISUM
(UNIQUE_NUMBER,DOCTPFX,CHDRCOY,CHDRNUM,BILLNO,TRANCD,TRANNO,TAXINVNO,TAXINVDT,GSTREG,CLNTNUM,CLNTNAME,CLTADDR1,CLTADDR2,CLTADDR3,CLTADDR4,CLTADDR5,Z6GSTRGN,AGNTNUM,AGNTNAME,POLTYPE,POLDESC,CCDATE,CRDATE,BILCURR,BILCURRD,GROSPREM,DICAMT,ZDISPCT,TRNSFEE,BNKCHARGE,SERVCHRG,MONENQFEE,TPCAFEE,TOTEXGST,GSTAMT,GSTRATE,STMDUTY,TOAMTPY,TAXCODE,TIDBCRNO,DBCRNO,DBCRPOLNO,DBCRDT,ENDNUM,EFFDATE,RSNDESC,COMMAMT,COMMPER,SERNAME,PROCIND,ZREFNUM,USRPRF,JOBNM,DATIME) AS
SELECT UNIQUE_NUMBER,
DOCTPFX,
CHDRCOY,
CHDRNUM,
BILLNO,
TRANCD,
TRANNO,
TAXINVNO,
TAXINVDT,
GSTREG,
CLNTNUM,
CLNTNAME,
CLTADDR1,
CLTADDR2,
CLTADDR3,
CLTADDR4,
CLTADDR5,
Z6GSTRGN,
AGNTNUM,
AGNTNAME,
POLTYPE,
POLDESC,
CCDATE,
CRDATE,
BILCURR,
BILCURRD,
GROSPREM,
DICAMT,
ZDISPCT,
TRNSFEE,
BNKCHARGE,
SERVCHRG,
MONENQFEE,
TPCAFEE,
TOTEXGST,
GSTAMT,
GSTRATE,
STMDUTY,
TOAMTPY,
TAXCODE,
TIDBCRNO,
DBCRNO,
DBCRPOLNO,
DBCRDT,
ENDNUM,
EFFDATE,
RSNDESC,
COMMAMT,
COMMPER,
SERNAME,
PROCIND,
ZREFNUM,
USRPRF,
JOBNM,
DATIME
FROM VM1DTA.GSTIPF ;


CREATE or replace VIEW VM1DTA.GSTITRN
(UNIQUE_NUMBER,DOCTPFX,CHDRCOY,CHDRNUM,BILLNO,TRANCD,TRANNO,TAXINVNO,TAXINVDT,GSTREG,CLNTNUM,CLNTNAME,CLTADDR1,CLTADDR2,CLTADDR3,CLTADDR4,
CLTADDR5,Z6GSTRGN,AGNTNUM,AGNTNAME,POLTYPE,POLDESC,CCDATE,CRDATE,BILCURR,BILCURRD,GROSPREM,DICAMT,ZDISPCT,TRNSFEE,BNKCHARGE,SERVCHRG,
MONENQFEE,TPCAFEE,TOTEXGST,GSTAMT,GSTRATE,STMDUTY,TOAMTPY,TAXCODE,TIDBCRNO,DBCRNO,DBCRPOLNO,DBCRDT,ENDNUM,EFFDATE,RSNDESC,COMMAMT,
COMMPER,SERNAME,PROCIND,ZREFNUM,USRPRF,JOBNM,DATIME) AS
SELECT UNIQUE_NUMBER,
DOCTPFX,
CHDRCOY,
CHDRNUM,
BILLNO,
TRANCD,
TRANNO,
TAXINVNO,
TAXINVDT,
GSTREG,
CLNTNUM,
CLNTNAME,
CLTADDR1,
CLTADDR2,
CLTADDR3,
CLTADDR4,
CLTADDR5,
Z6GSTRGN,
AGNTNUM,
AGNTNAME,
POLTYPE,
POLDESC,
CCDATE,
CRDATE,
BILCURR,
BILCURRD,
GROSPREM,
DICAMT,
ZDISPCT,
TRNSFEE,
BNKCHARGE,
SERVCHRG,
MONENQFEE,
TPCAFEE,
TOTEXGST,
GSTAMT,
GSTRATE,
STMDUTY,
TOAMTPY,
TAXCODE,
TIDBCRNO,
DBCRNO,
DBCRPOLNO,
DBCRDT,
ENDNUM,
EFFDATE,
RSNDESC,
COMMAMT,
COMMPER,
SERNAME,
PROCIND,
ZREFNUM,
USRPRF,
JOBNM,
DATIME
FROM VM1DTA.GSTIPF
WHERE PROCIND='1' or  PROCIND='2' ;


