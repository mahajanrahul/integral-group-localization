﻿CREATE OR REPLACE FUNCTION "VM1DTA"."TRANSLATEKANA" (vIN IN VARCHAR2) return varchar2
is
  vOUT VARCHAR2(75);
begin
  vOUT:=
  TRANSLATE(vIN,
  'ｱｲｳｴｵｶｷｸｹｺｻｼｽｾｿﾀﾁﾂﾃﾄﾅﾆﾇﾈﾉﾊﾋﾌﾍﾎﾏﾐﾑﾒﾓﾔﾕﾖﾗﾘﾙﾚﾛﾜｦﾝｧｨｩｪｫｯｬｭｮABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ﾟﾞ｡､ｰ1234567890!"#$%&()=-~^|\[]@*+;:./?<>,_{}｢｣''`･',
  'ｱｲｳｴｵｶｷｸｹｺｻｼｽｾｿﾀﾁﾂﾃﾄﾅﾆﾇﾈﾉﾊﾋﾌﾍﾎﾏﾐﾑﾒﾓﾔﾕﾖﾗﾘﾙﾚﾛﾜｦﾝｱｲｳｴｵﾂﾔﾕﾖABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ');

  if Length(vOUT)>60 then
     vOUT:=substr(vOUT,1,60);
  end if;

  return vOUT;

END;