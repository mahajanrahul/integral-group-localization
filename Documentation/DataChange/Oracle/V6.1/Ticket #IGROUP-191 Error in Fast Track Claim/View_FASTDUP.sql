
  CREATE OR REPLACE FORCE VIEW "VM1DTA"."FASTDUP" ("UNIQUE_NUMBER", "CHDRCOY", "CLMBATCH", "CHDRNUM", "MBRNO", "DPNTNO", "CLNTCOY", "CLNTNUM", "CLAMNUM", "GCOCCNO", "PRODTYP", "DTEVISIT", "GCDIAGCD", "PROVORG", "ZCLTCLMREF", "ZCLMRECD", "SRVCCODE", "INCURRED", "APAIDAMT", "REQNTYPE", "CRDTCARD", "WHOPAID", "CONTINUE", "SEQNO", "GRSKCLS", "DLVRMODE", "CASHLESS", "GCCAUSCD", "INVOICENO", "EXTRMTYP", "JOBNM", "USRPRF", "DATIME") AS 
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CLMBATCH,
            CHDRNUM,
            MBRNO,
            DPNTNO,
            CLNTCOY,
            CLNTNUM,
            CLAMNUM,
            GCOCCNO,
            PRODTYP,
            DTEVISIT,
            GCDIAGCD,
            PROVORG,
            ZCLTCLMREF,
            ZCLMRECD,
            SRVCCODE,
            INCURRED,
            APAIDAMT,
            REQNTYPE,
            CRDTCARD,
            WHOPAID,
            CONTINUE,
            SEQNO,
            GRSKCLS,
            DLVRMODE,
            CASHLESS,
            GCCAUSCD,
            INVOICENO,
            EXTRMTYP,
            JOBNM,
            USRPRF,
            DATIME
       FROM FASTPF
   ORDER BY CHDRCOY,
            CLMBATCH,
            CHDRNUM,
            CLNTCOY,
            CLNTNUM,
            UNIQUE_NUMBER DESC;
 