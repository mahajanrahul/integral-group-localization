
  CREATE OR REPLACE FORCE VIEW "VM1DTA"."FASTSEQ" ("UNIQUE_NUMBER", "CHDRCOY", "CLMBATCH", "CHDRNUM", "SEQNO", "MBRNO", "DPNTNO", "CLNTCOY", "CLNTNUM", "CLAMNUM", "GCOCCNO", "PRODTYP", "DTEVISIT", "GCDIAGCD", "PROVORG", "ZCLTCLMREF", "ZCLMRECD", "SRVCCODE", "INCURRED", "APAIDAMT", "REQNTYPE", "CRDTCARD", "WHOPAID", "CONTINUE", "GRSKCLS", "DLVRMODE", "CASHLESS", "GCCAUSCD", "INVOICENO", "EXTRMTYP", "JOBNM", "USRPRF", "DATIME") AS 
  SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CLMBATCH,
            CHDRNUM,
            SEQNO,
            MBRNO,
            DPNTNO,
            CLNTCOY,
            CLNTNUM,
            CLAMNUM,
            GCOCCNO,
            PRODTYP,
            DTEVISIT,
            GCDIAGCD,
            PROVORG,
            ZCLTCLMREF,
            ZCLMRECD,
            SRVCCODE,
            INCURRED,
            APAIDAMT,
            REQNTYPE,
            CRDTCARD,
            WHOPAID,
            CONTINUE,
            GRSKCLS,
            DLVRMODE,
            CASHLESS,
            GCCAUSCD,
            INVOICENO,
            EXTRMTYP,
            JOBNM,
            USRPRF,
            DATIME
       FROM FASTPF
   ORDER BY CHDRCOY,
            CLMBATCH,
            CHDRNUM,
            CLNTCOY,
            CLNTNUM,
            SEQNO DESC;
 