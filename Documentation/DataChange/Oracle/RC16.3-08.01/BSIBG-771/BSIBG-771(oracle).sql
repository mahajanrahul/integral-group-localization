
delete from VM1DTA.HELPPF
where HELPPROG ='SQ945' OR (HELPPROG = 'S9352' AND ( HELPITEM= 'onduty' OR HELPITEM='diacode'));

/*New Screen */
 INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
('HP','3','E','S','SQ945','','1','','1','Additional Benefits','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);


/*New Field*/

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
('HP',3,'E','F','S9352','ONDUTY',1,'',1,'indicate the claim occurred during on or off duty hours','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
('HP','3','E','F','S9352','DIACODE',1,'','1','The diagnosis code for claim','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);


INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
('HP','3','E','F','SQ945','CLAMNUM',1,'','1','Group claim number.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);


INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
('HP','3','E','F','SQ945','CHDRNUM',1,'','1','Policy number.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);


INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
('HP','3','E','F','SQ945','GCMBNAME',1,'','1','The name of the claimant','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);


INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
('HP','3','E','F','SQ945','PRODTYP',1,'','1','This is the Product code. ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);


INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
('HP','3','E','F','SQ945','PLANNO',1,'','1','This is the plan number field.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
('HP','3','E','F','SQ945','BENCDE',1,'','1','This is the benefit code.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
('HP','3','E','F','SQ945','ITMSHDES',1,'','1','The description of the benefit code.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
('HP','3','E','F','SQ945','CLAIMCOND',1,'','1','This is the claim condition code. ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
('HP','3','E','F','SQ945','ZUMINSU',1,'','1','The sum insured of the in policy. ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
('HP','3','E','F','SQ945','PRCNT',1,'','1','The percentage % of the benefit.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
('HP','3','E','F','SQ945','DATESTART',1,'','1','start date of temporary disablement peri.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
('HP','3','E','F','SQ945','DATEEND',1,'','1','end date of temporary disablement perid.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
('HP','3','E','F','SQ945','ZGCBENPY',1,'','1','Amount payable for the benefit.  ','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
('HP','3','E','F','SQ945','APAIDAMT',1,'','1','Claim amounthas been aid previously','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
('HP','3','E','F','SQ945','TOTPRE',1,'','1','Outstanding To Be Paid.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP);

commit;