-------------------------------Ticket # 5435 Started------------------
DELETE FROM VM1DTA.CLRRPF WHERE UNIQUE_NUMBER IN (
  SELECT cl.UNIQUE_NUMBER 
  FROM VM1DTA.CLRRPF cl
  LEFT JOIN VM1DTA.CHDRPF ch
  ON cl.FORENUM = ch.CHDRNUM
  AND cl.FOREPFX = ch.CHDRPFX
  AND cl.FORECOY = ch.CHDRCOY
  WHERE cl.CLRRROLE='OW' 
  AND cl.CLNTCOY='9'
  AND cl.CLNTPFX='CN'
  AND ch.CHDRNUM is null);
commit;
-------------------------------Ticket # 5435 End----------------------