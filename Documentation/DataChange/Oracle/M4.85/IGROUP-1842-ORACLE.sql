
CREATE TABLE VM1DTA.ZSNPVPF (
  UNIQUE_NUMBER NUMERIC(18,0) NOT NULL,         
	PLSYDATA NCHAR(400),        
	DATIME TIMESTAMP(7));
	
	
CREATE UNIQUE INDEX "VM1DTA"."PK_ZSNPVPF" ON "VM1DTA"."ZSNPVPF" ("UNIQUE_NUMBER"); 
 
ALTER TABLE "VM1DTA"."ZSNPVPF" ADD CONSTRAINT "PK_ZSNPVPF" PRIMARY KEY ("UNIQUE_NUMBER");

set define off;
create or replace TRIGGER "VM1DTA"."TR_ZSNPVPF" before insert on ZSNPVPF for each row
declare
       v_pkValue  number;
     begin
     select SEQ_ACAGPF.nextval into v_pkValue from dual;
     :New.unique_number := v_pkValue;
   end TR_ZSNPVPF;
   


ALTER TRIGGER "VM1DTA"."TR_ACAGPF" ENABLE;


COMMENT ON COLUMN VM1DTA.ZSNPVPF.PLSYDATA IS 'Policy Transaction Data';
COMMENT ON COLUMN VM1DTA.ZSNPVPF.DATIME IS 'Current Date Time';

       