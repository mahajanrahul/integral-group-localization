ALTER TABLE VM1DTA.USGUPF 
ADD (DISPLUSCOM NCHAR(1),
	DISAGE NCHAR(1));   


CREATE OR REPLACE VIEW VM1DTA.USGU(UNIQUE_NUMBER, USERID, COMPANY, MAXPCNT, PREMMTHD01, PREMMTHD02, PREMMTHD03, PREMMTHD04, PREMMTHD05, PREMMTHD06, PREMMTHD07, PREMMTHD08, PREMMTHD09, PREMMTHD10, PREMMTHD11, RVLPYIND, CLMPYIND, TLRNCPCT01, TLRNCPCT02, GSTFLG, DISPLUSCOM, DISAGE, JOBNM, USRPRF, DATIME) AS
SELECT UNIQUE_NUMBER,
            USERID,
            COMPANY,
            MAXPCNT,
            PREMMTHD01,
            PREMMTHD02,
            PREMMTHD03,
            PREMMTHD04,
            PREMMTHD05,
            PREMMTHD06,
            PREMMTHD07,
            PREMMTHD08,
            PREMMTHD09,
            PREMMTHD10,
            PREMMTHD11,
            RVLPYIND,
            CLMPYIND,
            TLRNCPCT01,
            TLRNCPCT02,
            GSTFLG,
			DISPLUSCOM,
			DISAGE,
            JOBNM,
            USRPRF,
            DATIME
       FROM VM1DTA.USGUPF;
       
commit;