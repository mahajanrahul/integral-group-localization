DECLARE
    varTmp NUMBER:=0;
BEGIN
    SELECT COUNT(*) INTO varTmp FROM VM1DTA.ERORPF WHERE EROREROR = 'RFWJ' and ERORLANG = 'E' and ERORPFX = 'ER';
    IF (varTmp <= 0) THEN
        INSERT INTO VM1DTA.ERORPF
           (ERORPFX
           ,ERORCOY
           ,ERORLANG
           ,ERORPROG
           ,EROREROR
           ,ERORDESC
           ,TRDT
           ,TRTM
           ,USERID
           ,TERMINALID
           ,USRPRF
           ,JOBNM
           ,DATIME
           ,ERORFILE)
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFWJ'
           ,'Enter Doc Received Date'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
    ELSE
        UPDATE VM1DTA.ERORPF SET ERORDESC = 'Enter Doc Received Date' WHERE EROREROR = 'RFWJ' and ERORLANG = 'E' and ERORPFX = 'ER';
    END IF;    
END;
/
