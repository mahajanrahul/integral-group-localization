CREATE TABLE  VM1DTA . GASIPF (
	 UNIQUE_NUMBER   NUMBER(18,0) NOT NULL,
	 CHDRCOY 	 nchar (01) NULL,
	 CHDRNUM 	 nchar (08) NULL,
	 MBRNO 		 nchar (05) NULL,
	 DPNTNO 	 nchar (02) NULL,
	 CLNTCOY 	 nchar (01) NULL,
	 CLNTNUM 	 nchar (08) NULL,
	 PRODTYP 	 nchar (04) NULL,
	 SUMINSU 	 NUMBER  NULL,
	 DTETRM 	 int  NULL,
	 VALIDFLAG   nchar  (01) NULL,
	 USRPRF 	 nchar  (10) NULL,
	 JOBNM       nchar (10) NULL,
	 DATIME      TIMESTAMP (6) NULL,
	 CONSTRAINT PK_GASIPF PRIMARY KEY (UNIQUE_NUMBER) 
	);
	
CREATE SEQUENCE VM1DTA.SEQ_GASIPF MINVALUE 1000000 MAXVALUE 99999999999999999999 INCREMENT BY 1 START WITH 3002058 NOCACHE NOORDER NOCYCLE;


CREATE OR REPLACE TRIGGER VM1DTA.TR_GASIPF before
  INSERT ON GASIPF FOR EACH row DECLARE v_pkValue NUMBER;
BEGIN
  SELECT SEQ_GASIPF.nextval INTO v_pkValue FROM dual;
  :New.unique_number := v_pkValue;
END TR_GASIPF;
/

ALTER TABLE VM1DTA.GCBNPF 
ADD (DATESTART  INT NULL,
    DATEEND  INT NULL)
;

CREATE TABLE  VM1DTA.GPMEPF (
	 UNIQUE_NUMBER   NUMBER(18,0) NOT NULL,
	 CHDRCOY 	 nchar (01) NULL,
	 EFFDATE     int  NULL,
	 GPOLTYP     nchar (03) NULL,
	 PRODTYP 	 nchar (04) NULL,
	 PLANNO 	 nchar (03) NULL,
	 INDUSCAT 	 nchar (02) NULL,
	 OCCPCLAS 	 nchar (02) NULL,
	 AGEMAX 	 int  NULL,
	 AGEMIN 	 int  NULL,
	 ZPRMAMT 	 NUMBER  NULL,
	 USRPRF 	 nchar  (10) NULL,
	 JOBNM       nchar (10) NULL,
	 DATIME      TIMESTAMP (6) NULL,
	 CONSTRAINT PK_GPMEPF PRIMARY KEY (UNIQUE_NUMBER) 
	);

CREATE SEQUENCE VM1DTA.SEQ_GPMEPF MINVALUE 1000000 MAXVALUE 99999999999999999999 INCREMENT BY 1 START WITH 3002058 NOCACHE NOORDER NOCYCLE;


CREATE OR REPLACE TRIGGER VM1DTA.TR_GPMEPF before
  INSERT ON GPMEPF FOR EACH row DECLARE v_pkValue NUMBER;
BEGIN
  SELECT SEQ_GPMEPF.nextval INTO v_pkValue FROM dual;
  :New.unique_number := v_pkValue;
END TR_GPMEPF;
/


CREATE TABLE  VM1DTA.GMTIPF (
	 UNIQUE_NUMBER   NUMBER(18,0) NOT NULL,
	 CHDRCOY 	 nchar (01) NULL,
	 CHDRNUM 	 nchar (08) NULL,
	 MTRVHREG    nchar (25) NULL,
	 MTRVHMKE    nchar (30) NULL,
	 MTRVHMDL 	 nchar (30) NULL,
	 NOFSEAT 	 int  NULL,
	 USRPRF 	 nchar  (10) NULL,
	 JOBNM       nchar (10) NULL,
	 DATIME      TIMESTAMP (6) NULL,
	 CONSTRAINT PK_GMTIPF PRIMARY KEY (UNIQUE_NUMBER) 
	);
	
CREATE SEQUENCE VM1DTA.SEQ_GMTIPF MINVALUE 1000000 MAXVALUE 99999999999999999999 INCREMENT BY 1 START WITH 3002058 NOCACHE NOORDER NOCYCLE;


CREATE OR REPLACE TRIGGER VM1DTA.TR_GMTIPF before
  INSERT ON GMTIPF FOR EACH row DECLARE v_pkValue NUMBER;
BEGIN
  SELECT SEQ_GMTIPF.nextval INTO v_pkValue FROM dual;
  :New.unique_number := v_pkValue;
END TR_GMTIPF;
/

CREATE OR REPLACE VIEW  VM1DTA.GASI ( UNIQUE_NUMBER , CHDRCOY ,  CHDRNUM ,  MBRNO ,  DPNTNO ,  CLNTCOY ,  CLNTNUM ,  PRODTYP ,  SUMINSU ,  DTETRM ,  VALIDFLAG ,  USRPRF ,  JOBNM ,  DATIME ) AS
SELECT  UNIQUE_NUMBER ,
	    CHDRCOY ,
	    CHDRNUM , 
	    MBRNO , 
	    DPNTNO , 
	    CLNTCOY , 
	    CLNTNUM , 
	    PRODTYP , 
	    SUMINSU , 
	    DTETRM , 
	    VALIDFLAG , 
	    USRPRF ,
	    JOBNM ,
	    DATIME 
 FROM VM1DTA.GASIPF;

CREATE OR REPLACE VIEW  VM1DTA.GPME ( UNIQUE_NUMBER ,  CHDRCOY ,  EFFDATE ,  GPOLTYP ,  PRODTYP ,  PLANNO ,  INDUSCAT ,  OCCPCLAS ,  AGEMAX ,  AGEMIN ,  ZPRMAMT ,  USRPRF ,  JOBNM ,  DATIME ) AS
SELECT  UNIQUE_NUMBER ,
	    CHDRCOY , 
	    EFFDATE , 
	    GPOLTYP , 
	    PRODTYP , 
	    PLANNO , 
	    INDUSCAT , 
	    OCCPCLAS , 
	    AGEMAX , 
	    AGEMIN , 
	    ZPRMAMT , 
	    USRPRF , 
	    JOBNM , 
	    DATIME 
 FROM VM1DTA.GPMEPF;

CREATE OR REPLACE VIEW  VM1DTA.GPMEEFF ( UNIQUE_NUMBER ,  CHDRCOY ,  EFFDATE ,  GPOLTYP ,  PRODTYP ,  PLANNO ,  INDUSCAT ,  OCCPCLAS ,  AGEMAX ,  AGEMIN ,  ZPRMAMT ,  USRPRF ,  JOBNM ,  DATIME ) AS
SELECT  UNIQUE_NUMBER ,
	    CHDRCOY , 
	    EFFDATE , 
	    GPOLTYP , 
	    PRODTYP , 
	    PLANNO , 
	    INDUSCAT , 
	    OCCPCLAS , 
	    AGEMAX , 
	    AGEMIN , 
	    ZPRMAMT , 
	    USRPRF , 
	    JOBNM , 
	    DATIME 
 FROM VM1DTA.GPMEPF;

CREATE OR REPLACE VIEW  VM1DTA.GMTI ( UNIQUE_NUMBER ,  CHDRCOY ,  CHDRNUM ,  MTRVHREG ,  MTRVHMKE ,  MTRVHMDL ,  NOFSEAT ,  USRPRF ,  JOBNM ,  DATIME ) AS
SELECT  UNIQUE_NUMBER , 
	    CHDRCOY , 
	    CHDRNUM , 
	    MTRVHREG , 
	    MTRVHMKE , 
	    MTRVHMDL , 
	    NOFSEAT , 
	    USRPRF , 
	    JOBNM , 
	    DATIME 
 FROM VM1DTA.GMTIPF;

CREATE OR REPLACE VIEW  VM1DTA . GCBN (UNIQUE_NUMBER, CHDRCOY, CLAMNUM, GCOCCNO, GCBENSEQ, BENCDE, GCINCAMT, BENAMT, BENBASIS, BENLMT, BENLMTB, GCBENPY, GCSYSPY, GCSYSRP, GCRDRPY, HSUMINSU, BENPC, RASCOY01, RASCOY02, RASCOY03, RASCOY04, RASCOY05, ACCNUM01, ACCNUM02, ACCNUM03, ACCNUM04, ACCNUM05, GRHCQSSI01, GRHCQSSI02, GRHCQSSI03, GRHCQSSI04, GRHCQSSI05, TERMID, USER_T, TRDT, TRTM, GCPROVCD, DATESTART, DATEEND, USRPRF, JOBNM, DATIME) AS
SELECT UNIQUE_NUMBER,
            CHDRCOY,
            CLAMNUM,
            GCOCCNO,
            GCBENSEQ,
            BENCDE,
            GCINCAMT,
            BENAMT,
            BENBASIS,
            BENLMT,
            BENLMTB,
            GCBENPY,
            GCSYSPY,
            GCSYSRP,
            GCRDRPY,
            HSUMINSU,
            BENPC,
            RASCOY01,
            RASCOY02,
            RASCOY03,
            RASCOY04,
            RASCOY05,
            ACCNUM01,
            ACCNUM02,
            ACCNUM03,
            ACCNUM04,
            ACCNUM05,
            GRHCQSSI01,
            GRHCQSSI02,
            GRHCQSSI03,
            GRHCQSSI04,
            GRHCQSSI05,
            TERMID,
            USER_T,
            TRDT,
            TRTM,
            GCPROVCD,
			DATESTART, 
			DATEEND,
            USRPRF,
            JOBNM,
            DATIME
       FROM VM1DTA.GCBNPF;

INSERT INTO  VM1DTA . ERORPF 
           ( ERORPFX 
           , ERORCOY 
           , ERORLANG 
           , ERORPROG 
           , EROREROR 
           , ERORDESC 
           , TRDT 
           , TRTM 
           , USERID 
           , TERMINALID 
           , USRPRF 
           , JOBNM 
           , DATIME 
           , ERORFILE )
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFPS'
           ,'Aggregate SI > Max.SI.'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,sysdate
           ,'');
INSERT INTO  VM1DTA . ERORPF 
           ( ERORPFX 
           , ERORCOY 
           , ERORLANG 
           , ERORPROG 
           , EROREROR 
           , ERORDESC 
           , TRDT 
           , TRTM 
           , USERID 
           , TERMINALID 
           , USRPRF 
           , JOBNM 
           , DATIME 
           , ERORFILE )
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFPU'
           ,'Must be <= max.weeks.'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,sysdate
           ,'');
INSERT INTO  VM1DTA . ERORPF 
           ( ERORPFX 
           , ERORCOY 
           , ERORLANG 
           , ERORPROG 
           , EROREROR 
           , ERORDESC 
           , TRDT 
           , TRTM 
           , USERID 
           , TERMINALID 
           , USRPRF 
           , JOBNM 
           , DATIME 
           , ERORFILE )
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFPV'
           ,'Mem. not have aggregate'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,sysdate
           ,'');		 

COMMIT;
