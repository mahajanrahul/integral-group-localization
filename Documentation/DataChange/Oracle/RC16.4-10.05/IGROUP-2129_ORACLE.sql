	UPDATE vm1dta.DESCPF SET LONGDESC = N'トーゴ' WHERE DESCITEM = 'TG' AND DESCPFX='IT' AND "LANGUAGE" =  'J' AND DESCTABL IN('T3645') and DESCCOY='9'; 

	UPDATE vm1dta.DESCPF SET LONGDESC = N'Age - Member                  ' WHERE DESCITEM = 'AGM' AND DESCPFX='IT' AND LANGUAGE =  
	'J' AND DESCTABL IN('TR93D') and DESCCOY='3'; 

	UPDATE vm1dta.DESCPF SET LONGDESC = N'Age - Policy                  ' WHERE DESCITEM = 'AGP     ' AND DESCPFX='IT' AND "LANGUAGE" =  
	'J' AND DESCTABL IN('TR93D') and DESCCOY='3'; 
	
	UPDATE vm1dta.DESCPF SET LONGDESC = N'Age - Current                 ' WHERE DESCITEM = 'AGC     ' AND DESCPFX='IT' AND "LANGUAGE" =  
	'J' AND DESCTABL IN('TR93D') and DESCCOY='3'; 
	
	UPDATE vm1dta.DESCPF SET LONGDESC = N'Date joined policy            ' WHERE DESCITEM = 'DPL     ' AND DESCPFX='IT' AND "LANGUAGE" =  
	'J' AND DESCTABL IN('TR93D') and DESCCOY='3'; 
	
	UPDATE vm1dta.DESCPF SET LONGDESC = N'Date of employment            ' WHERE DESCITEM = 'DEM     ' AND DESCPFX='IT' AND "LANGUAGE" =  
	'J' AND DESCTABL IN('TR93D') and DESCCOY='3'; 
	
	UPDATE vm1dta.DESCPF SET LONGDESC = N'Minimum Years Vesting         ' WHERE DESCITEM = 'MYV     ' AND DESCPFX='IT' AND "LANGUAGE" =  
	'J' AND DESCTABL IN('TR93D') and DESCCOY='3'; 

	UPDATE vm1dta.DESCPF SET LONGDESC = N'Cash Payment                  ' WHERE DESCITEM = '1' AND DESCPFX='IT' AND LANGUAGE =  
	'J' AND DESCTABL IN('T3676') and DESCCOY='3'; 

	UPDATE vm1dta.DESCPF SET LONGDESC = N'Cheque Payment                ' WHERE DESCITEM = '2' AND DESCPFX='IT' AND LANGUAGE =  
	'J' AND DESCTABL IN('T3676') and DESCCOY='3'; 

	UPDATE vm1dta.DESCPF SET LONGDESC = N'Credit Card                   ' WHERE DESCITEM = '9' AND DESCPFX='IT' AND LANGUAGE =  
	'J' AND DESCTABL IN('T3676') and DESCCOY='3'; 

	UPDATE vm1dta.DESCPF SET LONGDESC = N'Journal                       ' WHERE DESCITEM = '4' AND DESCPFX='IT' AND LANGUAGE =  
	'J' AND DESCTABL IN('T3676') and DESCCOY='3'; 
	
	UPDATE vm1dta.DESCPF SET LONGDESC = N'Other                         ' WHERE DESCITEM = '3' AND DESCPFX='IT' AND LANGUAGE =  
	'J' AND DESCTABL IN('T3676') and DESCCOY='3'; 

	UPDATE vm1dta.DESCPF SET LONGDESC = N'Receipt cancellation          ' WHERE DESCITEM = '8' AND DESCPFX='IT' AND LANGUAGE =  
	'J' AND DESCTABL IN('T3676') and DESCCOY='3'; 

	UPDATE vm1dta.DESCPF SET LONGDESC = N'Waiver of Premium             ' WHERE DESCITEM = '5' AND DESCPFX='IT' AND LANGUAGE =  
	'J' AND DESCTABL IN('T3676') and DESCCOY='3'; 
		
	UPDATE vm1dta.DESCPF SET LONGDESC = N'Debit Authorization           ' WHERE DESCITEM = 'A' AND DESCPFX='IT' AND LANGUAGE =  
	'J' AND DESCTABL IN('TR29R') and DESCCOY='9'; 

	UPDATE vm1dta.DESCPF SET LONGDESC = N'Online Authorization          ' WHERE DESCITEM = 'O' AND DESCPFX='IT' AND LANGUAGE =  
	'J' AND DESCTABL IN('TR29R') and DESCCOY='9'; 

	UPDATE vm1dta.DESCPF SET LONGDESC = N'Point of Sale                 ' WHERE DESCITEM = 'P' AND DESCPFX='IT' AND LANGUAGE =  
	'J' AND DESCTABL IN('TR29R') and DESCCOY='9'; 
	
COMMIT;

	