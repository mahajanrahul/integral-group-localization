INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP' 
           ,''
           ,'E'
           ,'F'
           ,'SR9GO'
           ,'DISCOUNT'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to store discount (in figures)'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP' 
           ,''
           ,'E'
           ,'F'
           ,'SR9GO'
           ,'DISCOUNT'
           ,'2'
           ,''
           ,'1'
           ,' given on R'||'&'||'B.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);

INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP' 
           ,''
           ,'E'
           ,'F'
           ,'SR9BT'
           ,'LOGNUM'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to capture LOG Number'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP' 
           ,''
           ,'E'
           ,'F'
           ,'SR9BT'
           ,'LOGNUM'
           ,'2'
           ,''
           ,'1'
           ,' during claim registration.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR9BX'
           ,'CPYAPLYFLG'
           ,'1'
           ,''
           ,'1'
           ,'This checkbox is used to identify if the particular claim'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR9BX'
           ,'CPYAPLYFLG'
           ,'2'
           ,''
           ,'1'
           ,' has the upgraded R'||'&'||'B co-payment rule applicable'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR9BX'
           ,'CPYAPLYFLG'
           ,'3'
           ,''
           ,'1'
           ,' for other benefits except R'||'&'||'B.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'		   
           ,'SR4IC'
           ,''
           ,'1'
           ,''
           ,'1'
           ,'This screen will be used for creating or enquiring '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'		   
           ,'SR4IC'
           ,''
           ,'2'
           ,''
           ,'1'
           ,'Letter of Guarantee (LOG).'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'CHDRNUM'
           ,'1'
           ,''
           ,'1'
           ,'This field must contain a valid Policy Number.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'CHDRNUM'
           ,'2'
           ,''
           ,'1'
           ,' If the Policy Number is not known, a Policy Owner window'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'CHDRNUM'
           ,'3'
           ,''
           ,'1'
           ,' may be invoked from which the relevant policy can be'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'CHDRNUM'
           ,'4'
           ,''
           ,'1'
           ,' selected. It is mandatory for the action – ‘Create LOG’ and'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'CHDRNUM'
           ,'5'
           ,''
           ,'1'
           ,' ‘Enquiry by Policy Number’ but not required'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'CHDRNUM'
           ,'6'
           ,''
           ,'1'
           ,' for the action – ‘Enquiry by LOG Number’'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'HOSPTYP'
           ,'1'
           ,''
           ,'1'
           ,'It can have values either ‘G’ for Govt.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'HOSPTYP'
           ,'2'
           ,''
           ,'1'
           ,' Hospitals or ‘P’ for Pvt Hospitals.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'HOSPTYP'
           ,'3'
           ,''
           ,'1'
           ,' This field is mandatory for the action – ‘Create LOG’'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'NOFLOGS'
           ,'1'
           ,''
           ,'1'
           ,'It is used to capture the number of LOGs to be generated.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'NOFLOGS'
           ,'2'
           ,''
           ,'1'
           ,' It is mandatory for the action ‘Create LOG”.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'LOGNUM'
           ,'1'
           ,''
           ,'1'
           ,'It is used to capture LOG Number for enquiry purposes.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'LOGNUM'
           ,'2'
           ,''
           ,'1'
           ,' It is mandatory for the action ‘Enquiry by LOG Number’.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'VDTETO'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to capture valid to date.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IC'
           ,'VDTETO'
           ,'2'
           ,''
           ,'1'
           ,' It is mandatory for the action ‘Create LOG’.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'		   
           ,'SR4ID'
           ,''
           ,'1'
           ,''
           ,'1'
           ,'This screen will be used for the confirmation about'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'		   
           ,'SR4ID'
           ,''
           ,'2'
           ,''
           ,'1'
           ,'Issuance of the screen.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4ID'
           ,'CHDRNUM'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to show policy number'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4ID'
           ,'CHDRNUM'
           ,'2'
           ,''
           ,'1'
           ,' as entered on the LOG Maintenance screen, ‘SR4ID’.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4ID'
           ,'OWNERNAME'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display the owner'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4ID'
           ,'OWNERNAME'
           ,'2'
           ,''
           ,'1'
           ,' of the policy.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4ID'
           ,'LOGNUM'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display LOG Numbers'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4ID'
           ,'LOGNUM'
           ,'2'
           ,''
           ,'1'
           ,' to be issued.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4ID'
           ,'VDTEFRM'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display valid from'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4ID'
           ,'VDTEFRM'
           ,'2'
           ,''
           ,'1'
           ,' date for the particular LOG.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4ID'
           ,'VDTETO'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display valid to'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4ID'
           ,'VDTETO'
           ,'2'
           ,''
           ,'1'
           ,' date for the particular LOG.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4ID'
           ,'MGPTXTE02'
           ,'1'
           ,''
           ,'1'
           ,'This field is editable and is used to capture response from'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4ID'
           ,'MGPTXTE02'
           ,'2'
           ,''
           ,'1'
           ,' user whether the user want to print the LOGs or not.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4ID'
           ,'MGPTXTE02'
           ,'3'
           ,''
           ,'1'
           ,' This field has values either ‘Y’ or ‘N’.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'		   
           ,'SR4IE'
           ,''
           ,'1'
           ,''
           ,'1'
           ,'This screen will be used to enquire about'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'		   
           ,'SR4IE'
           ,''
           ,'2'
           ,''
           ,'1'
           ,'Letter of Guarantee (LOG).'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'CHDRNUM'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and have same value as entered on'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'CHDRNUM'
           ,'2'
           ,''
           ,'1'
           ,' the screen ‘SR4IE’. It is mandatory for action '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'CHDRNUM'
           ,'3'
           ,''
           ,'1'
           ,'‘Enquiry by Policy Number’ whereas it is not required for'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'CHDRNUM'
           ,'4'
           ,''
           ,'1'
           ,' action ‘Enquiry by LOG Number’.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'LOGNUM'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and have same value as entered on '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'LOGNUM'
           ,'2'
           ,''
           ,'1'
           ,'the screen ‘SR4IE’. It is mandatory for action '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'LOGNUM'
           ,'3'
           ,''
           ,'1'
           ,'‘Enquiry by LOG Number’ whereas it is not required for'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'LOGNUM'
           ,'4'
           ,''
           ,'1'
           ,' action ‘Enquiry by Policy Number’.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'DTEFRM'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and contains the valid'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'DTEFRM'
           ,'2'
           ,''
           ,'1'
           ,' from for the particular LOG.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'VDTETO'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and contains the valid'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'VDTETO'
           ,'2'
           ,''
           ,'1'
           ,' to date for the particular LOG.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'HOSPTYP'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and contains the hospital type for'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'HOSPTYP'
           ,'2'
           ,''
           ,'1'
           ,' the particular LOG.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'CLAMNUM'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and contains the Claim No. against'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'CLAMNUM'
           ,'2'
           ,''
           ,'1'
           ,' that particular LOG. This field contains value only if the'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IE'
           ,'CLAMNUM'
           ,'3'
           ,''
           ,'1'
           ,' status is ‘Used’ else it is blank.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'		   
           ,'SR4IF'
           ,''
           ,'1'
           ,''
           ,'1'
           ,'This screen will be used to list out the related'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'		   
           ,'SR4IF'
           ,''
           ,'2'
           ,''
           ,'1'
           ,'service codes that are mapped with the benefit codes'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'		   
           ,'SR4IF'
           ,''
           ,'3'
           ,''
           ,'1'
           ,'of the product selected on the claim registration screen.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IF'
           ,'SERVCODE'
           ,'1'
           ,''
           ,'1'
           ,'This field is editable and is used to enter service code'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IF'
           ,'SERVCODE'
           ,'2'
           ,''
           ,'1'
           ,' initials to search the service code.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IF'
           ,'PDESCA'
           ,'1'
           ,''
           ,'1'
           ,'This field is editable and is used to enter description'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IF'
           ,'PDESCA'
           ,'2'
           ,''
           ,'1'
           ,' initials to search the service code.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IF'
           ,'SRVCCODE'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IF'
           ,'SRVCCODE'
           ,'2'
           ,''
           ,'1'
           ,' Service Codes.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IF'
           ,'PDESC'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display description'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IF'
           ,'PDESC'
           ,'2'
           ,''
           ,'1'
           ,' of the service codes.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IF'
           ,'INCURRED'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to enter incurred amount corresponding to'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IF'
           ,'INCURRED'
           ,'2'
           ,''
           ,'1'
           ,' to the particular service code.This field is editable and'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IF'
           ,'INCURRED'
           ,'3'
           ,''
           ,'1'
           ,' contains numeric characters.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IF'
           ,'DISCOUNT'
           ,'1'
           ,''
           ,'1'
           ,'This field is used to store discount (in figures) given on'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IF'
           ,'DISCOUNT'
           ,'2'
           ,''
           ,'1'
           ,' R'||'&'||'B.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'		   
           ,'SR4IG'
           ,''
           ,'1'
           ,''
           ,'1'
           ,'This screen will be used to print Claim Payment Advice.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'CLAMNUM'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display Claim Number'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'CLAMNUM'
           ,'2'
           ,''
           ,'1'
           ,' of the claim and the occurrence of the claim.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'DTEVISIT'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'DTEVISIT'
           ,'2'
           ,''
           ,'1'
           ,' Date of Visit.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'CLAIMSTZ'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display the status'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'CLAIMSTZ'
           ,'2'
           ,''
           ,'1'
           ,' of the claim. All valid status are defined in T9690.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'MBRNO'
           ,'1'
           ,''
           ,'1'
           ,'Member is used to represent the member number, dependent'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'MBRNO'
           ,'2'
           ,''
           ,'1'
           ,' number and member name of the particular member for which'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'MBRNO'
           ,'3'
           ,''
           ,'1'
           ,' claim is filed in that policy. This field is protected.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'CHDRNUM'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display the policy'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'CHDRNUM'
           ,'2'
           ,''
           ,'1'
           ,' number and corresponding policy owner.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'PRVORGN'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display the provider'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'PRVORGN'
           ,'2'
           ,''
           ,'1'
           ,' organization number and its name.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'PRODTYP'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected. This is the Product code. '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'PRODTYP'
           ,'2'
           ,''
           ,'1'
           ,'All valid product codes have to be defined in table T9797.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SR4IG'
           ,'PLANNO'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display plan number.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'		   
           ,'SQ902'
           ,''
           ,'1'
           ,''
           ,'1'
           ,'This screen will be used to display all the Surgery Codes'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'		   
           ,'SQ902'
           ,''
           ,'2'
           ,''
           ,'1'
           ,'attached to the claim.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SQ902'
           ,'SELECT'
           ,'1'
           ,''
           ,'1'
           ,'It is used to select the multiple Surgery Codes to delete.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SQ902'
           ,'SURGCATG'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SQ902'
           ,'SURGCATG'
           ,'2'
           ,''
           ,'1'
           ,' Surgery Codes.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SQ902'
           ,'LONGDESC'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display description'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SQ902'
           ,'LONGDESC'
           ,'2'
           ,''
           ,'1'
           ,' of the surgery codes.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'		   
           ,'SQ903'
           ,''
           ,'1'
           ,''
           ,'1'
           ,'This screen will be used to list out all the surgery codes'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'S'		   
           ,'SQ903'
           ,''
           ,'2'
           ,''
           ,'1'
           ,'mentioned in the table T9698.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SQ903'
           ,'SURGCATG'
           ,'1'
           ,''
           ,'1'
           ,'This field is editable and is used to enter surgery code'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SQ903'
           ,'SURGCATG'
           ,'2'
           ,''
           ,'1'
           ,' initials to search the surgery code.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SQ903'
           ,'PDESCA'
           ,'1'
           ,''
           ,'1'
           ,'This field is editable and is used to enter description'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SQ903'
           ,'PDESCA'
           ,'2'
           ,''
           ,'1'
           ,' initials to search the surgery code.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SQ903'
           ,'SELECT'
           ,'1'
           ,''
           ,'1'
           ,'It is used to select the multiple Surgery Codes to add.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SQ903'
           ,'SURGCODE'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SQ903'
           ,'SURGCODE'
           ,'2'
           ,''
           ,'1'
           ,'Surgery Codes.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SQ903'
           ,'LONGDESC'
           ,'1'
           ,''
           ,'1'
           ,'This field is protected and is used to display description '
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);
		   
INSERT INTO VM1DTA.HELPPF
           (HELPPFX
           ,HELPCOY
           ,HELPLANG
           ,HELPTYPE
           ,HELPPROG
           ,HELPITEM
           ,HELPSEQ
           ,TRANID
           ,VALIDFLAG
           ,HELPLINE
           ,USRPRF
           ,JOBNM
           ,DATIME)
     VALUES
           ('HP'
           ,''
           ,'E'
           ,'F'		   
           ,'SQ903'
           ,'LONGDESC'
           ,'2'
           ,''
           ,'1'
           ,'of the surgery codes.'
           ,'UNDERWR1'
           ,'UNDERWR1'
           ,CURRENT_TIMESTAMP);

INSERT INTO VM1DTA.ERORPF
           (ERORPFX
           ,ERORCOY
           ,ERORLANG
           ,ERORPROG
           ,EROREROR
           ,ERORDESC
           ,TRDT
           ,TRTM
           ,USERID
           ,TERMINALID
           ,USRPRF
           ,JOBNM
           ,DATIME
           ,ERORFILE)
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'GP0059'
           ,'Print LOGs?'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
INSERT INTO VM1DTA.ERORPF
           (ERORPFX
           ,ERORCOY
           ,ERORLANG
           ,ERORPROG
           ,EROREROR
           ,ERORDESC
           ,TRDT
           ,TRTM
           ,USERID
           ,TERMINALID
           ,USRPRF
           ,JOBNM
           ,DATIME
           ,ERORFILE)
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'GP0060'
           ,'Post Dt rule exceeded Continue? (Y/N)'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
INSERT INTO VM1DTA.ERORPF
           (ERORPFX
           ,ERORCOY
           ,ERORLANG
           ,ERORPROG
           ,EROREROR
           ,ERORDESC
           ,TRDT
           ,TRTM
           ,USERID
           ,TERMINALID
           ,USRPRF
           ,JOBNM
           ,DATIME
           ,ERORFILE)
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFPW'
           ,'LOG is in used'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
INSERT INTO VM1DTA.ERORPF
           (ERORPFX
           ,ERORCOY
           ,ERORLANG
           ,ERORPROG
           ,EROREROR
           ,ERORDESC
           ,TRDT
           ,TRTM
           ,USERID
           ,TERMINALID
           ,USRPRF
           ,JOBNM
           ,DATIME
           ,ERORFILE)
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFPD'
           ,'Invalid Hospital Type'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
INSERT INTO VM1DTA.ERORPF
           (ERORPFX
           ,ERORCOY
           ,ERORLANG
           ,ERORPROG
           ,EROREROR
           ,ERORDESC
           ,TRDT
           ,TRTM
           ,USERID
           ,TERMINALID
           ,USRPRF
           ,JOBNM
           ,DATIME
           ,ERORFILE)
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFPC'
           ,'Invalid LOG Number'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
		   
INSERT INTO VM1DTA.FLDDPF (FDID) VALUES ('HOSPTYP');

INSERT INTO VM1DTA.FLDTPF (FDID, LANG, FDTX, COLH01) VALUES ('HOSPTYP', 'E', 'Hospital Type', 'Hospital Type');

ALTER TABLE VM1DTA.GCLDPF
ADD (DISCOUNT NUMBER);

ALTER TABLE VM1DTA.GCLHPF
ADD (
  CLRRSVFLAG CHAR(1), -- Clear Reserve Flag
	CPYAPLYFLG CHAR(1), -- Co-payment applied flag
	CPYAPLYIND CHAR(1), -- Co-payment applied indicator
	CLMPROCID CHAR(10), -- Claim Process User ID
	CLMAPRVID CHAR(10)  -- Claim Approval User ID
	); 

ALTER TABLE VM1DTA.GCMHPF
ADD (CLMPROCID CHAR(10), -- Claim Process User ID
	CLMAPRVID CHAR(10) -- Claim Approval User ID
	);

CREATE TABLE VM1DTA.LOGIPF 
	(
	UNIQUE_NUMBER NUMBER(18,0) NOT NULL, -- Unique Number
	CHDRPFX CHAR(2), -- Policy Header Prefix
	CHDRCOY CHAR(1), -- Policy Header Company
	CHDRNUM CHAR(8), -- Policy Number
	LOGNUM  CHAR(15), -- LOG Number
	HOSPTYP CHAR(1), -- Hospital Type
	CLAMNUM CHAR(8), -- Claim Number
	GCOCCNO CHAR(2), -- Claim Occurrence No
	VDTEFRM NUMBER(8,0), -- Valid Date From
	VDTETO NUMBER(8,0), -- Valid Date To
	VALIDFLAG CHAR(1), -- Valid Flag
	USRPRF CHAR(10), -- User Profile
	JOBNM CHAR(10), -- Job Name
	DATIME TIMESTAMP (6)
	);
	
ALTER TABLE VM1DTA.LOGIPF
  ADD (
    CONSTRAINT PK_LOGIPF PRIMARY KEY (UNIQUE_NUMBER)
  );	
  
CREATE SEQUENCE VM1DTA.SEQ_LOGIPF MINVALUE 1000000 MAXVALUE 99999999999999999999 INCREMENT BY 1 START WITH 3002058 NOCACHE NOORDER NOCYCLE;


CREATE OR REPLACE TRIGGER VM1DTA.TR_LOGIPF before
  INSERT ON LOGIPF FOR EACH row DECLARE v_pkValue NUMBER;
BEGIN
  SELECT SEQ_LOGIPF.nextval INTO v_pkValue FROM dual;
  :New.unique_number := v_pkValue;
END TR_LOGIPF;
/


CREATE TABLE VM1DTA.CPAXPF 
	(
	UNIQUE_NUMBER NUMBER(18,0) NOT NULL, -- Unique Number
	CHDRPFX CHAR(2), -- Policy Header Prefix
	CHDRCOY CHAR(1), -- Policy Header Company
	CHDRNUM CHAR(8), -- Policy Number
	CNTTYPE CHAR(3), -- Policy Type
	AGNTPFX CHAR(2), -- Agent Prefix
	AGNTCOY CHAR(1), -- Agent Company
	AGNTNUM CHAR(5), -- Agent Code
	CLMCOY CHAR(1), -- Claim Company
	CLAMNUM CHAR(8), -- Claim Number
	GCOCCNO CHAR(2), -- Claim Occurrence No
	EMPNO CHAR(12), -- Employee ID
	EMPNUM CHAR(8), -- Employee Number
	CLNTPFX CHAR(2), -- Client Prefix
	CLNTCOY CHAR(1), -- Client Company
	CLNTNUM CHAR(8), -- ID of Insured
	CLTSEX CHAR(1), -- Gender of Insured
	CLTDOB NUMBER(8,0), -- Date of Birth of Insured
	DPNTNO CHAR(8), -- Dependent No
	DEPT CHAR(10), -- Department 
	CCDATE NUMBER(8,0), -- Insured From Date
	CRDATE NUMBER(8,0), -- Insured To Date
	DTEATT NUMBER(8,0), -- Member Start Date
	DATEFRM NUMBER(8,0), -- Admission Date
	DATETO NUMBER(8,0), -- Discharge Date
	PLANNO CHAR(3), -- Plan Number
	PROVORG CHAR(8), -- Provider Organization
	CC01 CHAR(5), -- Agent Code
	CC02 CHAR(3), -- Marketing Staff Code
	GCBENSEQ NUMBER(2,0) -- Amount Paid
	); 
	
ALTER TABLE VM1DTA.CPAXPF
  ADD (
    CONSTRAINT PK_CPAXPF PRIMARY KEY (UNIQUE_NUMBER)
  );	
  
CREATE SEQUENCE VM1DTA.SEQ_CPAXPF MINVALUE 1000000 MAXVALUE 99999999999999999999 INCREMENT BY 1 START WITH 3002058 NOCACHE NOORDER NOCYCLE;


CREATE OR REPLACE TRIGGER VM1DTA.TR_CPAXPF before
  INSERT ON CPAXPF FOR EACH row DECLARE v_pkValue NUMBER;
BEGIN
  SELECT SEQ_CPAXPF.nextval INTO v_pkValue FROM dual;
  :New.unique_number := v_pkValue;
END TR_CPAXPF;
/


CREATE TABLE VM1DTA.GCSCPF
	(
	UNIQUE_NUMBER NUMBER(18,0) NOT NULL, -- Unique Number
	CLMCOY CHAR(1), 	-- Claim Company
	CLAMNUM CHAR(8), 	-- Claim Number
	GCOCCNO CHAR(2),	-- Claim Occurrence No
	GCBENSEQ NUMBER(2,0),		-- Benefit Sequence No
	SURGCDE CHAR(8),	-- Surgery Codes
	USRPRF CHAR(10),	-- User Profile
	JOBNM CHAR(10),	-- Job Name
	DATIME TIMESTAMP (6)	-- Timestamp
	);

ALTER TABLE VM1DTA.GCSCPF
  ADD (
    CONSTRAINT PK_GCSCPF PRIMARY KEY (UNIQUE_NUMBER)
  );	

CREATE SEQUENCE VM1DTA.SEQ_GCSCPF MINVALUE 1000000 MAXVALUE 99999999999999999999 INCREMENT BY 1 START WITH 3002058 NOCACHE NOORDER NOCYCLE;


CREATE OR REPLACE TRIGGER VM1DTA.TR_GCSCPF before
  INSERT ON GCSCPF FOR EACH row DECLARE v_pkValue NUMBER;
BEGIN
  SELECT SEQ_GCSCPF.nextval INTO v_pkValue FROM dual;
  :New.unique_number := v_pkValue;
END TR_GCSCPF;
/

CREATE OR REPLACE VIEW VM1DTA.GCLD
	(UNIQUE_NUMBER, CLMCOY, CLAMNUM, GCOCCNO, GCBENSEQ, DATEFRM, DATETO, NOFDAY, SRVCCODE, NOFUNIT, POS, INCURRED, SYPYPROV, PAYPROV, GDEDUCT, COPAY, SYMBRSHR, MBRSHARE, SYHMOSHR, HMOSHARE, BENCDE, FEESCHID, WHRULE, WHSLAB, FEEBASIS, CLNTCOY, PROVIND, PROVCAP, NETLEVEL, FESCHMTH, NETDKEY, PFSDKEY, BENCDEFLG, HMOSHRMM, BNFTGRP, OUBNFGP, ZDAYCOV, ZCHRGDAY, ZHSLMTUP, MMINBFGP, MMOUBFGP, AMTDISAL, TPADIFF, INVOICENO, EXTRMTYP, GCDIAGCD, JOBNM, USRPRF, DATIME,
	DISCOUNT)
AS SELECT 
	UNIQUE_NUMBER,
	CLMCOY,
	CLAMNUM,
	GCOCCNO,
	GCBENSEQ,
	DATEFRM,
	DATETO,
	NOFDAY,
	SRVCCODE,
	NOFUNIT,
	POS,
	INCURRED,
	SYPYPROV,
	PAYPROV,
	GDEDUCT,
	COPAY,
	SYMBRSHR,
	MBRSHARE,
	SYHMOSHR,
	HMOSHARE,
	BENCDE,
	FEESCHID,
	WHRULE,
	WHSLAB,
	FEEBASIS,
	CLNTCOY,
	PROVIND,
	PROVCAP,
	NETLEVEL,
	FESCHMTH,
	NETDKEY,
	PFSDKEY,
	BENCDEFLG,
	HMOSHRMM,
	BNFTGRP,
	OUBNFGP,
	ZDAYCOV,
	ZCHRGDAY,
	ZHSLMTUP,
	MMINBFGP,
	MMOUBFGP,
	AMTDISAL,
	TPADIFF,
	INVOICENO,
	EXTRMTYP,
	GCDIAGCD,
	JOBNM,
	USRPRF,
	DATIME,
	DISCOUNT 
FROM VM1DTA.GCLDPF;


CREATE OR REPLACE VIEW VM1DTA.GCLDDUP
	(UNIQUE_NUMBER
	,CLMCOY
	,CLAMNUM
	,GCOCCNO
	,GCBENSEQ
	,DATEFRM
	,DATETO
	,NOFDAY
	,SRVCCODE
	,NOFUNIT
	,CLNTCOY
	,PROVIND
	,POS
	,INCURRED
	,SYPYPROV
	,PAYPROV
	,GDEDUCT
	,COPAY
	,SYMBRSHR
	,MBRSHARE
	,SYHMOSHR
	,HMOSHARE
	,BENCDE
	,FEESCHID
	,WHRULE
	,WHSLAB
	,FEEBASIS
	,PROVCAP
	,NETLEVEL
	,FESCHMTH
	,NETDKEY
	,PFSDKEY
	,BENCDEFLG
	,HMOSHRMM
	,BNFTGRP
	,OUBNFGP
	,ZDAYCOV
	,ZCHRGDAY
	,ZHSLMTUP
	,MMINBFGP
	,MMOUBFGP
	,JOBNM
	,USRPRF
	,DATIME
	,AMTDISAL
	,TPADIFF
	,INVOICENO
	,EXTRMTYP
	,GCDIAGCD
	,DISCOUNT
	)
AS SELECT UNIQUE_NUMBER
	,CLMCOY
	,CLAMNUM
	,GCOCCNO
	,GCBENSEQ
	,DATEFRM
	,DATETO
	,NOFDAY
	,SRVCCODE
	,NOFUNIT
	,CLNTCOY
	,PROVIND
	,POS
	,INCURRED
	,SYPYPROV
	,PAYPROV
	,GDEDUCT
	,COPAY
	,SYMBRSHR
	,MBRSHARE
	,SYHMOSHR
	,HMOSHARE
	,BENCDE
	,FEESCHID
	,WHRULE
	,WHSLAB
	,FEEBASIS
	,PROVCAP
	,NETLEVEL
	,FESCHMTH
	,NETDKEY
	,PFSDKEY
	,BENCDEFLG
	,HMOSHRMM
	,BNFTGRP
	,OUBNFGP
	,ZDAYCOV
	,ZCHRGDAY
	,ZHSLMTUP
	,MMINBFGP
	,MMOUBFGP
	,JOBNM
	,USRPRF
	,DATIME
	,AMTDISAL
	,TPADIFF
	,INVOICENO
	,EXTRMTYP
	,GCDIAGCD
	,DISCOUNT 
FROM VM1DTA.GCLDPF;


CREATE OR REPLACE VIEW VM1DTA.GCLH (
	UNIQUE_NUMBER
	,CLMCOY
	,CLAMNUM
	,GCOCCNO
	,CHDRNUM
	,MBRNO
	,DPNTNO
	,CLNTCOY
	,CLNTNUM
	,GCSTS
	,CLAIMCUR
	,CRATE
	,PRODTYP
	,GRSKCLS
	,DTEVISIT
	,DTEDCHRG
	,GCDIAGCD
	,PLANNO
	,PREAUTNO
	,PROVORG
	,AREACDE
	,REFERRER
	,CLAMTYPE
	,TIMEHH
	,TIMEMM
	,ZCLTCLMREF
	,GCDTHCLM
	,APAIDAMT
	,REQNTYPE
	,CRDTCARD
	,WHOPAID
	,DTEKNOWN
	,GCFRPDTE
	,ZCLMRECD
	,MCFROM
	,MCTO
	,GDEDUCT
	,COPAY
	,MBRTYPE
	,PROVNET
	,AAD
	,THIRDRCVY
	,THIRDPARTY
	,TLMBRSHR
	,TLHMOSHR
	,DATEAUTH
	,GCAUTHBY
	,GCOPRSCD
	,REVLINK
	,REVIND
	,TPRCVPND
	,PENDFROM
	,MMPROD
	,HMOSHRMM
	,TAKEUP
	,DATACONV
	,CLRATE
	,REFNO
	,UPDIND
	,PREMRCVY
	,DATEAUTHF
	,DATEAUTHS
	,GCAUTHBYF
	,GCAUTHBYS
	,CASHLESS
	,TPAREFNO
	,INWARDNO
	,ICD101L
	,ICD102L
	,ICD103L
	,REGCLM
	,GCCAUSCD
	,JOBNM
	,USRPRF
	,DATIME
	,TLCFAMT
	,CLMPREVLMT
	,CLRRSVFLAG
	,CPYAPLYFLG
	,CLMPROCID
	,CLMAPRVID
	,CPYAPLYIND
	)
AS
SELECT UNIQUE_NUMBER
	,CLMCOY
	,CLAMNUM
	,GCOCCNO
	,CHDRNUM
	,MBRNO
	,DPNTNO
	,CLNTCOY
	,CLNTNUM
	,GCSTS
	,CLAIMCUR
	,CRATE
	,PRODTYP
	,GRSKCLS
	,DTEVISIT
	,DTEDCHRG
	,GCDIAGCD
	,PLANNO
	,PREAUTNO
	,PROVORG
	,AREACDE
	,REFERRER
	,CLAMTYPE
	,TIMEHH
	,TIMEMM
	,ZCLTCLMREF
	,GCDTHCLM
	,APAIDAMT
	,REQNTYPE
	,CRDTCARD
	,WHOPAID
	,DTEKNOWN
	,GCFRPDTE
	,ZCLMRECD
	,MCFROM
	,MCTO
	,GDEDUCT
	,COPAY
	,MBRTYPE
	,PROVNET
	,AAD
	,THIRDRCVY
	,THIRDPARTY
	,TLMBRSHR
	,TLHMOSHR
	,DATEAUTH
	,GCAUTHBY
	,GCOPRSCD
	,REVLINK
	,REVIND
	,TPRCVPND
	,PENDFROM
	,MMPROD
	,HMOSHRMM
	,TAKEUP
	,DATACONV
	,CLRATE
	,REFNO
	,UPDIND
	,PREMRCVY
	,DATEAUTHF
	,DATEAUTHS
	,GCAUTHBYF
	,GCAUTHBYS
	,CASHLESS
	,TPAREFNO
	,INWARDNO
	,ICD101L
	,ICD102L
	,ICD103L
	,REGCLM
	,GCCAUSCD
	,JOBNM
	,USRPRF
	,DATIME
	,TLCFAMT
	,CLMPREVLMT
	,CLRRSVFLAG
	,CPYAPLYFLG
	,CLMPROCID
	,CLMAPRVID
	,CPYAPLYIND
FROM VM1DTA.GCLHPF;
CREATE VIEW VM1DTA.LOGI
	(
	UNIQUE_NUMBER, 
	CHDRPFX, 
	CHDRCOY, 
	CHDRNUM, 
	LOGNUM,  
	HOSPTYP, 
	CLAMNUM, 
	VALIDFLAG,
	GCOCCNO,
	VDTEFRM,
	VDTETO, 
	USRPRF,
	JOBNM, 
	DATIME 
	)
AS SELECT 
	UNIQUE_NUMBER, 
	CHDRPFX, 
	CHDRCOY, 
	CHDRNUM, 
	LOGNUM,  
	HOSPTYP, 
	CLAMNUM, 
	VALIDFLAG,
	GCOCCNO, 
	VDTEFRM, 
	VDTETO,
	USRPRF,
	JOBNM, 
	DATIME 
FROM VM1DTA.LOGIPF;

CREATE VIEW VM1DTA.CPAX
	(
	UNIQUE_NUMBER, 
	CHDRPFX,       
	CHDRCOY,       
	CHDRNUM,       
	CNTTYPE,       
	AGNTPFX,       
	AGNTCOY,       
	AGNTNUM,       
	CLMCOY,        
	CLAMNUM,       
	GCOCCNO,       
	EMPNO,         
	EMPNUM,        
	CLNTPFX,       
	CLNTCOY,       
	CLNTNUM,       
	CLTSEX,        
	CLTDOB,        
	DPNTNO,        
	DEPT,          
	CCDATE,        
	CRDATE,        
	DTEATT,        
	DATEFRM,       
	DATETO,        
	PLANNO,        
	PROVORG,       
	CC01,          
	CC02,          
	GCBENSEQ
	)
AS SELECT 
	UNIQUE_NUMBER, 
	CHDRPFX,        
	CHDRCOY,        
	CHDRNUM,        
	CNTTYPE,        
	AGNTPFX,        
	AGNTCOY,        
	AGNTNUM,        
	CLMCOY,         
	CLAMNUM,        
	GCOCCNO,        
	EMPNO,          
	EMPNUM,         
	CLNTPFX,        
	CLNTCOY,        
	CLNTNUM,        
	CLTSEX,         
	CLTDOB,         
	DPNTNO,         
	DEPT,           
	CCDATE,         
	CRDATE,         
	DTEATT,         
	DATEFRM,        
	DATETO,         
	PLANNO,         
	PROVORG,        
	CC01,           
	CC02,           
	GCBENSEQ
FROM VM1DTA.CPAXPF;

CREATE VIEW VM1DTA.GCSC
	(
	UNIQUE_NUMBER, 	
	CLMCOY,			
	CLAMNUM,		
	GCOCCNO,		
	GCBENSEQ,		
	SURGCDE,		
	USRPRF,			
	JOBNM,			
	DATIME			
	)
AS SELECT
	UNIQUE_NUMBER, 	
	CLMCOY,			
	CLAMNUM,		
	GCOCCNO,		
	GCBENSEQ,		
	SURGCDE,		
	USRPRF,			
	JOBNM,			
	DATIME			
FROM VM1DTA.GCSCPF;

commit;
