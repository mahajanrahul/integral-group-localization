DELETE FROM VM1DTA.ERORPF WHERE ERORLANG='E' AND EROREROR = 'RFRY';

DELETE FROM VM1DTA.ERORPF WHERE ERORLANG='E' AND EROREROR = 'RFUI';

INSERT INTO VM1DTA.ERORPF
           (ERORPFX
           ,ERORCOY
           ,ERORLANG
           ,ERORPROG
           ,EROREROR
           ,ERORDESC
           ,TRDT
           ,TRTM
           ,USERID
           ,TERMINALID
           ,USRPRF
           ,JOBNM
           ,DATIME
           ,ERORFILE)
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFRY'
           ,'Adjust Limits'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
 INSERT INTO VM1DTA.ERORPF
           (ERORPFX
           ,ERORCOY
           ,ERORLANG
           ,ERORPROG
           ,EROREROR
           ,ERORDESC
           ,TRDT
           ,TRTM
           ,USERID
           ,TERMINALID
           ,USRPRF
           ,JOBNM
           ,DATIME
           ,ERORFILE)
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFUI'
           ,'Issue Trial Bill not yet Run'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
COMMIT