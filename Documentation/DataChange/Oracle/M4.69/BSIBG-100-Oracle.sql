
CREATE TABLE VM1DTA.FWHDPF(
	UNIQUE_NUMBER NUMBER(18) NOT NULL,
	CHDRCOY nchar(1) NULL,
	CHDRNUM nchar(8) NULL,
	CNTTYPE nchar(3) NULL,
	EMPID nchar(12) NULL,
	IMMIGCODE nchar(8) NULL,
	CCDATE int NULL,
	CRDATE int NULL,
	PPERIOD numeric(2,0) NULL,
	IGGRPREF nchar(2) NULL,
	JOBNM nchar(10) NULL,
	DATIME TIMESTAMP(6) NULL);

CREATE OR REPLACE FORCE VIEW VM1DTA.FWHD(UNIQUE_NUMBER, CHDRCOY, CHDRNUM, CNTTYPE, EMPID, IMMIGCODE, CCDATE, CRDATE, PPERIOD, IGGRPREF, JOBNM, DATIME) AS
SELECT UNIQUE_NUMBER,CHDRCOY, CHDRNUM, CNTTYPE, EMPID, IMMIGCODE, CCDATE, CRDATE, PPERIOD, IGGRPREF, JOBNM, DATIME
       FROM VM1DTA.FWHDPF;


INSERT INTO VM1dta.FLDD
           (FDID
           ,NMPG
           ,TYPE_T
           ,EDIT
           ,LENF
           ,DECP
           ,WNTP
           ,WNST
           ,TABW
           ,TCOY
           ,ERRCD
           ,PROG01
           ,PROG02
           ,PROG03
           ,PROG04
           ,CFID
           ,TERMID
           ,USER_T
           ,TRDT
           ,TRTM
           ,ADDF01
           ,ADDF02
           ,ADDF03
           ,ADDF04
           ,CURRFDID
           ,ALLOCLEN
           ,DBCSCAP
           ,USRPRF
           ,JOBNM
           ,DATIME)
VALUES     ('IMMIGSEL  '
           ,'IMMIGSEL                '
           ,'A'
           ,' '
           ,'10'
           ,'0'
           ,'C'
           ,'N'
           ,'     '
           ,' '
           ,'    '
           ,'CLTWD'
           ,'P2464'
           ,'     '
           ,'     '
           ,'IMMIGNAME '
           ,'MK1 '
           ,'0'
           ,'880811'
           ,'165502'
           ,'          '
           ,'          '
           ,'          '
           ,'          '
           ,'          '
           ,'0'
           ,'N'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP); 
COMMIT;

INSERT INTO VM1DTA.FLDD
           (FDID
           ,NMPG
           ,TYPE_T
           ,EDIT
           ,LENF
           ,DECP
           ,WNTP
           ,WNST
           ,TABW
           ,TCOY
           ,ERRCD
           ,PROG01
           ,PROG02
           ,PROG03
           ,PROG04
           ,CFID
           ,TERMID
           ,USER_T
           ,TRDT
           ,TRTM
           ,ADDF01
           ,ADDF02
           ,ADDF03
           ,ADDF04
           ,CURRFDID
           ,ALLOCLEN
           ,DBCSCAP
           ,USRPRF
           ,JOBNM
           ,DATIME)
VALUES     ('IMMIGNAME '
           ,'IMMIGNAME               '
           ,'A'
           ,' '
           ,'47'
           ,'0'
           ,' '
           ,' '
           ,'     '
           ,' '
           ,'    '
           ,'     '
           ,'     '
           ,'     '
           ,'     '
           ,'          '
           ,'TR1T'
           ,'0'
           ,'890119'
           ,'112346'
           ,'          '
           ,'          '
           ,'          '
           ,'          '
           ,'          '
           ,'0'
           ,'Y'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP); 
COMMIT;

INSERT INTO VM1DTA.FLDD
           (FDID
           ,NMPG
           ,TYPE_T
           ,EDIT
           ,LENF
           ,DECP
           ,WNTP
           ,WNST
           ,TABW
           ,TCOY
           ,ERRCD
           ,PROG01
           ,PROG02
           ,PROG03
           ,PROG04
           ,CFID
           ,TERMID
           ,USER_T
           ,TRDT
           ,TRTM
           ,ADDF01
           ,ADDF02
           ,ADDF03
           ,ADDF04
           ,CURRFDID
           ,ALLOCLEN
           ,DBCSCAP
           ,USRPRF
           ,JOBNM
           ,DATIME)
VALUES     ('EMPSEL    '
           ,'EMPSEL                  '
           ,'A'
           ,' '
           ,'10'
           ,'0'
           ,'P'
           ,'D'
           ,'     '
           ,' '
           ,'    '
           ,'CLTWD'
           ,'PQ976'
           ,'PQ977'
           ,'     '
           ,'          '
           ,'DSP2'
           ,'0'
           ,'890926'
           ,'112629'
           ,'          '
           ,'          '
           ,'          '
           ,'          '
           ,'          '
           ,'0'
           ,'N'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP); 
COMMIT;

INSERT INTO VM1DTA.ERORPF
           (ERORPFX
           ,ERORCOY
           ,ERORLANG
           ,ERORPROG
           ,EROREROR
           ,ERORDESC
           ,TRDT
           ,TRTM
           ,USERID
           ,TERMINALID
           ,USRPRF
           ,JOBNM
           ,DATIME
           ,ERORFILE)
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFS9'
           ,'EmployerID not exists'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
COMMIT;

INSERT INTO VM1DTA.ERORPF
           (ERORPFX
           ,ERORCOY
           ,ERORLANG
           ,ERORPROG
           ,EROREROR
           ,ERORDESC
           ,TRDT
           ,TRTM
           ,USERID
           ,TERMINALID
           ,USRPRF
           ,JOBNM
           ,DATIME
           ,ERORFILE)
     VALUES
           ('ER'
           ,''
           ,'E'
           ,''
           ,'RFQ9'
           ,'Invalid POI'
           ,'0' 
           ,'0'
           ,'000036'
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,'UNDERWR1  '
           ,CURRENT_TIMESTAMP
           ,'');
COMMIT;

