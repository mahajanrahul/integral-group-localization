INSERT INTO [VM1DTA].[HELPPF]
           ([HELPPFX]
           ,[HELPCOY]
           ,[HELPLANG]
           ,[HELPTYPE]
           ,[HELPPROG]
           ,[HELPITEM]
           ,[HELPSEQ]
           ,[TRANID]
           ,[VALIDFLAG]
           ,[HELPLINE]
           ,[USRPRF]
           ,[JOBNM]
           ,[DATIME])
VALUES ('HP','','E','S','SQ921','','1','','1','Foreign Worker Fee Screen','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','S','SQ922','','1','','1','Mandatory Fields by Policy/Product Type Screen','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ921','FEE','1','','1','The fee (SF for FWCS or MCOF for FWHS) charged per worker.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ922','YNFLG01','1','','1','Industry Code -> Policy Header (Sg004).','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ922','YNFLG02','1','','1','Employer Group -> Policy Header(Sg004)','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ922','YNFLG03','1','','1','Occupation Code -> Group Member Entry(S9106)','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ922','YNFLG04','1','','1','Occupation Class -> Group Member Entry(S9106)','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ922','YNFLG05','1','','1','Insured For -> Group Member Entry(S9106)','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ922','YNFLG06','1','','1','Insured Status -> Group Member Entry(S9106)','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ922','YNFLG07','1','','1','Employment Site -> Group Member Entry(S9106)','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ922','YNFLG08','1','','1','Immigration Code -> Policy Header(Sg004)','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ922','YNFLG09','1','','1','LOG Number -> Policy Header(Sg004)','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ922','YNFLG10','1','','1','Employment details -> Group Member (S9106)','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ922','YNFLG11','1','','1','Work Permit Effective Date -> Group Member (S9106)','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ922','YNFLG12','1','','1','Work Permit Expiry Date -> Group Member (S9106)','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ922','YNFLG13','1','','1','Work Permit No. -> Group Member (S9106)','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','SQ922','YNFLG14','1','','1','FWCMS Reference No. -> Group Member Detail(S9106)','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','S9106','INSFOR','1','','1','This is used to store the insured for code.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','S9106','INSSTS','1','','1','This is used to store the insured status code.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','S9106','PLACEMP','1','','1','This is used to store the place of employment details.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','S9106','PMTDTE','1','','1','This is used to store the effective date of work permit.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','S9106','PMEXDT','1','','1','This is used to store the expiry date of work permit.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','S9106','PERMIT','1','','1','This is used to store the work permit number.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP),
('HP','','E','F','S9106','FWCMSREF','1','','1','This is used to store the FWCMS reference no.','UNDERWR1','UNDERWR1',CURRENT_TIMESTAMP)

--Create table FWMDPF
CREATE TABLE "VM1DTA"."FWMDPF" 
   (	
    "UNIQUE_NUMBER" NUMBER(18,0) NOT NULL , 
    "CHDRCOY" CHAR(1 CHAR), 
    "CHDRNUM" CHAR(8 CHAR), 
    "MBRNO" CHAR(5 CHAR), 
    "DPNTNO" CHAR(2 CHAR), 
    "INSFOR" CHAR(1 CHAR), 
    "INSSTS" CHAR(1 CHAR), 
    "PLACEMP" CHAR(30 CHAR), 
    "PMTDTE" NUMBER(12,0), 
    "PMEXDT" NUMBER(12,0), 
    "PERMIT" CHAR(20 CHAR), 
    "FWCMSREF" CHAR(20 CHAR), 
    "USRPRF" CHAR(10 CHAR), 
    "JOBNM" CHAR(10 CHAR), 
    "DATIME" TIMESTAMP (6)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "VM1DTA_DATA" ;

CREATE SEQUENCE VM1DTA.FWMDPF_seq START WITH 1 INCREMENT BY 1;
CREATE OR REPLACE TRIGGER VM1DTA.FWMDPF_seq_tr
 BEFORE INSERT ON VM1DTA.FWMDPF FOR EACH ROW
 WHEN (NEW.UNIQUE_NUMBER IS NULL)
BEGIN
 SELECT VM1DTA.FWMDPF_seq.NEXTVAL INTO :NEW.UNIQUE_NUMBER FROM DUAL;
END;

-- Create view FWMD
CREATE OR REPLACE FORCE VIEW "VM1DTA"."FWMD" ("UNIQUE_NUMBER", "CHDRCOY", "CHDRNUM", "MBRNO", "DPNTNO", "INSFOR", "INSSTS", "PLACEMP", "PMTDTE", "PMEXDT", "PERMIT", "FWCMSREF", "USRPRF", "JOBNM", "DATIME")
AS
  SELECT UNIQUE_NUMBER,
    CHDRCOY,
    CHDRNUM,
    MBRNO,
    DPNTNO,
    INSFOR,
    INSSTS,
    PLACEMP,
    PMTDTE,
    PMEXDT,
    PERMIT,
    FWCMSREF,
    USRPRF,
    JOBNM,
    DATIME
  FROM FWMDPF;
  COMMIT;
