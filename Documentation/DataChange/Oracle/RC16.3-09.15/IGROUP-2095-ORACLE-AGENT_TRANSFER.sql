CREATE TABLE ZAGRPF (
UNIQUE_NUMBER NUMBER(18,0) NOT NULL,
AGENTPFX_FROM               NCHAR(2),
AGENTCOY_FROM               NCHAR(1),
AGENT_FROM                  NCHAR(8),
VALIDFLAG                   NCHAR(1),
AGENTPFX_TO                 NCHAR(2),
AGENTCOY_TO                 NCHAR(1),
AGENT_TO                    NCHAR(8),
EFFECTIVE_DATE              NUMERIC(8,0),
COMPLISHED_DATE             NUMERIC(8,0),
TRANSACTION_DATE            NUMERIC(8,0),
USRPRF                      NCHAR(10) ,
JOBNM                       NCHAR(10) ,
DATIME                      TIMESTAMP (6)
);

CREATE UNIQUE INDEX "VM1DTA"."PK_ZAGRPF" ON "VM1DTA"."ZAGRPF" ("UNIQUE_NUMBER"); 
 
ALTER TABLE "VM1DTA"."ZAGRPF" ADD CONSTRAINT "PK_ZAGRPF" PRIMARY KEY ("UNIQUE_NUMBER");

set define off;

CREATE SEQUENCE SEQ_ZAGRPF INCREMENT BY 1 MAXVALUE 99999999999999999999 MINVALUE 1000000 NOCACHE;

create or replace TRIGGER "VM1DTA"."TR_ZAGRPF" before insert on ZAGRPF for each row
declare
       v_pkValue  number;
     begin
     select SEQ_ZAGRPF.nextval into v_pkValue from dual;
     :New.unique_number := v_pkValue;
   end TR_ZAGRPF;

/
ALTER TRIGGER "VM1DTA"."TR_ZAGRPF" ENABLE;


